<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jonathan.alba
 * Date: 14/05/12
 * Time: 16:50
 * To change this template use File | Settings | File Templates.
 */
class Controller_Login extends Controller_Padrao
{
    /**
     * Chama o construtor da classe pai
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método inicial que faz a renderização básica da página
     * @param $parametros
     * @return void
     */
    public function index($parametros)
    {

        if (isset($parametros->email) AND strlen($parametros->email)>0 AND isset($parametros->senha) AND strlen($parametros->senha)>0)
        {
            $cliente = new Model_Cliente();
            $cliente->senha = sha1(Funcoes::mysqli_escape($parametros->senha));
            $cliente->email = Funcoes::mysqli_escape($parametros->email);
            $cliente = $cliente->select("SELECT * FROM {tabela_nome} WHERE senha='".$cliente->senha."' AND email='".$cliente->email."' AND ativo=1 AND tipo = 1");
            if (isset($cliente) AND $cliente)
            {
                $atualiza = new Model_Cliente();
                $atualiza->id = $cliente->id;
                $atualiza->ultimo_login_data = date('d/m/Y H:i:s');
                $atualiza->update();
                $_SESSION['cliente_id'] = $cliente->id;
                // login efetuado
                //header("Location: ".SITE_URL."/comprar/".$parametros->produto_seo."#!");
                header("Location: ".SITE_URL."/comprar/dietwin#!");
            }
            else
            {
                $_SESSION['login_negado'] = true;
                //header("Location: ".SITE_URL."/comprar/".$parametros->produto_seo."#!");
                header("Location: ".SITE_URL."/comprar/dietwin#!");
            }
        }
        else
        {
            $_SESSION['login_erro_campos'] = true;
            //header("Location: ".SITE_URL."/comprar/".$parametros->produto_seo."#!");
            header("Location: ".SITE_URL."/comprar/dietwin#!");
        }

    }

}
