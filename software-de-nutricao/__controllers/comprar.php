<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Comprar extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        $produto = new Model_Produto();
        //adição do prfessional plus que vai substituir o normal
        $busca = $parametros->categoria;
        if($parametros->categoria == "profissional"){
            $busca = "profissional-plus";
        }
        $produto = $produto->select("SELECT * FROM {tabela_nome} WHERE nome_seo='dietwin-".Funcoes::mysqli_escape($busca)."'");
        if (!$produto)
        {
            header('HTTP/1.1 404 Not Found');
            $erro_404 = new Controller_Erro;
            $erro_404->index(404);
            exit;
        }
        $view = new View('comprar.php');
		$this->view_variaveis_obrigatorias($view);

        $view->adicionar('classe',$parametros->categoria);
        $view->adicionar('licenca',$produto->valor);
        $view->adicionar('produto',$produto);

		$view->adicionar('body_class', 'comprar');
		$view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Comprar - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */

    public function cupom($parametros)
    {
        if (isset($parametros->codigo) AND strlen($parametros->codigo) AND
            isset($parametros->categoria) AND strlen($parametros->categoria) AND
            isset($parametros->local) AND $parametros->local == 'software')
        {

            $codigo = Funcoes::mysqli_escape($parametros->codigo);
            $categoria = Funcoes::mysqli_escape($parametros->categoria);
			
			//ROBERTO
			// Verifica se vem o profissional (antigo) e manda o parâmetro como sendo o plus (novo)
			if($parametros->categoria == "profissional"){
            $categoria = "profissional-plus";
       		 }
			
            $produto = new Model_Produto();
            $produto = $produto->select("SELECT * FROM {tabela_nome} WHERE nome_seo='dietwin-".$categoria."'");

            if ($produto)
            {
                //echo $parametros->codigo; exit;
                $cupom = new Model_Cupom();
                $cupom = $cupom->select("SELECT * FROM {tabela_nome}
                        WHERE codigo='".$codigo."' AND produto_id=".$produto->id."
                        AND ativo=1 AND validade_inicio <= CURRENT_DATE AND validade_fim >= CURRENT_DATE");
                if ($cupom)
                {
                    $_SESSION['cupom_id'] = $cupom->id;
                    echo json_encode(array('tipo'=>'sucesso','mensagem'=>'Cupom de desconto é de R$ '.str_replace('.', ',',number_format($cupom->valor,2))));
                    exit;
                }
            }
        }
        echo json_encode(array('tipo' => 'erro', 'mensagem' => 'Cupom não encontrado para esse produto.'));
        exit;
    }

} // end class