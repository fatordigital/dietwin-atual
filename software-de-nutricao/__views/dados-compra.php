<?php include 'includes/cabecalho.php'; ?>

<body class="comprar comprar-<?php echo $classe ?> dados">

<?php include 'includes/topo.php'; ?>
	
    <div class="titulo-principal">
        <p><span>comprar o</span><br />dietWin</p>        
    </div>
    <div class="titulo-resumo">
        <p>	A praticidade de comprar online, com toda a comodidade e segurança que você precisa. Aproveite nossas promoções e adquira hoje mesmo a sua versão do dietWin.</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container">
    	
        <h1>Dados de sua compra</h1>
        
        <table border="0" cellpadding="0" cellspacing="2">
          <tr>
            <td class="cor" width="670"><b>1 <?php echo $_SESSION['produto']->nome ?> </b></td>
              <?php
              $produto_valor = $_SESSION['produto']->valor;
              //$produto_valor = str_replace('.',',',$produto_valor);
              ?>
            <td class="cor" width="170" align="right"><b>R$ <?php echo number_format($produto_valor, 2, ',', '.') ?></b></td>
          </tr>
          <?php /* <tr>
            <td class="cor" width="670"><b><?php echo $_SESSION['licencas']; ?> licenças adicionais</b></td>
              <?php
              $licencas_valor = str_replace(',','',$_SESSION['licencas'])*str_replace(',','',$_SESSION['produto']->licenca_valor);
              //$licencas_valor = str_replace('.',',',$licencas_valor);
              ?>
            <td class="cor" width="170" align="right"><b>R$ <?php echo number_format($licencas_valor, 2, ',', '.') ?></b></td>
          </tr> */ ?>
          <tr>
            <td class="desconto" width="670" align="right"><b>Cupom de desconto utilizado</b></td>
              <?php
              $cupom_valor = (isset($_SESSION['cupom'])? number_format($_SESSION['cupom']->valor,2):'0,00');
              //$cupom_valor = str_replace('.',',',$cupom_valor);
              ?>
            <td class="desconto" width="170" align="right"><b>R$ <?php echo ($cupom_valor == '0,00') ? $cupom_valor : '- '.number_format($cupom_valor, 2, ',', '.') ?></b></td>
          </tr>
          <tr>
            <td width="670" align="right" valign="middle"><h2>Total de sua compra</h2></td>
              <?php
             // $licencas_valor = (float)str_replace(',','',$licencas_valor);
              $produto_valor = (float)str_replace(',','',$produto_valor);
              $cupom_valor = (float)str_replace(',','',$cupom_valor);
              $total_compra = $produto_valor-$cupom_valor;
              //$total_compra = str_replace('.',',',$total_compra);
              $_SESSION['processo_compra'] = TRUE;
              ?>
            <td width="170" align="right"><p class="total-compra"><span>R$</span><?php echo number_format($total_compra, 2, ',', '.') ?></p></td>
          </tr>
          <tr>
            <td width="570" align="right" valign="middle"></td>
            <td width="270" class="valor" align="right"><p><b>Para instalação em <span>1</span> computador.</b><br>Se deseja instalar em mais de 1 computador, <a href="<?php echo SITE_BASE ?>/orcamento" title="Clique aqui para receber uma proposta personalizada">clique aqui</a> e solicite uma proposta personalizada.</p></td>
          </tr>
        </table>
        
        <div class="acesso">
        	<p>Para realizar a sua compra na loja virtual dietWIn, é importante que você seja um usuário cadastrado no site. O processo de cadastro é simples e rápido e garante a segurança na sua compra e na entrega de seus pedidos!</p>
        
            <div class="login">
                <?php
                if (!isset($_SESSION['cliente_id']))
                {
                ?>
                    <h2>Já sou Cadastrado</h2>
                    <form method="post" action="<?php echo SITE_URL ?>/login" id="form-login" >

                        <label><b>E-mail:</b></label>
                        <input type="text" name="email" id="email" maxlength="255" value="" />

                        <label><b>Senha:</b></label>
                        <input type="password" name="senha" id="senha" maxlength="10" value="" />

                        <div class="clear"></div>

                        <a href="<?php echo SITE_URL ?>/esqueci-senha" title="Esqueci minha senha">Esqueci minha senha</a>

                        <button id="form-login-enviar" type="submit">Enviar</button>

                        <div class="clear"></div>

                        <?php if (isset($_SESSION['notificacao'])) { $notificacao = $_SESSION['notificacao']; unset($_SESSION['notificacao']); } ?>
                        <?php if (isset($notificacao))
                    {
                        $notificacao->site_exibir();
                    }
                    else
                    {
                        ?>
                        <div id="form_notification" class=""></div>
                        <?php
                    }
                        ?>

                    </form>
                <?php
                }
                else
                {
                ?>
                    <div class="logado">
                        <h2>Bem-vindo(a), <span><?php echo $cliente->nome  ?></span></h2>
                        <a href="<?php echo SITE_URL.'/software/'.$classe.'/pagamento' ?>" title="Prosseguir com a compra"><img src="<?php echo SITE_BASE ?>/views/imagens/prosseguir-compra.jpg" alt="Prosseguir com a compra" /></a>
                    </div>
                <?php
                }
                ?>
            </div>
            
            <div class="cadastro">
                <h2>Quero me cadastrar</h2>
                    
                <a href="<?php echo SITE_URL ?>/cadastro-cliente" title="Quero me cadastrar gratuitamente na dietWin"><img src="<?php echo SITE_BASE ?>/views/imagens/quero-me-cadastrar.png" alt="Quero me cadastrar gratuitamente na dietWin" /></a>
                
            </div>
            
            <div class="formas-pagamento">
                <img src="<?php echo SITE_BASE ?>/views/imagens/formas-pagamento.jpg" alt="Todas as formas de pagamento" />
            </div>
    	</div>
                
        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>  
</div>

<?php include 'includes/rodape.php'; ?>