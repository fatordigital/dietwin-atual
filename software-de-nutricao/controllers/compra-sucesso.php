<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_CompraSucesso extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
    $view = new View('compra-sucesso.php');
    $this->view_variaveis_obrigatorias($view);

		$view->adicionar('body_class', 'index');
		$view->adicionar('notificacao',new Notificacao());
    	$view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */

    /**
     * Método inicial que faz sumir imagem no include topo
     * @param $parametros
     * @return void
     */
    public function cookie($parametros)
    {
        setcookie('chat', 'true', time()+(3600*24*30), '/');
        //var_dump($_COOKIE['chat']); exit;
    }


} // end class