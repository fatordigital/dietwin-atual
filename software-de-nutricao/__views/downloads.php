<?php include 'includes/cabecalho.php'; ?>

<body class="area-cliente softwares">

<?php include 'includes/topo.php'; ?>

<div class="titulo-principal">
    <p><span>Bem vindo à </span><br />Área do Cliente</p>
</div>
<div class="titulo-resumo">
    <p>Essa área é restrita e de uso exclusivo de usuários cadastrados ou clientes dietWin. Aqui você encontrará informações e arquivos importantes para a utilização do dietWin.</p>
</div>
</div>
</div>

<div class="conteudo">
    <div class="container">

        <?php include 'includes/toolbar.php' ?>
        <br />
        <div class="box">
            <h2>Downloads</h2>
            <p>Abaixo estão listados os arquivos disponíveis para você.</p>

            <?php if ($arquivos AND count($arquivos) > 0) { ?>
            <ul class="atualizacoes">
                <?php foreach ($arquivos as $arquivo) { ?>
                <li>
                    <div class="atualizacoes-titulo">
                        <h2><?php echo $arquivo->titulo; ?></h2>
                        <span>Tamanho: <small><?php echo number_format((filesize('download/'.(!is_null($arquivo->arquivo)?$arquivo->arquivo:$arquivo->link))/1024)/1024,2,'.','') ?>mb</small></span>
                    </div>
                    <p><?php echo $arquivo->descricao; ?>.</p>
                    <a class="atualizacoes-download" href="<?php echo SITE_URL.'/download/'.(!is_null($arquivo->arquivo)?$arquivo->arquivo:$arquivo->link) ?>" title="Download"> </a>

                </li>
                <?php } ?>
            </ul>
            <?php } else { ?>
            <p><strong>Nenhum arquivo disponibilizado até o momento.</strong></p>
            <?php } ?>
        </div>


<?php include 'includes/sidebar-area-cliente.php'; ?>



        <div class="clear"></div>
    </div>
    <div class="inferior"></div>
</div>



<?php include 'includes/rodape.php'; ?>