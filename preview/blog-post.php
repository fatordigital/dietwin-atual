<?php require_once 'includes/head.php' ?>
<body>
<?php require_once 'includes/nav.php' ?>

<section class="blog">
	<div class="container">

		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 detalhes-post">

			<?php if($novidade->imagem!=""){ ?>
			<img src="<?php echo SITE_URL ?>/arquivos/novidades/<?php echo $novidade->imagem; ?>" width="100%" class="img-responsive" alt="">
			<?php }else{ ?>
			<img src="<?php echo SITE_URL ?>/views/imagens/blog/post-1.jpg" class="img-responsive" alt="">
			<?php } ?>

			<div class="clearfix"></div>
			<div class="header">
				<div class="data">
					<?php 
					$data_dia = explode(" ",$novidade->data);
					$data_dia = $data_dia[0];
					$data_dia = explode("-",$data_dia); $data_dia = $data_dia[2]; ?>
					<span class="dia"><?php echo $data_dia; unset($data_dia); ?></span>
					<div class="clearfix"></div>
					<span class="mes"><?php echo Funcoes::blog_data_mes_abreviado($novidade->data); ?></span>
				</div>
				<h3><?php echo $novidade->titulo; ?></h3>
			</div>
			<div class="clearfix"></div>
			
			<?php echo $novidade->conteudo; ?>

			<div class="comentarios">
				<h4>Comentários</h4>
				<div id="disqus_thread"></div>
				<script type="text/javascript">
				    /* * * CONFIGURATION VARIABLES: THIS CODE IS ONLY AN EXAMPLE * * */
				    var disqus_shortname = 'dietwinblog'; // Required - Replace example with your forum shortname
				    var disqus_identifier = '<?php echo SITE_URL."/blog/post/".$novidade->titulo_seo; ?>';

				    /* * * DON'T EDIT BELOW THIS LINE * * */
				    (function() {
				        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
				        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
				        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
				    })();
				</script>
			</div>
		</div>

		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 sidebar" id="!">
			<h4>Blog Dietwin</h4>
			<hr>
			<p>Acompanhe as últimas novidades e dicas que a equipe do Dietwin prepara constamente para seus clientes.</p>
			<p>Se desejar receber uma notificação sempre que um novo post estiver disponível, preencha seu e-mail abaixo:</p>
			
			<?php if(isset($_SESSION['cadastro_blog_sucesso'])){ ?>
			<div class="alert alert-success blog-cadastro-sucesso" role="alert">						
				E-mail cadastrado com sucesso!
			</div>
			<?php unset($_SESSION['cadastro_blog_sucesso']); } ?>
			<form action="<?php echo SITE_URL."/blog/cadastro-email" ?>" method="post" role="form" class="form-inline">
				<div class="form-group">
					<label class="sr-only" for="email">E-mail</label>
					<input type="email" name="email" class="form-control" id="email" placeholder="seu@email" required="required">
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
				</div>
				<button type="submit">Cadastrar</button>
			</form>

			<div class="categorias">
				<h4>Categorias</h4>						
				<ul>
				<?php
				if(isset($categorias) AND is_array($categorias)){ 						
				foreach($categorias as $categoria): ?>	
					<li <?php echo $novidade->categoria_nome==$categoria->tag ? 'class="active"' : '' ?>><a href="<?php echo SITE_URL."/blog/categoria/".$categoria->tag; ?>"><?php echo $categoria->nome; ?> <span class="num"><?php echo $categoria->total_posts; ?></span></a></li>
				<?php 
				endforeach;						 						
				 }else{ 
				 	if(!empty($categorias)){
				 		?>
				 			<li <?php echo $novidade->categoria_nome==$categorias->tag ? 'class="active"' : '' ?>><a href="<?php echo SITE_URL."/blog/categoria/".$categorias->tag; ?>"><?php echo $categorias->nome; ?> <span class="num"><?php echo $categorias->total_posts; ?></span></a></li>
				 		<?php
				 	}else{
				 		echo "<li>Nenhuma categoria cadastrada.</li>";
				 	}
				 ?>							
				<?php } ?>
				</ul>
			</div>
				
			<div class="clear"></div>

			<div class="categorias novidades-recentes">
				<h4>Novidades Recentes</h4>	
				<ul>
				<?php
				if(isset($novidades_recentes) AND is_array($novidades_recentes)){ 						
				foreach($novidades_recentes as $novidades_recentes): ?>	
					<li><a href="<?php echo SITE_URL."/blog/post/".$novidades_recentes->titulo_seo; ?>"><?php echo $novidades_recentes->titulo; ?></a></li>
				<?php 
				endforeach;						 						
				 }else{ 
				 	if(!empty($novidades_recentes)){
				 		?>
				 			<li><a href="<?php echo SITE_URL."/blog/post/".$novidades_recentes->titulo_seo; ?>"><?php echo $novidades_recentes->titulo; ?></span></a></li>
				 		<?php
				 	}else{
				 		echo "<li>Nenhuma post recente.</li>";
				 	}
				 ?>							
				<?php } ?>
				</ul>
			</div>

			<div class="clear"></div>

			<div class="btn-siganos-no-facebook">
				<a href="https://www.facebook.com/dietwin.softwares">
					<img src="<?php echo SITE_URL ?>/views/imagens/blog/btn-siganos-no-facebook.png" class="img-responsive" alt="">
				</a>
			</div>

		</div>
	</div>
	
</section>

<?php require_once 'includes/footer.php' ?>
</body>
</html>