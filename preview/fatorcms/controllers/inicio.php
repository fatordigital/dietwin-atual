<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_Inicio extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai e testa se o usuário está logado
	 */
	public function __construct()
	{
		parent::__construct();
		$this->verificar_usuario_logado();
	}
	
	
	/**
	 * Método inicial que faz a renderização básica da página
	 */
	public function index()
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'inicio.php'));
		$view->adicionar('body_class', 'inicio');
        $view->adicionar('cms_usuario_logado', $this->cms_usuario_logado); //para liberar as opções para edição;
		$view->adicionar('lateral_menu', array('item'=>'inicio','link'=>''));
		$view->adicionar('notificacao', new Notificacao);

		$this->view_adicionar_obrigatorias($view);

		$view->exibir();
	}
} // end class