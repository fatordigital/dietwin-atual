<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Planos extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
    $view = new View('planos.php');
		$this->view_variaveis_obrigatorias($view);

    $produto = new Model_Produto();
    $dietwin_preco = $produto->select("SELECT * FROM {tabela_nome} WHERE nome_seo='dietwin'");
    $dietwin_preco = $dietwin_preco->valor;
    $dietwinplus_preco = $produto->select("SELECT * FROM {tabela_nome} WHERE nome_seo='dietwin-plus'");
    $dietwinplus_preco = $dietwinplus_preco->valor;

    $view->adicionar('dietwin_valor',$dietwin_preco);
    $view->adicionar('dietwinplus_valor',$dietwinplus_preco);

		$view->adicionar('body_class', 'index');
		$view->adicionar('notificacao',new Notificacao());
    $view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}



} // end class