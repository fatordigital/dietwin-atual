<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe

*/

class Controller_Cadastro extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        $view = new View('cadastro.php');

		$this->view_variaveis_obrigatorias($view);

        $estado = new Model_Estado();
        $estados = $estado->select('SELECT * FROM {tabela_nome} ORDER BY id',TRUE);
        $view->adicionar('estados', $estados);

        $cidade = new Model_Cidade();
        $cidades_principal = $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id=1 ORDER BY id',TRUE);
        $view->adicionar('cidades_principal',$cidades_principal);

        $cidade = new Model_Cidade();
        $cidades_entrega = $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id=1 ORDER BY id',TRUE);
        $view->adicionar('cidades_entrega',$cidades_entrega);

		$view->adicionar('body_class', 'cadastro');
		//$view->adicionar('notificacao', new Notificacao('Cadastro desabilitado.'));
        $view->adicionar('notificacao', new Notificacao());
        $view->adicionar('pagina_title', 'Cadastro - dietWin - Softwares de nutrição - Encontre o melhor software para você!o');

		$view->exibir();
	}

    public function cadastrar($parametros)
    {
        /*
	    // Não deixamos fazer o cadastro por enquanto
	    new Notificacao('Cadastro desabilitado.', 'erro', TRUE);
	    header('Location: '.SITE_URL.'/cadastro');
	    exit;*/


        $view = new View('cadastro.php');
        $this->view_variaveis_obrigatorias($view);

        $notificacao = NULL;

        if ($parametros AND isset($parametros->nome))
        {
            $cliente = new Model_Cliente;
            $cliente->carregar($parametros);

            // Campos especiais
            $cliente->email = mb_strtolower($parametros->email);
            $cliente->senha = ( ! empty($parametros->senha) ? sha1($parametros->senha) : NULL);
            $cliente->cpf = ( ! empty($parametros->cpf) ? str_replace(array('.','-'), '', $parametros->cpf) : (!empty($parametros->cpf_responsavel)?str_replace(array('.','-'), '', $parametros->cpf_responsavel):NULL));
            $cliente->cnpj = ( ! empty($parametros->cnpj) ? str_replace(array('.','-','/'), '', $parametros->cnpj) : NULL);
            $cliente->telefone_principal = str_replace(array('(',')',' ','-'), '', $parametros->telefone_principal);
            $cliente->telefone_extra = ( ! empty($parametros->telefone_extra) ? str_replace(array('(',')',' ','-'), '', $parametros->telefone_extra) : NULL);
            $cliente->ativo = 1;
            $cliente->rg = ( ! empty($parametros->rg) ? $parametros->rg : (! empty($parametros->rg_responsavel)?$parametros->rg_responsavel:NULL));
            //-----

            $cliente->colunas_mysqli_escape();

            if ($cliente->verificar_null(array('id', 'cadastro_data', 'tipo', 'cadastro_completo')))
            {

                // Validações ---

                if ( ! filter_var($cliente->email, FILTER_VALIDATE_EMAIL))
                {
                    $notificacao = new Notificacao('O endereço de e-mail <strong>'.$cliente->email.'</strong> não é válido.');
                }

                if ($parametros->usuario == 'pessoa_fisica')
                {
                    //-----

                    if ( ! isset($notificacao) AND ! Funcoes::validar_cpf($cliente->cpf))
                    {
                        $notificacao = new Notificacao('O CPF <strong>'.Funcoes::formatar_cpf($cliente->cpf).'</strong> não é válido.');
                    }
                    $cliente->responsavel_nome = NULL;
                    $cliente->cnpj = NULL;

                }
                elseif ($parametros->usuario == 'pessoa_juridica')
                {
                    //-----
                    //echo 'entrou aki'; exit;
                    if ( ! isset($notificacao) AND ! empty($cliente->cnpj) AND ! Funcoes::validar_cnpj($cliente->cnpj))
                    {
                        $notificacao = new Notificacao('O CNPJ <strong>'.Funcoes::formatar_cnpj($cliente->cnpj).'</strong> não é válido.');
                    }

                    if ( ! isset($notificacao) AND empty($cliente->inscricao_estadual))
                    {
                        $notificacao = new Notificacao('A inscrição estadual <strong>'.Funcoes::formatar_cnpj($cliente->cnpj).'</strong> não é válido.');
                    }


                    if ( ( empty($cliente->cnpj) OR empty($cliente->responsavel_nome) OR empty($cliente->nome_empresa)))
                    {
                        $notificacao = new Notificacao('Ao cadastrar uma empresa, é obrigatório o preenchimento do <strong>Nome da Empresa</strong>, <strong>CNPJ</strong> e do <strong>Responsável na empresa</strong>.');
                    }

                    $cliente->rg = ( ! empty($parametros->rg_responsavel) ? str_replace(array('(',')',' ','-'), '', $parametros->rg_responsavel) : NULL);
                    if ( ( empty($cliente->rg)))
                    {
                        $notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
                    }

                    $cliente->cpf = ( ! empty($parametros->cpf_responsavel) ? str_replace(array('.','-',' '), '', $parametros->cpf_responsavel) : NULL);
                    if ( ( empty($cliente->cpf)))
                    {
                        $notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
                    }


                }
                else
                {
                    header('HTTP/1.1 404 Not Found');
                    $erro_404 = new Controller_Erro;
                    $erro_404->index(404);
                    exit;
                }
                //-----

                // Se o cadastro vier com CNPJ, é empresa e deixamos que o cadastro ocorra mesmo havendo o mesmo CPF em outro cadastro
                // (funcionário tem um cadastro para a empresa onde trabalha e outro cadastro para usar em casa?)
                $cliente_existe = new Model_Cliente;
                $cliente_existe = $cliente_existe->select('
					SELECT id, cpf, cnpj, email
					FROM {tabela_nome}
					WHERE email = "'.$cliente->email.'" OR
							('.(empty($cliente->cnpj) ? 'cpf = '.$cliente->cpf.' AND cnpj IS NULL' : 'cnpj = '.$cliente->cnpj).')
					LIMIT 1'
                );
	            // O LIMIT 1 acima é gambiarra para evitar que venham mais registros por consulta

                if ( ! isset($notificacao) AND $cliente_existe AND ! is_null($cliente_existe->id))
                {
                    if ($cliente->email == $cliente_existe->email)
                    {
                        $notificacao = new Notificacao('O e-mail <strong>'.$cliente->email.'</strong> pertence a outro cliente. Por favor utilize outro endereço de e-mail.');
                    }
                    elseif (empty($cliente->cnpj) AND $cliente->cpf == $cliente_existe->cpf)
                    {
                        $notificacao = new Notificacao('O CPF <strong>'.Funcoes::formatar_cpf($cliente->cpf).'</strong> pertence a outro cliente. Por favor utilize outro CPF.');
                    }
                    else
                    {
                        $notificacao = new Notificacao('O CNPJ <strong>'.Funcoes::formatar_cnpj($cliente->cnpj).'</strong> pertence a outro cliente. Por favor utilize outro CNPJ.');
                    }
                }

	            //-----

	            // Endereço principal

                $endereco_principal = new Model_ClienteEndereco();
                $endereco_principal->entrega = 0;
                $endereco_principal->endereco = (isset($parametros->endereco) AND !empty($parametros->endereco)) ? $parametros->endereco : NULL;
                $endereco_principal->numero = (isset($parametros->numero) AND !empty($parametros->numero)) ? $parametros->numero : NULL;
                $endereco_principal->complemento = (isset($parametros->complemento) AND !empty($parametros->complemento)) ? $parametros->complemento : NULL;
                $endereco_principal->bairro = (isset($parametros->bairro) AND !empty($parametros->bairro)) ? $parametros->bairro : NULL;
                $endereco_principal->cidade_id = (isset($parametros->cidade) AND !empty($parametros->cidade)) ? $parametros->cidade : NULL;
                $endereco_principal->estado_id = (isset($parametros->estado) AND !empty($parametros->estado)) ? $parametros->estado : NULL;
                $endereco_principal->cep = (isset($parametros->cep) AND !empty($parametros->cep)) ? str_replace(array('-','.','_'),'',$parametros->cep) : NULL;
                $endereco_principal->observacao = (isset($parametros->observacao) AND !empty($parametros->observacao)) ? $parametros->observacao : NULL;
                if (!$endereco_principal->verificar_null(array('id','complemento','cliente_id')))
                {
                    $notificacao = new Notificacao('O endereço é obrigatório.');
                }

	            //-----

	            // Endereço de entrega

                if (!empty($parametros->endereco_entrega) OR !empty($parametros->numero_entrega) OR !empty($parametros->complemento_entrega) OR
                !empty($parametros->bairro_entrega) OR !empty($parametros->cep_entrega) OR !empty($parametros->observacao_entrega) OR strlen($parametros->endereco_entrega)>0 OR
                        strlen($parametros->numero_entrega)>0 OR strlen($parametros->complemento_entrega)>0 OR strlen($parametros->bairro_entrega)>0 OR
                            strlen($parametros->cep_entrega)>0 OR strlen($parametros->observacao_entrega)>0)
                {

                    $endereco_entrega = new Model_ClienteEndereco();
                    $endereco_entrega->entrega = 1;
                    $endereco_entrega->endereco = (isset($parametros->endereco_entrega) AND !empty($parametros->endereco_entrega)) ? $parametros->endereco_entrega : NULL;
                    $endereco_entrega->numero = (isset($parametros->numero_entrega) AND !empty($parametros->numero_entrega)) ? $parametros->numero_entrega : NULL;
                    $endereco_entrega->complemento = (isset($parametros->complemento_entrega) AND !empty($parametros->complemento_entrega)) ? $parametros->complemento_entrega : NULL;
                    $endereco_entrega->bairro = (isset($parametros->bairro_entrega) AND !empty($parametros->bairro_entrega)) ? $parametros->bairro_entrega : NULL;
                    $endereco_entrega->cidade_id = (isset($parametros->cidade_entrega) AND !empty($parametros->cidade_entrega)) ? $parametros->cidade_entrega : NULL;
                    $endereco_entrega->estado_id = (isset($parametros->estado_entrega) AND !empty($parametros->estado_entrega)) ? $parametros->estado_entrega : NULL;
                    $endereco_entrega->cep = (isset($parametros->cep_entrega) AND !empty($parametros->cep)) ? str_replace(array('-','.','_'),'',$parametros->cep_entrega) : NULL;
                    $endereco_entrega->observacao = (isset($parametros->observacao_entrega) AND !empty($parametros->observacao_entrega)) ? $parametros->observacao_entrega : NULL;
                    if (!$endereco_entrega->verificar_null(array('id','cliente_id','complemento')))
                    {
                        $notificacao = new Notificacao('Os campos do endereço de entrega devem ser todos preenchidos.');
                    }
                }

                // Fim das validações ---

                if ( ! isset($notificacao))
                {
                    // Data de cadastro do cliente
                    $cliente->cadastro_data = date('d/m/Y H:i:s');
                    $cliente->ultimo_login_data = date('d/m/Y H:i:s');
                    $cliente->tipo = 1;
                    $cliente->colunas_mysqli_escape();
                    //print_r($cliente); exit;
                    if ($cliente->insert())
                    {
                        $endereco_principal->colunas_mysqli_escape();
                        $endereco_principal->cliente_id = $cliente->id;
                        $endereco_principal->insert();

                        if (isset($endereco_entrega))
                        {
                            $endereco_entrega->colunas_mysqli_escape();
                            $endereco_entrega->cliente_id = $cliente->id;
                            $endereco_entrega->insert();
                        }

                        new Notificacao('Cliente <strong>'.$cliente->nome.'</strong> cadastrado com sucesso.', 'success', TRUE);



                        $_SESSION['cliente_id'] = $cliente->id;
                        if (isset($_SESSION['processo_compra']))
                        {
                            $produto = $_SESSION['produto'];
                            header('Location: '.SITE_URL.'/software/'.substr($produto->nome_seo,8).'/pagamento');
	                        exit;
                        }
                        header('Location: '.SITE_URL.'/cadastro/editar');
                        exit;
                    }
                    else
                    {
                        $notificacao = new Notificacao('Ocorreu um erro ao salvar os dados do cliente.');
                    }
                }
                // Deixa passar porque já veio uma notificação
            }
            else
            {
                //echo 'chegou aki'; exit;
                $notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
            }

            // Para retornar ao formulário com valores inseridos
            $view->adicionar('cliente', $cliente);

        }
        else
        {
            $notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
            $view->adicionar('cliente', NULL);
        }

        // Lista de cidades para o endereço que o cliente escolheu
        $cidade = new Model_Cidade;

        if (isset($endereco_principal))
        {
            $view->adicionar('endereco_principal', $endereco_principal);

            // Lista de cidades para o endereço que o cliente escolheu
            $view->adicionar('cidades_principal', $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id = "'.Funcoes::mysqli_escape($endereco_principal->estado_id).'"',TRUE));
            $view->adicionar('endereco_principal', $endereco_principal);
        }
        else
        {

            $view->adicionar('cidades_principal', $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id = 1',TRUE));
        }

        if (isset($endereco_entrega))
        {
            $view->adicionar('endereco_entrega', $endereco_entrega);
            $view->adicionar('cidades_entrega', $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id="'.Funcoes::mysqli_escape($endereco_entrega->estado_id).'"',TRUE));
        }
        else
        {
            $view->adicionar('cidades_entrega', $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id=1',TRUE));
        }

        $view->adicionar('body_class', 'cadastro');
        $view->adicionar('notificacao', $notificacao);

        $estado = new Model_Estado();
        $estados = $estado->select('SELECT * FROM {tabela_nome} ORDER BY nome',TRUE);
        $view->adicionar('estados', $estados);

        $view->adicionar('pagina_title', 'Cadastro - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        $view->exibir();

    }



    public function atualizar($parametros)
    {
        /*
	    // Não deixamos fazer o cadastro por enquanto
	    new Notificacao('Atualização desabilitada.', 'erro', TRUE);
	    header('Location: '.SITE_URL.'/cadastro');
	    exit;*/


        $view = new View('cadastro.php');
        $this->view_variaveis_obrigatorias($view);

        if (!isset($_SESSION['cliente_id']))
        {
            header('HTTP/1.1 404 Not Found');
            $erro_404 = new Controller_Erro;
            $erro_404->index(404);
            exit;
        }

        $notificacao = NULL;
        if ($parametros AND isset($parametros->nome))
        {
            $cliente_auxiliar = new Model_Cliente($_SESSION['cliente_id']);

            $cliente = new Model_Cliente;
            $cliente->carregar($parametros);

            // Campos especiais
            $cliente->email = mb_strtolower($parametros->email);
            $cliente->senha = ( ! empty($parametros->senha) ? sha1($parametros->senha) : $cliente_auxiliar->senha);
            $cliente->cpf = ( ! empty($parametros->cpf) ? str_replace(array('.','-'), '', $parametros->cpf) : (!empty($parametros->cpf_responsavel)?str_replace(array('.','-'), '', $parametros->cpf_responsavel):NULL));
            $cliente->cnpj = ( ! empty($parametros->cnpj) ? str_replace(array('.','-','/'), '', $parametros->cnpj) : NULL);
            $cliente->telefone_principal = str_replace(array('(',')',' ','-'), '', $parametros->telefone_principal);
            $cliente->telefone_extra = ( ! empty($parametros->telefone_extra) ? str_replace(array('(',')',' ','-'), '', $parametros->telefone_extra) : NULL);
            $cliente->ativo = 1;
            $cliente->rg = ( ! empty($parametros->rg) ? $parametros->rg : (! empty($parametros->rg_responsavel)?$parametros->rg_responsavel:NULL));
            $cliente->id = (isset($_SESSION['cliente_id'])?$_SESSION['cliente_id']:NULL);

            //-----

            $cliente->colunas_mysqli_escape();

            if ($cliente->verificar_null(array('cadastro_data', 'tipo', 'cadastro_completo')))
            {
                // Validações ---
                $campos_null = array();

                if (!isset($cliente->telefone_extra) OR !strlen($cliente->telefone_extra)>0 OR empty($cliente->telefone_extra))
                {
                    $campos_null[] = 'telefone_extra';
                }

                if ( ! filter_var($cliente->email, FILTER_VALIDATE_EMAIL))
                {
                    $notificacao = new Notificacao('O endereço de e-mail <strong>'.$cliente->email.'</strong> não é válido.');
                }

                if ($parametros->usuario == 'pessoa_fisica')
                {
                    //-----

                    if ( ! isset($notificacao) AND ! Funcoes::validar_cpf($cliente->cpf))
                    {
                        $notificacao = new Notificacao('O CPF <strong>'.Funcoes::formatar_cpf($cliente->cpf).'</strong> não é válido.');
                    }
                    $cliente->responsavel_nome = NULL;
                    $cliente->cnpj = NULL;

                    $campos_null[] = 'responsavel_nome';
                    $campos_null[] = 'cnpj';
                }
                elseif ($parametros->usuario == 'pessoa_juridica')
                {
                    //-----
                    //echo 'entrou aki'; exit;
                    if ( ! isset($notificacao) AND ! empty($cliente->cnpj) AND ! Funcoes::validar_cnpj($cliente->cnpj))
                    {
                        $notificacao = new Notificacao('O CNPJ <strong>'.Funcoes::formatar_cnpj($cliente->cnpj).'</strong> não é válido.');
                    }

                    if ( ! isset($notificacao) AND empty($cliente->inscricao_estadual))
                    {
                        $notificacao = new Notificacao('A inscrição estadual <strong>'.$cliente->inscricao_estadual.'</strong> não é válida.');
                    }

                    if ( ( empty($cliente->cnpj) OR empty($cliente->responsavel_nome) OR empty($cliente->nome_empresa)))
                    {
                        $notificacao = new Notificacao('Ao cadastrar uma empresa, é obrigatório o preenchimento do <strong>Nome da Empresa</strong>, <strong>CNPJ</strong> e do <strong>Responsável na empresa</strong>.');
                    }

                    $cliente->rg = ( ! empty($parametros->rg_responsavel) ? str_replace(array('(',')',' ','-'), '', $parametros->rg_responsavel) : NULL);
                    if ( ( empty($cliente->rg)))
                    {
                        $notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
                    }

                    $cliente->cpf = ( ! empty($parametros->cpf_responsavel) ? str_replace(array('.','-',' '), '', $parametros->cpf_responsavel) : NULL);
                    if ( ( empty($cliente->cpf)))
                    {
                        $notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
                    }
                }
                else
                {
                    $notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
                }

                //-----

                // Se o cadastro vier com CNPJ, é empresa e deixamos que o cadastro ocorra mesmo havendo o mesmo CPF em outro cadastro
                // (funcionário tem um cadastro para a empresa onde trabalha e outro cadastro para usar em casa?)
                $cliente_existe = new Model_Cliente;
                $cliente_existe = $cliente_existe->select('
					SELECT id, cpf, cnpj, email
					FROM {tabela_nome}
					WHERE email = "'.$cliente->email.'" OR
							('.(empty($cliente->cnpj) ? 'cpf = '.$cliente->cpf.' AND cnpj IS NULL' : 'cnpj = '.$cliente->cnpj).')
						  AND id != '.$cliente->id.'
					LIMIT 1'
                );
	            // O LIMIT 1 acima é gambiarra para evitar que venham mais registros por consulta

                if ( ! isset($notificacao) AND $cliente_existe AND ! is_null($cliente_existe->id))
                {
                    if (($cliente->email == $cliente_existe->email) AND ($cliente->id != $cliente_existe->id))
                    {
                        $notificacao = new Notificacao('O e-mail <strong>'.$cliente->email.'</strong> pertence a outro cliente. Por favor utilize outro endereço de e-mail.');
                    }
                    elseif (empty($cliente->cnpj) AND ($cliente->cpf == $cliente_existe->cpf) AND ($cliente->id != $cliente_existe->id))
                    {
                        $notificacao = new Notificacao('O CPF <strong>'.Funcoes::formatar_cpf($cliente->cpf).'</strong> pertence a outro cliente. Por favor utilize outro CPF.');
                    }
                    elseif (!empty($cliente->cnpj) AND ($cliente->cnpj == $cliente_existe->cnpj) AND ($cliente->id != $cliente_existe->id))
                    {
                        $notificacao = new Notificacao('O CNPJ <strong>'.Funcoes::formatar_cnpj($cliente->cnpj).'</strong> pertence a outro cliente. Por favor utilize outro CNPJ.');
                    }
                }

	            //-----

	            // Endereço principal

                $endereco_principal = new Model_ClienteEndereco();
                $endereco_principal = $endereco_principal->select('SELECT * FROM {tabela_nome} WHERE entrega=0 AND cliente_id='.$_SESSION['cliente_id']);

	            $endereco_principal->entrega = 0;
                $endereco_principal->endereco = (isset($parametros->endereco) AND !empty($parametros->endereco)) ? $parametros->endereco : NULL;
                $endereco_principal->numero = (isset($parametros->numero) AND !empty($parametros->numero)) ? $parametros->numero : NULL;
                $endereco_principal->complemento = (isset($parametros->complemento) AND !empty($parametros->complemento)) ? $parametros->complemento : NULL;
                $endereco_principal->bairro = (isset($parametros->bairro) AND !empty($parametros->bairro)) ? $parametros->bairro : NULL;
                $endereco_principal->cidade_id = (isset($parametros->cidade) AND !empty($parametros->cidade)) ? $parametros->cidade : NULL;
                $endereco_principal->estado_id = (isset($parametros->estado) AND !empty($parametros->estado)) ? $parametros->estado : NULL;
                $endereco_principal->cep = (isset($parametros->cep) AND !empty($parametros->cep)) ? str_replace(array('-','.','_'),'',$parametros->cep) : NULL;
                $endereco_principal->observacao = (isset($parametros->observacao) AND !empty($parametros->observacao)) ? $parametros->observacao : NULL;
                $endereco_principal->cliente_id = $_SESSION['cliente_id'];
                if (!$endereco_principal->verificar_null())
                {
                    $notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
                }

	            //-----

	            // Endereço de entrega
                if (isset($parametros->endereco_para_entrega) AND $parametros->endereco_para_entrega == '1')
                {
                    if (!empty($parametros->endereco_entrega) OR !empty($parametros->numero_entrega) OR !empty($parametros->complemento_entrega) OR
                        !empty($parametros->bairro_entrega) OR !empty($parametros->cep_entrega) OR !empty($parametros->observacao_entrega) OR strlen($parametros->endereco_entrega)>0 OR
                        strlen($parametros->numero_entrega)>0 OR strlen($parametros->complemento_entrega)>0 OR strlen($parametros->bairro_entrega)>0 OR
                        strlen($parametros->cep_entrega)>0 OR strlen($parametros->observacao_entrega)>0)
                    {
                        $endereco_entrega = new Model_ClienteEndereco();
                        $endereco_entrega = $endereco_entrega->select('SELECT * FROM {tabela_nome} WHERE entrega=1 AND cliente_id='.$_SESSION['cliente_id']);
                        $campos = array();
                        if (!isset($endereco_entrega->id))
                        {
                            $endereco_entrega = new Model_ClienteEndereco();
                            $campos = array('id');
                        }
                        $endereco_entrega->entrega = 1;
                        $endereco_entrega->endereco = (isset($parametros->endereco_entrega) AND !empty($parametros->endereco_entrega)) ? $parametros->endereco_entrega : NULL;
                        $endereco_entrega->numero = (isset($parametros->numero_entrega) AND !empty($parametros->numero_entrega)) ? $parametros->numero_entrega : NULL;
                        $endereco_entrega->complemento = (isset($parametros->complemento_entrega) AND !empty($parametros->complemento_entrega)) ? $parametros->complemento_entrega : NULL;
                        $endereco_entrega->bairro = (isset($parametros->bairro_entrega) AND !empty($parametros->bairro_entrega)) ? $parametros->bairro_entrega : NULL;
                        $endereco_entrega->cidade_id = (isset($parametros->cidade_entrega) AND !empty($parametros->cidade_entrega)) ? $parametros->cidade_entrega : NULL;
                        $endereco_entrega->estado_id = (isset($parametros->estado_entrega) AND !empty($parametros->estado_entrega)) ? $parametros->estado_entrega : NULL;
                        $endereco_entrega->cep = (isset($parametros->cep_entrega) AND !empty($parametros->cep)) ? str_replace(array('-','.','_'),'',$parametros->cep_entrega) : NULL;
                        $endereco_entrega->observacao = (isset($parametros->observacao_entrega) AND !empty($parametros->observacao_entrega)) ? $parametros->observacao_entrega : NULL;
                        $endereco_entrega->cliente_id = $_SESSION['cliente_id'];
                        if (!$endereco_entrega->verificar_null($campos))
                        {
                            $notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
                        }
                    }
                }
                else
                {
                    $apagar_entrega = TRUE;
                }

                // Fim das validações ---

                if ( ! isset($notificacao))
                {
                    $cliente->colunas_mysqli_escape();
                    if ($cliente->update($campos_null))
                    {
                        $endereco_principal->colunas_mysqli_escape();
                        $endereco_principal->update((isset($endereco_principal->complemento)?array():array('complemento')));

	                    if (isset($parametros->endereco_para_entrega) AND $parametros->endereco_para_entrega == '1')
	                    {
		                    // Se veio o valor 1 informando que há um endereço de entrega, continua normalmente
	                        if (isset($endereco_entrega))
	                        {
	                            $endereco_entrega->colunas_mysqli_escape();
	                            if (isset($endereco_entrega->id))
	                            {
	                                $endereco_entrega->update((isset($endereco_entrega->complemento)?array():array('complemento')));
	                            }
	                            else
	                            {
	                                $endereco_entrega->insert();
	                            }
	                        }
	                    }
	                    else
	                    {
		                    // Se não vier o campo, excluímos o endereço de entrega se houver

                            $endereco_entrega = new Model_ClienteEndereco();
                            $endereco_entrega = $endereco_entrega->select('SELECT * FROM {tabela_nome} WHERE entrega=1 AND cliente_id='.$cliente->id);
                            if ($endereco_entrega)
                            {
                                $endereco_entrega->delete();
                            }

	                    }

                        new Notificacao('Cliente <strong>'.$cliente->nome.'</strong> atualizado com sucesso.', 'success', TRUE);
                        $_SESSION['cliente_id'] = $cliente->id;
                        if (isset($_SESSION['processo_compra']))
                        {
                            $produto = $_SESSION['produto'];
                            header('Location: '.SITE_URL.'/software/'.substr($produto->nome_seo,8).'/pagamento');
	                        exit;
                        }
                        header('Location: '.SITE_URL.'/cadastro/editar');
                        exit;
                    }
                    else
                    {
                        $notificacao = new Notificacao('Ocorreu um erro ao salvar os dados do cliente.');
                    }
                }
                // Deixa passar porque já veio uma notificação
            }
            else
            {
                //echo 'chegou aki'; exit;
                $notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
            }

            // Para retornar ao formulário com valores inseridos
            $view->adicionar('cliente', $cliente);

	        if (isset($endereco_principal))
	        {
		        // Se conseguiu carregar pelo envio do formulário, manda de volta
	            $view->adicionar('endereco_principal', $endereco_principal);
	        }
	        else
	        {
		        // Se não, carregamos o endereço anterior
		        $endereco_principal = new Model_ClienteEndereco();
		        $endereco_principal = $endereco_principal->select('SELECT * FROM {tabela_nome} WHERE cliente_id='.$_SESSION['cliente_id'].' AND entrega=0');
		        $view->adicionar('endereco_principal', $endereco_principal);
	        }

	        // Lista de cidades para o endereço que o cliente escolheu
	        $cidade = new Model_Cidade;
	        $view->adicionar('cidades_principal', $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id = "'.Funcoes::mysqli_escape($endereco_principal->estado_id).'"',TRUE));

	        if (isset($endereco_entrega))
	        {
		        // Se conseguiu carregar pelo envio do formulário, manda de volta
		        $view->adicionar('endereco_entrega', $endereco_entrega);
		        $view->adicionar('cidades_entrega', $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id="'.Funcoes::mysqli_escape($endereco_entrega->estado_id).'"',TRUE));
	        }
	        else
	        {
		        // Se não, carregamos o endereço anterior, se houver
		        $endereco_entrega = new Model_ClienteEndereco();
		        $endereco_entrega = $endereco_entrega->select('SELECT * FROM {tabela_nome} WHERE cliente_id='.$_SESSION['cliente_id'].' AND entrega=1');
		        $view->adicionar('endereco_enterga', $endereco_entrega);

		        $cidades_entrega = NULL;
	        }

	        // Lista de cidades para o endereço que o cliente escolheu
	        $cidade = new Model_Cidade;
	        $view->adicionar('cidades_entrega', $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id = "'.Funcoes::mysqli_escape($endereco_entrega->estado_id).'"',TRUE));
        }
        else
        {
            $notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
            $view->adicionar('cliente', NULL);
        }

        $view->adicionar('body_class', 'cadastro');
        $view->adicionar('notificacao', $notificacao);

        $estado = new Model_Estado();
        $estados = $estado->select('SELECT * FROM {tabela_nome} ORDER BY id',TRUE);
        $view->adicionar('estados', $estados);

        $view->adicionar('pagina_title', 'Cadastro - dietWin - Softwares de Nutrição');

        $view->exibir();

    }


    public function editar($parametros)
    {
        /*
	    // Não deixamos fazer o cadastro por enquanto
	    new Notificacao('Edição desabilitada.', 'erro', TRUE);
	    header('Location: '.SITE_URL.'/cadastro');
	    exit;*/

        $view = new View('cadastro.php');
        $this->view_variaveis_obrigatorias($view);
        if (isset($_SESSION['cliente_id']))
        {
            $cliente = new Model_Cliente($_SESSION['cliente_id']);

	        $endereco_principal = new Model_ClienteEndereco();
            $endereco_principal = $endereco_principal->select('SELECT * FROM {tabela_nome} WHERE cliente_id='.$_SESSION['cliente_id'].' AND entrega=0');

	        $endereco_entrega = new Model_ClienteEndereco();
            $endereco_entrega = $endereco_entrega->select('SELECT * FROM {tabela_nome} WHERE cliente_id='.$_SESSION['cliente_id'].' AND entrega=1');

	        $cidade = new Model_Cidade();
            $cidades_principal = $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id='.$endereco_principal->estado_id,TRUE);

            $cidades_entrega = NULL;
            if (isset($endereco_entrega) AND $endereco_entrega)
            {
                $cidades_entrega = $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id='.$endereco_entrega->estado_id,TRUE);
            }
            else
            {
                $cidades_entrega = $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id=1',TRUE);
            }
        }
        else
        {
            header('HTTP/1.1 404 Not Found');
            $erro_404 = new Controller_Erro;
            $erro_404->index(404);
            exit;
        }

        $estado = new Model_Estado();
        $estados = $estado->select('SELECT * FROM {tabela_nome} ORDER BY id',TRUE);
        $view->adicionar('estados', $estados);

        $view->adicionar('cliente',$cliente);
        $view->adicionar('endereco_principal',$endereco_principal);
        $view->adicionar('endereco_entrega',$endereco_entrega);
        $view->adicionar('cidades_principal',$cidades_principal);
        $view->adicionar('cidades_entrega',$cidades_entrega);

        $view->adicionar('body_class', 'cadastro');
        $view->adicionar('notificacao', new Notificacao());

        $view->adicionar('pagina_title', 'Cadastro - dietWin - Softwares de Nutrição');

        $view->exibir();
    }


	/* ***************************** MÉTODOS EXTRAS ***************************** */


    /**
     * Passa as informações para a montagem e envio do email a partir do formulário "Cadastro" do site
     * @param $parametros
     * @return void
     */
    public function enviar($parametros)
    {
        $nome = isset($parametros->nome) ? trim($parametros->nome) : NULL;
        $telefone = isset($parametros->telefone) ? trim($parametros->telefone) : NULL;
        $email = isset($parametros->email) ? trim($parametros->email) : NULL;
        $cidade = isset($parametros->cidade) ? trim($parametros->cidade) : NULL;
        $usuario = isset($parametros->usuario) ? trim($parametros->usuario) : NULL;
        $assunto = isset($parametros->assunto) ? trim($parametros->assunto) : NULL;
        $mensagem = isset($parametros->mensagem) ? trim($parametros->mensagem) : NULL;

        if ( ! empty($nome) AND ! empty($email) AND ! empty($mensagem) AND ! empty($telefone) AND ! empty($assunto) AND ! empty($cidade))
        {
            $mail = new Controller_Email;

            if ($mail->contato($parametros))
            {
                echo json_encode(array('tipo'=>'sucesso', 'mensagem'=>'Sua mensagem foi enviada com sucesso!'));
            }
            else
            {
                echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Ocorreu um erro ao enviar sua mensagem.'));
            }
        }
        else
        {
            echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Ocorreu um erro ao receber suas informações.'));
        }
    }


    public function cidades_estado($parametros)
    {
        if (isset($parametros->estado) AND is_numeric($parametros->estado))
        {
            $cidade = new Model_Cidade();
            $cidades = $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id='.Funcoes::mysqli_escape($parametros->estado),TRUE);
            $array_cidades = array();
            foreach ($cidades as $obj)
            {
                $aux = array();
                $aux['id'] = $obj->id;
                $aux['nome'] = $obj->nome;
                $array_cidades[] = $aux;
            }
            echo json_encode($array_cidades); exit;
        }
        echo json_encode(array('tipo'=>'erro')); exit;
    }

} // end class