<?php include 'includes/cabecalho.php'; ?>

<??>
<body class="index">
<?php include 'includes/topo.php'; ?>
    <div class="titulo-principal">
        <span>O mais completo da família<br />dietWin acaba de chegar.</span>
        <div class="clear"></div>
        <p style="color:#FFF;"><strong>Profissional</strong> PLUS</p>
    </div>
    </div>
</div>
<div class="conteudo">
	<div class="container">        
        <div class="familia-dietwin">
        	<h1><span>Família <span class="nome">dietWin</span> - Encontre o <strong>software de nutrição</strong> ideal para você.</span></h1>
            <ul>
            	<li class="professional">
                	<h3>Profissional <span style="font-weight:100 !important; color:#FFF !important;">PLUS</span> </h3>
                    <p>O objetivo principal do dietWin Profissional Plus é AUXILIAR O NUTRICIONISTA NA TOMADA DE DECISÃO, monitorando informações através do relacionamento de resultados obtidos para análise das avaliações executadas. Além disso, gerar subsídios para facilitar o diagnóstico nutricional, obviamente sem ser... </p>
                    <a href="<?php echo SITE_URL ?>/profissional" class="cufon" title="Conheça o Software Profissional " style="margin-right:27px;"><span>CONHEÇA O</span> PROFISSIONAL PLUS </a>
                </li>
                <li class="personal">
                	<h3>Personal </h3>
                    <p>É uma versão que atende todas as necessidades básicas da avaliação nutricional e prescrição dietética para todos os ciclos da vida. Um novo lay-out de telas é apresentado para uma nova proposta de atendimento clínico diferenciado onde a evolução da consulta fica disponível durante todo o atendimento. Permite a abertura de múltiplas fichas de pacientes e pesquisas facilitadas. ...</p>
                    <a href="<?php echo SITE_URL ?>/personal" class="cufon" title="Conheça o Software Personal "><span>CONHEÇA O</span> PERSONAL </a>
                </li>
                <li class="rotulo-alimentos">
                	<h3>Rótulo de Alimentos</h3>
                    <p>Este programa tem a finalidade principal de calcular as Informações Nutricionais que devem estar presentes nos rótulos dos alimentos embalados de acordo as regras estabelecidas e as exigência Brasileiras determinadas pela ANVISA Resolução - RDC nº 360, de 23 de dezembro de 2003 - Aprova Regulamento Técnico sobre Rotulagem ... </p>
                    <a href="<?php echo SITE_URL ?>/rotulo-de-alimentos" class="cufon" title="Conheça o Software Rótulo de Alimentos" style="margin-right:27px;"><span>CONHEÇA O</span> RÓTULO DE ALIMENTOS</a>
                </li>
                <li class="descubra-versao">
                	<h3>Qual o melhor <strong>software de nutrição</strong> pra mim?</h3>
                    <p>A Família dietWin é composta por 3 softwares de nutrição com funcionalidades e recursos específicos.</p>
                    <a href="<?php echo SITE_URL ?>/qual-versao-escolho" class="cufon" title="Descubra qual a melhor ferramenta para atender às suas necessidades">ENCONTRE O SOFTWARE DE NUTRIÇÃO IDEAL.</a>
                </li>
            </ul>
            <div class="clear"></div>
        </div>
        
        <div class="clear"></div>
        
        <div class="novidades">
        	<h2><span>Acompanhe as</span><br /> Novidades</h2>
            <?php
            if ($novidades AND count($novidades) > 0)
            {
            ?>
            <ul>
                <?php
                foreach ($novidades as $novidade)
                {
                ?>
                    <li>
                        <p class="data"><?php echo substr($novidade->data,8,2).' de '.Funcoes::mes_nome(substr($novidade->data,6,2)) ?></p>
                        <p class="titulo"><b><a href="<?php echo SITE_URL.'/novidade/'.$novidade->titulo_seo ?>" title="<?php echo $novidade->titulo ?>"><?php echo mb_strtolower($novidade->titulo) ?></a></b></p>
                        <p><?php echo Funcoes::cortar_texto($novidade->resumo, 300) ?></p>
                        <a href="<?php echo SITE_URL.'/novidade/'.$novidade->titulo_seo ?>" class="link" title="<?php echo $novidade->titulo ?>">Leia mais &#187;</a>
                    </li>
                <?php
                }
                ?>
            </ul>
            <?php
            }
            else
            {
                echo '<p>Não há novidades cadastradas.</p>';
            }
            ?>
            <div class="clear"></div>
    	</div>
        <div class="treinamento">
        	<h2><span>Multimídia & </span><br /> Treinamento</h2>
            <?php
            if ($videos AND count($videos) > 0)
            {
            ?>
            <ul>
                <?php
                foreach ($videos as $video)
                {
                ?>
                    <li>
                        <?php
                        $regex = '/http\:\/\/www\.youtube\.com\/watch\?v=(\w{11})/';
                        preg_match($regex, $video->link,$matches);

                        ?>
                        <div class="play"><a href="<?php echo SITE_URL ?>/suporte/<?php echo (isset($video->nome_seo) ? substr($video->nome_seo,8) : 'topicos-diversos') ?>/video/<?php echo $video->titulo_seo ?>" title="<?php echo $video->titulo ?>"><img src="<?php echo SITE_BASE ?>/views/imagens/multimidia-treinamento-play.png" alt="Profissional  - Novo Paciente e Avaliação Nutricional Adulto/Atleta" /></a></div>
                        <a href="<?php echo SITE_URL ?>/suporte/<?php echo (isset($video->nome_seo) ? substr($video->nome_seo,8) : 'topicos-diversos') ?>/video/<?php echo $video->titulo_seo ?>" title="<?php echo $video->titulo ?>"><img src="<?php echo SITE_BASE ?>/biblioteca/timthumb.php?src=http://i3.ytimg.com/vi/<?php echo substr($video->link, strrpos($video->link,'v=')+2,11) ?>/0.jpg&w=200&h=109" alt="<?php echo $video->titulo ?>" /></a>
                        <p class="titulo"><b><a href="<?php echo SITE_URL ?>/suporte/<?php echo (isset($video->nome_seo) ? substr($video->nome_seo,8) : 'topicos-diversos') ?>/video/<?php echo $video->titulo_seo ?>" title="<?php echo $video->titulo ?>"><?php echo $video->titulo ?></a></b></p>
                        <p><?php echo Funcoes::cortar_texto($video->resumo,90) ?></p>
                    </li>
                <?php
                }
                ?>
    		</ul>
            <?php
            }
            else
            {
                echo '<p>Não há vídeos cadastrados.</p>';
            }
            ?>
            <div class="clear"></div>
    	</div>
        <div class="faq">
        	<h2><span>  </span><br /> FAQ</h2>
            <?php if ($faqs AND count($faqs) > 0) { ?>
            <ul>
                <?php foreach ($faqs as $faq) { ?>
                <li>
                    <p><b><a href="<?php echo SITE_URL.'/suporte/'.(isset($faq->nome_seo)?substr($faq->nome_seo,8):'topicos-diversos') ?>/faq/<?php echo $faq->pergunta_seo ?>" title="<?php echo $faq->pergunta ?>"><?php echo mb_strtolower($faq->pergunta) ?></a></b></p>
                </li>
                <?php } ?>
    		</ul>
            <?php
            }
            else
            {
                echo '<p>Não há perguntas cadastradas.</p>';
            }
            ?>
            <div class="clear"></div>
    	</div>
        <div class="clear"></div>
        <div class="inferior"></div>
    </div>    
</div>

<?php include 'includes/rodape.php'; ?>