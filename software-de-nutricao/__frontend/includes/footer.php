<footer>
	<div class="conteudo">
		<div class="container">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="laranja">
					<div class="row">
						<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-left">
							<h2>Rótulo de Alimentos</h2>
						</div>
						<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-left">
							<p>Conheça o primeiro software para cálculo nutricional doo Brasil. Desenvolvimento por nutricionistas, ele calcula a informação nutricional, conforme as Resoluções 359 e 360 da ANVISA. </p>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
							<div class="outros">
								Ideal para profissionais que realizam consultoria nutricional.
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="azul">
					<h2>Suporte especializado</h2>
					<p>Está com alguma dificuldade em trabalhar com um software da família Dietwin? Fique tranquilo que iremos ajudá-lo. Utilize um dos canais abaixo para fazer contato.  </p>
					<div class="telefone-footer">
						<span>(51)</span> 3337.7908
					</div>
					<a href="mailto:suporte@dietwin.com.br" title="Envie um e-mail para suporte@dietwin.com.br" class="link-suporte">suporte@dietwin.com.br</a>
				</div>
			</div>
		</div>
	</div>
	<section class="logo">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				<img src="<?php echo $baseurl ?>/imagens/dietwin-logo-footer-2.png" alt="dietWin - Software de Nutrição">
			</div>
		</div>
	</section>
</footer>

<link href='http://fonts.googleapis.com/css?family=Lato:400,700,900,300' rel='stylesheet' type='text/css'>
<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="<?php echo $baseurl ?>/js/plugins.js"></script>
<script src="<?php echo $baseurl ?>/js/main.js"></script>