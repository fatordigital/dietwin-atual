<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>FatorCMS - dietWin</title>

	<link rel="shortcut icon" href="<?php echo SITE_BASE ?>/favicon.ico" type="image/x-icon"/>

	<!--                       CSS                       -->

	<!-- Reset Stylesheet -->
	<link rel="stylesheet" href="<?php echo SITE_BASE ?>/fatorcms/views/css/reset.css" type="text/css" media="screen" />

	<!-- Main Stylesheet -->
	<link rel="stylesheet" href="<?php echo SITE_BASE ?>/fatorcms/views/css/style.css" type="text/css" media="screen" />

	<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
	<link rel="stylesheet" href="<?php echo SITE_BASE ?>/fatorcms/views/css/invalid.css" type="text/css" media="screen" />

	<link rel="stylesheet" href="<?php echo SITE_BASE ?>/fatorcms/views/css/cupertino/jquery-ui-1.8.18.custom.css" type="text/css" media="screen" />

	<link rel="stylesheet" href="<?php echo SITE_BASE ?>/fatorcms/views/js/autoSuggest/autoSuggest.css" type="text/css" media="screen" />

	<!--                       Javascripts                       -->
	
	<script type="text/javascript">
		var SITE_BASE = '<?php echo SITE_BASE ?>';
		var SITE_URL = '<?php echo SITE_URL ?>';
	</script>

	<!-- jQuery -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
    <!--<script type="text/javascript" src="<?php /*echo SITE_BASE */?>/fatorcms/views/js/jquery.js"></script>
	<script type="text/javascript" src="<?php /*echo SITE_BASE */?>/fatorcms/views/js/jquery.ui.datepicker.min.js"></script>-->

	<!-- jQuery Configuration -->
	<script type="text/javascript" src="<?php echo SITE_BASE ?>/fatorcms/views/js/simpla.jquery.configuration.js"></script>

	<!-- Facebox jQuery Plugin -->
	<script type="text/javascript" src="<?php echo SITE_BASE  ?>/fatorcms/views/js/facebox.js"></script>

	<!-- jQuery Validate Plugin -->
	<script type="text/javascript" src="<?php echo SITE_BASE ?>/fatorcms/views/js/validate/jquery.validate.min.js"></script>
	<!-- jQuery Masked Input Plugin -->
	<script type="text/javascript" src="<?php echo SITE_BASE ?>/fatorcms/views/js/jquery.maskedinput-1.3.min.js"></script>
	<!-- jQuery Price Format Plugin -->
	<script type="text/javascript" src="<?php echo SITE_BASE ?>/fatorcms/views/js/jquery.price_format.1.7.min.js"></script>

	<!-- jQuery autoSuggest Plugin -->
    <?php if (strpos($body_class,'cupons') === false AND strpos($body_class,'arquivos') === false) { ?>
	<script type="text/javascript" src="<?php echo SITE_BASE ?>/fatorcms/views/js/autoSuggest/jquery.autoSuggest.packed.js"></script>
    <?php } else { ?>
    <script type="text/javascript" src="<?php echo SITE_BASE ?>/fatorcms/views/js/autoSuggest/jquery.autoSuggest-modificado.js"></script>
    <?php } ?>

    <!-- TinyMCE -->
    <script type="text/javascript" src="<?php echo SITE_BASE ?>/fatorcms/views/js/tiny_mce/tiny_mce.js"></script>
	<!-- Funções javascript do sistema -->
	<script type="text/javascript" src="<?php echo SITE_BASE ?>/fatorcms/views/js/funcoes.js"></script>

	<!-- Javascripts extras  -->
	<?php if (isset($head_script) AND ! empty($head_script)) { ?>
	<script type="text/javascript"><?php echo $head_script ?></script>
	<?php } ?>

</head>

<body class="<?php echo isset($body_class) ? $body_class : '' ?>">

	<div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->