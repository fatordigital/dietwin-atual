<?php include 'includes/cabecalho.php'; ?>

<body class="suporte profissional softwares">

<?php include 'includes/topo.php'; ?>

    <div class="titulo-principal">
        <p><span>Suporte</span><br />DietWin</p>
    </div>
    <div class="titulo-resumo">
        <p>Entre em contato conosco para tirar todas suas dúvidas e obter o suporte necessário. Confira os detalhes abaixo:</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container">

        <?php include 'includes/toolbar.php' ?>


        <div class="box" id="tamanho-fonte">
        	<?php $versao = $_GET["versao"];
            if($versao == 'V2945'){ ?>
                <img src="<?php echo SITE_BASE?>/views/imagens/software_atualizado.jpg" style="margin:20px 0;" alt="Sua versão do dietWin está atualizada" />
            
            <?php }elseif ($versao == ""){ ?>

            
            <?php }else{ ?>
                <img src="<?php echo SITE_BASE?>/views/imagens/software_desatualizado.jpg" style="margin:20px 0;" alt="Sua versão do dietWin está desatualizada" />
            <?php } ?>
            <br/>
            <h2>SUPORTE</h2>

            <p>Por mais eficiente que seja as ferramentas <strong>dietWin</strong>, consideramos normal surgirem dúvidas iniciais até o conhecimento mais detalhado sobre o sistema. Para suprir esse atendimento o dietWin conta com uma forte estrutura de Suporte. Uma equipe plenamente qualificada para atender e suprir as suas necessidades em todas as etapas da instalação e orientação de uso.

            </p>

            <h3>HORÁRIO DE ATENDIMENTO ÚTIL:</h3>
            <p>De segunda a sexta-feira das 9 às 12hs e das 13 às 17hs. (HORARIO DE BRASILIA)</p>

            <h3>Como utilizar o suporte dietWin?</h3>
            <p>Sua atenção para:</p>
            <ul class="detalhes">
                <li>Certifique-se que sua dúvida não consta dos textos da AJUDA ou dos vídeos demonstração.</li>
                <li>O suporte e orientações serão fornecidos somente ao usuário titular.</li>
                <li>As despesas de correio e telefone são responsabilidade do usuário.</li>
                <li>Mantenha seu endereço e seu e-mail e número de telefone <u>sempre</u> atualizados.</li>
                <li>Formule suas perguntas com OBJETIVIDADE numerando-as preferencialmente.</li>
                <li>Copie a tela onde existe o erro ou dúvidas, para alguma ferramenta de editoração (ex.: Word) e aponte as suas observações por escrito, demonstrando um passo a passo imprimindo a maior clareza possível de sua localização.</li>
                <li>Reenvie o e-mail caso você não obtenha uma resposta em 48 horas úteis.</li>
            </ul>
           
            <h3>Use nossos canais de atendimento</h3>
            <p>
                <strong>E-mail:</strong> <a href="mailto:atendimento@dietwin.com.br">atendimento@dietwin.com.br</a>
            </p>
            <p>
                <strong>Facebook:</strong> <a href="http://www.facebook.com/dietwin.softwares" target="_blank">http://www.facebook.com/dietwin.softwares</a>
            </p>
            <p>
                <strong>Fone: </strong>(51) 3337-7908
            </p>
            <p>
                <strong>Acesso Remoto:</strong> <a href="http://get.teamviewer.com/vjms4ag" target="_blank">http://get.teamviewer.com/vjms4ag</a> <img src="<?php echo SITE_BASE ?>/views/imagens/teamviewer.png" alt="Teamviewer Logo" class="teamviewer">
            </p>

            <p>Para nós do <b>dietWin</b>, a sua satisfação é nosso principal objetivo.
                Suas sugestões serão sempre bem vindas.
                Não deixe de registrar suas observações e esclarecer as dúvidas que não encontrar em nossos manuais, ajuda ou vídeos.
            </p>

        </div>

        <div class="sidebar">
            <div class="filtro">
                <h2>Filtre os tópicos</h2>
                <p>Clique para marcar apenas as categorias de tópicos que você deseja visualizar.</p>
                <a href="<?php echo SITE_URL.'/suporte/personal/faq'; ?>" class="filtro-personal<?php if ($categoria == 'Personal') { echo ' ativo'; } ?>" title="Filtrar por Personal "><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-personal<?php if ($categoria == 'Personal') { echo '-ativo'; } ?>.png" alt="Filtrar por Personal " /></a>
                <a href="<?php echo SITE_URL.'/suporte/profissional/faq'; ?>" class="filtro-profissional<?php if ($categoria == 'Profissional') { echo ' ativo'; } ?>" title="Filtrar por Profissional "><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-profissional<?php if ($categoria == 'Profissional') { echo '-ativo'; } ?>.png" alt="Filtrar por Profissional " /></a>
                <a href="<?php echo SITE_URL.'/suporte/rotulo-de-alimentos/faq'; ?>" class="filtro-rotulo-alimentos<?php if ($categoria == 'Rótulo de Alimentos') { echo ' ativo'; } ?>" title="Filtrar por Rótulo de Alimentos"><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-rotulo<?php if ($categoria == 'Rótulo de Alimentos') { echo '-ativo'; } ?>.png" alt="Filtrar por Rótulo de Alimentos" /></a>
                <a href="<?php echo SITE_URL.'/suporte/topicos-diversos/faq'; ?>" class="filtro-topicos-diversos<?php if ($categoria == 'Tópicos Diversos') { echo ' ativo'; } ?>" title="Filtrar por Tópicos diversos"><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-topicos<?php if ($categoria == 'Tópicos Diversos') { echo '-ativo'; } ?>.png" alt="Filtrar por Tópicos diversos" /></a>
            </div>
            <?php
            if ($faqs AND count($faqs) > 0)
            {
                ?>
                <div class="filtro">
                    <h2>Busque por palavra-chave</h2>
                    <form method="post" action="<?php echo ($categoria != 'Todas') ? SITE_URL.'/suporte/'.$classe : SITE_URL ?>/faq" id="buscar" >
                        <input type="text" name="buscar" id="busca-campo" maxlength="255" value="" />
                        <button type="submit">Buscar</button>
                    </form>
                    <div class="clear"></div>
                </div>
                <?php
            }
            ?>
        </div>

        <div class="clear"></div>
    </div>
    <div class="inferior"></div>
</div>

<?php include 'includes/rodape.php'; ?>