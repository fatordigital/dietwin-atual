<?php require_once 'includes/head.php' ?>
<body>
  <style type="text/css">
    .overlay{
      width: 100%;
      height: 100%;
      display: block;
      position: absolute;
      left: 0%;
      top: 0%;
      background-color: rgba(0,0,0,0.7);
      z-index: 999;
      display: none;
    }

    .overlay .myContainer{
      width: 800px;
      margin: auto;
      display: block;
      position: relative;
      top: 20%;
    }

    .overlay .myContainer img{
      max-width: 100%;
      width: auto;
    }

    .overlay .myContainer span{
      border-radius: 50%;
      background-color: #000;
      color: #FFF;
      display: inline-block;
      width: 30px;
      height: 30px;
      text-align: center;
      line-height: 25px;
      border: 2px solid #ccc;
      float: right; 
      cursor: pointer;
    }

</style>
<?php $data = date ("Y-m-d h:m"); ?>
<?php if(strtotime('2016-11-25 00:00') < strtotime($data) && strtotime('2016-11-25 23:59') > strtotime($data)){ ?>
  <div class="overlay">
    <div class="myContainer">
      <span>X</span>
      <a href="http://pages.dietwin.com.br/black-friday-dietwin" target="_blank">
        <img src="<?php echo SITE_URL ?>/views/imagens/bloack-friday.jpg">
      </a>
    </div>
  </div>
<?php } ?>
<?php require_once 'includes/header.php' ?>
<div id="wrapper">
<?php require_once 'includes/nav.php' ?>
<section class="conteudo cinza">
  <div class="container">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <h2>A evolução da nutrição nas suas mãos.</h2>
      <p>Bem-vindo ao melhor e mais tradicional software de nutrição do Brasil, que foi feito para quem sabe a importância da qualidade e da eficiência no dia a dia. O Dietwin possui navegação fácil e rápida, organizando as suas consultas e simplificando a sua vida. E o banco de dados é flexível, podendo receber novas informações a todo momento. Depois, o Dietwin calcula, cruzas as informações e interpreta os resultados, auxiliando na tomada de decisões.
Tudo o que você sempre sonhou agora é realidade. Depois de conhecer o Dietwin, você não vai entender como conseguiu trabalhar sem ele.</p>
    </div>
    <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
      <img src="<?php echo SITE_URL ?>/views/imagens/computador-preview.jpg" alt="O futuro da nutrição. Hoje." class="img-responsive">
    </div>
  </div>
</section>
<section class="conteudo cinza">
  <div class="container">
    <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
      <img src="<?php echo SITE_URL ?>/views/imagens/mulher-teste-software.jpg" alt="DietWin Plus. Tudo o que o dietWin oferece. Com um Plus a mais." class="img-responsive">
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <h2>Dietwin Plus. <span>Muito além do melhor.</span></h2>
      <h3>Se o Dietwin já é completo, a versão Plus é a definitiva.</h3>
      <p>Com uma série de recursos extras, o Dietwin Plus é a solução ideal para profissionais que exigem o melhor. É muita tecnologia ao seu alcance. Imagine ter um software com avaliação nutricional avançada, avaliação clínica com cadastro de doenças, diversos protocolos de avaliação nutricional, estratégias nutricionais e muito mais. E todas essas informações sendo avaliadas, calculadas e interpretadas para você. Sempre com uma interface simples e rápida porque você não tem tempo a perder.</p>
      <p>Trabalhar da maneira que você sempre quis está a apenas um clique de distância. </p>
      <p>Teste agora mesmo o software de nutrição mais inovador do Brasil. </p>
    </div>
  </div>
</section>
<section class="conteudo cinza">
  <div class="container">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <h2>A solução indispensável. </h2>
      <p>O Dietwin é desenvolvido por nutricionistas. Isso significa que ele foi criado pensando exatamente no que você precisa: a otimização do seu tempo. Enquanto o Dietwin calcula e cruza as informações do paciente, você analisa os dados e realiza um atendimento ainda melhor e mais eficiente.  </p>
      <p>O Dietwin tem inteligência artificial. Ao mesmo tempo em que emite resultados, vai eliminando as distorções da avaliação convencional. Você ganha tempo e precisão na tomada de decisão.</p>
      <p>Tenha todas as informações de seus pacientes ao alcance de um clique. O Dietwin é um grande banco de dados que armazena de maneira fácil tudo o que você precisa. E para auxiliar na consulta e promover a educação alimentar, tudo é mostrado de maneira fácil e didática.</p>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 hidden-xs">
      <div id="swiffycontainer" style="width: 100%; height: 402px">
    </div>
      <p>Nenhum outro software tem tantas funções e recursos. O Dietwin é usado por universidades, hospitais, agências governamentais e nutricionistas de todo o Brasil. Está na hora de você também contar com o Dietwin no seu dia a dia. </p>
    </div>
  </div>
  <a id="caracteristicas"></a>
</section>
<section class="conteudo cinza tab">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
      <h2>Todas as funções que você sempre quis.<br /><span>Juntas.</span></h2>
    </div>
    <div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-10 col-sm-offset-1 hidden-xs text-center">
      <div id="controla-tabs" class="flexslider">
        <ul class="slides">

          <li class="text-center">
            <div class="prontuario"></div>
            <span class="label-icone">Ficha do<br /> paciente</span>
          </li>

          <li class="text-center">
            <div class="avaliacao"></div>
            <span class="label-icone">Avaliação clínica</span>
          </li>

          <li class="text-center">
            <div class="exercicios"></div>
            <span class="label-icone">Avaliação<br />nutricional</span>
          </li>

          <li class="text-center">
            <div class="prescricao"></div>
            <span class="label-icone">Prescrição<br />dietética</span>
          </li>

          <li class="text-center">
            <div class="dieta"></div>
            <span class="label-icone">Análise<br />dietética</span>
          </li>   

          <li class="text-center">
            <div class="analise"></div>
            <span class="label-icone">Multitasking e personalização</span>
          </li>


          
        </ul>
      </div>
    </div>
    <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 hidden-xs">
      <div id="tabs" class="flexslider">
        <ul class="slides">
          <li>
            <p>O Dietwin armazena a ficha do paciente com a evolução de todas as datas das consultas permitindo sua visualização e acessibilidade de navegação em ajustes e adequação para a avaliação bem como poder evoluir a prescrição e seu prontuário. Decreta o fim do papel, tudo o que você necessita consultar sobre o paciente deve estar contido nesta ficha.</p>
          </li>
          <li>
            <p>O Dietwin Plus permite o registro clínico com o histórico de doenças, sintomas, sinais clínicos, fármacos ingeridos e exames laboratoriais. Ainda, disponibiliza uma ferramenta de anamnese, onde questionários de inquéritos podem ser gravados como modelos para minimizar o trabalho futuro em novas avaliações. Com base em todos esses dados, que podem ser atualizados sempre que necessário, o software realiza uma análise relacional em cadeia das informações, gerando indicativos para a estratégia nutricional e facilitando a prescrição dietética. Este grande “livro eletrônico” faz toda a diferença para a qualidade e a eficiência do atendimento nutricional.
Suas ferramentas dinâmicas garantem a navegação rápida dentro do sistema, possibilitando a abertura de inúmeras telas e consultas simultâneas.</p>
          </li>
          <li>
            <p>Avalia todos os ciclos da vida de uma forma intuitiva. Você informa apenas o nome do paciente, data de nascimento, sexo, peso atual e altura. O Dietwin realiza os cálculos das fórmulas e equações necessárias a sua avaliação sinalizando todos os indicadores nutricionais de acordo com as medidas realizadas para o seu diagnóstico e interpretação final. O Dietwin vai calculando tudo, e você seleciona quais referências deseja informar no relatório. Toda a evolução do paciente fica arquivada em sua ficha, sendo facilmente verificada através da data da consulta. Outra facilidade é que o software permite o acompanhamento da evolução dentro do ciclo de vida, fornecendo gráficos comparativos para melhor análise do nutricionista e entendimento do paciente.</p>
          </li>
          <li>
            <p>Você tem a opção de detalhar ou refinar os resultados para análise dietética, administrando o seu tempo na consulta. A dieta torna-se muito mais rápida a partir de modelos de cardápios e um amplo cadastro de receitas. Tudo funciona de maneira simples e prática: ao selecionar um alimento e arrastar para a refeição desejada, é indicada uma medida caseira como sugestão. Os nutrientes são calculados automaticamente, assim como sua adequação com as DRIs.</p>
          </li>
          <li>
            <p>O Dietwin executa uma ampla análise dietética com as seguintes funções: Análise dos nutrientes; Adequação das recomendações diárias das DRIS: EAR, RDA, Al, UL; Nutrientes para monitoramento; Interações nutricionais; Pirâmide Alimentar; Estratégias nutricionais monitoradas. QAVE e QAVP/AMDR (Quadro de análise dos valores previstos e encontrados); Conflitos alimentares: alergias, intolerâncias, aversões; Características alimentares: dietéticas, corantes, conservantes, aditivos; Alimentos que compõem a quantidade do nutriente; Média de consumo de nutrientes de 3 dietas selecionadas.</p>
          </li>          
          <li>
            <p>O Dietwin possui um exclusivo recurso de multitasking, que permite a navegação rápida dentro do sistema com a abertura de inúmeras telas e consultas ao mesmo tempo. Além disso, oferece a função de copiar e colar entre as fichas de atendimento, garantindo mais agilidade na consulta. Você também pode customizar modelos de recomendações, capas de relatórios, tabelas de alimentos, listas de substituição e alimentos favoritos e prontuários. Todos esses recursos garantem que o seu atendimento seja ainda mais qualificado e eficaz.</p>
          </li>
          
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="conteudo marrom" style="display: none;">
  <div class="container">
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-right">
      <div class="documentacao-tecnica">
        <h2><span>Especificações</span> Técnicas</h2>
        <p>Está curioso em saber ainda mais sobre o Dietwin? Então acesse todos os detalhes nos arquivos técnicos. Neles, você vai encontrar a informação que deseja e também perceber que o Dietwin é ainda melhor do que você imagina.</p>
      </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="documentacao-arquivo">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <img src="<?php echo SITE_URL ?>/views/imagens/pdf-ico.jpg" alt="Detalhamento da prescrição da dieta">
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <span>(4.8mb)</span>
                <a href="#" title="Detalhamento da prescrição da dieta" target="_blank">Detalhamento da prescrição da dieta</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="documentacao-arquivo">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <img src="<?php echo SITE_URL ?>/views/imagens/pdf-ico.jpg" alt="Detalhamento da prescrição da dieta">
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <span>(4.8mb)</span>
                <a href="#" title="Detalhamento da prescrição da dieta" target="_blank">Detalhamento da prescrição da dieta</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="documentacao-arquivo">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <img src="<?php echo SITE_URL ?>/views/imagens/pdf-ico.jpg" alt="Detalhamento da prescrição da dieta">
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <span>(4.8mb)</span>
                <a href="#" title="Detalhamento da prescrição da dieta" target="_blank">Detalhamento da prescrição da dieta</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="documentacao-arquivo">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <img src="<?php echo SITE_URL ?>/views/imagens/pdf-ico.jpg" alt="Detalhamento da prescrição da dieta">
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <span>(4.8mb)</span>
                <a href="#" title="Detalhamento da prescrição da dieta" target="_blank">Detalhamento da prescrição da dieta</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="documentacao-arquivo">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <img src="<?php echo SITE_URL ?>/views/imagens/pdf-ico.jpg" alt="Detalhamento da prescrição da dieta">
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <span>(4.8mb)</span>
                <a href="#" title="Detalhamento da prescrição da dieta" target="_blank">Detalhamento da prescrição da dieta</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="documentacao-arquivo">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <img src="<?php echo SITE_URL ?>/views/imagens/pdf-ico.jpg" alt="Detalhamento da prescrição da dieta">
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <span>(4.8mb)</span>
                <a href="#" title="Detalhamento da prescrição da dieta" target="_blank">Detalhamento da prescrição da dieta</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="documentacao-arquivo">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <img src="<?php echo SITE_URL ?>/views/imagens/pdf-ico.jpg" alt="Detalhamento da prescrição da dieta">
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <span>(4.8mb)</span>
                <a href="#" title="Detalhamento da prescrição da dieta" target="_blank">Detalhamento da prescrição da dieta</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="documentacao-arquivo">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <img src="<?php echo SITE_URL ?>/views/imagens/pdf-ico.jpg" alt="Detalhamento da prescrição da dieta">
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <span>(4.8mb)</span>
                <a href="#" title="Detalhamento da prescrição da dieta" target="_blank">Detalhamento da prescrição da dieta</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
          <div class="documentacao-arquivo">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <img src="<?php echo SITE_URL ?>/views/imagens/pdf-ico.jpg" alt="Detalhamento da prescrição da dieta">
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <span>(4.8mb)</span>
                <a href="#" title="Detalhamento da prescrição da dieta" target="_blank">Detalhamento da prescrição da dieta</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <a id="comprar"></a>
</section>
<section class="conteudo cinza">
  <div class="container">
    <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
      <img src="<?php echo SITE_URL ?>/views/imagens/box-preview.jpg" alt="Muitos recursos, pouco investimento." class="img-responsive">
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <h2 class="margin-maior">Tudo é acima das expectativas.<br/><span>Menos o preço.</span></h2>
      <div class="row">
        <div class="col-lg-5 col-md-6 col-sm-offset-1 col-sm-4 col-xs-12">
          <div class="preco">
            <span class="label-icone">A partir de</span>
            <span class="moeda">R$</span><span class="reais">454,</span><span class="centavos">00</span>
          </div>
        </div>
        <div class="col-lg-6 col-md-7 col-sm-4 col-xs-12 comprar">
          <a href="<?php echo SITE_URL ?>/planos" class="botao-comprar compre" title="Comprar agora">
            <img src="<?php echo SITE_URL ?>/views/imagens/botao-comprar.png" alt="Comprar agora"> Comprar agora
          </a>
        </div>
      </div>
      <img src="<?php echo SITE_URL ?>/views/imagens/cartoes-aceitos.jpg" alt="Cartões aceitos" class="img-responsive hidden-xs cartoes">
    </div>
  </div>
  <a id="teste-gratis"></a>
</section>
<section class="conteudo verde">
  <div class="container">
    <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
      <h2>Teste o Dietwin por 14 dias grátis. <span><br />E você nunca mais vai querer trabalhar sem ele.</span></h2>
      <p>Preencha o formulário e faça o download da versão de demonstração do Dietwin, que possui todos os recursos disponíveis por 14 dias. </p>
    </div>
    <div class="col-lg-5 col-lg-offset-1 col-md-6 col-sm-12 col-xs-12">
      <form action="#" id="duvidas">
        <label for="nome">Nome Completo</label>
        <input type="text" name="nome" id="nome">
        <label for="email">E-mail</label>
        <input type="text" name="email" id="email">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <label for="telefone">Telefone</label>
            <input type="text" name="telefone" id="telefone">
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <label for="cidade">Cidade</label>
            <input type="text" name="cidade" id="cidade">
          </div>
          <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
            <div class="erro-form">

            </div>
          </div>
          <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
            <input type="submit" value="Fazer o download">
          </div>
        </div>
      </form>
    </div>
  </div>
  <a id="o-dietwin"></a>
</section>


<?php if(!empty($novidades)){ ?>
<section class="blog">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h2>Acompanhe nosso blog</h2>
    </div>

    <?php 

    if(isset($novidades) AND count($novidades)>0 AND is_array($novidades)){

    foreach($novidades as $novidade): ?>
    <div class="col-lg-6 col md-6 col-sm-12 col-xs-12">
      <div class="post">
        <a href="<?php echo SITE_URL."/blog/post/".$novidade->titulo_seo; ?>">
          <?php if($novidade->imagem!=""){ ?>
          <img src="<?php echo SITE_URL ?>/arquivos/novidades/thumb1/<?php echo $novidade->imagem; ?>" width="100%" class="img-responsive" alt="">
          <?php }else{ ?>
            <img src="<?php echo SITE_URL ?>/views/imagens/blog/post-1.jpg" class="img-responsive" alt="">
          <?php } ?>
        </a>
        <div class="content">
          <h3><a href="<?php echo SITE_URL."/blog/post/".$novidade->titulo_seo; ?>"><?php echo $novidade->titulo; ?></a></h3>
          <p><?php echo $novidade->resumo; ?></p>
        </div>
        <hr>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center footer">
          <span class="data"><?php echo Funcoes::blog_data_mes_completo($novidade->data); ?></span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center num-comentarios footer normal">
          <span class="icon-comentario"></span>
          <div class="clearfix"></div>
          <span class="total-comentarios"><span class="disqus-comment-count" data-disqus-url="<?php echo SITE_URL."/blog/post/".$novidade->titulo_seo; ?>">0</span> comentário (s)</span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center footer">
          <a href="<?php echo SITE_URL."/blog/post/".$novidade->titulo_seo; ?>" class="saiba-mais">Leia mais</a>
        </div>
      </div>
    </div>
  <?php endforeach; 
    }else if(!empty($novidades)){ ?>
      <div class="col-lg-6 col md-6 col-sm-12 col-xs-12">
      <div class="post">
        <a href="<?php echo SITE_URL."/blog/post/".$novidades->titulo_seo; ?>">
          <?php if($novidades->imagem!=""){ ?>
          <img src="<?php echo SITE_URL ?>/arquivos/novidades/thumb1/<?php echo $novidades->imagem; ?>" width="100%" class="img-responsive" alt="">
          <?php }else{ ?>
            <img src="<?php echo SITE_URL ?>/views/imagens/blog/post-1.jpg" class="img-responsive" alt="">
          <?php } ?>
        </a>
        <div class="content">
          <h3><a href="<?php echo SITE_URL."/blog/post/".$novidades->titulo_seo; ?>"><?php echo $novidades->titulo; ?></a></h3>
          <p><?php echo $novidades->resumo; ?></p>
        </div>
        <hr>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center footer">
          <span class="data"><?php echo Funcoes::blog_data_mes_completo($novidades->data); ?></span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center num-comentarios footer normal">
          <span class="icon-comentario"></span>
          <div class="clearfix"></div>
          <span class="total-comentarios"><span class="disqus-comment-count" data-disqus-url="<?php echo SITE_URL."/blog/post/".$novidades->titulo_seo; ?>">0</span> comentário (s)</span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center footer">
          <a href="<?php echo SITE_URL."/blog/post/".$novidades->titulo_seo; ?>" class="saiba-mais">Leia mais</a>
        </div>
      </div>
    </div>
  <?php }else{ ?>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        Nenhum post cadastrado no momento        
      </div>
  <?php } ?>

  <script type="text/javascript">
    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
    var disqus_shortname = 'dietwinblog'; // required: replace example with your forum shortname

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
    var s = document.createElement('script'); s.async = true;
    s.type = 'text/javascript';
    s.src = '//' + disqus_shortname + '.disqus.com/count.js';
    (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
  </script>

    <!--
    <div class="col-lg-6 col md-6 col-sm-12 col-xs-12">
      <div class="post">
        <a href="#">
          <img src="<?php echo SITE_URL ?>/views/imagens/blog/post-2.jpg" class="img-responsive" alt="">
        </a>
        <div class="content">
          <h3><a href="#">Visite um nutricionista. Hoje.</a></h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quam dolor, sagittis vitae elit vitae, interdum tristique sem. Cras quis elit bibendum, commodo diam eget, consequat nisl. Sed rhoncus nulla non rutrum venenatis. Sed cursus condimentum dolor sit amet convallis. Phasellus tortor eros.</p>
        </div>
        <hr>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center footer">
          <span class="data">14 de maio de 2015</span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center num-comentarios footer normal">
          <span class="icon-comentario"></span>
          <div class="clearfix"></div>
          <span class="total-comentarios">0 comentário (s)</span>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center footer">
          <a href="#" class="saiba-mais">Leia mais</a>
        </div>
      </div>
    </div>
  -->

    <div class="mais-posts col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
      <a href="<?php echo SITE_URL ?>/blog">Ver todas as novidades</a>
      <span class="folha"></span>
    </div>
  </div>
</section>
<?php } ?>

<section class="conteudo usuarios">
  <div class="container">
    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
      <h2>Milhares de usuários em todo o Brasil.</h2>
    </div>
    <div class="col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-12 col-xs-12">
      <p>O Dietwin é um sucesso há mais de 20 anos. Pioneiro no mercado de softwares de nutrição, o Dietwin é utilizado em universidades, hospitais, instituições de pesquisa, estabelecimentos de saúde, agências governamentais, pesquisadores brasileiros que trabalham fora do Brasil e diversos profissionais de nutrição.  </p>
      <p>Chegou a sua vez de contar com a qualidade, a facilidade e a precisão do melhor software de nutrição do Brasil. </p>
      <?php /*<div class="row">
                            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-10 col-offset-sm-1 col-xs-12">
                                <a title="Leia mais" class="leia-mais">Leia mais</a>
                            </div>
                        </div> */ ?>
    </div>
  </div>
</section>
<section class="conteudo quem-faz">
  <div class="container">
    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis augue neque, tempus nec nisl eu, suscipit mollis felis. Praesent non dolor dignissim, semper ante et, lacinia erat. Pellentesque sodales convallis ante a ornare. Nullam egestas odio in lorem rhoncus iaculis. Phasellus ac erat lorem. Maecenas ac vestibulum massa.</p>
      <p>Vestibulum et ligula mollis, viverra sapien at, consectetur nunc. Aliquam sit amet lacus eu odio tincidunt tincidunt. Mauris sed ultrices metus. Quisque mattis justo et eros laoreet pellentesque. Nunc mollis, sapien a pulvinar </p>
    </div>
    <div class="col-lg-5 col-md-5 hidden-sm hidden-xs text-center">
      <img src="<?php echo SITE_URL ?>/views/imagens/pessoas-dietwin.jpg" alt="Pessoas por trás do Dietwin">
      <h2>Por trás do Dietwin</h2>
    </div>
  </div>
</section>
<section class="conteudo cinza especialista-mapa">
  <div class="container">
    <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
      <img src="<?php echo SITE_URL ?>/views/imagens/especialista-dietwin.jpg" alt="Dietwin. Auxiliando o nutricionista na tomada de decisão. " class="img-responsive">
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <h2><span>Dietwin.</span> Auxiliando o nutricionista na tomada de decisão. </h2>
      <p>Nós também queremos ser o seu aliado para você decidir pelo melhor software de nutrição. Caso você queira ainda mais informações ou falar pessoalmente, entre em contato. Teremos o maior prazer em atendê-lo. </p>
      <div class="telefone text-center">
        <span>(51)</span> 3337.7908
      </div>
      <img src="<?php echo SITE_URL ?>/views/imagens/mapa-localizacao.jpg" alt="Mapa localização Dietwin" class="img-responsive hidden-xs mapa-dietwin">
    </div>
  </div>
  <a id="contato"></a>
</section>
<?php require_once 'includes/footer.php' ?>
</div>

<script type="text/javascript">
    $(".myContainer span").click(function(event) {
      $(".overlay").fadeOut();
    });

    $(".overlay").click(function(event) {
      $(this).fadeOut();
    });


     // cookie
    var myCookies = document.cookie; 
    if(myCookies!=undefined && myCookies.length > 0) // verificando se o mesmo existe
    {
      var meuCookie = myCookies.indexOf("flutuante");
    }

     if(!(meuCookie >= 0)){
        expires = 7*24*60*60*1000;
        document.cookie="name=flutuante; expires="+expires+"";
        jQuery('.overlay').fadeIn();
      }

</script>

</body>
</html>
