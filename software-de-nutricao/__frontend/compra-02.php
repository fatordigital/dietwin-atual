<!DOCTYPE html>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <?php require_once 'includes/head.php' ?>
    <body>
        <?php require_once 'includes/nav.php' ?>
        
        <section class="conteudo cinza">
            <div class="container">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <h1 class="interna">Carrinho de compras</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ut mi eget purus semper vestibulum sit amet nec justo. Vivamus non elit non est faucibus hendrerit. Fusce at nulla ut sapien tempus fringilla</p>
                </div>
                <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
                    <img src="<?php echo $baseurl ?>/imagens/caixas-dietwin-cadastre-se.jpg" alt="dietWin Plus e dietWin Tradicional">
                </div>
                <section class="formulario">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="titulo-formulario">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h2>Dados de sua compra</h2>
                            </div>
                        </div>
                        <div class="endereco-entrega">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="lista-produtos">
                                        <ol>
                                            <li>Dietwin Plus</li>
                                        </ol>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
                                    <div class="listagem-valores">
                                        <ul>
                                            <li>R$ 690,00</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="formulario-cadastro total-venda">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 text-right">
                                    Total de sua compra:
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-right">
                                <div class="preco">
                                    <span class="moeda">R$</span><span class="reais">690,</span><span class="centavos">00</span>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="endereco-entrega login-cadastro">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <p class="detalhe-finalizar-compra">Para realizar a sua compra na loja virtual dietWIn, é importante que você seja um usuário cadastrado no site. O processo de cadastro é simples e rápido e garante a segurança na sua compra e na entrega de seus pedidos!</p>
                                </div>
                                <div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-12 col-xs-12">
                                    <h3>Já sou cadastrado</h3>
                                    <form action="#" id="login">
                                        <label for="email">E-mail:</label>
                                        <input type="text" id="email" name="email">
                                        <label for="senha">Senha:</label>
                                        <input type="password" id="senha" name="senha">
                                        <input type="submit" value="Entrar" class="processo-compra-submit">
                                    </form>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 text-center">
                                    <h3>Quero me cadastrar</h3>
                                    <a href="#" title="Quero me cadastrar">
                                        <img src="<?php echo $baseurl ?>/imagens/quero-me-cadastrar.png" alt="Quero me cadastrar" class="quero-me-cadastrar">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="formulario-cadastro">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/compra-segura-pagseguro.png" alt="Compra 100% segura - PagSeguro">
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/formas-de-pagamento.png" alt="Compra 100% segura - PagSeguro" class="cartoes-compra">
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-4 hidden-xs pull-right">
                                    <input type="submit" value="Finalizar compra" class="processo-compra-submit">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>

        <?php require_once 'includes/footer.php' ?>
    </body>
</html>
