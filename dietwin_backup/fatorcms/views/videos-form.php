<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php'; ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

        <!-- Page Head -->
		<?php if (is_null($video) OR is_null($video->id)) { ?>
			<h2>Cadastro de um Vídeo</h2>
			<p id="page-intro">Utilize o formulário abaixo para cadastrar um vídeo no site.</p>
		<?php } else { ?>
			<h2>Edição de um Vídeo</h2>
			<p id="page-intro">No formulário abaixo você pode alterar os dados de um vídeo.</p>
		<?php } ?>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<?php if (is_null($video) OR is_null($video->id)) { ?>
					<h3>Dados de um vídeo</h3>
				<?php } else { ?>
					<h3>Informações do vídeo <?php echo $video ? $video->titulo : '' ?></h3>
				<?php } ?>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<form action="<?php echo SITE_URL ?>/fatorcms/videos/<?php echo (is_null($video) OR is_null($video->id)) ? 'cadastrar' : 'atualizar' ?>" method="post" id="video_form">

					<fieldset>

						<?php if ( ! is_null($video)) { ?>
							<input type="hidden" name="id" value="<?php echo $video->id ?>" />
						<?php } ?>

						<p>
                            <label for="produto_id">Categoria</label>
							<select name="produto_id" id="produto_id" class="text-input medium-input required">
								<option value="0" <?php echo ($video AND $video->produto_id == 0) ? 'selected="selected"' : '' ?>>Tópicos diversos</option>
								<?php
								if ($produtos AND count($produtos) > 0)
								{
									foreach ($produtos as $produto)
									{
										echo '<option value="'.$produto->id.'" '.(($video AND $video->produto_id == $produto->id) ? 'selected="selected"' : '').'>'.$produto->nome.'</option>';
									}
								}
								?>
							</select>
                        </p>

                        <p>
                            <label for="titulo">Título</label>
                            <input class="text-input large-input required" type="text" id="titulo" name="titulo" maxlength="100" value="<?php echo $video ? $video->titulo : '' ?>" />
                            <br/><small>Número de caracteres restantes: <span id="titulo_contador">100</span></small>
                        </p>

                        <p>
							<label for="link">Link</label>
							<input class="text-input large-input required" type="text" id="link" name="link" maxlength="500" value="<?php echo $video ? $video->link : '' ?>" />
	                        <br/><small>Cole no campo acima o link (que aparece na barra de endereço do seu navegador) para o vídeo do YouTube.</small>
						</p>

						<p>
							<label for="resumo">Resumo</label>
							<textarea class="text-input large-input" id="resumo" name="resumo" cols="80" rows="4"><?php echo $video ? $video->resumo : '' ?></textarea>
							<br /><small>Número de caracteres restantes: <span id="resumo_contador">600</span></small>
						</p>

						<p>
							<label for="descricao">Descrição</label>
							<textarea class="text-input" cols="79" rows="10" id="descricao" name="descricao"><?php echo $video ? $video->descricao : '' ?></textarea>
						</p>

                        <p>
                            <label for="data">Data</label>
                            <input class="text-input small-input required" type="text" id="data" name="data" maxlength="10" value="<?php echo ($video AND ! is_null($video->data)) ? date('d/m/Y',strtotime($video->data)) : '' ?>" />
                        </p>

						<p>
							<input class="button" type="submit" value="<?php echo (is_null($video) OR is_null($video->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
						</p>

					</fieldset>

					<div class="clear"></div><!-- End .clear -->

				</form>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>