<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Dietwin</title>

		<!-- Estilos -->
		<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL ?>/assets_novo/css/lib/reset.css">
		<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL ?>/assets_novo/css/lib/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL ?>/assets_novo/css/lib/owl.carousel.css">
		<link rel="stylesheet" type="text/css" href="<?php echo SITE_URL ?>/assets_novo/css/style.min.css">

		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	</head>
	<body>
	<div id="fullpage">
		<div class="section header" id="section0">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h1 class="logo"><a href="#">Dietwin</a></h1>
					</div>
					<div class="col-xs-12">
						<div class="box-caption">
							<h2>A evolução da nutrição nas suas mãos.</h2>
							<h3>Tudo o que você procura em software de nutrição.</h3>
							<a href="#section2" id="link-prof" class="btn btn-default">DIETWIN PROFISSIONAL</a>
							<a href="#tabela-nutricional" class="btn btn-default">TABELA NUTRICIONAL</a>
						</div>
					</div>
				</div>
			</div>		
			<nav class="navbar navbar-default">
			  <div class="container">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand" href="#"><img src="<?php echo SITE_URL ?>/assets_novo/images/logo-menu.png" class="img-responsive"></a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">			      
			      <ul class="nav navbar-nav navbar-right">
			        <li><a href="#section2">Ajuda</a></li>
			        <li><a href="#section6">Contato</a></li>
			        <li><a href="#">Blog</a></li>
                    <li><a href="#section2" class="aaa">DietWin Profissional</a></li>
                    <li><a href="#section4" class="aaa">Tabela Nutricional</a></li>
                    <?php /*
			        <li><a href="#section1" class="hide">Institucional</a></li>
			        <li><a href="#" class="btn btn-default hide">Login</a></li>
			        <li><a href="#" class="btn btn-warning hide">Faça um teste grátis</a></li>
                    */;?>
			      </ul>
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>
		</div>		
		<div class="section dietwin" id="section1">
			<div class="container">
				<div class="row">
					<div class="col-lg-offset-7 col-md-offset-6 col-lg-5 col-md-6 col-xs-12">
						<h1>Dietwin</h1>
						<h2>Auxiliando o nutricionista na tomada de decisão.</h2>
						<p>
							Bem-vindo ao melhor e mais tradicional software de nutrição do Brasil, que foi feito 
							para quem sabe a importância da qualidade e da eficiência no dia a dia. 
						</p>
						<p>
							O Dietwin é um sucesso há mais de 20 anos. Pioneiro no mercado de softwares de 
							nutrição, o Dietwin é utilizado em universidades, hospitais, instituições de 
							pesquisa, estabelecimentos de saúde, agências governamentais, pesquisadores 
							brasileiros que trabalham fora do Brasil e diversos profissionais de nutrição.
						</p>
						<p>
							<img src="<?php echo SITE_URL ?>/assets_novo/images/logo-dietwin.png" class="img-responsive">
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="section dietwin-profissional" id="section2">
			<div class="container box-linha">
				<div class="row">
					<div class="zSlider" id="zSlider">
						<div class="inner">
							<div class="item" id="calculo-dietas">
								<div class="col-lg-offset-1 col-md-offset-1 col-lg-6 col-md-6 col-xs-12">
									<h1>Dietwin Profissional</h1>
									<h2>A solução indispensável.</h2>
									<p>
										O melhor e mais tradicional software de nutrição do Brasil, que foi feito para quem 
										sabe a importância da qualidade e da eficiência no dia a dia. O Dietwin possui 
										navegação fácil e rápida, organizando as suas consultas e simplificando a sua vida. 
										E o banco de dados é flexível, podendo receber novas informações a todo momento. 
									</p>
									<p>
										Depois, o Dietwin calcula, cruzas as informações e interpreta os resultados, auxiliando
										 na tomada de decisões. Tudo o que você sempre sonhou agora é realidade. Depois de 
										 conhecer o Dietwin, você não vai entender como conseguiu trabalhar sem ele.
									</p>
								</div>
								<div class="col-lg-5 col-md-5 colsm-5 col-xs-12">
									<a href="<?php echo SITE_URL ?>/profissional" class="btn btn-sucess">CONHEÇA MAIS</a>
									<a href="<?php echo SITE_URL ?>/planos" class="link link-download hide">Download</a>
									<a href="<?php echo SITE_URL ?>/planos" class="link link-comprar hide">Comprar</a>
								</div>		
							</div><!--END item zSlider -->	
							<div class="item active" id="funcionalidades">
								<div class="col-lg-offset-1 col-md-offset-1 col-lg-6 col-md-6 col-xs-12">
									<h1>Funcionalidades</h1>
									<h2>Todas as funções que você<br> sempre quis, juntas. </h2>
									<ul>
										<li>
											<div class="icon-funcionalidade um">ANÁLISE DIETÉTICA</div>
											<div class="icon-funcionalidade quatro">AVALIAÇÃO NUTRICIONAL</div>
										</li>
										<li>
											<div class="icon-funcionalidade dois">FICHA DO PACIENTE</div>
											<div class="icon-funcionalidade cinco">Prescrição dietética</div>
										</li>
										<li>
											<div class="icon-funcionalidade tres">AVALIAÇÃO CLÍNICA</div>
											<div class="icon-funcionalidade seis">Multitasking e personalização</div>
										</li>
									</ul>										
								</div>
								<div class="col-lg-5 col-md-5 colsm-5 col-xs-12">
									<a href="<?php echo SITE_URL ?>/profissional" class="btn btn-sucess hide">CONHEÇA MAIS</a>
									<a href="http://www.dietwin.com.br" class="btn btn-sucess">CONHEÇA MAIS</a>
									<a href="<?php echo SITE_URL ?>/planos" class="link link-download hide">Download</a>
									<a href="<?php echo SITE_URL ?>/planos" class="link link-comprar hide">Comprar</a>
								</div>		
							</div><!--END item zSlider -->	
							<div class="controls">
								<div class="my-control left-control">
									Funcionalidades
								</div>
								<div class="my-control right-control">
									Cálculo de Dietas 
								</div>
							</div>
						</div><!--END inner zSlider -->	
					</div><!--END zSlider -->
					<div class="box-monitor">
						<div class="imagem-monitor">
							<img src="<?php echo SITE_URL ?>/assets_novo/images/monitor.png" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
        <?php /*
		<div class="section preco" id="section3">
			<div class="container box-linha">
				<div class="row">
					<div class="col-lg-offset-1 col-md-offset-1 col-lg-6 col-md-6 col-xs-12">
						<h1>Tudo é acima das expectativas.</h1>
						<h2>Menos o preço.</h2>
						<h3>Teste o Dietwin por 14 dias grátis.</h3>
						<p>E você nunca mais vai querer trabalhar sem ele.</p>
						<a href="<?php echo SITE_URL ?>/planos" class="btn btn-warning btn-lg">FAÇA O DOWNLOAD AGORA</a>
					</div>
				</div>
			</div>
		</div>
        */; ?>
		<div class="section tabela-nutricional" id="section4">
			<div class="container">
				<div class="row">
					<div class="col-lg-offset-1 col-md-offset-1 col-lg-6 col-md-6 col-xs-12">
						<h1>Tabela Nutricional</h1>
						<h2>A solução completa para<br>Tabelas Nutricionais. </h2>
						<a href="<?php echo SITE_URL ?>/tabela-nutricional" class="btn btn-warning btn-lg">CONHEÇA A <strong>TABELA NUTRICIONAL</strong></a>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-4 col-xs-12">
						<ul class="animation-white">
							<li><img src="<?php echo SITE_URL ?>/assets_novo/images/icons-animacao/icon-white-um.png"></li>
							<li><img src="<?php echo SITE_URL ?>/assets_novo/images/icons-animacao/icon-white-quatro.png"></li>
							<li><img src="<?php echo SITE_URL ?>/assets_novo/images/icons-animacao/icon-white-dois.png"></li>
							<li><img src="<?php echo SITE_URL ?>/assets_novo/images/icons-animacao/icon-white-cinco.png"></li>
							<li><img src="<?php echo SITE_URL ?>/assets_novo/images/icons-animacao/icon-white-tres.png"></li>
						</ul>
						<div class="celular">
							<img src="<?php echo SITE_URL ?>/assets_novo/images/cel.png" class="img-responsive">
						</div>
						<ul class="animation-orange">
							<li><img src="<?php echo SITE_URL ?>/assets_novo/images/icons-animacao/icon-orange-um.png"></li>
							<li><img src="<?php echo SITE_URL ?>/assets_novo/images/icons-animacao/icon-orange-quatro.png"></li>
							<li><img src="<?php echo SITE_URL ?>/assets_novo/images/icons-animacao/icon-orange-dois.png"></li>
							<li><img src="<?php echo SITE_URL ?>/assets_novo/images/icons-animacao/icon-orange-cinco.png"></li>
							<li><img src="<?php echo SITE_URL ?>/assets_novo/images/icons-animacao/icon-orange-tres.png"></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
        <div class="section tabela-nutricional-funcionalidades" id="section5">
            <div class="container box-linha">
                <div class="row">
                    <div class="zSlider" id="zSlider2">
                        <div class="inner">
                            <div class="item active" id="calculo-dietas-2">
                                <div class="col-lg-offset-1 col-md-offset-1 col-lg-6 col-md-6 col-xs-12">
                                    <h1>Tabela Nutricional</h1>
                                    <h2 class="hide">Lorem Ipsum dolor.</h2>
                                    <p>
                                        A tabela nutricional é obrigatória em quase todos os alimentos comercializados prontos, seja caseiro ou industrializado, e tem como objetivo informar o consumidor a quantidade de nutrientes e calorias que aquele alimento ou bebida possui.
                                    </p>
                                    <p>
                                        Conhecida também como Informação Nutricional, esta tabela possui as quantidades de carboidratos, proteínas, gorduras totais, além de vitaminas e minerais, por porção de acordo com o VRC (Valor de Referência para Consumo). Você só precisa da ficha técnica completa, com ingredientes e quantidades para o sistema calcular todas informações nutricionais.
                                    </p>
                                </div>
                                <div class="col-lg-5 col-md-5 colsm-5 col-xs-12">
                                    <a href="http://www.dietwin.com.br/preview/tabela-nutricional" target="_blank" class="btn btn-sucess">CONHEÇA MAIS</a>
                                    <a href="#" class="link link-download hide">Download</a>
                                    <a href="#" class="link link-comprar hide">Comprar</a>
                                </div>
                            </div><!--END item zSlider -->
                            <div class="item hide" id="funcionalidades-2">
                                <div class="col-lg-offset-1 col-md-offset-1 col-lg-6 col-md-6 col-xs-12">
                                    <h1>Funcionalidades</h1>
                                    <h2>Todas as funções que você<br> sempre quis, juntas. </h2>
                                    <ul>
                                        <li>
                                            <div class="icon-funcionalidade um">ANÁLISE DIETÉTICA</div>
                                            <div class="icon-funcionalidade quatro">AVALIAÇÃO NUTRICIONAL</div>
                                        </li>
                                        <li>
                                            <div class="icon-funcionalidade dois">FICHA DO PACIENTE</div>
                                            <div class="icon-funcionalidade cinco">Prescrição dietética</div>
                                        </li>
                                        <li>
                                            <div class="icon-funcionalidade tres">AVALIAÇÃO CLÍNICA</div>
                                            <div class="icon-funcionalidade seis">Multitasking e personalização</div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-5 col-md-5 colsm-5 col-xs-12">
                                    <a href="http://www.dietwin.com.br/preview/tabela-nutricional" target="_blank" class="btn btn-sucess">CONHEÇA MAIS</a>
                                    <a href="#" class="link link-download hide">Download</a>
                                    <a href="#" class="link link-comprar hide">Comprar</a>
                                </div>
                            </div><!--END item zSlider -->
                            <div class="controls hide">
                                <div class="my-control left-control">
                                    Funcionalidades
                                </div>
                                <div class="my-control right-control">
                                    Cálculo de Dietas
                                </div>
                            </div>
                        </div><!--END inner zSlider -->
                    </div><!--END zSlider -->
                    <div class="box-monitor">
                        <div class="imagem-monitor">
                            <img src="<?php echo SITE_URL ?>/assets_novo/images/monitor.png" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="section contato" id="section6">
			<div class="container">
				<div class="row">
					<div class="col-lg-offset-2 col-md-offset-2 col-lg-8 col-md-8 col-xs-12">
						<img src="<?php echo SITE_URL ?>/assets_novo/images/icons/icon-suporte.png" class="img-responsive center-block">
						<h1>Suporte especializado</h1>
						<p>
							Está com alguma dificuldade em trabalhar com um software da família Dietwin? 
							Fique tranquilo que iremos ajudá-lo. Utilize um dos canais abaixo para fazer contato. 
						</p>
					</div>
					<div class="clearfix"></div>
					<form action="#" method="post" id='form1'>
						<p class="msg-form">Mensagem enviada com sucesso!!</p>
						<div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div class="col-lg-12 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="nome" placeholder="Nome" required title="Informe o nome">
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="email" class="form-control" name="email" placeholder="Email" required title="Informe um email válido">
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<input class="form-control" type="text" name="telefone" placeholder="Telefone" required title="Informe o seu telefone">
								</div>
							</div>
						</div>
						
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<textarea class="form-control" rows="4" name="mensagem" placeholder="Mensagem" required title="Informe uma mensagem"></textarea>
							</div>
						</div>
						<div class="col-lg-11 col-md-11 col-sm-11 col-xs-12">
							<div class="form-group">
								<button class="btn btn-primary pull-right">Enviar</button>
							</div>
						</div>
					</form> 
					<div class="clearfix"></div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<h3 class="phone">
							<big>(51) <strong>3337.7908</strong></big>
							suporte@dietwin.com.br
						</h3>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<h3 class="address">
							<big>Rua Xavier Ferreira, 101</big>
							Bairro Auxiliadora<br>Porto Alegre/RS
						</h3>
					</div>
				</div>
			</div>
		</div>
		<div class="section footer" id="section7">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
						<h2>Institucional</h2>
						<p>
                            Através de nossos softwares procuramos ajudar nossos clientes a gerar resultados com mais confiança, segunça e praticidade no lançamento das informações.
						</p>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<h2>Sobre</h2>
						<nav>
							<ul>
								<li><a href="#section2">Ajuda</a></li>
							        <li><a href="#section6">Contato</a></li>
			        				<li><a href="#">Blog</a></li>
								<li><a href="#section2" class="aaa">DietWin Profissional</a></li>
								<li><a href="#section4" class="aaa">Tabela Nutricional</a></li>
							</ul>
						</nav>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<h2>Assine nossa newsletter</h2>
						<p>
							Fique por dentro das principais informações da Dietwin Profissional. 
							Assine e receba novidades exclusivas!
						</p>
						<form action="#" method="post" id='fomr2'>
							<div class="form-group">
								<input type="text" class="form-control" nome="newsEmail" placeholder="Digite seu Email" required title="Informe um email válido">
							</div>
							<button class="btn btn-warning">Enviar</button>		
							<p class="msg-form">Mensagem enviada com sucesso!!</p>					
						</form>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
						<p>(51) 3337.7908  suporte@dietwin.com.br</p>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
						<img src="<?php echo SITE_URL ?>/assets_novo/images/selo-google.png" class="img-responsive selos">
					</div>
					<div class="col-lg-1 col-md-1 col-sm-1 col-xs-6">
						<img src="<?php echo SITE_URL ?>/assets_novo/images/selo-ebit.png" class="img-responsive selos">
					</div>
					<div class="col-xs-12">
						<hr>
						<p class="text-center">Desenvolvido por Fator Digital &copy;2017. Todos os direitos reservados.</p>
					</div>
				</div>
			</div>
		</div>
	</div>			
		<script type="text/javascript" src="<?php echo SITE_URL ?>/assets_novo/js/lib/jquery-1.12.0.min.js"></script>
		<script type="text/javascript" src="<?php echo SITE_URL ?>/assets_novo/js/lib/jquery-scrollto.js"></script>		
		<script type="text/javascript" src="<?php echo SITE_URL ?>/assets_novo/js/lib/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo SITE_URL ?>/assets_novo/js/lib/jquery.validate.min.js"></script>
		<script type="text/javascript" src="<?php echo SITE_URL ?>/assets_novo/js/lib/jquery.fullpage.js"></script>
		<script type="text/javascript" src="<?php echo SITE_URL ?>/assets_novo/js/lib/mask.min.js"></script>
		<script type="text/javascript" src="<?php echo SITE_URL ?>/assets_novo/js/main.js"></script>		
		
	</body>
</html>