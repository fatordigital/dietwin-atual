<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php' ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

		<!-- Page Head -->
		<h2>Lista de Categorias cadastradas</h2>
		<p id="page-intro">Abaixo estão listadas todas as categorias que são exibidas no site.</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Categorias</h3>

				<input class="novidade-categoria button botao-cadastrar" type="button" value="Cadastrar uma categoria" />

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<table>

					<thead>
						<tr>
							<th>ID</th>
							<th>Categoria</th>							
							<th>Ações</th>
						</tr>
					</thead>

					<tbody>
						<?php
						if ($categorias AND count($categorias) > 0)
						{
							foreach ($categorias as $categoria)
							{
							?>
								<tr>
									<td class="">
										<?php echo $categoria->id ?>
									</td>
									<td class="">
										<a href="<?php echo SITE_URL ?>/fatorcms/novidades/categorias-editar/<?php echo $categoria->id ?>" title="Editar a categoria &quot;<?php echo $categoria->nome ?>&quot;"><?php echo $categoria->nome; ?></a>
									</td>
									<td>
										<!-- Icons -->
										<a href="<?php echo SITE_URL ?>/fatorcms/novidades/categorias-editar/<?php echo $categoria->id ?>" title="Editar a categoria &quot;<?php echo $categoria->nome ?>&quot;">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" />
										</a>
										<a href="<?php echo SITE_URL ?>/fatorcms/novidades/categorias-excluir/<?php echo $categoria->id ?>" title="Excluir a categoria &quot;<?php echo $categoria->nome ?>&quot;" class="item-confirmar">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/cross.png" alt="Excluir" />
										</a>
									</td>
								</tr>
							<?php
							}
						}
						else
						{
							echo '<tr><td colspan="4">'.((isset($buscar) AND ! is_null($buscar)) ? 'Nenhuma novidade encontrada para esta busca.' : 'Nenhuma novidade cadastrada até o momento.' ).'</td></tr>';
						}
						?>
					</tbody>

				</table>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>