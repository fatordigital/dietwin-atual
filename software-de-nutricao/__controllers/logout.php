<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jonathan.alba
 * Date: 14/05/12
 * Time: 16:50
 * To change this template use File | Settings | File Templates.
 */
class Controller_Logout extends Controller_Padrao
{
    /**
     * Chama o construtor da classe pai
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método inicial que faz a renderização básica da página
     * @param $parametros
     * @return void
     */
    public function index($parametros)
    {

        if (isset($_SESSION['cliente_id']))
        {
            unset($_SESSION['cliente_id']);
        }
        header('Location: '.SITE_URL.'/index.php'); exit;

    }

}
