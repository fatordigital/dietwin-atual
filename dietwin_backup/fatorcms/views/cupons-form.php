<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php'; ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

        <!-- Page Head -->
		<?php if (is_null($cupom) OR is_null($cupom->id)) { ?>
			<h2>Cadastro de um Cupom</h2>
			<p id="page-intro">Utilize o formulário abaixo para cadastrar um cupom no site.</p>
		<?php } else { ?>
			<h2>Edição de um Cupom</h2>
			<p id="page-intro">No formulário abaixo você pode alterar os dados de um cupom.</p>
		<?php } ?>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<?php if (is_null($cupom) OR is_null($cupom->id)) { ?>
					<h3>Dados de um cupom</h3>
				<?php } else { ?>
					<h3>Informações do cupom de código <?php echo $cupom ? $cupom->codigo : '' ?></h3>
				<?php } ?>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<form action="<?php echo SITE_URL ?>/fatorcms/cupons/<?php echo (is_null($cupom) OR is_null($cupom->id)) ? 'cadastrar' : 'atualizar' ?>" method="post" id="cupom_form">

					<fieldset>

						<?php if ( ! is_null($cupom)) { ?>
							<input type="hidden" name="id" value="<?php echo $cupom->id ?>" />
						<?php } ?>

						<p>
                            <label for="produto_id">Produto</label>
							<select name="produto_id" id="produto_id" class="text-input medium-input required">
								<?php
								if ($produtos AND count($produtos) > 0)
								{
									foreach ($produtos as $produto)
									{
										echo '<option value="'.$produto->id.'" '.(($cupom AND $cupom->produto_id == $produto->id) ? 'selected="selected"' : '').'>'.$produto->nome.'</option>';
									}
								}
								?>
							</select>
                        </p>

                        <p>
                            <label for="cliente_id">Cliente</label>
                            <input class="" type="text" id="cliente_id" name="cliente_id" />
                        </p>

                       <?php /* <p>
                            <label for="clientes">Cliente</label>
                            <input type="text" name="clientes" id="clientes" class="text-input medium-input" value="<?php echo (isset($cliente) AND $cliente) ? $cliente->nome : '' ?>" />
                            <input type="hidden" name="cliente_id" id="cliente_id" value="<?php echo (isset($cliente) AND $cliente) ? $cliente->id : '' ?>" />
                        </p>   */ ?>

                        <p>
                            <label for="codigo">Código</label>
                            <input class="text-input medium-input required" type="text" id="codigo" name="codigo" maxlength="20" value="<?php echo $cupom ? $cupom->codigo : '' ?>" />
                        </p>

						<p>
							<label for="valor">Valor do desconto</label>
							<input class="text-input medium-input required" type="text" id="valor" name="valor" maxlength="11" value="<?php echo $cupom ? $cupom->valor : '' ?>" />
						</p>

                        <p>
                            <label for="validade_inicio">Início da validade do cupom</label>
                            <input class="text-input small-input" type="text" id="validade_inicio" name="validade_inicio" maxlength="10" value="<?php echo ($cupom AND ! is_null($cupom->validade_inicio)) ? date('d/m/Y',strtotime($cupom->validade_inicio)) : '' ?>" />
	                        <br /><small>Deixe os campos de data em branco para que o cupom tenha validade indeterminada.</small>
                        </p>

						<p>
							<label for="validade_fim">Fim da validade do cupom</label>
							<input class="text-input small-input" type="text" id="validade_fim" name="validade_fim" maxlength="10" value="<?php echo ($cupom AND ! is_null($cupom->validade_fim)) ? date('d/m/Y',strtotime($cupom->validade_fim)) : '' ?>" />
						</p>

						<p>
							<label for="ativo_1">Pode ser utilizado em compras?</label>
							<input type="radio" name="ativo" id="ativo_1" value="1" <?php echo ($cupom AND $cupom->ativo) ? 'checked="checked"' : '' ?> /> <span class="sim">Sim</span>
							<input type="radio" name="ativo" id="ativo_0" value="0" <?php echo ( ! $cupom OR ! $cupom->ativo) ? 'checked="checked"' : '' ?> /> <span class="nao">Não</span>
						</p>

						<p>
							<input class="button" type="submit" value="<?php echo (is_null($cupom) OR is_null($cupom->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
						</p>

					</fieldset>

					<div class="clear"></div><!-- End .clear -->

				</form>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>