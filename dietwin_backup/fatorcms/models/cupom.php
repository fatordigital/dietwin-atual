<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property int produto_id
 * @property string codigo
 * @property float valor
 * @property string validade_inicio
 * @property string validade_fim
 * @property int ativo
 * @property int log_id
 * @property string log_data
 * @property int cliente_id

 */

class FatorCMS_Model_Cupom extends FatorCMS_Model_Padrao
{
	protected $tabela_nome = 'cupons';

} // end class