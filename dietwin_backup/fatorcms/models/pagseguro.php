<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property int compra_id
 * @property string data
 * @property string identificador
 * @property string referencia
 * @property int transacao_status_id
 * @property int pagamento_tipo_id
 * @property int pagamento_codigo_id
 */

class FatorCMS_Model_PagSeguro extends FatorCMS_Model_Padrao
{
	protected $tabela_nome = 'pagseguro';

} // end class