<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php' ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

		<!-- Page Head -->
		<h2>Lista de Arquivos cadastradas</h2>
		<p id="page-intro">Abaixo estão listados todos os arquivos que são disponibilizados para clientes cadastrado na "Área de Cliente".</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<form action="<?php echo SITE_URL ?>/fatorcms/arquivos/listar" method="get" id="arquivos_buscar_form">
			<fieldset>
				<p>
					<label for="buscar">Digite uma ou mais palavras-chave para buscar por um arquivo cadastrado:</label>
					<input class="text-input medium-input" type="text" id="buscar" name="buscar" maxlength="100" value="<?php echo isset($buscar) ? $buscar : '' ?>" />
					<input class="button" type="submit" value="Buscar" />
				</p>
			</fieldset>
			<div class="clear"></div><!-- End .clear -->
		</form>

		<?php if (isset($buscar) AND ! is_null($buscar)) { ?>
			<p class="buscar-resultado">
				Foram encontrados <strong><?php echo $paginacao->linhas_total ?></strong> resultado(s) para a busca por "<strong><?php echo $buscar ?></strong>".
				<a title="Limpar a busca" href="<?php echo SITE_URL?>/fatorcms/arquivos/listar"><img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier-cross.png" alt="Limpar busca" /> Clique aqui para limpar a busca</a>
			</p>
		<?php } ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Arquivos</h3>

				<input class="arquivo button botao-cadastrar" type="button" value="Cadastrar um novo arquivo" />

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<table>

					<thead>
						<tr>
							<?php echo Funcoes::montar_th_ordenacao_listagem('arquivos', 'listar', $paginacao, $buscar, 'Data', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('arquivos', 'listar', $paginacao, $buscar, 'Título', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('arquivos', 'listar', $paginacao, $buscar, 'Descrição', $ordenar_por, $ordem) ?>
                            <th>Ações</th>
						</tr>
					</thead>

					<tfoot>
						<tr>
							<td colspan="5">

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_quantidades() ?>

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_links() ?>

								<div class="clear"></div>
							</td>
						</tr>
					</tfoot>

					<tbody>
						<?php
                        if($arquivos AND count($arquivos) > 0)
						{
							foreach ($arquivos as $arquivo)
							{
							?>
								<tr>
									<td class="<?php echo $ordenar_por=='data' ? 'current' : '' ?>"><?php echo date('d/m/Y',strtotime($arquivo->data)) ?></td>
									<td class="<?php echo $ordenar_por=='titulo' ? 'current' : '' ?>">
										<a href="<?php echo SITE_URL ?>/fatorcms/arquivos/editar/<?php echo $arquivo->id ?>" title="Editar o arquivo &quot;<?php echo $arquivo->titulo ?>&quot;">
											<?php echo $arquivo->titulo ?>
										</a>
									</td>
									<td class="<?php echo $ordenar_por=='descricao' ? 'current' : '' ?>"><?php echo Funcoes::cortar_texto($arquivo->descricao,130); ?></td>
									<td>
										<!-- Icons -->
										<a href="<?php echo SITE_URL ?>/fatorcms/arquivos/editar/<?php echo $arquivo->id ?>" title="Editar o arquivo &quot;<?php echo $arquivo->titulo ?>&quot;">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" />
										</a>
										<a href="<?php echo SITE_URL ?>/fatorcms/arquivos/excluir/<?php echo $arquivo->id ?>" title="Excluir o arquivo &quot;<?php echo $arquivo->titulo ?>&quot;" class="item-confirmar">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/cross.png" alt="Excluir" />
										</a>
									</td>
								</tr>
							<?php
							}
						}
						else
						{
							echo '<tr><td colspan="4">'.((isset($buscar) AND ! is_null($buscar)) ? 'Nenhum arquivo encontrado para esta busca.' : 'Nenhum arquivo cadastrado até o momento.' ).'</td></tr>';
						}
						?>
					</tbody>

				</table>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>