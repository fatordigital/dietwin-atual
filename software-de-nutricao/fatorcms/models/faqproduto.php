<?php


/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property int faq_id
 * @property int produto_id
 */
class FatorCMS_Model_FaqProduto extends FatorCMS_Model_Padrao
{

    protected $tabela_nome = 'faqs_produtos';

    /****************************** NOVOS MÉTODOS ******************************/

    /**
     * Executa o delete no banco, conforme o os parâmetros salvos no objeto
     * @return bool
     */
    public function delete()
    {
        // "Limpa" os valores
        $this->colunas_mysqli_escape();

        if ( ! is_null($this->id))
        {
            // Exclusão de um registro específico
            $sql = 'DELETE FROM '.$this->tabela_nome.' WHERE id = '.$this->id;
            return $this->query($sql);
        }
        elseif ( ! is_null($this->produto_id))
        {
            // Exclusão de todos os registro de determinado módulo
            $sql = 'DELETE FROM '.$this->tabela_nome.' WHERE produto_id = '.$this->produto_id;
            return $this->query($sql);
        }
        elseif ( ! is_null($this->faq_id))
        {
            // Exclusão de todos os registro de determinada tag
            $sql = 'DELETE FROM '.$this->tabela_nome.' WHERE faq_id = '.$this->faq_id;
            return $this->query($sql);
        }

        // Se chegou aqui, é porque não excluiu nada
        return FALSE;
    }

}
