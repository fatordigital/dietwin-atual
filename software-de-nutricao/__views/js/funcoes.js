// Cufon
Cufon.replace('h1, h2, h3, h4, h5, .titulo-personal, .titulo-rotulo, .titulo-profissional, .menu ul a, .coluna-titulo, ' +
	'.qual-versao td, .qual-versao-autora td, .familia-dietwin a, .titulo-principal, .titulo-resumo, .box-valor .valor, ' +
	'.box-valor .condicoes, .erro .info, .endereco-entrega p, #forma-entrega, .pagamento .valor-total-compra, ' +
	'.comprar .opcoes a, .comprar .total, .comprar .cupon p, .dados .total-compra, .telefone, .subtitulo, ' +
	'.contato .email, .titulo-colorbox');

$(document).ready(function() {

    $(".atualizacoes li:even").css("background-color", "#ffffff");

    $(".qual-versao tr:odd").css("background-color", "#ffffff");
    // Menu no header : click
    /*
    $('a.mais-opcoes').click(function()
    {
        // Clicar em um item que tenha submenu, faz nada
        if ($(this).siblings('.box-menu').length)
        {
            return false;
        }
    });
    */

    $(".close-chat").on('click', function(){
        $.post
        (
            SITE_BASE+"/index/cookie",
            {cookie: true}
        )
    });

    // Drop-down do menu
    var menu_ativo = '';
    $('li.softwares').hover(
        function() // in
        {
            $('a.menu-link').children().attr('src', SITE_BASE + '/views/imagens/menu-softwares-ativo.png');
            $(this).children('.box-menu').stop(true, true).slideDown();
            menu_ativo = true;
        },
        function() // out
        {
            $('a.menu-link').children().attr('src', SITE_BASE + '/views/imagens/menu-softwares.png');
            $(this).children('.box-menu').stop(true, true).slideUp();
            menu_ativo = false;
        }
    );

    $('li.suporte').hover(
        function() // in
        {
            $('a.menu-link_suporte').children().attr('src', SITE_BASE + '/views/imagens/menu-suporte-ativo.png');
            $(this).children('.box-menu_suporte').stop(true, true).slideDown();
            menu_ativo = true;
        },
        function() // out
        {
            $('a.menu-link_suporte').children().attr('src', SITE_BASE + '/views/imagens/menu-suporte.png');
            $(this).children('.box-menu_suporte').stop(true, true).slideUp();
            menu_ativo = false;
        }
    );


    $('.box-menu').hover(
        function() // in
        {
            $('a.menu-link').children().attr('src', SITE_BASE + '/views/imagens/menu-softwares-ativo.png');
            menu_ativo = true;
        },
        function() // out
        {
            if (menu_ativo != true) {
                $('a.menu-link').children().attr('src', SITE_BASE + '/views/imagens/menu-softwares.png');
            }
        }
    );

    //----

	// Versão para impressão
	$('#imprimir').click(function()
	{
		window.print();
		return false;
	});

	//----

    // Colorbox do botão "Enviar para amigo"
    $('#colorbox-enviar-amigo').colorbox({transition:'elastic', fixed:true, inline:true, href:".colorbox-enviar-amigo",
        onComplete:function(){
            $('#cboxTitle').css({'display': 'none'});
            Cufon.refresh()
        }
    });
	
	// Colorbox do botão "Mais Informações"
	$('.link-mais-info').colorbox({transition:'elastic', fixed:true, inline:true, href:".colorbox-mais-info",
		onComplete:function(){
            $('#cboxTitle').css({'display': 'none'});
			Cufon.refresh()
		}
	});

    // Colorbox do botão "Mais Informações sobre um tópico"
    $('.link-info').colorbox({transition:'elastic', fixed:true, inline:true, href:".colorbox-mais-info",
        onComplete:function(){
            $('#cboxTitle').css({'display': 'none'});
            Cufon.refresh()
        }
    });
	
	//----

    // Colorbox das imagens do DIETWIN PROFISSIONAL
    $(".thumbs a[rel='profissional']").colorbox({transition:'elastic', fixed:true, rel:'profissional' });

    // Colorbox das imagens do DIETWIN PERSONAL
    $(".thumbs a[rel='personal']").colorbox({transition:'elastic', fixed:true, rel:'personal' });

    // Colorbox das imagens do DIETWIN RÓTULO DE ALIMENTOS
    $(".thumbs a[rel='rotulo-de-alimentos']").colorbox({transition:'elastic', fixed:true, rel:'rotulo-de-alimentos' });

    // Colorbox das imagens do DIETWIN RÓTULO DE ALIMENTOS
    $(".imagem a[rel='novidade']").colorbox({transition:'elastic', rel:'novidade' });

    //----

    var imagem_ativa = '';
    $(".bubbleInfo").hover(
        function () {
            var imagem_ativa = $(this).attr('id');
            if (imagem_ativa == 'pop10')
            {
                $(this).children().children('img').attr('src', SITE_BASE + '/views/imagens/opcao-profissional-ativo.jpg');
            }
            else if (imagem_ativa == 'pop11')
            {
                $(this).children().children('img').attr('src', SITE_BASE + '/views/imagens/opcao-personal-ativo.jpg');
            }
            else
            {
                $(this).children().children('img').attr('src', SITE_BASE + '/views/imagens/opcao-rotulo-de-alimentos-ativo.jpg');
            }
        },
        function () {
            $('#pop10').children().children('img').attr('src', SITE_BASE + '/views/imagens/opcao-profissional.jpg');
            $('#pop11').children().children('img').attr('src', SITE_BASE + '/views/imagens/opcao-personal.jpg');
            $('#pop12').children().children('img').attr('src', SITE_BASE + '/views/imagens/opcao-rotulo-de-alimentos.jpg');
        }
    );

    //----

    $('#link_boleto').click(function()
    {
        $('.compra-pagseguro').css('display', 'none');
        $('.compra-boleto').css('display', 'block');
        $('#link_pagseguro').children().attr("src",SITE_URL+"/views/imagens/pagamento-pagseguro-off.png");
        $('#link_boleto').children().attr("src",SITE_URL+"/views/imagens/boleto-bancario.png");
    });

    $('#link_pagseguro').click(function()
    {
        $('.compra-boleto').css('display', 'none');
        $('.compra-pagseguro').css('display', 'block');
        $('#link_boleto').children().attr("src",SITE_URL+"/views/imagens/boleto-bancario-off.png");
        $('#link_pagseguro').children().attr("src",SITE_URL+"/views/imagens/pagamento-pagseguro.png");
    });
	
	//----
	
	// Aumentar a fonte do texto
	var fonte_cont = 0;
	$("#aumenta_fonte").click(function()
	{
		if (fonte_cont < 5)
		{
			$("#tamanho-fonte p").each(function()
			{					
				var newSize = parseInt($(this).css('fontSize').replace("px","")) + 1;			
				$(this).css({"font-size" : newSize+"px"});					
			});
			fonte_cont++;
		}
    });
	// Reduz a fonte do texto
	$("#reduz_fonte").click(function()
	{
		if (fonte_cont > -2)
		{
			$("#tamanho-fonte p").each(function()
			{					
				var newSize = parseInt($(this).css('fontSize').replace("px","")) - 1;			
				$(this).css({"font-size" : newSize+"px"});					
			});
			fonte_cont--;
		}
    });

	//-----
	
	// Tooltip
	$('.bubbleInfo').each(function ()
	{
		var referId = $(this).attr('id');
		var info = $('#d'+referId).css('opacity', 0); 
		
		// options
		var distance = 10;
		var time = 250;
		var hideDelay = 100;
	
		var hideDelayTimer = null;
	
		// tracker
		var beingShown = false;
		var shown = false;
		
		var trigger = $('.trigger', this);
		// var trigger = $('#'+referId+' .trigger');
		var popup = $('.popup', this).css('opacity', 0);
	
		// set the mouseover and mouseout on both element
		$([trigger.get(0), popup.get(0)]).mouseover(function () {
		  // stops the hide event if we move from the trigger to the popup element
		  if (hideDelayTimer) clearTimeout(hideDelayTimer);
	
		  // don't trigger the animation again if we're being shown, or already visible
		  if (beingShown || shown) {
			return;
		  } else {
			beingShown = true;
	
			// reset position of popup box
			popup.css({
			  top: -52,
			  left: -13,
			  display: 'block' // brings the popup back in to view
			})
	
			// (we're using chaining on the popup) now animate it's opacity and position
			.animate({
			  top: '-=' + distance + 'px',
			  opacity: 1
			}, time, 'swing', function() {
			  // once the animation is complete, set the tracker variables
			  beingShown = false;
			  shown = true;
			});
		  }
		}).mouseout(function () {
		  // reset the timer if we get fired again - avoids double animations
		  if (hideDelayTimer) clearTimeout(hideDelayTimer);
		  
		  // store the timer so that it can be cleared in the mouseover if required
		  hideDelayTimer = setTimeout(function () {
			hideDelayTimer = null;
			popup.animate({
			  top: '-=' + distance + 'px',
			  opacity: 0
			}, time, 'swing', function () {
			  // once the animate is complete, set the tracker variables
			  shown = false;
			  // hide the popup entirely after the effect (opacity alone doesn't do the job)
			  popup.css('display', 'none');
			});
		  }, hideDelay);
		});
	});

    //-----

    // Hover do botão "Compre Online"

    $('.comprar-personal').hover(
        function() // in
        {
            $(this).children().attr('src', SITE_BASE + '/views/imagens/botao-compre-online-personal-ativo.png');
        },
        function() // out
        {
            if($(this).hasClass('ativo')){}
            else { $(this).children().attr('src', SITE_BASE + '/views/imagens/botao-compre-online-personal.png'); }
        }
    );

    $('.comprar-profissional').hover(
        function() // in
        {
            $(this).children().attr('src', SITE_BASE + '/views/imagens/botao-compre-online-profissional-ativo.png');
        },
        function() // out
        {
            if($(this).hasClass('ativo')){}
            else { $(this).children().attr('src', SITE_BASE + '/views/imagens/botao-compre-online-profissional.png'); }

        }
    );

    $('.comprar-rotulo-de-alimentos').hover(
        function() // in
        {
            $(this).children().attr('src', SITE_BASE + '/views/imagens/botao-compre-online-rotulo-de-alimentos-ativo.png');
        },
        function() // out
        {
            if($(this).hasClass('ativo')){}
            else { $(this).children().attr('src', SITE_BASE + '/views/imagens/botao-compre-online-rotulo-de-alimentos.png'); }
        }
    );

    //-----
				
	// Funções para trocar o "Filtro"
    /*
    $('.filtro-personal').click(function()
    {
        $('#tipo_suporte').removeClass().addClass('personal');
        $('h1').html('Exibindo categorias: <span>Personal</span>');
        Cufon.refresh();
    });

    $('.filtro-profissional').click(function()
    {
        $('#tipo_suporte').removeClass().addClass('profissional');
        $('h1').html('Exibindo categorias: <span>Profissional</span>');
        Cufon.refresh();
    });

    $('.filtro-rotulo-alimentos').click(function()
    {
        $('#tipo_suporte').removeClass().addClass('rotulo-de-alimentos');
        $('h1').html('Exibindo categorias: <span>Rótulo de Alimentos</span>');
        Cufon.refresh();
    });

    $('.filtro-topicos-diversos').click(function()
    {
        $('#tipo_suporte').removeClass().addClass('topicos-diversos');
        $('h1').html('Exibindo categorias: <span>Tópicos Diversos</span>');
        Cufon.refresh();
    });
    */
    //----

    $('.filtro-personal').hover(
        function() // in
        {
            $(this).children().attr('src', SITE_BASE + '/views/imagens/filtro-personal-ativo.png');
        },
        function() // out
        {
            if($(this).hasClass('ativo')){}
            else { $(this).children().attr('src', SITE_BASE + '/views/imagens/filtro-personal.png'); }
        }
    );

    $('.filtro-profissional').hover(
        function() // in
        {
            $(this).children().attr('src', SITE_BASE + '/views/imagens/filtro-profissional-ativo.png');
        },
        function() // out
        {
            if($(this).hasClass('ativo')){}
            else { $(this).children().attr('src', SITE_BASE + '/views/imagens/filtro-profissional.png'); }

        }
    );

    $('.filtro-rotulo-alimentos').hover(
        function() // in
        {
            $(this).children().attr('src', SITE_BASE + '/views/imagens/filtro-rotulo-ativo.png');
        },
        function() // out
        {
            if($(this).hasClass('ativo')){}
            else { $(this).children().attr('src', SITE_BASE + '/views/imagens/filtro-rotulo.png'); }
        }
    );

    $('.filtro-topicos-diversos').hover(
        function() // in
        {
            $(this).children().attr('src', SITE_BASE + '/views/imagens/filtro-topicos-ativo.png');
        },
        function() // out
        {
            if($(this).hasClass('ativo')){}
            else { $(this).children().attr('src', SITE_BASE + '/views/imagens/filtro-topicos.png'); }
        }
    );

    //----

    $("#slider").slider();

    // valores utilizados na função total quando o slider() ainda não inicializou as variáveis
    $("#resultado").val(0);
    $("#valor_slider").val(0);
    $("#aux_slider").val(0);

    $(function() {
        var aux = null;
        var valor = 0;
        var valor_final;
        var valor_licenca;
        if ($('body').hasClass('comprar-profissional'))
        {
            valor_final = 690;
            valor_licenca = 345;
        }
        else if ($('body').hasClass('comprar-personal'))
        {
            valor_final = 395;
            valor_licenca = 198;
        }
        else if ($('body').hasClass('comprar-rotulo-de-alimentos'))
        {
            valor_final = 525;
            valor_licenca = 263;
        }

        if ($('body').hasClass('comprar-plano'))
        {
            valor_final = 99;
            valor_licenca = 49;
        }

        // var x = parseInt(document.getElementById("valor_total").value);
        var select = $( "#contador" );
        var slider = $( "<div id='slider' class='ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all'></div>" ).insertAfter( '.valueLicense' ).slider({
            min: 1,
            max: 10,
            range: "min",
            value: 1,
            slide: function( event, ui ) {
                $('#contador').html(ui.value);
                aux = ui.value;
                if (aux == 1 ) {
                    $('#computers').html("computador");
                    $('.valueLicense').html("(incluso)");

                    if ($('body').hasClass('plano-de-assistencia'))
                    {
                        $('.quantidade').html("<b>Plano para proteção de <span>" + aux + "</span> computador</b>");
                    }
                    else
                    {
                        $('.quantidade').html("<b>Para instalação em <span>" + aux + "</span> computador</b>");
                    }


                    $('#valorTotal').html(valor_final);
                    $("#valorTotal").prettynumber();

                    Cufon.refresh();
                }
                else {
                    $('#computers').html("computadores");

                    valor = (aux - 1) * valor_licenca;

                    $('.valueLicense').html("( + R$ " + valor + ",00 )");

                    if ($('body').hasClass('plano-de-assistencia'))
                    {
                        $('.quantidade').html("<b>Plano para proteção de <span>" + aux + "</span> computadores</b>");
                    }
                    else
                    {
                        $('.quantidade').html("<b>Para instalação em <span>" + aux + "</span> computadores</b>");
                    };

                    var valor_total = valor_final + ((aux - 1) * valor_licenca);

                    $('#valorTotal').html(valor_total);
                    $("#valorTotal").prettynumber();

                    Cufon.refresh();
                }

                $('#input_licencas').val(aux-1);

                if (aux == 1) { $('p.textLicense').css("margin-left", "0px"); $('.valueLicense').css("margin-left", "0px"); }
                if (aux == 2) { $('p.textLicense').css("margin-left", "30px"); $('.valueLicense').css("margin-left", "30px"); }
                if (aux == 3) { $('p.textLicense').css("margin-left", "80px"); $('.valueLicense').css("margin-left", "80px"); }
                if (aux == 4) { $('p.textLicense').css("margin-left", "130px"); $('.valueLicense').css("margin-left", "130px"); }
                if (aux == 5) { $('p.textLicense').css("margin-left", "180px"); $('.valueLicense').css("margin-left", "180px"); }
                if (aux == 6) { $('p.textLicense').css("margin-left", "230px"); $('.valueLicense').css("margin-left", "230px"); }
                if (aux == 7) { $('p.textLicense').css("margin-left", "280px"); $('.valueLicense').css("margin-left", "280px"); }
                if (aux == 8) { $('p.textLicense').css("margin-left", "330px"); $('.valueLicense').css("margin-left", "330px"); }
                if (aux == 9) { $('p.textLicense').css("margin-left", "380px"); $('.valueLicense').css("margin-left", "380px"); }
                if (aux == 10) { $('p.textLicense').css("margin-left", "400px"); $('.valueLicense').css("margin-left", "400px"); }

            }

        });
    });

    $('.suporte .info').hide();
    // Exibir/Ocultar FAQ's
    $('.perguntas h3 a').click(function(e)
    {
        e.preventDefault();
        var item = $(this).attr('id');

        if ($(this).hasClass('active'))
        {
            $(this).removeClass('active');
            $(this).parent().siblings('.info').slideToggle();
        }
        else
        {
            $('.perguntas h3 a').each(function()
            {
                if ($(this).hasClass('active') && $(this).attr('id') != ('#' + item))
                {
                    $(this).removeClass('active');
                    $(this).parent().siblings('.info').slideToggle();
                }
            });

            if ($('#' + item).hasClass(''))
            {
                $('#' + item).addClass('active');
                $('.' + item).slideToggle();
            }
        }

    });

	//----

    // Habilita as opções exclusivas para "Pessoa Física" no formulário de "Cadastro"
    $('#pessoa_fisica').click(function()
    {
        $('.pessoa-juridica').css('display', 'none');
        $('.pessoa-fisica').css('display', 'block');
    });

    // Habilita as opções exclusivas para "Pessoa Jurídica" no formulário de "Cadastro"
    $('#pessoa_juridica').click(function()
    {
        $('.pessoa-fisica').css('display', 'none');
        $('.pessoa-juridica').css('display', 'block');
    });

    //----

    if ($('#form-cadastro').length)
    {
        $("#telefone_principal").mask('(99) 9999-9999?9', {placeholder:' '});
        $('#telefone_extra').mask('(99) 9999-9999?9', {placeholder:' '});
        var array_codigo_cidades_ddd_11 = ["4662", "4679","4693","4696","4693","4713","4723","4728","4735","4744","4750","4752","4758","4770","4794","4802","4817","4818","4831","4838","4858","4862","4863","4881","4905","4908","4915","4918","4923","4924","4934","4936","4939","4944","4947","4970","4971","4975","5008","5014","5017","5038","5044","5065","5080","5083","5088","5083","5095","5137","5144","5155","5158","5159","5160","5174","5183","5185","5195","5196","5209","5213","5216","5236","5239","5266","5282","5283","5284","5285"];
         $("#cidade").change(function(){
             $("#telefone_principal").val('');
             $("#telefone_extra").val('');
             var cidade = $("#cidade").val();
             var estado = $("#estado").val();
             if(estado == '25'){
                 for(i = 0; i <= 70; i++){
                     if(cidade == array_codigo_cidades_ddd_11[i]){
                         $("#telefone_principal").mask('(11)'+'9999-9999?9', {placeholder:' '});
                         $("#telefone_extra").mask('(11)'+'9999-9999?9', {placeholder:' '});
                         break;
                     }else{
                         $("#telefone_principal").mask('(99) 9999-9999', {placeholder:' '});
                         $('#telefone_extra').mask('(99) 9999-9999', {placeholder:' '});
                     }
                 }
             }else{
                 $("#telefone_principal").mask('(99) 9999-9999', {placeholder:' '});
                 $('#telefone_extra').mask('(99) 9999-9999', {placeholder:' '});
             }
         });


        $('#cpf, #cpf_responsavel').mask('999.999.999-99', {placeholder:' '});
        $('#cnpj').mask('99.999.999/9999-99', {placeholder:' '});
        $('#rg, #rg_responsavel').mask('9?999999999', {placeholder:''});
        $('#numero, #numero_entrega').mask('9?999999', {placeholder:''});
        $('#cep, #cep_entrega').mask('99999-999', {placeholder:' '});


        $('#form-cadastro').submit(function(e)
        {
            var nome = $('#nome').val();
            var email = $('#email').val();
            var senha = $('#senha').val();
            var senha_obrigatoria = $('#senha').hasClass('obrigatorio');
            var telefone_principal = $('#telefone_principal').val();
            //var telefone_extra = $('#telefone_extra').val();

            // Campos para Pessoa Física
            var rg = $('#rg').val();
            var cpf = $('#cpf').val();

            // Campos para Pessoa Jurídica
            var cnpj = $('#cnpj').val();
            var responsavel = $('#nome_responsavel').val();
            var rg_responsavel = $('#rg_responsavel').val();
            var cpf_responsavel = $('#cpf_responsavel').val();

            $('#form-cadastro input, #form-cadastro textarea').removeClass('input-notificacao-erro');

            if (nome != '' && email !='' && ((senha_obrigatoria && senha !='') || ( ! senha_obrigatoria)) && telefone_principal !='' && ((rg !='' && cpf !='') || (cnpj !='' && responsavel !='' && rg_responsavel !='' && cpf_responsavel !='')))
            {
                if(validar_email(email))
                {
                    // Limpa as mensagens
                    $('#form_notification').html('Aguarde...').addClass('informacao');
                    return true;

                }
                else
                {
                    //$('#mais_info_notification').html('<div class="erro">E-mail inválido.</div>');
                    $('#form_notification').removeClass('sucesso erro informacao').addClass('erro').html('Email inválido.');
                    $('#email').addClass('input-notificacao-erro');
                    return false;
                }
            }
            else
            {
                //$('#mais_info_notification').html('<div class="erro">Todos os campos marcados são obrigatórios.</div>');
                $('#form_notification').removeClass('sucesso erro informacao').addClass('erro').html('Todos os campos marcados são obrigatórios.');
                // Marcamos os campos inválidos
                $('#form-cadastro input, #form-cadastro textarea').each(function()
                {
                    // Testamos se o value do input está vazio
                    if( ! $(this).val() && $(this).hasClass('obrigatorio'))
                    {
                        $(this).addClass('input-notificacao-erro');
                    }
                });
                return false;
            }
        });

        $('#checkbox-endereco-entrega').change(function(){
            if (this.checked)
            {
                $('#endereco_entrega, #numero_entrega, #complemento_entrega, ' +
                    '#bairro_entrega, #cep_entrega, #observacao_entrega').attr('disabled', 'disabled');
                $('#estado_entrega, #cidade_entrega').attr('disabled','disabled');

                $('#endereco_entrega').val($('#endereco').val());
                $('#numero_entrega').val($('#numero').val());
                $('#complemento_entrega').val($('#complemento').val());
                $('#cep_entrega').val($('#cep').val());
                $('#bairro_entrega').val($('#bairro').val());
                $('#observacao_entrega').val($('#observacao').val());
                $('#estado_entrega').val($('#estado').val());
                $('#cidade_entrega').val($('#cidade').val());

                carrega_cidades($('#estado_entrega'));
                

                setTimeout(function(){
                    $('#cidade_entrega').val($('#cidade').val());
                },100)
                $('#cidade_entrega').val($('#cidade').val());

            }
            else
            {
                $('#endereco_entrega, #numero_entrega, #complemento_entrega, ' +
                    '#cep_entrega, #bairro_entrega, #observacao_entrega').removeAttr('disabled').val('');
                $('#estado_entrega, #cidade_entrega').removeAttr('disabled');
                $('#estado_entrega').val(1);
                carrega_cidades($('#estado_entrega'));

            }
        });

        $('#email').blur(function(e){
            var email = $('#email').val();

            if (email != '')
            {
                $.get(
                    SITE_URL+'/cadastro-cliente/verificar-email',
                    {
                        email: email
                    },
                    function(data)
                    {
                        if (data.status == 'erro')
                        {
                            //alert('dsaydas')
                            $('<span>'+data.mensagem+'</span>').appendTo($('#email').parent());
                        }
                    },
                    'json'
                );
            }
        });
    }


    //----

    if ($('#form-senha').length)
    {
        $('#form-senha').submit(function(e)
        {
            e.preventDefault();

            var email = $('#email').val();

            $('#form-senha input, #form-senha textarea').removeClass('input-notificacao-erro');

            if (email !='')
            {
                if(validar_email(email))
                {
                    // Limpa as mensagens
                    $('#form_notification').html('Aguarde...').addClass('informacao');

                    $.post(
                        $('#form-senha').attr('action'),
                        $('#form-senha').serialize(), // Transforma os campos do form em parâmetros,
                        function(json)
                        {
                            var mensagem = json.mensagem;
                            var tipo = json.tipo;

                            $('#form_notification').removeClass('sucesso erro informacao').addClass(tipo).html(mensagem);

                            if (tipo == 'sucesso')
                            {
                                $('.formulario p').fadeOut('fast');
                                $('#form-senha').fadeOut('fast');
                            }
                        },
                        'json'
                    );
                }
                else
                {
                    //$('#mais_info_notification').html('<div class="erro">E-mail inválido.</div>');
                    $('#form_notification').removeClass('sucesso erro informacao').addClass('erro').html('Email inválido.');
                    $('#email').addClass('input-notificacao-erro');
                }
            }
            else
            {
                //$('#mais_info_notification').html('<div class="erro">Todos os campos marcados são obrigatórios.</div>');
                $('#form_notification').removeClass('sucesso erro informacao').addClass('erro').html('Digite abaixo seu e-mail cadastrado na dietWin.');
                // Marcamos os campos inválidos
                $('#form-senha input, #form-senha textarea').each(function()
                {
                    // Testamos se o value do input está vazio
                    if( ! $(this).val())
                    {
                        $(this).addClass('input-notificacao-erro');
                    }
                });
            }
        });
    }

    //----

	if ($('#form-contato').length)
	{
        $('#telefone').mask('(99) 9999-9999', {placeholder:' '});

		$('#form-contato').submit(function(e)
		{
			e.preventDefault();

			$('#form-contato button').addClass('enviando').attr('disabled', 'disabled');

			var nome = $('#nome').val();
			var email = $('#email').val();
            var cidade = $('#cidade').val();
			var telefone = $('#telefone').val();
            var assunto = $('#assunto').val();
			var mensagem = $('#mensagem').val();

            var sim = $('#sim:checked').length;
            if (sim == 1) { sim = 'Sim'; } else { sim = ''; }

            var nao = $('#nao:checked').length;
            if (nao == 1) { nao = 'Não'; } else { nao = ''; }

			$('#form-contato input, #form-contato textarea').removeClass('input-notificacao-erro');

			if (nome != '' &&  email !='' &&  cidade !='' && telefone !='' &&  assunto !='' && mensagem !='' && (sim != '' || nao != ''))
			{
				if(validar_email(email))
				{
					// Limpa as mensagens
					$('#form_notification').html('Aguarde...').addClass('informacao');

					$.post(
						$('#form-contato').attr('action'),
						$('#form-contato').serialize(), // Transforma os campos do form em parâmetros,
						function(json)
						{
							var mensagem = json.mensagem;
							var tipo = json.tipo;

							$('#form_notification').removeClass('sucesso erro informacao').addClass(tipo).html(mensagem);

							$('#form-contato button').removeClass('enviando').removeAttr('disabled');

							if (tipo == 'sucesso')
							{
                                $('.formulario p').fadeOut('fast');
                                $('#form-contato').fadeOut('fast');
                                $('#form_notification').append(adwords_codigo);
								$('#nome, #email, #telefone, #cidade, #assunto, #mensagem').val('');
							}
						},
						'json'
					);
				}
				else
				{
					//$('#mais_info_notification').html('<div class="erro">E-mail inválido.</div>');
					$('#form_notification').removeClass('sucesso erro informacao').addClass('erro').html('Email inválido.');
					$('#email').addClass('input-notificacao-erro');
					$('#form-contato button').removeClass('enviando').removeAttr('disabled');;
				}
			}
			else
			{
				//$('#mais_info_notification').html('<div class="erro">Todos os campos marcados são obrigatórios.</div>');
				$('#form_notification').removeClass('sucesso erro informacao').addClass('erro').html('Todos os campos são obrigatórios.');
				// Marcamos os campos inválidos
				$('#form_notification input, #form_notification textarea').each(function()
				{
					// Testamos se o value do input está vazio
					if( ! $(this).val())
					{
						$(this).addClass('input-notificacao-erro');
					}
				});
				$('#form-contato button').removeClass('enviando').removeAttr('disabled');;
			}
		});
	}

    /*if ($('#form-orcamento').length)
    {
        $('#telefone').mask('(99) 9999-9999', {placeholder:' '});

        $('#form-orcamento').submit(function(e)
        {

            $('#form-contato button').addClass('enviando').attr('disabled', 'disabled');

            var nome = $('#nome').val();
            var nome = $('#cpf_cnpj').val();
            var email = $('#email').val();
            var cidade = $('#cidade').val();
            var telefone = $('#telefone').val();
            var celular = $('#celular').val();
            var endereco = $('#endereco').val();
            var estado = $('#estado').val();
            var cep = $('#cep').val();


            var sim = $('#sim:checked').length;
            if (sim == 1) { sim = 'Sim'; } else { sim = ''; }

            var nao = $('#nao:checked').length;
            if (nao == 1) { nao = 'Não'; } else { nao = ''; }

            $('#form-contato input, #form-contato textarea').removeClass('input-notificacao-erro');

            if (nome != '' &&  email !='' &&  cidade !='' && telefone !='' &&  celular !='' && endereco !='' && estado !='' && cep !='' )
            {

            }
            else
            {
                //$('#mais_info_notification').html('<div class="erro">E-mail inválido.</div>');
                $('#form_notification').removeClass('sucesso erro informacao').addClass('erro').html('Email inválido.');
                $('#email').addClass('input-notificacao-erro');
                $('#form-contato button').removeClass('enviando').removeAttr('disabled');;
            }

        });
    }*/

	//----
    
	if ($('#form-enviar-amigo').length)
	{
		$('#form-enviar-amigo').submit(function(e)
		{
			e.preventDefault();

			var nome_amigo = $('#nome_amigo').val();
			var email_amigo = $('#email_amigo').val();
			var seu_nome = $('#seu_nome').val();
			var seu_email = $('#seu_email').val();

			$('#form-enviar-amigo input').removeClass('input-notificacao-erro');

			if (nome_amigo != '' &&  email_amigo !='' && seu_nome !='' && seu_email !='')
			{
				if(validar_email(email_amigo))
				{
					// Limpa as mensagens
					$('#enviar_amigo_notification').html('<div class="informacao">Aguarde...</div>');

					$.post(
						$('#form-enviar-amigo').attr('action'),
						$('#form-enviar-amigo').serialize(), // Transforma os campos do form em parâmetros,
						function(json)
						{
							var mensagem = json.mensagem;
							var tipo = json.tipo;

							$('#enviar_amigo_notification').removeClass('sucesso erro informacao').addClass(tipo).html(mensagem);

							if (tipo == 'sucesso')
							{
                                $('#form-enviar-amigo').html('');
                                $('.conteudo-colorbox p').html('');
                                $('#enviar_amigo_notification').append(adwords_codigo);
							}
						},
						'json'
					);
				}
				else
				{
					//$('#mais_info_notification').html('<div class="erro">E-mail inválido.</div>');
					$('#enviar_amigo_notification').removeClass('sucesso erro informacao').addClass('erro').html('Email inválido.');
					$('#email_amigo').addClass('input-notificacao-erro');
				}
			}
			else
			{
				//$('#mais_info_notification').html('<div class="erro">Todos os campos marcados são obrigatórios.</div>');
				$('#enviar_amigo_notification').removeClass('sucesso erro informacao').addClass('erro').html('Todos os campos são obrigatórios.');
				// Marcamos os campos inválidos
				$('#form-enviar-amigo input').each(function()
				{
					// Testamos se o value do input está vazio
					if( ! $(this).val())
					{
						$(this).addClass('input-notificacao-erro');
					}
				});
			}
		});
	}

    //----

    if ($('#form-mais-info').length)
    {

        $('#info_telefone').mask('(99) 9999-9999', {placeholder:' '});

        $('#form-mais-info').submit(function(e)
        {
            e.preventDefault();
            
            var info_nome = $('#info_nome').val();
            var info_email = $('#info_email').val();
            var info_cidade = $('#info_cidade').val();
            var info_telefone = $('#info_telefone').val();
            var info_mensagem = $('#info_mensagem').val();
            var captcha_origin = $('#txtCaptcha').val();
            var captcha_verify = $('#txtInput').val();
            

            $('#form-mais-info input, #form-mais-info textarea').removeClass('input-notificacao-erro');

            if (info_nome != '' &&  info_email !='' &&  info_cidade !='' && info_telefone !='' && info_mensagem !='' && captcha_origin==captcha_verify)
            {
                if(validar_email(info_email))
                {
                    // Limpa as mensagens
                    $('#form_notification').html('Aguarde...').addClass('informacao');

                    $.post(
                        $('#form-mais-info').attr('action'),
                        $('#form-mais-info').serialize(), // Transforma os campos do form em parâmetros,
                        function(json)
                        {
                            var mensagem = json.mensagem;
                            var tipo = json.tipo;

                            $('#form_notification').removeClass('sucesso erro informacao').addClass(tipo).html(mensagem);

                            if (tipo == 'sucesso')
                            {
                                $('.conteudo-colorbox p').fadeOut('fast');
                                $('#form-mais-info').fadeOut('fast');
                                $('#form_notification').append(adwords_codigo);
                                $('#info_nome, #info_email, #info_telefone, #info_cidade, #info_mensagem').val('');
                            }
                        },
                        'json'
                    );
                }
                else
                {
                    //$('#mais_info_notification').html('<div class="erro">E-mail inválido.</div>');
                    $('#form_notification').removeClass('sucesso erro informacao').addClass('erro').html('Email inválido.');
                    $('#info_email').addClass('input-notificacao-erro');
                }
            }
            else
            {
                //$('#mais_info_notification').html('<div class="erro">Todos os campos marcados são obrigatórios.</div>');
                $('#form_notification').removeClass('sucesso erro informacao').addClass('erro').html('Todos os campos são obrigatórios.');
                // Marcamos os campos inválidos
                $('#form-mais-info input, #form-mais-info textarea').each(function()
                {
                    // Testamos se o value do input está vazio
                    if( ! $(this).val())
                    {
                        $(this).addClass('input-notificacao-erro');
                    }
                });
            }
        });
    }
    if ($('#atualizar_cupon').length > 0) {
    //Faz a busca do cupom
    $('#atualizar_cupon').submit(function(e){
        //alert(SITE_URL+'/software/'+$('body').attr('class')+'/comprar/cupom')
        e.preventDefault();
        var codigo = $('#cupon').val();
        if (codigo != null || codigo.length > 0)
        {
            //alert(SITE_URL+'/software/'+$('body').attr('class')+'/comprar/cupom')
            $.post(
                SITE_URL+'/software/'+PRODUTO+'/comprar/cupom',
                {codigo: codigo},
                function (data)
                {
                    $('.cupon .mensagem').html('');
                    $('<p></p>').attr('class',data.tipo).text(data.mensagem).appendTo('.cupon .mensagem');
                },
                'json'
            );
        }
    });
    }

    if ($('#estado').length > 0) {
        $('#estado, #estado_entrega').change(function(){
            carrega_cidades($(this));
        });
    }
    if ($('#estado').length > 0) {
        // -------
        //carrega_cidades($('#estado'));
        //carrega_cidades($('#estado_entrega'));
    }

    if ($('#forma-entrega').length > 0)
    {
        $('#download, #pac, #sedex').click(function(){
            if ($(this).attr('id') == 'sedex')
            {
                $('#valor-total-frete').html('');
                var valor = parseFloat($('#input-valor-compra').val())+parseFloat($('#input-valor-frete').val());
                $('#valor-total-frete').html(formatar_numero(parseFloat($('#input-valor-frete').val()),2,'.',',','R$ ','','',''));
                $('#span-total-compra').html(formatar_numero(parseFloat($('#input-valor-compra').val())+parseFloat($('#input-valor-frete').val()),2,'.',',','R$ ','','',''));
                $('#forma-entrega-pagseguro').val('sedex');
                $('#forma-entrega-boleto').val('sedex');

            }else if($(this).attr('id') == 'pac'){
                $('#valor-total-frete').html('');
                var valor = parseFloat($('#input-valor-compra').val())+parseFloat($('#input-valor-frete').val());
                $('#valor-total-frete').html(formatar_numero(parseFloat(12),2,'.',',','R$ ','','',''));
                $('#span-total-compra').html(formatar_numero(parseFloat($('#input-valor-compra').val())+parseFloat(12),2,'.',',','R$ ','','',''));
                $('#forma-entrega-pagseguro').val('pac');
                $('#forma-entrega-boleto').val('pac');
            }
            else if($(this).attr('id') == 'download'){
                var valor = $('#input-valor-compra').val();
                $('#valor-total-frete').html('');
                $('#valor-total-frete').html('Gratuito');
                $('#span-total-compra').html(formatar_numero(valor,2,'.',',','R$ ','','',''));
                $('#forma-entrega-pagseguro').val('download');
                $('#forma-entrega-boleto').val('download');
            }
            Cufon.refresh();
        });
    }

    if ($('#form-login').length > 0)
    {
        $('#form-login-enviar').click(function(e){
            e.preventDefault();
            if ($('#email').val() != '' && $('#senha').val() != '')
            {
                $.post(
                    SITE_URL+'/login',
                    {email:$('#email').val(),senha:$('#senha').val()},
                    function(data){
                        if (data.tipo == 'sucesso')
                        {
                            window.location = SITE_URL+'/software/'+PRODUTO+'/pagamento';
                        }
                        else
                        {
                            if ($('div.notificacao').length > 0)
                            {
                                $('div.notificacao').text(data.mensagem);
                            }
                            else
                            {
                                $('<div></div>').attr({'class':'notificacao erro'}).text(data.mensagem).appendTo('#form-login');
                            }

                        }
                    },
                    'json'
                );
            }
            else
            {
                $('<div></div>').attr({'class':'notificacao erro'}).text('Preencha todos os campos.').appendTo('#form-login');
            }
        });
    }

    var chat_desabilitado = true;
    var chat_criado = false;
    setInterval(function(){
        var source = $('#webchat_widget img, div.chat2 img').attr('src');
        if (source !== undefined && source.indexOf('ONLINE') !== -1)
        {
            //console.log('chegou aqui');
            var data = new Date();
            //console.log(data.getHours()+':'+data.getMinutes()+':'+data.getSeconds());
            var chat_cookie = RecuperarCookie('chat_desabilitado');
            if (chat_cookie != '')
            {
                //console.log('Vc ja existe');
            }
            else
            {
                if (!chat_criado)
                {
                    $('#div-chat').slideDown(2000);
                    chat_criado = true;
                    chat_desabilitado = false;
                    //console.log('DIV apareceu')
                }
            }
        }


    },45000);

    $('#div-chat a').click(function(e){
        ArmazenarCookie('chat_desabilitado','1');
        chat_criado = false;
        chat_desabilitado = true;
        $(this).parent().slideUp(2000);
        //console.log('DIV removida');
    });

    if ($('#form-topo-login').length > 0)
    {
        $('#form-topo-login-enviar').click(function(e){

            var action = $('#form-topo-login').attr('action');

            e.preventDefault();
            if ($('#email-login').val() != '' && $('#senha-login').val() != '')
            {
                $.post(
                    SITE_URL+'/login',
                    {email:$('#email-login').val(),senha:$('#senha-login').val()},
                    function(data){
                        if (data.tipo == 'sucesso')
                        {
                            window.location = action;
                        }
                        else
                        {
                            $('#form-topo-notification').attr({'class':'notificacao erro'}).text(data.mensagem);
                        }
                    },
                    'json'
                );
            }
            else
            {
                $('#form-topo-notification').attr({'class':'notificacao erro'}).text(data.mensagem);
            }
        });

        $('#email-login').focus(function(){
         if ($(this).val() == 'E-mail')
         {
         $(this).val('')
         }
        }).blur(function(){
            if ($(this).val() == '')
            {
                $(this).val('E-mail')
            }
        });

        $('#senha-login').focus(function(){
         if ($(this).val() == 'Senha')
         {
         $(this).val('')
         }
         }).blur(function(){
         if ($(this).val() == '')
         {
         $(this).val('Senha')
         }
         });
    }

    $('.close-chat').click(function(){
        $(this).fadeOut(1000);
    });


    /* ENVIO DO FORMULÁRIO DE ORÇAMENTO VIA AJAX */

    $('form#form-orcamento').submit(function(){
        var dados = $(this).serialize();
        var _this = $(this);
        $.ajax({
            type: "POST",
            url: "/enviar-contato",
            data: dados,
            success: function(data)
            {
                if(data == 1){
                    $('form#form-orcamento input[type="text"], form#form-orcamento textarea').val("");
                    $('form#form-orcamento .aviso-envio p').remove();
                    $('form#form-orcamento .aviso-envio').fadeIn().append('<p class="success">Enviado com sucesso.</p>');
                }
                else{
                    $('form#form-orcamento .aviso-envio p').remove();
                    $('form#form-orcamento .aviso-envio').fadeIn().append('<p class="error">*Preencha ou corrija os campos com a borda vermelha.</p>');
                }
            }
        });
        return false;
    });


});

function carrega_cidades ($obj) {
    $.post(SITE_URL+'/cadastro-cliente/cidades-estado',
        {estado:$obj.val()},
        function(data)
        {
            var destino = ($obj.attr('id') == 'estado') ? 'cidade' : 'cidade_entrega';
            $('#'+destino).html('');
            $.each(data,function(i,cidade){
                $('#'+destino).append($('<option></option>').attr('value', cidade.id).text(cidade.nome));
            });
        },
        'json'
    )
}

var adwords_codigo = '<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1059574202/?value=0&amp;label=AoglCP6irAIQuqOf-QM&amp;guid=ON&amp;script=0 "/>';

//----------

// função de validação de email
function validar_email(email)
{
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(email);
}

// number formatting function
// copyright Stephen Chapman 24th March 2006, 10th February 2007
// permission to use this function is granted provided
// that this copyright notice is retained intact
//http://ntt.cc/2008/04/25/6-very-basic-but-very-useful-javascript-number-format-functions-for-web-developers.html
function formatar_numero(num, dec, thou, pnt, curr1, curr2, n1, n2)
{
    var x = Math.round(num * Math.pow(10,dec));
    if (x >= 0) n1=n2='';

    var y = (''+Math.abs(x)).split('');
    var z = y.length - dec;

    if (z<0) z--;

    for(var i = z; i < 0; i++)
        y.unshift('0');

    y.splice(z, 0, pnt);
    if(y[0] == pnt) y.unshift('0');

    while (z > 3)
    {
        z-=3;
        y.splice(z,0,thou);
    }

    var r = curr1+n1+y.join('')+n2+curr2;
    return r;
}

function ArmazenarCookie (Identificador, Valor)
{
    var DataHora;

    DataHora       = new Date ();
    DataHora.setMinutes(DataHora.getMinutes()+10);
    document.cookie= Identificador + '=' + Valor + '; expires=' + DataHora.toGMTString () + '; path=/';
}

function RecuperarCookie (Identificador)

{
    var Caracter, Frase, I, Identificado, N;

    I            = 0;
    N            = document.cookie.length;
    Frase        = '';
    Identificado = false;

    while (I < N) {
        Caracter= document.cookie.charAt (I);
        if (Caracter == ';') {
            if (Identificado)
                break;
            else
                Frase= '';
        }
        else if (Caracter == '=') {
            if (Frase == Identificador)
                Identificado= true;
            Frase= '';
        }
        else if ((Caracter != ' ') || Identificado)
            Frase= Frase + Caracter;
        I= I + 1;
    }

    if (Identificado)
        return Frase;
    else
        return '';
}

function Listar ()
{
    var DataHora;

    DataHora= new Date ();

    ArmazenarCookie ('ExemploCookie_Data', DataHora.toLocaleDateString ());
    ArmazenarCookie ('ExemploCookie_Hora', DataHora.toLocaleTimeString ());
    ArmazenarCookie ('ExemploCookie_Dindin', '$DiN DiN$');

    Paragrafo.innerHTML+= 'ExemploCookie_Data: ' + RecuperarCookie ('ExemploCookie_Data') + '<br>';
    Paragrafo.innerHTML+= 'ExemploCookie_Hora: ' + RecuperarCookie ('ExemploCookie_Hora') + '<br>';
    Paragrafo.innerHTML+= 'ExemploCookie_Dindin: ' + RecuperarCookie ('ExemploCookie_Dindin') + '<br>';
}

function DrawCaptcha()
{
    var a = Math.ceil(Math.random() * 10)+ '';
    var b = Math.ceil(Math.random() * 10)+ '';       
    var c = Math.ceil(Math.random() * 10)+ '';  
    var d = Math.ceil(Math.random() * 10)+ '';   
    var code = a + b + c + d;
    
    document.getElementById("txtCaptcha").value = code;
    document.getElementById("dazuera").value = code
    
    document.getElementById("txtCaptchaBox").value = code;
    document.getElementById("dazueraBox").value = code
}

function ValidCaptcha(){
    var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
    var str2 = removeSpaces(document.getElementById('txtInput').value);
    
    var str3 = removeSpaces(document.getElementById('txtCaptchaBox').value);
    var str4 = removeSpaces(document.getElementById('txtInputBox').value);
    
    if (str1 == str2 || str3 == str4){ 
      return true;
    }else{        
      return false
    };   
}

function removeSpaces(string)
{
    return string.split(' ').join('');
}