<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Profissional extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        $faq = new Model_FAQ();
        $faqs = $faq->select("
            SELECT {tabela_nome}.id as 'id',{tabela_nome}.pergunta as 'pergunta',{tabela_nome}.pergunta_seo as 'pergunta_seo'
            FROM fd_faqs
            JOIN fd_faqs_produtos ON {tabela_nome}.id = fd_faqs_produtos.faq_id
            /*WHERE fd_faqs_produtos.produto_id=1*/
            WHERE fd_faqs_produtos.produto_id=5
            ORDER BY id DESC LIMIT 0,5
        ",TRUE);

        $video = new Model_Video();
        //$videos = $video->select('SELECT * FROM {tabela_nome} WHERE produto_id=1 ORDER BY data DESC LIMIT 0,9',TRUE);
        $videos = $video->select('SELECT * FROM {tabela_nome} WHERE produto_id=5 ORDER BY data DESC LIMIT 0,9',TRUE);

        $galeria = new Model_Galeria();
        //$galeria = $galeria->select('SELECT * FROM {tabela_nome} WHERE produto_id=1');
        $galeria = $galeria->select('SELECT * FROM {tabela_nome} WHERE produto_id=5');
        $fotos = array();
        if (isset($galeria) AND $galeria)
        {
            $foto = new Model_GaleriaFoto();
            $fotos = $foto->select('SELECT * FROM {tabela_nome} WHERE galeria_id='.$galeria->id.' ORDER BY id LIMIT 0,4',TRUE);
        }

        $view = new View('profissional.php');
		$this->view_variaveis_obrigatorias($view);

        $view->adicionar('faqs',$faqs);
        $view->adicionar('videos',$videos);
        $view->adicionar('fotos', $fotos);

		$view->adicionar('body_class', 'profissional');
		$view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Profissional - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */

    /**
     * Passa as informações para a montagem e envio do email a partir do formulário "Mais Informações"
     * @param $parametros
     * @return void
     */
    public function mais_info($parametros)
    {
        $info_nome = isset($parametros->info_nome) ? trim($parametros->info_nome) : NULL;
        $info_telefone = isset($parametros->info_telefone) ? trim($parametros->info_telefone) : NULL;
        $info_email = isset($parametros->info_email) ? trim($parametros->info_email) : NULL;
        $info_cidade = isset($parametros->info_cidade) ? trim($parametros->info_cidade) : NULL;
        $info_mensagem = isset($parametros->info_mensagem) ? trim($parametros->info_mensagem) : NULL;

        //$produto = new Model_Produto(1);
        $produto = new Model_Produto(5);

        if ( ! empty($info_nome) AND ! empty($info_email) AND ! empty($info_mensagem) AND ! empty($info_telefone) AND ! empty($info_cidade))
        {
            $mail = new Controller_Email;

            if ($mail->mais_info($parametros, $produto))
            {
	            if (SITE_LOCAL == 'fdserver') {
		            //$Metriks_CID = 0; $Metriks_FID = 0;
	            } else {
		            $Metriks_CID = 2; $Metriks_FID = 13;
	            }
	            $_POST['info_produto'] = $produto->nome;
	            @include 'biblioteca/metriks-enviar-conversao.php';

                echo json_encode(array('tipo'=>'sucesso', 'mensagem'=>'Sua mensagem foi enviada com sucesso!'));
            }
            else
            {
                echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Ocorreu um erro ao enviar sua mensagem.'));
            }
        }
        else
        {
            echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Ocorreu um erro ao receber suas informações.'));
        }
    }

} // end class