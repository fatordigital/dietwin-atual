<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

header ("content-type: text/xml");
echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<url>
    <loc>http://www.dietwin.com.br/</loc>
    <changefreq>monthly</changefreq>
    <priority>0.1</priority>
</url>
<url>
    <loc>http://www.dietwin.com.br/contato</loc>
    <changefreq>hourly</changefreq>
    <priority>1.0</priority>
</url>
<url>
    <loc>http://www.dietwin.com.br/faq</loc>
    <changefreq>hourly</changefreq>
    <priority>1.0</priority>
</url>
<url>
    <loc>http://www.dietwin.com.br/suporte/personal/faq</loc>
    <changefreq>daily</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>http://www.dietwin.com.br/suporte/profissional/faq</loc>
    <changefreq>daily</changefreq>
    <priority>0.5</priority>
</url>
<url>
    <loc>http://www.dietwin.com.br/suporte/rotulo-de-alimentos/faq</loc>
    <changefreq>daily</changefreq>
    <priority>0.5</priority>
</url>
	<?php
	if ($faq_rotulo AND count($faq_rotulo) > 0)
	{
		foreach($faq_rotulo as $faq)
		{
			echo '<url>';
				echo '<loc>'.SITE_URL.'/suporte/rotulo-de-alimentos/faq/'.$faq->get_pergunta_seo().'</loc>';
				echo '<changefreq>monthly</changefreq>';
				echo '<priority>0.1</priority>';
			echo '</url>'.PHP_EOL;
		};
	}

	//----------------------------------------------------------------------
	?>

    <url>
        <loc>http://www.dietwin.com.br/index</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/institucional</loc>
        <changefreq>hourly</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/novidade/</loc>
        <changefreq>daily</changefreq>
        <priority>0.4</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/novidades</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/personal</loc>
        <changefreq>hourly</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/profissional</loc>
        <changefreq>hourly</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/qual-versao-escolho</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/rotulo-de-alimentos</loc>
        <changefreq>hourly</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/software/personal/comprar</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/software/personal/galeria-foto</loc>
        <changefreq>weekly</changefreq>
        <priority>0.3</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/software/personal/videos</loc>
        <changefreq>daily</changefreq>
        <priority>0.4</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/software/profissional/comprar</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/software/profissional/videos</loc>
        <changefreq>daily</changefreq>
        <priority>0.4</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/software/rotulo-de-alimentos/comprar</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/software/rotulo-de-alimentos/videos</loc>
        <changefreq>daily</changefreq>
        <priority>0.4</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/softwares</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>



    <url>
        <loc>http://www.dietwin.com.br/suporte/personal/videos</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>


    <url>
        <loc>http://www.dietwin.com.br/suporte/profissional/videos</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>

    <url>
        <loc>http://www.dietwin.com.br/suporte/rotulo-de-alimentos/videos</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/suporte/topicos-diversos/faq</loc>
        <changefreq>daily</changefreq>
        <priority>0.4</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/suporte/topicos-diversos/videos</loc>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc>http://www.dietwin.com.br/videos</loc>
        <changefreq>daily</changefreq>
        <priority>0.9</priority>
    </url>

</urlset>
