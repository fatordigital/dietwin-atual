<?php require_once 'includes/head.php' ?>
<body>
<?php require_once 'includes/nav.php' ?>

<section class="blog">

	<div class="container">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 posts">			
			<?php 

				if(isset($novidades) AND is_array($novidades)){ 
					foreach($novidades as $novidade){

			?>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="post<?php if($novidade->categoria_id==999){echo ' ebook';} ?>">
						<a href="<?php echo SITE_URL."/post/".$novidade->titulo_seo; ?>">
							<?php if($novidade->imagem!=""){ ?>
				          	<img src="<?php echo SITE_URL ?>/arquivos/novidades/thumb2/<?php echo $novidade->imagem; ?>" width="100%" class="img-responsive" alt="">
				          	<?php }else{ ?>
				            <!-- <img src="<?php echo SITE_URL ?>/views/imagens/blog/post-1-thumb1.jpg" class="img-responsive" alt=""> -->
				          	<?php } ?>
						</a>
						<div class="content">
							<h3><a href="<?php echo SITE_URL."/post/".$novidade->titulo_seo; ?>"><?php echo $novidade->titulo; ?></a></h3>
		          			<p><?php echo $novidade->resumo; ?></p>
						</div>
						<hr>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center footer separador">
							<span class="data desktop"><?php echo Funcoes::blog_data_mes_completo($novidade->data); ?></span>
							<span class="data mobile"><?php echo Funcoes::blog_data_abreviado($novidade->data); ?></span>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center footer">
							<a href="<?php echo SITE_URL."/post/".$novidade->titulo_seo; ?>" class="saiba-mais">Leia mais</a>
						</div>
					</div>
				</div>

			<?php 
					}
				}else if(!empty($novidades)){
			?>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="post<?php if($novidades->categoria_id==999){echo ' ebook';} ?>">
						<a href="<?php echo SITE_URL."/post/".$novidades->titulo_seo; ?>">
							<?php if($novidades->imagem!=""){ ?>
				          	<img src="<?php echo SITE_URL ?>/arquivos/novidades/thumb2/<?php echo $novidades->imagem; ?>" width="100%" class="img-responsive" alt="">
				          	<?php }else{ ?>
				            <!-- <img src="<?php echo SITE_URL ?>/views/imagens/post-1-thumb1.jpg" class="img-responsive" alt=""> -->
				          	<?php } ?>
						</a>
						<div class="content">
							<h3><a href="<?php echo SITE_URL."/post/".$novidades->titulo_seo; ?>"><?php echo $novidades->titulo; ?></a></h3>
		          			<p><?php echo $novidades->resumo; ?></p>
						</div>
						<hr>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center footer separador">
							<span class="data desktop"><?php echo Funcoes::blog_data_mes_completo($novidades->data); ?></span>
							<span class="data mobile"><?php echo Funcoes::blog_data_abreviado($novidades->data); ?></span>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center footer">
							<a href="<?php echo SITE_URL."/post/".$novidades->titulo_seo; ?>" class="saiba-mais">Leia mais</a>
						</div>
					</div>
				</div>

			<?php }else{ ?>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					Nenhum post cadastrado nesta categoria.
				</div>
			<?php } ?>


			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="paginacao">
					<ul>
						<?php if(isset($_GET['p'])){ 
							if($_GET['p']==1){
						?>
							<li class="prev"><a href="<?php echo "/categoria/".$categoria_ativa."?p=1"; ?>"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
						<?php
							}else{
						?>
							<li class="prev"><a href="<?php echo "/categoria/".$categoria_ativa."?p=".($_GET['p']-1); ?>"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
						<?php
							}
							}else{
						?>		
							<li class="prev"><a href="<?php echo "/categoria/".$categoria_ativa."?p=1"; ?>"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
						<?php
							}
						?>		

						<?php
						for($x=1; $x<=$paginacao; $x++){
							if(isset($_GET['p'])){
						?>
						<li <?php echo $x==$_GET['p'] ? 'class="active"' : ''; ?>><a href="<?php echo SITE_URL."/categoria/".$categoria_ativa."?p=".$x; ?>"><?php echo $x; ?></a></li>
						<?php }else{ ?>
							<li <?php echo $x==1 ? 'class="active"' : ''; ?>><a href="<?php echo SITE_URL."/categoria/".$categoria_ativa."?p=".$x; ?>"><?php echo $x; ?></a></li>
						<?php
						} } ?>

						<?php if(isset($_GET['p'])){ 
							if($_GET['p']==$paginacao){
						?>
							<li class="next"><a href="<?php echo "/categoria/".$categoria_ativa."?p=".$paginacao; ?>"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
						<?php
							}else{
						?>
							<li class="next"><a href="<?php echo "/categoria/".$categoria_ativa."?p=".($_GET['p']+1); ?>"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
						<?php
							}
							}else{
						?>		
							<li class="next"><a href="<?php echo "/categoria/".$categoria_ativa."?p=".$paginacao; ?>"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
						<?php
							}
						?>	

					</ul>
				</div>
			</div>
		</div>

		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 sidebar" id="!">
			<h4>Blog Dietwin</h4>
			<hr>
			<p>Acompanhe as últimas novidades e dicas que a equipe do Dietwin prepara constamente para seus clientes.</p>
			
			<?php if(isset($_SESSION['cadastro_blog_sucesso'])){ ?>
			<div class="alert alert-success blog-cadastro-sucesso" role="alert">						
				E-mail cadastrado com sucesso!
			</div>
			<?php unset($_SESSION['cadastro_blog_sucesso']); } ?>
			<form action="<?php echo SITE_URL."/cadastro-email" ?>" method="post" role="form" class="form-inline">
				<div class="form-group">
					<label class="sr-only" for="email">E-mail</label>
					<input type="email" name="email" class="form-control" id="email" placeholder="seu@email" required="required">
					<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
				</div>
				<button type="submit">Cadastrar</button>
			</form>

			<div class="categorias">
				<h4>Categorias</h4>						
				<ul>
				<?php
				if(isset($categorias) AND is_array($categorias)){ 						
				foreach($categorias as $categoria): ?>	
					<li <?php echo $categoria_ativa==$categoria->tag ? 'class="active"' : '' ?>><a href="<?php echo SITE_URL."/categoria/".$categoria->tag; ?>"><?php echo $categoria->nome; ?> <span class="num"><?php echo $categoria->total_posts; ?></span></a></li>
				<?php 
				endforeach;						 						
				 }else{ 
				 	if(!empty($categorias)){
				 		?>
				 			<li <?php echo $categoria_ativa==$categorias->tag ? 'class="active"' : '' ?>><a href="<?php echo SITE_URL."/categoria/".$categorias->tag; ?>"><?php echo $categorias->nome; ?> <span class="num"><?php echo $categorias->total_posts; ?></span></a></li>
				 		<?php
				 	}else{
				 		echo "<li>Nenhuma categoria cadastrada.</li>";
				 	}
				 ?>							
				<?php } ?>
				</ul>
			</div>
		</div>
	</div>

</section>


<?php require_once 'includes/footer.php' ?>
</body>
</html>