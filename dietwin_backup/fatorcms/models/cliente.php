<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property string nome
 * @property string email
 * @property string email_secundario
 * @property string senha
 * @property float cpf
 * @property int rg
 * @property float cnpj
 * @property string inscricao_estadual
 * @property string responsavel_nome
 * @property float telefone_principal
 * @property float telefone_extra
 * @property string profissao
 * @property string cadastro_data
 * @property string ultimo_login_data
 * @property int ativo
 * @property string nome_empresa
 * @property int cadastro_completo
 * @property int tipo
 */

class FatorCMS_Model_Cliente extends FatorCMS_Model_Padrao
{
	protected $tabela_nome = 'clientes';

    public function verificar_null($excecoes = array())
    {
        $colunas_not_null = array('id', 'nome', 'email', 'senha', 'cpf', 'telefone_principal', 'profissao', 'cadastro_data', 'ativo', 'cadastro_completo', 'tipo');

        foreach ($colunas_not_null as $nome)
        {
            //echo in_array($nome, $excecoes) ? 'Encontrou '.$nome.'<br>' : 'Não encontrou '.$nome.'<br>';
            if (is_null($this->$nome) AND in_array($nome, $colunas_not_null) AND ! in_array($nome, $excecoes))
            {

                return FALSE;
            }
        }
        return TRUE;
    }

    public function verificar_cadastro_completo()
    {
        if ($this->verificar_null(array('senha')))
        {
            $endereco = new FatorCMS_Model_ClienteEndereco();
            $endereco = $endereco->select('SELECT id FROM {tabela_nome} WHERE cliente_id = '.$this->id.' LIMIT 1');

            //print_r($endereco); exit;

            if ($endereco AND is_numeric($endereco->id))
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

} // end class