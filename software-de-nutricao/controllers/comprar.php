<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Comprar extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
    if(!isset($_SESSION['produto_temp'])){
      $_SESSION['produto_temp'] = 'dietwin';
    }
    $parametros->_0 = $_SESSION['produto_temp'];

    //print_r($parametros); die;
    // Se o usuário não estiver logado, redireciona ele para a pagina de login enviando por session o produto selecionado antes da compra
    if(!isset($_SESSION['cliente_id'])){
      $_SESSION['produto_selecionado'] = $parametros->_0;
      header('Location: '.SITE_URL.'/comprar/identificacao');
      exit;
    }

    if($parametros->_0!=""){
    $produto = new Model_Produto();
    $produto = $produto->select("SELECT * FROM {tabela_nome} WHERE nome_seo='".$parametros->_0."'");
    $_SESSION['produto_id'] = $produto->id;

    //print('<pre>'); print_r($produto); die;
      if($produto){
        $view = new View('comprar.php');
        $this->view_variaveis_obrigatorias($view);

        if(isset($_SESSION['cliente_id'])){
          $endereco = new Model_ClienteEndereco;
          $endereco = $endereco->select('SELECT * FROM {tabela_nome} WHERE entrega=0 AND cliente_id='.$_SESSION['cliente_id']);

          $frete_cep_valido = 'no';

          $controller_frete = new Controller_Frete();
          if($controller_frete->cepValido($endereco->cep)){
            $frete_cep_valido = 'yes';
          }
          $frete = $controller_frete->calcular($endereco->cep);
          $frete[] = array('servico' => 'Download', array('valor' => 0, 'prazo_entrega' => 'Imediato'));
        }else{
          $frete = null;
        }

        // Verificamos se existe um cupom
        if(isset($_SESSION['cupom_id'])){
          $cupom = new Model_Cupom();
          $cupom = $cupom->select("SELECT * FROM {tabela_nome} WHERE id='".$_SESSION['cupom_id']."'");
          $view->adicionar('cupom_valor',$cupom->valor);
        }

        $view->adicionar('cliente_id',$_SESSION['cliente_id']);
        $view->adicionar('frete_cep_valido',$frete_cep_valido);
        $view->adicionar('produto_nome_seo',$produto->nome_seo);
        $view->adicionar('produto_nome',$produto->nome);
        $view->adicionar('produto_valor',$produto->valor);
        $view->adicionar('frete',$frete);

        $view->adicionar('body_class', 'index');
        $view->adicionar('notificacao',new Notificacao());
        $view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        $view->exibir();
      }else{
        header('Location: '.SITE_URL.'/planos');
      }
    }else{
      header('Location: '.SITE_URL.'/planos');
    }
	}

  public function identificacao($parametros){

    $parametros->_0 = $_SESSION['produto_selecionado'];
    
    //unset($_SESSION['produto_selecionado']);

    if($parametros->_0!=""){
    $produto = new Model_Produto();
    $produto = $produto->select("SELECT * FROM {tabela_nome} WHERE nome_seo='".$parametros->_0."'");
    $_SESSION['produto_id'] = $produto->id;

    //print('<pre>'); print_r($produto); die;
      if($produto){
        $view = new View('comprar.php');
        $this->view_variaveis_obrigatorias($view);

        if(isset($_SESSION['cliente_id'])){
          $endereco = new Model_ClienteEndereco;
          $endereco = $endereco->select('SELECT * FROM {tabela_nome} WHERE entrega=0 AND cliente_id='.$_SESSION['cliente_id']);

          $controller_frete = new Controller_Frete();
          $frete = $controller_frete->calcular($endereco->cep);
          $frete[] = array('servico' => 'Download', array('valor' => 0, 'prazo_entrega' => 'Imediato'));
        }else{
          $frete = null;
        }



        // Verificamos se existe um cupom
        if(isset($_SESSION['cupom_id'])){
          $cupom = new Model_Cupom();
          $cupom = $cupom->select("SELECT * FROM {tabela_nome} WHERE id='".$_SESSION['cupom_id']."'");
          $view->adicionar('cupom_valor',$cupom->valor);
        }

        $view->adicionar('frete_cep_valido','yes');
        $view->adicionar('produto_nome_seo',$produto->nome_seo);
        $view->adicionar('produto_nome',$produto->nome);
        $view->adicionar('produto_valor',$produto->valor);
        $view->adicionar('frete',$frete);

        $view->adicionar('body_class', 'index');
        $view->adicionar('notificacao',new Notificacao());
        $view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        $view->exibir();
      }else{
        header('Location: '.SITE_URL.'/planos');
      }
    }else{
      header('Location: '.SITE_URL.'/planos');
    }
  }

} // end class