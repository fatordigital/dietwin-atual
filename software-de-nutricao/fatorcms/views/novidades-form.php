<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php'; ?>

<script type="text/javascript">
tinyMCE.init({
	// General options
	editor_selector : "novidade_conteudo",
	mode : "textareas",
	theme : "advanced",
	plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
	plugins : 'searchreplace,contextmenu,paste,filemanager,imagemanager',
	// Theme options
	theme_advanced_buttons1 : 'bold,italic,underline,strikethrough,formatselect,|,justifyleft,justifycenter,justifyright,justifyfull,|,|,fontsizeselect,forecolor,backcolor,|,pasteword,removeformat,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,anchor,|,code,|,insertimage,insertfile',
	theme_advanced_buttons2 : "",
	theme_advanced_buttons3 : "",
	theme_advanced_buttons4 : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : true
});
</script>

	<div id="main-content"> <!-- Main Content Section with everything -->

        <!-- Page Head -->
		<?php if (is_null($novidade) OR is_null($novidade->id)) { ?>
			<h2>Cadastro de um Post</h2>
			<p id="page-intro">Utilize o formulário abaixo para incluir um post no blog.</p>
		<?php } else { ?>
			<h2>Edição de um Post</h2>
			<p id="page-intro">No formulário abaixo você pode alterar os dados de um post.</p>
		<?php } ?>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<?php if (is_null($novidade) OR is_null($novidade->id)) { ?>
					<h3>Dados de um post</h3>
				<?php } else { ?>
					<h3>Informações do post <?php echo $novidade ? $novidade->titulo : '' ?></h3>
				<?php } ?>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<form action="<?php echo SITE_URL ?>/fatorcms/novidades/<?php echo (is_null($novidade) OR is_null($novidade->id)) ? 'cadastrar' : 'atualizar' ?>" method="post" id="novidade_form" enctype="multipart/form-data">

					<fieldset>

						<?php if ( ! is_null($novidade)) { ?>
							<input type="hidden" name="id" value="<?php echo $novidade->id ?>" />
						<?php } ?>

                        <p>
                            <label for="alias">
                                Site
                            </label>
                            <select name="alias"
                                    class="large-input"
                                    id="alias">
                                <option <?php echo $novidade && $novidade->alias == 'dietwin' ? 'selected':'' ?> value="dietwin">DietWin Blog</option>
                                <option <?php echo $novidade && $novidade->alias == 'rotulo' ? 'selected':'' ?> value="rotulo">Rótulo de Alimentos Blog</option>
                            </select>
                        </p>

						<p>
							<label for="categoria_id">Categoria</label>

							<select name="categoria_id" class="large-input" id="categoria_id">
								<?php foreach($categorias as $categoria){ ?>
									<option value="<?php echo $categoria->id; ?>" <?php if($novidade){ if($categoria->id==$novidade->categoria_id){ echo 'selected="selected"'; } } ?>><?php echo $categoria->nome; ?></option>
								<?php } ?>
							</select>
						</p>

						<p>
							<label for="titulo">Título</label>
							<input class="text-input large-input required" type="text" id="titulo" name="titulo" maxlength="200" value="<?php echo $novidade ? $novidade->titulo : '' ?>" />
						</p>

						<p>
							<label for="resumo">Resumo</label>
							<textarea class="text-input large-input" id="resumo" name="resumo" cols="80" rows="4"><?php echo $novidade ? $novidade->resumo : '' ?></textarea><br />
							<small>Número de caracteres restantes: <span id="resumo_contador">600</span></small>
						</p>

						<p class="required-input">
							<label for="conteudo">Conteúdo</label>
							<textarea class="text-input required novidade_conteudo" cols="79" rows="20" id="conteudo" name="conteudo">
								<?php
									$new = '';
									if($novidade){
										$conteudo = $novidade->conteudo;
										$conteudo =  str_replace('../../../arquivos/', 'http://dietwin.com.br/arquivos/', $conteudo);
										$conteudo =  str_replace('../../arquivos/', 'http://dietwin.com.br/arquivos/', $conteudo);
									}else{
										$conteudo = '';
									}
								?>
								<?php echo $conteudo; ?></textarea>
						</p>

						<link rel="stylesheet" href="<?php echo SITE_BASE ?>/fatorcms/views/css/jquery-ui-timepicker-addon.css" type="text/css" media="screen" />
						<script type="text/javascript" src="<?php echo SITE_BASE ?>/fatorcms/views/js/jquery-ui-timepicker-addon.js"></script>
						<script type="text/javascript">
							$(document).ready(function(){
								$('#data_time').datetimepicker({
								    dateFormat: "dd/mm/yy",
								    timeFormat:  "HH:mm:ss",
								    currentText: "Agora",
								    closeText: "Fechar",							    
								});
							});
						</script>

                        <p>
                           <label for="data">Data</label>
                           <input class="text-input small-input required" type="text" id="data_time" name="data_time" maxlength="20" value="<?php echo ($novidade AND $novidade->data) ? date('d/m/Y H:i:s',strtotime($novidade->data)) : '' ?>" />
                        </p>
						<!-- date('d/m/Y H:i:s',strtotime($novidade->data)) -->




						<p>
							<label for="eh_visivel_1">Pode ser exibida no site?</label>
							<input type="radio" name="eh_visivel" id="eh_visivel_1" value="1" <?php echo ($novidade AND $novidade->eh_visivel) ? 'checked="checked"' : '' ?> /> <span class="sim">Sim</span>
							<input type="radio" name="eh_visivel" id="eh_visivel_0" value="0" <?php echo ( ! $novidade OR ! $novidade->eh_visivel) ? 'checked="checked"' : '' ?> /> <span class="nao">Não</span>
						</p>

                        <p>
							<label for="imagem">Imagem Principal</label>
							<input class="text-input small-input" type="file" name="imagem" id="imagem" value="<?php echo $novidade ? ''.$novidade->imagem  : '' ?>" /><br />
	                        <small>Envie arquivos <strong>JPG</strong> com no máximo <strong>3mb</strong> de tamanho.</small>
							<?php if (isset($novidade) AND ! is_null($novidade->imagem)) { ?>
								<br/>Atual:<br/>
								<a href="<?php echo SITE_BASE ?>/arquivos/novidades/<?php echo $novidade->imagem ?>" rel="modal">
									<img src="<?php echo SITE_BASE ?>/arquivos/novidades/thumb1/<?php echo $novidade->imagem ?>" alt="Imagem da novidade &quot;<?php echo $novidade->titulo ?>&quot;" />
								</a><br />
								<a href="<?php echo SITE_URL ?>/fatorcms/novidades/imagem-excluir/<?php echo $novidade->id ?>" class="item-confirmar" title="Excluir a imagem atual">Excluir a imagem atual?</a>
							<?php } ?>
						</p>

                        
                        <!--<p>
							<label for="tags">Tags</label>
							<input class="" type="text" id="tags" name="tags" />
	                        <small>Para trocar para uma outra tag digite <strong>,</strong> (vírgula) ou aperte a tecla <strong>Tab</strong>. Itens que não constam na listagem serão automaticamente adicionados e vinculados à novidade.</small>
						</p>-->

						<p>
							<input class="button" type="submit" value="<?php echo (is_null($novidade) OR is_null($novidade->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
						</p>

					</fieldset>

					<div class="clear"></div><!-- End .clear -->

				</form>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>