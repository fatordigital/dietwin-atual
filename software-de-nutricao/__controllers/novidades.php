<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Novidades extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        $buscar = "";
        if (isset($parametros->buscar) AND strlen($parametros->buscar)>0)
        {
            $buscar = Funcoes::mysqli_escape($parametros->buscar);
        }

        $novidade = new Model_Novidade();
        $novidade->clausula = "
            SELECT *
            FROM fd_novidades
            WHERE fd_novidades.eh_visivel=1 AND DATA <= CURRENT_DATE AND (titulo LIKE '%".$buscar."%' OR fd_novidades.id IN (
                SELECT DISTINCT fd_novidades.id FROM fd_novidades
                RIGHT JOIN fd_novidades_tags ON fd_novidades.id = fd_novidades_tags.novidade_id
	            RIGHT JOIN fd_tags ON fd_novidades_tags.tag_id = fd_tags.id
	            WHERE fd_tags.nome LIKE '%".$buscar."%'

            ))
            ORDER BY data DESC";

        // Através da paginação vai verificar se existe a necessidade de dividir o resultado em várias páginas e se é necessário adicionar um LIMIT
        $paginacao = new Paginacao($novidade, $parametros, 6, 6);

        // Executa a cláusula lá de cima
        $novidades = $novidade->select(NULL, TRUE); // NULL para não alterar a clásula já definida e TRUE para forçar retorno em array
        $view = new View('novidades.php');
		$this->view_variaveis_obrigatorias($view);

        $view->adicionar('novidades',$novidades);
        $view->adicionar('paginacao',$paginacao);

		$view->adicionar('body_class', 'novidades');
		$view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Novidades - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */



} // end class