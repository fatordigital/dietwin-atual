<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Novidade extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        if ($parametros->titulo_seo AND strlen($parametros->titulo_seo) > 0)
        {
            $novidade = new Model_Novidade();
            $novidade = $novidade->select("SELECT * FROM {tabela_nome} WHERE eh_visivel = 1 AND data <= CURRENT_DATE AND titulo_seo='".Funcoes::mysqli_escape($parametros->titulo_seo)."'");
            if (!$novidade)
            {
                header('HTTP/1.1 404 Not Found');
                $erro_404 = new Controller_Erro;
                $erro_404->index(404);
                exit;
            }
            $outras_novidades = $novidade->select("SELECT * FROM {tabela_nome} WHERE eh_visivel = 1 AND data <= CURRENT_DATE AND titulo_seo !='".Funcoes::mysqli_escape($parametros->titulo_seo)."' ORDER BY data DESC LIMIT 0,3", TRUE);
        }
        else
        {
            header('HTTP/1.1 404 Not Found');
            $erro_404 = new Controller_Erro;
            $erro_404->index(404);
            exit;
        }

        $view = new View('novidade.php');
		$this->view_variaveis_obrigatorias($view);
        $view->adicionar('novidade',$novidade);
        $view->adicionar('outras_novidades',$outras_novidades);
		$view->adicionar('body_class', 'novidades novidade');
		$view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', $novidade->titulo.' - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */



} // end class