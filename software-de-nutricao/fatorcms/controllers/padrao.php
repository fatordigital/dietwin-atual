<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_Padrao
{
	protected $cms_usuario_logado;


	/**
	 * Construtor
	 *
	 * Inicializa as classes que serão utilizadas nas classes estendidas
	 */  
	public function __construct()
	{
		if ( ! isset($_SESSION))
		{
			session_start();
		}

		// Se o usuário estiver logado, busca as informações dele
		if (isset($_SESSION['cms_usuario_id']) AND is_numeric($_SESSION['cms_usuario_id']))
		{
			// Diferente de -1 é Usuário com cadastro no Banco
			if (intval($_SESSION['cms_usuario_id']) != -1)
			{
				$this->cms_usuario_logado = new FatorCMS_Model_CMSUsuario($_SESSION['cms_usuario_id']);
			}
			else
			{
				// Criamos um objeto especial contendo informações do super administrador da Fator
				$admin_fator = new FatorCMS_Model_CMSUsuario;
				$admin_fator->eh_administrador = 1;
				$admin_fator->nome = 'Fator Admin';
				$admin_fator->id = -1;

				$this->cms_usuario_logado = $admin_fator;
			}
		}
	}
	
	
	/**
	 * Verifica se o usuário está logado para acessar o FatorCMS
	 */
	public function verificar_usuario_logado()
	{
		// Testa se um usuário está logado
		if ( ! isset($_SESSION['cms_usuario_id']) OR ! is_numeric($_SESSION['cms_usuario_id']))
		{
			// Se o usuário não está logado, monta a mensagem e redireciona ele para a tela de login
			$_SESSION['notificacao_tipo'] = 'information';
			$_SESSION['notificacao_texto'] = 'Você precisa estar logado para acessar o sistema.';

			// Redirecionamento a ser feito após login
			$_SESSION['redirecionar'] = $_SERVER['REQUEST_URI'];
			
			header('Location: '.SITE_URL.'/fatorcms/index');
			exit;
		}
	}


	/**
	 * Adicionamos na view variáveis que são utilizadas em praticamente todas as páginas
	 * @param $view
	 */
	protected function view_adicionar_obrigatorias(&$view)
	{
		// Adicionamos o objeto do atual usuário logado
		$view->adicionar('cms_usuario_logado', $this->cms_usuario_logado);

		//-----

		// Adicionamos os módulos que o atual usuário logado pode acessar
		$cms_modulo = new FatorCMS_Model_CMSModulo;
		$cms_modulos = $cms_modulo->select('
			SELECT modulo.menu_nome
			FROM {tabela_nome} AS modulo
				RIGHT JOIN {tabela_prefixo}cms_permissoes AS permissao ON modulo.id = permissao.cms_modulo_id
			WHERE permissao.cms_usuario_id = '.$this->cms_usuario_logado->id
		, TRUE);
		$cms_usuario_logado_modulos = array();
		if ($cms_modulos)
		{
			foreach ($cms_modulos as $cms_modulo)
			{
				$cms_usuario_logado_modulos[] = $cms_modulo->menu_nome;
			}
		}
		$view->adicionar('cms_usuario_logado_modulos', $cms_usuario_logado_modulos);
	}



	/*********************************** MÉTODOS EXTRAS ***********************************/



	/**
	 * Verificamos se o usuário tem permissão de acesso ao módulo (utilizamos esta função nos constructs dos controllers
	 * @param $modulo_menu_nome
	 * @return FatorCMS_Model_CMSPermissao
	 */
	public function verificar_usuario_permissao($modulo_menu_nome)
	{
		$cms_permissao = new FatorCMS_Model_CMSPermissao;
		$cms_permissao = $cms_permissao->select('
			SELECT permissao.id
			FROM {tabela_nome} AS permissao
				RIGHT JOIN {tabela_prefixo}cms_modulos AS modulo ON permissao.cms_modulo_id = modulo.id
			WHERE modulo.menu_nome = "'.$modulo_menu_nome.'" AND permissao.cms_usuario_id = '.$this->cms_usuario_logado->id
		);

		if (( ! $cms_permissao OR ! isset($cms_permissao->id)) AND ! $this->cms_usuario_logado->eh_administrador)
		{
			$_SESSION['notificacao_tipo'] = 'information';
			$_SESSION['notificacao_texto'] = 'Você não possui permissão para acessar este módulo.';

			header('Location: '.SITE_URL.'/fatorcms/inicio');
			exit;
		}
	}


	/**
	 * Não mexer nessa função. Usada para o login especial do usuário "fator"
	 * @static
	 * @return string
	 */
	protected function gerar_senha_fator()
	{
		return substr(base64_encode(sha1(('http://'.$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!='80'?':'.$_SERVER["SERVER_PORT"]:'').mktime(12,34,56)).md5('271828182845904523536028747135266249775724709369995'))).'271828182845904523536028747135266249775724709369995', 10, 10);
	}
	
} // end class