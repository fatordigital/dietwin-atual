<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php' ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

		<!-- Page Head -->
		<h2>Lista de Interações</h2>
		<p id="page-intro">Abaixo estão listadas todas as interações com clientes.</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<form action="<?php echo SITE_URL ?>/fatorcms/interacoes/listar" method="get" id="interacoes_buscar_form">
			<fieldset>
				<p>
					<label for="buscar">Digite uma ou mais palavras-chave para buscar por uma interação cadastrada:</label>
					<input class="text-input medium-input" type="text" id="buscar" name="buscar" maxlength="100" value="<?php echo isset($buscar) ? $buscar : '' ?>" />
					<input class="button" type="submit" value="Buscar" />
				</p>
			</fieldset>
			<div class="clear"></div><!-- End .clear -->
		</form>

		<?php if (isset($buscar) AND ! is_null($buscar)) { ?>
			<p class="buscar-resultado">
				Foram encontrados <strong><?php echo $paginacao->linhas_total ?></strong> resultado(s) para a busca por "<strong><?php echo $buscar ?></strong>".
				<a title="Limpar a busca" href="<?php echo SITE_URL?>/fatorcms/interacoes/listar"><img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier-cross.png" alt="Limpar busca" /> Clique aqui para limpar a busca</a>
			</p>
		<?php } ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Interações</h3>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<table>

					<thead>
						<tr>
							<?php echo Funcoes::montar_th_ordenacao_listagem('interacoes', 'listar', $paginacao, $buscar, 'Data', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('interacoes', 'listar', $paginacao, $buscar, 'Cód.', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('interacoes', 'listar', $paginacao, $buscar, 'Cliente', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('interacoes', 'listar', $paginacao, $buscar, 'Tipo', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('interacoes', 'listar', $paginacao, $buscar, 'Título', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('interacoes', 'listar', $paginacao, $buscar, 'Conteúdo', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('interacoes', 'listar', $paginacao, $buscar, 'Autor', $ordenar_por, $ordem) ?>
                            <th>Ações</th>
						</tr>
					</thead>

					<tfoot>
						<tr>
							<td colspan="8">

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_quantidades() ?>

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_links() ?>

								<div class="clear"></div>
							</td>
						</tr>
					</tfoot>

					<tbody>
						<?php
                        if($interacoes AND count($interacoes) > 0)
						{
							foreach ($interacoes as $interacao)
							{
							?>
								<tr>
                                    <td class="<?php echo $ordenar_por=='data' ? 'current' : '' ?>"><?php echo date('d/m/Y \à\s H:i:s', strtotime($interacao->data)) ?></td>
                                    <td class="<?php echo $ordenar_por=='cod' ? 'current' : '' ?>">
                                        <a href="<?php echo SITE_URL ?>/fatorcms/clientes/editar/<?php echo $interacao->cliente_id ?>/aba/4#interacao_<?php echo $interacao->id ?>" title="Visualizar a interação de código &quot;<?php echo $interacao->id ?>&quot;">
                                            <?php echo $interacao->id ?>
                                        </a>
                                    </td>
									<td class="<?php echo $ordenar_por=='cliente' ? 'current' : '' ?>">
										<a href="<?php echo SITE_URL ?>/fatorcms/clientes/editar/<?php echo $interacao->cliente_id ?>" title="Editar o cliente &quot;<?php echo $interacao->cliente_nome ?>&quot;">
											<?php echo $interacao->cliente_nome ?>
										</a>
									</td>
									<td class="<?php echo $ordenar_por=='tipo' ? 'current' : '' ?>"><?php echo $interacao->tipo ?></td>
									<td class="<?php echo $ordenar_por=='titulo' ? 'current' : '' ?>"><?php echo ( ! empty($interacao->titulo) ? $interacao->titulo : '<em>em branco</em>') ?></td>
									<td class="<?php echo $ordenar_por=='conteudo' ? 'current' : '' ?>"><?php echo Funcoes::cortar_texto(strip_tags(trim($interacao->conteudo)), 200) ?></td>
									<td class="<?php echo $ordenar_por=='autor' ? 'current' : '' ?>">
                                        <a href="<?php echo SITE_URL ?>/fatorcms/cms-usuarios/editar/<?php echo $interacao->cms_usuario_id ?>" title="Editar o usuario &quot;<?php echo $interacao->cms_usuario_nome ?>&quot;">
                                            <?php echo $interacao->cms_usuario_nome ?>
                                        </a>
                                    </td>
									<td>
										<a href="<?php echo SITE_URL ?>/fatorcms/clientes/editar/<?php echo $interacao->cliente_id ?>/aba/4#interacao_<?php echo $interacao->id ?>" title="Visualizar a interação de código &quot;<?php echo $interacao->id ?>&quot;">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier.png" alt="Visualizar" />
										</a>
									</td>
								</tr>
							<?php
							}
						}
						else
						{
							echo '<tr><td colspan="8">'.((isset($buscar) AND ! is_null($buscar)) ? 'Nenhuma interação encontrada para esta busca.' : 'Nenhuma interação cadastrada até o momento.' ).'</td></tr>';
						}
						?>
					</tbody>

				</table>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>