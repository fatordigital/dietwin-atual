<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property string nome
 * @property string nome_seo
 * @property float valor
 * @property string versao
 * @property float licenca_valor
 * @property int log_id
 * @property string log_data
 */

class FatorCMS_Model_Produto extends FatorCMS_Model_Padrao
{
	protected $tabela_nome = 'produtos';

} // end class