<?php include 'includes/cabecalho.php'; ?>

<body class="institucional erro">

<?php include 'includes/topo.php'; ?>

<div class="titulo-principal">
    <p><span>Quem está por trás do</span><br />dietWin</p>
</div>
<div class="titulo-resumo">
    <p>Por trás da família de softwares de nutrição mais vendidos do Brasil está uma equipe experiente de nutricionistas e desenvolvedores. Conheça um pouco sobre a história e as pessoas envolvidas.</p>
</div>
</div>
</div>

<div class="conteudo">
    <div class="container">

        <h1>Erro 404</h1>

        <div class="conteudo-erro">

            <p class="info">O conteúdo que você está procurando não foi encontrado.</p>

            <p>Clique <a href="<?php echo SITE_URL ?>">aqui</a> para voltar para a página inicial ou utilize os menus para navegar por outras áreas do site.</p>

        </div>

        <div class="clear"></div>
    </div>
    <div class="inferior"></div>
</div>

<?php include 'includes/rodape.php'; ?>

