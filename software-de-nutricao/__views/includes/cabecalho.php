<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" /> -->
<meta name="description" content="O dietWin é um conjunto de Softwares de Nutrição, desenvolvidos por nutricionistas que imprime confiabilidade aos dados apresentados." />
<meta name="keywords" content="dietWin, softwares de nutrição, alimentação, bem estar, cuidar da saúde, comer bem, nutrição, diagnóstico nutricional, musculação, alimentação adequada" />
<meta name="author" content="Fator Digital - http://www.fatordigital.com.br" />
<title><?php echo $pagina_title; ?></title>

<!-- META ROBOTS -->

<meta name="robots" content="all"/>
<meta name="revisit-after" content="7 days"/>

<!-- GEO LOCATION TAGS -->

<meta name="geo.region" content="BR-RS" />
<meta name="geo.placename" content="Porto Alegre" />
<meta name="geo.position" content="-14.235004;-51.92528" />
<meta name="ICBM" content="-14.235004, -51.92528" />

<!-- OG TAGS -->

<meta property="og:url" content="http://www.dietwin.com.br/" />
<meta property="og:type" content="website" />
<meta property="og:image" content="http://www.dietwin.com.br/views/imagens/og-dietwin.jpg" />
<meta property="og:image:type" content="image/jpg" />
<meta property="og:image:width" content="640"/>
<meta property="og:image:height" content="400" />

<meta property="og:site_name" content="Dietwin">
<meta property="og:title" content="DietWin - Softwares de nutrição">
<meta property="og:description" content="O dietWin é um conjunto de Softwares de Nutrição, desenvolvidos por nutricionistas que imprime confiabilidade aos dados apresentados.">

<!-- END OG -->

<?php /* <!-- SEO !!!!!!!!!!!!!!ATUALIZAR!!!!!!!!!!!!!!!!! -->
<link rel="home" href="<?php echo SITE_URL ?>" title="Home Page" /> 
<link rel="about" href="<?php echo SITE_URL ?>/institucional" title="Sobre a Factum" /> 
<link rel="multimedia" href="<?php echo SITE_URL ?>/multimidia" title="Multimídia" /> 
<link rel="news" href="<?php echo SITE_URL ?>/novidades" title="Novidades" />
<link rel="sitemap" href="<?php echo SITE_BASE ?>/sitemap.xml.gz" title="Notícias" /> 
<link rel="sitemap" href="<?php echo SITE_BASE ?>/sitemap.xml" title="Sitemap" />
*/ ?>

<link rel="stylesheet" href="<?php echo SITE_BASE ?>/views/css/estilo.css?<?php echo time() ?>" type="text/css" media="screen" />
<!-- Versão para impressão -->
<link rel="stylesheet" href="<?php echo SITE_BASE ?>/views/css/imprimir.css" type="text/css" media="print" />

<!--[if IE 7]>
    <link rel="stylesheet" href="<?php echo SITE_BASE ?>/views/css/estilo_ie7.css" type="text/css" media="screen" />
<![endif]-->

<!--[if IE 8]>
    <link rel="stylesheet" href="<?php echo SITE_BASE ?>/views/css/estilo_ie8.css" type="text/css" media="screen" />
<![endif]-->

<!--[if IE 9]>
    <link rel="stylesheet" href="<?php echo SITE_BASE ?>/views/css/estilo_ie9.css" type="text/css" media="screen" />
<![endif]-->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>

<link href="<?php echo SITE_BASE ?>/views/css/jquery-ui.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<?php echo SITE_BASE ?>/views/js/cufon/cufon.js"></script>
<script type="text/javascript" src="<?php echo SITE_BASE ?>/views/js/cufon/Swis721_Cn_BT_400-Swis721_Cn_BT_700.font.js"></script>

<!-- Colorbox -->
<link id="colorbox_sheet" rel="stylesheet" href="<?php echo SITE_BASE ?>/views/js/colorbox/colorbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo SITE_BASE ?>/views/js/colorbox/jquery.colorbox-min.js"></script>
    
<script type="text/javascript" src="<?php echo SITE_BASE ?>/views/js/jquery.maskedinput-1.3.min.js"></script>

<!-- AddThis-->
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4e6688052c5018ae"></script>
<script type="text/javascript">var addthis_config = {data_ga_property: 'UA-3191692-1',data_ga_social : true};</script>

<script type="text/javascript">
var SITE_URL = '<?php echo SITE_URL ?>';
var SITE_BASE = '<?php echo SITE_BASE ?>';
var PRODUTO = <?php echo (isset($classe) AND strlen($classe)>0) ? "'".$classe."'": 'null' ?>;

</script>

<script type="text/javascript" src="<?php echo SITE_BASE; ?>/views/js/validate/jquery.validate.min.js"></script>

<script type="text/javascript" src="<?php echo SITE_BASE; ?>/views/js/jshashtable.js"></script>
<script type="text/javascript" src="<?php echo SITE_BASE; ?>/views/js/jquery.prettynumber.js"></script>

<script type="text/javascript" src="<?php echo SITE_BASE; ?>/views/js/funcoes.js?<?php echo time() ?>"></script>

<?php if (SITE_LOCAL == 'cliente') { ?>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-3191692-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<?php } ?>
</head>