<?php

$errors = array();    // array to hold validation errors
$data = array();        // array to pass back data

$data = $_POST['nome'];

require 'phpmailer/PHPMailerAutoload.php';

$mail = new PHPMailer;

$nome = "";
$email = "";
$telefone = "";
$mensage = "";

$nome = trim($_POST['nome']);
$email = trim($_POST['email']);
$telefone = trim($_POST['telefone']);
$mensagem = trim($_POST['mensagem']);

if (empty($nome) || empty($telefone) || empty($email) || empty($mensagem)) {
    http_response_code(400);
    die(json_encode(array('error' => true, 'msg' => 'Todos os campos são obrigatórios.')));
}

$title = 'Contato - Fale-Conoso';


//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.mandrillapp.com:587';             // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'mandrill@fatordigital.com.br';     // SMTP username
$mail->Password = '3L5z5MENVKUba3YJpeUNgA';              // chave mandril teste/padrão
$mail->SMTPSecure = 'tls';                            // TCP port to connect to

//$mail->AddAddress('recepcao@universitario.net');

//$mail->AddBCC('cleber.alves@fatordigital.com.br', 'Cléber Alves'); // Cópia Oculta
$mail->AddAddress('suporte@dietwin.com.br');
//$mail->AddBCC('andrew.rodrigues@fatordigital.com.br'); // Cópia Oculta

$mail->From = "contato@dietwin.com.br"; // Seu e-mail
$mail->FromName = $title; // Seu nome
$mail->AddReplyTo($email, $nome);

//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
$mail->isHTML(true);                                    // Set email format to HTML

$mail->Subject = $title;
$mail->CharSet = 'UTF-8';
$msg = "<table width='100%' height='100%' border='0'><tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 13px;line-height: 150%;'>";
$msg .= "<h4>Dietwin - " . $title . "</h4>";
$msg .= "<b>Nome: </b>" . $nome . "<br>";
$msg .= "<b>E-mail: </b>" . $email . "<br>";
$msg .= "<b>Telefone: </b>" . $telefone . "<br>";
$msg .= "<b>Mensagem: </b>" . $mensagem . "<br>";
$msg .= "</td></tr></table>";
$mail->Body = $msg;
//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

if (!$mail->send()) {
    http_response_code(400);
    $data = array(
        'error' => true,
        'msg' => 'Ocorreu um erro durante o envio.'
    );
} else {
    http_response_code(200);
    $data = array(
        'msg' => 'Mensagem enviada com sucesso',
        'error' => false
    );
}

die(json_encode($data));