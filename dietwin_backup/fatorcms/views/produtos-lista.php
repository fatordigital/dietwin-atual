<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php' ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

		<!-- Page Head -->
		<h2>Lista de Produtos cadastrados</h2>
		<p id="page-intro">Abaixo estão listados todos os produtos que foram cadastrados para uso no FatorCMS.</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<form action="<?php echo SITE_URL ?>/fatorcms/produtos/listar" method="get" id="produtos_buscar_form">
			<fieldset>
				<p>
					<label for="buscar">Digite uma ou mais palavras-chave para buscar por um produto cadastrado:</label>
					<input class="text-input medium-input" type="text" id="buscar" name="buscar" maxlength="100" value="<?php echo isset($buscar) ? $buscar : '' ?>" />
					<input class="button" type="submit" value="Buscar" />
				</p>
			</fieldset>
			<div class="clear"></div><!-- End .clear -->
		</form>

		<?php if (isset($buscar) AND ! is_null($buscar)) { ?>
			<p class="buscar-resultado">
				Foram encontrados <strong><?php echo $paginacao->linhas_total ?></strong> resultado(s) para a busca por "<strong><?php echo $buscar ?></strong>".
				<a title="Limpar a busca" href="<?php echo SITE_URL?>/fatorcms/produtos/listar"><img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier-cross.png" alt="Limpar busca" /> Clique aqui para limpar a busca</a>
			</p>
		<?php } ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Produtos</h3>

				<?php /* <input class="produto button botao-cadastrar" type="button" value="Cadastrar um novo produto" /> */ ?>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<table>

					<thead>
						<tr>
							<?php echo Funcoes::montar_th_ordenacao_listagem('produtos', 'listar', $paginacao, $buscar, 'Nome', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('produtos', 'listar', $paginacao, $buscar, 'Valor', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('produtos', 'listar', $paginacao, $buscar, 'Versão', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('produtos', 'listar', $paginacao, $buscar, 'Valor da Licença', $ordenar_por, $ordem) ?>
                            <th>Ações</th>
						</tr>
					</thead>

					<tfoot>
						<tr>
							<td colspan="5">

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_quantidades() ?>

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_links() ?>

								<div class="clear"></div>
							</td>
						</tr>
					</tfoot>

					<tbody>
						<?php
                        if($produtos AND count($produtos) > 0)
						{
							foreach ($produtos as $produto)
							{
							?>
								<tr>
									<td class="<?php echo $ordenar_por=='nome' ? 'current' : '' ?>">
										<a href="<?php echo SITE_URL ?>/fatorcms/produtos/editar/<?php echo $produto->id ?>" title="Editar o produto &quot;<?php echo $produto->nome ?>&quot;">
											<?php echo $produto->nome ?>
										</a>
									</td>
									<td class="<?php echo $ordenar_por=='valor' ? 'current' : '' ?>">R$ <?php echo number_format($produto->valor, 2, ',', '.') ?></td>
									<td class="<?php echo $ordenar_por=='versao' ? 'current' : '' ?>"><?php echo $produto->versao ?></td>
									<td class="<?php echo $ordenar_por=='valor-da-licenca' ? 'current' : '' ?>">R$ <?php echo number_format($produto->licenca_valor, 2, ',', '.') ?></td>
									<td>
										<!-- Icons -->
										<a href="<?php echo SITE_URL ?>/fatorcms/produtos/editar/<?php echo $produto->id ?>" title="Editar o produto &quot;<?php echo $produto->nome ?>&quot;">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" />
										</a>
										<?php /* <a href="<?php echo SITE_URL ?>/fatorcms/produtos/excluir/<?php echo $produto->id ?>" title="Excluir o produto &quot;<?php echo $produto->nome ?>&quot;" class="item-confirmar">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/cross.png" alt="Excluir" />
										</a> */ ?>
									</td>
								</tr>
							<?php
							}
						}
						else
						{
							echo '<tr><td colspan="5">'.((isset($buscar) AND ! is_null($buscar)) ? 'Nenhum produto encontrado para esta busca.' : 'Nenhum produto cadastrado até o momento.' ).'</td></tr>';
						}
						?>
					</tbody>

				</table>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>