$(document).ready(function(){

	$(window).scroll(function(){    
        var st = $(this).scrollTop();
        $('header h1').css({'margin-top': 50+(st/2)+"px", 'margin-bottom': 90-(st/3)+"px"});
        $('.botoes-chamada').css({'margin-top': +(st/7)+"px"});
    });

    var navpos = $('section.nav').offset();
    console.log(navpos.top);
    $(window).bind('scroll', function() {
      if ($(window).scrollTop() > navpos.top) {
        $('section.nav').addClass('fixed');
       }
       else {
         $('section.nav').removeClass('fixed');
       }
    });

    $('a.exibir-telefone').click(function(){
        var efeito = 'slide';
        var opcao = { direction: 'right' };
        var duracao = 300;
        $('.telefone').toggle(efeito, opcao, duracao);
    });

    $('table td .conteudo-feature span.titulo-feature').click(function(){
        $(this).siblings('div.detalhe-feature').slideToggle();
    });

    $('#controla-tabs').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 120,
        itemMargin: 5,
        asNavFor: '#tabs'
    });
   
    $('#tabs').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#controla-tabs"
    }); 
    /*$('#tabs').flexslider({
        animation: "slide",
        controlNav: "thumbnails",
        slideshow: false
    });

    $('ol li img').each(function(){
        var t = $(this);
        var src1 = t.attr('src'); 
        var newSrc = src1.substring(0, src1.lastIndexOf('.'));; 

        t.hover(function(){
            $(this).attr('src', newSrc+ '-hover.' + /[^.]+$/.exec(src1));
        }, function(){
            $(this).attr('src', newSrc + '.' + /[^.]+$/.exec(src1));
        });
    });*/

	$("input#telefone").mask('(00) 0000-0000');

	$("form#duvidas").validate({
        rules:{
            nome:{
                required: true
            },
            email:{
                required: true, email: true
            },
            telefone: {
                required: true
            },
            cidade: {
            	required: true
            }
        },
        invalidHandler: function(e, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $(".erro-form").html('Ops, você esqueceu de preencher um dos campos. Por favor, complete as informações e clique novamente em “Fazer o download”.').fadeIn(500);
            } else {
                $(".erro-form").fadeOut(500);
            }
        },
    });

    $("form#cadastro").validate({
        rules:{
            nome:{
                required: true
            },
        },
        invalidHandler: function(e, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $(".erro-form").html('Ops, você esqueceu de preencher um dos campos.<br />Por favor, complete as informações e clique novamente em “Cadastrar”.').fadeIn(500);
            } else {
                $(".erro-form").fadeOut(500);
            }
        },
    });

	$("a.leia-mais").click(function(){
		$("section.quem-faz").slideToggle();
	});

    //Smooth Scroll
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
    });

});
