<?php require_once 'includes/head.php' ?>
<body>
<?php require_once 'includes/nav.php' ?>

<section class="conteudo cinza">
  <div class="container">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <h1 class="interna">Qual a melhor solução <span class="verde">para você?</span></h1>
      <p><b>Cada software da família dietWin é cuidadosamente construído tendo como foco um perfil de usuário. Descubra qual é a melhor solução para você!</b></p>
    </div>
    <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
      <img src="<?php echo SITE_URL ?>/views/imagens/caixas-dietwin-compra.jpg" alt="dietWin Plus e dietWin Tradicional" class="img-responsive">
    </div>
  </div>
</section>
<section class="conteudo cinza">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <table class="table table-bordered">
        <thead>
        <tr>
          <th class="features" style="width: 60%">Features</th>
          <th class="diet-win text-center" style="width: 20%">Diet<b>win</b></th>
          <th class="diet-win-plus text-center" style="width: 20%">Diet<b>win</b> <span>Plus</span></th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td class="explicacao-feature">
            <img src="<?php echo SITE_URL ?>/views/imagens/prontuario-detalhado-ico.png" alt="Prontuário Detalhado">
            <div class="conteudo-feature">
              <span class="titulo-feature">Ficha do Paciente <img style="float:right; margin:-2px 296px 0 0;" src="<?php echo SITE_URL ?>/views/imagens/planos-mais-informacoes.png" alt="Mais informações"></span>
              <div class="detalhe-feature">
                  O Dietwin armazena a ficha do paciente e apresenta a evolução das consultas, permitindo uma rápida visualização.
              </div>
            </div>
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="Positivo">
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="Positivo">
          </td>
        </tr>
        <tr class="cinza-claro">
          <td class="explicacao-feature">
            <img src="<?php echo SITE_URL ?>/views/imagens/avaliacao-clinica-ico.png" alt="Avaliação Clínica">
            <div class="conteudo-feature">
              <span class="titulo-feature">Avaliação Clínica <img style="float:right; margin:-2px 303px 0 0;" src="<?php echo SITE_URL ?>/views/imagens/planos-mais-informacoes.png" alt="Mais informações"></span>
              <div class="detalhe-feature">
                  O Dietwin permite o cadastro descritivo de doenças, sintomas e sinais, fármacos, e exames laboratoriais para consulta, além disto, possui a ferramenta de anamnese, em que é possível editá-la a qualquer momento.
Já o Dietwin Plus possui um banco de dados para consulta o qual lhe retorna as informações sobre doenças, sintomas e sinais, fármacos e exames laboratoriais pré-cadastradas, possibilitando novos cadastros dos mesmos. Também disponibiliza a ferramenta de anamnese.
              </div>
            </div>
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="Positivo">
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="Positivo">
          </td>
        </tr>
        
        <tr class="cinza-claro">
          <td class="explicacao-feature">
            <img src="<?php echo SITE_URL ?>/views/imagens/avaliacao-nutricional-ico.png" alt="Prescricao-dieta">
            <div class="conteudo-feature">
              <span class="titulo-feature">Avaliação nutricional <img style="float:right; margin:-2px 270px 0 0;" src="<?php echo SITE_URL ?>/views/imagens/planos-mais-informacoes.png" alt="Mais informações"></span>
              <div class="detalhe-feature">
O Dietwin atende a todas as necessidades básicas da avaliação nutricional em todos os ciclos da vida, disponibilizando apenas alguns protocolos para o cálculo da avaliação.
Já o Dietwin Plus avalia todos os ciclos da vida de uma forma intuitiva, realizando inúmeros cálculos através de diversos protocolos disponíveis.
              </div>
            </div>
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="Positivo">
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="Positivo">
          </td>
        </tr>
        
        <tr>
          <td class="explicacao-feature">
            <img src="<?php echo SITE_URL ?>/views/imagens/prescricao-dietetica-ico.png" alt="Prescrição Dietética">
            <div class="conteudo-feature">
              <span class="titulo-feature">Prescrição Dietética <img style="float:right; margin:-2px 280px 0 0;" src="<?php echo SITE_URL ?>/views/imagens/planos-mais-informacoes.png" alt="Mais informações"></span>
              <div class="detalhe-feature">
                  O Dietwin disponibiliza ferramentas que facilitam a prescrição da dieta. Dentre elas: modelos de cardápios, lista de substituição, lista de alimentos e receitas favoritas e um amplo cadastro de alimentos e receitas.
O Dietwin Plus une todas estas ferramentas para detalhar ou refinar os resultados para a análise dietética.

              </div>
            </div>
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="Positivo">
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="Positivo">
          </td>
        </tr>
        <tr>
          <td class="explicacao-feature">
            <img src="<?php echo SITE_URL ?>/views/imagens/prescricao-dieta-ico.png" alt="Prescricao-dieta">
            <div class="conteudo-feature">
              <span class="titulo-feature">Análise dietética <img style="float:right; margin:-2px 305px 0 0;" src="<?php echo SITE_URL ?>/views/imagens/planos-mais-informacoes.png" alt="Mais informações"></span>

              <div class="detalhe-feature">
                  O Dietwin Plus realiza um amplo monitoramento de nutrientes no momento da prescrição dietética. Além disso, ele verifica conflitos alimentares, alertando para alergias, aversão ou intolerância a alimentos. Contando com a sinalização de características especiais de cada alimento.
Possui uma ferramenta que auxilia na estratégia nutricional em que se determina um Quadro de Análise de Valores Previstos e Encontrados (QAVP/QAVE).
              </div>
            </div>            
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/negativo-ico.png" alt="Negativo">
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="Positivo">
          </td>
        </tr>
        <tr class="cinza-claro">
          <td class="explicacao-feature">
            <img src="<?php echo SITE_URL ?>/views/imagens/analise-dietetica-ico.png" alt="Análise Dietética">
            <div class="conteudo-feature">
              <span class="titulo-feature">Multitasking e Personalização <img style="float:right; margin:-2px 190px 0 0;" src="<?php echo SITE_URL ?>/views/imagens/planos-mais-informacoes.png" alt="Mais informações"></span>
              <div class="detalhe-feature">
                  O Dietwin conta com um recurso exclusivo de ser multitarefas, que permite a abertura de inúmeras telas e consultas ao mesmo tempo.
              </div>
            </div>
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="Negativo">
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="Positivo">
          </td>
        </tr>
        
        <!--
        <tr class="cinza-claro">
          <td class="explicacao-feature">
            <img src="<?php echo SITE_URL ?>/views/imagens/estrategia-monitorada-ico.png" alt="Estratégias Nutricionais Monitoradas">
            <div class="conteudo-feature">
              <span class="titulo-feature">Estratégias Nutricionais Monitoradas</span>
              <div class="detalhe-feature">

              </div>
            </div>
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="positivo">
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="Positivo">
          </td>
        </tr>
        -->
        <!--
        <tr>
          <td class="explicacao-feature">
            <img src="<?php echo SITE_URL ?>/views/imagens/avaliacao-clinica-2-ico.png" alt="Avaliação Clínica">
            <div class="conteudo-feature">
              <span class="titulo-feature">Avaliação Clínica</span>
              <div class="detalhe-feature">

              </div>
            </div>
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="positivo">
          </td>
          <td class="text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/positivo-ico.png" alt="Positivo">
          </td>
        </tr>
        -->
        <tr class="cinza-claro">
          <td class="explicacao-feature">

          </td>
          <td class="text-center">
            <div class="preco">
              <!-- <span class="aviso">De <strike>R$ 454,00</strike> por</span> -->
              <span class="moeda">R$</span><span class="reais"><?php echo str_replace('.00','',(string)$dietwin_valor); ?>,</span><span class="centavos">00</span>
            </div>
            <div class="parcelamento">
              Boleto ou até <b>3x</b><br /> sem juros.
            </div>
          </td>
          <td class="text-center">
            <div class="preco">
                <!-- <span class="aviso">De <strike>R$ 795,00</strike> por</span> -->
              <span class="moeda">R$</span><span class="reais"><?php echo str_replace('.00','',(string)$dietwinplus_valor); ?>,</span><span class="centavos">00</span>
            </div>
            <div class="parcelamento">
              Boleto ou até <b>3x</b><br /> sem juros.
            </div>
          </td>
        </tr>
        <tr>
          <td class="explicacao-feature">

          </td>
          <td class="text-center">
            <a href="<?php echo SITE_URL ?>/dietwin/produto/dietwin" class="botao-comprar" title="Comprar agora">
              <img src="<?php echo SITE_URL ?>/views/imagens/botao-comprar.png" alt="Comprar agora"> Comprar agora
            </a>
          </td>
          <td class="text-center">
            <a href="<?php echo SITE_URL ?>/dietwin/produto/dietwin-plus" class="botao-comprar" title="Testar grátis!">
              <img src="<?php echo SITE_URL ?>/views/imagens/botao-comprar.png" alt="Testar grátis!"> Comprar agora
            </a>
          </td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>
</section>

<?php require_once 'includes/footer.php' ?>
</body>
</html>