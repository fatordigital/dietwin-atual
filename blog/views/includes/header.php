<header>
	<div class="logos">
		<div class="container">
			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-8">
				<img src="<?php echo SITE_URL ?>/views/imagens/dietwin-logo.png" alt="dietWin - Software de Nutrição" class="img-responsive dietwin-logo">
			</div>
			<div class="col-lg-offset-5 col-lg-3 col-md-offset-5 col-md-2 hidden-sm hidden-xs">
				<img src="<?php echo SITE_URL ?>/views/imagens/windows-logo.png" alt="Windows 8" class="img-responsive windows-logo">
			</div>
		</div>
	</div>
	<div class="container">
		<div class="col-lg-offset-1 col-lg-9 col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
			<h1>Tudo o que você procura em um software de nutrição.</h1>
		</div>
		<div class="col-lg-offset-1 col-lg-8 col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
			<div class="botoes-chamada">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<a href="<?php echo SITE_URL ?>/planos" class="botao-comprar" title="Comprar agora">
							<img src="<?php echo SITE_URL ?>/views/imagens/botao-comprar.png" alt="Comprar agora"> Comprar agora
						</a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<a href="#teste-gratis" class="botao-testar" title="Testar grátis!">
							<img src="<?php echo SITE_URL ?>/views/imagens/botao-teste.png" alt="Testar grátis!"> Testar grátis!
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<a href="#comeco-conteudo" class="conhecer-software" title="Conheça o nosso software">
	<img src="<?php echo SITE_URL ?>/views/imagens/setas.png" alt="Conheça o nosso software">
	Conheça o nosso <span>software</span>
</a>