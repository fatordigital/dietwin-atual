<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php'; ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

        <!-- Page Head -->
		<h2>Informações de uma transação</h2>
		<p id="page-intro">Abaixo estão as informações de uma transação, compra e cliente. Se for possível, os estados anteriores da transação também serão apresentados.</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Transação</h3>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<p>
					<strong>Data</strong><br />
					<?php echo $transacao ? date('d/m/Y \à\s H:i:s', strtotime($transacao->data)) : '' ?>
				</p>

				<p>
					<strong>ID no PagSeguro</strong><br />
					<?php echo $transacao ? $transacao->identificador : '' ?>
				</p>

				<p>
					<strong>Situação atual</strong><br />
					<?php echo $transacao ? $transacao->transacao_status : '' ?>
				</p>

				<p>
					<strong>Tipo de pagamento</strong><br />
					<?php echo $transacao ? $transacao->pagamento_tipo : '' ?>
				</p>

				<p>
					<strong>Meio de pagamento</strong><br />
					<?php echo $transacao ? $transacao->pagamento_meio : '' ?>
				</p>

				<p>
					<strong>Código da compra</strong><br />
					<a href="<?php echo SITE_URL.'/fatorcms/compras/visualizar/'.$transacao->compra_id ?>"><?php echo $transacao ? $transacao->compra_codigo : '' ?></a>
				</p>

				<p>
					<strong>Cliente</strong><br />
					<a href="<?php echo SITE_URL.'/fatorcms/clientes/editar/'.$transacao->cliente_id ?>"><?php echo $transacao ? $transacao->cliente_nome : '' ?></a>
				</p>

			</div>

		</div> <!-- End .content-box -->


		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Situações anteriores</h3>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<table>

					<thead>
						<tr>
							<th>Data</th>
							<th>Situação</th>
						</tr>
					</thead>

					<tfoot>
						<tr>
							<td colspan="2">

								<div class="clear"></div>
							</td>
						</tr>
					</tfoot>

					<tbody>
						<?php
						if ($transacoes_anteriores AND count($transacoes_anteriores) > 0)
						{
							foreach ($transacoes_anteriores as $transacao_anterior)
							{
							?>
								<tr>
									<td><?php echo date('d/m/Y \à\s H:i:s', strtotime($transacao_anterior->data)) ?></td>
									<td><?php echo $transacao_anterior->transacao_status ?></td>
								</tr>
							<?php
							}
						}
						else
						{
							echo '<tr><td colspan="2">Nenhum histórico de atualizações até o momento.</td></tr>';
						}
						?>
					</tbody>

				</table>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>