<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_Novidades extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai e testa se o usuário está logado
	 */
	public function __construct()
	{
		parent::__construct();
		$this->verificar_usuario_logado();
		$this->verificar_usuario_permissao('novidades');
	}


	/**
	 * Método padrão que não deve ser chamado diretamente. Se for, redireciona para a listagem
	 * @return void
	 */
	public function index()
	{
		header('Location: '.SITE_URL.'/fatorcms/novidades/listar');
		exit;
	}


	/**
	 * Método inicial que exibe a lista de registros do banco de dados
	 * @param $parametros object
	 * @return void
	 */
	public function listar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'novidades-lista.php'));
		$view->adicionar('body_class', 'novidades lista');
		$view->adicionar('lateral_menu', array('item'=>'novidades','link'=>'listar todas'));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$this->view_adicionar_obrigatorias($view);



		//-----

		// Foi feita ou "contém" uma busca
		$clausula_where = ' TRUE ';
		$buscar = NULL;
		if (isset($parametros->buscar) AND strlen(trim($parametros->buscar)) > 0)
		{
			$buscar = trim($parametros->buscar);
			$buscar_aux = Funcoes::mysqli_escape(str_replace(' ', '%', $buscar));
			$clausula_where .= ' AND (titulo LIKE "%'.$buscar_aux.'%" OR resumo LIKE "%'.$buscar_aux.'%" OR conteudo LIKE "%'.$buscar_aux.'%")';
		}

		//-----

		// Ordenação dos resultados, podendo vir da página
		
		$ordenar_por = (isset($parametros->ordenar_por)) ? strtolower($parametros->ordenar_por) : 'data'; // Valor padrão
		$ordem = isset($parametros->ordem) ? strtolower($parametros->ordem) : 'DESC'; // Valor padrão

		switch ($ordenar_por)
		{
			case 'data': $ordenar_por_sql = 'data'; break;
			case 'titulo': $ordenar_por_sql = 'titulo'; break;
			case 'visivel-no-site': $ordenar_por_sql = 'eh_visivel'; break;
            default: $ordenar_por_sql = $ordenar_por; break;
		}

		//-----

		$novidade = new FatorCMS_Model_Novidade;

		// Define a cláusula do select
		$novidade->clausula = 'SELECT * FROM {tabela_nome} WHERE '.$clausula_where.' ORDER BY '.$ordenar_por_sql.' '.$ordem;

		// Através da paginação vai verificar se existe a necessidade de dividir o resultado em várias páginas e se é necessário adicionar um LIMIT
		$paginacao = new Paginacao($novidade, $parametros, 15, 10);

		// Executa a cláusula lá de cima
		$novidades = $novidade->select(NULL, TRUE); // NULL para não alterar a clásula já definida e TRUE para forçar retorno em array

		//-----

		

		$view->adicionar('buscar', $buscar);
		$view->adicionar('ordenar_por', $ordenar_por);
		$view->adicionar('ordem', $ordem);
		$view->adicionar('novidades', $novidades);
		$view->adicionar('paginacao', $paginacao);
		

		$view->exibir();
	}


	/**
	 * Dependendo do parâmetro, apresenta a tela de edição ou de cadastro
	 * @param $parametros object
	 * @return void
	 */
	public function editar($parametros)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'novidades-form.php'));

        $view->adicionar('body_class', 'novidades form');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
		$this->view_adicionar_obrigatorias($view);

		$categorias = new FatorCMS_Model_NovidadeCategoria;
		$categorias = $categorias->select('SELECT * FROM {tabela_nome}');
		$view->adicionar('categorias', $categorias);

        // Decide o que fazer
        if (isset($parametros->_0) AND is_numeric($parametros->_0))
        {
            // É atualização
            $novidade = new FatorCMS_Model_Novidade;
	        $novidade = $novidade->select('SELECT * FROM {tabela_nome} WHERE id = '.Funcoes::mysqli_escape($parametros->_0));
            $view->adicionar('novidade', $novidade);

            $view->adicionar('lateral_menu', array('item'=>'novidades','link'=>''));

            $script = $this->novidade_buscar_tags($novidade->id);
            $view->adicionar('head_script', isset($script) ? $script : NULL);
        }
        else
        {
            // É cadastro
			$view->adicionar('novidade', NULL);
			$view->adicionar('lateral_menu', array('item'=>'novidades','link'=>'cadastrar'));

            $script = $this->novidade_buscar_tags();
            $view->adicionar('head_script', isset($script) ? $script : NULL);
        }

        $view->exibir();
    }

    public function teste_imagem($parametros){

    	if(!isset($parametros->_0)){
    		echo 'Coloque um ID de um post na URL para ver as imagens.<br /><br />';
    		echo '<strong>Exemplo:</strong> /fatorcms/novidades/teste-imagem/17';
    		exit;
    	}

    	$novidade = new FatorCMS_Model_Novidade;
	    $novidade = $novidade->select('SELECT * FROM {tabela_nome} WHERE id = '.Funcoes::mysqli_escape($parametros->_0));
        
        // arquivos/novidades/thumb1/
    	
    	echo '<img src="http://www.dietwin.com.br/arquivos/novidades/'.$novidade->imagem.'" />';
    	echo '<hr />';  
    	echo '<img src="http://www.dietwin.com.br/arquivos/novidades/thumb1/'.$novidade->imagem.'" />';
    	echo '<hr />';
    	echo '<img src="http://www.dietwin.com.br/arquivos/novidades/thumb2/'.$novidade->imagem.'" />';
    	echo '<hr />';
    }

	/**
	 * Recebe os parâmetros do formulário e adiciona o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function cadastrar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'novidades-form.php'));
		$this->view_adicionar_obrigatorias($view);

				//-----

		if ($parametros AND isset($parametros->titulo))
		{
            //print_r($parametros->as_values_tags); exit;

            $data = explode(" ", trim($parametros->data_time));
			$data[0] = explode("/", $data[0]);
			$data[0] = $data[0][2]."-".$data[0][1]."-".$data[0][0];
			$data = $data[0]." ".$data[1];			
			$parametros->data = $data;

			$novidade = new FatorCMS_Model_Novidade;
			$novidade->carregar($parametros);

			// Campos especiais
			$novidade->titulo_seo = Funcoes::texto_para_seo($parametros->titulo);

			//-----

			// Verifica se o titulo_seo está disponível, se não inicia o contador até achar um
			$cont = 2;
			do
			{
				$novidade_existe = new FatorCMS_Model_Novidade;
				$novidade_existe = $novidade_existe->select('SELECT id FROM {tabela_nome} WHERE titulo_seo = "'.$novidade->titulo_seo.'"');
				if ($novidade_existe AND ! is_null($novidade_existe->id))
				{
					$novidade->titulo_seo = Funcoes::texto_para_seo($novidade->titulo).'-'.$cont++;
				}
			} while ($novidade_existe AND ! is_null($novidade_existe->id));

			//-----

			// Log
			$novidade->log_id = $this->cms_usuario_logado->id;
			$novidade->log_data = date('Y-m-d H:i:s');

			//-----

            $novidade->colunas_mysqli_escape();

			if ($novidade->verificar_null(array('id')))
			{
				// Validações ---

				// Testamos o tamanho e as extensões
				if (isset($_FILES['imagem']) AND $_FILES['imagem']['size'] > 0 )
				{
					// Pegamos a string que vem depois do último ponto
					$imagem_extensao = strtolower(substr($_FILES['imagem']['name'], strrpos($_FILES['imagem']['name'],'.')+1));
					if ($_FILES['imagem']['size'] > 1048576)
					{
						$notificacao = new Notificacao('Somente são aceitas imagens com até <strong>1mb</strong> de tamanho.');
					}
					elseif ( ! in_array($imagem_extensao, array('jpg','png')))
					{
						$notificacao = new Notificacao('Somente são aceitas imagens do tipo <strong>JPG</strong>.');
					}
				}

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
                     $this->objeto_adicionar_fotos($novidade);

					if ($novidade->insert())
					{
                        $this->objeto_fotos_salvar($novidade);

                        $this->relacionar_tags($novidade->id, $parametros->as_values_tags);

						new Notificacao('Novidade <strong>'.$novidade->titulo.'</strong> cadastrada com sucesso.', 'success', TRUE);
						header('Location: '.SITE_URL.'/fatorcms/novidades/listar');
						exit;
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao salvar os dados da Novidade.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos os campos marcados são obrigatórios.');
			}

			// Para retornar ao formulário com valores inseridos
			$view->adicionar('novidade', $novidade);

			// Para retornar ao formulário com as tags
			$script = $this->post_buscar_tags($parametros->as_values_tags);

			$view->adicionar('head_script', $script);
		}
		else
		{
			$view->adicionar('novidade', NULL);

			// Traz "nenhuma" tag
			$script = $this->novidade_buscar_tags();
			$view->adicionar('head_script', $script);
		}

		//-----

		$categorias = new FatorCMS_Model_NovidadeCategoria;
		$categorias = $categorias->select('SELECT * FROM {tabela_nome}');
		$view->adicionar('categorias', $categorias);



		$view->adicionar('body_class', 'novidades formulario');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
        $view->adicionar('lateral_menu', array('item'=>'novidades','link'=>'cadastrar'));

        $view->exibir();
	}


	/**
	 * Recebe os parâmetros do formulário e atualiza o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function atualizar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'novidades-form.php'));
		$this->view_adicionar_obrigatorias($view);

		//-----

		if (isset($parametros->id) AND is_numeric($parametros->id))
		{
			$data = explode(" ", trim($parametros->data_time));
			$data[0] = explode("/", $data[0]);
			$data[0] = $data[0][2]."-".$data[0][1]."-".$data[0][0];
			$data = $data[0]." ".$data[1];			
			$parametros->data = $data;

			$novidade = new FatorCMS_Model_Novidade;
			$novidade->carregar($parametros);

			// Campos especiais
			$novidade->titulo_seo = Funcoes::texto_para_seo($novidade->titulo);

			//-----

			// Verificamos se o titulo_seo de antes é o igual ao novo. Se for diferente verificamos a possibilidade
			$novidade_atual = new FatorCMS_Model_Novidade($novidade->id);
			if ($novidade->titulo_seo != $novidade_atual->titulo_seo)
			{
				// Verifica se o titulo_seo está disponível (desde que não seja o do próprio registro), se não inicia o contador até achar um
				$cont = 2;
				do
				{
					$novidade_existe = new FatorCMS_Model_Novidade;
					$novidade_existe = $novidade_existe->select('SELECT id FROM {tabela_nome} WHERE titulo_seo = "'.$novidade->titulo_seo.'" AND id != '.$novidade->id);
					if ($novidade_existe AND ! is_null($novidade_existe->id))
					{
						$novidade->titulo_seo = Funcoes::texto_para_seo($novidade->titulo).'-'.$cont++;
					}
				} while ($novidade_existe AND ! is_null($novidade_existe->id));
			}

			//-----

			// Log
			$novidade->log_id = $this->cms_usuario_logado->id;
			$novidade->log_data = date('Y-m-d H:i:s');


			//-----

			$novidade->colunas_mysqli_escape();

			if ($novidade->verificar_null())
			{
				// Validações ---

				// Testamos o tamanho e as extensões
				if (isset($_FILES['imagem']) AND $_FILES['imagem']['size'] > 0 )
				{
					// Pegamos a string que vem depois do último ponto
					$imagem_extensao = strtolower(substr($_FILES['imagem']['name'], strrpos($_FILES['imagem']['name'],'.')+1));
					if ($_FILES['imagem']['size'] > 3048576)
					{
						$notificacao = new Notificacao('Somente são aceitas imagens com até <strong>1mb</strong> de tamanho.');
					}
					elseif ( ! in_array($imagem_extensao, array('jpg','png','gif')))
					{
						$notificacao = new Notificacao('Somente são aceitas imagens dos tipos <strong>JPG</strong>, <strong>PNG</strong> ou <strong>GIF</strong>.');
					}
				}

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					$this->objeto_adicionar_fotos($novidade);

					// Se veio uma nova foto, prepara para excluir a antiga
					if ( ! is_null($novidade->imagem))
					{
						$novidade_foto_antiga = new FatorCMS_Model_Novidade($novidade->id);
					}

					if ($novidade->update())
					{
                        // Se for o caso, exclui a foto antiga
						if (isset($novidade_foto_antiga) AND $novidade->imagem != $novidade_foto_antiga->imagem)
						{
							$this->imagem_excluir_arquivos($novidade_foto_antiga->imagem);
						}

                        $this->objeto_fotos_salvar($novidade);

						$this->relacionar_tags($novidade->id, $parametros->as_values_tags);
                        
						new Notificacao('Novidade <strong>'.$novidade->titulo.'</strong> atualizada com sucesso.', 'success', TRUE);
						header('Location: '.SITE_URL.'/fatorcms/novidades/listar');
						exit;
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro durante a atualização da Novidade.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos os campos marcados são obrigatórios.');
			}

			// Se for o caso definimos no objeto a imagem atual
			if (is_null($novidade->imagem))
			{
				$novidade->imagem = $novidade_atual->imagem;
			}
			// Para retornar ao formulário com valores inseridos
			$view->adicionar('novidade', $novidade);
		}
		else
		{
			$notificacao = new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention');
		}
        
        $script = $this->post_buscar_tags($parametros->as_values_tags);
        $view->adicionar('head_script', isset($script) ? $script : NULL);

		$view->adicionar('body_class', 'novidades formulario');
		$view->adicionar('lateral_menu', array('item'=>'novidades','link'=>''));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

        $view->exibir();
	}


	/**
	 * Recebe o ID do registro e excluir ele do banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function excluir($parametros)
	{
		if (isset($parametros->_0) AND is_numeric($parametros->_0))
		{
			$novidade = new FatorCMS_Model_Novidade($parametros->_0);

			if ($novidade AND ! is_null($novidade->id))
			{
				// Apagamos a novidade do banco de dados
				if ($novidade->delete())
				{
					// Apaga as imagens da  novidade
					@unlink('arquivos/novidades/'.$novidade->imagem);
					@unlink('arquivos/novidades/thumb1/'.$novidade->imagem);
					@unlink('arquivos/novidades/thumb2/'.$novidade->imagem);

					// Excluímos qualquer vínculo entre tags e a novidade
					$novidade_tag = new FatorCMS_Model_NovidadeTag;
					$novidade_tag->novidade_id = $novidade->id;
					$novidade_tag->delete();

					new Notificacao('Novidade <strong>'.$novidade->titulo.'</strong> excluída com sucesso.', 'success', TRUE);
				}
				else
				{
					new Notificacao('Ocorreu um erro ao excluir a novidade <strong>'.$novidade->titulo.'</strong>.', 'error', TRUE);
				}
			}
			else
			{
				new Notificacao('Novidade não encontrada.', 'error', TRUE);
			}
		}
		else
		{
			new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention', 'error', TRUE);
		}

		header('Location: '.SITE_URL.'/fatorcms/novidades');
		exit;
	}


	/**
	 * Recebe o id do registro por parâmetro, exclui o arquivo e atualiza o banco de dados
	 * @param $parametros
	 * @return void
	 */
	public function imagem_excluir($parametros)
	{
		// Se houve envio do formulário, entra aqui
		if (isset($parametros->_0))
		{
			if (is_numeric($parametros->_0))
			{
				$objeto_id = (int) $parametros->_0;

				// Cria um novo objeto e carrega a Promoção
				$objeto = new FatorCMS_Model_Novidade($objeto_id);

				// Busca o nome da foto que será excluída
				$foto_arquivo = $objeto->imagem;

				// Cria outro objeto só para fazer a atualização
				$objeto_update = new FatorCMS_Model_Novidade;
				$objeto_update->id = $objeto->id;

				// Tenta excluir os arquivos
				if (@unlink('arquivos/novidades/'.$foto_arquivo) AND @unlink('arquivos/novidades/thumb1/'.$foto_arquivo) AND @unlink('arquivos/novidades/thumb2/'.$foto_arquivo))
				{
					// Tenta atualizar o registro dizendo que a coluna "foto" deve ter o valor NULL
					if ($objeto_update->update(array('imagem')))
					{
						new Notificacao('A imagem foi excluída com sucesso.', 'success', TRUE);
					}
					else
					{
						new Notificacao('Ocorreu um erro ao excluir a foto do evento do banco de dados.','error',TRUE);
					}
				}
				else
				{
					new Notificacao('Ocorreu um erro ao excluir a foto do evento do diretório de imagens.','error',TRUE);
				}
			}
			else
			{
				new Notificacao('Identificador de novidade inválido ao tentar excluir a foto.','error', TRUE);
			}

			header('Location: '.SITE_URL.'/fatorcms/novidades/editar/'.$parametros->_0);
			exit;
		}
		else
		{
			// Se não houve envio de um id, exibe a página de listagem novamente
			header('Location: '.SITE_URL.'/fatorcms/novidades');
			exit;
		}
	}


	/****************************** MÉTODOS EXTRAS ******************************/


    /**
    * Adiciona o novo nome da imagem ao objeto
    * @param $objeto
    * @return void
    */
    private function objeto_adicionar_fotos(&$objeto)
    {
        if( isset($_FILES['imagem']) AND $_FILES['imagem']['size']> 0)
        {
            $objeto->imagem = $objeto->titulo_seo.'.jpg';
        }
        else
        {
            $objeto->imagem = NULL;
        }
    }


    /**
    * Redimensionamos a imagem e criamos as thumbs (se for o caso)
    * @param $objeto
    * @return void
    */
    private function objeto_fotos_salvar($objeto)
    {
	    include_once 'biblioteca/WideImage/WideImage.php';
	    
        $imagem_nome = $objeto->imagem;

        if( ! is_null($imagem_nome))
        {
            $foto = WideImage::loadFromFile($_FILES['imagem']['tmp_name']);

/*
            $foto_nova = $foto->resize(800, NULL, 'outside', 'down');
            $foto_nova_thumb_1 = $foto->resize(202, 116, 'outside')->crop('center', 'center', 202, 116);
            $foto_nova_thumb_2 = $foto->resize(230, NULL, 'outside');
*/
            $foto_nova = $foto->resize(700, NULL, 'outside', 'down')->crop('top', 'center', 700, 382);
            $foto_nova_thumb_1 = $foto->resize(500, NULL, 'outside')->crop('top', 'center', 500, 275);
            $foto_nova_thumb_2 = $foto->resize(300, NULL, 'outside')->crop('top', 'center', 300, 165);

            $foto_nova->saveToFile('arquivos/novidades/'.$imagem_nome, 80);
            $foto_nova_thumb_1->saveToFile('arquivos/novidades/thumb1/'.$imagem_nome, 85);
            $foto_nova_thumb_2->saveToFile('arquivos/novidades/thumb2/'.$imagem_nome, 85);
        }
    }


    /**
     * Recebe o nome do arquivo da foto por parâmetro e exclui os arquivos do disco
     * @param $imagem_arquivo
     * @return void
     */
    public function imagem_excluir_arquivos($imagem_arquivo)
    {
	    // Se houve envio do formulário, entra aqui
		if ( ! empty($imagem_arquivo))
		{
			// Tenta excluir os arquivos
			if ( ! @unlink('arquivos/novidades/'.$imagem_arquivo) OR ! @unlink('arquivos/novidades/thumb1/'.$imagem_arquivo) OR ! @unlink('arquivos/novidades/thumb2/'.$imagem_arquivo))
			{
				new Log('Erro ao excluir os arquivos da foto:'.$imagem_arquivo);
			}
		}
    }


    /**
	 * Cuida do relacionamento entre tags e produto. Realiza a exclusão dos relacionamentos prévios e cria e recria eles
	 * @param $novidade_id
	 * @param $tags
	 * @return bool Retorna TRUE acontecendo pelo menos uma inserção
	 */
	protected function relacionar_tags($novidade_id, $tags)
	{
		$retorno = FALSE;

		// Apaga todas as tags relacionadas ao produto
		$novidade_tag = new FatorCMS_Model_NovidadeTag;
		$novidade_tag->novidade_id = $novidade_id;

		if ($novidade_tag->delete())
		{

			$tags = explode(',', $tags);

			if (count($tags) > 0)
			{
				// Percorre cada tag para fazer o relacionamento de novo, se for o caso
				foreach ($tags as $item)
				{
					if (mb_strlen(trim($item)) > 0)
					{
						$item = Funcoes::mysqli_escape(trim($item));

						// Verifica se o ID da tag ou o nome ou o nome_seo existem
						$tag_existe = new FatorCMS_Model_Tag;
						$tag_existe = $tag_existe->select('SELECT id FROM {tabela_nome} WHERE (LOWER(nome) = "'.mb_strtolower($item).'" OR nome_seo = "'.Funcoes::texto_para_seo($item).'")');

						if ( ! $tag_existe OR is_null($tag_existe->id))
						{
							// Não existe, vamos adicionar
							$tag = new FatorCMS_Model_Tag;
							$tag->nome = $item;
							$tag->nome_seo = Funcoes::texto_para_seo($item);

							if ($tag->insert())
							{
								// Pode acontecer um só inserção, mas já está valendo
								$retorno = TRUE;
							}
							else
							{
								$log = new Log('Erro ao adicionar uma tag no banco. $tag:'.var_export($item, TRUE));
							}
						}
						else
						{
							// Copia o objeto para uso no insert abaixo
							$tag = $tag_existe;
						}

						// Se não houve problemas, faz o relacionamento
						if ( ! isset($log))
						{
							$novidade_tag = new FatorCMS_Model_NovidadeTag;
							$novidade_tag->novidade_id = $novidade_id;
							$novidade_tag->tag_id = $tag->id;

							if ( ! $novidade_tag->insert())
							{
								$log = new Log('Erro ao relacionar tag com novidade. $tag_id='.$tag->id.'|$novidade_id'.$novidade_id);
							}
						}
					}
				}
			}
			else
			{
				// Se não veio nenhum, pode ser apenas exclusão de todos, uma limpeza
				$retorno = TRUE;
			}
		}
		else
		{
			new Log('Erro ao excluir as tags de uma novidade. $novidade_id:'.$novidade_id);
		}

		return $retorno;
	}


    /**
	 * Busca todos as tags que foram enviadas pelo formulário. Função utilizada quando a página do formulário tem que ser recarregada
	 * @param $tags
	 * @return string
	 */
	protected function post_buscar_tags($tags)
	{
		// Monta as informações para o javascript
		$script = 'var tags = {itens: [';
		if ($tags AND strlen($tags) > 0)
		{
			$tags = explode(',', $tags);

			foreach ($tags as $tag)
			{
				if (strlen(trim($tag)) > 0)
				{
					$script .= '{nome: "'.$tag.'"},';
				}
			}
			$script = substr($script, 0, -1); // Retira a última vírgula
		}
		$script .= ']};';
        return $script;
    }


    /**
	 * Busca todas as tags relacionadas com a noi
	 * @param $novidade_id
	 * @return array|bool
	 */
	protected function novidade_buscar_tags($novidade_id = NULL)
	{
		if ( ! is_null($novidade_id))
		{
			// Busca as tags do produto
			$tag = new FatorCMS_Model_Tag;
			$tags = $tag->select('
				SELECT tag.nome
				FROM {tabela_prefixo}novidades_tags AS novidade_tag
					LEFT JOIN {tabela_nome} AS tag ON novidade_tag.tag_id = tag.id
				WHERE novidade_tag.novidade_id = ' . $novidade_id . ' ORDER BY tag.nome
			', TRUE);
		}
		else
		{
			$tags = FALSE;
		}

		// E monta as informações para o javascript
		$script = 'var tags = {itens: [';
		if ($tags AND count($tags) > 0)
		{
			foreach ($tags as $tag)
			{
				$script .= '{nome: "'.$tag->nome.'"},';
			}
			$script = substr($script, 0, -1); // Retira a última vírgula
		}
		$script .= ']};';

		return $script;
	}


	public function categorias(){

		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'novidades-categorias-lista.php'));
		$view->adicionar('body_class', 'novidades lista');
		$view->adicionar('lateral_menu', array('item'=>'novidades','link'=>'listar todas categorias'));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$this->view_adicionar_obrigatorias($view);


		$categorias = new FatorCMS_Model_NovidadeCategoria;

		$categorias = $categorias->select("SELECT * FROM {tabela_nome}"); // NULL para não alterar a clásula já definida e TRUE para forçar retorno em array

		//-----
		$view->adicionar('categorias', $categorias);
		

		$view->exibir();
	}

	public function categorias_cadastrar($parametros){
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'novidades-categorias-form.php'));
		$this->view_adicionar_obrigatorias($view);

				//-----

		if ($parametros AND isset($parametros->nome))
		{
            //print_r($parametros->as_values_tags); exit;

			$novidade = new FatorCMS_Model_NovidadeCategoria;
			$novidade->carregar($parametros);

			// Campos especiais
			$novidade->tag = Funcoes::texto_para_seo($parametros->nome);

			//-----

			// Verifica se o titulo_seo está disponível, se não inicia o contador até achar um
			$cont = 2;
			do
			{
				$novidade_existe = new FatorCMS_Model_NovidadeCategoria;
				$novidade_existe = $novidade_existe->select('SELECT id FROM {tabela_nome} WHERE tag = "'.$novidade->tag.'"');

				//print('<pre>'); print_r($novidade_existe); die;

				if ($novidade_existe AND ! is_null($novidade_existe->id))
				{
					$novidade->tag = Funcoes::texto_para_seo($novidade->nome).'-'.$cont++;
				}
			} while ($novidade_existe AND ! is_null($novidade_existe->id));


            $novidade->colunas_mysqli_escape();

			if ($novidade->verificar_null(array('id')))
			{
				// Validações ---

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
                    
					if ($novidade->insert())
					{
                    
						new Notificacao('Novidade <strong>'.$novidade->nome.'</strong> cadastrada com sucesso.', 'success', TRUE);
						header('Location: '.SITE_URL.'/fatorcms/novidades/categorias');
						exit;
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao salvar os dados da Novidade.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos os campos marcados são obrigatórios.');
			}

			// Para retornar ao formulário com valores inseridos
			$view->adicionar('novidade', $novidade);

			$view->adicionar('head_script', $script);
		}
		else
		{
			$view->adicionar('novidade', NULL);

			//$view->adicionar('head_script', $script);
		}

		//-----

		$categorias = new FatorCMS_Model_NovidadeCategoria;
		$categorias = $categorias->select('SELECT * FROM {tabela_nome}');
		$view->adicionar('categorias', $categorias);



		$view->adicionar('body_class', 'novidades formulario');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
        $view->adicionar('lateral_menu', array('item'=>'novidades','link'=>'cadastrar categorias'));

        $view->exibir();
	}

	public function categorias_editar($parametros){
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'novidades-categorias-form.php'));

        $view->adicionar('body_class', 'novidades form');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
		$this->view_adicionar_obrigatorias($view);

		// Decide o que fazer
        if (isset($parametros->_0) AND is_numeric($parametros->_0))
        {
            // É atualização
            $novidade = new FatorCMS_Model_NovidadeCategoria;
	        $novidade = $novidade->select('SELECT * FROM {tabela_nome} WHERE id = '.Funcoes::mysqli_escape($parametros->_0));
            $view->adicionar('novidade', $novidade);

            $view->adicionar('lateral_menu', array('item'=>'novidades','link'=>''));
        }
        else
        {
            // É cadastro
			$view->adicionar('novidade', NULL);
			$view->adicionar('lateral_menu', array('item'=>'novidades','link'=>'cadastrar categorias'));

            $script = $this->novidade_buscar_tags();
            $view->adicionar('head_script', isset($script) ? $script : NULL);
        }

        $view->exibir();
	}

	public function categorias_atualizar($parametros){
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'novidades-categorias-form.php'));
		$this->view_adicionar_obrigatorias($view);

		//-----

		if (isset($parametros->id) AND is_numeric($parametros->id))
		{
			$novidade = new FatorCMS_Model_NovidadeCategoria;
			$novidade->carregar($parametros);

			// Campos especiais
			$novidade->tag = Funcoes::texto_para_seo($novidade->nome);

			//-----

			// Verificamos se o titulo_seo de antes é o igual ao novo. Se for diferente verificamos a possibilidade
			$novidade_atual = new FatorCMS_Model_NovidadeCategoria($novidade->id);
			if ($novidade->tag != $novidade_atual->tag)
			{
				// Verifica se o titulo_seo está disponível (desde que não seja o do próprio registro), se não inicia o contador até achar um
				$cont = 2;
				do
				{
					$novidade_existe = new FatorCMS_Model_NovidadeCategoria;
					$novidade_existe = $novidade_existe->select('SELECT id FROM {tabela_nome} WHERE tag = "'.$novidade->tag.'" AND id != '.$novidade->id);
					if ($novidade_existe AND ! is_null($novidade_existe->id))
					{
						$novidade->tag = Funcoes::texto_para_seo($novidade->nome).'-'.$cont++;
					}
				} while ($novidade_existe AND ! is_null($novidade_existe->id));
			}

			//-----

			//-----

			$novidade->colunas_mysqli_escape();

			if ($novidade->verificar_null())
			{
				// Validações ---

				if ( ! isset($notificacao))
				{

					if ($novidade->update())
					{
                        // Se for o caso, exclui a foto antiga
                        
						new Notificacao('Novidade <strong>'.$novidade->titulo.'</strong> atualizada com sucesso.', 'success', TRUE);
						header('Location: '.SITE_URL.'/fatorcms/novidades/categorias');
						exit;
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro durante a atualização da Novidade.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos os campos marcados são obrigatórios.');
			}

			// Para retornar ao formulário com valores inseridos
			$view->adicionar('novidade', $novidade);
		}
		else
		{
			$notificacao = new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention');
		}
        
        $script = $this->post_buscar_tags($parametros->as_values_tags);
        $view->adicionar('head_script', isset($script) ? $script : NULL);

		$view->adicionar('body_class', 'novidades formulario');
		$view->adicionar('lateral_menu', array('item'=>'novidades','link'=>''));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

        $view->exibir();
	}

	public function categorias_excluir($parametros){

		if (isset($parametros->_0) AND is_numeric($parametros->_0))
		{
			$novidade = new FatorCMS_Model_NovidadeCategoria($parametros->_0);

			if ($novidade AND ! is_null($novidade->id))
			{
				// Apagamos a novidade do banco de dados
				if ($novidade->delete())
				{
					new Notificacao('Categoria <strong>'.$novidade->titulo.'</strong> excluída com sucesso.', 'success', TRUE);
				}
				else
				{
					new Notificacao('Ocorreu um erro ao excluir a Categoria <strong>'.$novidade->titulo.'</strong>.', 'error', TRUE);
				}
			}
			else
			{
				new Notificacao('Categoria não encontrada.', 'error', TRUE);
			}
		}
		else
		{
			new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention', 'error', TRUE);
		}

		header('Location: '.SITE_URL.'/fatorcms/novidades/categorias');
		exit;
	}

} // end class