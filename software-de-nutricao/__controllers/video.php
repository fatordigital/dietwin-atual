<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe

*/

class Controller_Video extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        if (isset($parametros->titulo_seo) AND strlen($parametros->titulo_seo) > 0 AND isset($parametros->categoria) AND strlen($parametros->categoria)>0)
        {
            if (Funcoes::mysqli_escape($parametros->categoria) == 'topicos-diversos')
            {
                $clausula = ' AND produto_id = 0';
                $classe = 'topicos-diversos';
                $categoria = 'Tópicos Diversos';
            }
            else
            {
                $produto = new Model_Produto();
                $produto->nome_seo = Funcoes::mysqli_escape($parametros->categoria);
                $produto = $produto->select("SELECT id,nome,nome_seo FROM {tabela_nome} WHERE nome_seo = 'dietwin-".$produto->nome_seo."'");
                if ($produto)
                {
                    $clausula = ' AND produto_id = '.$produto->id;
                    $categoria = substr($produto->nome,8);
                    $classe = Funcoes::mysqli_escape($parametros->categoria);
                }
                else
                {
                    $clausula = ' AND FALSE';
                    $classe = 'nao-encontrada';
                    $categoria = 'Não encontrada';
                }
            }
            $video = new Model_Video();
            $video = $video->select("SELECT {tabela_nome}.*,fd_produtos.nome_seo FROM {tabela_nome} LEFT JOIN fd_produtos ON fd_produtos.id = fd_videos.produto_id WHERE titulo_seo = '".Funcoes::mysqli_escape($parametros->titulo_seo)."'".$clausula);
            if (!$video)
            {
                header('HTTP/1.1 404 Not Found');
                $erro_404 = new Controller_Erro;
                $erro_404->index(404);
                exit;
            }
        }
        else
        {
            header('HTTP/1.1 404 Not Found');
            $erro_404 = new Controller_Erro;
            $erro_404->index(404);
            exit;
        }

        $view = new View('video.php');
		$this->view_variaveis_obrigatorias($view);

        $view->adicionar('video',$video);
        $view->adicionar('classe',$parametros->categoria);

		$view->adicionar('body_class', 'video');
		$view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Vídeo - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */


} // end class