<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_QualVersaoEscolho extends Controller_Padrao
{
    /**
     * Chama o construtor da classe pai
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método inicial que faz a renderização básica da página
     * @param $parametros
     * @return void
     */
    public function index($parametros)
    {

        $video = new Model_Video();
        $videos = $video->select('SELECT {tabela_nome}.*,fd_produtos.nome_seo, fd_produtos.nome FROM {tabela_nome} LEFT JOIN fd_produtos ON {tabela_nome}.produto_id = fd_produtos.id ORDER BY data DESC LIMIT 0,3',TRUE);

        $faq = new Model_FAQ();
        $faqs = $faq->select('SELECT {tabela_nome}.*,fd_produtos.nome_seo, fd_produtos.nome FROM {tabela_nome} LEFT JOIN fd_produtos ON {tabela_nome}.produto_id = fd_produtos.id ORDER BY pergunta ASC LIMIT 0,4',TRUE);

        $novidade = new Model_Novidade();
        $novidades = $novidade->select('SELECT * FROM {tabela_nome} ORDER BY data DESC LIMIT 0,4',TRUE);

        $view = new View('qual-versao-escolho.php');
        $this->view_variaveis_obrigatorias($view);

        $view->adicionar('videos', $videos);
        $view->adicionar('faqs', $faqs);
        $view->adicionar('novidades', $novidades);

        $view->adicionar('body_class', 'index');
        $view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        $view->exibir();
    }

    /* ***************************** MÉTODOS EXTRAS ***************************** */


} // end class