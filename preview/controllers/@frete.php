<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe

- Classe exclusiva para montagem e envio de emails, para reunir tudo em um só lugar e poupar os outros Controllers

*/

class Controller_Frete extends Controller_Padrao
{

    /**
     * Chama o construtor da classe pai
     */
    public function __construct()
    {
        parent::__construct();

        require_once 'biblioteca/SwiftMailer/swift_required.php';
    }

    /**
     * Método inicial que faz a renderização básica da página
     */
    public function index()
    {
        header('HTTP/1.1 403 Forbidden');
        exit;
    }

    /**
     * Método que retorna o valor do frete
     * @param $parametros
     * @return float
     */
    public function calcular($parametros)
    {
        #OFICINADANET###############################
        # Código dos Serviços dos Correios
        # 41106 PAC sem contrato
        # 40010 SEDEX sem contrato
        # 40045 SEDEX a Cobrar, sem contrato
        # 40215 SEDEX 10, sem contrato
        ############################################

        // TRATA OS CEP'S
        $cep_destino = (isset($parametros)?$parametros:NULL);
        $cep_origem = '90510002';// CEP DE QUEM ESTÁ ENVIANDO (LOJA - SE VC QUISER PODE PUXAR DO BANCO DE DADOS)

        $peso = '0,300';

        /*
         * TIPOS DE FRETE
         *
              41106 = PAC sem contrato
              40010 = SEDEX sem contrato
              40045 = SEDEX a Cobrar, sem contrato
              40215 = SEDEX 10, sem contrato
              40290 = SEDEX Hoje, sem contrato
              40096 = SEDEX com contrato
              40436 = SEDEX com contrato
              40444 = SEDEX com contrato
              81019 = e-SEDEX, com contrato
              41068 = PAC com contrato
         */
        //$webservice = 'http://shopping.correios.com.br/wbm/shopping/script/CalcPrecoPrazo.asmx?WSDL';// URL ANTIGA
        $webservice = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.asmx?WSDL';

        // TORNA EM OBJETO AS VARIAVEIS
        $parms = new stdClass;
        $parms->nCdServico = '40010';
        $parms->nCdEmpresa = '';// <- LOGIN DO CADASTRO NO CORREIOS (OPCIONAL)
        $parms->sDsSenha = '';// <- SENHA DO CADASTRO NO CORREIOS (OPCIONAL)
        $parms->StrRetorno = 'xml';

        // DADOS DINAMICOS
        $parms->sCepDestino = $cep_destino;// CEP CLIENTE
        $parms->sCepOrigem = $cep_origem;// CEP DA LOJA (BD)
        $parms->nVlPeso = $peso;

        // VALORES MINIMOS DO PAC (SE VC PRECISAR ESPECIFICAR OUTROS FAÇA ISSO AQUI)
        $parms->nVlComprimento = 16;
        $parms->nVlDiametro = 0;
        $parms->nVlAltura = 2;
        $parms->nVlLargura = 16;

        // OUTROS OBRIGATORIOS (MESMO VAZIO)
        $parms->nCdFormato = 1;
        $parms->sCdMaoPropria = 'N';
        $parms->nVlValorDeclarado = 0;
        $parms->sCdAvisoRecebimento = 'N';

        // Inicializa o cliente SOAP
        $soap = @new SoapClient($webservice, array(
            'trace' => true,
            'exceptions' => true,
            'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
            'connection_timeout' => 1000
        ));

        // Resgata o valor calculado
        $resposta = $soap->CalcPrecoPrazo($parms);
        $frete = $resposta->CalcPrecoPrazoResult->Servicos->cServico;

        return str_replace(',','.',$frete->Valor);
    }

}
