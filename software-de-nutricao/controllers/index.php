<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Index extends Controller_Padrao
{
	#private $carrinhoAbandonado;
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
		#$this->carrinhoAbandonado = new Controller_Abandonado();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
		//$this->carrinhoAbandonado->fakeCron();
		
	    $view = new View('index.php');
	    $this->view_variaveis_obrigatorias($view);

	    $novidade = new Model_Novidade;
		$novidades = $novidade->select("SELECT * FROM fd_novidades WHERE fd_novidades.alias = 'dietwin' AND fd_novidades.data <= '" . date('Y-m-d') . "' AND fd_novidades.eh_visivel=1 ORDER BY fd_novidades.data DESC LIMIT 0,2");
		
		$view->adicionar('novidades',$novidades);

		if(isset($_GET['dev'])){
			print('<pre>'); print_r($novidades); 
			echo $novidades->titulo_seo;

			die;
		}



		$view->adicionar('body_class', 'index');
		$view->adicionar('notificacao',new Notificacao());
    	$view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */

    /**
     * Método inicial que faz sumir imagem no include topo
     * @param $parametros
     * @return void
     */
    public function cookie($parametros)
    {
        setcookie('chat', 'true', time()+(3600*24*30), '/');
        //var_dump($_COOKIE['chat']); exit;
    }

} // end class