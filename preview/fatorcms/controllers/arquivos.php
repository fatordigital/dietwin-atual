<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_Arquivos extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai e testa se o usuário está logado
	 */
	public function __construct()
	{
		parent::__construct();
		$this->verificar_usuario_logado();
		$this->verificar_usuario_permissao('area-cliente');
	}


	/**
	 * Método padrão que não deve ser chamado diretamente. Se for, vai para a listagem
	 * @return void
	 */
	public function index()
	{
		header('Location: '.SITE_URL.'/fatorcms/arquivos/listar');
		exit;
	}


	/**
	 * Método inicial que exibe a lista de registros do banco de dados
	 * @param $parametros object
	 * @return void
	 */
	public function listar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'arquivos-lista.php'));
		$view->adicionar('body_class', 'arquivos lista');
		$view->adicionar('lateral_menu', array('item'=>'area-cliente','link'=>'listar arquivos'));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$this->view_adicionar_obrigatorias($view);

		//-----

		// Lógica caso tenha sido feita uma busca na listagem
		$clausula_where = ' TRUE ';

		$buscar = NULL;
		if (isset($parametros->buscar) AND strlen(trim($parametros->buscar)) > 0)
		{

			$buscar = trim($parametros->buscar);
			$buscar_aux = Funcoes::mysqli_escape(str_replace(' ', '%', $buscar));
			$clausula_where .= 'AND (arquivo.titulo LIKE "%'.$buscar_aux.'%" OR arquivo.descricao LIKE "%'.$buscar_aux.'%")';
		}

		//-----

		// Ordenação dos resultados, podendo vir da página
		$ordenar_por = (isset($parametros->ordenar_por)) ? strtolower($parametros->ordenar_por) : 'data'; // Valor padrão
		$ordem = isset($parametros->ordem) ? strtolower($parametros->ordem) : 'desc'; // Valor padrão

		switch ($ordenar_por)
        {
            case 'data': $ordenar_por_sql = 'arquivo.data'; break;
            case 'titulo': $ordenar_por_sql = 'arquivo.titulo'; break;
            case 'descricao': $ordenar_por_sql = 'arquivo.descricao'; break;
            default: $ordenar_por_sql = $ordenar_por; break;
        }

		//-----

		$arquivo = new FatorCMS_Model_Arquivo;
		$arquivo->clausula = '
			SELECT arquivo.*
			FROM {tabela_nome} AS arquivo
				WHERE '.$clausula_where.'
				ORDER BY '.$ordenar_por_sql.' '.$ordem
		;


		// Através da paginação vai verificar se existe a necessidade de dividir o resultado em várias páginas e se é necessário adicionar um LIMIT
		$paginacao = new Paginacao($arquivo, $parametros, 10, 10);

		// Executa a cláusula lá de cima
		$arquivos = $arquivo->select(NULL, TRUE); // NULL para não alterar a clásula já definida e TRUE para forçar retorno em array

		//-----

		$view->adicionar('buscar', $buscar);
		$view->adicionar('ordenar_por', $ordenar_por);
		$view->adicionar('ordem', $ordem);
		$view->adicionar('arquivos', $arquivos);
		$view->adicionar('paginacao', $paginacao);

		$view->exibir();
	}

    /**
	 * Dependendo do parâmetro, aprensenta a tela de edição ou de cadastro
	 * @param $parametros object
	 * @return void
	 */
	public function editar($parametros)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'arquivos-form.php'));

        $view->adicionar('body_class', 'arquivos form');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
        $this->view_adicionar_obrigatorias($view);

        // Decide o que fazer
        if (isset($parametros->_0) AND is_numeric($parametros->_0))
        {
            // É atualização
            $arquivo = new FatorCMS_Model_Arquivo;
            $arquivo = $arquivo->select('SELECT * FROM {tabela_nome} WHERE id = '.Funcoes::mysqli_escape($parametros->_0));
            $view->adicionar('arquivo', $arquivo);

            $view->adicionar('lateral_menu', array('item'=>'area-cliente','link'=>''));

            $script = $this->arquivo_buscar_clientes($arquivo->id);
            $view->adicionar('head_script', isset($script) ? $script : NULL);
        }
        else
        {
            // É cadastro
            $view->adicionar('arquivo', NULL);
            $view->adicionar('lateral_menu', array('item'=>'area-cliente','link'=>'cadastrar arquivo'));

            $script = $this->arquivo_buscar_clientes();
            $view->adicionar('head_script', isset($script) ? $script : NULL);
        }

        $view->exibir();
    }


	/**
	 * Recebe os parâmetros do formulário e adiciona o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function cadastrar($parametros)
	{
        if (isset($parametros->titulo) AND !empty($parametros->titulo))
        {
            if(! isset($parametros->habilitado)){
                $parametros->habilitado = '0';
            }

            $arquivo = new FatorCMS_Model_Arquivo();
            $arquivo->carregar($parametros);

            $arquivo->data = date('Y-m-d H:i:s');

            //-----

            $arquivo->colunas_mysqli_escape();

            if ($arquivo->verificar_null(array('id')))
            {
                // Validações ---

                // Fim das validações ---

                if ( ! isset($notificacao))
                {
                    if ($arquivo->insert())
                    {
                        $this->relacionar_clientes($arquivo->id, $parametros->as_values_clientes);

                        new Notificacao('Arquivo <strong>'.$arquivo->titulo.'</strong> cadastrado com sucesso.', 'success', TRUE);
                        echo json_encode(array('tipo'=>'success','id' => $arquivo->id));
                    }
                    else
                    {
                        echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao cadastrar o arquivo.'));
                    }
                }
            }
            else
            {
                echo json_encode(array('tipo'=>'error','mensagem'=>'Todos campos marcados são obrigatórios.'));
            }
            exit;
        }

        $view = new View(array('diretorios' => 'fatorcms', 'arquivo' => 'arquivos-form.php'));
		$this->view_adicionar_obrigatorias($view);

        // Traz "nenhuma" tag
        $script = $this->arquivo_buscar_clientes();
        $view->adicionar('head_script', $script);

		//-----

        $view->adicionar('arquivo', NULL);

        // E monta as informações para o javascript
        $script = 'var clientes = {itens: []};';
        $view->adicionar('head_script', isset($script) ? $script : NULL);

        //-----

		$view->adicionar('body_class', 'arquivos formulario');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
        $view->adicionar('lateral_menu', array('item' => 'area-cliente', 'link' => 'cadastrar arquivo'));

        $view->exibir();
	}

	
	/**
	 * Recebe os parâmetros do formulário e atualiza o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function atualizar($parametros)
	{
        if (isset($parametros->id) AND !empty($parametros->id))
        {
            if(! isset($parametros->habilitado)){
                $parametros->habilitado = '0';
            }

            $arquivo = new FatorCMS_Model_Arquivo();
            $arquivo->carregar($parametros);

            $arquivo_existe = new FatorCMS_Model_Arquivo($parametros->id);

            $colunas_null = array();

            if (!empty($arquivo->link) AND !is_null($arquivo_existe->arquivo))
            {
                @unlink('arquivos/arquivos/'.$arquivo_existe->arquivo);
                $arquivo->arquivo = NULL;
                $colunas_null[] = 'arquivo';
            }

            //-----

            $arquivo->colunas_mysqli_escape();

            if ($arquivo->verificar_null(array()))
            {
                // Validações ---

                // Fim das validações ---

                if ( ! isset($notificacao))
                {
                    if ($arquivo->update($colunas_null))
                    {
                        $this->relacionar_clientes($arquivo->id, $parametros->as_values_clientes);

                        new Notificacao('Arquivo <strong>'.$arquivo->titulo.'</strong> atualizado com sucesso.', 'success', TRUE);
                        echo json_encode(array('tipo'=>'success','id' => $arquivo->id));
                    }
                    else
                    {
                        echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao atualizar o arquivo.'));
                    }
                }
            }
            else
            {
                echo json_encode(array('tipo'=>'error','mensagem'=>'Todos campos marcados são obrigatórios.'));
            }
        }
        else
        {
            echo json_encode(array('tipo'=>'error','mensagem'=>'Arquivo não encontrado.'));
        }

	}


	/**
	 * Recebe o ID do registro e excluir ele do banco de dados
	 * @param  $parametros
	 * @return void
	 */
    public function excluir($parametros)
	{
        if (isset($parametros->_0) AND is_numeric($parametros->_0))
        {
            $arquivo = new FatorCMS_Model_Arquivo($parametros->_0);

            if ($arquivo AND ! is_null($arquivo->id))
            {
                // Apagamos a novidade do banco de dados
                if ($arquivo->delete())
                {
                    // Apaga as imagens da  novidade
                    @unlink('arquivos/arquivos/'.$arquivo->arquivo);

                    // Excluímos qualquer vínculo entre tags e a novidade
                    $arquivo_cliente = new FatorCMS_Model_ArquivoCliente();
                    $arquivo_cliente->arquivo_id = $arquivo->id;
                    $arquivo_cliente->delete();

                    new Notificacao('Arquivo <strong>'.$arquivo->titulo.'</strong> excluído com sucesso.', 'success', TRUE);
                }
                else
                {
                    new Notificacao('Ocorreu um erro ao excluir o arquivo <strong>'.$arquivo->titulo.'</strong>.', 'error', TRUE);
                }
            }
            else
            {
                new Notificacao('Arquivo não encontrado.', 'error', TRUE);
            }
        }
        else
        {
            new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention', 'error', TRUE);
        }

        header('Location: '.SITE_URL.'/fatorcms/arquivos');
        exit;
	}


    public function download($parametros)
    {
        if (!isset($_SESSION['cliente_id']))
        {
            header('HTTP/1.1 404 Not Found');
            $erro_404 = new Controller_Erro;
            $erro_404->index(404);
            exit;
        }
        if (!isset($parametros->_0) OR empty($parametros->_0))
        {
            new Notificacao('Arquivo não encontrado','error',TRUE);
            header('Location: '.SITE_URL.'/fatorcms/arquivos/listar'); exit;
        }
        else
        {
            $arquivo = new Model_Arquivo();
            $arquivo = $arquivo->select("
                SELECT {tabela_nome}.titulo, {tabela_nome}.id, {tabela_nome}.link, {tabela_nome}.arquivo
                FROM {tabela_nome}
                WHERE ({tabela_nome}.arquivo = '".Funcoes::mysqli_escape($parametros->_0)."' OR {tabela_nome}.link = '".Funcoes::mysqli_escape($parametros->_0)."')
                LIMIT 1
            ");


            if ($arquivo AND is_numeric($arquivo->id))
            {
                $nome_arquivo = (!is_null($arquivo->link) ? $arquivo->link : $arquivo->arquivo);

                $caminho = 'arquivos/arquivos/'.$nome_arquivo;

                header ("Content-Disposition: attachment; filename=".$nome_arquivo."");
                header ("Content-Type: application/octet-stream");
                header ("Content-Length: ".filesize($caminho));
                readfile($caminho);
                exit;
            }
            else
            {
                new Notificacao('Arquivo não encontrado','error',TRUE);
                header('Location: '.SITE_URL.'/fatorcms/arquivos/listar'); exit;
            }

        }
    }

	/****************************** MÉTODOS EXTRAS ******************************/


    /**
     * Cuida do relacionamento entre tags e produto. Realiza a exclusão dos relacionamentos prévios e cria e recria eles
     * @param $novidade_id
     * @param $tags
     * @return bool Retorna TRUE acontecendo pelo menos uma inserção
     */
    protected function relacionar_clientes($arquivo_id, $clientes)
    {
        $retorno = FALSE;

        // Apaga todas as tags relacionadas ao produto
        $arquivo_cliente = new FatorCMS_Model_ArquivoCliente();
        $arquivo_cliente->arquivo_id = $arquivo_id;

        if ($arquivo_cliente->delete())
        {
            $clientes = substr($clientes,1);
            $clientes = explode(',', $clientes);

            if (count($clientes) > 0)
            {
                // Percorre cada tag para fazer o relacionamento de novo, se for o caso
                foreach ($clientes as $item)
                {
                    if (mb_strlen(trim($item)) > 0)
                    {
                        $item = Funcoes::mysqli_escape(trim($item));

                        // Verifica se o ID da tag ou o nome ou o nome_seo existem
                        $cliente = new FatorCMS_Model_Cliente($item);


                        // Se não houve problemas, faz o relacionamento
                        if ( ! isset($log))
                        {
                            $arquivo_cliente = new FatorCMS_Model_ArquivoCliente();
                            $arquivo_cliente->arquivo_id = $arquivo_id;
                            $arquivo_cliente->cliente_id = $cliente->id;

                            if ( ! $arquivo_cliente->insert())
                            {
                                $log = new Log('Erro ao relacionar cliente com arquivo. $cliente_id='.$cliente->id.'|$arquivo_id'.$arquivo_id);
                            }
                        }
                    }
                }
            }
            else
            {
                // Se não veio nenhum, pode ser apenas exclusão de todos, uma limpeza
                $retorno = TRUE;
            }
        }
        else
        {
            new Log('Erro ao excluir os clientes de um arquivo. $arquivo_id:'.$arquivo_id);
        }

        return $retorno;
    }


    /**
     * Busca todos as tags que foram enviadas pelo formulário. Função utilizada quando a página do formulário tem que ser recarregada
     * @param $tags
     * @return string
     */
    protected function post_buscar_clientes($clientes)
    {
        // Monta as informações para o javascript
        $script = 'var clientes = {itens: [';
        if ($clientes AND strlen($clientes) > 1)
        {
            $clientes = substr($clientes,1);
            $clientes = explode(',', $clientes);

            foreach ($clientes as $cliente)
            {
                $script .= '{nome: "'.$cliente->nome.'("+"'.$cliente->email.')", id: "'.$cliente->id.'"},';
            }
            $script = substr($script, 0, -1); // Retira a última vírgula
        }
        $script .= ']};';
        return $script;
    }


    /**
     * Busca todas as tags relacionadas com a noi
     * @param $novidade_id
     * @return array|bool
     */
    protected function arquivo_buscar_clientes($arquivo_id = NULL)
    {
        if ( ! is_null($arquivo_id))
        {
            // Busca as tags do produto
            $cliente = new FatorCMS_Model_Cliente;
            $clientes = $cliente->select('
				SELECT cliente.nome, cliente.email, cliente.id
				FROM {tabela_prefixo}arquivos_clientes AS arquivo_cliente
					LEFT JOIN {tabela_nome} AS cliente ON arquivo_cliente.cliente_id = cliente.id
				WHERE arquivo_cliente.arquivo_id = ' . $arquivo_id . ' ORDER BY cliente.nome
			', TRUE);
        }
        else
        {
            $clientes = FALSE;
        }

        // E monta as informações para o javascript
        $script = 'var clientes = {itens: [';
        if ($clientes AND count($clientes) > 0)
        {
            foreach ($clientes as $cliente)
            {
                $script .= '{nome: "'.$cliente->nome.'("+"'.$cliente->email.')", id: "'.$cliente->id.'"},';
            }
            $script = substr($script, 0, -1); // Retira a última vírgula
        }
        $script .= ']};';

        return $script;
    }

    public function listar_clientes($parametros)
    {
        $texto = Funcoes::mysqli_escape($parametros->q);
        $data = array();

        $cliente = new FatorCMS_Model_Cliente;
        $clientes = $cliente->select('SELECT id, nome, email FROM {tabela_nome} WHERE LOWER(nome) LIKE "'.mb_strtolower($texto).'%" ORDER BY nome', TRUE);

        if ($clientes AND count($clientes) > 0)
        {
            foreach($clientes as $cliente)
            {
                $json = array();
                $json['id'] = $cliente->id;
                $json['nome'] = $cliente->nome .' ('. $cliente->email.')';
                $data[] = $json;
            }
        }

        header("Content-type: application/json");
        echo json_encode($data);
    }


} // end class
