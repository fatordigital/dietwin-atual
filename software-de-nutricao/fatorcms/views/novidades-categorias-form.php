<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php'; ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

        <!-- Page Head -->
		<?php if (is_null($novidade) OR is_null($novidade->id)) { ?>
			<h2>Cadastro de uma Categoria</h2>
			<p id="page-intro">Utilize o formulário abaixo para incluir uma categoria no site.</p>
		<?php } else { ?>
			<h2>Edição de uma Categoria</h2>
			<p id="page-intro">No formulário abaixo você pode alterar os dados de uma categoria.</p>
		<?php } ?>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<?php if (is_null($novidade) OR is_null($novidade->id)) { ?>
					<h3>Dados de uma categoria</h3>
				<?php } else { ?>
					<h3>Informações da categoria <?php echo $novidade ? $novidade->nome : '' ?></h3>
				<?php } ?>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<form action="<?php echo SITE_URL ?>/fatorcms/novidades/categorias-<?php echo (is_null($novidade) OR is_null($novidade->id)) ? 'cadastrar' : 'atualizar' ?>" method="post" id="novidade_form" enctype="multipart/form-data">

					<fieldset>

						<?php if ( ! is_null($novidade)) { ?>
							<input type="hidden" name="id" value="<?php echo $novidade->id ?>" />
						<?php } ?>

						<p>
							<label for="titulo">Nome</label>
							<input class="text-input large-input required" type="text" id="nome" name="nome" maxlength="200" value="<?php echo $novidade ? $novidade->nome : '' ?>" />
						</p>

						
						<p>
							<input class="button" type="submit" value="<?php echo (is_null($novidade) OR is_null($novidade->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
						</p>

					</fieldset>

					<div class="clear"></div><!-- End .clear -->

				</form>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>