<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property int galeria_id
 * @property string arquivo
 * @property string titulo

 */

class Model_GaleriaFoto extends Model_Padrao
{
	protected $tabela_nome = 'galerias_fotos';

} // end class