<?php include 'includes/cabecalho.php'; ?>

<body class="cadastro">

<?php include 'includes/topo.php'; ?>

<div class="titulo-principal">
    <p><span>Participe da dietWin</span><br />Cadastre-se</p>
</div>
<div class="titulo-resumo">
    <p>A equipe do dietwin está sempre a disposição de seus clientes. Preencha o formulário abaixo para efetuar seu cadastro e ter acesso a todos os softwares da dietwin!</p>
</div>
</div>
</div>

<div class="conteudo">
<div class="container">

<div class="formulario">
    <?php
    $total = $compra_produto->valor+($compra_produto->licenca_valor*$compra_produto->licenca_quantidade)+$compra->entrega_valor-((isset($compra_cupom) AND $compra_cupom)?$compra_cupom->valor:0);
    ?>
    <p>Você realizou uma compra na dietWin. Abaixo estão algumas informações sobre ela:</p>
    <p><strong>Código da compra:</strong> <?php echo $compra->codigo; ?></p>
    <p><strong>Forma de pagamento:</strong> <?php echo $compra->pagamento_metodo; ?></p>
    <?php if (isset($pagseguro))
    { ?>
    <p><strong>Código da transação do PagSeguro:</strong> <?php echo $pagseguro; ?></p>
    <?php }
    else
    { ?>
    <p><strong>Nº de parcelas:</strong> <?php echo $compra->boleto_parcelas; ?></p>
    <p><strong>Data de vencimento dos boletos:</strong> Dia <?php echo str_pad($compra->boleto_vencimento,2,'0',STR_PAD_LEFT); ?></p>
    <?php } ?>
    <p><strong>Forma de entrega:</strong> <?php echo $compra->entrega_forma; ?></p>
    <p><strong>Valor do frete:</strong> R$ <?php echo number_format($compra->entrega_valor, 2, ',', '.'); ?></p>
    <p><strong>Produto:</strong> <?php echo $produto->nome; ?></p>
    <p><strong>Valor do produto:</strong> R$ <?php echo number_format($produto->valor, 2, ',', '.'); ?></p>
    <p><strong>Nº de licenças adicionais:</strong> <?php echo $compra_produto->licenca_quantidade; ?></p>
    <?php echo (($compra_produto->licenca_quantidade>0)?'<p><strong>Valor da licença adicional:</strong> R$ '.number_format($produto->licenca_valor, 2, ',', '.').'</p>':''); ?>
    <p><strong>Cupom:</strong> <?php echo ((isset($compra_cupom) AND $compra_cupom AND $compra_cupom->id > 0)?'R$ '.number_format($compra_cupom->valor,2,',','.'):'Não'); ?></p>
    <p><strong>Endereço de entrega:</strong> <?php echo $compra_endereco->endereco.', '.$compra_endereco->numero.(isset($compra_endereco->complemento)?'/'.$compra_endereco->complemento:'').' - '.$compra_endereco->bairro.' - '.$compra_endereco->cep.' - '.$cidade->nome.' - '.$estado->sigla; ?></p>
    <p><strong>Total da Compra:</strong> R$ <?php echo number_format($total, 2, ',', '.'); ?></p>

	<!-- tracking code do adwords -->
	<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1059574202/?value=0&amp;label=AoglCP6irAIQuqOf-QM&amp;guid=ON&amp;script=0" />
</div>

<div class="clear"></div>
</div>
<div class="inferior"></div>
</div>

<?php include 'includes/rodape.php'; ?>