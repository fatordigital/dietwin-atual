<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_Compras extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai e testa se o usuário está logado
	 */
	public function __construct()
	{
		parent::__construct();
		$this->verificar_usuario_logado();
		$this->verificar_usuario_permissao('compras');
	}


	/**
	 * Método padrão que não deve ser chamado diretamente. Se for, vai para a listagem
	 * @return void
	 */
	public function index()
	{
        header('Location: '.SITE_URL.'/fatorcms/compras/listar');
		exit;
	}


	/**
	 * Método inicial que exibe a lista de registros do banco de dados
	 * @param $parametros object
	 * @return void
	 */
	public function listar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'compras-lista.php'));
		$view->adicionar('body_class', 'compras lista');
		$view->adicionar('lateral_menu', array('item'=>'compras','link'=>''));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$this->view_adicionar_obrigatorias($view);

		//-----

		// Lógica caso tenha sido feita uma busca na listagem
		$clausula_where = ' TRUE ';

		$buscar = NULL;
		$join_cliente = '';
		if (isset($parametros->buscar) AND strlen(trim($parametros->buscar)) > 0)
		{
			$buscar = trim($parametros->buscar);
			$buscar_aux = Funcoes::mysqli_escape(str_replace(' ', '%', $buscar));
			$clausula_where .= ' AND (pagseguro.identificador LIKE "%'.$buscar_aux.'%" OR pagseguro_transacao_status.status LIKE "%'.$buscar_aux.'%"
										OR compra.codigo LIKE "%'.$buscar_aux.'%"
										OR cliente.nome LIKE "%'.$buscar_aux.'%" OR cliente.email LIKE "%'.$buscar_aux.'%"
										OR cliente.cpf LIKE "%'.$buscar_aux.'%" OR cliente.cnpj LIKE "%'.$buscar_aux.'%"
										OR cliente.rg LIKE "%'.$buscar_aux.'%" OR cliente.responsavel_nome LIKE "%'.$buscar_aux.'%")';

			$join_cliente = 'LEFT JOIN {tabela_prefixo}clientes AS cliente ON compra.cliente_id = cliente.id
							 LEFT JOIN {tabela_prefixo}pagseguro AS pagseguro ON compra.id = pagseguro.compra_id
							 LEFT JOIN {tabela_prefixo}pagseguro_transacoes_status AS pagseguro_transacao_status ON pagseguro.transacao_status_id = pagseguro_transacao_status.id';
		}

		//-----

		// Ordenação dos resultados, podendo vir da página
		$ordenar_por = (isset($parametros->ordenar_por)) ? strtolower($parametros->ordenar_por) : 'data'; // Valor padrão
		$ordem = isset($parametros->ordem) ? strtolower($parametros->ordem) : 'desc'; // Valor padrão

		switch ($ordenar_por)
		{
			case 'data': $ordenar_por_sql = 'compra.data'; break;
			case 'codigo': $ordenar_por_sql = 'compra.codigo'; break;
			case 'cliente': $ordenar_por_sql = 'cliente_nome'; break;
			case 'produto': $ordenar_por_sql = 'produto_nome'; break;
			case 'valor-total': $ordenar_por_sql = 'valor_total'; break;
			case 'met-pagamento': $ordenar_por_sql = 'pagamento_metodo'; break;
			case 'situacao': $ordenar_por_sql = 'compra.situacao'; break;
            default: $ordenar_por_sql = $ordenar_por; break;
		}

		//-----

		//(compra_produto.valor + (compra_produto.licenca_valor * compra_produto.licenca_quantidade) + compra.entrega_valor - IFNULL(compra_cupom.valor, 0)) AS valor_total

		$compra = new FatorCMS_Model_Compra;
		// Define a cláusula do select
		$compra->clausula = '
			SELECT compra.id, compra.codigo, compra.data, compra.pagamento_metodo, compra.situacao,
                compra.cliente_id AS cliente_id, compra.cliente_nome AS cliente_nome, produto.id AS produto_id, produto.nome AS produto_nome,
                compra_produto.licenca_quantidade AS produto_licenca_quantidade, compra_produto.valor AS compra_produto_valor, compra_produto.licenca_valor AS compra_produto_licenca_valor, 
                compra.entrega_valor AS compra_entrega_valor, compra_cupom.valor AS compra_cupom_valor
			FROM {tabela_nome} AS compra
				LEFT JOIN {tabela_prefixo}compras_produtos AS compra_produto ON compra.id = compra_produto.compra_id
				LEFT JOIN {tabela_prefixo}produtos AS produto ON compra_produto.produto_id = produto.id
				LEFT JOIN {tabela_prefixo}compras_cupons AS compra_cupom ON compra.id = compra_cupom.compra_id
				'. $join_cliente .'
			WHERE '.$clausula_where.'
			GROUP BY compra.id
			ORDER BY '.$ordenar_por_sql.' '.$ordem
		;

		// Através da paginação vai verificar se existe a necessidade de dividir o resultado em várias páginas e se é necessário adicionar um LIMIT
		$paginacao = new Paginacao($compra, $parametros, 30, 10);
		// print('<pre>');
		// print_r($compra);
		// die;
		// var_dump();
		// Executa a cláusula lá de cima
		$compras = $compra->select(NULL, TRUE); // NULL para não alterar a clásula já definida e TRUE para forçar retorno em array

		//-----

		$view->adicionar('buscar', $buscar);
		$view->adicionar('ordenar_por', $ordenar_por);
		$view->adicionar('ordem', $ordem);
		$view->adicionar('compras', $compras);
		$view->adicionar('paginacao', $paginacao);

		$view->exibir();
	}


	/**
	 * Apresenta todas as informações relacionadas do registro
	 * @param $parametros object
	 * @return void
	 */
	public function visualizar($parametros)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'compras-form.php'));
        $view->adicionar('body_class', 'compras form');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
		$this->view_adicionar_obrigatorias($view);

        //-----

        // Se vier o ID, carregamos todas as informações
        if (isset($parametros->_0) AND is_numeric($parametros->_0))
        {
            $compra = new FatorCMS_Model_Compra($parametros->_0);
            $view->adicionar('compra', $compra);

            // Cliente
            $compra_cliente = new FatorCMS_Model_Cliente($compra->cliente_id);
            $view->adicionar('compra_cliente', $compra_cliente);

            // Produto
            $compra_produto = new FatorCMS_Model_CompraProduto;
            $compra_produto = $compra_produto->select('
                SELECT compra_produto.*, produto.nome AS produto_nome
                FROM {tabela_nome} AS compra_produto
                    LEFT JOIN {tabela_prefixo}produtos AS produto ON compra_produto.produto_id = produto.id
                WHERE compra_produto.compra_id = '.$compra->id);
            $view->adicionar('compra_produto', $compra_produto);

            // Cupom
            $compra_cupom = new FatorCMS_Model_CompraCupom;
            $compra_cupom = $compra_cupom->select('
                SELECT compra_cupom.*, cupom.codigo
                FROM {tabela_nome} AS compra_cupom
                    LEFT JOIN {tabela_prefixo}cupons AS cupom ON compra_cupom.cupom_id = cupom.id
                WHERE compra_cupom.compra_id = '.$compra->id
            );
            $view->adicionar('compra_cupom', $compra_cupom);

            // Endereco
            $compra_endereco = new FatorCMS_Model_CompraEndereco;
            $compra_endereco = $compra_endereco->select('
                SELECT compra_endereco.*, cidade.nome AS cidade_nome, estado.nome AS estado_nome
                FROM {tabela_nome} AS compra_endereco
                    LEFT JOIN {tabela_prefixo}cidades AS cidade ON compra_endereco.cidade_id = cidade.id
                    LEFT JOIN {tabela_prefixo}estados AS estado ON compra_endereco.estado_id = estado.id
                WHERE compra_endereco.compra_id = '.$compra->id
            );
            $view->adicionar('compra_endereco', $compra_endereco);

            // PagSeguro
            $compra_pagseguro = new FatorCMS_Model_PagSeguro;
            $compra_pagseguro = $compra_pagseguro->select('
                SELECT pagseguro.*, pagseguro_transacao_status.status AS transacao_status,
                    pagseguro_pagamento_tipo.tipo AS pagamento_tipo, pagseguro_pagamento_codigo.nome AS pagamento_meio
                FROM (SELECT * FROM
                            (SELECT * FROM {tabela_nome} ORDER BY data DESC) AS pagseguro_aux GROUP BY identificador
                        ) AS pagseguro
                    LEFT JOIN {tabela_prefixo}pagseguro_transacoes_status AS pagseguro_transacao_status ON pagseguro.transacao_status_id = pagseguro_transacao_status.id
                    LEFT JOIN {tabela_prefixo}pagseguro_pagamentos_tipos AS pagseguro_pagamento_tipo ON pagseguro.pagamento_tipo_id = pagseguro_pagamento_tipo.id
                    LEFT JOIN {tabela_prefixo}pagseguro_pagamentos_codigos AS pagseguro_pagamento_codigo ON pagseguro.pagamento_codigo_id = pagseguro_pagamento_codigo.id
                WHERE pagseguro.compra_id = '.$compra->id
            );
            $view->adicionar('compra_pagseguro', $compra_pagseguro);

            $view->exibir();
        }
        else
        {
            new Notificacao('Identificador de compra inválido.', 'error', TRUE);
            header('Location: '.SITE_URL.'/fatorcms/compras/listar');
            exit;
        }
    }


} // end class