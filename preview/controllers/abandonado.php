<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe

- Classe exclusiva para montagem e envio de emails, para reunir tudo em um só lugar e poupar os outros Controllers

*/
class Controller_Abandonado extends Controller_Padrao
{

	private $cliente;
	private $email;
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();

		$this->cliente = new Model_Cliente();
		$this->email = new Controller_Email();
	}

	public function verify()
	{
		#return $this->cliente->select("SELECT *, {tabela_nome}.id AS cliente_id, fd_compras.id as compra_id FROM {tabela_nome} LEFT JOIN fd_compras ON fd_compras.cliente_id = {tabela_nome}.id WHERE {tabela_nome}.cadastro_data >= (CURDATE() - INTERVAL 1 HOUR) GROUP BY {tabela_nome}.id");
		return $this->cliente->select("SELECT *, {tabela_nome}.id AS cliente_id, fd_compras.id AS compra_id FROM {tabela_nome} LEFT JOIN fd_compras ON fd_compras.cliente_id = {tabela_nome}.id WHERE {tabela_nome}.cadastro_data >= (CURDATE() - INTERVAL 1 HOUR) AND {tabela_nome}.senha IS NOT NULL AND {tabela_nome}.abandonado IS NULL GROUP BY {tabela_nome}.id");
	}

	public function cron()
	{
		/*die('ok');
		$hora = 3600; // 1 hora
		if(!isset($_SESSION['hora']))
			$_SESSION['hora'] = time() + $hora;

		$conta = time() - $_SESSION['hora'];

		if($conta >= $hora)
		{*/
			$verify = $this->verify();
			if($verify)
			{
				if(is_array($verify))
				{
					foreach($verify as $abandonado)
					{
						if(!isset($abandonado->compra_id) || $abandonado->compra_id == '')
						{
							$atualiza = new Model_Cliente();
							$atualiza->id = $abandonado->cliente_id;
							$atualiza->abandonado = true;
							$atualiza->update();
							$this->email->carrinho_abandonado($abandonado);
						}
					}
				} else {
					if(!isset($verify->compra_id) || $verify->compra_id == '')
					{
						$atualiza = new Model_Cliente();
						$atualiza->id = $verify->cliente_id;
						$atualiza->abandonado = true;
						$atualiza->update();
						$this->email->carrinho_abandonado($verify);
					}
				}
			}

			/*$_SESSION['hora'] = time();
		}*/
	}
}