<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe

- Classe exclusiva para montagem e envio de emails, para reunir tudo em um só lugar e poupar os outros Controllers

*/

class Controller_Frete extends Controller_Padrao
{

    /**
     * Chama o construtor da classe pai
     */
    public function __construct()
    {
        parent::__construct();
        require_once 'biblioteca/correios/com/imasters/php/ect/ECT.php';
        //require_once 'biblioteca/SwiftMailer/swift_required.php';
    }

    /**
     * Método inicial que faz a renderização básica da página
     */
    public function index()
    {
        header('HTTP/1.1 403 Forbidden');
        exit;
    }

    /**
     * Método que retorna o valor do frete
     * @param $parametros
     * @return float
     */
    public function calcular($parametros)
    {
        $ect = new ECT();
        $prdt = $ect->prdt();
        $prdt->setNVlAltura( 5.4 );
        $prdt->setNVlComprimento( 19.8 );
        $prdt->setNVlLargura( 11.3 );
        $prdt->setNCdFormato( ECTFormatos::FORMATO_CAIXA_PACOTE );
        $prdt->setNCdServico( implode( ',' , array( ECTServicos::PAC , ECTServicos::SEDEX ) ) );
        $prdt->setSCepOrigem( '90510002' );
        $prdt->setSCepDestino( $parametros );
        $prdt->setNVlPeso( 0.300 );

        $return = array();

        foreach ( $prdt->call() as $servico ) {
            if($servico->Erro != -3){                
                $nomeServico = '';
                if($servico->Codigo == 41106){
                    $nomeServico .= 'PAC';
                }elseif($servico->Codigo == 40010){
                    $nomeServico .= 'Sedex';
                }

                // Casos de entregas de difícil acesso
                if($servico->Erro == 10){
                    $PrazoEntrega = $servico->PrazoEntrega+7;
                    $return[] = array('servico' => $nomeServico, array('valor' => ($servico->Valor), 'prazo_entrega' => $PrazoEntrega), 'erro' => $servico->Erro);
                }else{
                    $return[] = array('servico' => $nomeServico, array('valor' => ($servico->Valor ), 'prazo_entrega' => $servico->PrazoEntrega), 'erro' => $servico->Erro);
                }
                
            }else{
                $return = null;
                //$return[] = $servico;
                //$return[] = $servico->MsgErro;
            }
        }
        return $return;
    }

    public function cepValido($cep){

        $return = false;

        $frete = $this->calcular($cep);

        if(isset($frete[0]) AND is_array($frete[0])){
            $return = true;
        }
        
        return $return;
    }

    public function teste(){
        echo '<hr />';
        var_dump($this->cepValido('02334130'));
        print('<pre>'); var_dump($this->calcular('02334130')); print('</pre>');

        echo '<hr />';
        var_dump($this->cepValido('94475280'));
        print('<pre>'); var_dump($this->calcular('94475280')); print('</pre>');
    }

}
