<?php include 'includes/cabecalho.php'; ?>

<body class="comparativo">

<?php include 'includes/topo.php'; ?>

        <div class="titulo-principal">
            <p><span>Qual a melhor</span><br />Solução?</p>
        </div>
        <div class="titulo-resumo">
            <p>Cada software da família dietWin é cuidadosamente construído tendo como foco um perfil de usuário. Descubra qual é a melhor solução para você!</p>
        </div>
</div>
</div>

<div class="conteudo">
    <div class="container">
        <div class="box">

            <?php include 'includes/toolbar.php' ?>
            <br/>
            <h3>COMPARATIVO DAS FUNÇÕES DE CADA VERSÃO</h3>

            <p>Ao compararmos softwares de nutrição uma atenção especial deve estar voltada para a funcionalidade de cada versão. O objetivo principal nem sempre é o mais relevante e sim o número de ferramentas e suas funções são os determinantes das diferenças entre eles. Analisar a integridade relacional das informações obtidas no banco de dados é de importância fundamental, pois um cadastro “morto” sem uma ação para análise das implicações e interações nutricionais de nada adianta. No dietWin, todas as informações tem uma razão de ser com o fim específico de geral resultados oara AUXILIAR O NUTRICIONISTA NA TOMADA DE DECISÃO frente ao diagnóstico nutricional, obviamente sem ser conclusivo a interpretação dos resultados sempre será do profissional que assina a avaliação.Ao compararmos softwares de nutrição uma atenção especial deve estar voltada para a funcionalidade de cada versão. O objetivo principal nem sempre é o mais relevante e sim o número de ferramentas e suas funções são os determinantes das diferenças entre eles. Analisar a integridade relacional das informações obtidas no banco de dados é de importância fundamental, pois um cadastro “morto” sem uma ação para análise das implicações e interações nutricionais de nada adianta. No dietWin, todas as informações tem uma razão de ser com o fim específico de geral resultados oara AUXILIAR O NUTRICIONISTA NA TOMADA DE DECISÃO frente ao diagnóstico nutricional, obviamente sem ser conclusivo a interpretação dos resultados sempre será do profissional que assina a avaliação.</p>

            <table cellpadding="0" cellspacing="0" border="0" class="qual-versao">
                <thead>
                    <th class="coluna_texto"></th>
                    <th class="coluna_profissional titulo-profissional" style="color:#060">Profissional Plus</th>
                    <th class="coluna_personal titulo-personal">Personal</th>
                    </thead>
                <tbody>
                 <th colspan="3" class="coluna-titulo">Prescrição da Dieta</th>
                    <tr>
                        <td>Dieta: permite a  prescrição de inúmeros cardápios por consulta</td>
                        <td class="sim"></td>
                        <td class="sim"></td>
                    </tr>
                        <tr>
                            <td>Recordatório: permite  o cálculo de inúmeros cardápios por consulta</td>
                            <td class="sim"></td>
                            <td class="sim"></td>
                        </tr>
                        <tr>
                            <td><strong>Ferramentas para prescrição de alimentos e  receitas</strong></td>
                            <td class="sim"></td>
                            <td class="sim"></td>
                        </tr>
                        <tr>
                            <td>Pesquisa de alimentos  e receitas </td>
                            <td class="sim"></td>
                            <td class="sim"></td>
                        </tr>
                        <tr>
                            <td>Pesquisa avançada com  filtros de nutrientes, grupos e categorias de alimentos</td>
                            <td class="sim"></td>
                            <td class="sim"></td>
                        </tr>
                        <tr>
                            <td>Modelos de cardápios</td>
                            <td class="sim"></td>
                            <td class="sim"></td>
                        </tr>
                        <tr>
                            <td>Alimentos favoritos</td>
                            <td class="sim"></td>
                            <td class="sim"></td>
                        </tr>
                        <tr>
                            <td>Lista de substituição  de alimentos (padrão)</td>
                            <td class="sim"></td>
                            <td class="sim"></td>
                        </tr>
                        <tr>
                            <td>Características da  dieta: modelos</td>
                            <td class="sim"></td>
                            <td class="sim"></td>
                        </tr>
                        <tr>
                            <td><strong>Estratégias nutricionais previstas </strong></td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>Nutrientes para  monitoramento</td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>QAVP (Quadro de  análise dos valores previstos)</td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>AMDR (Acceptable Macronutrient  Distribution Ranges)</td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Distribuição  energética entre as refeições</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Conflitos alimentares:  alergias, intolerâncias, aversões </td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Características  alimentares: dietéticas, corantes, conservantes, aditivos</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td><strong> Análise  dietética: </strong></td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Análise de 143  nutrientes</td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Alimentos que compõe  a quantidade do nutriente </td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Adequação das  recomendações diária das DRIS: EAR, RDA, Al, UL</td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Pirâmide Alimentar:  sintético e analítico detalhada de cada grupo da pirâmide</td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Contagem de  carboidratos</td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td><strong>Interações nutricionais</strong>: </td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Relação g/caloria</td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Relação kcal/N </td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Relação Peso g/dieta, </td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Relação PTN-AN/VEG</td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>NPU, NDpcal (Net Protein  Utilization)</td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Na, Nacl, Sal de  adição</td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Relação AGS/M/P (Ácido graxo  saturado/monoinsaturado/polinsaturado)*</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Relação AAR/AAA (Aminoácido ramificados/aromáticos)*</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Relação Ferro heme/Ferro não heme*</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Relação n-6/n-3*</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Relação  Ca/P*</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Relação  K/Na*</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>% Gordura Saturada*</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Índice glicêmico*</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Oxalatos*</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>K (mEq)*</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Análise de frações de nutrientes: å das frações: AAA, AAR, Fibra solúvel e  insolúvel, Açúcares disponíveis, Ômega 6, Ômega 3, AGS, AGM, AGP*</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td><strong>Estratégias nutricionais monitoradas</strong></td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>QAVE (Quadro de  análise dos valores encontrados)</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Avaliação clínica  (monitoramento dos nutrientes implicados e sua adequação)</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>AMDR (Acceptable Macronutrient  Distribution Ranges)</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Análise e  monitoramento do açúcar de adição</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Conflitos alimentares:  alergias, intolerâncias, aversões </td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Características  alimentares: dietéticas, corantes, conservantes, aditivos</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>g/kg/PA</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Distribuição  energética das refeições</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td><strong>Análise dietética avançada:</strong></td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Carga glicêmica por  refeição (IG) *</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Biodisponibilidade de  nutrientes: Relação molar dos nutrientes*</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>% Ferro absorvível*</td>
                          <td class="sim"></td>
                          <td class="nao"></td>
                        </tr>
                        <tr>
                          <td>Nutrientes parceiros  e antagônicos*</td>
                          <td class=""></td>
                          <td class=""></td>
                        </tr>
                        <tr>
                          <td><strong>Ferramentas de apoio a prescrição</strong></td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Lista de substituição  de alimentos (do paciente)</td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Recomendações  dietoterápica: modelos</td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Capa para relatórios</td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <tr>
                          <td>Relatórios: da  dieta/recordatório, análise dietética (todas as etapas), receitas da dieta, Lista  de substituição de alimentos, Recomendações dietoterápica, Lista de nutrientes  ajustadas ao paciente (mais de 500 derivações) ...</td>
                          <td class="sim"></td>
                          <td class="sim"></td>
                        </tr>
                        <th colspan="3" class="coluna-titulo">Avaliação Nutricional</th>
                     <tr>
                         <td>Avaliação de todos os ciclos da vida</td>
                         <td class="sim"></td>
                         <td class="sim"></td>
                     </tr>
                     <tr>
                         <td><strong>Antropometria: </strong>peso atual e altura. Outras medidas: peso estimado, peso desejável, peso  habitual, altura estimada, 3 medidas de diâmetros, 10 medidas para dobras  cutâneas, 12 perímetros. Cada campo da antropometria sinaliza a localização da  medida aferida em um boneco.</td>
                         <td class="sim"></td>
                         <td class="sim"></td>
                     </tr>
                     <tr>
                         <td><strong>Indicadores nutricionais</strong> são sinalizados de acordo com os dados do  paciente e as medidas informadas.</td>
                         <td class="sim"></td>
                         <td class="sim"></td>
                     </tr>
                 <tr>
                     <td><strong>ADULTO</strong>: <strong>Protocolo  da Avaliação</strong></td>
                     <td class="sim"></td>
                     <td class="sim"></td>
                 </tr>
                 <tr>
                     <td><strong>Peso teórico (PT)</strong></td>
                     <td class="sim"></td>
                     <td class="sim"></td>
                 </tr>
                 <tr>
                     <td>PT - Berardinelli</td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td>PT - West/1980* </td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td>PT - MET LIFE/1983*</td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td>PT - Lorenz* </td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td>PT - Cormillot* </td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td>Médias dos protocolos*</td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td>Classificação dos  indicadores*</td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td>Relação Blackburn (1977): % PA/PT, PA/PD, PA/PH </td>
                     <td class="sim"></td>
                     <td class="sim"></td>
                 </tr>

                 <tr>
                     <td>Classificação do  biótipo*</td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>
                 <tr>
                     <td>Indice de Wolf*</td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>
                 <tr>
                     <td>Gráfico da curva de  evolução de peso </td>
                     <td class="sim"></td>
                     <td class="sim"></td>
                 </tr>
                 <tr>
                     <td>Comparativo de  antropometria*</td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Gráfico de medidas  antropométricas*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Peso estimado (Chumlea  et al., 1988) *</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Altura  estimada (Chumlea,  Roche, Steinbaugh, 1985) *</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td><strong>Classificação do estado nutricional</strong>:</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>IMC: OMS/2004 </td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>IMC: OMS/Garrow/81* </td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>IMC: Keys et al./ * </td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>IMC: Krosla&amp;Lowe/67* </td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>IMC: Shills/94*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                     <td>IMC: Royal College of Physicians/1983* </td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>IMC: Waterlow/1999*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>CB, CMB, PCT, Jellife</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>AB, AMB, AGB (Heymsfield/1999)</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Classificação AB, AMB, AGB (Ann Arlor/1990)</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Circunferência da cintura, SBC</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td><strong>Composição corporal</strong>, <strong>protocolos de % gordura</strong></td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Pollock &amp;  Jackson, 1978</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Pollock &amp; Ward, 1980*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Faulkner/1968*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Guedes/1985*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Katch &amp; Mcardle/1973*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Petroski/1995*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Media dos % gordura*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Peso gordo/Siri*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Peso magro/Siri*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Peso  ósseo/Matiegka,1921 *</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Peso  residual/Matiegka,1921*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Peso  muscular/Matiegka,1921*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Densidade corporal,  Durnin &amp; Womersley/1984*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td><strong>Estimativas calóricas</strong>: </td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>TMB/FAO/OMS/85 </td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>GEB/Long/97 </td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                     <td>VET: FAO/OMS/85 </td>
                     <td class="sim"></td>
                     <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>VET: FAM/OMS/85*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>VET: HB/Waitzberg &amp; Rodrigues 95*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>VET: HB/queimados*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>VET: GEAF/Kacth*  </td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>EER (Estimated energy  requirement)*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>VET para redução de  peso, VEMTA*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>VET para redução de  peso, Escola Argentina* </td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>VET para redução de  peso, Reis,NT*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>VET para redução de  peso. % Redução*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>VET para redução de  peso, CFN*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>VET para redução de  peso, Nutroclínica*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td><strong>IDOSO: Protocolo da Avaliação </strong></td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>IMC  Lipschitz,1994</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Indicadores  nutricionais compatíveis com o do adulto</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td><strong>GESTANTE</strong>: <strong>Protocolo  da Avaliação </strong></td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Normas  do protocolo do Ministério da Saúde, 1986 e SISVAN 2005 </td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>DUM  (data da última menstruação), </td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Ganho  de peso recomendado do período, MS/IOM/1992*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Ganho  de peso médio semanal*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Relação  peso e altura segundo o percentil*</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>Curva  de ganho de peso gestacional classificação do IMC (Atalah et al.1997) </td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td>VET/FAO/OMS/85 </td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Adicional  energético gestacional FAO/OMS/85 e National Research Council</td>
                   <td class="sim"></td>
                   <td class="nao"></td>
                 </tr>
                 <tr>
                   <td><strong>CRIANÇA E ADOLESCENTE</strong>: <strong>Protocolo  da Avaliação </strong></td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Gráficos  das curvas de crescimento da OMS 2006 e 2007</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Percentil:  P/I, A/I, P/A, IMC/I </td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Escore-z </td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Diagnóstico  nutricional</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>TMB/FAO/OMS/85 </td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>VET  (kcal/kg/P, RDA 89)</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>GEB,  Schofield 85</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Recomendação  de PTN/g/kg / RDA 89</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 <tr>
                   <td>Relatórios:  de todos os níveis da avaliação</td>
                   <td class="sim"></td>
                   <td class="sim"></td>
                 </tr>
                 </tbody>
              <th colspan="3" class="coluna-titulo">Avaliação Clínica</th>
                <tr>
                    <td><strong>Ficha do paciente</strong></td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td><strong>Doenças  e Condutas dietoterápicas: </strong>protocolos de 140 doenças cadastradas, estratégias  nutricionais e cuidados nutricionais, patologias associadas, exames  monitorados, fármacos</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td><strong>Sintomas  e Sinais Clínicos</strong> – protocolos de 680 sinais e sintomas cadastrados,  estratégias nutricionais e cuidados nutricionais, patologias associadas, exames  monitorados, fármacos</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td><strong>Fármacos</strong> –  protocolos de 500 fármacos cadastrados, com nomes comerciais, grupo  farmacológico, interações nutricionais, cuidados nutricionais, exames  alterados.</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td><strong>Exames  Laboratoriais</strong> – protocolos de 100 exames, faixa etária. Evolução  dos exames dos exames do paciente.</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td><strong>Anamnese</strong>: permite cadastrar  inúmeros modelos de questionários para serem utilizados de acordo com cada tipo  de consulta</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td><strong>Prontuário</strong>: permite o registro  da evolução na data de cada consulta.</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td><strong>Relatórios</strong></td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <th colspan="3" class="coluna-titulo"><strong>Ferramentas complementares</strong></th>

                <tr>
                    <td>Cadastro da Ficha  técnica de alimentos: grupo, categoria, fabricante, valor de referência para  consumo, medida caseira de referência, coeficiente NPU, observações, medida  caseira usuais do alimento, nutrientes em 100g, calculadora da análise  dietética, replicar cadastro de alimentos </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Cadastro da Ficha  técnica de alimentos - Características  do alimento: características nutricionais (alimentos indicados para diferentes  patologias), corantes, conservantes, aditivos, principais fontes de: CHO, PTN,  GORD, nutrientes adicionados, nutrientes retirados</td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Cadastro da Ficha  técnica de receitas: grupo, categoria, fabricante, valor de referência para  consumo, medida caseira de referência, Modo de preparo, medida caseira usuais  do alimento, nutrientes pelo VRC, calculadora da análise dietética, replicar  cadastro de receitas</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Cadastro da Ficha  técnica de receitas - Características  do alimento: características nutricionais, corantes, conservantes, aditivos,  principais fontes de: CHO, PTN, GORD, nutrientes adicionados, nutrientes  retirados</td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Cadastro de protocolos:  Doenças, Sintomas e Sinais, Fármacos, Exames laboratoriais e Conduta  dietoterápica</td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Cadastro da modelos  de cardápios</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Tabelas: TACO,  DIETWIN e tabela do usuário</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Mais 1301 receitas  cadastradas</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Mais 3.849 alimentos cadastrados</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Cadastro de grupos de  alimentos favoritos</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Cadastro de tipo de  refeição, Horário das refeições</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Cadastro de medidas  caseiras</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Configurações: ficha  técnica alimento e receita, Pesquisa de alimentos e receitas, E-mail para  exportação de relatórios, cadastro do nutricionista</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Relatórios,  exportação Excel, PDF, BMP, Word</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
            </table>
            <br/>
            <br/>
            <br/>
            <h3>Comentários da autora sobre a versão Personal do dietWin:</h3>

            <table cellpadding="0" cellspacing="0" border="0" class="qual-versao-autora" width="100%">
                <thead>
                
                <th class="coluna_personal titulo-personal">Personal</th>
                </thead>
                <tbody>
                <th class="coluna-titulo">É um software para:</th>
                <tr>
                    <td>- atendimento clínico nutricional;<br/>
                        -aquele profissional que precisa de agilidade e não disponibiliza de muito tempo para alimentar o sistema;<br/>
                        - ser prático, mas que preencha as necessidades básicas do atendimento nutricional.<br/>
                        - quem está começando a utilizar um software pela primeira vez;<br/>
                        - o profissional que está começando atuar na área clínica;<br/>
                    </td>
                </tr>

            <th class="coluna-titulo">Ideal para:</th>
                <tr>
                  <td>Consultórios; personal diet; consultorias; hospitais; ensino acadêmico; UANs</td>
                </tr>
            </table>

            <br/>
          <h3>Comentários da autora sobre o dietWin Profissional Plus:</h3>
            <table cellpadding="0" cellspacing="0" border="0" class="qual-versao-autora" width="100%">
                <thead>
                
                <th class="coluna_profissional titulo-profissional">Profissional Plus</th>
                </thead>
                <tbody>
                <th class="coluna-titulo">Aviso Importante</th>
                <tr>
                    <td>Esta versão encontra-se em desenvolvimento. Atualizações frequentes serão disponibilizadas até a sua conclusão, por esta razão sua avaliação e sugestões são muito bem vindas para melhoria do sistema
                    </td>
                </tr>

            
            </table>
            
            
            
          <br/><br/>
        </div>
        <div class="clear"></div>

        <div class="clear"></div>
        <div class="inferior"></div>
    </div>
</div>

<?php include 'includes/rodape.php'; ?>