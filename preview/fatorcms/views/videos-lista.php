<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php' ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

		<!-- Page Head -->
		<h2>Lista de Vídeos cadastrados</h2>
		<p id="page-intro">Abaixo estão listados todos os vídeos que são exibidos na área de Multimídia do site.</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<form action="<?php echo SITE_URL ?>/fatorcms/videos/listar" method="get" id="videos_buscar_form">
			<fieldset>
				<p>
					<label for="buscar">Digite uma ou mais palavras-chave para buscar por um vídeo cadastrado:</label>
					<input class="text-input medium-input" type="text" id="buscar" name="buscar" maxlength="100" value="<?php echo isset($buscar) ? $buscar : '' ?>" />
					<input class="button" type="submit" value="Buscar" />
				</p>
			</fieldset>
			<div class="clear"></div><!-- End .clear -->
		</form>

		<?php if (isset($buscar) AND ! is_null($buscar)) { ?>
			<p class="buscar-resultado">
				Foram encontrados <strong><?php echo $paginacao->linhas_total ?></strong> resultado(s) para a busca por "<strong><?php echo $buscar ?></strong>".
				<a title="Limpar a busca" href="<?php echo SITE_URL?>/fatorcms/videos/listar"><img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier-cross.png" alt="Limpar busca" /> Clique aqui para limpar a busca</a>
			</p>
		<?php } ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Vídeos</h3>

				<input class="video button botao-cadastrar" type="button" value="Cadastrar um novo vídeo" />

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<table>

					<thead>
						<tr>
							<?php echo Funcoes::montar_th_ordenacao_listagem('videos', 'listar', $paginacao, $buscar, 'Data', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('videos', 'listar', $paginacao, $buscar, 'Título', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('videos', 'listar', $paginacao, $buscar, 'Categoria', $ordenar_por, $ordem) ?>
                            <th>Ações</th>
						</tr>
					</thead>

					<tfoot>
						<tr>
							<td colspan="4">

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_quantidades() ?>

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_links() ?>

								<div class="clear"></div>
							</td>
						</tr>
					</tfoot>

					<tbody>
						<?php
                        if($videos AND count($videos) > 0)
						{
							foreach ($videos as $video)
							{
							?>
								<tr>
									<td class="<?php echo $ordenar_por=='data' ? 'current' : '' ?>"><?php echo date('d/m/Y',strtotime($video->data)) ?></td>
									<td class="<?php echo $ordenar_por=='titulo' ? 'current' : '' ?>">
										<a href="<?php echo SITE_URL ?>/fatorcms/videos/editar/<?php echo $video->id ?>" title="Editar o vídeo &quot;<?php echo $video->titulo ?>&quot;">
											<?php echo $video->titulo ?>
										</a>
									</td>
									<td class="<?php echo $ordenar_por=='categoria' ? 'current' : '' ?>"><?php echo (is_null($video->produto_nome) ? '<em>Tópicos diversos</em>' : $video->produto_nome) ?></td>
									<td>
										<!-- Icons -->
										<a href="<?php echo SITE_URL ?>/fatorcms/videos/editar/<?php echo $video->id ?>" title="Editar o vídeo &quot;<?php echo $video->titulo ?>&quot;">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" />
										</a>
										<a href="<?php echo SITE_URL ?>/fatorcms/videos/excluir/<?php echo $video->id ?>" title="Excluir o vídeo &quot;<?php echo $video->titulo ?>&quot;" class="item-confirmar">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/cross.png" alt="Excluir" />
										</a>
									</td>
								</tr>
							<?php
							}
						}
						else
						{
							echo '<tr><td colspan="4">'.((isset($buscar) AND ! is_null($buscar)) ? 'Nenhum vídeo encontrado para esta busca.' : 'Nenhum vídeo cadastrado até o momento.' ).'</td></tr>';
						}
						?>
					</tbody>

				</table>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>