<?php include 'includes/cabecalho.php'; ?>

<body class="suporte">

<?php include 'includes/topo.php'; ?>

<div class="titulo-principal">
    <p><span>Tire suas</span><br />dúvidas</p>
</div>
<div class="titulo-resumo">
    <p>O FAQ (Frequently Asked Questions - Perguntas mais frequentes) é um local onde condensamos todas as principais dúvidas dos usuários do dietWin.</p>
</div>
</div>
</div>

<div class="conteudo">
    <div class="container">

        <?php include 'includes/toolbar.php' ?>

        <div id="tipo_suporte" class="<?php echo $classe ?>">
            <h1>Exibindo categoria: <span><?php echo $categoria ?></span></h1>

            <div class="perguntas" id="tamanho-fonte">
                <?php
                if ($faqs AND count($faqs) > 0)
                {
                    ?>
                    <ul>
                        <?php
                        $i = 1;
                        foreach ($faqs as $faq)
                        {
                            ?>
                            <li>
                                <h3><a href="#" id="info-<?php echo $i ?>" title="<?php echo $faq->pergunta; ?>"><?php echo $i ?>. <?php echo mb_strtolower($faq->pergunta); ?></a></h3>
                                <div class="info info-<?php echo $i; ?>">
                                    <?php echo $faq->resposta; ?>
                                    <a href="<?php echo SITE_URL.'/suporte/'.(isset($faq->nome_seo)?substr($faq->nome_seo,8):'topicos-diversos') ?>/faq/<?php echo $faq->pergunta_seo ?>" class="link-direto" title="Link direto para esse tópico">Link direto para esse tópico</a>
                                    <a href="#" class="link-info sem-borda" title="Solicitar mais informações sobre esse tópico">Solicitar mais informações sobre esse tópico</a>
                                </div>
                            </li>
                            <?php
                            $i++;
                        }
                        ?>
                    </ul>
                    <?php
                }
                else
                {
                    echo '<p>Não há perguntas cadastradas para essa categoria.</p>';
                }
                ?>

                <!-- Conteúdo do colorbox do botão "Mais Informações" -->
                <div style="display:none">
                    <div class="colorbox-mais-info">
                        <div class="topo"></div>
                        <div class="box">
                            <div class="titulo-colorbox">Mais Informações sobre esse Tópico</div>
                            <div class="conteudo-colorbox">

                                <p>Preencha o formulário abaixo para solicitar mais informações sobre o tópico <span><?php echo $faq->pergunta; ?></span>.</p>
                                <form method="post" action="<?php echo SITE_URL ?>/faq/duvida" id="form-mais-info" >

                                    <input type="hidden" name="categoria" value="<?php echo $categoria; ?>" />
                                    <input type="hidden" name="pergunta" value="<?php echo $faq->pergunta; ?>" />

                                    <label><b>Nome:</b></label>
                                    <input type="text" class="obrigatorio" name="info_nome" id="info_nome" maxlength="100" />

                                    <label><b>E-mail:</b></label>
                                    <input type="text" class="obrigatorio" name="info_email" id="info_email" maxlength="255" />

                                    <label><b>Telefone:</b></label>
                                    <input type="text" class="obrigatorio" name="info_telefone" id="info_telefone" maxlength="14" />

                                    <label><b>Cidade:</b></label>
                                    <input type="text" class="obrigatorio" name="info_cidade" id="info_cidade" maxlength="100" />

                                    <label><b>Mensagem:</b></label>
                                    <textarea type="text" class="obrigatorio" name="info_mensagem" id="info_mensagem" style="resize: vertical; overflow:auto"></textarea>
                                    
                                    <div style="">Digite o código no campo abaixo</div>
                                    <input type="text" id="txtCaptcha" class="captcha" style="width:60px; border:none; background-image:none; background-color:#F1EFED; text-align:center; color:red;" />
                                    <input type="hidden" id="dazuera">
                                    <input type="text" id="txtInput" style="width:72px; text-align: center" maxlength="8" />
                                    
                                    <button type="submit">Enviar</button>
                                    <div class="clear"></div>
                                </form>

                                <div id="form_notification"></div>

                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="inferior"></div>
                    </div>
                </div>

            </div>
        </div>

        <div class="sidebar">
            <div class="filtro">
                <h2>Filtre os tópicos</h2>
                <p>Clique para marcar apenas as categorias de tópicos que você deseja visualizar.</p>
                <a href="<?php echo SITE_URL.'/suporte/personal/faq'; ?>" class="filtro-personal<?php if ($categoria == 'Personal') { echo ' ativo'; } ?>" title="Filtrar por Personal "><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-personal<?php if ($categoria == 'Personal') { echo '-ativo'; } ?>.png" alt="Filtrar por Personal " /></a>
                <a href="<?php echo SITE_URL.'/suporte/profissional/faq'; ?>" class="filtro-profissional<?php if ($categoria == 'Profissional') { echo ' ativo'; } ?>" title="Filtrar por Profissional "><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-profissional<?php if ($categoria == 'Profissional') { echo '-ativo'; } ?>.png" alt="Filtrar por Profissional " /></a>
                <a href="<?php echo SITE_URL.'/suporte/rotulo-de-alimentos/faq'; ?>" class="filtro-rotulo-alimentos<?php if ($categoria == 'Rótulo de Alimentos') { echo ' ativo'; } ?>" title="Filtrar por Rótulo de Alimentos"><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-rotulo<?php if ($categoria == 'Rótulo de Alimentos') { echo '-ativo'; } ?>.png" alt="Filtrar por Rótulo de Alimentos" /></a>
                <a href="<?php echo SITE_URL.'/suporte/topicos-diversos/faq'; ?>" class="filtro-topicos-diversos<?php if ($categoria == 'Tópicos Diversos') { echo ' ativo'; } ?>" title="Filtrar por Tópicos diversos"><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-topicos<?php if ($categoria == 'Tópicos Diversos') { echo '-ativo'; } ?>.png" alt="Filtrar por Tópicos diversos" /></a>
            </div>
            <?php
            if ($faqs AND count($faqs) > 0)
            {
                ?>
                <div class="filtro">
                    <h2>Busque por palavra-chave</h2>
                    <form method="post" action="<?php echo SITE_URL ?>/faq" id="buscar" >
                        <input type="text" name="buscar" id="busca-campo" maxlength="255" value="" />
                        <button type="submit">Buscar</button>
                    </form>
                    <div class="clear"></div>
                </div>
                <?php
            }
            ?>
        </div>

        <div class="clear"></div>
    </div>
    <div class="inferior"></div>
</div>

<script type="text/javascript">

$(document).ready(function($) {
  DrawCaptcha();
});

</script>

<?php include 'includes/rodape.php'; ?>