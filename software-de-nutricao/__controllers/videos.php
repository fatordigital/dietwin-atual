<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe

*/

class Controller_Videos extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        $video = new Model_Video();

        $buscar = '';
        if (isset($parametros->buscar) AND strlen($parametros->buscar) > 0)
        {
            $buscar = " AND (titulo like '%".Funcoes::mysqli_escape($parametros->buscar)."%' OR  resumo like '%".Funcoes::mysqli_escape($parametros->buscar)."%' OR  descricao like '%".Funcoes::mysqli_escape($parametros->buscar)."%')";
        }

        if (isset($parametros->categoria) AND strlen($parametros->categoria) > 0)
        {
            if (Funcoes::mysqli_escape($parametros->categoria) == 'topicos-diversos')
            {
                $clausula = ' WHERE produto_id = 0';
                $classe = 'topicos-diversos';
                $categoria = 'Tópicos Diversos';
            }
            else
            {
                $produto = new Model_Produto();
                $produto->nome_seo = Funcoes::mysqli_escape($parametros->categoria);
                $produto = $produto->select("SELECT id,nome,nome_seo FROM {tabela_nome} WHERE nome_seo = 'dietwin-".$produto->nome_seo."'");
                if ($produto)
                {
                    $clausula = ' WHERE produto_id = '.$produto->id;
                    $categoria = substr($produto->nome,8);
                    $classe = Funcoes::mysqli_escape($parametros->categoria);
                }
                else
                {
                    $clausula = ' WHERE FALSE';
                    $classe = 'nao-encontrada';
                    $categoria = 'Não encontrada';
                }
            }
            $videos = $video->select('SELECT {tabela_nome}.*,fd_produtos.nome_seo FROM {tabela_nome} LEFT JOIN fd_produtos ON fd_produtos.id = fd_videos.produto_id '.$clausula.$buscar.' ORDER BY titulo ASC', TRUE);

        }
        else
        {
            $videos = $video->select('SELECT {tabela_nome}.*,fd_produtos.nome_seo FROM {tabela_nome} LEFT JOIN fd_produtos ON fd_produtos.id = fd_videos.produto_id WHERE TRUE'.$buscar.' ORDER BY data', TRUE);
            $categoria = 'Todas';
            $classe = 'topicos-diversos';
        }

        $view = new View('videos.php');
		$this->view_variaveis_obrigatorias($view);

		$view->adicionar('body_class', 'videos');
		$view->adicionar('notificacao', new Notificacao);

        $view->adicionar('videos', $videos);
        $view->adicionar('classe', $classe);
        $view->adicionar('categoria', $categoria);

        $view->adicionar('pagina_title', 'Vídeos - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */


} // end class