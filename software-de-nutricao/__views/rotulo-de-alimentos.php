<?php include 'includes/cabecalho.php'; ?>

<body class="softwares rotulo-de-alimentos">

<?php include 'includes/topo.php'; ?>
	
    <div class="titulo-principal">
        <p><span>dietWin</span><br />Rótulo de Alimentos</p>
    </div>
    <div class="titulo-resumo">
        <p>Possui um assistente que facilita a compreensão deste cálculo e tem função de conduzir o usuário até a conclusão da análise da fórmula do produto para a informação nutricional do rótulo.</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container">
    	
        <?php include 'includes/toolbar.php' ?>
        
        <h1>dietWin <span>Rótulo de Alimentos</span></h1>
        
        <div class="box" id="tamanho-fonte">
        	<h2>O primeiro software para cálculo da rotulagem do mercado.</h2>
        
            <p>Este programa tem a finalidade principal de calcular as Informações Nutricionais que devem estar presentes nos rótulos dos alimentos embalados de acordo as regras estabelecidas e as exigência Brasileiras determinadas pela ANVISA <a href="http://portal.anvisa.gov.br/wps/wcm/connect/1c2998004bc50d62a671ffbc0f9d5b29/RDC_N_360_DE_23_DE_DEZEMBRO_DE_2003.pdf?MOD=AJPERES" title="Resolução RDC"> Resolução - RDC nº 360, de 23 de dezembro de 2003</a> - Aprova Regulamento Técnico sobre Rotulagem Nutricional de Alimentos Embalados, tornando obrigatória a rotulagem nutricional e <a href="http://portal.anvisa.gov.br/wps/wcm/connect/d12c9e804745947f9bf0df3fbc4c6735/RDC_359.pdf?MOD=AJPERES" title="Resolução RDC nº359"> Resolução - RDC nº 359, de 23 de dezembro de 2003</a> - Tabela de Valores de Referência para Porções de Alimentos e Bebidas Embalados para Fins de Rotulagem Nutricional. Esta versão permite aos fabricantes de produtos calcular suas receitas e preparações  de acordo com seus os critérios permitindo o cálculos de uma análise dietética completa da formulação com vistas a obter a informação nutricional estabelecida em lei.</p>
            <ul class="detalhes">
                <li>Desde 2003, calculando a informação nutricional de alimentos e produtos.</li>
                <li>Calculo da INFORMAÇÃO NUTRICIONAL conforme as Resoluções 359 e 360 da ANVISA.</li>
                <li>É a ferramenta ideal para o profissional que faz consultoria nutricional.</li>
                <li>Integração entre do módulos do sistema, facilitando a rotina do usuário.</li>
                <li>Utilização dos protocolos padronizados de domínio público na nutrição.</li>
                <li>Desenvolvido por nutricionistas, para nutricionistas.</li>
                <li>Alia conhecimento e experiência no desenvolvimento das melhores ferramentas desde 1993.</li>
            </ul>


    <h3>Interface amigável e intuitiva</h3>
            <?php
            if ($fotos and count($fotos)>0)
            {
            ?>
                <ul class="thumbs">
                    <?php
                    foreach ($fotos as $foto)
                    {
                    ?>
                        <li><a href="<?php echo SITE_BASE ?>/arquivos/galerias/<?php echo $foto->galeria_id ?>/<?php echo $foto->arquivo ?>" rel="rotulo-de-alimentos" title="<?php echo isset($foto->titulo)?$foto->titulo:'dietWin Rótulo de Alimentos' ?>"><img src="<?php echo SITE_BASE ?>/arquivos/galerias/<?php echo $foto->galeria_id ?>/thumb1/<?php echo $foto->arquivo ?>" alt="<?php echo isset($foto->titulo)?$foto->titulo:'dietWin Rótulo de Alimentos' ?>" /></a></li>
                    <?php
                    }
                    ?>
                </ul>

                <div class="clear"></div>
            <a href="<?php echo SITE_URL ?>/software/rotulo-de-alimentos/galeria-foto" class="link-thumbs" title="Veja mais imagens do dietwin Rótulo de Alimentos">Veja mais imagens do dietwin Rótulo de Alimentos</a>
            <?php
            }
            ?>
           
            
            <h3>Recursos e soluções para quem é exigente.</h3>
            <h4>Por quê utilizar o dietWin Rótulo de Alimentos?</h4>
            <h5>ROTULAGEM DE ALIMENTOS E PRODUTOS</h5>

            <p>Para o cálculo da informação nutricional que devem estar presentes nos rótulos dos alimentos embalados segundo legislação vigente desde 2001, é necessário o conhecimento mínimo das RDC’s 359/360. </p>
            <p><b>Assistente para a rotulagem: </b>o dietWin auxilia a execução do cálculo através de um assistente que facilita essa compreensão. Tem função de conduzir o usuário até a conclusão da análise da fórmula do produto inserida para obter a informação nutricional do rótulo. Os nutrientes mínimos exigidos pela lei de rotulagem de alimentos já estão configurados como padrão no programa bem como os 3 modelos de rótulo para a emissão das etiquetas. Outros nutrientes podem ser selecionados através de uma configuração prévia. O sistema conduz um passo a passo sinalizando todos os procedimentos a serem feitos, o que não dispensa o conhecimento das RDC`s em vigor.</p>
            <p><b>Cadastro das fichas técnicas de alimentos: </b>o dietWin possui 3 tabelas de alimentos para selecionar que constitui o <b> banco de dados do dietWin</b> com mais de 2700 alimentos disponibilizado para compor o cadastro das preparações:</p>

            <ul class="detalhes">
           	    <li><b>Tabela Taco</b></li>
           	    <li><b>Tabela dietWin</b></li>
           	    <li><b>Tabela do usuário</b></li>
            </ul>

            <p>O cérebro de um software é o usuário tem o livre arbítrio para alterar, incluir novos cadastros em qualquer etapa do banco de dados (exceto inclusão de novas fórmulas e nutrientes). O dietWin é apenas uma ferramenta e como tal está preparada para executar os cálculos provenientes destes cadastros.</p>

            <p><b>Cadastro das fichas técnicas de receitas: </b>na medida em que as preparações vão sendo cadastradas elas vão sendo salvas no Banco de Dados para posterior acesso e emissão de relatórios. Como ficha técnica esta ferramenta permite a padronização de um receituário com vistas a sua padronização. Um relatório desta preparação permite emitir pelo número de porções desejadas, resultado em uma lista de compras.</p>

            <p><b>Análise dietética:</b></p>
            <ul class="detalhes">
                <li>Mais de 110 Nutrientes em análise</li>
                <li>% VALORES DIÁRIOS IDR baseado para uma dieta de 2000 calorias</li>
            </ul>

            <p><b>Outros:</b></p>
            <ul class="detalhes">
                <li>Relatório da Ficha Técnica do Rótulo; Etiquetas</li>
                <li>Listagens de alimentos e produtos</li>
                <li>Impressão das etiquetas para o rótulos nos modelos linear, simplificado, vertical e horizontal</li>
                <li>Cadastro de Formato de etiquetas</li>
                <li>Cadastro de Modelo de Rótulo</li>
                <li>Configuração: nutrientes; relatório; rótulo</li>
            </ul>

            <h3>Muitos recursos, pouco investimento</h3>
            <p>um investimento R$ 0,97 ao dia, este é o valor da sua amortização em um ano. </p>
            <p>A aquisição dá direito: 1 licença do direito de uso; Suporte técnico por 90 dias sem custo: via telefone, chat ou e-mail; Treinamento online; Atualização sem custo;</p>
            <p>Após este período você pode optar pelo: <b>PLANO DE ASSISTÊNCIA DIETWIN</b>, contempla uma série de vantagens por tempo ilimitado e não obrigatório. A adesão ao plano oferece: Suporte técnico via telefone, chat ou e-mail; Suporte online; Treinamento online; Atualização sem custo; Upgrade gratuito; Desconto de 50% na 2ª. Licença adquirida; Desconto de 60% na troca ou aquisição de outra versão dietWin.<b> Investimento: <s>De R$ 250,00</s> por R$ 98,00/ANUAL</b></p>


            <div class="box-valor">
            	<p class="valor"><span>R$</span>525</p>
                <p class="condicoes">Podendo ser pago em 1x no boleto ou cartão de crédito!</p>
                <a href="<?php echo SITE_URL ?>/software/rotulo-de-alimentos/comprar" title="Compre seu dietWin Rótulo de Alimentos na Loja Online"><img src="<?php echo SITE_BASE ?>/views/imagens/compre-loja-online-rotulo-de-alimentos.gif" alt="Compre seu dietWin Rótulo de Alimentos na Loja Online" /></a>
            </div>
            <img src="<?php echo SITE_BASE ?>/views/imagens/rotulo-de-alimentos-thumb.jpg" alt="dietWin Rótulo de Alimentos" />
            <div class="clear"></div>
            
            <h3>O que é preciso para rodar o Rótulo de Alimentos?</h3>


            <ul class="detalhes">
           		<li><b>Configuração Mínima </b>(Instalação: local): Processador com 750MHz; 256 Mb de RAM; 500 Mb de espaço livre em disco; Windows 2000 Pro ou superior; Monitor resolução 1024X768, Impressora. ou em rede</li>
                <li><b>Configuração Mínima </b>(Instalação em Servidor): Processador com 1GHz; 512 Mb de RAM; 500 Mb de espaço livre em disco; Windows 2000 Server ou superior; Windows XP Pro ou superior</li>
                <li><b style="color:#005B7F">Importante:</b> O dietWin Rótulo de Alimentos roda apenas nas versões 32bits do windows.</li>
			</ul>
            
            <h3>Um canal aberto com você</h3>
            <p>Disponibilizamos treinamentos online para apresentação do sistema. Um manual de ajuda em cada tela aberta do sistema bem como um botão com vídeo de operação ajudam no entendimento do sistema.
                O projeto dietWin avança sempre de forma intermitente, participe apresentando suas sugestões para novas implementações ou modificações dos protocolos.
            </p>
            
            <div class="box-treinamento">
            	<p>Confira os últimos vídeos disponíveis sobre o dietwin Rótulo de Alimentos</p>
                <?php
                if ($videos AND count($videos)>0)
                {
                    ?>
                    <ul class="galeria">
                        <?php
                        foreach ($videos as $video)
                        {
                            ?>
                            <?php
                            $regex = '/http\:\/\/www\.youtube\.com\/watch\?v=(\w{11})/';
                            preg_match($regex, $video->link,$matches);
                            ?>
                            <li>
                                <a href="<?php echo SITE_URL ?>/suporte/rotulo-de-alimentos/video/<?php echo $video->titulo_seo ?>" class="thumb" title="<?php echo $video->titulo ?>">
                                    <img src="<?php echo SITE_BASE ?>/biblioteca/timthumb.php?src=http://i3.ytimg.com/vi/<?php echo substr($video->link, strrpos($video->link,'v=')+2,11) ?>/0.jpg&w=48&h=48" alt="<?php echo $video->titulo ?>" />
                                    <div class="play"></div>
                                </a>
                            </li>
                            <?php
                        }
                        ?>

                    </ul>
                    <?php
                }
                else
                {
                    echo '<p>Não há vídeos cadastrados para esta categoria.</p>';
                }
                ?>
                <div class="clear"></div>
                <a href="<?php echo SITE_URL ?>/software/rotulo-de-alimentos/videos" title="Veja a galeria completa de vídeos do dietWin Rótulo de Alimentos">&#187; Veja a galeria completa</a>
            </div>

            <div class="box-treinamento">
                <p>Últimas dúvidas respondidas sobre a ferramenta:</p>
                <?php
                if ($faqs AND count($faqs)>0)
                {
                    ?>
                    <ul class="duvidas">
                        <?php
                        foreach ($faqs as $faq)
                        {
                            ?>
                            <li><a href="<?php echo SITE_URL.'/suporte/rotulo-de-alimentos/faq/'.$faq->pergunta_seo ?>" title="<?php echo $faq->pergunta ?>"><?php echo $faq->pergunta ?></a></li>
                            <?php
                        }
                        ?>
                    </ul>
                <?php
                }
                else
                {
                    echo '<p>Não há dúvidas cadastradas para esta categoria.</p>';
                }
                ?>
                <div class="clear"></div>
                <a href="<?php echo SITE_URL ?>/suporte/rotulo-de-alimentos/faq" title="Veja todas as dúvidas sobre o dietWin Rótulo de Alimentos">&#187; Veja todas as dúvidas</a>
            </div>
        </div>
        
        <div class="opcoes">
            <!-- inicio do widget do Webchat Locaweb -->
            <div id="webchat_widget"><!-- Coloque esse div onde você quer que o widget apareça. --></div>
            <script type="text/javascript">
                (function() {
                    var wc = document.createElement('script'); wc.type = "text/javascript";
                    wc.src = 'https://dietwin.webchatlw.com.br/widget.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(wc, s);
                })();
            </script>
            <!-- fim do widget do Webchat Locaweb -->
            <a href="#" class="link-mais-info" title="Solicite maiores informações sobre a ferramenta"><img src="<?php echo SITE_BASE ?>/views/imagens/botao-mais-info.png" alt="Solicite maiores informações sobre a ferramenta" /></a>

            <!-- Conteúdo do colorbox do botão "Mais Informações" -->
            <div style="display:none">
                <div class="colorbox-mais-info">
                    <div class="topo"></div>
                    <div class="box">
                        <div class="titulo-colorbox">Mais Informações</div>
                        <div class="conteudo-colorbox">

                            <p>Preencha o formulário abaixo para solicitar mais informações sobre este software.</p>
                            <form method="post" action="<?php echo SITE_URL ?>/rotulo-de-alimentos/mais_info" id="form-mais-info" >

                                <label><b>Nome:</b></label>
                                <input type="text" class="obrigatorio" name="info_nome" id="info_nome" maxlength="100" />

                                <label><b>E-mail:</b></label>
                                <input type="text" class="obrigatorio" name="info_email" id="info_email" maxlength="255" />

                                <label><b>Telefone:</b></label>
                                <input type="text" class="obrigatorio" name="info_telefone" id="info_telefone" maxlength="14" />

                                <label><b>Cidade:</b></label>
                                <input type="text" class="obrigatorio" name="info_cidade" id="info_cidade" maxlength="100" />

                                <label><b>Mensagem:</b></label>
                                <textarea type="text" class="obrigatorio" name="info_mensagem" id="info_mensagem" style="resize: vertical; overflow:auto"></textarea>

                                <button type="submit">Enviar</button>
                                <div class="clear"></div>
                            </form>

                            <div id="form_notification"></div>

                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="inferior"></div>
                </div>
            </div>
            <!-- Fim do colorbox do botão "Mais Informações" -->

            <a href="<?php echo SITE_URL ?>/software/rotulo-de-alimentos/videos" title="Acesse o centro de multimídia e treinamento dessa ferramenta"><img src="<?php echo SITE_BASE ?>/views/imagens/botao-centro-multimidia.png" alt="Acesse o centro de multimídia e treinamento dessa ferramenta" /></a>
            <div class="separador"></div>
            <a href="<?php echo SITE_URL ?>/software/rotulo-de-alimentos/comprar" class="comprar-rotulo-de-alimentos" title="Compre Online seu dietWin Rótulo de Alimentos"><img src="<?php echo SITE_BASE ?>/views/imagens/botao-compre-online-rotulo-de-alimentos.png" alt="Compre Online seu dietWin Rótulo de Alimentos" /></a>

            <div class="separador"></div>

            <h3>Conheça as outras ferramentas</h3>

            <ul>
                <li class="bubbleInfo" id="pop10">
                    <a href="<?php echo SITE_BASE ?>/profissional" class="trigger" title="Conheça o dietWin Professional">
                        <img src="<?php echo SITE_BASE ?>/views/imagens/opcao-profissional.jpg" alt="Conheça o dietWin Profissional" />
                        <table id="tpop10" class="popup">
                            <tbody>
                            <tr>
                                <td class="topleft corner"></td> <td class="top"></td>	<td class="topright corner"></td>
                            </tr>
                            <tr>
                                <td class="left"></td> <td><table class="popup-contents">
                                <tbody>
                                <tr>
                                    <td><b>Profissional Plus</b></td>
                                </tr>
                                </tbody></table>
                            </td>
                                <td class="right"></td>
                            </tr>
                            <tr>
                                <td class="bottomleft corner"></td> <td class="bottom"><img width="13" height="29" alt="popup tail" src="<?php echo SITE_BASE ?>/views/imagens/menu-bubble-tail2.gif"/></td> <td class="bottomright corner"></td>
                            </tr>
                            </tbody>
                        </table>
                    </a>
                </li>
                <li class="bubbleInfo" id="pop11">
                    <a href="<?php echo SITE_BASE ?>/personal" class="trigger" title="Conheça o dietWin Personal">
                        <img src="<?php echo SITE_BASE ?>/views/imagens/opcao-personal.jpg" alt="Conheça o dietWin Personal" />
                        <table id="tpop11" class="popup">
                            <tbody>
                            <tr>
                                <td class="topleft corner"></td> <td class="top"></td>	<td class="topright corner"></td>
                            </tr>
                            <tr>
                                <td class="left"></td> <td><table class="popup-contents">
                                <tbody>
                                <tr>
                                    <td><b>Personal </b></td>
                                </tr>
                                </tbody></table>
                            </td>
                                <td class="right"></td>
                            </tr>
                            <tr>
                                <td class="bottomleft corner"></td> <td class="bottom"><img width="13" height="29" alt="popup tail" src="<?php echo SITE_BASE ?>/views/imagens/menu-bubble-tail2.gif"/></td> <td class="bottomright corner"></td>
                            </tr>
                            </tbody>
                        </table>
                    </a>
                </li>
            </ul>
        </div>
        
        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>  
</div>

<div id="div-chat" >
    <a id="chat-abrir" href="#" onClick="wc_popup()"><img src="<?php echo SITE_BASE; ?>/views/imagens/chat-passivo.png" alt="chat" /></a>
    <a id="chat-fechar" href="#"><img src="<?php echo SITE_BASE; ?>/views/imagens/chat-fechar.png" alt="chat" /></a>
</div>

<?php include 'includes/rodape.php'; ?>