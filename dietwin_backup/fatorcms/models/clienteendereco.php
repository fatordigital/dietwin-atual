<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property int cliente_id
 * @property string tipo
 * @property string endereco
 * @property string numero
 * @property string complemento
 * @property string bairro
 * @property int cidade_id
 * @property int estado_id
 * @property int cep
 * @property string observacao
 * @property int entrega
 */

class FatorCMS_Model_ClienteEndereco extends FatorCMS_Model_Padrao
{
	protected $tabela_nome = 'clientes_enderecos';

} // end class