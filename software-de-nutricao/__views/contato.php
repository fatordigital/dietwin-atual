<?php include 'includes/cabecalho.php'; ?>

<body class="contato">

<?php include 'includes/topo.php'; ?>
	
    <div class="titulo-principal">
        <p><span>Entre em</span><br />Contato</p>
    </div>
    <div class="titulo-resumo">
        <p>A equipe do dietwin está sempre a disposição de seus clientes. Utilize a forma de contato abaixo que melhor se encaixar para sua necessidade e aguarde nosso retorno!</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container">
    	
        <div class="formulario">
            <p>Utilize o formulário abaixo para entrar em contato com a equipe do dietwin. Se preferir , mande um e-mail para um dos endereços eletrônicos listados ao lado.</p>
            <form method="post" action="<?php echo SITE_URL ?>/contato/enviar" id="form-contato" >
    
                <label><b>Nome:</b></label>
                <input type="text" class="obrigatorio" name="nome" id="nome" maxlength="100" value="" />
                
                <label><b>E-mail:</b></label>
                <input type="text" class="obrigatorio" name="email" id="email" maxlength="255" value="" />
                
                <label><b>Cidade:</b></label>
                <input type="text" class="obrigatorio cidade" name="cidade" id="cidade" maxlength="100" value="" />
                
                <label><b>Telefone:</b></label>
                <input type="text" class="obrigatorio fone" name="telefone" id="telefone" maxlength="14" value="" />

                <div class="clear"></div>

                <p><b>Já é usuário do dietwin?</b></p>
                <div class="usuario">
                    <input type="radio" id="sim" name="usuario" value="Sim" /><span>Sim</span>
                    <input type="radio" id="nao" name="usuario" value="Não" /><span>Não</span>
                    <div class="clear"></div>
                </div>
                
                <label><b>Assunto:</b></label>
                <select class="obrigatorio" name="assunto" id="assunto">
                  <option value="Entrar em contato com o suporte">Entrar em contato com o suporte</option>
                  <option value="Mais informações sobre os softwares">Mais informações sobre os softwares</option>
                  <option value="Outro">Outro</option>
                </select>
                
                <label><b>Mensagem:</b></label>
                <textarea type="text" class="obrigatorio" name="mensagem" id="mensagem" style="overflow:auto;"></textarea>
    
                <button type="submit">Enviar</button>

                <div class="clear"></div>

            </form>

            <div id="form_notification"></div>

        </div>
        
        <div class="formas">
            <p class="subtitulo">Contato por telefone</p>
            <p class="telefone">(51) 3337.7908</p>
            <div class="clear"></div>
            <div class="separador"></div>
            <p class="subtitulo">Suporte</p>
            <p class="email">suporte@dietwin.com.br</p>
            <div class="clear"></div>
            <p class="info">Se você encontrar alguma dificuldade com algum produto da linha dietWin, utilize nosso suporte.</p>
            <div class="clear"></div>
        </div>
        
        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>  
</div>

<?php include 'includes/rodape.php'; ?>