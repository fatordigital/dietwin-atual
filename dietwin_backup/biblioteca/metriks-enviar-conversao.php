<?php
/**
 * Metriks - Envio de Conversões
 * Criado por FatorDigital - www.fatordigital.com.br
 *
 * versão: 0.1
 */


// -----------------------------------------------------------------------------
//
//      NÃO ALTERE O CÓDIGO A SEGUIR
//
// -----------------------------------------------------------------------------

// ##### ESTE CÓDIGO DEVE SER APAGADO QUANDO COLOCADO NA HOSPEDAGEM DO CLIENTE
define('METRIKS_URL', 'http://metriks.fdserver/conversoes-recebimento/entrada');
// ##### E A LINHA ABAIXO DEVE SER DESCOMENTADA

//define('METRIKS_URL', 'http://www.metriks.net/conversoes-recebimento/entrada');
define('METRIKS_CID', ( ! isset($Metriks_CID) ? 0 : $Metriks_CID));
define('METRIKS_FID', ( ! isset($Metriks_FID) ? 0 : $Metriks_FID));


class Metriks_Enviar_Conversao
{
	function Metriks_Enviar_Conversao()
	{
		try {
			if (function_exists('fsockopen')) {
				// Parametros a serem passados
				$parametros = 'metriks_cid='.METRIKS_CID.'&';
				$parametros .= 'metriks_fid='.METRIKS_FID.'&';
				if (function_exists('http_build_query'))
					$parametros .= http_build_query(array_merge($_GET, $_POST));
				else {
					foreach (array_merge($_GET, $_POST) as $chave=>$valor)
						$parametros .= $chave.'='.$valor.'&';
					$parametros = urlencode($parametros);
				}

				// Quebramos a URL
				$metriks_url_partes = parse_url(METRIKS_URL);

				// Abre o socket
				$socket = fsockopen(
					$metriks_url_partes['host'],
					isset($metriks_url_partes['port']) ? $metriks_url_partes['port'] : 80,
					$errno,
					$errstr,
					5
				);

				// Cabeçalho http
				$informacoes = 'POST '.$metriks_url_partes['path'].' HTTP/1.1'.PHP_EOL;
				$informacoes .= 'Host: '.$metriks_url_partes['host'].PHP_EOL;
				$informacoes .= 'Content-Type: application/x-www-form-urlencoded'.PHP_EOL;
				$informacoes .= 'Content-Length: '.mb_strlen($parametros).PHP_EOL;
				$informacoes .= 'Connection: Close'.PHP_EOL.PHP_EOL;
				$informacoes .= $parametros.PHP_EOL.PHP_EOL;

				// Envio
				fwrite($socket, $informacoes);
				fclose($socket);
			}
		} catch (Exception $e) {
			// Nada é feito
		}
	}
}

$metriks_enviar_conversao = new Metriks_Enviar_Conversao();