<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php'; ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

        <!-- Page Head -->
		<?php if (is_null($produto) OR is_null($produto->id)) { ?>
			<h2>Cadastro de um Produto</h2>
			<p id="page-intro">Utilize o formulário abaixo para cadastrar um produto no site.</p>
		<?php } else { ?>
			<h2>Edição de um Produto</h2>
			<p id="page-intro">No formulário abaixo você pode alterar os dados de um produto.</p>
		<?php } ?>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<?php if (is_null($produto) OR is_null($produto->id)) { ?>
					<h3>Dados de um produto</h3>
				<?php } else { ?>
					<h3>Informações do produto <?php echo $produto ? $produto->nome : '' ?></h3>
				<?php } ?>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<form action="<?php echo SITE_URL ?>/fatorcms/produtos/<?php echo (is_null($produto) OR is_null($produto->id)) ? 'cadastrar' : 'atualizar' ?>" method="post" id="produto_form">

					<fieldset>

						<?php if ( ! is_null($produto)) { ?>
							<input type="hidden" name="id" value="<?php echo $produto->id ?>" />
						<?php } ?>

                        <p>
                            <label for="nome">Nome</label>
                            <input class="text-input large-input required" type="text" id="nome" name="nome" maxlength="100" value="<?php echo $produto ? $produto->nome : '' ?>" />
                        </p>

                        <p>
							<label for="valor">Valor</label>
							<input class="text-input medium-input required" type="text" id="valor" name="valor" maxlength="11" value="<?php echo $produto ? $produto->valor : '' ?>" />
						</p>

                        <p>
                            <label for="versao">Versão</label>
                            <input class="text-input medium-input required" type="text" id="versao" name="versao" maxlength="20" value="<?php echo $produto ? $produto->versao : '' ?>" />
                        </p>

                        <p>
                            <label for="licenca_valor">Valor da licença</label>
                            <input class="text-input medium-input required" type="text" id="licenca_valor" name="licenca_valor" maxlength="11" value="<?php echo $produto ? $produto->licenca_valor : '' ?>" />
                        </p>

						<p>
							<input class="button" type="submit" value="<?php echo (is_null($produto) OR is_null($produto->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
						</p>

					</fieldset>

					<div class="clear"></div><!-- End .clear -->

				</form>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>