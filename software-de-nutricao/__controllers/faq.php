<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe

*/

class Controller_FAQ extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        $faq = new Model_FAQ();

        //Se tiver busca
        $buscar = '';
        if (isset($parametros->buscar) AND strlen($parametros->buscar) > 0)
        {
            $buscar = " AND (pergunta like '%".Funcoes::mysqli_escape($parametros->buscar)."%' OR  resposta like '%".Funcoes::mysqli_escape($parametros->buscar)."%')";
        }

        //Caso seja de uma categoria especifica
        if (isset($parametros->categoria) AND strlen($parametros->categoria) > 0)
        {
            $pergunta = '';
            if (isset($parametros->titulo_seo) AND strlen($parametros->titulo_seo) > 0)
            {
                $pergunta = " AND {tabela_nome}.pergunta_seo = '".Funcoes::mysqli_escape($parametros->titulo_seo)."'";
            }

            if (Funcoes::mysqli_escape($parametros->categoria) == 'topicos-diversos')
            {
                $clausula = ' WHERE fd_produtos.nome is NULL';
                $classe = 'topicos-diversos';
                $categoria = 'Tópicos Diversos';
            }
            else
            {
                $produto = new Model_Produto();
                $produto->nome_seo = Funcoes::mysqli_escape($parametros->categoria);
                $produto = $produto->select("SELECT id,nome,nome_seo FROM {tabela_nome} WHERE nome_seo = 'dietwin-".$produto->nome_seo."'");
                if ($produto)
                {
                    $clausula = ' WHERE fd_faqs_produtos.produto_id = '.$produto->id;
                    $categoria = substr($produto->nome,8);
                    $classe = Funcoes::mysqli_escape($parametros->categoria);
                }
                else
                {
                    header('HTTP/1.1 404 Not Found');
                    $erro_404 = new Controller_Erro;
                    $erro_404->index(404);
                    exit;
                }
            }
            $faqs = $faq->select("
                SELECT {tabela_nome}.id, {tabela_nome}.pergunta, {tabela_nome}.resposta, {tabela_nome}.pergunta_seo, fd_produtos.nome, fd_produtos.nome_seo
                FROM {tabela_nome}
                JOIN fd_faqs_produtos ON {tabela_nome}.id = fd_faqs_produtos.faq_id
                LEFT JOIN fd_produtos ON fd_faqs_produtos.produto_id = fd_produtos.id
                ".$clausula.$buscar.$pergunta."
                GROUP BY {tabela_nome}.id
                ORDER BY {tabela_nome}.pergunta ASC
            ",TRUE);
        }
        else
        {
            $classe = 'topicos-diversos';
            $categoria = 'Todas';
            $faqs = $faq->select("
            SELECT {tabela_nome}.id, {tabela_nome}.pergunta, {tabela_nome}.resposta, {tabela_nome}.pergunta_seo, fd_produtos.nome, fd_produtos.nome_seo
            FROM {tabela_nome}
            JOIN fd_faqs_produtos ON {tabela_nome}.id = fd_faqs_produtos.faq_id
            LEFT JOIN fd_produtos ON fd_faqs_produtos.produto_id = fd_produtos.id
            WHERE TRUE".$buscar."
            GROUP BY {tabela_nome}.id
            ORDER BY {tabela_nome}.pergunta ASC
        ",TRUE);
        }
        if (isset($parametros->titulo_seo) AND strlen($parametros->titulo_seo))
        {
            if (!$faqs AND count($faqs) <= 0)
            {
                header('HTTP/1.1 404 Not Found');
                $erro_404 = new Controller_Erro;
                $erro_404->index(404);
                exit;
            }
            $view = new View('topico.php');
            $this->view_variaveis_obrigatorias($view);
            $view->adicionar('pagina_title', $faqs[0]->pergunta.' - dietWin - Softwares de Nutrição');

        }
        else
        {
            $view = new View('faq.php');
            $this->view_variaveis_obrigatorias($view);
            $view->adicionar('pagina_title', 'Suporte - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        }



        $view->adicionar('faqs',$faqs);
        $view->adicionar('classe', $classe);
        $view->adicionar('categoria', $categoria);

		$view->adicionar('body_class', 'suporte');
		$view->adicionar('notificacao', new Notificacao);

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */

    /**
     * Passa as informações para a montagem e envio do email a partir do formulário "Mais Informações sobre tópico"
     * @param $parametros
     * @return void
     */
    public function duvida($parametros)
    {
        $info_nome = isset($parametros->info_nome) ? trim($parametros->info_nome) : NULL;
        $info_telefone = isset($parametros->info_telefone) ? trim($parametros->info_telefone) : NULL;
        $info_email = isset($parametros->info_email) ? trim($parametros->info_email) : NULL;
        $info_cidade = isset($parametros->info_cidade) ? trim($parametros->info_cidade) : NULL;
        $info_mensagem = isset($parametros->info_mensagem) ? trim($parametros->info_mensagem) : NULL;

        $categoria = isset($parametros->categoria) ? trim($parametros->categoria) : NULL;
        $pergunta = isset($parametros->pergunta) ? trim($parametros->pergunta) : NULL;

        if ( ! empty($info_nome) AND ! empty($info_email) AND ! empty($info_mensagem) AND ! empty($info_telefone) AND ! empty($info_cidade))
        {
            $mail = new Controller_Email;

            if ($mail->faq($parametros))
            {
	            if (SITE_LOCAL == 'fdserver') {
		            //$Metriks_CID = 0; $Metriks_FID = 0;
	            } else {
		            $Metriks_CID = 2; $Metriks_FID = 13;
	            }
	            @include 'biblioteca/metriks-enviar-conversao.php';

                echo json_encode(array('tipo'=>'sucesso', 'mensagem'=>'Sua mensagem foi enviada com sucesso!'));
            }
            else
            {
                echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Ocorreu um erro ao enviar sua mensagem.'));
            }
        }
        else
        {
            echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Ocorreu um erro ao receber suas informações.'));
        }
    }

} // end class