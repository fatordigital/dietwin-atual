<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jonathan.alba
 * Date: 16/05/12
 * Time: 18:35
 * To change this template use File | Settings | File Templates.
 */
class Controller_BoletoCompra extends Controller_Padrao
{
    /**
     * Chama o construtor da classe pai
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método inicial que faz a renderização básica da página
     * @param $parametros
     * @return void
     */
    public function index($parametros)
    {

        //print_r($_SESSION['produto']); exit;

        if (!isset($_SESSION['processo_compra']))
        {
            header('Location: '.SITE_URL.'/index.php'); exit;
        }

        if (!isset($_SESSION['cliente_id']))
        {
            header('Location: '.SITE_URL.'/index.php'); exit;
        }

        $parcelas = ((isset($parametros->parcelas) AND strlen($parametros->parcelas)>0)?(int)Funcoes::mysqli_escape($parametros->parcelas):NULL);
        $data = ((isset($parametros->data) AND strlen($parametros->data)>0)?(int)Funcoes::mysqli_escape($parametros->data):'05');
        $entrega_forma = ((isset($parametros->entrega_forma) AND strlen($parametros->entrega_forma)>0)?Funcoes::mysqli_escape($parametros->entrega_forma):'pac');

        if (isset($parcelas) AND isset($data) AND isset($entrega_forma) AND $parcelas AND ($entrega_forma == 'pac' OR $entrega_forma == 'sedex' OR $entrega_forma == 'download'))
        {
            //Validacoes

            if ($parcelas <= 0 OR $parcelas > 3)
            {
                $notificacao = new Notificacao('Número de parcelas inválido.');
            }

            //Recuperamos o numero de licencas da sessão
            $licencas = $_SESSION['licencas'];

            //Recuperamos o produto da sessão
            $produto = $_SESSION['produto'];

            //Recuperamos o produto caso haja
            $cupom = (isset($_SESSION['cupom'])?$_SESSION['cupom']:NULL);

            //Recuperamos o cliente da sessão
            $cliente = new Model_Cliente($_SESSION['cliente_id']);

            //Recuperamos o endereço do cliente
            $endereco = new Model_ClienteEndereco();
            $endereco = $endereco->select('SELECT * FROM {tabela_nome} WHERE entrega=1 AND cliente_id='.$cliente->id);
            if (!isset($endereco) OR !$endereco)
            {
                //Caso não possua o endereço de entrega, usa o endereço principal
                $endereco = new Model_ClienteEndereco();
                $endereco = $endereco->select('SELECT * FROM {tabela_nome} WHERE entrega=0 AND cliente_id='.$cliente->id);
            }

  /*          if ($entrega_forma == 'pac')
            {
                $entrega_forma = 'PAC';
                $valor_frete = 12;
            }
            else
            {
                $entrega_forma = 'Sedex';
                $frete = new Controller_Frete();
                $valor_frete = $frete->calcular($endereco->cep);
            }
*/

            if ($entrega_forma == 'pac')
            {
                $entrega_forma = 'PAC';
                $valor_frete = 12.00;
            }
            else if($entrega_forma == 'sedex')
            {
                $entrega_forma = 'Sedex';
                $frete = new Controller_Frete();
                $valor_frete = $frete->calcular($endereco->cep);
            }
            else
            {
                $entrega_forma = 'Download';
                $valor_frete = 0;
            }


            $compra = new Model_Compra();
            $compra->cliente_id = $cliente->id;
            $compra->data = date('d/m/Y H:i:s');
            $compra->entrega_forma = $entrega_forma;
            $compra->entrega_valor = $valor_frete;
            $compra->pagamento_metodo = 'Boleto';
            $compra->boleto_parcelas = $parcelas;
            $compra->boleto_vencimento = $data;
            $compra->situacao = 'Aguardando pagamento';
            $compra->inicio = date('Y-m-d');
            $compra->fim = date('Y-m-d', mktime(0,0,0,date('m'),date('d'),date('Y')+1));

            if ($compra->insert())
            {
                // Criamos o código para a compra e atualizamos ela
                $compra_atualizar = new Model_Compra;
                $compra_atualizar->id = $compra->id;
                $compra_atualizar->codigo = $compra->id + 1234; // Não mexer nesta soma, ela deve ser IGUAL à do site
                $compra_atualizar->update();
                $compra->codigo = $compra_atualizar->codigo;

                // Cadastramos o endereço da compra

                $compra_endereco = new Model_CompraEndereco();
                $compra_endereco->carregar(new Model_ClienteEndereco($endereco->id));

                // Campos especiais
                $compra_endereco->id = NULL;
                $compra_endereco->compra_id = $compra->id;

                $compra_endereco->colunas_mysqli_escape();
                if ($compra_endereco->verificar_null(array('id')))
                {
                    if ($compra_endereco->insert())
                    {
                        // Cadastramos o produto da compra
                        $compra_produto = new Model_CompraProduto;
                        $compra_produto->carregar($produto);

                        // Campos especiais
                        $compra_produto->compra_id = $compra->id;

                        // Colocamos os valores nas colunas corretas após o carregamento
                        $compra_produto->produto_id = $compra_produto->id;
                        $compra_produto->id = NULL;

                        // Definimos a versão do produto
                        $compra_produto->versao = $produto->versao;

                        // Definimos a quantidade de licenças com a compra do produto
                        $compra_produto->licenca_quantidade = $licencas;

                        $compra_produto->colunas_mysqli_escape();

                        if ($compra_produto->verificar_null(array('id')))
                        {
                            if ($compra_produto->insert())
                            {
                                // Cadastramos o cupom da compra, se houver um
                                if (isset($cupom))
                                {
                                    $compra_cupom = new Model_CompraCupom;
                                    $compra_cupom->carregar($cupom);

                                    // Campos especiais
                                    $compra_cupom->compra_id = $compra->id;

                                    // Colocamos os valores nas colunas corretas após o carregamento
                                    $compra_cupom->cupom_id = $compra_cupom->id;
                                    $compra_cupom->id = NULL;

                                    $compra_cupom->colunas_mysqli_escape();

                                    if ($compra_cupom->verificar_null(array('id')))
                                    {
                                        if ( ! $compra_cupom->insert())
                                        {
                                            $notificacao = new Notificacao('Ocorreu um erro ao cadastrar os dados do cupom da compra do cliente.');
                                        }
                                    }
                                }

                                $notificacao = new Notificacao('Compra de código <strong>'.$compra->codigo.'</strong> cadastrada com sucesso.');
                                if (isset($_SESSION['produto'])) { unset($_SESSION['produto']); }
                                if (isset($_SESSION['licencas'])) { unset($_SESSION['licencas']); }
                                if (isset($_SESSION['cupom'])) { unset($_SESSION['cupom']); }
                                if (isset($_SESSION['processo_compra'])) { unset($_SESSION['processo_compra']); }

                                // Enviamos email para a Dietwin informando a venda
                                $enviar_email = new Controller_Email;
                                $enviar_email->pagseguro_dietwin($cliente, $compra);

	                            //-----

	                            if (SITE_LOCAL == 'fdserver') {
		                            //$Metriks_CID = 0; $Metriks_FID = 0;
	                            } else {
		                            $Metriks_CID = 2; $Metriks_FID = 16;
	                            }
	                            // Aqui temos que forçar as informações serem enviadas
	                            $_POST['nome'] = $cliente->nome;
	                            $_POST['email'] = $cliente->email;
	                            $_POST['telefone'] = $cliente->telefone_principal;
	                            $_POST['produto'] = $produto->nome;
	                            $_POST['compra_codigo'] = $compra->codigo;
	                            $_POST['forma_pagamento'] = 'Boleto';

	                            @include 'biblioteca/metriks-enviar-conversao.php';

	                            //-----

                                $email = new Controller_Email();
                                $email->boleto_retorno_cliente($cliente, $compra);

                                $cidade = new Model_Cidade($compra_endereco->cidade_id);
                                $estado = new Model_Estado($compra_endereco->estado_id);

                                $view = new View('compra-sucesso.php');
                                $this->view_variaveis_obrigatorias($view);

                                if ($compra_produto->produto_id == 4)
                                {
                                    $classe = 'plano';
                                }
                                elseif($compra_produto->produto_id == 5)
                                {
                                    $classe = 'profissional';
                                }
                                else
                                {
                                    $classe = substr($produto->nome_seo,8);
                                }

                                //parte do adwords que mostra caso haja o retorno da compra
                                $valor_total_adwords = $compra_produto->valor+($compra_produto->licenca_valor*$compra_produto->licenca_quantidade)+$compra->entrega_valor-((isset($compra_cupom) AND $compra_cupom)?$compra_cupom->valor:0);
                                $view->adicionar('valor_total_adwords',$valor_total_adwords);

                                $view->adicionar('classe',$classe);

                                $view->adicionar('body_class', 'compra-sucesso');
                                $view->adicionar('notificacao', new Notificacao);

                                $view->adicionar('compra',$compra);
                                $view->adicionar('compra_endereco',$compra_endereco);
                                $view->adicionar('compra_produto',$compra_produto);
                                $view->adicionar('compra_cupom',(isset($compra_cupom)AND $compra_cupom)?$compra_cupom:NULL);
                                $view->adicionar('cliente',$cliente);
                                $view->adicionar('cidade',$cidade);
                                $view->adicionar('estado',$estado);
                                $view->adicionar('produto',$produto);

                                $view->adicionar('pagina_title', 'Compra Efetuada - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

                                $view->exibir();
                            }
                            else
                            {
                                $notificacao = new Notificacao('Ocorreu um erro ao cadastrar os dados do produto da compra do cliente.');
                            }
                        }
                        else
                        {
                            $notificacao = new Notificacao('As informações sobre o produto da compra estão incompletas.');
                        }
                    }
                    else
                    {
                        $notificacao = new Notificacao('Ocorreu um erro ao cadastrar o endereço da compra do cliente.');
                    }
                }
                else
                {
                    $notificacao = new Notificacao('As informações sobre o endereço da compra estão incompletas.');
                }

            }
            else
            {
                $notificacao = new Notificacao('Erro ao salvar compra.');
            }
        }
        else
        {
            $notificacao = new Notificacao('Todos os campos são obrigatórios');
        }
    }
}
