<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>Login - FatorCMS - dietWin</title>

	<!--                       CSS                       -->

	<!-- Reset Stylesheet -->
	<link rel="stylesheet" href="<?php  echo SITE_BASE ?>/fatorcms/views/css/reset.css" type="text/css" media="screen" />

	<!-- Main Stylesheet -->
	<link rel="stylesheet" href="<?php echo SITE_BASE ?>/fatorcms/views/css/style.css" type="text/css" media="screen" />

	<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
	<link rel="stylesheet" href="<?php echo SITE_BASE ?>/fatorcms/views/css/invalid.css" type="text/css" media="screen" />

	<!--                       Javascripts                       -->

	<!-- jQuery -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function()
		{
			if ($('input[name="usuario"]').length)
			{
				$('input[name="usuario"]').focus();
			}
		});
	</script>
</head>
  
<body id="login" class="<?php echo isset($body_class) ? $body_class : '' ?>">

	<div id="login-wrapper" class="png_bg">
		<div id="login-top">

			<h1>FatorCMS - dietWin</h1>
			<!-- Logo (221px width) -->
			<img id="logo" src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/logo.png" alt="FatorCMS logo" />
		</div> <!-- End #login-top -->

		<div id="login-content">

			<form action="<?php echo SITE_URL ?>/fatorcms/index/login" method="post">

				<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

				<p>
					<label>Usuário</label>
					<input class="text-input" type="text" name="usuario" />
				</p>
				<div class="clear"></div>
				<p>
					<label>Senha</label>
					<input class="text-input" type="password" name="senha" />
				</p>
				<div class="clear"></div>
				<p>
					<input class="button" type="submit" value="Entrar" />
				</p>

			</form>
		</div> <!-- End #login-content -->

	</div> <!-- End #login-wrapper -->

</body>
</html>