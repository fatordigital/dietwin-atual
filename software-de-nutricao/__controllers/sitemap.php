<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/


class Controller_Sitemap extends Controller_Padrao
{
    public function index()
	{
		parent::iniciar();

		$view = new View('sitemap.php');

		//-----

		// Busca todas as notícias
		//$noticia = new Model_Novidade;
        //$noticias = $noticia->select('SELECT * FROM {tabela_nome} WHERE data <= CURRENT_DATE ORDER BY data', TRUE);
		//$view->adicionar('noticias', $noticias);


		// Busca todos os cursos
		//$curso = new Model_Curso;
		//$cursos = $curso->select('SELECT * FROM {tabela_nome} ORDER BY tipo_seo, titulo_seo', TRUE);
		//$view->adicionar('cursos', $cursos);

		//-----

        $faq = new Model_FAQ;
        $faq_profisional = $faq ->select('SELECT * FROM {tabela_nome} WHERE produto_id = "1"');
        $faq_personal = $faq ->select('SELECT * FROM {tabela_nome} WHERE produto_id = "2"');
        $faq_rotulo = $faq ->select('SELECT * FROM {tabela_nome} WHERE produto_id = "3"');
        $faq_geral = $faq ->select('SELECT * FROM {tabela_nome} WHERE produto_id = "0"');
        $view->adicionar('faq_profisional', $faq_profisional, 'faq_personal', $faq_personal, 'faq_rotulo', $faq_rotulo, 'faq_geral', $faq_geral);

		$view->exibir();
    }

} // end class