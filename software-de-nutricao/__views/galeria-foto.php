<?php include 'includes/cabecalho.php'; ?>
<body class="softwares <?php echo $classe ?> galeria-fotos">

<?php include 'includes/topo.php'; ?>
	
    <div class="titulo-principal">
        <p><span>dietWin</span><br /><?php echo substr($galeria->nome,8) ?></p>
    </div>
    <div class="titulo-resumo">
        <p>O software mais abrangente da família dietWin é também a ferramenta mais completa do mercado. Desenvolvida para profissionais exigentes que necessitam de todas as informações ao seu alcance e de ferramentas que facilitem o seu dia-a-dia.</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container">
    	
        <?php include 'includes/toolbar.php' ?>
        
        <h1>Galeria de fotos - <span><?php /*dietWin Profissional */ echo $galeria->titulo ?></span></h1>
        
        <div class="box">
            <p><?php echo $galeria->descricao ?></p>
            <?php
            if ($fotos AND count($fotos) > 0)
            {
            ?>
            <ul class="thumbs">
                <?php
                foreach ($fotos as $foto)
                {
                ?>
                <li><a href="<?php echo SITE_BASE ?>/arquivos/galerias/<?php echo $galeria->id ?>/<?php echo $foto->arquivo ?>" rel="<?php echo $classe ?>" title="<?php echo $foto->titulo ?>"><img src="<?php echo SITE_BASE ?>/arquivos/galerias/<?php echo $galeria->id ?>/thumb1/<?php echo $foto->arquivo ?>" alt="<?php echo $foto->titulo ?>" /></a></li>
                <?php
                }
                ?>
                <?php /*
            	<li><a href="<?php echo SITE_BASE ?>/views/imagens/imagem-1.jpg" rel="profissional" title="Título imagem 1"><img src="<?php echo SITE_BASE ?>/views/imagens/thumb-1.jpg" alt="Título imagem 1" /></a></li>
                <li><a href="<?php echo SITE_BASE ?>/views/imagens/imagem-2.jpg" rel="profissional" title="Título imagem 2"><img src="<?php echo SITE_BASE ?>/views/imagens/thumb-2.jpg" alt="Título imagem 2" /></a></li>
                <li><a href="<?php echo SITE_BASE ?>/views/imagens/imagem-3.jpg" rel="profissional" title="Título imagem 3"><img src="<?php echo SITE_BASE ?>/views/imagens/thumb-3.jpg" alt="Título imagem 3" /></a></li>
                <li><a href="<?php echo SITE_BASE ?>/views/imagens/imagem-4.jpg" rel="profissional" title="Título imagem 4"><img src="<?php echo SITE_BASE ?>/views/imagens/thumb-4.jpg" alt="Título imagem 4" /></a></li>
                <li><a href="<?php echo SITE_BASE ?>/views/imagens/imagem-1.jpg" rel="profissional" title="Título imagem 1"><img src="<?php echo SITE_BASE ?>/views/imagens/thumb-1.jpg" alt="Título imagem 1" /></a></li>
                <li><a href="<?php echo SITE_BASE ?>/views/imagens/imagem-2.jpg" rel="profissional" title="Título imagem 2"><img src="<?php echo SITE_BASE ?>/views/imagens/thumb-2.jpg" alt="Título imagem 2" /></a></li>
                <li><a href="<?php echo SITE_BASE ?>/views/imagens/imagem-3.jpg" rel="profissional" title="Título imagem 3"><img src="<?php echo SITE_BASE ?>/views/imagens/thumb-3.jpg" alt="Título imagem 3" /></a></li>
                <li><a href="<?php echo SITE_BASE ?>/views/imagens/imagem-4.jpg" rel="profissional" title="Título imagem 4"><img src="<?php echo SITE_BASE ?>/views/imagens/thumb-4.jpg" alt="Título imagem 4" /></a></li>
                <li><a href="<?php echo SITE_BASE ?>/views/imagens/imagem-1.jpg" rel="profissional" title="Título imagem 1"><img src="<?php echo SITE_BASE ?>/views/imagens/thumb-1.jpg" alt="Título imagem 1" /></a></li>
                <li><a href="<?php echo SITE_BASE ?>/views/imagens/imagem-2.jpg" rel="profissional" title="Título imagem 2"><img src="<?php echo SITE_BASE ?>/views/imagens/thumb-2.jpg" alt="Título imagem 2" /></a></li>
                <li><a href="<?php echo SITE_BASE ?>/views/imagens/imagem-3.jpg" rel="profissional" title="Título imagem 3"><img src="<?php echo SITE_BASE ?>/views/imagens/thumb-3.jpg" alt="Título imagem 3" /></a></li>
                <li><a href="<?php echo SITE_BASE ?>/views/imagens/imagem-4.jpg" rel="profissional" title="Título imagem 4"><img src="<?php echo SITE_BASE ?>/views/imagens/thumb-4.jpg" alt="Título imagem 4" /></a></li>
                */ ?>
            </ul>
            <?php
            }
            else
            {
                echo '<p>Não há fotos cadastradas para essa galeria.</p>';
            }
            ?>
            <div class="clear"></div>
        </div>
        
        <div class="opcoes">
        	<a href="#" title="Converse agora mesmo com nosso atendimento online"><img src="<?php echo SITE_BASE ?>/views/imagens/botao-chat.png" alt="Converse agora mesmo com nosso atendimento online" /></a>
            <a href="#" class="link-mais-info" title="Solicite maiores informações sobre a ferramenta"><img src="<?php echo SITE_BASE ?>/views/imagens/botao-mais-info.png" alt="Solicite maiores informações sobre a ferramenta" /></a>

            <!-- Conteúdo do colorbox do botão "Mais Informações" -->
            <div style="display:none">
                <div class="colorbox-mais-info">
                    <div class="topo"></div>
                    <div class="box">
                        <div class="titulo-colorbox">Mais Informações</div>
                        <div class="conteudo-colorbox">

                            <p>Preencha o formulário abaixo para solicitar mais informações sobre este software.</p>
                            <form method="post" action="<?php echo SITE_URL ?>/profissional/mais_info" id="form-mais-info" >

                                <label><b>Nome:</b></label>
                                <input type="text" class="obrigatorio" name="info_nome" id="info_nome" maxlength="100" />

                                <label><b>E-mail:</b></label>
                                <input type="text" class="obrigatorio" name="info_email" id="info_email" maxlength="255" />

                                <label><b>Telefone:</b></label>
                                <input type="text" class="obrigatorio" name="info_telefone" id="info_telefone" maxlength="14" />

                                <label><b>Cidade:</b></label>
                                <input type="text" class="obrigatorio" name="info_cidade" id="info_cidade" maxlength="100" />

                                <label><b>Mensagem:</b></label>
                                <textarea type="text" class="obrigatorio" name="info_mensagem" id="info_mensagem" style="resize: vertical; overflow:auto"></textarea>

                                <button type="submit">Enviar</button>
                                <div class="clear"></div>
                            </form>

                            <div id="form_notification"></div>

                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="inferior"></div>
                </div>
            </div>
            <!-- Fim do colorbox do botão "Mais Informações" -->

            <a href="<?php echo SITE_URL ?>/suporte/<?php echo $classe ?>/videos" title="Acesse o centro de multimídia e treinamento dessa ferramenta"><img src="<?php echo SITE_BASE ?>/views/imagens/botao-centro-multimidia.png" alt="Acesse o centro de multimídia e treinamento dessa ferramenta" /></a>
            <div class="separador"></div>
            <a href="<?php echo SITE_URL ?>/software/<?php echo $classe ?>/comprar" class="comprar-<?php echo $classe ?>" title="Compre Online seu dietWin <?php echo $galeria->titulo ?>"><img src="<?php echo SITE_BASE ?>/views/imagens/botao-compre-online-<?php echo $classe ?>.png" alt="Compre Online seu dietWin <?php echo $galeria->titulo ?>" /></a>

            <div class="separador"></div>

            <h3>Conheça as outras ferramentas</h3>

            <ul>
                <li class="bubbleInfo <?php echo ($classe == 'profissional') ? ' inativo' : ''; ?>" id="pop10">
                    <a href="#" class="trigger" title="Conheça o dietWin Profissional ">
                        <img src="<?php echo SITE_BASE ?>/views/imagens/opcao-profissional.jpg" alt="Conheça o dietWin Profissional" />
                        <table id="tpop10" class="popup">
                            <tbody>
                            <tr>
                                <td class="topleft corner"></td> <td class="top"></td>	<td class="topright corner"></td>
                            </tr>
                            <tr>
                                <td class="left"></td> <td><table class="popup-contents">
                                <tbody>
                                <tr>
                                    <td><b>Profissional </b></td>
                                </tr>
                                </tbody></table>
                            </td>
                                <td class="right"></td>
                            </tr>
                            <tr>
                                <td class="bottomleft corner"></td> <td class="bottom"><img width="13" height="29" alt="popup tail" src="<?php echo SITE_BASE ?>/views/imagens/menu-bubble-tail2.gif"/></td> <td class="bottomright corner"></td>
                            </tr>
                            </tbody>
                        </table>
                    </a>
                </li>
                <li class="bubbleInfo <?php echo ($classe == 'personal') ? ' inativo' : ''; ?>" id="pop11">
                    <a href="#" class="trigger" title="Conheça o dietWin Personal">
                        <img src="<?php echo SITE_BASE ?>/views/imagens/opcao-personal.jpg" alt="Conheça o dietWin Personal" />
                        <table id="tpop11" class="popup">
                            <tbody>
                            <tr>
                                <td class="topleft corner"></td> <td class="top"></td>	<td class="topright corner"></td>
                            </tr>
                            <tr>
                                <td class="left"></td> <td><table class="popup-contents">
                                <tbody>
                                <tr>
                                    <td><b>Personal </b></td>
                                </tr>
                                </tbody></table>
                            </td>
                                <td class="right"></td>
                            </tr>
                            <tr>
                                <td class="bottomleft corner"></td> <td class="bottom"><img width="13" height="29" alt="popup tail" src="<?php echo SITE_BASE ?>/views/imagens/menu-bubble-tail2.gif"/></td> <td class="bottomright corner"></td>
                            </tr>
                            </tbody>
                        </table>
                    </a>
                </li>
                <li class="bubbleInfo <?php echo ($classe == 'rotulo-de-alimentos') ? ' inativo' : ''; ?>" id="pop12">
                    <a href="#" class="trigger" title="Conheça o dietWin Rótulo de Alimentos">
                        <img src="<?php echo SITE_BASE ?>/views/imagens/opcao-rotulo-de-alimentos.jpg" alt="Conheça o dietWin Rótulo de Alimentos" />
                        <table id="tpop12" class="popup">
                            <tbody>
                            <tr>
                                <td class="topleft corner"></td> <td class="top"></td>	<td class="topright corner"></td>
                            </tr>
                            <tr>
                                <td class="left"></td> <td><table class="popup-contents">
                                <tbody>
                                <tr>
                                    <td><b>Rótulo de Alimentos</b></td>
                                </tr>
                                </tbody></table>
                            </td>
                                <td class="right"></td>
                            </tr>
                            <tr>
                                <td class="bottomleft corner"></td> <td class="bottom"><img width="13" height="29" alt="popup tail" src="<?php echo SITE_BASE ?>/views/imagens/menu-bubble-tail2.gif"/></td> <td class="bottomright corner"></td>
                            </tr>
                            </tbody>
                        </table>
                    </a>
                </li>
            </ul>
        </div>
        
        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>  
</div>

<?php include 'includes/rodape.php'; ?>