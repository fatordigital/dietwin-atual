<?php require_once 'includes/head.php' ?>
<body>
<?php require_once 'includes/nav.php' ?>

<?php $versao = substr($versao, 1) ?>
<?php $atual = 3048 ?>

<section class="conteudo cinza">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
      <h1 class="titulo-pagina-versao">Versão do Dietwin instalada: <strong class="cod-versao-<?php echo $versao >= $atual ? 'atual' : 'antiga' ?>">V<?php echo $versao ?></strong></h1>
      <?php if($versao >= $atual): ?>
      <span class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-centered box-versao-atual">Sua versão do Dietwin está atualizada!</span>
      <?php else: ?>
      <span class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-centered box-versao-antiga">
        <span class="pull-left">Sua versão do Dietwin está desatualizada!</span>
        <small><a href="http://pages.dietwin.com.br/atualizar-dietwin" title="Clique aqui"><u>Clique aqui</u></a> para solicitar atualização.</small>
      </span>
      <?php endif; ?>
    </div>
  </div>
</section>
<section class="conteudo cinza especialista-mapa">
  <div class="container">
    <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
      <img src="<?php echo SITE_URL ?>/views/imagens/especialista-dietwin.jpg" alt="Dietwin. Auxiliando o nutricionista na tomada de decisão. " class="img-responsive">
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <h2><span>Dietwin.</span> Auxiliando o nutricionista na tomada de decisão. </h2>
      <p>Nós também queremos ser o seu aliado para você decidir pelo melhor software de nutrição. Caso você queira ainda mais informações ou falar pessoalmente, entre em contato. Teremos o maior prazer em atendê-lo. </p>
      <div class="telefone text-center">
        <span>(51)</span> 3337.7908
      </div>
      <img src="<?php echo SITE_URL ?>/views/imagens/mapa-localizacao.jpg" alt="Mapa localização Dietwin" class="img-responsive hidden-xs mapa-dietwin">
    </div>
  </div>
  <a id="contato"></a>
</section>
<?php require_once 'includes/footer.php' ?>
</body>
</html>
