<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_PagSeguroCompra extends Controller_Padrao
{
    /**
     * Chama o construtor da classe pai
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método inicial que faz a renderização básica da página
     * @param $parametros
     * @return void
     */
    public function index($parametros)
    {
        // Para que as credenciais fiquem disponíveis
        global $pagseguro_config;

        if ( ! isset($_SESSION['processo_compra']))
        {
            header('Location: '.SITE_URL.'/index.php');
	        exit;
        }

        if ( ! isset($_SESSION['cliente_id']))
        {
            header('Location: '.SITE_URL.'/index.php');
	        exit;
        }

        $entrega_forma = ((isset($parametros->entrega_forma) AND strlen($parametros->entrega_forma)>0)?Funcoes::mysqli_escape($parametros->entrega_forma):'download');

        if (isset($entrega_forma) AND ($entrega_forma == 'pac' OR $entrega_forma == 'sedex' OR $entrega_forma == 'download'))
        {
            //Validacoes

            //Recuperamos o numero de licencas da sessão
            $licencas = $_SESSION['licencas'];

            //Recuperamos o produto da sessão
            $produto = $_SESSION['produto'];

            //Recuperamos o produto caso haja
            $cupom = (isset($_SESSION['cupom'])?$_SESSION['cupom']:NULL);

            //Recuperamos o cliente da sessão
            $cliente = new Model_Cliente($_SESSION['cliente_id']);

            //Recuperamos o endereço do cliente
            $endereco = new Model_ClienteEndereco();
            $endereco = $endereco->select('SELECT * FROM {tabela_nome} WHERE entrega=1 AND cliente_id='.$cliente->id);
            if (!isset($endereco) OR !$endereco)
            {
                //Caso não possua o endereço de entrega, usa o endereço principal
                $endereco = new Model_ClienteEndereco();
                $endereco = $endereco->select('SELECT * FROM {tabela_nome} WHERE entrega=0 AND cliente_id='.$cliente->id);
            }

            if ($entrega_forma == 'pac')
            {
                $entrega_forma = 'PAC';
                $valor_frete = 12.00;
            }
            else if($entrega_forma == 'sedex')
            {
                $entrega_forma = 'Sedex';
                $frete = new Controller_Frete();
                $valor_frete = $frete->calcular($endereco->cep);
            }
            else
            {
                $entrega_forma = 'Download';
                $valor_frete = 0;
            }


            $compra = new Model_Compra();
            $compra->cliente_id = $cliente->id;
            $compra->data = date('d/m/Y H:i:s');
            $compra->entrega_forma = $entrega_forma;
            $compra->entrega_valor = $valor_frete;
            $compra->pagamento_metodo = 'PagSeguro';
            $compra->situacao = 'Aguardando pagamento';
            $compra->inicio = date('Y-m-d');
            $compra->fim = date('Y-m-d', mktime(0,0,0,date('m'),date('d'),date('Y')+1));

            if ($compra->insert())
            {
                // Criamos o código para a compra e atualizamos ela
                $compra_atualizar = new Model_Compra;
                $compra_atualizar->id = $compra->id;
                $compra_atualizar->codigo = $compra->id + 1234; // Não mexer nesta soma, ela deve ser IGUAL à do site
                $compra_atualizar->update();
                $compra->codigo = $compra_atualizar->codigo;

                // Cadastramos o endereço da compra

                $compra_endereco = new Model_CompraEndereco();
                $compra_endereco->carregar(new Model_ClienteEndereco($endereco->id));

                // Campos especiais
                $compra_endereco->id = NULL;
                $compra_endereco->compra_id = $compra->id;

                $compra_endereco->colunas_mysqli_escape();
                if ($compra_endereco->verificar_null(array('id')))
                {
                    if ($compra_endereco->insert())
                    {
                        // Cadastramos o produto da compra
                        $compra_produto = new Model_CompraProduto;
                        $compra_produto->carregar($produto);

                        // Campos especiais
                        $compra_produto->compra_id = $compra->id;

                        // Colocamos os valores nas colunas corretas após o carregamento
                        $compra_produto->produto_id = $compra_produto->id;
                        $compra_produto->id = NULL;

                        // Definimos a versão do produto
                        $compra_produto->versao = $produto->versao;

                        // Definimos a quantidade de licenças com a compra do produto
                        $compra_produto->licenca_quantidade = $licencas;

                        $compra_produto->colunas_mysqli_escape();

                        if ($compra_produto->verificar_null(array('id')))
                        {
                            if ($compra_produto->insert())
                            {
                                // Cadastramos o cupom da compra, se houver um
                                if (isset($cupom))
                                {
                                    $compra_cupom = new Model_CompraCupom;
                                    $compra_cupom->carregar($cupom);

                                    // Campos especiais
                                    $compra_cupom->compra_id = $compra->id;

                                    // Colocamos os valores nas colunas corretas após o carregamento
                                    $compra_cupom->cupom_id = $compra_cupom->id;
                                    $compra_cupom->id = NULL;

                                    $compra_cupom->colunas_mysqli_escape();

                                    if ($compra_cupom->verificar_null(array('id')))
                                    {
                                        if ( ! $compra_cupom->insert())
                                        {
                                            $notificacao = new Notificacao('Ocorreu um erro ao cadastrar os dados do cupom da compra do cliente.');
                                        }
                                    }
                                }

                                $notificacao = new Notificacao('Compra de código <strong>'.$compra->codigo.'</strong> cadastrada com sucesso.');
                                if (isset($_SESSION['produto'])) { unset($_SESSION['produto']); }
                                if (isset($_SESSION['licencas'])) { unset($_SESSION['licencas']); }
                                if (isset($_SESSION['cupom'])) { unset($_SESSION['cupom']); }
                                if (isset($_SESSION['processo_compra'])) { unset($_SESSION['processo_compra']); }

                                //-----

                                // Enviamos email para a dietWin informando a venda
                                $enviar_email = new Controller_Email;
                                $enviar_email->pagseguro_dietwin($cliente, $compra);

	                            if (SITE_LOCAL == 'fdserver') {
		                            //$Metriks_CID = 0; $Metriks_FID = 0;
	                            } else {
		                            $Metriks_CID = 2; $Metriks_FID = 16;
	                            }
	                            // Aqui temos que forçar as informações serem enviadas
	                            $_POST['nome'] = $cliente->nome;
	                            $_POST['email'] = $cliente->email;
	                            $_POST['telefone'] = $cliente->telefone_principal;
	                            $_POST['produto'] = $produto->nome;
	                            $_POST['compra_codigo'] = $compra->codigo;
	                            $_POST['forma_pagamento'] = 'PagSeguro';

	                            @include 'biblioteca/metriks-enviar-conversao.php';

                                //--------------------

                                // Fazemos toda a parte do PagSeguro após o trabalho no banco por conflito entre métodos Autoloaders

                                require_once 'biblioteca/PagSeguroLibrary/PagSeguroLibrary.php';

                                // Carregamos as credenciais de acesso ao PagSeguro
                                $pg_credenciais = new PagSeguroAccountCredentials($pagseguro_config[SITE_LOCAL]['email'], $pagseguro_config[SITE_LOCAL]['token']);

                                // Criamos o objeto que fará a requisição de pagamento ao PagSeguro
                                $pg_pagamento_requisicao = new PagSeguroPaymentRequest();

                                // Adiciona o produto na requisição
                                $pg_pagamento_requisicao->addItem(
                                    array(
                                        'id' => $produto->id,
                                        'description' => utf8_decode($produto->nome).((isset($cupom) AND $cupom->id>0)?' - Desconto cupom promocional':''),
                                        'quantity' => 1,
                                        'amount' => number_format($produto->valor-((isset($cupom) AND $cupom->id>0)?$cupom->valor:0), 2, '.', '')
                                    )
                                );

                                if ($licencas > 0)
                                {
                                    $pg_pagamento_requisicao->addItem(
                                        array(
                                            'id' => $produto->id,
                                            'description' => utf8_decode('Licenças '.$produto->nome),
                                            'quantity' => $licencas,
                                            'amount' => number_format($produto->licenca_valor, 2, '.', '')
                                        )
                                    );
                                }

                                //-----

                                // Carregamos as informações do cliente na requisição
                                $comprador = new PagSeguroSender();
                                $comprador->setName($cliente->nome);
                                $comprador->setEmail($cliente->email);
                                $comprador->setPhone(substr($cliente->telefone_principal, 0, 2), substr($cliente->telefone_principal, 2));

                                $pg_pagamento_requisicao->setSender($comprador);

                                //-----

                                $cidade = new Model_Cidade($endereco->cidade_id);
                                $estado = new Model_Estado($endereco->estado_id);

                                $pg_pagamento_requisicao->setShippingAddress(
                                    array(
                                        'postalCode' => $endereco->cep,
                                        'street' => $endereco->endereco,
                                        'number' => $endereco->numero,
                                        'complement' => $endereco->complemento,
                                        'district' => $endereco->bairro,
                                        'city' => $cidade->nome,
                                        'state' => $estado->sigla,
                                        'country' => 'BRA'
                                    )
                                );

                                //-----

                                $pg_pagamento_requisicao->setExtraAmount($valor_frete);

                                // Nosso código de referência é a letra da loja, mais o ID do cliente mais o ID da compra
                                // B = Brubins loja virtual
                                // D = Dietwin
                                $pg_pagamento_requisicao->setReference('D-'.$cliente->id.'-'.$compra->id);

                                // Para onde o cliente deve ser redirecionado após o pagamento
                                $pg_pagamento_requisicao->setRedirectURL(SITE_URL.'/pagseguro-compra/retorno'); //assim que for para o ar alterar o site da fatordigital por site_url.'/pagseguro/retorno'

                                // Moeda utilizada na transação
                                $pg_pagamento_requisicao->setCurrency("BRL");

                                // Definimos que a requisição de pagamento só poderá ser usada 1 vez
                                $pg_pagamento_requisicao->setMaxUses(1);

                                // Tipo de envio da compra
                                $pg_pagamento_requisicao->setShippingType(3); // 1 = PAC, 2 = SEDEX, 3 = Não especificado

                                // Envia a requisição para o PagSeguro
                                $pg_url = $pg_pagamento_requisicao->register($pg_credenciais);
                                // Envia o cliente para o PagSeguro
                                header('Location: '.$pg_url);
                                exit;

                            }
                            else
                            {
                                $notificacao = new Notificacao('Ocorreu um erro ao cadastrar os dados do produto da compra do cliente.');
                            }
                        }
                        else
                        {
                            $notificacao = new Notificacao('As informações sobre o produto da compra estão incompletas.');
                        }
                    }
                    else
                    {
                        $notificacao = new Notificacao('Ocorreu um erro ao cadastrar o endereço da compra do cliente.');
                    }
                }
                else
                {
                    $notificacao = new Notificacao('As informações sobre o endereço da compra estão incompletas.');
                }

            }
            else
            {
                $notificacao = new Notificacao('Erro ao salvar compra.');
            }
        }
        else
        {
            $notificacao = new Notificacao('Todos os campos são obrigatórios');
        }

        header('HTTP/1.1 404 Not Found');
        $erro_404 = new Controller_Erro;
        $erro_404->index(404);
        exit;
    }


	/**
	 * Método que recebe o redirecionado do site da Brubins e o retorno para o cliente.
	 * @param $parametros
	 */
	public function retorno($parametros)
	{
        require_once 'biblioteca/PagSeguroLibrary/PagSeguroLibrary.php';

        //VERIFICAR SEMPRE A PÁGINA DE RETORNO DO PAGSEGURO

		// Para que as credenciais fiquem disponíveis
		global $pagseguro_config;

		$pg_credenciais = new PagSeguroAccountCredentials($pagseguro_config[SITE_LOCAL]['email'], $pagseguro_config[SITE_LOCAL]['token']);

		// Vem da Brubins
		if (isset($parametros->_0, $parametros->_1))
		{
			$pg_notificacao_tipo = $parametros->_0;
			$pg_notificacao_codigo = $parametros->_1;
		}
		// Para nossos testes diretos
		if (isset($parametros->notificationType, $parametros->notificationCode))
		{
			$pg_notificacao_tipo = $parametros->notificationType;
			$pg_notificacao_codigo = $parametros->notificationCode;
		}

		if (isset($pg_notificacao_tipo, $pg_notificacao_codigo))
		{
			// Verificando tipo de notificação recebida
			if ($pg_notificacao_tipo == 'transaction')
			{
				// Obtendo o objeto Transaction a partir do código de notificação
				$pg_transacao = PagSeguroNotificationService::checkTransaction($pg_credenciais, $pg_notificacao_codigo);

				if ($pg_transacao)
				{
					list($loja, $cliente_id, $compra_id) = explode('-', $pg_transacao->getReference());

					// B = Brubins
					// D = dietWin
					if ($loja == 'B')
					{
						// Não fazemos nada porque este código está na Brubins
						new Log('Retorno inesperado da Brubins na dietWin. notificationType:'.$pg_notificacao_tipo.'|notificationCode:'.$pg_notificacao_codigo);
					}
					elseif ($loja == 'D')
					{
                        $pagseguro = new Model_PagSeguro();
						$pagseguro->compra_id = $compra_id;
						$pagseguro->data = date('Y-m-d H:i:s', strtotime($pg_transacao->getLastEventDate()));
						$pagseguro->identificador = $pg_transacao->getCode();
						$pagseguro->referencia = $pg_transacao->getReference();
						$pagseguro->transacao_status_id = $pg_transacao->getStatus()->getValue();
						$pagseguro->pagamento_tipo_id = $pg_transacao->getPaymentMethod()->getType()->getValue();
						$pagseguro->pagamento_codigo_id = $pg_transacao->getPaymentMethod()->getCode()->getValue();

						if ($pagseguro->insert())
						{
							$cliente = new Model_Cliente($cliente_id);
							$compra = new Model_Compra($compra_id);

							// Aqui envia o email para o cliente quando a transação for aprovada
							if ($pagseguro->transacao_status_id == 3) // 3 = Paga
							{
                                //echo 'entrou aki';
                                $email = new Controller_Email();
								$email->pagseguro_retorno_cliente($cliente, $compra, $pagseguro);
							}
						}
						else
						{
							$log = new Log('Erro ao inserir o retorno do PagSeguro no banco. $referencia='.$pg_transacao->getReference().' $identificador='.$pg_transacao->getCode());
						}
					}
					else
					{
						// Salvamos as informações dessa transação que não bate com as nossas
						new Log('Compra inesperada. notificationType:'.$pg_notificacao_tipo.'|notificationCode:'.$pg_notificacao_codigo);
					}
				}
				else
				{
					$log = new Log('Erro ao trazer a transação do PagSeguro. $pg_transacao='.var_export($pg_transacao, TRUE));
				}
			}
			else
			{
				$log = new Log('Erro ao receber a transação, sem parametros');
			}

		}
        else
        {
            // Momento do redirecionamento do cliente, buscamos
            if($_GET['id'])
            {
                $transaction_id = trim($_GET['id']);
                $transaction = PagSeguroTransactionSearchService::searchByCode($pg_credenciais, $transaction_id);

                if ( ! is_null($transaction->getCode()))
                {
                    list($loja, $cliente_id, $compra_id) = explode('-', $transaction->getReference());

                    if ($loja == 'D')
                    {
                        // Carregamos a compra
                        $compra = new Model_Compra($compra_id);

                        // Salvamos em uma variável o id da transação do PagSeguro
                        $pagseguro_identificador = $transaction->getCode();

                        //Cliente q fez a compra
                        $cliente = new Model_Cliente($cliente_id);

                        $compra_produto = new Model_CompraProduto();
                        $compra_produto = $compra_produto->select('SELECT * FROM {tabela_nome} WHERE compra_id ='.$compra->id);

                        $produto = new Model_Produto($compra_produto->produto_id);

                        $compra_endereco = new Model_CompraEndereco();
                        $compra_endereco = $compra_endereco->select('SELECT * FROM {tabela_nome} WHERE compra_id='.$compra->id);

                        $cidade = new Model_Cidade($compra_endereco->cidade_id);
                        $estado = new Model_Estado($compra_endereco->estado_id);

                        $compra_cupom = new Model_CompraCupom();
                        $compra_cupom = $compra_cupom->select('SELECT * FROM {tabela_nome} WHERE compra_id='.$compra->id);

                        $view = new View('compra-sucesso.php');
                        $this->view_variaveis_obrigatorias($view);

                        $view->adicionar('body_class', 'compra-sucesso');
                        $view->adicionar('notificacao', new Notificacao);

                        if ($produto->id == 4)
                        {
                            $classe = 'plano';
                        }
                        else if ($produto->id == 5)
                        {
                            $classe = 'profissional';
                        }
                        else
                        {
                            $classe = substr($produto->nome_seo,8);
                        }

                        $view->adicionar('classe',$classe);

                        //parte do adwords que mostra caso haja o retorno da compra
                        //parte do adwords que mostra caso haja o retorno da compra
                        $valor_total_adwords = $compra_produto->valor+($compra_produto->licenca_valor*$compra_produto->licenca_quantidade)+$compra->entrega_valor-((isset($compra_cupom) AND $compra_cupom)?$compra_cupom->valor:0);
                        $view->adicionar('valor_total_adwords',$valor_total_adwords);

                        $view->adicionar('compra',$compra);
                        $view->adicionar('compra_endereco',$compra_endereco);
                        $view->adicionar('compra_produto',$compra_produto);
                        $view->adicionar('compra_cupom',$compra_cupom);
                        $view->adicionar('cliente',$cliente);
                        $view->adicionar('pagseguro',$pagseguro_identificador);
                        $view->adicionar('cidade',$cidade);
                        $view->adicionar('estado',$estado);
                        $view->adicionar('produto',$produto);

                        $view->adicionar('pagina_title', 'Compra Efetuada - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

                        $view->exibir();
                    }
                    else
                    {
                        header('HTTP/1.1 404 Not Found');
                        $erro_404 = new Controller_Erro;
                        $erro_404->index(404);
                        exit;
                    }

                }
            }
            else
            {
                header('HTTP/1.1 404 Not Found');
                $erro_404 = new Controller_Erro;
                $erro_404->index(404);
                exit;
            }
        }
	}

    public function teste($parametros)
    {
        $view = new View('compra-sucesso.php');
        $this->view_variaveis_obrigatorias($view);

        $view->adicionar('body_class', 'compra-sucesso');
        $view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Compra Efetuada - dietWin - Softwares de Nutrição');

        $view->exibir();
    }


} // end class
