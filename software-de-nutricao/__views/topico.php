<?php include 'includes/cabecalho.php'; ?>

<body class="suporte">

<?php include 'includes/topo.php'; ?>

<div class="titulo-principal">
    <p><span>Tire suas</span><br />dúvidas</p>
</div>
<div class="titulo-resumo">
    <p>O FAQ (Frequently Asked Questions - Perguntas mais frequentes) é um local onde condensamos todas as principais dúvidas dos usuários do dietWin.</p>
</div>
</div>
</div>

<div class="conteudo">
    <div class="container">

        <?php include 'includes/toolbar.php' ?>

        <div id="tipo_suporte" class="<?php echo $classe ?>">
            <h1>Exibindo categoria: <span><?php echo $categoria ?></span></h1>

            <div class="pergunta" id="tamanho-fonte">
                <?php
                foreach ($faqs as $faq)
                {
                ?>
                <h2><?php echo mb_strtolower($faq->pergunta) ?></h2>
                <div class="descricao">
                    <?php echo $faq->resposta; ?>

                    <a href="#" class="link-info" title="Solicitar mais informações sobre esse tópico">Solicitar mais informações sobre esse tópico</a>

                    <!-- Conteúdo do colorbox do botão "Mais Informações" -->
                    <div style="display:none">
                        <div class="colorbox-mais-info">
                            <div class="topo"></div>
                            <div class="box">
                                <div class="titulo-colorbox">Mais Informações sobre esse Tópico</div>
                                <div class="conteudo-colorbox">

                                    <p>Preencha o formulário abaixo para solicitar mais informações sobre o tópico <span><?php echo $faq->pergunta; ?></span>.</p>
                                    <form method="post" action="<?php echo SITE_URL ?>/faq/duvida" id="form-mais-info" >

                                        <input type="hidden" name="categoria" value="<?php echo $categoria; ?>" />
                                        <input type="hidden" name="pergunta" value="<?php echo $faq->pergunta; ?>" />

                                        <label><b>Nome:</b></label>
                                        <input type="text" class="obrigatorio" name="info_nome" id="info_nome" maxlength="100" />

                                        <label><b>E-mail:</b></label>
                                        <input type="text" class="obrigatorio" name="info_email" id="info_email" maxlength="255" />

                                        <label><b>Telefone:</b></label>
                                        <input type="text" class="obrigatorio" name="info_telefone" id="info_telefone" maxlength="14" />

                                        <label><b>Cidade:</b></label>
                                        <input type="text" class="obrigatorio" name="info_cidade" id="info_cidade" maxlength="100" />

                                        <label><b>Mensagem:</b></label>
                                        <textarea type="text" class="obrigatorio" name="info_mensagem" id="info_mensagem" style="resize: vertical; overflow:auto"></textarea>

                                        <button type="submit">Enviar</button>
                                        <div class="clear"></div>
                                    </form>

                                    <div id="form_notification"></div>

                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="inferior"></div>
                        </div>
                    </div>

                    <a href="<?php echo SITE_URL.'/suporte/'.$classe.'/faq' ?>" class="link-voltar" title="Voltar para a listagem de tópicos da categoria <?php echo $categoria ?>">Voltar para tópicos sobre <?php echo $categoria ?></a>
                </div>
                <?php
                }
                ?>
            </div>
        </div>

        <div class="sidebar">
            <div class="filtro">
                <h2>Filtre os tópicos</h2>
                <p>Clique para marcar apenas as categorias de tópicos que você deseja visualizar.</p>
                <a href="<?php echo SITE_URL.'/suporte/personal/faq'; ?>" class="filtro-personal<?php if ($categoria == 'Personal') { echo ' ativo'; } ?>" title="Filtrar por Personal "><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-personal<?php if ($categoria == 'Personal') { echo '-ativo'; } ?>.png" alt="Filtrar por Personal " /></a>
                <a href="<?php echo SITE_URL.'/suporte/profissional/faq'; ?>" class="filtro-profissional<?php if ($categoria == 'Profissional') { echo ' ativo'; } ?>" title="Filtrar por Profissional "><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-profissional<?php if ($categoria == 'Profissional') { echo '-ativo'; } ?>.png" alt="Filtrar por Profissional " /></a>
                <a href="<?php echo SITE_URL.'/suporte/rotulo-de-alimentos/faq'; ?>" class="filtro-rotulo-alimentos<?php if ($categoria == 'Rótulo de Alimentos') { echo ' ativo'; } ?>" title="Filtrar por Rótulo de Alimentos"><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-rotulo<?php if ($categoria == 'Rótulo de Alimentos') { echo '-ativo'; } ?>.png" alt="Filtrar por Rótulo de Alimentos" /></a>
                <a href="<?php echo SITE_URL.'/suporte/topicos-diversos/faq'; ?>" class="filtro-topicos-diversos<?php if ($categoria == 'Tópicos Diversos') { echo ' ativo'; } ?>" title="Filtrar por Tópicos diversos"><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-topicos<?php if ($categoria == 'Tópicos Diversos') { echo '-ativo'; } ?>.png" alt="Filtrar por Tópicos diversos" /></a>
            </div>
            <?php
            if ($faqs AND count($faqs) > 0)
            {
                ?>
                <div class="filtro">
                    <h2>Busque por palavra-chave</h2>
                    <form method="post" action="<?php echo ($categoria != 'Todas') ? SITE_URL.'/suporte/'.$classe : SITE_URL ?>/faq" id="buscar" >
                        <input type="text" name="buscar" id="busca-campo" maxlength="255" value="" />
                        <button type="submit">Buscar</button>
                    </form>
                    <div class="clear"></div>
                </div>
                <?php
            }
            ?>
        </div>

        <div class="clear"></div>
    </div>
    <div class="inferior"></div>
</div>

<?php include 'includes/rodape.php'; ?>