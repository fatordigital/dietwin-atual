<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_GaleriaFoto extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{

        if (isset($parametros->categoria) AND strlen($parametros->categoria))
        {
            $produto = new Model_Produto();
            $produto = $produto->select("SELECT * FROM {tabela_nome} WHERE nome_seo = 'dietwin-".Funcoes::mysqli_escape($parametros->categoria)."'");
            //echo "SELECT * FROM {tabela_nome} WHERE nome_seo = 'dietwin-".Funcoes::mysqli_escape($parametros->categoria)."'"; exit;
            //print_r($produto); exit;
            if ($produto)
            {
                $galeria = new Model_Galeria();
                $galeria = $galeria->select('SELECT {tabela_nome}.*,fd_produtos.nome_seo,fd_produtos.nome FROM {tabela_nome} LEFT JOIN fd_produtos ON fd_produtos.id = {tabela_nome}.produto_id WHERE produto_id = '.$produto->id);
                $classe = $parametros->categoria;
                if ($galeria)
                {
                    $galeria_foto = new Model_GaleriaFoto();
                    $galeria_fotos = $galeria_foto->select('SELECT * FROM {tabela_nome} WHERE galeria_id = '.$galeria->id, TRUE);
                }
                else
                {
                    header('HTTP/1.1 404 Not Found');
                    $erro_404 = new Controller_Erro;
                    $erro_404->index(404);
                    exit;
                }
            }
            else
            {
                header('HTTP/1.1 404 Not Found');
                $erro_404 = new Controller_Erro();
                $erro_404->index(404);
                exit;
            }
        }
        else
        {
            header('HTTP/1.1 404 Not Found');
            $erro_404 = new Controller_Erro();
            $erro_404->index(404);
            exit;
        }

        $view = new View('galeria-foto.php');
		$this->view_variaveis_obrigatorias($view);

        $view->adicionar('classe',$classe);
        $view->adicionar('galeria',$galeria);
        $view->adicionar('fotos',$galeria_fotos);

		$view->adicionar('body_class', 'galeria-foto');
		$view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Galeria de Fotos - Profissional 2011 - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */

    /**
     * Passa as informações para a montagem e envio do email a partir do formulário "Mais Informações"
     * @param $parametros
     * @return void
     */
    public function mais_info($parametros)
    {
        $info_nome = isset($parametros->info_nome) ? trim($parametros->info_nome) : NULL;
        $info_telefone = isset($parametros->info_telefone) ? trim($parametros->info_telefone) : NULL;
        $info_email = isset($parametros->info_email) ? trim($parametros->info_email) : NULL;
        $info_cidade = isset($parametros->info_cidade) ? trim($parametros->info_cidade) : NULL;
        $info_mensagem = isset($parametros->info_mensagem) ? trim($parametros->info_mensagem) : NULL;

        if ( ! empty($info_nome) AND ! empty($info_email) AND ! empty($info_mensagem) AND ! empty($info_telefone) AND ! empty($info_cidade))
        {
            $mail = new Controller_Email;

            if ($mail->profissional($parametros))
            {
                echo json_encode(array('tipo'=>'sucesso', 'mensagem'=>'Sua mensagem foi enviada com sucesso!'));
            }
            else
            {
                echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Ocorreu um erro ao enviar sua mensagem.'));
            }
        }
        else
        {
            echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Ocorreu um erro ao receber suas informações.'));
        }
    }

} // end class