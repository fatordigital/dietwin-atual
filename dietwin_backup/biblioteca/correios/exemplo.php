<?php
require_once 'com/imasters/php/ect/ECT.php';

$ect = new ECT();
$prdt = $ect->prdt();
$prdt->setNVlAltura( 2 );
$prdt->setNVlComprimento( 16 );
$prdt->setNVlLargura( 16 );
$prdt->setNCdFormato( ECTFormatos::FORMATO_CAIXA_PACOTE );
$prdt->setNCdServico( implode( ',' , array( ECTServicos::PAC , ECTServicos::SEDEX ) ) );
$prdt->setSCepOrigem( '90510002' );
$prdt->setSCepDestino( '90010460' );
$prdt->setNVlPeso( '0,300' );

foreach ( $prdt->call() as $servico ) {
	printf( "O preço do frete do correios é R$ %.02f\n" , $servico->Valor );
}