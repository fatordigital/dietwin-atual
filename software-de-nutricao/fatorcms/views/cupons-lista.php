<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php' ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

		<!-- Page Head -->
		<h2>Lista de Cupons cadastrados</h2>
		<p id="page-intro">Abaixo estão listados todos os cupons que podem ser utilizados no momento da compra pelos clientes.</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<form action="<?php echo SITE_URL ?>/fatorcms/cupons/listar" method="get" id="cupons_buscar_form">
			<fieldset>
				<p>
					<label for="buscar">Digite uma ou mais palavras-chave para buscar por um cupom cadastrado:</label>
					<input class="text-input medium-input" type="text" id="buscar" name="buscar" maxlength="100" value="<?php echo isset($buscar) ? $buscar : '' ?>" />
					<input class="button" type="submit" value="Buscar" />
				</p>
			</fieldset>
			<div class="clear"></div><!-- End .clear -->
		</form>

		<?php if (isset($buscar) AND ! is_null($buscar)) { ?>
			<p class="buscar-resultado">
				Foram encontrados <strong><?php echo $paginacao->linhas_total ?></strong> resultado(s) para a busca por "<strong><?php echo $buscar ?></strong>".
				<a title="Limpar a busca" href="<?php echo SITE_URL?>/fatorcms/cupons/listar"><img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier-cross.png" alt="Limpar busca" /> Clique aqui para limpar a busca</a>
			</p>
		<?php } ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Cupons</h3>

				<input class="cupom button botao-cadastrar" type="button" value="Cadastrar um novo cupom" />

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<table>

					<thead>
						<tr>
							<?php echo Funcoes::montar_th_ordenacao_listagem('cupons', 'listar', $paginacao, $buscar, 'Código', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('cupons', 'listar', $paginacao, $buscar, 'Produto', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('cupons', 'listar', $paginacao, $buscar, 'Desconto', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('cupons', 'listar', $paginacao, $buscar, 'Datas', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('cupons', 'listar', $paginacao, $buscar, 'Ativo?', $ordenar_por, $ordem) ?>
                            <th>Ações</th>
						</tr>
					</thead>

					<tfoot>
						<tr>
							<td colspan="6">

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_quantidades() ?>

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_links() ?>

								<div class="clear"></div>
							</td>
						</tr>
					</tfoot>

					<tbody>
						<?php
                        if($cupons AND count($cupons) > 0)
						{
							foreach ($cupons as $cupom)
							{
							?>
								<tr>
									<td class="<?php echo $ordenar_por=='codigo' ? 'current' : '' ?>">
										<a href="<?php echo SITE_URL ?>/fatorcms/cupons/editar/<?php echo $cupom->id ?>" title="Editar o cupom de código &quot;<?php echo $cupom->codigo ?>&quot;">
											<?php echo $cupom->codigo ?>
										</a>
									</td>
									<td class="<?php echo $ordenar_por=='produto' ? 'current' : '' ?>"><?php echo $cupom->produto_nome ?></td>
									<td class="<?php echo $ordenar_por=='desconto' ? 'current' : '' ?>">R$ <?php echo number_format($cupom->valor, 2, ',', '.') ?></td>
									<td class="<?php echo $ordenar_por=='datas' ? 'current' : '' ?>">
										<?php echo ( ! empty($cupom->validade_inicio) ? date('d/m/Y', strtotime($cupom->validade_inicio)) : '<em>indefinida</em>' ) ?> à
										<?php echo ( ! empty($cupom->validade_fim) ? date('d/m/Y', strtotime($cupom->validade_fim)) : '<em>indefinida</em>' ) ?>
									</td>
									<td class="<?php echo $ordenar_por=='ativo' ? 'current' : '' ?>">
										<span class="<?php echo $cupom->ativo ? 'sim': 'nao' ?>"><?php echo $cupom->ativo ? 'Sim': 'Não' ?></span>
									</td>
									<td>
										<!-- Icons -->
										<a href="<?php echo SITE_URL ?>/fatorcms/cupons/editar/<?php echo $cupom->id ?>" title="Editar o cupom &quot;<?php echo $cupom->codigo ?>&quot;">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" />
										</a>
										<a href="<?php echo SITE_URL ?>/fatorcms/cupons/excluir/<?php echo $cupom->id ?>" title="Excluir o cupom &quot;<?php echo $cupom->codigo ?>&quot;" class="item-confirmar">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/cross.png" alt="Excluir" />
										</a>
									</td>
								</tr>
							<?php
							}
						}
						else
						{
							echo '<tr><td colspan="6">'.((isset($buscar) AND ! is_null($buscar)) ? 'Nenhum cupom encontrado para esta busca.' : 'Nenhum cupom cadastrado até o momento.' ).'</td></tr>';
						}
						?>
					</tbody>

				</table>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>