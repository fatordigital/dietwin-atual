<?php include 'includes/cabecalho.php'; ?>

<body class="novidades">

<?php include 'includes/topo.php'; ?>

<div class="titulo-principal">
    <p><span>Acompanhe as</span><br />novidades</p>
</div>
<div class="titulo-resumo">
    <p>A equipe do dietWin disponibiliza sempre as últimas novidades de seus softwares para que você possa estar atualizado sobre toda a tecnologia oferecida!</p>
</div>
</div>
</div>

<div class="conteudo">
    <div class="container" id="tamanho-fonte">

        <?php include 'includes/toolbar.php' ?>

        <h1>Novidades</h1>
        <?php
        if ($novidades AND count($novidades)>0)
        {
        ?>
        <ul class="lista">
            <?php
            $i=0;
            foreach ($novidades as $novidade)
            {
            ?>
            <li<?php if ($i == 1) { echo ' class="sem-margem"'; } ?>>
                <?php
                if (isset($novidade->imagem))
                {
                ?>
                <div class="imagem">
                    <a href="<?php echo SITE_URL ?>/novidade/<?php echo $novidade->titulo_seo ?>" title="<?php echo $novidade->titulo ?>"><img src="<?php echo SITE_BASE ?>/arquivos/novidades/thumb1/<?php echo $novidade->imagem ?>" alt="<?php echo $novidade->titulo ?>" /></a>
                </div>
                <?php
                }
                ?>
                <p class="data"><?php echo date('d/m/Y',strtotime($novidade->data)) ?></p>
                <h3><?php echo $novidade->titulo ?></h3>
                <p><?php echo $novidade->resumo ?></p>
                <a href="<?php echo SITE_URL ?>/novidade/<?php echo $novidade->titulo_seo ?>" class="link" title="">Ler a novidade completa</a>
                <div class="clear"></div>
            </li>
            <?php
                if ($i == 0)
                {
                    $i++;
                }
                else
                {
                    $i=0;
                }
            }
            ?>

        </ul>
        <?php
            echo '<div class="clear"></div>';
            echo '<div class="paginacao">';
			$paginacao->site_exibir_links();
			echo '</div>';
        }
        else
        {
            echo '<p>Não há novidades cadastradas.</p>';
        }
        ?>
        <div class="clear"></div>
    </div>
    <div class="inferior"></div>
</div>

<?php include 'includes/rodape.php'; ?>