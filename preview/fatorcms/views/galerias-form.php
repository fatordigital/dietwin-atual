<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php'; ?>

<script type="text/javascript" src="<?php echo SITE_BASE ?>/fatorcms/views/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_BASE ?>/fatorcms/views/js/uploadify/swfobject.js"></script>
<link rel="stylesheet" href="<?php echo SITE_BASE ?>/fatorcms/views/js/uploadify/uploadify.css" type="text/css" media="screen" />
<script type="text/javascript">
$(document).ready(function()
{
	$('#uploader_fotos').uploadify(
	{
		'uploader'       : '<?php echo SITE_BASE; ?>/fatorcms/views/js/uploadify/uploadify.swf?preventswfcaching=<?php echo time(); ?>',
		'expressInstall' : '<?php echo SITE_BASE; ?>/fatorcms/views/js/uploadify/expressInstall.swf',
		'script'         : '<?php echo SITE_BASE ?>/fatorcms/views/js/uploadify/uploadify.php',
		'cancelImg'      : '<?php echo SITE_BASE; ?>/fatorcms/views/js/uploadify/cancel.png',
		'buttonImg'      : '<?php echo SITE_BASE; ?>/fatorcms/views/js/uploadify/selecione-imagens.png',
		'fileDataName'   : 'galeria_[]',
		'scriptData'     : { 'galeria_id': '<?php echo ( ! is_null($galeria) ? $galeria->id : '') ?>' },
		'multi'          : true,
		'auto'           : false,
		'width'	         : 163,
        'height'         : 27,
		'fileExt'        : '*.jpg;*.jpeg;',
		'fileDesc'       : 'Imagens (.JPG)',
		'queueID'        : 'fila_upload_imagens',
		'sizeLimit'      : 1048576,
		'removeCompleted': true,
		'onSelectOnce'   : function(event, data) {
			$('#status_message').text(data.filesSelected + ' arquivos foram adicionados à fila de envio.');
		},
		'onAllComplete'  : function(event, data) {
            window.location.href = '<?php echo SITE_URL.'/fatorcms/galerias/editar/'.( ! is_null($galeria) ? $galeria->id : '').'/aba/2' ?>';
		},
		'onError'        : function(event, data, errorObj) {
            console.log(event);
			console.log(data);
            console.log(errorObj);
            alert('');
		}
	});

});
</script>

	<div id="main-content"> <!-- Main Content Section with everything -->

        <!-- Page Head -->
		<?php if (is_null($galeria) OR is_null($galeria->id)) { ?>
			<h2>Cadastro de uma Galeria</h2>
			<p id="page-intro">Utilize o formulário abaixo para incluir uma galeria no site.</p>
		<?php } else { ?>
			<h2>Edição de uma Galeria</h2>
			<p id="page-intro">No formulário abaixo você pode alterar os dados de uma galeria.</p>
		<?php } ?>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<?php if (is_null($galeria) OR is_null($galeria->id)) { ?>
					<h3>Dados de uma galeria</h3>
				<?php } else { ?>
					<h3>Informações da galeria <?php echo $galeria ? $galeria->titulo : '' ?></h3>
				<?php } ?>
				
				<ul class="content-box-tabs">
					<li><a href="#tab1" <?php echo ( ! isset($aba) OR $aba == 1 ) ? 'class="default-tab"' : '' ?>>Galeria</a></li>
					<?php if ( ! is_null($galeria) AND ! empty($galeria->id)) { ?>
						<li><a href="#tab2" <?php echo (isset($aba) AND $aba == 2 ) ? 'class="default-tab"' : '' ?>>Visualizar</a></li>
						<li><a href="#tab3" <?php echo (isset($aba) AND $aba == 3 ) ? 'class="default-tab"' : '' ?>>Enviar fotos</a></li>
					<?php } ?>
				</ul>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<div class="tab-content <?php echo ( ! isset($aba) OR $aba == 1 ) ? 'default-tab' : '' ?>" id="tab1">
			
					<form action="<?php echo SITE_URL ?>/fatorcms/galerias/<?php echo (is_null($galeria) OR is_null($galeria->id)) ? 'cadastrar' : 'atualizar' ?>" method="post" id="galeria_form" enctype="multipart/form-data">

						<fieldset>

							<?php if ( ! is_null($galeria)) { ?>
								<input type="hidden" name="id" value="<?php echo $galeria->id ?>" />
							<?php } ?>

							<p>
								<label for="produto_id">Produto</label>
								<select name="produto_id" id="produto_id" class="text-input medium-input required">
									<?php
									if ($produtos AND count($produtos) > 0)
									{
										foreach ($produtos as $produto)
										{
											echo '<option value="'.$produto->id.'" '.(($galeria AND $galeria->produto_id == $produto->id) ? 'selected="selected"' : '').'>'.$produto->nome.'</option>';
										}
									}
									else
									{
										echo '<option value="">Nenhum produto disponível</option>';
									}
									?>
								</select>
								<br /><small>Só é possível cadastrar uma galeria de fotos por produto.</small>
							</p>

							<p>
								<label for="titulo">Título</label>
								<input class="text-input large-input required" type="text" id="titulo" name="titulo" maxlength="100" value="<?php echo $galeria ? $galeria->titulo : '' ?>" />
							</p>

							<p>
							   <label for="data">Data</label>
							   <input class="text-input small-input required" type="text" id="data" name="data" maxlength="10" value="<?php echo ($galeria AND ! empty($galeria->data)) ? date('d/m/Y', strtotime($galeria->data)) : '' ?>" />
							</p>
							
							<p>
								<label for="descricao">Descrição</label>
								<textarea class="text-input" cols="79" rows="10" id="descricao" name="descricao" ><?php echo $galeria ? $galeria->descricao : '' ?></textarea>
							</p>
							
							<p>
								<input class="button" type="submit" value="<?php echo (is_null($galeria) OR is_null($galeria->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
							</p>

						</fieldset>

						<div class="clear"></div><!-- End .clear -->

					</form>
					
				</div>
				
				<?php if ( ! is_null($galeria) AND ! empty($galeria->id)) { ?>

					<div class="tab-content <?php echo ( isset($aba) AND $aba == 2 ) ? 'default-tab' : '' ?>" id="tab2">
	                    <?php
	                    if ($fotos AND count($fotos) > 0)
	                    {
	                        echo '<ul id="galeria">';
	                        foreach ($fotos as $foto)
	                        {
		                    ?>
	                            <li>
	                                <form method="post" action="">
	                                    <input type="hidden" name="id" value="<?php echo $foto ? $foto->id : '' ?>" />

	                                    <a rel="modal" href="<?php echo SITE_BASE ?>/arquivos/galerias/<?php echo $foto->galeria_id.'/'.$foto->arquivo ?>">
	                                        <img src="<?php echo SITE_BASE ?>/arquivos/galerias/<?php echo $foto->galeria_id.'/thumb1/'.$foto->arquivo ?>" alt="<?php echo $foto ? $foto->titulo : '' ?>" />
	                                    </a>

	                                    <label for="titulo_<?php echo $foto ? $foto->id : '' ?>">Título da Foto</label>
	                                    <input class="text-input large-input" type="text" name="titulo" id="titulo_<?php echo $foto ? $foto->id : '' ?>" maxlength="150" value="<?php echo $foto ? $foto->titulo : '' ?>" />

	                                    <p>
	                                        <a class="atualizar_foto">Atualizar</a><span>&nbsp;|&nbsp;</span><a class="apagar_foto">Apagar</a>
	                                    </p>
	                                </form>
	                            </li>
	                    <?php
	                        }
	                        echo '</ul>';
	                    }
	                    else
	                    {
	                        echo '<p>Não há fotos cadastradas para esta galeria.</p>';
	                    }?>
	                    <div class="clear"></div>
					</div>

					<div class="tab-content <?php echo (isset($aba) AND $aba == 3 ) ? 'default-tab' : '' ?>" id="tab3">

						<p>
							<div id="status_message">Selecione as fotos para envio:</div>
						</p>

	                    <input id="uploader_fotos" type="file" name="fotos" />
						<p>
							<div id="fila_upload_imagens"></div>
							<br /><small>Envie somente imagens <strong>JPG</strong> e com no máximo <strong>1mb</strong> de tamanho.</small>
						</p>

						<p>
							<a href="" onclick="$('#uploader_fotos').uploadifyUpload(); return false;"><img src="<?php echo SITE_BASE; ?>/fatorcms/views/js/uploadify/fazer-upload.png" width="190" height="27" style="border:0" /></a>
							<a href="" onclick="$('#uploader_fotos').uploadifyClearQueue(); return false;"><img src="<?php echo SITE_BASE; ?>/fatorcms/views/js/uploadify/limpar-lista.png" width="184" height="27" style="border:0" /></a>
						</p>

					</div>
				
				<?php } // is_null galeria ?>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>