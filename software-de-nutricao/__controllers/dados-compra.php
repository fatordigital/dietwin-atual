<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_DadosCompra extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        $licencas = (isset($parametros->licencas) AND strlen($parametros->licencas)>0)?$parametros->licencas : NULL;
        if ((!isset($licencas) OR !is_numeric($licencas) OR !(int)$licencas < 0))
        {
            if (!isset($_SESSION['licencas']))
            {
                header('HTTP/1.1 404 Not Found');
                $erro_404 = new Controller_Erro;
                $erro_404->index(404);
                exit;
            }
            else
            {
                $licencas = $_SESSION['licencas'];
            }
        }
        if ((!isset($parametros->categoria) OR strlen($parametros->categoria)<=0))
        {
            if (!isset($_SESSION['produto']))
            {
                header('HTTP/1.1 404 Not Found');
                $erro_404 = new Controller_Erro;
                $erro_404->index(404);
                exit;
            }
        }
        $produto = new Model_Produto();
        $busca = $parametros->categoria;
        if($busca == 'profissional'){
            $busca = 'profissional-plus';
        }
        $produto = $produto->select("SELECT * FROM {tabela_nome} WHERE nome_seo = '".(isset($parametros->categoria) ? 'dietwin-'.Funcoes::mysqli_escape($busca) : $_SESSION['produto']->nome_seo)."'");
        if (!isset($produto) OR !$produto)
        {
            header('HTTP/1.1 404 Not Found');
            $erro_404 = new Controller_Erro;
            $erro_404->index(404);
            exit;
        }
        $cupom = NULL;
        if (isset($_SESSION['cupom_id']))
        {
            $cupom = new Model_Cupom();
            $cupom = $cupom->select("SELECT * FROM {tabela_nome}
                        WHERE id='".$_SESSION['cupom_id']."' AND produto_id=".$produto->id."
                        AND ativo=1 AND validade_inicio <= CURRENT_DATE AND validade_fim >= CURRENT_DATE");
            if (!$cupom)
            {
                $cupom = NULL;
                unset($_SESSION['cupom_id']);
            }
        }
        $cliente = NULL;
        if (isset($_SESSION['cliente_id']) AND !is_null($_SESSION['cliente_id']))
        {
            $cliente = new Model_Cliente($_SESSION['cliente_id']);
        }


        $view = new View('dados-compra.php');
		$this->view_variaveis_obrigatorias($view);

        $view->adicionar('cliente',$cliente);

        $view->adicionar('classe',$parametros->categoria);
        $_SESSION['cupom'] = $cupom;
        $_SESSION['produto'] = $produto;
        $_SESSION['licencas'] = isset($licencas)?$licencas:$_SESSION['licencas'];

		$view->adicionar('body_class', 'dados-compra');
		$view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Dados de sua compra - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */


} // end class