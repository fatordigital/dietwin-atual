<?php include 'includes/cabecalho.php'; ?>

<body class="videos videos-<?php echo $classe ?>">

<?php include 'includes/topo.php'; ?>
	
    <div class="titulo-principal">
        <p><span>Assista aos nossos</span><br />vídeos</p>       
    </div>
    <div class="titulo-resumo">
        <p>A equipe do dietWin reuniu os melhores vídeos de treinamento para que você possa conhecer cada funcionalidade de nossos softwares da maneira mais intuitiva possível!</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container">
    	
        <?php include 'includes/toolbar.php' ?>

        <div id="tipo_suporte" class="<?php echo $classe ?>">
            <h1>Exibindo categorias: <span><?php echo $categoria ?></span></h1>

            <?php
            if ($videos AND count($videos) > 0)
            {
            ?>
            <ul class="lista" id="tamanho-fonte">
                <?php
                $i = 1;
                foreach ($videos as $video)
                {
                ?>
                <li>
                    <div class="play"><a href="<?php echo SITE_URL ?>/suporte/<?php echo (isset($video->nome_seo) ? substr($video->nome_seo,8) : 'topicos-diversos') ?>/video/<?php echo $video->titulo_seo ?>" title="<?php echo $video->titulo ?>"><img src="<?php echo SITE_BASE ?>/views/imagens/multimidia-treinamento-play.png" alt="<?php echo $video->titulo ?>" /></a></div>
                    <div class="imagem">
                        <?php
                            $regex = '/http\:\/\/www\.youtube\.com\/watch\?v=(\w{11})/';
                            preg_match($regex, $video->link,$matches);

                        ?>
                        <a href="<?php echo SITE_URL ?>/suporte/<?php echo (isset($video->nome_seo) ? substr($video->nome_seo,8) : 'topicos-diversos') ?>/video/<?php echo $video->titulo_seo ?>" title="<?php echo $video->titulo ?>"><img src="<?php echo SITE_BASE ?>/biblioteca/timthumb.php?src=http://i3.ytimg.com/vi/<?php echo substr($video->link, strrpos($video->link,'v=')+2,11) ?>/0.jpg&w=229&h=133" alt="<?php echo $video->titulo ?>" /></a>
                    </div>
                    <p class="data"><?php echo substr($video->data,8,2).'/'.substr($video->data,5,2).'/'.substr($video->data,0,4) ?></p>
                    <h3><?php echo $video->titulo ?></h3>
                    <p><?php echo Funcoes::cortar_texto($video->resumo, 300) ?></p>
                    <a href="<?php echo SITE_URL ?>/suporte/<?php echo (isset($video->nome_seo) ? substr($video->nome_seo,8) : 'topicos-diversos') ?>/video/<?php echo $video->titulo_seo ?>" class="link" title="Assita ao vídeo <?php echo $video->titulo ?>">Assista ao vídeo</a>
                    <div class="clear"></div>
                </li>
                <?php
                    $i++;
                }
                ?>
                <?php /*
                <li>
                    <div class="play"><a href="<?php echo SITE_URL ?>/video" title=""><img src="<?php echo SITE_BASE ?>/views/imagens/multimidia-treinamento-play.png" alt="" /></a></div>
                    <div class="imagem">
                        <a href="<?php echo SITE_URL ?>/video" title=""><img src="<?php echo SITE_BASE ?>/views/imagens/videos.jpg" alt="" /></a>
                    </div>
                    <p class="data">20/03/</p>
                    <h3>Como criar uma dieta no Personal ?</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla pellentesque hendrerit. Suspendisse enim nisl, ullamcorper in porta eget, gravida a neque. Phasellus ut augue augue, vitae tempus nunc.</p>
                    <a href="<?php echo SITE_URL ?>/video" class="link" title="">Assista ao vídeo</a>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="play"><a href="<?php echo SITE_URL ?>/video" title=""><img src="<?php echo SITE_BASE ?>/views/imagens/multimidia-treinamento-play.png" alt="" /></a></div>
                    <div class="imagem">
                        <a href="<?php echo SITE_URL ?>/video" title=""><img src="<?php echo SITE_BASE ?>/views/imagens/videos.jpg" alt="" /></a>
                    </div>
                    <p class="data">20/03/</p>
                    <h3>Como criar uma dieta no Personal ?</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla pellentesque hendrerit. Suspendisse enim nisl, ullamcorper in porta eget, gravida a neque. Phasellus ut augue augue, vitae tempus nunc.</p>
                    <a href="<?php echo SITE_URL ?>/video" class="link" title="">Assista ao vídeo</a>
                    <div class="clear"></div>
                </li>
                <li class="sem-borda">
                    <div class="play"><a href="<?php echo SITE_URL ?>/video" title=""><img src="<?php echo SITE_BASE ?>/views/imagens/multimidia-treinamento-play.png" alt="" /></a></div>
                    <div class="imagem">
                        <a href="<?php echo SITE_URL ?>/video" title=""><img src="<?php echo SITE_BASE ?>/views/imagens/videos.jpg" alt="" /></a>
                    </div>
                    <p class="data">20/03/</p>
                    <h3>Como criar uma dieta no Personal ?</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla pellentesque hendrerit. Suspendisse enim nisl, ullamcorper in porta eget, gravida a neque. Phasellus ut augue augue, vitae tempus nunc.</p>
                    <a href="<?php echo SITE_URL ?>/video" class="link" title="">Assista ao vídeo</a>
                    <div class="clear"></div>
                </li>
                */ ?>
            </ul>
            <?php
            }
            else
            {
                echo '<p>Não há vídeos cadastrados para essa categoria.</p>';
            }
            ?>
        </div>

        <div class="sidebar">
            <div class="filtro">
                <h2>Filtre os tópicos</h2>
                <p>Clique para marcar apenas as categorias de tópicos que você deseja visualizar.</p>
                <a href="<?php echo SITE_URL.'/suporte/personal/videos'; ?>" class="filtro-personal<?php if ($categoria == 'Personal') { echo ' ativo'; } ?>" title="Filtrar por Personal "><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-personal<?php if ($categoria == 'Personal') { echo '-ativo'; } ?>.png" alt="Filtrar por Personal " /></a>
                <a href="<?php echo SITE_URL.'/suporte/profissional/videos'; ?>" class="filtro-profissional<?php if ($categoria == 'Profissional') { echo ' ativo'; } ?>" title="Filtrar por Profissional "><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-profissional<?php if ($categoria == 'Profissional') { echo '-ativo'; } ?>.png" alt="Filtrar por Profissional " /></a>
                <a href="<?php echo SITE_URL.'/suporte/rotulo-de-alimentos/videos'; ?>" class="filtro-rotulo-alimentos<?php if ($categoria == 'Rótulo de Alimentos') { echo ' ativo'; } ?>" title="Filtrar por Rótulo de Alimentos"><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-rotulo<?php if ($categoria == 'Rótulo de Alimentos') { echo '-ativo'; } ?>.png" alt="Filtrar por Rótulo de Alimentos" /></a>
                <a href="<?php echo SITE_URL.'/suporte/topicos-diversos/videos'; ?>" class="filtro-topicos-diversos<?php if ($categoria == 'Tópicos Diversos') { echo ' ativo'; } ?>" title="Filtrar por Tópicos diversos"><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-topicos<?php if ($categoria == 'Tópicos Diversos') { echo '-ativo'; } ?>.png" alt="Filtrar por Tópicos diversos" /></a>
            </div>
            <?php
            if ($videos AND count($videos) > 0)
            {
                ?>
                <div class="filtro">
                    <h2>Busque por palavra-chave</h2>
                    <form method="post" action="<?php echo ($categoria != 'Todas') ? SITE_URL.'/suporte/'.$classe : SITE_URL ?>/videos" id="buscar" >
                        <input type="text" name="buscar" id="busca-campo" maxlength="255" value="" />
                        <button type="submit">Buscar</button>
                    </form>
                    <div class="clear"></div>
                </div>
                <?php
            }
            ?>
        </div>
        
        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>  
</div>

<?php include 'includes/rodape.php'; ?>