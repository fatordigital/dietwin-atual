<?php include 'includes/cabecalho.php'; ?>

<body class="contato">

<?php include 'includes/topo.php'; ?>
    <div class="titulo-principal">
        <p>Orçamento personalizado</p>
    </div>
    <div class="titulo-resumo">
        <p><Br>Preencha os campos abaixo para receber um orçamento personalizado referente ao produto que deseja!</p>
    </div>
    </div>

</div>

<div class="conteudo">
	<div class="container">
    	
        <div class="formulario">
            <form method="post" action="" id="form-orcamento" >

                <label><b>Nome:</b></label>
                <input type="text" class="obrigatorio" name="nome" id="nome" maxlength="100" value="" />
                
                <label><b>CNPJ/CPF</b></label>
                <input type="text" class="obrigatorio" name="cnpj_cpf" id="cnpj_cpf" maxlength="11" value="" />
                
                <label><b>E-mail:</b></label>
                <input type="text" class="obrigatorio" name="email" id="email" maxlength="255" value="" />
                
                <label><b>Telefone:</b></label>
                <input type="text" class="obrigatorio cidade" name="telefone" id="telefone" maxlength="14" value="" />
                
                <label><b>Celular:</b></label>
                <input type="text" class="fone" name="celular" id="celular" maxlength="100" value="" />
                
                <label><b>Cidade:</b></label>
                <input type="text" class="obrigatorio" name="cidade" id="cidade" maxlength="100" value="" />
                
                <label><b>Endereço:</b></label>
                <input type="text" class="obrigatorio" name="endereco" id="endereco" maxlength="255" value="" />
                
                <label><b>Número:</b></label>
                <input type="text" class="obrigatorio fone" name="numero" id="numero" maxlength="255" value="" />

                <label><b>Complemento:</b></label>
                <input type="text" class="cidade" name="complemento" id="complemento" maxlength="5" value="" />

                <label><b>CEP:</b></label>
                <input type="text" class="obrigatorio" name="cep" id="complemento" maxlength="255" value="" />
                
                
                <div class="clear"></div>
                
                <label><b>Estado:</b></label>
                <select class="obrigatorio" name="estado" id="estado">
                    <option value="Acre">Acre</option>
                    <option value="Alagoas">Alagoas</option>
                    <option value="Amapá">Amapá</option>
                    <option value="Amazonas">Amazonas</option>
                    <option value="Bahia">Bahia</option>
                    <option value="Ceará">Ceará</option>
                    <option value="Distrito Federal">Distrito Federal</option>
                    <option value="Espírito Santo">Espírito Santo</option>
                    <option value="Goiás">Goiás</option>
                    <option value="Maranhão">Maranhão</option>
                    <option value="Mato Grosso">Mato Grosso</option>
                    <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
                    <option value="Minas Gerais">Minas Gerais</option>
                    <option value="Pará">Pará</option>
                    <option value="Paraíba">Paraíba</option>
                    <option value="Paraná">Paraná</option>
                    <option value="Pernambuco">Pernambuco</option>
                    <option value="Piauí">Piauí</option>
                    <option value="Rio de Janeiro">Rio de Janeiro</option>
                    <option value="Rio Grande do Norte">Rio Grande do Norte</option>
                    <option value="Rio Grande do Sul">Rio Grande do Sul</option>
                    <option value="Rondônia">Rondônia</option>
                    <option value="Roraima">Roraima</option>
                    <option value="Santa Catarina">Santa Catarina</option>
                    <option value="São Paulo">São Paulo</option>
                    <option value="Sergipe">Sergipe</option>
                    <option value="Tocantins">Tocantins</option>
                </select>
                
                <p><b>Qual software você deseja?</b></p>
                <div class="usuario sem-float">
                    <input type="checkbox" name="rotulo_de_alimento" id="rotulo_de_alimento" value="Rótulo de Alimentos" /><span>Rótulo de Alimentos</span><br><br>
                    <input type="checkbox" name="personal" id="personal" value="Personal" /><span>Personal</span><br><br>
                    <input type="checkbox" name="profissional" id="profissional" value="Profissional" /><span>Profissional</span><br><br>
                    <input type="checkbox" name="analise_nutricional" id="analise_nutricional" value="Análise Nutricional" /><span>Análise Nutricional</span>
                    <div class="clear"></div>
                </div>
                
                <label><b>Alguma forma de pagamento especial?</b></label>
                <textarea type="text" class="obrigatorio" name="forma_pagamento" id="forma_pagamento" style="overflow:auto"></textarea>
                
                <label><b>Informações Adicionais:</b></label>
                <textarea type="text" class="" name="info_adicional" id="info_adicional" style="overflow:auto"></textarea>
                <div style="color:red"><?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?></div>
                <button type="submit">Enviar</button>
                
                <div class="aviso-envio"></div>

                <div class="clear"></div>

            </form>

            <div id="form_notification"></div>

        </div>
        
        <div class="formas">
            <p class="subtitulo">Contato por telefone</p>
            <p class="telefone">(51) 3337.7908</p>
            <div class="clear"></div>
            <div class="separador"></div>
            <p class="subtitulo">Suporte</p>
            <p class="email">suporte@dietwin.com.br</p>
            <div class="clear"></div>
            <p class="info">Se você encontrar alguma dificuldade com algum produto da linha dietWin, utilize nosso suporte.</p>
            <div class="clear"></div>
        </div>
        
        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>  
</div>

<?php include 'includes/rodape.php'; ?>