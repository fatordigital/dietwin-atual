<?php include 'includes/cabecalho.php'; ?>

<body class="comprar comprar-plano">

<?php include 'includes/topo.php'; ?>
	
    <div class="titulo-principal">
        <p><span>comprar </span><br />Plano de Assistência</p>
    </div>
    <div class="titulo-resumo">
        <p>	<i>Descontos, upgrades gratuitos, suporte, treinamento e outras vantagens diferenciadas.</i></p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container">
    	
        <?php include 'includes/toolbar.php' ?>
        
        <div class="produto" id="tamanho-fonte">
            <img src="<?php echo SITE_BASE ?>/views/imagens/comprar-plano-de-assistencia.png" alt="Plano de Assistência " />
            <div class="info">
                <h1>Plano de Assistência dietWin - 1 ano</h1>
                <p>O plano de assistência do dietWin é um serviço diferenciado prestado pelo dietWin para os clientes que adquirirem o serviço. Entre os diferenciais estão: Descontos de até 60% na compra de softwares, atualizações sem custos, upgrades gratuitos, suporte diferenciado, treinamento online e muito mais.</p>
            </div>


            <div class="opcoes">

                <!-- inicio do widget do Webchat Locaweb -->
                <div id="webchat_widget" style="float:right;"><!-- Coloque esse div onde você quer que o widget apareça. --></div>
                <script type="text/javascript">
                    (function() {
                        var wc = document.createElement('script'); wc.type = "text/javascript";
                        wc.src = 'https://dietwin.webchatlw.com.br/widget.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(wc, s);
                    })();
                </script>
                <div style="clear: both;"></div>


                <a href="#" class="mais-info link-mais-info" title="Solicite maiores informações sobre a ferramenta">Solicite maiores informações sobre a ferramenta</a>

                <!-- Conteúdo do colorbox do botão "Mais Informações" -->
                <div style="display:none">
                    <div class="colorbox-mais-info">
                        <div class="topo"></div>
                        <div class="box">
                            <div class="titulo-colorbox">Mais Informações</div>
                            <div class="conteudo-colorbox">

                                <p>Preencha o formulário abaixo para solicitar mais informações sobre este software.</p>
                                <form method="post" action="<?php echo SITE_URL ?>/profissional/mais_info" id="form-mais-info" >

                                    <label><b>Nome:</b></label>
                                    <input type="text" class="obrigatorio" name="info_nome" id="info_nome" maxlength="100" />

                                    <label><b>E-mail:</b></label>
                                    <input type="text" class="obrigatorio" name="info_email" id="info_email" maxlength="255" />

                                    <label><b>Telefone:</b></label>
                                    <input type="text" class="obrigatorio" name="info_telefone" id="info_telefone" maxlength="14" />

                                    <label><b>Cidade:</b></label>
                                    <input type="text" class="obrigatorio" name="info_cidade" id="info_cidade" maxlength="100" />

                                    <label><b>Mensagem:</b></label>
                                    <textarea type="text" class="obrigatorio" name="info_mensagem" id="info_mensagem" style="resize: vertical; overflow:auto"></textarea>

                                    <button type="submit">Enviar</button>
                                    <div class="clear"></div>
                                </form>

                                <div id="form_notification"></div>

                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="inferior"></div>
                    </div>
                </div>
                <!-- Fim do colorbox do botão "Mais Informações" -->
            </div>
            <div class="clear"></div>
        </div>       
        
        <div class="instalar">
        	<h2>E qual o custo do Plano de Assistência?</h2>

			<p>O plano de assistência dietWin custa R$ 99 por software e mais R$ 49 por licença adicional. Ou seja, se você adquiriu o dietWin e tem uma licença adicional para instalação em mais um computador, o plano custará R$ 148,00. <span>O plano tem validade de 1 ano a partir da compensação do pagamento</span>.</p>
            
            <div class="grafico">
                <p class="textLicense">
                    <span id="contador">1</span>
                    <span id="computers">computador</span>
                </p>
                <div id="valueLicense" class="valueLicense">(incluso)</div>
                <div style="text-align:center"><img src="<?php echo SITE_BASE ?>/views/imagens/slider-divisorias.gif" /></div>
                <div class="sliderAviso">
                    <span>Importante:</span>
                    <p>Se você possui mais de 10 adicionais, <a href="/comprar/orcamento.php">clique aqui</a> e solicite uma proposta personalizada.</p>
                    <p>O Plano de assistência não pode ser dividido com outra pessoa, somente para o mesmo CPF.</p>
                </div>
            </div>
            
            <div class="valor">
            	<p class="total" id="valor_total"><span class="moeda">R$ </span><span id="valorTotal"><?php echo number_format($licenca,0) ?></span></p>
                <p class="quantidade"><b>Plano para proteção de <span>1</span> computador</b></p>
                <div class="cupon">
                	<p>Eu tenho um cupom de desconto</p>
                    <form method="post" action="<?php echo SITE_URL.'/software/'.$classe.'/comprar' ?>" id="atualizar_cupon" >
                        <input type="text" name="cupon" id="cupon" maxlength="255" value="" />
                        <button type="submit">Buscar</button>
                    </form>
                    <div class="mensagem"></div>
           			<div class="clear"></div>
                </div>
            </div>            
            
            <div class="clear"></div>
		</div>
        
        <img src="<?php echo SITE_BASE ?>/views/imagens/pagseguro.jpg" class="pagseguro" alt="Compra segura com Pagseguro" />
        <form action="<?php echo SITE_URL.'/area-do-cliente/plano-de-assistencia/dados-compra' ?>" method="post">
            <input type="hidden" id="input_licencas" name="licencas" value="0" />
            <input type="image" src="<?php echo SITE_BASE ?>/views/imagens/prosseguir-compra.jpg" class="prosseguir" value="Prosseguir com a compra." />
        </form>

        
        <div class="formas-pagamento">
            <img src="<?php echo SITE_BASE ?>/views/imagens/formas-pagamento.jpg" alt="Todas as formas de pagamento" />
        </div>
            
        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>  
</div>

<div id="div-chat" >
    <a id="chat-abrir" href="#" onclick="wc_popup(); return false;"><img src="<?php echo SITE_BASE; ?>/views/imagens/chat-passivo.png" alt="chat" /></a>
    <a id="chat-fechar" href="#"><img src="<?php echo SITE_BASE; ?>/views/imagens/chat-fechar.png" alt="chat" /></a>
</div>

<?php include 'includes/rodape.php'; ?>