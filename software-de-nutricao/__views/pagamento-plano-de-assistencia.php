<?php include 'includes/cabecalho.php'; ?>

<body class="comprar comprar-plano dados pagamento">

<?php include 'includes/topo.php'; ?>

<div class="titulo-principal">
    <p><span>comprar </span><br />Plano de Assistência</p>
</div>
<div class="titulo-resumo">
    <p>	Descontos, upgrades gratuitos, suporte, treinamento e outras vantagens diferenciadas.</p>
</div>
</div>
</div>

<div class="conteudo">
	<div class="container">
    	<?php

        $produto_valor = $produto->valor;
        $licencas_valor = str_replace(',','',$licencas)*str_replace(',','',$produto->licenca_valor);
        $cupom_valor = (isset($cupom)? number_format($cupom->valor,2):'0,00');

        $licencas_valor = (float)str_replace(',','',$licencas_valor);
        $produto_valor = (float)str_replace(',','',$produto_valor);
        $cupom_valor = (float)str_replace(',','',$cupom_valor);
        $total = $licencas_valor+$produto_valor-$cupom_valor;

        ?>
        <h1>Como você deseja realizar o pagamento (<span>R$ <?php echo number_format($total,2,',','.'); ?></span>)?</h1>
        
        <div class="opcao boleto">
        	<a id="link_boleto" title="Boleto Bancário parcelado diretamente com o dietWin"><img src="<?php echo SITE_BASE ?>/views/imagens/boleto-bancario.png" alt="Boleto Bancário parcelado diretamente com o dietWin" /></a>
        </div>
        
        <div class="opcao">
        	<a id="link_pagseguro" title="Pagamento à vista ou parcelado por cartão de crédito via PagSeguro"><img src="<?php echo SITE_BASE ?>/views/imagens/pagamento-pagseguro.png" alt="Pagamento à vista ou parcelado por cartão de crédito via PagSeguro" /></a>
        </div>

        <div class="compra-boleto">
            <div class="separador"></div>

            <h2>Preencha as informações abaixo para que possamos gerar os boletos bancários:</h2>
            <form method="post" action="<?php echo SITE_URL.'/software/plano-de-assistencia/boleto-compra' ?>" id="form-pagamento" >
                <input id="forma-entrega-boleto" type="hidden" name="entrega_forma" value="pac" />
                <label><b>Número de parcelas:</b></label>
                <select name="parcelas" id="parcelas">
                <?php
                    if ($classe == 'profissional') {
                        $max = 8;
                    }
                    else if ($classe == 'personal') {
                        $max = 5;
                    }
                    else {
                        $max = 1;
                    }
                    for ($i = 0 ; $i < $max ; $i++)
                    {
                        echo '<option value="'.($i+1).'">'.($i+1).'</option>';
                    }
                ?>

                </select>
                
                <label><b>Data de vencimento dos boletos:</b></label>
                <div class="dias">
                	<input type="radio" name="data" value="05" checked="checked" /><span>Dia 05</span>
                    <input type="radio" name="data" value="15" /><span>Dia 15</span>
                    <input type="radio" name="data" value="25" /><span>Dia 25</span>
                </div>
                
                <div class="clear"></div>
                
                <div class="separador"></div>
    
                <p class="info-pedido"><b>Confira os dados acima e, caso estejam corretamente preenchidos, clique no botão "Enviar pedido de compra" para confirmar os dados e efetivar seu pedido</b></p>
                
                <button type="submit">Enviar</button>
                
                <div id="form_notification"></div>
                
                <div class="clear"></div>
            </form>
            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="compra-pagseguro">
            <div class="separador"></div>

            <p class="info-pagseguro">Ao clicar em <span>"Finalizar a compra"</span> você será direcionado ao site do PagSeguro, onde será realizado todo o processo de pagamento, garantindo total segurança. Após a conclusão você será redirecionado para o site do dietwin novamente.</p>

            <form method="post" action="<?php echo SITE_URL.'/software/plano-de-assistencia/pagseguro-compra' ?>" id="form-pagseguro" >
                <input id="forma-entrega-pagseguro" type="hidden" name="entrega_forma" value="pac" />
                <button type="submit">Enviar</button>

            <?/* <a href="<?php echo SITE_URL.'/software/'.$classe.'/finalizar-compra' ?>" title="Finalizar a compra"><img src="<?php echo SITE_BASE ?>/views/imagens/finalizar-compra.png" alt="Finalizar a compra" /></a> */?>
            
            <div class="clear"></div>
        </div>
        
        <div class="clear"></div>
            
        <div class="formas-pagamento">
            <img src="<?php echo SITE_BASE ?>/views/imagens/formas-pagamento.jpg" alt="Todas as formas de pagamento" />
        </div>
                
        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>  
</div>

<?php include 'includes/rodape.php'; ?>