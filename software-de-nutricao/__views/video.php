<?php include 'includes/cabecalho.php'; ?>

<body class="videos videos-<?php echo $classe ?> video">

<?php include 'includes/topo.php'; ?>
	
    <div class="titulo-principal">
        <p><span>Assista aos nossos</span><br />vídeos</p>       
    </div>
    <div class="titulo-resumo">
        <p>A equipe do dietWin reuniu os melhores vídeos de treinamento para que você possa conhecer cada funcionalidade de nossos softwares da maneira mais intuitiva possível!</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container" id="tamanho-fonte">
    	
        <?php include 'includes/toolbar.php' ?>
        <?php
            $regex = '/http\:\/\/www\.youtube\.com\/watch\?v=(\w{11})/';
            preg_match($regex, $video->link,$matches);
        ?>
        
        <object width="640" height="355"><param name="movie" value="http://www.youtube.com/v/<?php echo substr($video->link, strrpos($video->link,'v=')+2,11) ?>?version=3&amp;hl=pt_BR"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/<?php echo $matches[1] ?>?version=3&amp;hl=pt_BR" type="application/x-shockwave-flash" width="640" height="355" allowscriptaccess="always" allowfullscreen="true"></embed></object>
        
        <p class="data"><b><?php echo substr($video->data,8,2).' de '.Funcoes::mes_nome(substr($video->data,6,2)) ?></b></p>
        <h1><?php echo $video->titulo ?></h1>
        <p><?php echo $video->descricao ?></p>
        <a href="<?php echo SITE_URL.'/suporte/'.(isset($video->nome_seo)?substr($video->nome_seo,8):'topicos-diversos') ?>/videos" class="voltar" title="Voltar à listagem"><img src="<?php echo SITE_BASE ?>/views/imagens/voltar-listagem.png" alt="Voltar à listagem" /></a>
        
        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>  
</div>

<?php include 'includes/rodape.php'; ?>