<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_Pagseguro extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai e testa se o usuário está logado
	 */
	public function __construct()
	{
        parent::__construct();
        $this->verificar_usuario_logado();
        $this->verificar_usuario_permissao('pagseguro');
	}


    /**
     * Método padrão que não deve ser chamado diretamente. Se for, vai para a listagem
     * @return void
     */
    public function index()
    {
        header('Location: '.SITE_URL.'/fatorcms/pagseguro/listar');
        exit;
    }


    /**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @param null $transacao_status_id
	 * @return void
	 */
	public function listar($parametros, $transacao_status_id = NULL)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'pagseguro-lista.php'));
        $view->adicionar('body_class', 'pagseguro lista');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
        $this->view_adicionar_obrigatorias($view);

        //-----

        // Foi feita ou "contém" uma busca
		$clausula_where = ' TRUE ';
		$buscar = NULL;
		if (isset($parametros->buscar) AND strlen(trim($parametros->buscar)) > 0)
		{
			$buscar = trim($parametros->buscar);
			$buscar_aux = Funcoes::mysqli_escape(str_replace(' ', '%', $buscar));
            $clausula_where .= ' AND (pagseguro.identificador LIKE "%'.$buscar_aux.'%" OR pagseguro_transacao_status.status LIKE "%'.$buscar_aux.'%"
										OR compra.codigo LIKE "%'.$buscar_aux.'%"
										OR cliente.nome LIKE "%'.$buscar_aux.'%" OR cliente.email LIKE "%'.$buscar_aux.'%"
										OR cliente.cpf LIKE "%'.$buscar_aux.'%" OR cliente.cnpj LIKE "%'.$buscar_aux.'%"
										OR cliente.rg LIKE "%'.$buscar_aux.'%" OR cliente.responsavel_nome LIKE "%'.$buscar_aux.'%")';
		}

		//-----
		
		// Caso seja um tipo de transação específico
		if ( ! is_null($transacao_status_id))
		{
			// Adiciona o WHERE
			// Só trazemos as transações que não possuem registros mais recentes
			$clausula_where .= ' AND pagseguro_transacao_status.id = '.$transacao_status_id.'
								 AND pagseguro.identificador NOT IN (SELECT pagseguro_aux.identificador FROM {tabela_nome} AS pagseguro_aux WHERE pagseguro_aux.data > pagseguro.data)';
		}

		//-----

		// Ordenação dos resultados, podendo vir da página
		$ordenar_por = (isset($parametros->ordenar_por)) ? strtolower($parametros->ordenar_por) : 'data'; // Valor padrão
		$ordem = isset($parametros->ordem) ? strtolower($parametros->ordem) : 'desc'; // Valor padrão

		switch ($ordenar_por)
		{
			case 'data': $ordenar_por_sql = 'pagseguro.data'; break;
			case 'id': $ordenar_por_sql = 'pagseguro.identificador'; break;
			case 'situacao': $ordenar_por_sql = 'pagseguro_transacao_status.status'; break;
			case 'compra': $ordenar_por_sql = 'compra.codigo'; break;
			case 'cliente': $ordenar_por_sql = 'cliente.nome'; break;
            default: $ordenar_por_sql = $ordenar_por; break;
		}

		//-----

		$transacao = new FatorCMS_Model_Pagseguro;
		// Define a cláusula do select
		// Selects dentro de selects para conseguir pegar a atualização de status mais recente entre todas possíveis (pode haver diferentes tipos de pagamento para uma compra)
		// TODO pensar em uma maneira melhor de montar esse select em casos assim
		$transacao->clausula = '
			SELECT pagseguro.*, compra.codigo AS compra_codigo, cliente.id AS cliente_id, cliente.nome AS cliente_nome,
			    pagseguro_transacao_status.status AS transacao_status
			FROM (SELECT * FROM
						(SELECT * FROM {tabela_nome} ORDER BY DATA DESC) AS pagseguro_aux GROUP BY identificador
					) AS pagseguro
				LEFT JOIN {tabela_prefixo}compras AS compra ON pagseguro.compra_id = compra.id
				LEFT JOIN {tabela_prefixo}clientes AS cliente ON compra.cliente_id = cliente.id
				LEFT JOIN {tabela_prefixo}pagseguro_transacoes_status AS pagseguro_transacao_status ON pagseguro.transacao_status_id = pagseguro_transacao_status.id
				LEFT JOIN {tabela_prefixo}pagseguro_pagamentos_tipos AS pagseguro_pagamento_tipo ON pagseguro.pagamento_tipo_id = pagseguro_pagamento_tipo.id
				LEFT JOIN {tabela_prefixo}pagseguro_pagamentos_codigos AS pagseguro_pagamento_codigo ON pagseguro.pagamento_codigo_id = pagseguro_pagamento_codigo.id
			WHERE '.$clausula_where.'
			GROUP BY pagseguro.identificador
			ORDER BY '.$ordenar_por_sql.' '.$ordem
		;

		// Através da paginação vai verificar se existe a necessidade de dividir o resultado em várias páginas e se é necessário adicionar um LIMIT
		$paginacao = new Paginacao($transacao, $parametros, 30);

		// Executa a cláusula lá de cima
		$transacoes = $transacao->select(NULL, TRUE); // NULL para não alterar a clásula já definida e TRUE para forçar retorno em array

        //-----

		$view->adicionar('paginacao', $paginacao);
		$view->adicionar('buscar', $buscar);
		$view->adicionar('ordenar_por', $ordenar_por);
		$view->adicionar('ordem', $ordem);
		$view->adicionar('transacoes', $transacoes);
		$view->adicionar('paginacao', $paginacao);

		// Passa para a view o código do status para que possamos mostrar diferentes conteúdos
		$view->adicionar('transacao_status_id', $transacao_status_id);

		// Especifica o item da sidebar que ficará selecionado e a URL de pesquisa
		if (is_null($transacao_status_id)) {
			$view->adicionar('lateral_menu', array('item'=>'pagseguro','link'=>'listar todas'));
			$view->adicionar('busca_url', SITE_URL.'/fatorcms/pagseguro/listar');
			$view->adicionar('transacao_status', 'listar');
		} elseif ($transacao_status_id == 1) {
			$view->adicionar('lateral_menu', array('item'=>'pagseguro','link'=>'aguardando'));
			$view->adicionar('busca_url', SITE_URL.'/fatorcms/pagseguro/aguardando');
			$view->adicionar('transacao_status', 'aguardando');
		} elseif ($transacao_status_id == 2) {
			$view->adicionar('lateral_menu', array('item'=>'pagseguro','link'=>'analise'));
			$view->adicionar('busca_url', SITE_URL.'/fatorcms/pagseguro/analise');
			$view->adicionar('transacao_status', 'analise');
		} elseif ($transacao_status_id == 3) {
			$view->adicionar('lateral_menu', array('item'=>'pagseguro','link'=>'pagas'));
			$view->adicionar('busca_url', SITE_URL.'/fatorcms/pagseguro/pagas');
			$view->adicionar('transacao_status', 'pagas');
		} elseif ($transacao_status_id == 4) {
			$view->adicionar('lateral_menu', array('item'=>'pagseguro','link'=>'disponiveis'));
			$view->adicionar('busca_url', SITE_URL.'/fatorcms/pagseguro/disponiveis');
			$view->adicionar('transacao_status', 'disponiveis');
		} elseif ($transacao_status_id == 5) {
			$view->adicionar('lateral_menu', array('item'=>'pagseguro','link'=>'disputa'));
			$view->adicionar('busca_url', SITE_URL.'/fatorcms/pagseguro/disputa');
			$view->adicionar('transacao_status', 'disputa');
		} elseif ($transacao_status_id == 6) {
			$view->adicionar('lateral_menu', array('item'=>'pagseguro','link'=>'devolvidas'));
			$view->adicionar('busca_url', SITE_URL.'/fatorcms/pagseguro/devolvidas');
			$view->adicionar('transacao_status', 'devolvidas');
		} elseif ($transacao_status_id == 7) {
			$view->adicionar('lateral_menu', array('item'=>'pagseguro','link'=>'canceladas'));
			$view->adicionar('busca_url', SITE_URL.'/fatorcms/pagseguro/canceladas');
			$view->adicionar('transacao_status', 'canceladas');
		}

		$view->exibir();
	}


	/**
	 * Chama o método de listagem passando o parâmetro de status
	 * @param $parametros
	 * @return void
	 */
	public function aguardando($parametros)
	{
		$this->listar($parametros, 1);
	}

	/**
	 * Chama o método de listagem passando o parâmetro de status
	 * @param $parametros
	 * @return void
	 */
	public function analise($parametros)
	{
		$this->listar($parametros, 2);
	}

	/**
	 * Chama o método de listagem passando o parâmetro de status
	 * @param $parametros
	 * @return void
	 */
	public function pagas($parametros)
	{
		$this->listar($parametros, 3);
	}

	/**
	 * Chama o método de listagem passando o parâmetro de status
	 * @param $parametros
	 * @return void
	 */
	public function disponiveis($parametros)
	{
		$this->listar($parametros, 4);
	}

	/**
	 * Chama o método de listagem passando o parâmetro de status
	 * @param $parametros
	 * @return void
	 */
	public function disputa($parametros)
	{
		$this->listar($parametros, 5);
	}

	/**
	 * Chama o método de listagem passando o parâmetro de status
	 * @param $parametros
	 * @return void
	 */
	public function devolvidas($parametros)
	{
		$this->listar($parametros, 6);
	}

	/**
	 * Chama o método de listagem passando o parâmetro de status
	 * @param $parametros
	 * @return void
	 */
	public function canceladas($parametros)
	{
		$this->listar($parametros, 7);
	}


    /**
     * Exibe as informações de determinada transação, também listando possíveis atualizações anteriores
     * @param $parametros
     */
	public function visualizar($parametros)
	{
		if (isset($parametros->_0) AND is_numeric($parametros->_0))
		{
			$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'pagseguro-form.php'));
            $view->adicionar('body_class', 'pagseguro form');
            $view->adicionar('lateral_menu', array('item'=>'pagseguro','link'=>''));
            $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
            $this->view_adicionar_obrigatorias($view);

			//-----

			$transacao_id = Funcoes::mysqli_escape($parametros->_0);

			$transacao = new FatorCMS_Model_PagSeguro;
			$transacao = $transacao->select('
				SELECT pagseguro.*, compra.codigo AS compra_codigo, compra.cliente_id AS cliente_id, cliente.nome AS cliente_nome,
				    pagseguro_transacao_status.status AS transacao_status, pagseguro_pagamento_tipo.tipo AS pagamento_tipo,
				    pagseguro_pagamento_codigo.nome AS pagamento_meio
				FROM {tabela_nome} AS pagseguro
					LEFT JOIN {tabela_prefixo}compras AS compra ON pagseguro.compra_id = compra.id
					LEFT JOIN {tabela_prefixo}clientes AS cliente ON compra.cliente_id = cliente.id
					LEFT JOIN {tabela_prefixo}pagseguro_transacoes_status AS pagseguro_transacao_status ON pagseguro.transacao_status_id = pagseguro_transacao_status.id
					LEFT JOIN {tabela_prefixo}pagseguro_pagamentos_tipos AS pagseguro_pagamento_tipo ON pagseguro.pagamento_tipo_id = pagseguro_pagamento_tipo.id
					LEFT JOIN {tabela_prefixo}pagseguro_pagamentos_codigos AS pagseguro_pagamento_codigo ON pagseguro.pagamento_codigo_id = pagseguro_pagamento_codigo.id
				WHERE pagseguro.id = '.$transacao_id
			);
			$view->adicionar('transacao', $transacao);

			if ($transacao AND ! is_null($transacao->identificador))
			{
				// Buscamos as transações anteriores (status anteriores)
				$transacoes_anteriores = $transacao->select('
					SELECT pagseguro.*, pagseguro_transacao_status.status AS transacao_status
					FROM {tabela_nome} AS pagseguro
						LEFT JOIN {tabela_prefixo}pagseguro_transacoes_status AS pagseguro_transacao_status ON pagseguro.transacao_status_id = pagseguro_transacao_status.id
					WHERE pagseguro.identificador = "'.$transacao->identificador.'" AND pagseguro.id != '.$transacao->id.'
					ORDER BY pagseguro.data DESC
				', TRUE);

				$view->adicionar('transacoes_anteriores', $transacoes_anteriores);


				$view->exibir();
			}
			else
			{
				$n = new Notificacao('Transação não encontrada.', 'error', TRUE);
				header('Location: '.SITE_URL.'/fatorcms/pagseguro/listar');
				exit;
			}
		}
		else
		{
			$n = new Notificacao('Identificador de transação inválido.', 'error', TRUE);
			header('Location: '.SITE_URL.'/fatorcms/pagseguro/listar');
			exit;
		}
	}

	/****************************** MÉTODOS EXTRAS ******************************/
	

} // end class