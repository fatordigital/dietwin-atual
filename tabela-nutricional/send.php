<?php

$errors = array();
$data = array();

require 'phpmailer/PHPMailerAutoload.php';

$mail = new PHPMailer;

$nome = "";
$email = "";
$mensagem = "";

$nome = trim($_POST['nome']);
$email = trim($_POST['email']);
$mensagem = trim($_POST['mensagem']);

if (empty($nome) || empty($email) || empty($mensagem)) {
    http_response_code(400);
    die(json_encode(array('error' => true, 'msg' => 'Todos os campos são obrigatórios.')));
}


// Set mailer to use SMTP
$mail->isSMTP();
// Specify main and backup SMTP servers
$mail->Host = 'smtp.mandrillapp.com:2525';
// Enable SMTP authentication
$mail->SMTPAuth = true;
// SMTP username
$mail->Username = 'mandrill@fatordigital.com.br';
// SMTP password
$mail->Password = 's5S72kL0BChLCkvLgh1feQ';
$mail->SMTPSecure = 'tls';


// Define o replyTo
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->AddReplyTo($email, $nome); //Facilita a resposta de e-mail do cliente

// Define os destinatário(s)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//	$mail->AddBCC('heron.martins@fatordigital.com.br');
//$mail->AddBCC('andrew.rodrigues@fatordigital.com.br');
$mail->AddAddress('suporte@dietwin.com.br');
//$mail->AddAddress('andre.carello@fatordigital.com.br');
//$mail->AddCC('cleber.alves@fatordigital.com.br', 'Cléber Alves'); // Copia
//$mail->AddBCC('cleber.alves@fatordigital.com.br', 'Cléber Alves'); // Cópia Oculta (manter sempre essa cópia oculta, afins de validação)


$mail->From = 'contato@dietwin.com.br';
$mail->FromName = 'DietWin';
$mail->isHTML(true);

$mail->Subject = 'Contato DietWin - Tabela Nutricional';
$mail->CharSet = 'UTF-8';
$mail->Body = '
    <div>
      <p>Contato pelo site DietWin - Tabela Nutricional <br><br></p>
      <p>Nome: ' . $nome . ' </p>
      <p>E-mail: ' . $email . '</p>
      <p>Mensagem: <br><br>' . $mensagem . '</p>
    </div>
    ';

if (!$mail->send()) {
    http_response_code(400);
    $data = array(
        'error' => true,
        'msg' => 'Ocorreu um erro durante o envio.'
    );
} else {
    http_response_code(200);
    $data = array(
        'msg' => 'Mensagem enviada com sucesso',
        'error' => false
    );
}

die(json_encode($data));