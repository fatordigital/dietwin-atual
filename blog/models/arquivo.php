<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property string titulo
 * @property string descricao
 * @property string data
 * @property string link
 * @property string arquivo
 */

class Model_Arquivo extends Model_Padrao
{
	protected $tabela_nome = 'arquivos';

} // end class