<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.'); ?>

<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php' ?>
		
	<div id="main-content"> <!-- Main Content Section with everything -->

		<!-- Page Head -->
		<h2>Lista de Usuários do FatorCMS cadastrados</h2>
		<p id="page-intro">Abaixo estão todos os usuários cadastrados para acesso no FatorCMS, e cada um pode ter seus dados editados.</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<form action="<?php echo SITE_URL ?>/fatorcms/cms-usuarios/listar" method="get" id="usuarios_buscar_form">
			<fieldset>
				<p>
					<label for="buscar">Digite uma ou mais palavras-chave para buscar por um usuário cadastrado:</label>
					<input class="text-input medium-input" type="text" id="buscar" name="buscar" maxlength="100" value="<?php echo isset($buscar) ? $buscar : '' ?>" />
					<input class="button" type="submit" value="Buscar" />
				</p>
			</fieldset>
			<div class="clear"></div><!-- End .clear -->
		</form>

		<?php if (isset($buscar) AND ! is_null($buscar)) { ?>
			<p class="buscar-resultado">
				Foram encontrados <strong><?php echo $paginacao->linhas_total ?></strong> resultado(s) para a busca por "<strong><?php echo $buscar ?></strong>".
				<a title="Limpar a busca" href="<?php echo SITE_URL?>/fatorcms/cms-usuarios/listar"><img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier-cross.png" alt="Limpar busca" /> Clique aqui para limpar a busca</a>
			</p>
		<?php } ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Usuários</h3>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<table>

					<thead>
						<tr>
							<?php echo Funcoes::montar_th_ordenacao_listagem('cms-usuarios', 'listar', $paginacao, $buscar, 'Nome', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('cms-usuarios', 'listar', $paginacao, $buscar, 'Usuário', $ordenar_por, $ordem) ?>
							<th>Ações</th>
						</tr>

					</thead>

					<tfoot>
						<tr>
							<td colspan="3">

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_quantidades() ?>

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_links() ?>

								<div class="clear"></div>
							</td>
						</tr>
					</tfoot>

					<tbody>
						<?php
						if ($cms_usuarios AND count($cms_usuarios) > 0)
						{
							foreach ($cms_usuarios as $cms_usuario)
							{
							?>
								<tr>
									<td class="<?php echo $ordenar_por=='nome' ? 'current' : '' ?>">
										<?php if ($cms_usuario_logado->id == $cms_usuario->id OR $cms_usuario_logado->eh_administrador) { ?>
                                            <a href="<?php echo SITE_URL ?>/fatorcms/cms-usuarios/editar/<?php echo $cms_usuario->id ?>" title="Editar usuário &quot;<?php echo $cms_usuario->nome ?>&quot;"><?php echo $cms_usuario->nome ?></a>
	                                    <?php } else { ?>
											<?php echo $cms_usuario->nome ?>
	                                    <?php } ?>
                                    </td>
									<td class="<?php echo $ordenar_por=='usuario' ? 'current' : '' ?>"><?php echo $cms_usuario->usuario ?></td>
									<td>
										<?php if ($cms_usuario_logado->id == $cms_usuario->id OR $cms_usuario_logado->eh_administrador) { ?>
											<a href="<?php echo SITE_URL ?>/fatorcms/cms-usuarios/editar/<?php echo $cms_usuario->id ?>" title="Editar usuário &quot;<?php echo $cms_usuario->nome ?>&quot;"><img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" /></a>
										<?php } ?>
										<?php if ($cms_usuario_logado->eh_administrador) { ?>
											<a href="<?php echo SITE_URL ?>/fatorcms/cms-usuarios/excluir/<?php echo $cms_usuario->id?>" title="Excluir usuário &quot;<?php echo $cms_usuario->nome ?>&quot;" class="item-confirmar"><img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/cross.png" alt="Excluir" /></a>
										<?php } ?>
									</td>
								</tr>
							<?php
							}
						}
						else
						{
							echo '<tr><td colspan="3">'.((isset($buscar) AND ! is_null($buscar)) ? 'Nenhum usuário encontrado para esta busca.' : 'Nenhum usuário cadastrado até o momento.' ).'</td></tr>';
						}
						?>
					</tbody>

				</table>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>