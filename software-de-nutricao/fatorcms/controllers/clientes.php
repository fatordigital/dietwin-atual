<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_Clientes extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai e testa se o usuário está logado
	 */
	public function __construct()
	{
		parent::__construct();
		$this->verificar_usuario_logado();
		$this->verificar_usuario_permissao('clientes');
	}


	/**
	 * Método padrão que não deve ser chamado diretamente. Se for, vai para a listagem
	 * @return void
	 */
	public function index()
	{
        header('Location: '.SITE_URL.'/fatorcms/clientes/listar');
		exit;
	}


	//<editor-fold desc="Lógica do gerenciamento de Cliente">
	/**
	 * Método inicial que exibe a lista de registros do banco de dados
	 * @param $parametros object
	 * @return void
	 */
	public function listar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'clientes-lista.php'));
		$view->adicionar('body_class', 'clientes lista');
		$view->adicionar('lateral_menu', array('item'=>'clientes','link'=>'listar todos'));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$this->view_adicionar_obrigatorias($view);

		//-----

		// Lógica caso tenha sido feita uma busca na listagem
		$clausula_where = ' TRUE AND cliente.tipo = 1 ';

		$buscar = NULL;
		if (isset($parametros->buscar) AND strlen(trim($parametros->buscar)) > 0)
		{
			$buscar = trim($parametros->buscar);
			$buscar_aux = Funcoes::mysqli_escape(str_replace(' ', '%', $buscar));
			$clausula_where .= ' AND (cliente.nome LIKE "%'.$buscar_aux.'%" OR cliente.email LIKE "%'.$buscar_aux.'%" OR
										cliente.cpf LIKE "%'.$buscar_aux.'%" OR cliente.cnpj LIKE "%'.$buscar_aux.'%")';
		}

		//-----

		// Ordenação dos resultados, podendo vir da página
		$ordenar_por = (isset($parametros->ordenar_por)) ? strtolower($parametros->ordenar_por) : 'nome'; // Valor padrão
		$ordem = isset($parametros->ordem) ? strtolower($parametros->ordem) : 'asc'; // Valor padrão

		switch ($ordenar_por)
		{
			case 'nome': $ordenar_por_sql = 'cliente.nome'; break;
			case 'interacoes': $ordenar_por_sql = 'interacoes_total'; break;
			case 'compras': $ordenar_por_sql = 'compras_total'; break;
			case 'email': $ordenar_por_sql = 'cliente.email'; break;
			case 'cpf-cnpj': $ordenar_por_sql = 'cliente.cpf'; break;
			case 'ativo': $ordenar_por_sql = 'cliente.ativo'; break;
            default: $ordenar_por_sql = $ordenar_por; break;
		}


		//-----

		$cliente = new FatorCMS_Model_Cliente;

		//backup sub select
		// (SELECT COUNT(interacao.id) FROM {tabela_prefixo}interacoes AS interacao WHERE cliente.id = interacao.cliente_id) AS interacoes_total,
		//(SELECT COUNT(compra.id) FROM {tabela_prefixo}compras AS compra WHERE cliente.id = compra.cliente_id) AS compras_total,

		// Define a cláusula do select
		$cliente->clausula = '
			SELECT cliente.id, cliente.nome, cliente.nome_empresa, cliente.email, cliente.cpf, cliente.cnpj, cliente.ativo, cliente.compras_total,
                (SELECT fd_compras.fim FROM fd_compras_produtos JOIN fd_compras ON fd_compras.id = fd_compras_produtos.compra_id WHERE fd_compras.fim > CURRENT_DATE AND fd_compras_produtos.produto_id = 4 AND fd_compras.cliente_id = cliente.id limit 1) AS plano_assistencia
			FROM {tabela_nome} AS cliente
			WHERE '.$clausula_where.'
			GROUP BY cliente.id
			ORDER BY '.$ordenar_por_sql.' '.$ordem
		;

		// Através da paginação vai verificar se existe a necessidade de dividir o resultado em várias páginas e se é necessário adicionar um LIMIT
		$paginacao = new Paginacao($cliente, $parametros, 20, 10);

		// Executa a cláusula lá de cima
		$clientes = $cliente->select(NULL, TRUE); // NULL para não alterar a clásula já definida e TRUE para forçar retorno em array

		//-----

		$view->adicionar('buscar', $buscar);
		$view->adicionar('ordenar_por', $ordenar_por);
		$view->adicionar('ordem', $ordem);
		$view->adicionar('clientes', $clientes);
		$view->adicionar('paginacao', $paginacao);

		$view->exibir();
	}


	/**
	 * Dependendo do parâmetro, aprensenta a tela de edição ou de cadastro
	 * @param $parametros object
	 * @return void
	 */
	public function editar($parametros)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'clientes-form.php'));

        $view->adicionar('body_class', 'clientes form');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
		$this->view_adicionar_obrigatorias($view);

        // Decide o que fazer
        if (isset($parametros->_0) AND is_numeric($parametros->_0))
        {
            // É atualização
            $cliente = new FatorCMS_Model_Cliente;
	        $cliente = $cliente->select('SELECT * FROM {tabela_nome} AS cliente WHERE id = '.Funcoes::mysqli_escape($parametros->_0));
            $view->adicionar('cliente', $cliente);

	        // Endereços
	        $view->adicionar('cliente_enderecos', $this->carregar_cliente_enderecos($cliente->id));

	        // Compras
	        $view->adicionar('cliente_compras', $this->carregar_cliente_compras($cliente->id));

            //Plano de Assistencia
            $plano_assistencia = new FatorCMS_Model_Compra();
            $plano_assistencia = $plano_assistencia->select('
                SELECT
                    fd_compras.id, fd_compras.data, fd_compras.fim, fd_compras.inicio, fd_compras.informacao_adicional,
                    fd_compras_produtos.licenca_quantidade,
                    fd_compras_produtos.valor+(fd_compras_produtos.licenca_valor * fd_compras_produtos.licenca_quantidade) - IF(fd_compras_cupons.valor IS NULL,0,fd_compras_cupons.valor) AS total
                FROM fd_compras
                JOIN fd_compras_produtos ON fd_compras.id = fd_compras_produtos.compra_id
                LEFT JOIN fd_compras_cupons ON fd_compras.id = fd_compras_cupons.compra_id
                WHERE fd_compras.cliente_id = '.$cliente->id.' AND fd_compras_produtos.produto_id = 4 AND fd_compras.fim >= CURRENT_DATE
                ORDER BY fd_compras.fim DESC, fd_compras.inicio DESC, fd_compras.data DESC
                LIMIT 1
            ');

            //print_r($plano_assistencia); exit;

            $view->adicionar('plano_assistencia', $plano_assistencia);

            $recorrencia = new FatorCMS_Model_ClienteRecorrencia();
            $recorrencia = $recorrencia->select("SELECT * FROM fd_clientes_recorrencia WHERE fd_clientes_recorrencia.cliente_id = ".$cliente->id);

            //var_dump($recorrencia); die;

            $view->adicionar('recorrencia', $recorrencia);

	        // Interações
	        $view->adicionar('cliente_interacoes', $this->carregar_cliente_interacoes($cliente->id));

            $view->adicionar('lateral_menu', array('item'=>'clientes','link'=>''));
        }
        else
        {
            // É cadastro
			$view->adicionar('cliente', NULL);
			$view->adicionar('lateral_menu', array('item'=>'clientes','link'=>'cadastrar'));
        }

		// Estados para o <select>
		$view->adicionar('estados', $this->carregar_estados());

		// Produtos para o <select>
		$view->adicionar('produtos', $this->carregar_produtos());

		// Cupons para o <select>
		$view->adicionar('cupons', $this->carregar_cupons());

		// Abre a aba que veio na URL, ou a padrão
		$view->adicionar('aba', isset($parametros->aba) ? $parametros->aba : 1);

        $view->exibir();
    }


	/**
	 * Recebe os parâmetros do formulário e adiciona o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function cadastrar($parametros)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'clientes-form.php'));
		$this->view_adicionar_obrigatorias($view);
		//-----
		if ($parametros AND isset($parametros->nome))
		{
            if(isset($parametros->senha_aleatoria)){
                $senha = substr(sha1($parametros->nome), 0, 6);
                $parametros->senha = $senha;
            }

			$cliente = new FatorCMS_Model_Cliente;
			$cliente->carregar($parametros);

			// Campos especiais
			$cliente->email = mb_strtolower($parametros->email);
			$cliente->senha = ( ! empty($parametros->senha) ? sha1($parametros->senha) : NULL);
			$cliente->cpf = str_replace(array('.','-'), '', $parametros->cpf);
			$cliente->cnpj = ( ! empty($parametros->cnpj) ? str_replace(array('.','-','/'), '', $parametros->cnpj) : NULL);
            $cliente->telefone_principal = str_replace(array('(',')',' ','-'), '', $parametros->telefone_principal);
            $cliente->telefone_extra = ( ! empty($parametros->telefone_extra) ? str_replace(array('(',')',' ','-'), '', $parametros->telefone_extra) : NULL);

			//-----

            $cliente->colunas_mysqli_escape();

			if ($cliente->verificar_null(array('id', 'cadastro_data', 'tipo', 'cadastro_completo')))
			{
				// Validações ---

				if ( ! filter_var($cliente->email, FILTER_VALIDATE_EMAIL))
				{
					$notificacao = new Notificacao('O endereço de e-mail <strong>'.$cliente->email.'</strong> não é válido.');
				}

				//-----

				if ( ! isset($notificacao) AND ! Funcoes::validar_cpf($cliente->cpf))
				{
					$notificacao = new Notificacao('O CPF <strong>'.Funcoes::formatar_cpf($cliente->cpf).'</strong> não é válido.');
				}

				//-----

				if ( ! isset($notificacao) AND ! empty($cliente->cnpj) AND ! Funcoes::validar_cnpj($cliente->cnpj))
				{
					$notificacao = new Notificacao('O CNPJ <strong>'.Funcoes::formatar_cnpj($cliente->cnpj).'</strong> não é válido.');
				}

				//-----

				// Se o cadastro vier com CNPJ, é empresa e deixamos que o cadastro ocorra mesmo havendo o mesmo CPF em outro cadastro
				// (funcionário tem um cadastro para a empresa onde trabalha e outro cadastro para usar em casa?)
				$cliente_existe = new FatorCMS_Model_Cliente;
				$cliente_existe = $cliente_existe->select('
					SELECT id, cpf, cnpj, email
					FROM {tabela_nome}
					WHERE email = "'.$cliente->email.'" OR
							('.(empty($cliente->cnpj) ? 'cpf = '.$cliente->cpf.' AND cnpj IS NULL' : 'cnpj = '.$cliente->cnpj).')'
				);

				if ( ! isset($notificacao) AND $cliente_existe AND ! is_null($cliente_existe->id))
				{
					if ($cliente->email == $cliente_existe->email)
					{
						$notificacao = new Notificacao('O e-mail <strong>'.$cliente->email.'</strong> pertence a outro cliente. Por favor utilize outro endereço de e-mail.');
					}
					elseif (empty($cliente->cnpj) AND $cliente->cpf == $cliente_existe->cpf)
					{
						$notificacao = new Notificacao('O CPF <strong>'.Funcoes::formatar_cpf($cliente->cpf).'</strong> pertence a outro cliente. Por favor utilize outro CPF.');
					}
					else
					{
						$notificacao = new Notificacao('O CNPJ <strong>'.Funcoes::formatar_cnpj($cliente->cnpj).'</strong> pertence a outro cliente. Por favor utilize outro CNPJ.');
					}
				}

				//-----

				if ( ( ! empty($cliente->cnpj) AND empty($cliente->responsavel_nome)) OR ( ! empty($cliente->responsavel_nome) AND empty($cliente->cnpj)) OR
                     ( ! empty($cliente->cnpj) AND empty($cliente->nome_empresa)) OR ( ! empty($cliente->nome_empresa) AND empty($cliente->cnpj)) OR
                     ( ! empty($cliente->responsavel_nome) AND empty($cliente->nome_empresa)) OR ( ! empty($cliente->nome_empresa) AND empty($cliente->responsavel_nome)))
				{
					$notificacao = new Notificacao('Ao cadastrar uma empresa, é obrigatório o preenchimento do <strong>Nome da Empresa</strong>, <strong>CNPJ</strong> e do <strong>Responsável na empresa</strong>.');
				}

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					// Data de cadastro do cliente
					$cliente->cadastro_data = date('d/m/Y H:i:s');
                    $cliente->tipo = 1;

					if ($cliente->insert())
					{
                        //parte que faz ligação entre um novo cliente e os arquivos disponíveis para novos clientes
                        //if($parametros->arquivos == 1){
                        $arquivo = new Model_Arquivo;
                        $arquivos = $arquivo->select('SELECT * FROM {tabela_nome} WHERE habilitado = 1', TRUE);
                        foreach($arquivos as $arq){
                                $cliente_arquivo = new FatorCMS_Model_ArquivoCliente;
                                $cliente_arquivo->arquivo_id = $arq->id;
                                $cliente_arquivo->cliente_id = $cliente->id;
                                $cliente_arquivo->insert();
                        }
                        //}
                        //>>>>>>> PARTE DO E-MAIL <<<<<<<<<<<<<<<<<<<
                        if(isset($parametros->senha_aleatoria))
                        {
                            $mail = new Controller_Email;
                            if ( ! $mail->cadastra_usuario($parametros))
                            {
                                new Notificacao('E-mail com senha para cliente <strong>'.$cliente->nome.'</strong> não foi eviado corretamente.');
                                header('Location: '.SITE_URL.'/fatorcms/clientes/listar');
                                exit;
                            }
                        }
                        //>>>>>>> PARTE DO E-MAIL <<<<<<<<<<<<<<<<<<<
                        $cliente = new FatorCMS_Model_Cliente($cliente->id);
                        if ($cliente->verificar_cadastro_completo())
                        {
                            $cliente->cadastro_completo = 1;
                        }
                        else
                        {
                            $cliente->cadastro_completo = 0;
                        }
                        $cliente->update();

						new Notificacao('Cliente <strong>'.$cliente->nome.'</strong> cadastrado com sucesso.', 'success', TRUE);
						header('Location: '.SITE_URL.'/fatorcms/clientes/listar');
						exit;
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao salvar os dados do cliente.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
			}
            
			// Para retornar ao formulário com valores inseridos
			$view->adicionar('cliente', $cliente);
		}
		else
		{
			$view->adicionar('cliente', NULL);
		}

		//-----

		$view->adicionar('body_class', 'clientes formulario');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
        $view->adicionar('lateral_menu', array('item'=>'clientes','link'=>'cadastrar'));

        $view->exibir();
	}


	/**
	 * Recebe os parâmetros do formulário e atualiza o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function atualizar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'clientes-form.php'));
		$this->view_adicionar_obrigatorias($view);

		//-----

		if (isset($parametros->id) AND is_numeric($parametros->id))
		{
			$cliente = new FatorCMS_Model_Cliente;
			$cliente->carregar($parametros);

			// Campos especiais
            $cliente->email = mb_strtolower($parametros->email);
            $cliente->senha = ( ! empty($parametros->senha) ? sha1($parametros->senha) : NULL);
            $cliente->cpf = str_replace(array('.','-'), '', $parametros->cpf);
            $cliente->cnpj = ( ! empty($parametros->cnpj) ? str_replace(array('.','-','/'), '', $parametros->cnpj) : NULL);
            $cliente->telefone_principal = str_replace(array('(',')',' ','-'), '', $parametros->telefone_principal);
            $cliente->telefone_extra = ( ! empty($parametros->telefone_extra) ? str_replace(array('(',')',' ','-'), '', $parametros->telefone_extra) : NULL);

			//-----

			$cliente->colunas_mysqli_escape();

            if ($cliente->verificar_null(array('cadastro_data', 'senha', 'cadastro_completo')))
			{
				// Validações ---

                if ( ! filter_var($cliente->email, FILTER_VALIDATE_EMAIL))
                {
                    $notificacao = new Notificacao('O endereço de e-mail <strong>'.$cliente->email.'</strong> não é válido.');
                }

                //-----

                if ( ! isset($notificacao) AND ! Funcoes::validar_cpf($cliente->cpf))
                {
                    $notificacao = new Notificacao('O CPF <strong>'.Funcoes::formatar_cpf($cliente->cpf).'</strong> não é válido.');
                }

                //-----

                if ( ! isset($notificacao) AND ! empty($cliente->cnpj) AND ! Funcoes::validar_cnpj($cliente->cnpj))
                {
                    $notificacao = new Notificacao('O CNPJ <strong>'.Funcoes::formatar_cnpj($cliente->cnpj).'</strong> não é válido.');
                }

                //-----

                // Se o cadastro vier com CNPJ, é empresa e deixamos que o cadastro ocorra mesmo havendo o mesmo CPF em outro cadastro
                // (funcionário tem um cadastro para a empresa onde trabalha e outro cadastro para usar em casa?)
                $cliente_existe = new FatorCMS_Model_Cliente;
                $cliente_existe = $cliente_existe->select('
					SELECT id, cpf, cnpj, email
					FROM {tabela_nome}
					WHERE id != "'.$cliente->id.'" AND (email = "'.$cliente->email.'" OR
							('.(empty($cliente->cnpj) ? 'cpf = '.$cliente->cpf.' AND cnpj IS NULL' : 'cnpj = '.$cliente->cnpj).'))'
                );

                if ( ! isset($notificacao) AND $cliente_existe AND ! is_null($cliente_existe->id))
                {
                    if ($cliente->email == $cliente_existe->email)
                    {
                        $notificacao = new Notificacao('O e-mail <strong>'.$cliente->email.'</strong> pertence a outro cliente. Por favor utilize outro endereço de e-mail.');
                    }
                    elseif (empty($cliente->cnpj) AND $cliente->cpf == $cliente_existe->cpf)
                    {
                        $notificacao = new Notificacao('O CPF <strong>'.Funcoes::formatar_cpf($cliente->cpf).'</strong> pertence a outro cliente. Por favor utilize outro CPF.');
                    }
                    else
                    {
                        $notificacao = new Notificacao('O CNPJ <strong>'.Funcoes::formatar_cnpj($cliente->cnpj).'</strong> pertence a outro cliente. Por favor utilize outro CNPJ.');
                    }
                }

                //-----

                if ( ( ! empty($cliente->cnpj) AND empty($cliente->responsavel_nome)) OR ( ! empty($cliente->responsavel_nome) AND empty($cliente->cnpj)) OR
                    ( ! empty($cliente->cnpj) AND empty($cliente->nome_empresa)) OR ( ! empty($cliente->nome_empresa) AND empty($cliente->cnpj)) OR
                    ( ! empty($cliente->responsavel_nome) AND empty($cliente->nome_empresa)) OR ( ! empty($cliente->nome_empresa) AND empty($cliente->responsavel_nome)))
                {
                    $notificacao = new Notificacao('Ao cadastrar uma empresa, é obrigatório o preenchimento do <strong>Nome da Empresa</strong>, <strong>CNPJ</strong> e do <strong>Responsável na empresa</strong>.');
                }

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($cliente->update())
					{
                       $cliente = new FatorCMS_Model_Cliente($cliente->id);
                        if ($cliente->verificar_cadastro_completo())
                        {
                           // echo 'completo';
                            $cliente->cadastro_completo = 1;
                        }
                        else
                        {
                            //echo 'incompleto';
                            $cliente->cadastro_completo = 0;
                        }
                        $cliente->update();
                        //exit;
						new Notificacao('Cliente <strong>'.$cliente->nome.'</strong> atualizado com sucesso.', 'success', TRUE);
						header('Location: '.SITE_URL.'/fatorcms/clientes/listar');
						exit;
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao atualizar os dados do cliente.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos os campos marcados são obrigatórios.');
			}

			// Para retornar ao formulário com valores inseridos
			$view->adicionar('cliente', $cliente);

            // Endereços
            $view->adicionar('cliente_enderecos', $this->carregar_cliente_enderecos($cliente->id));

            // Compras
            $view->adicionar('cliente_compras', $this->carregar_cliente_compras($cliente->id));

            // Interações
            $view->adicionar('cliente_interacoes', $this->carregar_cliente_interacoes($cliente->id));
		}
		else
		{
			$notificacao = new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention');
		}

		//-----

        // Estados para o <select>
        $view->adicionar('estados', $this->carregar_estados());

        // Produtos para o <select>
        $view->adicionar('produtos', $this->carregar_produtos());

		$view->adicionar('body_class', 'clientes formulario');
		$view->adicionar('lateral_menu', array('item'=>'clientes','link'=>''));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$view->exibir();
	}


	/**
	 * Recebe o ID do registro altera o status dele do banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function alterar_ativo($parametros)
	{
		if (isset($parametros->_0) AND is_numeric($parametros->_0))
		{
			$cliente = new FatorCMS_Model_Cliente;
			$cliente = $cliente->select('SELECT id, nome, ativo FROM {tabela_nome} WHERE id="'.Funcoes::mysqli_escape($parametros->_0).'"');

			if ($cliente AND ! is_null($cliente->id))
			{
				// Invertemos o valor da coluna ativo do cliente
				$cliente->ativo = ($cliente->ativo ? 0 : 1);

				if ($cliente->update())
				{
					new Notificacao('O cliente <strong>'.$cliente->nome.'</strong> foi '.($cliente->ativo ? 'habilitado' : 'desabilitado').' para acesso no site com sucesso.', 'success', TRUE);
				}
				else
				{
					new Notificacao('Ocorreu um erro ao alterar a permissão de acesso do cliente <strong>'.$cliente->nome.'</strong>.', 'error', TRUE);
				}
            }
			else
			{
				new Notificacao('Nenhuma informação encontrada para este cliente.', 'error', TRUE);
			}
		}
		else
		{
			new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention', 'error', TRUE);
		}

		header('Location: '.SITE_URL.'/fatorcms/clientes/listar');
		exit;
	}
	//</editor-fold>


	/*************** MÉTODOS PARA A PARTE DOS ENDEREÇOS ***************/


    //<editor-fold desc="Lógica de gerenciamento de endereços">
    /**
	 * Retorna os dados do endereço do cliente conforme ID passado por parâmetro, para edição no formulário
	 * Método chamado por requisição AJAX, por isso os echos json_encode
	 * @param  $parametros
	 * @return void
	 */
	public function endereco_editar($parametros)
	{
		if (isset($parametros->_0) AND is_numeric($parametros->_0))
		{
			$cliente_endereco = new FatorCMS_Model_ClienteEndereco($parametros->_0);

			if ($cliente_endereco AND ! is_null(($cliente_endereco->id)))
			{
				$cliente_endereco_dados = array();
				foreach ($cliente_endereco->buscar_colunas() as $coluna=>$valor)
				{
					$cliente_endereco_dados[$coluna] = $cliente_endereco->$coluna;
				}
				echo json_encode(array('tipo'=>'success', 'cliente_endereco'=>$cliente_endereco_dados));
			}
			else
			{
				echo json_encode(array('tipo'=>'error','mensagem'=>'Endereço não encontrado.'));
			}
		}
		else
		{
			echo json_encode(array('tipo'=>'error','mensagem'=>'Identificador de endereço inválido.'));
		}
	}


	/**
	 * Recebe os parâmetros do formulário e adiciona o registro no banco de dados
	 * Método chamado por requisição AJAX, por isso os echos json_encode
	 * @param  $parametros
	 * @return void
	 */
	public function endereco_cadastrar($parametros)
	{
		if ($parametros AND isset($parametros->endereco))
		{
			$cliente_endereco = new FatorCMS_Model_ClienteEndereco;
			$cliente_endereco->carregar($parametros);

			// Campos especiais
			$cliente_endereco->cep = str_replace(array('-'), '', $parametros->cep);

			//-----

			$cliente_endereco->colunas_mysqli_escape();

			if ($cliente_endereco->verificar_null(array('id','entrega')))
			{
				// Validações ---

				// Se houver outro endereço, este aqui fica marcado como "para entrega". Se for o primeiro, marcamos com
				// zero mas no site ele é considerado como "para entrega". E se já houver dois endereços, não deixa
				// cadastrar outro
				$cliente_endereco_existe = new FatorCMS_Model_ClienteEndereco;
				$cliente_endereco_existem = $cliente_endereco_existe->select('
							SELECT id, entrega FROM {tabela_nome} WHERE cliente_id = "'.$cliente_endereco->cliente_id.'"
				', TRUE);

				if ($cliente_endereco_existem AND count($cliente_endereco_existem) > 0)
				{
					if (count($cliente_endereco_existem) >= 2)
					{
						// Não pode cadastrar um terceiro endereço
						$notificacao = TRUE; // Para o IF abaixo
						echo json_encode(array('tipo'=>'error','mensagem'=>'Um cliente só pode ter dois endereços cadastrados.'));
					}
					else
					{
						// Só tem um endereço, então este novo é para entrega
						$cliente_endereco->entrega = 1;
					}
				}
				else
				{
					// Provavelmente nenhum outro endereço
					$cliente_endereco->entrega = 0;
				}

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($cliente_endereco->insert())
					{
                        $cliente = new FatorCMS_Model_Cliente($cliente_endereco->cliente_id);
                        if ($cliente->verificar_cadastro_completo())
                        {
                           // echo 'completo'; exit;
                            $cliente->cadastro_completo = 1;
                        }
                        else
                        {
                            //echo 'incompleto'; exit;
                            $cliente->cadastro_completo = 0;
                        }
                        $cliente->update();

						new Notificacao('Endereço <strong>'.$cliente_endereco->endereco.'</strong> cadastrado com sucesso.', 'success', TRUE);
						echo json_encode(array('tipo'=>'success'));
					}
					else
					{
						echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao cadastrar os dados do endereço do cliente.'));
					}
				}
			}
			else
			{
				echo json_encode(array('tipo'=>'error','mensagem'=>'Todos campos marcados são obrigatórios.'));
			}
		}
		else
		{
			echo json_encode(array('tipo'=>'error','mensagem'=>'Nenhuma informação encontrada.'));
		}
	}


	/**
	 * Recebe os parâmetros do formulário e atualiza o registro no banco de dados
	 * Método chamado por requisição AJAX, por isso os echos json_encode
	 * @param  $parametros
	 * @return void
	 */
	public function endereco_atualizar($parametros)
	{
		if ($parametros AND isset($parametros->endereco))
		{
			$cliente_endereco = new FatorCMS_Model_ClienteEndereco;
			$cliente_endereco->carregar($parametros);

			// Campos especiais
			$cliente_endereco->cep = str_replace(array('-'), '', $parametros->cep);

			//-----

			$cliente_endereco->colunas_mysqli_escape();

			if ($cliente_endereco->verificar_null(array('entrega')))
			{
				// Validações ---

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($cliente_endereco->update())
					{
						new Notificacao('Endereço <strong>'.$cliente_endereco->endereco.'</strong> atualizado com sucesso.', 'success', TRUE);
						echo json_encode(array('tipo'=>'success'));
					}
					else
					{
						echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao atualizar os dados do endereço do cliente.'));
					}
				}
			}
			else
			{
				echo json_encode(array('tipo'=>'error','mensagem'=>'Todos campos marcados são obrigatórios.'));
			}
		}
		else
		{
			echo json_encode(array('tipo'=>'error','mensagem'=>'Nenhuma informação encontrada.'));
		}
	}


	/**
	 * Exclui um endereço de um cliente
	 * @param $parametros
	 */
	public function endereco_excluir($parametros)
	{
		if (isset($parametros->_0) AND is_numeric($parametros->_0))
		{
			$cliente_endereco = new FatorCMS_Model_ClienteEndereco($parametros->_0);

			if ($cliente_endereco->delete())
			{
				// Verificamos se existe o cliente possui outro endereço e mantemos correta a informação sobre qual é
				// para entrega
				if ( ! $cliente_endereco->entrega) // Foi excluído um que não era entrega
				{
					$cliente_endereco_existe = new FatorCMS_Model_ClienteEndereco;
					$cliente_endereco_existem = $cliente_endereco_existe->select('
						SELECT id, entrega
						FROM {tabela_nome}
						WHERE cliente_id = "'.$cliente_endereco->cliente_id.'"
					', TRUE);

					if ($cliente_endereco_existem AND count($cliente_endereco_existem) > 0)
					{
						foreach ($cliente_endereco_existem AS $endereco)
						{
							// Se for entrega, atualizamos o endereço para que não seja mais marcado como "para entrega"
							if ($endereco->entrega)
							{
								$endereco->entrega = 0;
								if ( ! $endereco->update())
								{
									new Log('Ocorreu um erro ao atualizar um endereço na exclusão de outro. $cliente_endereco:'.var_export($cliente_endereco, TRUE).'|$cliente_endereco_existem:'.var_export($cliente_endereco_existem, TRUE).'|$endereco:'.var_export($endereco, TRUE));
								}
							}
						}
					}
					else
					{
						// Não possui outro endereço
					}
				}

                $cliente = new FatorCMS_Model_Cliente($cliente_endereco->cliente_id);
                if ($cliente->verificar_cadastro_completo())
                {
                    //echo 'completo'; exit;
                    $cliente->cadastro_completo = 1;
                }
                else
                {
                    //echo 'incompleto'; exit;
                    $cliente->cadastro_completo = 0;
                }
                $cliente->update();

				new Notificacao('Endereço <strong>'.$cliente_endereco->endereco.'</strong> excluído com sucesso.', 'success', TRUE);
			}
			else
			{
				new Notificacao('Ocorreu um erro ao excluir o endereço do cliente.', 'error', TRUE);
			}
		}
		else
		{
			new Notificacao('Não foram encontradas informações sobre o endereço.', 'error', TRUE);
		}

		header('Location: '.SITE_URL.'/fatorcms/clientes/editar/'.$cliente_endereco->cliente_id.'/aba/2');
		exit;
	}


	/**
	 * Busca a lista de cidades a partir do ID de um estado e retorna via JSON, para uma chamada em AJAX
	 * @param $parametros
	 */
	public function buscar_cidades_estado($parametros)
	{
		$cidades_array = array();

		if (isset($parametros->estado_id) AND is_numeric($parametros->estado_id))
		{
			$cidade = new FatorCMS_Model_Cidade;
			$cidades = $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id = "'.Funcoes::mysqli_escape($parametros->estado_id).'"', TRUE);

			$cidades_array = array();
			if ($cidades AND count($cidades) > 0)
			{
				foreach ($cidades as $cidade)
				{
					$cidades_array[$cidade->id] = $cidade->nome;
				}
			}
		}

		echo json_encode($cidades_array);
	}
    //</editor-fold>


	/*************** MÉTODOS PARA A PARTE DAS COMPRAS ***************/


    //<editor-fold desc="Lógica para gerenciamento de compras">
    /**
	 * Retorna os dados da compra do cliente conforme ID passado por parâmetro, para edição no formulário
	 * Método chamado por requisição AJAX, por isso os echos json_encode
	 * @param  $parametros
	 * @return void
	 */
	public function compra_editar($parametros)
	{
		if (isset($parametros->_0) AND is_numeric($parametros->_0))
		{
			$compra = new FatorCMS_Model_Compra($parametros->_0);

			if ($compra AND ! is_null(($compra->id)))
			{
				// Transformamos o objeto da compra em um array para retonar via JSON
				$compra_dados = array();
				foreach ($compra->buscar_colunas() as $coluna=>$valor)
				{
					$compra_dados[$coluna] = $compra->$coluna;
				}

				//-----

				// Buscamos o endereço utilizado na compra
				$compra_endereco = new FatorCMS_Model_CompraEndereco;
				$compra_endereco = $compra_endereco->select('
					SELECT compra_endereco.*, cidade.nome AS cidade_nome, estado.sigla AS estado_sigla
					FROM {tabela_nome} AS compra_endereco
						LEFT JOIN {tabela_prefixo}cidades AS cidade ON compra_endereco.cidade_id = cidade.id
						LEFT JOIN {tabela_prefixo}estados AS estado ON compra_endereco.estado_id = estado.id
					WHERE compra_endereco.compra_id = "'.$compra->id.'"
				');
				$compra_endereco_dados = array();
				foreach ($compra_endereco->buscar_colunas() as $coluna=>$valor)
				{
					$compra_endereco_dados[$coluna] = $compra_endereco->$coluna;
				}
				// Colunas extras que não estão no Model
				$compra_endereco_dados['cidade_nome'] = $compra_endereco->cidade_nome;
				$compra_endereco_dados['estado_sigla'] = $compra_endereco->estado_sigla;

				//-----

				// Buscamos o produto escolhido na compra
				$compra_produto = new FatorCMS_Model_CompraProduto;
				$compra_produto = $compra_produto->select('
					SELECT compra_produto.*, produto.nome AS produto_nome
					FROM {tabela_nome} AS compra_produto
						LEFT JOIN {tabela_prefixo}produtos AS produto ON compra_produto.produto_id = produto.id
					WHERE compra_produto.compra_id = "'.$compra->id.'"
				');
				$compra_produto_dados = array();
				foreach ($compra_produto->buscar_colunas() as $coluna=>$valor)
				{
					$compra_produto_dados[$coluna] = $compra_produto->$coluna;
				}
				// Colunas extras que não estão no Model
				$compra_produto_dados['produto_nome'] = $compra_produto->produto_nome;

				//-----

                // Buscamos o cupom utilizado na compra
                $compra_cupom = new FatorCMS_Model_CompraCupom;
                $compra_cupom = $compra_cupom->select('
					SELECT compra_cupom.*, cupom.codigo AS cupom_codigo, produto.nome AS produto_nome
					FROM {tabela_nome} AS compra_cupom
						LEFT JOIN {tabela_prefixo}cupons AS cupom ON compra_cupom.cupom_id = cupom.id
						LEFT JOIN {tabela_prefixo}produtos AS produto ON cupom.produto_id = produto.id
					WHERE compra_cupom.compra_id = "'.$compra->id.'"
				');
                $compra_cupom_dados = array();
                // Se existir um cupom na compra, passamos as informações
                if ($compra_cupom AND ! is_null($compra_cupom->id))
                {
                    foreach ($compra_cupom->buscar_colunas() as $coluna=>$valor)
                    {
                        $compra_cupom_dados[$coluna] = $compra_cupom->$coluna;
                    }
                    // Colunas extras que não estão no Model
                    $compra_cupom_dados['cupom_codigo'] = $compra_cupom->cupom_codigo;
                    $compra_cupom_dados['produto_nome'] = $compra_cupom->produto_nome;
                }
                else
                {
                    $compra_cupom_dados['id'] = FALSE;
                }

				//-----

				echo json_encode(array(
					'tipo'=>'success',
					'cliente_compra'=>$compra_dados,
					'cliente_compra_endereco'=>$compra_endereco_dados,
					'cliente_compra_produto'=>$compra_produto_dados,
					'cliente_compra_cupom'=>$compra_cupom_dados
				));
			}
			else
			{
				echo json_encode(array('tipo'=>'error','mensagem'=>'Compra não encontrada.'));
			}
		}
		else
		{
			echo json_encode(array('tipo'=>'error','mensagem'=>'Identificador de compra inválido.'));
		}
	}


	/**
	 * Recebe os parâmetros do formulário e adiciona o registro no banco de dados
	 * Método chamado por requisição AJAX, por isso os echos json_encode
	 * @param $parametros
	 * @return void
	 */
	public function compra_cadastrar($parametros)
	{
		if ($parametros AND isset($parametros->data))
		{
			$compra = new FatorCMS_Model_Compra;
			$compra->carregar($parametros);

			// Campos especiais
			$compra->data = $parametros->data.' '.date('H:i:s');
            $compra->boleto_parcelas = ( ! empty($compra->boleto_parcelas) ? $compra->boleto_parcelas : 1);

            $compra->inicio = date('Y-m-d');
            $compra->fim = date('Y-m-d', mktime(0,0,0,date('m'),date('d'),date('Y')+1));

			$compra->colunas_mysqli_escape();

			if ($compra->verificar_null(array('id')))
			{
				// Validações ---

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($compra->insert())
					{
						// Criamos o código para a compra e atualizamos ela
						$compra_atualizar = new FatorCMS_Model_Compra;
						$compra_atualizar->id = $compra->id;
						$compra_atualizar->codigo = $compra->id + 1234; // Não mexer nesta soma, ela deve ser IGUAL à do site
						$compra_atualizar->update();
						$compra->codigo = $compra_atualizar->codigo;

						//-----

						// Cadastramos o endereço da compra

						$compra_endereco = new FatorCMS_Model_CompraEndereco();
						$compra_endereco->carregar(new FatorCMS_Model_ClienteEndereco($parametros->cliente_endereco_id));

						// Campos especiais
						$compra_endereco->id = NULL;
						$compra_endereco->compra_id = $compra->id;

						$compra_endereco->colunas_mysqli_escape();

						if ($compra_endereco->verificar_null(array('id')))
						{
							if ($compra_endereco->insert())
							{
								//-----

								// Cadastramos o produto da compra

								$compra_produto = new FatorCMS_Model_CompraProduto;
								$compra_produto->carregar(new FatorCMS_Model_Produto($parametros->produto_id));

								//-----

								// Campos especiais
								$compra_produto->compra_id = $compra->id;
								// Colocamos os valores nas colunas corretas após o carregamento
								$compra_produto->produto_id = $compra_produto->id;
								$compra_produto->id = NULL;

								// Definimos a versão do produto conforme especificado no formulário,
								// sobreescrevendo o que está salvo no banco de dados
								if (isset($parametros->produto_versao) AND ! empty($parametros->produto_versao))
								{
									$compra_produto->versao = $parametros->produto_versao;
								}

								// Definimos a quantidade de licenças com a compra do produto conforme especificado no formulário
								if (isset($parametros->produto_licencas) AND strlen($parametros->produto_licencas) > 0)
								{
									$compra_produto->licenca_quantidade = $parametros->produto_licencas;
								}
								else
								{
									$compra_produto->licenca_quantidade = 0;
								}

								//-----

								$compra_produto->colunas_mysqli_escape();

								if ($compra_produto->verificar_null(array('id')))
								{
									if ($compra_produto->insert())
									{
                                        //-----

                                        // Cadastramos o cupom da compra, se houver um

                                        if (isset($parametros->cliente_cupom_id) AND ! empty($parametros->cliente_cupom_id) AND $parametros->cliente_cupom_id != '-1')
                                        {
                                            $compra_cupom = new FatorCMS_Model_CompraCupom;
                                            $compra_cupom->carregar(new FatorCMS_Model_Cupom($parametros->cliente_cupom_id));
                                            // Campos especiais
                                            $compra_cupom->compra_id = $compra->id;
                                            // Colocamos os valores nas colunas corretas após o carregamento
                                            $compra_cupom->cupom_id = $compra_cupom->id;
                                            $compra_cupom->id = NULL;
                                            $compra_cupom->colunas_mysqli_escape();
                                            if ($compra_cupom->verificar_null(array('id')))
                                            {
                                                if ( ! $compra_cupom->insert())
                                                {
                                                    echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao cadastrar os dados do cupom da compra do cliente.'));
                                                }
                                            }
                                        }

                                        //-----

										new Notificacao('Compra de código <strong>'.$compra->codigo.'</strong> cadastrada com sucesso.', 'success', TRUE);
										echo json_encode(array('tipo'=>'success'));
									}
									else
									{
										echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao cadastrar os dados do produto da compra do cliente.'));
									}
								}
								else
								{
									echo json_encode(array('tipo'=>'error','mensagem'=>'As informações sobre o produto da compra estão incompletas.'));
								}
							}
							else
							{
								echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao cadastrar o endereço da compra do cliente.'));
							}
						}
						else
						{
							echo json_encode(array('tipo'=>'error','mensagem'=>'As informações sobre o endereço da compra estão incompletas.'));
						}
					}
					else
					{
						echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao cadastrar os dados da compra do cliente.'));
					}
				}
			}
			else
			{
				echo json_encode(array('tipo'=>'error','mensagem'=>'Todos campos marcados são obrigatórios.'));
			}
		}
		else
		{
			echo json_encode(array('tipo'=>'error','mensagem'=>'Nenhuma informação encontrada.'));
		}
	}


	/**
	 * Recebe os parâmetros do formulário e atualiza o registro no banco de dados
	 * Método chamado por requisição AJAX, por isso os echos json_encode
	 * @param  $parametros
	 * @return void
	 */
	public function compra_atualizar($parametros)
	{
		if ($parametros AND isset($parametros->id))
		{
			$compra = new FatorCMS_Model_Compra;
			$compra->carregar($parametros);

			// Campos especiais
			$compra->data = $parametros->data.' '.date('H:i:s');
            $compra->boleto_parcelas = ( ! empty($compra->boleto_parcelas) ? $compra->boleto_parcelas : 1);

			$compra->colunas_mysqli_escape();

			if ($compra->verificar_null())
			{
				// Validações ---

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($compra->update())
					{
						//-----

						// Se for o caso, atualizamos o endereço da compra

						if (isset($parametros->cliente_endereco_id) AND ! empty($parametros->cliente_endereco_id))
						{
							// Apagamos o registro atual
							$compra_endereco_atual = new FatorCMS_Model_CompraEndereco;
							$compra_endereco_atual->compra_id = $compra->id;
							$compra_endereco_atual->delete();

							// E criamos um novo
							$compra_endereco_novo = new FatorCMS_Model_CompraEndereco;
							$compra_endereco_novo->carregar(new FatorCMS_Model_ClienteEndereco($parametros->cliente_endereco_id));

							// Campos especiais
							$compra_endereco_novo->id = NULL;
							$compra_endereco_novo->compra_id = $compra->id;

							$compra_endereco_novo->colunas_mysqli_escape();

							if ($compra_endereco_novo->verificar_null(array('id')))
							{
								if ( ! $compra_endereco_novo->insert())
								{
									echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao cadastrar o endereço da compra do cliente.'));
									exit;
								}
							}
							else
							{
								echo json_encode(array('tipo'=>'error','mensagem'=>'As informações sobre o endereço da compra estão incompletas.'));
								exit;
							}
						}

						//-----

						// Se for o caso, atualizamos o produto da compra

						if (isset($parametros->produto_id) AND ! empty($parametros->produto_id))
						{
							// Apagamos o registro atual
							$compra_produto_atual = new FatorCMS_Model_CompraProduto;
							$compra_produto_atual->compra_id = $compra->id;
							$compra_produto_atual->delete();

							// E criamos um novo
							$compra_produto_novo = new FatorCMS_Model_CompraProduto;
							$compra_produto_novo->carregar(new FatorCMS_Model_Produto($parametros->produto_id));

							//-----

							// Campos especiais
							$compra_produto_novo->compra_id = $compra->id;
							// Colocamos os valores nas colunas corretas após o carregamento
							$compra_produto_novo->produto_id = $compra_produto_novo->id;
							$compra_produto_novo->id = NULL;

							// Definimos a versão do produto conforme especificado no formulário,
							// sobreescrevendo o que está salvo no banco de dados
							if (isset($parametros->produto_versao) AND ! empty($parametros->produto_versao))
							{
								$compra_produto_novo->versao = $parametros->produto_versao;
							}

							// Definimos a quantidade de licenças com a compra do produto conforme especificado no formulário
							if (isset($parametros->produto_licencas) AND strlen($parametros->produto_licencas) > 0)
							{
								$compra_produto_novo->licenca_quantidade = $parametros->produto_licencas;
							}
							else
							{
								$compra_produto_novo->licenca_quantidade = 0;
							}

							//-----

							$compra_produto_novo->colunas_mysqli_escape();

							if ($compra_produto_novo->verificar_null(array('id')))
							{
								if ( ! $compra_produto_novo->insert())
								{
									echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao cadastrar os dados do produto da compra do cliente.'));
                                    exit;
								}
							}
							else
							{
								echo json_encode(array('tipo'=>'error','mensagem'=>'As informações sobre o produto da compra estão incompletas.'));
                                exit;
							}
						}

						//-----

                        // Se for o caso, atualizamos o cupom da compra ou excluímos ele

                        if (isset($parametros->cliente_cupom_id) AND ! empty($parametros->cliente_cupom_id))
                        {
                            // Apagamos o registro atual
                            $compra_cupom_atual = new FatorCMS_Model_CompraCupom;
                            $compra_cupom_atual->compra_id = $compra->id;
                            $compra_cupom_atual->delete();

                            // Se é diferente de vazio e de -1 (que serve para a exclusão do atual), inserimos o novo cupom
                            if ($parametros->cliente_cupom_id != '-1')
                            {
                                $compra_cupom = new FatorCMS_Model_CompraCupom;
                                $compra_cupom->carregar(new FatorCMS_Model_Cupom($parametros->cliente_cupom_id));
                                // Campos especiais
                                $compra_cupom->compra_id = $compra->id;
                                // Colocamos os valores nas colunas corretas após o carregamento
                                $compra_cupom->cupom_id = $compra_cupom->id;
                                $compra_cupom->id = NULL;
                                $compra_cupom->colunas_mysqli_escape();
                                if ($compra_cupom->verificar_null(array('id')))
                                {
                                    if ( ! $compra_cupom->insert())
                                    {
                                        echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao cadastrar os dados do cupom da compra do cliente.'));
                                        exit;
                                    }
                                }
                                else
                                {
                                    echo json_encode(array('tipo'=>'error','mensagem'=>'As informações sobre o cupom da compra estão incompletas.'));
                                    exit;
                                }
                            }
                        }


						//-----

						// Se chegou aqui, deu tudo certo

                        // Aqui fazemos o cálculo do código da compra para poupar um acesso ao Banco de Dados. CUIDADO!!
                        $compra_codigo = $compra->id + 1234;

						new Notificacao('Compra de código <strong>'.$compra_codigo.'</strong> atualizada com sucesso.', 'success', TRUE);
						echo json_encode(array('tipo'=>'success'));
					}
					else
					{
						echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao atualizar os dados da compra do cliente.'));
					}
				}
			}
			else
			{
				echo json_encode(array('tipo'=>'error','mensagem'=>'Todos campos marcados são obrigatórios.'));
			}
		}
		else
		{
			echo json_encode(array('tipo'=>'error','mensagem'=>'Nenhuma informação encontrada.'));
		}
	}


	/**
	 * Exclui uma compra de um cliente
	 * @param $parametros
	 */
	/*public function compra_excluir($parametros)
	{
		if (isset($parametros->_0) AND is_numeric($parametros->_0))
		{
			$cliente_endereco = new FatorCMS_Model_ClienteEndereco($parametros->_0);

			if ($cliente_endereco->delete())
			{
				new Notificacao('Endereço <strong>'.$cliente_endereco->endereco.'</strong> excluído com sucesso.', 'success', TRUE);
			}
			else
			{
				new Notificacao('Ocorreu um erro ao excluir o endereço do cliente.', 'error', TRUE);
			}
		}
		else
		{
			new Notificacao('Não foram encontradas informações sobre o endereço.', 'error', TRUE);
		}

		header('Location: '.SITE_URL.'/fatorcms/clientes/editar/'.$cliente_endereco->cliente_id.'/aba/2');
		exit;
	}*/
    //</editor-fold>


    /*************** MÉTODOS PARA A PARTE DAS INTERAÇÕES ***************/


    //<editor-fold desc="Lógica de gerenciamento de interaçoes">
    /**
     * Retorna os dados do endereço do cliente conforme ID passado por parâmetro, para edição no formulário
     * Método chamado por requisição AJAX, por isso os echos json_encode
     * @param  $parametros
     * @return void
     */
    public function interacao_editar($parametros)
    {
        if (isset($parametros->_0) AND is_numeric($parametros->_0))
        {
            $interacao = new FatorCMS_Model_Interacao($parametros->_0);

            if ($interacao AND ! is_null(($interacao->id)))
            {
                // Somente um administrador ou o próprio autor da interação podem editá-la
                if ($this->cms_usuario_logado->eh_administrador OR $interacao->cms_usuario_id == $this->cms_usuario_logado->id)
                {
                    $interacao_dados = array();
                    foreach ($interacao->buscar_colunas() as $coluna=>$valor)
                    {
                        $interacao_dados[$coluna] = $interacao->$coluna;
                    }
                    echo json_encode(array('tipo'=>'success', 'cliente_interacao'=>$interacao_dados));
                }
                else
                {
                    echo json_encode(array('tipo'=>'error','mensagem'=>'Você não possui permissão para editar esta interação.'));
                }
            }
            else
            {
                echo json_encode(array('tipo'=>'error','mensagem'=>'Interação não encontrado.'));
            }
        }
        else
        {
            echo json_encode(array('tipo'=>'error','mensagem'=>'Identificador de interação inválido.'));
        }
    }


    /**
     * Recebe os parâmetros do formulário e adiciona o registro no banco de dados
     * Método chamado por requisição AJAX, por isso os echos json_encode
     * @param  $parametros
     * @return void
     */
    public function interacao_cadastrar($parametros)
    {
        if ($parametros AND isset($parametros->conteudo))
        {
            $interacao = new FatorCMS_Model_Interacao;
            $interacao->carregar($parametros);

            // Campos especiais
            $interacao->data = date('d/m/Y H:i:s');
            $interacao->cms_usuario_id = $this->cms_usuario_logado->id;

            //-----

            $interacao->colunas_mysqli_escape();

            if ($interacao->verificar_null(array('id')))
            {
                // Validações ---

                // Fim das validações ---

                if ( ! isset($notificacao))
                {
                    if ($interacao->insert())
                    {
	                    // Se a opção de enviar email para o cliente vier marcada, enviamos o email
	                    if ($parametros->enviar_cliente AND $parametros->enviar_cliente == '1')
	                    {
		                    $cliente = new FatorCMS_Model_Cliente;
		                    $cliente = $cliente->select('SELECT id, nome, email, responsavel_nome FROM {tabela_nome} WHERE id = "'.$interacao->cliente_id.'"');
		                    if ($cliente AND ! is_null($cliente->id))
		                    {
			                    $email = new Controller_Email;
			                    if ( ! $email->interacao_cliente($interacao, $cliente))
			                    {
				                    new Log('Erro ao enviar a interação para o cliente. $interacao:'.var_export($interacao, TRUE).'|$cliente:'.var_export($cliente, TRUE));
			                    }
		                    }
		                    else
		                    {
			                    new Log('Cliente não encontrado para enviar email sobre a interação. $interacao_id:'.var_export($interacao->id, TRUE).'|$cliente_id:'.var_export($interacao->cliente_id, TRUE));
		                    }
	                    }

                        new Notificacao('Interação de código <strong>'.$interacao->id.'</strong> cadastrada com sucesso.', 'success', TRUE);
                        echo json_encode(array('tipo'=>'success'));
                    }
                    else
                    {
                        echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao cadastrar a interação com o cliente.'));
                    }
                }
            }
            else
            {
                echo json_encode(array('tipo'=>'error','mensagem'=>'Todos campos marcados são obrigatórios.'));
            }
        }
        else
        {
            echo json_encode(array('tipo'=>'error','mensagem'=>'Nenhuma informação encontrada.'));
        }
    }


    /**
     * Recebe os parâmetros do formulário e atualiza o registro no banco de dados
     * Método chamado por requisição AJAX, por isso os echos json_encode
     * @param  $parametros
     * @return void
     */
    public function interacao_atualizar($parametros)
    {
        if ($parametros AND isset($parametros->conteudo))
        {
            $interacao = new FatorCMS_Model_Interacao;
            $interacao->carregar($parametros);

            // Campos especiais
            //$interacao->data = date('d/m/Y H:i:s');
            $interacao->cms_usuario_id = $this->cms_usuario_logado->id;

            //-----

            $interacao->colunas_mysqli_escape();

            if ($interacao->verificar_null(array('data')))
            {
                // Validações ---

                // Fim das validações ---

                if ( ! isset($notificacao))
                {
                    if ($interacao->update())
                    {
	                    // Se a opção de enviar email para o cliente vier marcada, enviamos o email
	                    if ($parametros->enviar_cliente AND $parametros->enviar_cliente == '1')
	                    {
		                    $cliente = new FatorCMS_Model_Cliente;
		                    $cliente = $cliente->select('SELECT id, nome, email, responsavel_nome FROM {tabela_nome} WHERE id = "'.$interacao->cliente_id.'"');
		                    if ($cliente AND ! is_null($cliente->id))
		                    {
			                    // Recarregamos o objeto para ter todas as informações
			                    $interacao = new FatorCMS_Model_Interacao($interacao->id);

			                    $email = new Controller_Email;
			                    if ( ! $email->interacao_cliente($interacao, $cliente, TRUE))
			                    {
				                    new Log('Erro ao enviar a interação para o cliente. $interacao:'.var_export($interacao, TRUE).'|$cliente:'.var_export($cliente, TRUE));
			                    }
		                    }
		                    else
		                    {
			                    new Log('Cliente não encontrado para enviar email sobre a interação. $interacao_id:'.var_export($interacao->id, TRUE).'|$cliente_id:'.var_export($interacao->cliente_id, TRUE));
		                    }
	                    }

                        new Notificacao('Interação de código <strong>'.$interacao->id.'</strong> atualizada com sucesso.', 'success', TRUE);
                        echo json_encode(array('tipo'=>'success'));
                    }
                    else
                    {
                        echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao atualizar a interação com o cliente.'));
                    }
                }
            }
            else
            {
                echo json_encode(array('tipo'=>'error','mensagem'=>'Todos campos marcados são obrigatórios.'));
            }
        }
        else
        {
            echo json_encode(array('tipo'=>'error','mensagem'=>'Nenhuma informação encontrada.'));
        }
    }


    /**
     * Exclui um endereço de um cliente
     * @param $parametros
     */
    public function interacao_excluir($parametros)
    {
        if (isset($parametros->_0) AND is_numeric($parametros->_0))
        {
            $interacao = new FatorCMS_Model_Interacao($parametros->_0);

            if ($this->cms_usuario_logado->eh_administrador)
            {
                if ($interacao->delete())
                {
                    new Notificacao('Interação de código <strong>'.$interacao->id.'</strong> excluída com sucesso.', 'success', TRUE);
                }
                else
                {
                    new Notificacao('Ocorreu um erro ao excluir a interação do cliente.', 'error', TRUE);
                }
            }
            else
            {
                new Notificacao('Você não possui permissão para excluir uma interação.', 'error', TRUE);
            }

            header('Location: '.SITE_URL.'/fatorcms/clientes/editar/'.$interacao->cliente_id.'/aba/4');
            exit;
        }
        else
        {
            new Notificacao('Não foram encontradas informações sobre a interação.', 'error', TRUE);
        }

        header('Location: '.SITE_URL.'/fatorcms/clientes/listar');
        exit;

    }
    //</editor-fold>


	/****************************** MÉTODOS EXTRAS ******************************/



	/**
	 * Retorna todos os estados cadastrados no banco, geralmente para completar <select>s
	 * @return array|bool
	 */
	protected function carregar_estados()
	{
		$estado = new FatorCMS_Model_Estado;
		return $estado->select('SELECT * FROM {tabela_nome} ORDER BY nome', TRUE);
	}


	/**
	 * Retorna todos os estados cadastrados no banco, geralmente para completar <select>s
	 * @param int $estado_id
	 * @return array|bool
	 */
	protected function carregar_cidades($estado_id = NULL)
	{
		$cidade = new FatorCMS_Model_Cidade;
		return $cidade->select('SELECT * FROM {tabela_nome} '.( ! is_null($estado_id) ? 'WHERE estado_id='.$estado_id : '').' ORDER BY nome', TRUE);
	}


	/**
	 * Retorna todos os produtos cadastrados no banco, geralmente para completar <select>s
	 * @return array|bool
	 */
	protected function carregar_produtos()
	{
		$produto = new FatorCMS_Model_Produto;
		return $produto->select('SELECT id, nome, versao FROM {tabela_nome} ORDER BY nome', TRUE);
	}


	/**
	 * Retorna todos os cupons cadastrados no banco, geralmente para completar <select>s
	 * @return array|bool
	 */
	protected function carregar_cupons()
	{
		$cupom = new FatorCMS_Model_Cupom;
		return $cupom->select('
		    SELECT cupom.id, cupom.codigo, cupom.valor, produto.nome AS produto_nome
		    FROM {tabela_nome} AS cupom
		        LEFT JOIN {tabela_prefixo}produtos AS produto ON cupom.produto_id = produto.id
		    ORDER BY cupom.codigo'
        , TRUE);
	}


    /**
     * Retorna todos os endereços cadastrados de um cliente específico
     * @param null $cliente_id
     * @return array|bool
     */
    protected function carregar_cliente_enderecos($cliente_id = NULL)
    {
        if ( ! is_null($cliente_id))
        {
            $endereco = new FatorCMS_Model_ClienteEndereco;
            return $endereco->select('
                SELECT endereco.*, cidade.nome AS cidade_nome, estado.sigla AS estado_sigla
                FROM {tabela_nome} AS endereco
                    LEFT JOIN {tabela_prefixo}cidades AS cidade ON endereco.cidade_id = cidade.id
                    LEFT JOIN {tabela_prefixo}estados AS estado ON endereco.estado_id = estado.id
                WHERE endereco.cliente_id = "'.$cliente_id.'"'
            , TRUE);
        }

        return FALSE;
    }


    /**
     * Retorna todas as compras cadastrados de um cliente específico
     * @param null $cliente_id
     * @return array|bool
     */
    protected function carregar_cliente_compras($cliente_id = NULL)
    {
        if ( ! is_null($cliente_id))
        {
            $compra = new FatorCMS_Model_Compra;
            return  $compra->select('
	            SELECT compra.*, (compra_produto.valor + (compra_produto.licenca_quantidade * compra_produto.licenca_valor) - IFNULL(compra_cupom.valor, 0)) AS valor_total,
	                compra_produto.licenca_quantidade, produto.id AS produto_id, produto.nome AS produto_nome,
	                pagseguro_transacao_status.status AS transacao_status
	            FROM {tabela_nome} AS compra
	                LEFT JOIN {tabela_prefixo}compras_produtos AS compra_produto ON compra.id = compra_produto.compra_id
	                LEFT JOIN {tabela_prefixo}produtos AS produto ON compra_produto.produto_id = produto.id
	                LEFT JOIN {tabela_prefixo}compras_cupons AS compra_cupom ON compra.id = compra_cupom.compra_id
	                LEFT JOIN {tabela_prefixo}pagseguro AS pagseguro ON compra.id = pagseguro.compra_id
					LEFT JOIN {tabela_prefixo}pagseguro_transacoes_status AS pagseguro_transacao_status ON pagseguro.transacao_status_id = pagseguro_transacao_status.id
	            WHERE compra.cliente_id = "'.$cliente_id.'"
	            GROUP BY compra.id
		    ', TRUE);
        }

        return FALSE;
    }


    /**
     * Retorna todas as interaões cadastrados de um cliente específico
     * @param null $cliente_id
     * @return array|bool
     */
    protected function carregar_cliente_interacoes($cliente_id = NULL)
    {
        if ( ! is_null($cliente_id))
        {
            $interacao = new FatorCMS_Model_Interacao;
            return $interacao->select('
	            SELECT interacao.*, cms_usuario.nome AS cms_usuario_nome
	            FROM {tabela_nome} AS interacao
	                LEFT JOIN {tabela_prefixo}cms_usuarios AS cms_usuario ON interacao.cms_usuario_id = cms_usuario.id
	            WHERE cliente_id = "'.$cliente_id.'"
	            ORDER BY interacao.data DESC
	        ', TRUE);
        }

        return FALSE;
    }


    public function atualizar_plano_assistencia($parametros)
    {

        //print_r($parametros); exit;
        if ($parametros AND isset($parametros->id))
        {

            $compra = new FatorCMS_Model_Compra($parametros->id);


            $compra->inicio = $parametros->inicio;
            $compra->fim = $parametros->fim;
            $compra->data = $parametros->data.' '.date('H:i:s');
            $compra->informacao_adicional = $parametros->informacao_adicional;

            $compra->colunas_mysqli_escape();

            if ($compra->verificar_null())
            {
                // Validações ---

                // Fim das validações ---

                if ( ! isset($notificacao))
                {
                    if ($compra->update())
                    {
                        //-----

                        //echo $compra->id; exit;

                        $compra_produto = new FatorCMS_Model_CompraProduto();
                        $compra_produto = $compra_produto->select('SELECT * FROM {tabela_nome} WHERE compra_id = '.$compra->id);

                        $compra_produto_aux = new FatorCMS_Model_CompraProduto();
                        $compra_produto_aux->licenca_valor = $compra_produto->licenca_valor;
                        $compra_produto_aux->produto_id = $compra_produto->produto_id;
                        $compra_produto_aux->compra_id = $compra_produto->compra_id;
                        $compra_produto_aux->id = $compra_produto->id;
                        $compra_produto_aux->licenca_quantidade = !empty($parametros->licenca_quantidade) ? $parametros->licenca_quantidade : $compra_produto->licenca_quantidade;
                        $compra_produto_aux->valor = $compra_produto->valor;
                        $compra_produto_aux->versao = $compra_produto->versao;

                        $compra_produto_aux->colunas_mysqli_escape();

                        //print_r($compra_produto_aux); exit;

                        if (!$compra_produto_aux->update())
                        {
                            echo json_encode(array('tipo'=>'error','mensagem'=>'1Ocorreu um erro ao atualizar os dados do plano de assistência.')); exit;
                        }


                        //-----

                        // Se chegou aqui, deu tudo certo

                        new Notificacao('Plano de assistência atualizado com sucesso.', 'success', TRUE);
                        echo json_encode(array('tipo'=>'success'));
                    }
                    else
                    {
                        echo json_encode(array('tipo'=>'error','mensagem'=>'2Ocorreu um erro ao atualizar os dados do plano de assistência.'));
                    }
                }
            }
            else
            {
                echo json_encode(array('tipo'=>'error','mensagem'=>'Todos campos marcados são obrigatórios.'));
            }
        }
        else
        {
            echo json_encode(array('tipo'=>'error','mensagem'=>'Nenhuma informação encontrada.'));
        }
    }

    public function atualizar_recorrencia($parametros)
    {

        //print_r($parametros); die;
        if ($parametros AND isset($parametros->cliente_id))
        {
        	// Verificamos se já é um registro existente
        	if($parametros->id!=''){

        		$recorrencia = new FatorCMS_Model_ClienteRecorrencia($parametros->id);

        		$recorrencia->versao_produto = $parametros->versao_produto;
        		$recorrencia->data_expiracao = $parametros->data_expiracao;
        		$recorrencia->licenca = $parametros->licenca;

        		if($recorrencia->update()){
        			new Notificacao('Recorrência atualizada com sucesso.', 'success', TRUE);
                    echo json_encode(array('tipo'=>'success'));
        		}else{
        			echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao atualizar a recorrência.'));
        		}

        	}else{

        		// Inserimos o registro
        		$recorrencia = new FatorCMS_Model_ClienteRecorrencia;
        		$recorrencia->cliente_id = $parametros->cliente_id;
        		$recorrencia->versao_produto = $parametros->versao_produto;
        		$recorrencia->data_expiracao = $parametros->data_expiracao;
        		$recorrencia->licenca = $parametros->licenca;

        		if($recorrencia->insert()){
        			new Notificacao('Recorrência atualizada com sucesso.', 'success', TRUE);
                    echo json_encode(array('tipo'=>'success'));
        		}else{
        			echo json_encode(array('tipo'=>'error','mensagem'=>'Ocorreu um erro ao atualizar a recorrência.'));
        		}      		
        	}
        	
        }
        else
        {
            echo json_encode(array('tipo'=>'error','mensagem'=>'Nenhuma informação encontrada.'));
        }
    }


	/**
	 * Retorna todas as permissões para módulos de determinado usuário
	 * @param $cms_usuario_id
	 * @return array|bool
	 */
	/*protected function carregar_usuario_permissoes($cms_usuario_id)
	{
		if ( ! is_null($cms_usuario_id) AND is_numeric($cms_usuario_id))
		{
			$cms_permissao = new FatorCMS_Model_CMSPermissao;
			return $cms_permissao->select('SELECT * FROM {tabela_nome} WHERE cms_usuario_id = '.$cms_usuario_id, TRUE);
		}
		else
		{
			return FALSE;
		}
	}*/


} // end class