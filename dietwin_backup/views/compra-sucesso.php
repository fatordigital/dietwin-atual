<?php require_once 'includes/head.php' ?>
<body>
<?php require_once 'includes/nav.php' ?>

<section class="conteudo cinza">
  <div class="container">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <h1 class="interna">Confirmação de pedido </h1>
    </div>
    <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
      <img src="<?php echo SITE_URL ?>/views/imagens/caixas-dietwin-cadastre-se.jpg" alt="dietWin Plus e dietWin Tradicional">
    </div>
    <section class="formulario">
      <div class="col-lg-6 col-md-6-col-sm-12 col-xs-12">
        <div class="compra-sucesso">
          <img src="<?php echo SITE_URL ?>/views/imagens/botao-sucesso.png" alt="Compra efetuada com sucesso">
          Seu pedido foi finalizado com sucesso!
        </div>
        <p>O número do seu pedido é: <b class="maior"><?php echo $compra->codigo; ?></b><br />
        Este número e os demais dados do seu pedido foram enviados para o seu e-mail.</p>
        <p>Em caso de dúvida, entre em contato pelo e-mail <a href="mailto:suporte@dietwin.com.br" title="Envie um e-mail para suporte@dietwin.com.br">suporte@dietwin.com.br</a> ou pelo telefone <b class="maior azul">(51) 3337.7908.</b></p>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="titulo-formulario">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>Dados de sua compra</h2>
          </div>
        </div>
        <div class="endereco-entrega">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="lista-produtos">
                        <ol>
                            <li><?php echo $produto->nome; ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="formulario-cadastro total-venda">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right text-center-sm text-center-xs">
              Total de sua compra:
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-right">
              <div class="preco">
                <span class="moeda">R$</span><span class="reais"><?php echo number_format($produto->valor+$frete, 2, ',', '.'); ?></span><span class="centavos"></span>
              </div>
            </div>
          </div>
        </div>
        <div class="endereco-entrega">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="seus-dados">
                    <b class="maior">Seus dados</b>
                    <p>Nome: <?php echo $cliente->nome; ?><br />
                    Endereço: <?php echo $compra_endereco->endereco; ?> - <?php $compra_endereco->numero; ?><?php if($compra_endereco->complemento!=''){ echo " / ".$compra_endereco->complemento; } ?><br />
                    CEP: <?php echo $compra_endereco->cep; ?><br />

                    <p>Telefone: <?php echo $cliente->telefone_principal; ?></p>
                  </div>
                </div>
            </div>
        </div>
      </div>
    </section>
  </div>
</section>

<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/951916294/?value=<?php echo $produto->valor; ?>&amp;currency_code=BRL&amp;label=l52mCKmUoVkQhq70xQM&amp;guid=ON&amp;script=0"/>

<?php require_once 'includes/footer.php' ?>
</body>
</html>
