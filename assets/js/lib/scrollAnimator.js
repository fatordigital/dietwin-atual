/*
 ################################################################
 # ScrollAnimator - Jquery-Plugin-File overall for all pages
 # Author: 		Matthias Rosenthal
 # Website:		http://www.matthiasrosenthal.de
 # Agency:		http://www.bplusd-interactive.de
 # Version 1.0:	2013/06/04
 ################################################################
 */
(function($) {
	$.fn.scrollAnimation = function(options) {
		//set up default options
		var defaults = {
			xPos : 0,//Horizontale Verschiebung des Elements in px
			yPos : 0, //Vertikale Verschiebung des Elements in px
			rotation : 0,//Rotationsverschiebung in Grad
			alpha : 0,//Einblend-Alpha-Wert von 0-100
			easingType : Cubic.easeOut, //Animations-Typ
			animationDuration : 0.5 //Animationszeit
		};

		//vars
		var opts = $.extend({}, defaults, options);
		var currentID = "";

		//hide every item
		return this.each(function() {
			var target = this;
			//set preset position
			TweenMax.to($(target), 0, {
				css : {
					opacity : opts.alpha,
					marginLeft : opts.xPos + "px",
					marginTop : opts.yPos + "px",
					rotation : opts.rotation,
				}
			});
			//add element to object array
			objectArray.push(target);
			optionsArray.push(opts);
		});
	};
})(jQuery);

//function check offset-positions
function checkOffsetPosition() {
	$(objectArray).each(function(index, target) {
		var targetObject = target;
		var opts = optionsArray[index];

		var objectOffsetTop = $(targetObject).offset().top;
		var objectHeight = $(targetObject).height();

		var windowOffsetTop = $(window).scrollTop();
		var windowHeight = $(window).height();

		var middleObjectPosition = objectOffsetTop + objectHeight / 2;
		var middleWindowPosition = windowOffsetTop + windowHeight / 2;

		var showGap = 280;

		if(middleWindowPosition < middleObjectPosition + showGap * 2.5 && middleWindowPosition > middleObjectPosition - showGap) {
			//animate and show target object
			TweenMax.to($(targetObject), opts.animationDuration, {
				css : {
					opacity : 1,
					marginLeft : "0px",
					marginTop : "0px",
					rotation : 0,
				},
				ease : opts.easingType,
				onComplete : function() {
				}
			});
		} else {
			//animate and hide target object
			TweenMax.to($(targetObject), opts.animationDuration, {
				css : {
					opacity : opts.alpha,
					marginLeft : opts.xPos + "px",
					marginTop : opts.yPos + "px",
					rotation : opts.rotation,
				},
				ease : opts.easingType,
				onComplete : function() {
				}
			});
		}
	});
}

//init scroll-event only once for better performance -> save target-data first in arrays
var objectArray = new Array();
var optionsArray = new Array();
$(window).scroll(function() {
	checkOffsetPosition();
});
