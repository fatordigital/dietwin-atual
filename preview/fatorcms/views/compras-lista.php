<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php' ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

		<!-- Page Head -->
		<h2>Lista de Compras</h2>
		<p id="page-intro">Abaixo estão listadas todas as compras feitas pelos clientes.</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<form action="<?php echo SITE_URL ?>/fatorcms/compras/listar" method="get" id="compras_buscar_form">
			<fieldset>
				<p>
					<label for="buscar">Digite uma ou mais palavras-chave para buscar por uma compra cadastrada:</label>
					<input class="text-input medium-input" type="text" id="buscar" name="buscar" maxlength="100" value="<?php echo isset($buscar) ? $buscar : '' ?>" />
					<input class="button" type="submit" value="Buscar" />
				</p>
			</fieldset>
			<div class="clear"></div><!-- End .clear -->
		</form>

		<?php if (isset($buscar) AND ! is_null($buscar)) { ?>
			<p class="buscar-resultado">
				Foram encontrados <strong><?php echo $paginacao->linhas_total ?></strong> resultado(s) para a busca por "<strong><?php echo $buscar ?></strong>".
				<a title="Limpar a busca" href="<?php echo SITE_URL?>/fatorcms/compras/listar"><img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier-cross.png" alt="Limpar busca" /> Clique aqui para limpar a busca</a>
			</p>
		<?php } ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Compras</h3>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<table>

					<thead>
						<tr>
							<?php echo Funcoes::montar_th_ordenacao_listagem('compras', 'listar', $paginacao, $buscar, 'Data', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('compras', 'listar', $paginacao, $buscar, 'Código', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('compras', 'listar', $paginacao, $buscar, 'Cliente', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('compras', 'listar', $paginacao, $buscar, 'Produto', $ordenar_por, $ordem) ?>
                            <?php echo Funcoes::montar_th_ordenacao_listagem('compras', 'listar', $paginacao, $buscar, 'Valor total', $ordenar_por, $ordem) ?>
                            <?php echo Funcoes::montar_th_ordenacao_listagem('compras', 'listar', $paginacao, $buscar, 'Mét. Pagamento', $ordenar_por, $ordem) ?>
                            <th>Ações</th>
						</tr>
					</thead>

					<tfoot>
						<tr>
							<td colspan="8">

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_quantidades() ?>

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_links() ?>

								<div class="clear"></div>
							</td>
						</tr>
					</tfoot>

					<tbody>
						<?php
                        if($compras AND count($compras) > 0)
						{
							foreach ($compras as $compra)
							{
                                if($compra->situacao == 'Cancelada'){
							?>
                            <tr>
                                    <td style="color: #c8c8c8;" class="<?php echo $ordenar_por=='data' ? 'current' : '' ?>"><?php echo date('d/m/Y \à\s H:i:s', strtotime($compra->data)) ?></style></td>
                                    <td style="color: #c8c8c8;" class="<?php echo $ordenar_por=='codigo' ? 'current' : '' ?>">
                                        <a style="color: #c8c8c8;" href="<?php echo SITE_URL ?>/fatorcms/compras/visualizar/<?php echo $compra->id ?>" title="Visualizar a compra de código &quot;<?php echo $compra->codigo ?>&quot;">
                                            <?php echo $compra->codigo ?>
                                        </a>
                                    </td>
                                    <td style="color: #c8c8c8;" class="<?php echo $ordenar_por=='cliente' ? 'current' : '' ?>">
                                        <a style="color: #c8c8c8;" href="<?php echo SITE_URL ?>/fatorcms/clientes/editar/<?php echo $compra->cliente_id ?>" title="Editar o cliente &quot;<?php echo $compra->cliente_nome ?>&quot;">
                                            <?php echo $compra->cliente_nome ?>
                                        </a>
                                    </td>
                                    <td style="color: #c8c8c8;" class="<?php echo $ordenar_por=='produto' ? 'current' : '' ?>" title="O número entre parênteses representa a quantidade de licenças adquiridas">
                                        <a style="color: #c8c8c8;" href="<?php echo SITE_URL ?>/fatorcms/produtos/editar/<?php echo $compra->produto_id ?>" title="Editar o produto &quot;<?php echo $compra->produto_nome ?>&quot;">
                                            <?php echo $compra->produto_nome ?>
                                        </a>
                                        <?php //echo ' ('.$compra->produto_licenca_quantidade.')' ?>
                                    </td>
                                    <td style="color: #c8c8c8;" class="<?php echo $ordenar_por=='valor-total' ? 'current' : '' ?>">
                                    	<?php $valor_cupom = (!is_null($compra->compra_cupom_valor)) ? $compra->compra_cupom_valor : 0; ?>
                                    	<?php $valor_total = $compra->compra_produto_valor + ($compra->compra_produto_licenca_valor * $compra->produto_licenca_quantidade) + $compra->compra_entrega_valor - $valor_cupom; ?>
                                    	R$ <?php echo number_format($valor_total, 2, ',', '.')?>
                                    </td>
                                    <td style="color: #c8c8c8;" class="<?php echo $ordenar_por=='met-pagamento' ? 'current' : '' ?>"><?php echo $compra->pagamento_metodo ?></td>
                                    <td>
                                        <a style="color: #c8c8c8;" href="<?php echo SITE_URL ?>/fatorcms/compras/visualizar/<?php echo $compra->id ?>" title="Visualizar a compra de código &quot;<?php echo $compra->codigo ?>&quot;">
                                            <img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier.png" alt="Visualizar" />
                                        </a>
                                    </td>
                            </tr>
                                <?php }else{ ?>
                            <tr>
                                <td class="<?php echo $ordenar_por=='data' ? 'current' : '' ?>"><?php echo date('d/m/Y \à\s H:i:s', strtotime($compra->data)) ?></td>
                                <td class="<?php echo $ordenar_por=='codigo' ? 'current' : '' ?>">
                                    <a href="<?php echo SITE_URL ?>/fatorcms/compras/visualizar/<?php echo $compra->id ?>" title="Visualizar a compra de código &quot;<?php echo $compra->codigo ?>&quot;">
                                        <?php echo $compra->codigo ?>
                                    </a>
                                </td>
                                <td class="<?php echo $ordenar_por=='cliente' ? 'current' : '' ?>">
                                    <a href="<?php echo SITE_URL ?>/fatorcms/clientes/editar/<?php echo $compra->cliente_id ?>" title="Editar o cliente &quot;<?php echo $compra->cliente_nome ?>&quot;">
                                        <?php echo $compra->cliente_nome ?>
                                    </a>
                                </td>
                                <td class="<?php echo $ordenar_por=='produto' ? 'current' : '' ?>" title="O número entre parênteses representa a quantidade de licenças adquiridas">
                                    <a href="<?php echo SITE_URL ?>/fatorcms/produtos/editar/<?php echo $compra->produto_id ?>" title="Editar o produto &quot;<?php echo $compra->produto_nome ?>&quot;">
                                        <?php echo $compra->produto_nome ?>
                                    </a>
                                    <?php //echo ' ('.$compra->produto_licenca_quantidade.')' ?>
                                </td>
                                <td class="<?php echo $ordenar_por=='valor-total' ? 'current' : '' ?>">
                                	<?php $valor_cupom = (!is_null($compra->compra_cupom_valor)) ? $compra->compra_cupom_valor : 0; ?>
                                	<?php $valor_total = $compra->compra_produto_valor + ($compra->compra_produto_licenca_valor * $compra->produto_licenca_quantidade) + $compra->compra_entrega_valor - $valor_cupom; ?>
                                	R$ <?php echo number_format($valor_total, 2, ',', '.') ?>
                                </td>
                                <td class="<?php echo $ordenar_por=='met-pagamento' ? 'current' : '' ?>"><?php echo $compra->pagamento_metodo ?></td>
                                <td>
                                    <a href="<?php echo SITE_URL ?>/fatorcms/compras/visualizar/<?php echo $compra->id ?>" title="Visualizar a compra de código &quot;<?php echo $compra->codigo ?>&quot;">
                                        <img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier.png" alt="Visualizar" />
                                    </a>
                                </td>
                            </tr>
							<?php
                                }
                            }
						}
						else
						{
							echo '<tr><td colspan="8">'.((isset($buscar) AND ! is_null($buscar)) ? 'Nenhuma compra encontrada para esta busca.' : 'Nenhuma compra cadastrada até o momento.' ).'</td></tr>';
						}
						?>
					</tbody>

				</table>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>