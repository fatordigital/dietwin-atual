<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_Cupons extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai e testa se o usuário está logado
	 */
	public function __construct()
	{
		parent::__construct();
		$this->verificar_usuario_logado();
		$this->verificar_usuario_permissao('cupons');
	}


	/**
	 * Método padrão que não deve ser chamado diretamente. Se for, vai para a listagem
	 * @return void
	 */
	public function index()
	{
        header('Location: '.SITE_URL.'/fatorcms/cupons/listar');
		exit;
	}


	/**
	 * Método inicial que exibe a lista de registros do banco de dados
	 * @param $parametros object
	 * @return void
	 */
	public function listar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'cupons-lista.php'));
		$view->adicionar('body_class', 'cupons lista');
		$view->adicionar('lateral_menu', array('item'=>'cupons','link'=>'listar todos'));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$this->view_adicionar_obrigatorias($view);

		//-----

		// Lógica caso tenha sido feita uma busca na listagem
		$clausula_where = ' TRUE ';

		$buscar = NULL;
		if (isset($parametros->buscar) AND strlen(trim($parametros->buscar)) > 0)
		{
			$buscar = trim($parametros->buscar);
			$buscar_aux = Funcoes::mysqli_escape(str_replace(' ', '%', $buscar));
			$clausula_where .= ' AND (cupom.codigo LIKE "%'.$buscar_aux.'%" OR produto.nome LIKE "%'.$buscar_aux.'%")';
		}

		//-----

		// Ordenação dos resultados, podendo vir da página
		$ordenar_por = (isset($parametros->ordenar_por)) ? strtolower($parametros->ordenar_por) : 'codigo'; // Valor padrão
		$ordem = isset($parametros->ordem) ? strtolower($parametros->ordem) : 'asc'; // Valor padrão

		switch ($ordenar_por)
		{
			case 'codigo': $ordenar_por_sql = 'cupom.codigo'; break;
			case 'produto': $ordenar_por_sql = 'produto.nome'; break;
			case 'desconto': $ordenar_por_sql = 'cupom.valor'; break;
			case 'datas': $ordenar_por_sql = 'cupom.validade_inicio'; break;
			case 'ativo': $ordenar_por_sql = 'cupom.ativo'; break;
            default: $ordenar_por_sql = $ordenar_por; break;
		}

		//-----

		$cupom = new FatorCMS_Model_Cupom;
		// Define a cláusula do select
		$cupom->clausula = '
			SELECT cupom.*, produto.nome AS produto_nome
			FROM {tabela_nome} AS cupom
				LEFT JOIN {tabela_prefixo}produtos AS produto ON cupom.produto_id = produto.id
			WHERE '.$clausula_where.'
			ORDER BY '.$ordenar_por_sql.' '.$ordem
		;

		// Através da paginação vai verificar se existe a necessidade de dividir o resultado em várias páginas e se é necessário adicionar um LIMIT
		$paginacao = new Paginacao($cupom, $parametros, 20, 10);

		// Executa a cláusula lá de cima
		$cupons = $cupom->select(NULL, TRUE); // NULL para não alterar a clásula já definida e TRUE para forçar retorno em array

		//-----

		$view->adicionar('buscar', $buscar);
		$view->adicionar('ordenar_por', $ordenar_por);
		$view->adicionar('ordem', $ordem);
		$view->adicionar('cupons', $cupons);
		$view->adicionar('paginacao', $paginacao);

		$view->exibir();
	}


	/**
	 * Dependendo do parâmetro, aprensenta a tela de edição ou de cadastro
	 * @param $parametros object
	 * @return void
	 */
	public function editar($parametros)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'cupons-form.php'));

        $view->adicionar('body_class', 'cupons form');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
		$this->view_adicionar_obrigatorias($view);

        // Decide o que fazer
        if (isset($parametros->_0) AND is_numeric($parametros->_0))
        {
            // É atualização
            $cupom = new FatorCMS_Model_Cupom;
	        $cupom = $cupom->select('SELECT * FROM {tabela_nome} AS cupom WHERE id = '.Funcoes::mysqli_escape($parametros->_0));

            if (!is_null($cupom->cliente_id))
            {
                $cliente = new FatorCMS_Model_Cliente($cupom->cliente_id);
                $view->adicionar('cliente', $cliente);
            }

            $view->adicionar('cupom', $cupom);

            $view->adicionar('lateral_menu', array('item'=>'cupons','link'=>''));

           if (!is_null($cupom->cliente_id))
           {
               $cliente = new FatorCMS_Model_Cliente($cupom->cliente_id);
               $cliente_script = '{nome: "'.$cliente->nome.' ("+"'.$cliente->email.')", id: "'.$cliente->id.'"}';
           }
        }
        else
        {
            // É cadastro
			$view->adicionar('cupom', NULL);
			$view->adicionar('lateral_menu', array('item'=>'cupons','link'=>'cadastrar'));
        }

        //-----
        // E monta as informações para o javascript
        $script = 'var clientes = {itens: [';
        $script .= isset($cliente_script) ? $cliente_script : '';
        $script .= ']};';
        $view->adicionar('head_script', isset($script) ? $script : NULL);

		// Lista de Produtos para o <select>
        $produto = new FatorCMS_Model_Produto;
        $view->adicionar('produtos', $produto->select('SELECT * FROM {tabela_nome} ORDER BY nome', TRUE));

        $cliente = new FatorCMS_Model_Cliente();
        $view->adicionar('clientes', $cliente->select('SELECT * FROM {tabela_nome} ORDER BY nome', TRUE));

        $view->exibir();
    }


	/**
	 * Recebe os parâmetros do formulário e adiciona o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function cadastrar($parametros)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'cupons-form.php'));
		$this->view_adicionar_obrigatorias($view);

		//-----

		if ($parametros AND isset($parametros->codigo))
		{
			$cupom = new FatorCMS_Model_Cupom;
			$cupom->carregar($parametros);

			//-----

			// Log
			$cupom->log_id = $this->cms_usuario_logado->id;
			$cupom->log_data = date('Y-m-d H:i:s');

			//-----

            $cupom->colunas_mysqli_escape();

			if ($cupom->verificar_null(array('id')))
			{
				// Validações ---

				// Verifica se o código está disponível
				$cupom_existe = new FatorCMS_Model_Cupom;
				$cupom_existe = $cupom_existe->select('SELECT id FROM {tabela_nome} WHERE codigo = "'.$cupom->codigo.'"');
				if ($cupom_existe AND ! is_null($cupom_existe->id))
				{
					$notificacao = new Notificacao('Já existe um cupom cadastrado com o codigo <strong>'.$cupom->codigo.'</strong>. Por favor escolha outro código.');
				}

				// Datas da validade
				if ( ! empty($cupom->validade_inicio) AND ! empty($cupom->validade_fim))
				{
					if (strtotime($cupom->validade_inicio) > strtotime($cupom->validade_fim))
					{
						$notificacao = new Notificacao('A data final da validade não pode ser anterior à data de início.');
					}
				}

                if (!empty($parametros->as_values_clientes) AND $parametros->as_values_clientes != ',')
                {
                    $cupom->cliente_id = str_replace(',','',$parametros->as_values_clientes);
                }
                else
                {
                    $cupom->cliente_id = NULL;
                }

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					//var_dump($cupom);
					if ($cupom->insert())
					{
						new Notificacao('Cupom de código <strong>'.$cupom->codigo.'</strong> cadastrado com sucesso.', 'success', TRUE);
						header('Location: '.SITE_URL.'/fatorcms/cupons/listar');
						exit;
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao salvar os dados do cupom.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
			}

            if (!is_null($cupom->cliente_id))
            {
                $cliente = new FatorCMS_Model_Cliente($cupom->cliente_id);
                $cliente_script = '{nome: "'.$cliente->nome.' ("+"'.$cliente->email.')", id: "'.$cliente->id.'"}';
            }

			// Para retornar ao formulário com valores inseridos
			$view->adicionar('cupom', $cupom);
		}
		else
		{
			$view->adicionar('cupom', NULL);
		}

        //-----
        // E monta as informações para o javascript
        $script = 'var clientes = {itens: [';
        $script .= isset($cliente_script) ? $cliente_script : '';
        $script .= ']};';
        $view->adicionar('head_script', isset($script) ? $script : NULL);

		// Lista de Produtos para o <select>
        $produto = new FatorCMS_Model_Produto;
        $view->adicionar('produtos', $produto->select('SELECT * FROM {tabela_nome} ORDER BY nome', TRUE));

		$view->adicionar('body_class', 'cupons formulario');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
        $view->adicionar('lateral_menu', array('item'=>'cupons','link'=>'cadastrar'));

        $view->exibir();
	}


	/**
	 * Recebe os parâmetros do formulário e atualiza o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function atualizar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'cupons-form.php'));
		$this->view_adicionar_obrigatorias($view);

		//-----

		if (isset($parametros->id) AND is_numeric($parametros->id))
		{
			$cupom = new FatorCMS_Model_Cupom;
			$cupom->carregar($parametros);

            //print_r($parametros); exit;

			//-----

			// Log
			$cupom->log_id = $this->cms_usuario_logado->id;
			$cupom->log_data = date('Y-m-d H:i:s');

			//-----

			$cupom->colunas_mysqli_escape();

			if ($cupom->verificar_null())
			{
				// Validações ---

				// Verifica se o código está disponível
				$cupom_existe = new FatorCMS_Model_Cupom;
				$cupom_existe = $cupom_existe->select('SELECT id FROM {tabela_nome} WHERE codigo = "'.$cupom->codigo.'" AND id != "'.$cupom->id.'"');
				if ($cupom_existe AND ! is_null($cupom_existe->id))
				{
					$notificacao = new Notificacao('Já existe um cupom cadastrado com o codigo <strong>'.$cupom->codigo.'</strong>. Por favor escolha outro código.');
				}

				// Datas da validade
				if ( ! empty($cupom->validade_inicio) AND ! empty($cupom->validade_fim))
				{
					if (strtotime($cupom->validade_inicio) > strtotime($cupom->validade_fim))
					{
						$notificacao = new Notificacao('A data final da validade não pode ser anterior à data de início.');
					}
				}

                $colunas_null = array();

                if (!empty($parametros->as_values_clientes) AND $parametros->as_values_clientes != ',')
                {
                    $cupom->cliente_id = str_replace(',','',$parametros->as_values_clientes);
                }
                else
                {
                    $cupom->cliente_id = NULL;
                    $colunas_null[] = 'cliente_id';
                }

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($cupom->update($colunas_null))
					{
						new Notificacao('Cupom de código <strong>'.$cupom->codigo.'</strong> atualizado com sucesso.', 'success', TRUE);
						header('Location: '.SITE_URL.'/fatorcms/cupons/listar');
						exit;
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao atualizar os dados do cupom.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos os campos marcados são obrigatórios.');
			}

            if (!is_null($cupom->cliente_id))
            {
                $cliente = new FatorCMS_Model_Cliente($cupom->cliente_id);
                $cliente_script = '{nome: "'.$cliente->nome.' ("+"'.$cliente->email.')", id: "'.$cliente->id.'"}';
            }

			// Para retornar ao formulário com valores inseridos
			$view->adicionar('cupom', $cupom);
		}
		else
		{
			$notificacao = new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention');
		}



		//-----
        // E monta as informações para o javascript
        $script = 'var clientes = {itens: [';
        $script .= isset($cliente_script) ? $cliente_script : '';
        $script .= ']};';
        $view->adicionar('head_script', isset($script) ? $script : NULL);

		// Lista de Produtos para o <select>
        $produto = new FatorCMS_Model_Produto;
        $view->adicionar('produtos', $produto->select('SELECT * FROM {tabela_nome} ORDER BY nome', TRUE));

		$view->adicionar('body_class', 'cupons formulario');
		$view->adicionar('lateral_menu', array('item'=>'cupons','link'=>''));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$view->exibir();
	}


	/**
	 * Recebe o ID do registro e excluir ele do banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function excluir($parametros)
	{
		if (isset($parametros->_0) AND is_numeric($parametros->_0))
		{
			$cupom = new FatorCMS_Model_Cupom($parametros->_0);

			if ($cupom AND ! is_null($cupom->id))
			{
				if ($cupom->delete())
				{
					new Notificacao('O cupom de código <strong>'.$cupom->codigo.'</strong> foi excluído com sucesso.', 'success', TRUE);
				}
				else
				{
					new Notificacao('Ocorreu um erro ao excluir o cupom <strong>'.$cupom->codigo.'</strong>.', 'error', TRUE);
				}
            }
			else
			{
				new Notificacao('Nenhuma informação encontrada para este cupom.', 'error', TRUE);
			}
		}
		else
		{
			new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention', 'error', TRUE);
		}

		header('Location: '.SITE_URL.'/fatorcms/cupons/listar');
		exit;
	}



	/****************************** MÉTODOS EXTRAS ******************************/

    public function listar_clientes($parametros)
    {
       /* $iniciais = (isset($parametros->iniciais) AND !empty($parametros->iniciais)) ? Funcoes::mysqli_escape($parametros->iniciais) : NULL;
        $maximo = (isset($parametros->maximo) AND !empty($parametros->maximo)) ? Funcoes::mysqli_escape($parametros->maximo) : NULL;

        if (!is_null($iniciais) AND !is_null($maximo))
        {
            $cliente = new FatorCMS_Model_Cliente();
            $clientes = $cliente->select("SELECT id,nome FROM {tabela_nome} WHERE nome like '".$iniciais."%' LIMIT ".$maximo, TRUE);

            $lista_clientes = array();

            foreach ($clientes as $cliente)
            {
                $lista_clientes[] = array(
                    'id' => $cliente->id,
                    'nome' => $cliente->nome
                );
            }

            echo json_encode(array('clientes' => $lista_clientes)); exit;
        }

        echo json_encode(array('clientes' => array())); exit;  */

        $texto = Funcoes::mysqli_escape($parametros->q);
        $data = array();

        $cliente = new FatorCMS_Model_Cliente;
        $clientes = $cliente->select('SELECT id, nome, email FROM {tabela_nome} WHERE LOWER(nome) LIKE "'.mb_strtolower($texto).'%" ORDER BY nome', TRUE);

        if ($clientes AND count($clientes) > 0)
        {
            foreach($clientes as $cliente)
            {
                $json = array();
                $json['id'] = $cliente->id;
                $json['nome'] = $cliente->nome .' ('. $cliente->email.')';
                $data[] = $json;
            }
        }

        header("Content-type: application/json");
        echo json_encode($data);
    }

    /**
     * Busca todas as tags relacionadas com a noi
     * @param $novidade_id
     * @return array|bool
     */
    protected function cupom_buscar_clientes($cupom_id = NULL)
    {
        if ( ! is_null($cupom_id))
        {
            // Busca as tags do produto
            $cliente = new FatorCMS_Model_Cliente;
            $clientes = $cliente->select('
				SELECT {tabela_nome}.id, {tabela_nome}.nome, {tabela_nome}.email
                FROM {tabela_nome}
                JOIN fd_cupons ON fd_cupons.cliente_id = {tabela_nome}.id
                WHERE fd_cupons.id = '.$cupom_id.'
			', TRUE);
        }
        else
        {
            $clientes = FALSE;
        }

        // E monta as informações para o javascript
        $script = 'var clientes = {itens: [';
        if ($clientes AND count($clientes) > 0)
        {
            foreach ($clientes as $cliente)
            {
                $script .= '{nome: "'.$cliente->nome.'("+"'.$cliente->email.')", id: "'.$cliente->id.'"},';
            }
            $script = substr($script, 0, -1); // Retira a última vírgula
        }
        $script .= ']};';

        return $script;
    }

} // end class