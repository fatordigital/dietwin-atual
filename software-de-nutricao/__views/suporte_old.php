<?php include 'includes/cabecalho.php'; ?>

<body class="suporte profissional softwares">

<?php include 'includes/topo.php'; ?>

    <div class="titulo-principal">
        <p><span>Tire suas</span><br />dúvidas</p>
    </div>
    <div class="titulo-resumo">
        <p>O FAQ (Frequently Asked Questions - Perguntas mais frequentes) é um local onde condensamos todas as principais dúvidas dos usuários do dietWin.</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container">

        <?php include 'includes/toolbar.php' ?>


        <div class="box" id="tamanho-fonte">
        	
            <br/>
            <h2>SUPORTE</h2>

            <p>Por mais eficiente que seja as ferramentas <b>dietWin</b>, consideramos normal surgirem dúvidas iniciais até o conhecimento mais detalhado sobre o sistema.
                Para suprir esse atendimento o <b>dietWin</b> conta com uma forte estrutura de Suporte.
                Uma equipe plenamente qualificada para atender e suprir as suas necessidades em todas as etapas da instalação e orientação de uso.
            </p>

            <h3>HORÁRIO DE ATENDIMENTO ÚTIL:</h3>
            <p>De segunda a sexta-feira das 9 às 12hs e das 13 às 17hs.</p>

            <h3>Como utilizar o suporte dietWin?</h3>
            <p>Sua atenção para:</p>
            <ul class="detalhes">
                <li>Certifique-se que sua dúvida não consta dos textos da AJUDA ou dos vídeos demonstração.</li>
                <li>O suporte e orientações serão fornecidos somente ao usuário titular.</li>
                <li>As despesas de correio e telefone são responsabilidade do usuário.</li>
                <li>Mantenha seu endereço e seu e-mail e número de telefone <u>sempre</u> atualizados.</li>
            </ul>
            <p>DÚVIDAS POR ESCRITO - procedimentos:<br/>
                <b>E-MAIL: <a href="mailto:suporte@dietwin.com.br"> suporte@dietwin.com.br</a></b>
            </p>
            <p></p>

            <ul class="detalhes">
                <li>Formule suas perguntas com OBJETIVIDADE numerando-as preferencialmente.</li>
                <li>Copie a tela onde existe o erro ou dúvidas, para alguma ferramenta de editoração (ex.: Word) e aponte as suas observações por escrito, demonstrando um passo a passo imprimindo a maior clareza possível de sua localização.</li>
                <li>Reenvie o e-mail caso você não obtenha uma resposta em 48 horas úteis.</li>
            </ul>
            <p>
                <b>SUPORTE POR TELEFONE:</b><br/>
                    <b>FONE:</b>  (51) 3337.7908<br/>
                        <b>ATENÇÃO:</b> Se o problema for à orientação para um problema, você deverá estar na FRENTE DO COMPUTADOR e deve ter em mãos a "situação de erro" encontrado,  para que possamos identificar e avaliar melhor através de um passo a passo.

            </p>
            <p>

                FALAR SEM CUSTO ATRAVÉS DOS ENDEREÇOS:<br/>
               <b> SKYPE:</b> dietwin.suporte<br/>
                   <b> MSN:</b> suporte@dietwin.com.br
            </p>
            <p>
                OUTROS ENDEREÇOS:<br/>
                <b>FAX:</b> (51) 3342-4569<br/>
                    <b>SITE:</b> www.dietwin.com.br<br/>
                        <b>FACEBOOK:</b> http://www.facebook.com/dietwin.softwares
            </p>
            <p>Para nós do <b>dietWin</b>, a sua satisfação é nosso principal objetivo.
                Suas sugestões serão sempre bem vindas.
                Não deixe de registrar suas observações e esclarecer as dúvidas que não encontrar em nossos manuais, ajuda ou vídeos.
            </p>

        </div>

        <div class="sidebar">
            <div class="filtro">
                <h2>Filtre os tópicos</h2>
                <p>Clique para marcar apenas as categorias de tópicos que você deseja visualizar.</p>
                <a href="<?php echo SITE_URL.'/suporte/personal/faq'; ?>" class="filtro-personal<?php if ($categoria == 'Personal') { echo ' ativo'; } ?>" title="Filtrar por Personal "><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-personal<?php if ($categoria == 'Personal') { echo '-ativo'; } ?>.png" alt="Filtrar por Personal " /></a>
                <a href="<?php echo SITE_URL.'/suporte/profissional/faq'; ?>" class="filtro-profissional<?php if ($categoria == 'Profissional') { echo ' ativo'; } ?>" title="Filtrar por Profissional "><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-profissional<?php if ($categoria == 'Profissional') { echo '-ativo'; } ?>.png" alt="Filtrar por Profissional " /></a>
                <a href="<?php echo SITE_URL.'/suporte/rotulo-de-alimentos/faq'; ?>" class="filtro-rotulo-alimentos<?php if ($categoria == 'Rótulo de Alimentos') { echo ' ativo'; } ?>" title="Filtrar por Rótulo de Alimentos"><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-rotulo<?php if ($categoria == 'Rótulo de Alimentos') { echo '-ativo'; } ?>.png" alt="Filtrar por Rótulo de Alimentos" /></a>
                <a href="<?php echo SITE_URL.'/suporte/topicos-diversos/faq'; ?>" class="filtro-topicos-diversos<?php if ($categoria == 'Tópicos Diversos') { echo ' ativo'; } ?>" title="Filtrar por Tópicos diversos"><img src="<?php echo SITE_BASE ?>/views/imagens/filtro-topicos<?php if ($categoria == 'Tópicos Diversos') { echo '-ativo'; } ?>.png" alt="Filtrar por Tópicos diversos" /></a>
            </div>
            <?php
            if ($faqs AND count($faqs) > 0)
            {
                ?>
                <div class="filtro">
                    <h2>Busque por palavra-chave</h2>
                    <form method="post" action="<?php echo ($categoria != 'Todas') ? SITE_URL.'/suporte/'.$classe : SITE_URL ?>/faq" id="buscar" >
                        <input type="text" name="buscar" id="busca-campo" maxlength="255" value="" />
                        <button type="submit">Buscar</button>
                    </form>
                    <div class="clear"></div>
                </div>
                <?php
            }
            ?>
        </div>

        <div class="clear"></div>
    </div>
    <div class="inferior"></div>
</div>

<?php include 'includes/rodape.php'; ?>