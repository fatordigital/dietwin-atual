<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe

*/

class Controller_Contato extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        $view = new View('contato.php');
		$this->view_variaveis_obrigatorias($view);

		$view->adicionar('body_class', 'contato');
		$view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Contato - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */

    /**
     * Passa as informações para a montagem e envio do email a partir do formulário "Contato" do site
     * @param $parametros
     * @return void
     */
    public function enviar($parametros)
    {
        $nome = isset($parametros->nome) ? trim($parametros->nome) : NULL;
        $telefone = isset($parametros->telefone) ? trim($parametros->telefone) : NULL;
        $email = isset($parametros->email) ? trim($parametros->email) : NULL;
        $cidade = isset($parametros->cidade) ? trim($parametros->cidade) : NULL;
        $usuario = isset($parametros->usuario) ? trim($parametros->usuario) : NULL;
        $assunto = isset($parametros->assunto) ? trim($parametros->assunto) : NULL;
        $mensagem = isset($parametros->mensagem) ? trim($parametros->mensagem) : NULL;

        if ( ! empty($nome) AND ! empty($email) AND ! empty($mensagem) AND ! empty($telefone) AND ! empty($assunto) AND ! empty($cidade))
        {
            $mail = new Controller_Email;

            if ($mail->contato($parametros))
            {
	            if (SITE_LOCAL == 'fdserver') {
		            //$Metriks_CID = 0; $Metriks_FID = 0;
	            } else {
		            $Metriks_CID = 2; $Metriks_FID = 15;
	            }
	            @include 'biblioteca/metriks-enviar-conversao.php';

                echo json_encode(array('tipo'=>'sucesso', 'mensagem'=>'Sua mensagem foi enviada com sucesso!'));
            }
            else
            {
                echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Ocorreu um erro ao enviar sua mensagem.'));
            }
        }
        else
        {
            echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Ocorreu um erro ao receber suas informações.'));
        }
    }


} // end class