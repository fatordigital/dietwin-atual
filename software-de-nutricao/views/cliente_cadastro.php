<?php require_once 'includes/head.php' ?>
<body>
<?php require_once 'includes/nav.php' ?>

<section class="conteudo cinza">
<div class="container">
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <h1 class="interna">Cadastre-se</h1>
    <p>A equipe do dietWin está sempre à disposição de seus clientes. Preencha o formulário abaixo para efetuar seu cadastro e ter acesso a todas as informações sobre os softwares da dietWin.</p>
</div>
<div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
    <img src="<?php echo SITE_URL ?>/views/imagens/caixas-dietwin-cadastre-se.jpg" alt="dietWin Plus e dietWin Tradicional">
</div>
<section class="formulario">
<div class="col-lg-9 col-lg-offset-1 col-md-9 col-md-offset-1 col-sm-12 col-xs-12">
<div class="titulo-formulario">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2>Formulário de cadastro</h2>
    </div>
</div>

<form action="<?php echo SITE_URL.'/cliente/enviar-cadastro'; ?>" method="post" id="cadastro">
<div class="formulario-cadastro">
    <div class="col-lg-12 col-md-12 col-xs-12">
        <div class="row">
            <?php if(isset($_SESSION['cadastro_error'])){ ?>
            <div style="background-color:#8A2727; color:#fff; padding:15px;">
                <?php echo $_SESSION['cadastro_error']; ?>
            </div>
            <?php } unset($_SESSION['cadastro_error']); ?>

            <?php if(isset($_SESSION['cadastro_sucesso'])){ ?>
                <div style="background-color:#3F7232; color:#fff; padding:15px;">
                    <?php echo $_SESSION['cadastro_sucesso']; ?>
                </div>
            <?php } unset($_SESSION['cadastro_sucesso']); ?>


        </div>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                <label for="nome">Nome:</label>
            </div>
            <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                <input type="text" id="nome" name="nome" maxlength="100" class="size400" data-rule-required="true" data-msg-required="- Digite seu nome" value="<?php if(isset($campos->nome)){echo $campos->nome;}?>">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                <label for="email">E-mail:</label>
            </div>
            <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                <input type="text" id="email" name="email" maxlength="100" class="size400" data-rule-required="true" data-msg-required="- Digite seu e-mail" data-rule-email="true" data-msg-email="- Digite um e-mail correto" value="<?php if(isset($campos->email)){echo $campos->email;}?>">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                <label for="senha">Senha:</label>
            </div>
            <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                <input type="password" id="senha" name="senha" maxlength="18" class="size250" data-rule-required="true" data-msg-required="- Digite sua senha">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                <label for="confirmar_senha">Confirmar Senha:</label>
            </div>
            <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                <input type="password" id="confirmar_senha" name="confirmar_senha" maxlength="18" class="size250" data-rule-equalTo="#senha" data-msg-equalTo="- As senhas não conferem">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                <label for="profissao">Profissão:</label>
            </div>
            <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                <input type="text" id="profissao" name="profissao" maxlength="100" class="size400" data-rule-required="true" data-msg-required="- Digite sua profissão" value="<?php if(isset($campos->nome)){echo $campos->nome;}?>">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 pull-right">
                <label>Escola uma das opções abaixo:</label>
                <div class="tipo-pessoa">
                    <input type="radio" name="tipo_pessoa" id="pessoa-fisica" data-rule-required="true" data-msg-required="- Escolha o tipo de pessoa, física ou jurídica" value="Pessoa Física" <?php if(isset($campos->tipo_pessoa) AND $campos->tipo_pessoa=='Pessoa Física'){ echo 'checked="checked"'; } ?>>
                    <label for="pessoa-fisica">Pessoa Física</label>
                    <input type="radio" name="tipo_pessoa" id="pessoa-juridica" value="Pessoa Jurídica" <?php if(isset($campos->tipo_pessoa) AND $campos->tipo_pessoa=='Pessoa Jurídica'){ echo 'checked="checked"'; } ?>
                    <label for="pessoa-juridica">Pessoa Jurídica</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                <label for="rg">RG:</label>
            </div>
            <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                <input type="text" id="rg" name="rg" class="size400" maxlength="15" data-rule-required="true" data-msg-required="- Digite seu RG" value="<?php if(isset($campos->rg)){echo $campos->rg;}?>">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                <label for="cpf">CPF:</label>
            </div>
            <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                <input type="text" id="cpf" name="cpf" class="size400" data-rule-required="true" data-msg-required="- Digite seu CPF" value="<?php if(isset($campos->cpf)){echo $campos->cpf;}?>">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                <label for="endereco">Endereço:</label>
            </div>
            <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                <input type="text" id="endereco" name="endereco" class="size400" data-rule-required="true" data-msg-required="- Digite seu Endereço" value="<?php if(isset($campos->endereco)){echo $campos->endereco;}?>">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-right text-left-xs">
                <label for="numero">Número:</label>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <input type="text" id="numero" name="numero" style="width:130px;" data-rule-required="true" data-msg-required="- Digite o número de seu endereço" value="<?php if(isset($campos->numero)){echo $campos->numero;}?>">
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-right text-left-xs">
                <label for="complemento">Complemento:</label>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <input type="text" id="complemento" name="complemento" style="width:133px;" value="<?php if(isset($campos->complemento)){echo $campos->complemento;}?>">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 text-right text-left-xs">
                <label for="bairro">Bairro:</label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <input type="text" id="bairro" name="bairro" data-rule-required="true" data-msg-required="- Digite seu Bairro" value="<?php if(isset($campos->bairro)){echo $campos->bairro;}?>">
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 text-right text-left-xs">
                <label for="cep">CEP:</label>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <input type="text" id="cep" name="cep" style="width:133px;" data-rule-required="true" data-msg-required="- Digite seu CEP" value="<?php if(isset($campos->cep)){echo $campos->cep;}?>">
            </div>

        </div>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                <label for="estado">Estado:</label>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-8 col-xs-12">
                <select name="estado" id="estado" data-rule-required="true" data-msg-required="- Escolha seu Estado" autocomplete="off">
                    <?php
                    if(isset($estados)){
                        foreach($estados as $estado){
                            if($estado->sigla=='RS'){
                                echo '<option value="'.$estado->id.'" selected="selected">'.$estado->nome.'</option>';
                            }else{
                                echo '<option value="'.$estado->id.'" >'.$estado->nome.'</option>';
                            }
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                <label for="cidade">Cidade:</label>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-8 col-xs-12">
                <select name="cidade" id="cidade" data-rule-required="true" data-msg-required="- Escolha sua Cidade" style="width:133px;">
                    <?php
                    if(isset($cidades)){
                        foreach($cidades as $cidade){
                            if($cidade->nome=='Porto Alegre'){
                                echo '<option value="'.$cidade->id.'" selected="selected">'.$cidade->nome.'</option>';
                            }else{
                                echo '<option value="'.$cidade->id.'" >'.$cidade->nome.'</option>';
                            }
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                <label for="telefone">Telefone:</label>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-8 col-xs-12">
                <input type="text" id="telefone" name="telefone" style="width:165px;" data-rule-required="true" data-msg-required="- Digite seu telefone" value="<?php if(isset($campos->telefone)){echo $campos->telefone;}?>">
            </div>
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                <label for="celular">Celular:</label>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-8 col-xs-12">
                <input type="text" id="celular" name="celular" style="width:133px;" data-rule-required="true" data-msg-required="- Digite seu celular" value="<?php if(isset($campos->celular)){echo $campos->celular;}?>">
            </div>
        </div>
    </div>
</div>
<div class="endereco-entrega" style="display: none;">
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 pull-right">
            <h3>Endereço para entrega:</h3>
            <input type="checkbox" id="usar-endereco"> <label for="usar-endereco">Desejo receber no mesmo endereço informado acima</label>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
            <label for="endereco-entrega">Endereço:</label>
        </div>
        <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
            <input type="text" id="endereco-entrega" name="endereco-entrega">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
            <label for="bairro-entrega">Bairro:</label>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
            <input type="text" id="bairro-entrega" name="bairro-entrega">
        </div>
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
            <label for="numero-entrega">Número:</label>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
            <input type="text" id="numero-entrega" name="numero-entrega">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
            <label for="cep-entrega">CEP:</label>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
            <input type="text" id="cep-entrega" name="cep-entrega">
        </div>
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
            <label for="complemento-entrega">Complemento:</label>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
            <input type="text" id="complemento-entrega" name="complemento-entrega">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
            <label for="estado-entrega">Estado:</label>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
            <select name="estado-entrega" id="estado-entrega">
                <?php
                    if(isset($estados)){
                        foreach($estados as $estado){
                            echo '<option value="'.$estado->id.'">'.$estado->name.'</option>';
                        }
                    }
                ?>
            </select>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
            <label for="cidade-entrega">Cidade:</label>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
            <select name="cidade-entrega" id="cidade-entrega">
                <option value="null"></option>
                <option value="Porto Alegre">Porto Alegre</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
            <label for="observacoes">Observações:</label>
        </div>
        <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
            <textarea name="observacoes" id="observacoes"></textarea>
        </div>
    </div>
</div>
<div class="formulario-cadastro">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="erro-form">
                <label for="" class="error"><strong>Corrija os erros abaixo:</strong></label>
            </div>
        </div>
        <div class="col-lg-3 col-sm-4 col-sm-4 col-xs-12 pull-right">
            <input type="submit" value="Cadastrar" class="processo-compra-submit">
        </div>
    </div>
</div>
</form>
</div>
</section>
</div>
</section>

<?php require_once 'includes/footer.php' ?>

<script type="text/javascript">
    $(document).ready(function(){

        $('#estado').on('change', function(e){
            e.preventDefault();
            var $id = $(this).val();
            
            $.ajax({
                type:'POST',
                url:SITE_URL+'/cliente/ajax_get_cidades/'+$('#estado option:selected').val(),
                data: $id,
                beforeSend: function() {
                    $('#cidade').html('Carregando...');
                },
                success:function(response) {
                    $('#cidade').html(response);
                }
            });
        });

        /*getCidades();


        function getCidades(){
            $('#cidade-entrega').html('Carregando...');
        $.ajax(
            {
                type:'POST',
                url:SITE_URL+'/cliente/ajax_get_cidades/'+$('#estado option:selected').val(),
                data:$('#estado option:selected').val(),
                success:function(response)
                {
                    $('#cidade-entrega').html(response);
                }
            }
        );
        }*/
    });
</script>

</body>
</html>