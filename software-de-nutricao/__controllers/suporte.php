<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Suporte extends Controller_Padrao
{
    /**
     * Chama o construtor da classe pai
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método inicial que faz a renderização básica da página
     * @param $parametros
     * @return void
     */



    public function index($parametros)
    {
        $faq = new Model_FAQ();

        //Se tiver busca
        $buscar = '';
        if (isset($parametros->buscar) AND strlen($parametros->buscar) > 0)
        {
            $buscar = " AND (pergunta like '%".Funcoes::mysqli_escape($parametros->buscar)."%' OR  resposta like '%".Funcoes::mysqli_escape($parametros->buscar)."%')";
        }

        //Caso seja de uma categoria especifica
        if (isset($parametros->categoria) AND strlen($parametros->categoria) > 0)
        {
            $pergunta = '';
            if (isset($parametros->titulo_seo) AND strlen($parametros->titulo_seo) > 0)
            {
                $pergunta = " AND pergunta_seo = '".Funcoes::mysqli_escape($parametros->titulo_seo)."'";
            }

            if (Funcoes::mysqli_escape($parametros->categoria) == 'topicos-diversos')
            {
                $clausula = ' WHERE produto_id = 0';
                $classe = 'topicos-diversos';
                $categoria = 'Tópicos Diversos';
            }
            else
            {
                $produto = new Model_Produto();
                $produto->nome_seo = Funcoes::mysqli_escape($parametros->categoria);
                $produto = $produto->select("SELECT id,nome,nome_seo FROM {tabela_nome} WHERE nome_seo = 'dietwin-".$produto->nome_seo."'");
                if ($produto)
                {
                    $clausula = ' WHERE produto_id = '.$produto->id;
                    $categoria = substr($produto->nome,8);
                    $classe = Funcoes::mysqli_escape($parametros->categoria);
                }
                else
                {
                    header('HTTP/1.1 404 Not Found');
                    $erro_404 = new Controller_Erro;
                    $erro_404->index(404);
                    exit;
                }
            }
            $faqs = $faq->select('SELECT {tabela_nome}.*,fd_produtos.nome_seo, fd_produtos.nome FROM {tabela_nome} LEFT JOIN fd_produtos ON {tabela_nome}.produto_id = fd_produtos.id'.$clausula.$buscar.$pergunta.' ORDER BY pergunta ASC', TRUE);
            //echo 'SELECT {tabela_nome}.*,fd_produtos.nome_seo FROM {tabela_nome} LEFT JOIN fd_produtos ON {tabela_nome}.produto_id = fd_produtos.id'.$clausula.$buscar.$pergunta.' ORDER BY pergunta ASC'; exit;
        }
        else
        {
            $classe = 'topicos-diversos';
            $categoria = 'Todas';
            $faqs = $faq->select('SELECT {tabela_nome}.*,fd_produtos.nome_seo, fd_produtos.nome FROM {tabela_nome} LEFT JOIN fd_produtos ON {tabela_nome}.produto_id = fd_produtos.id WHERE TRUE '.$buscar.' ORDER BY pergunta ASC', TRUE);
        }

        if (isset($parametros->titulo_seo) AND strlen($parametros->titulo_seo))
        {
            if (!$faqs AND count($faqs) <= 0)
            {
                header('HTTP/1.1 404 Not Found');
                $erro_404 = new Controller_Erro;
                $erro_404->index(404);
                exit;
            }
            $view = new View('topico.php');
            $this->view_variaveis_obrigatorias($view);
            $view->adicionar('pagina_title', $faqs[0]->pergunta.' - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        }
        else
        {
            $view = new View('suporte.php');
            $this->view_variaveis_obrigatorias($view);
            $view->adicionar('pagina_title', 'Suporte - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        }



        $view->adicionar('faqs',$faqs);
        $view->adicionar('classe', $classe);
        $view->adicionar('categoria', $categoria);

        $view->adicionar('body_class', 'suporte');
        $view->adicionar('notificacao', new Notificacao);

        $view->exibir();
    }

    /* ***************************** MÉTODOS EXTRAS ***************************** */


} // end class