<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe

TODO: transformar os parâmetros que vem por $_GET ou REQUEST_URI em parametros mesmo (?campo1=qwe&campo2=asd&escondido1=Rodrigo&escondido2=Saling)
TODO: pode acontecer dos parâmetros acima entrarem como metodo, cuidar isso de alguma maneira (rotas)

2011-08-17 : adicionado a opção de definir "rotas" e agora é possível utilizar nomes complexos de controllers

*/

class Iniciar extends Sistema
{
	protected $diretorio;
	protected $controller;
	protected $metodo;
	protected $parametros;
	protected $parametro_i;


	// ----- Métodos mágicos

	public function __set($nome, $valor)
    {
	    if (isset($this->$nome))
	    {
		    switch ($nome)
		    {
			    case 'controller' :
				case 'metodo' :
				    $this->$nome = strtolower(trim($valor));
				    break;

			    case 'parametro_i' :
				    $this->parametro_i = intval($valor);
				    break;

			    default :
				    $this->parametros->$nome = is_string($valor) ? trim($valor) : $valor;
				    break;
		    }
            $this->$nome = $valor;
        }
    }

    public function __get($nome)
    {
	    if (isset($this->$nome))
	    {
            return $this->$nome;
        }

	    return NULL;
    }


	//----- Métodos extras para as propriedades

	public function carregar_parametros($parametros)
	{
		if (is_object($parametros))
		{
			$parametros = get_object_vars($parametros);
		}

		if (is_array($parametros))
		{
			foreach ($parametros as $nome=>$valor)
			{
				$this->parametros->$nome = is_string($valor) ? urldecode(trim($valor)) : $valor;
			}
		}
	}

	public function carregar_parametro($nome, $valor)
	{
		if (empty($nome))
        {
            $nome = '_'.$this->parametro_i;
            // Incrementa o contador de parâmetros para o próximo uso
            $this->parametro_i++;
        }

        // Adiciona a propriedade no objeto, junto com o valor
        $this->parametros->$nome = is_string($valor) ? urldecode(trim($valor)) : $valor;
	}


	//----- Método normais


	/**
	 * Por enquanto só chama o contrutor da classe pai
	 */
    public function __construct()
    {
		parent::__construct();

	    // Inicializa o objeto parametros para receber n propriedades
	    $this->parametros = new stdClass;
	    $this->parametro_i = 0;
	}


	/**
	 * Busca as requisições e salva as informações necessárias para dar continuidade na criação dos objetos necessários  	
	 */
	public function iniciar()
	{
        $url_completa = $_SERVER['REQUEST_URI'];
        // Limpa da URL retirando parte desnecessária inicial
        // Observação - Andrew: Caso o sistema esteja em um novo diretório, use o str_replace para sobrescrever o nome do diretório
        $url_parametros = str_replace(explode('/',str_replace('blog', '', $_SERVER['PHP_SELF'])), '', $url_completa);


		//-----
		
		// Verifica se há algo vindo via GET, por exemplo "?busca=abc", e adiciona nos parametros do objeto
		$this->get_parametros_adicionar($url_completa, $url_parametros);

		//-----

		// Separa todos os elementos da URL com base na utilizaçao de pretty/clean URL
        $parametros = explode ('/', str_replace('//', '/', $url_parametros));

		// Limpa os itens em branco do array
		$parametros_cont = count($parametros);
		for ($i=0; $i<$parametros_cont; $i++)
		{
			if (isset($parametros[$i]) AND strlen(trim($parametros[$i])) == 0)
			{
				unset($parametros[$i]);
			}
		}
		$parametros = array_values($parametros); // Reorganiza os índices do array

		// ! Aqui temos um array começando no índice zero contendo todos os elementes que antes estavam entre as "/" da URL

		// Nesta função, é feita a conferência entre o que tem de parâmetros da URL contra as regras estabelecidas para o funcionamento do site
		if (count($parametros) > 0)
		{
			$this->fazer_a_magica($parametros); // bringing some fun to the code!
		}

		//-----

		// Caso a variável do controller ainda estaja vazia, define o valor padrão
        if (empty($this->controller))
        {
            $this->controller = CONTROLLER_PADRAO;
        }


		// Caso a variável do metodo do controller ainda estaja vazia, define o valor padrão
        if (empty($this->metodo))
        {
            $this->metodo = METODO_PADRAO;
        }

		// Se houver algo enviado via POST, adiciona nos parâmetros
		if (isset($_POST) AND count($_POST) > 0)
		{
			$this->carregar_parametros($_POST);
		}
		// Se houver algo enviado via GET, adiciona nos parâmetros
		if (isset($_GET) AND count($_POST) > 0)
		{
			$this->carregar_parametros($_GET);
		}

//        $this->controller = 'Blog';
//        $this->metodo = '';
//
//        if ($this->controller == 'post') {
//            $this->controller = 'index';
//            $this->metodo = 'post';
//        }

//		echo 'diretorio:'.$this->diretorio.'<br>';
//		echo 'controller:'.$this->controller.'<br>';
//		echo 'metodo:'.$this->metodo.'<br>';
//
//        exit;

    }



	/**
	 * Aqui aplicamos as regras em cima da parte da URL que nos interessa, para que possamos "pescar" controller, método, e parâmetros
	 * @param $url_parametros
	 */
	private function fazer_a_magica($url_parametros)
	{
		global $regras, $regras_especiais;

		// Prepara as regras para serem usadas na verificação contra os parâmetros da URL
		$regras_parametros = array();
		$i = 0;
		foreach ($regras as $regra)
		{
			// Ex.: aqui separa '=@controller/=@metodo' em dois itens
			$regras_parametros[$i++] = explode('/', $regra);
			// Ex.: fica assim: array([0]=>'=@controller', [1]=>'=@metodo')
		}


		//-----

		// Percorre as regras especiais e salva os parâmetros
		if (count($regras_especiais) > 0)
		{
			// Regra especial, ex.: 'pagina/=$pagina'
			foreach ($regras_especiais as $regra_especial)
			{
				$regra_especial_parametros = explode('/', $regra_especial);
				// Pega a a próxima regra e a atribuição que vem logo depois
				$regra_parametro_proximo = $regra_especial_parametros[0]; // 'pagina'
				list($nome, $atribuicao) = explode('=', $regra_especial_parametros[1]); // '=$pagina' vira '' e '$pagina'

				// Procura nos parâmetros por ela
				//$url_parametros_aux = $url_parametros;
				$url_parametro_cont = 0;
				foreach ($url_parametros as $url_parametro)
				{
					// Se existir, pula para o próximo parâmetro e chama a função de transformação
					if ($url_parametro == $regra_parametro_proximo)
					{
						$this->carregar_parametro(str_replace('$','',$atribuicao), $url_parametros[$url_parametro_cont+1]);
						// Retira os parâmetros para que não sejam mais utilizados
						unset($url_parametros[$url_parametro_cont]); // Subtrai (depois de usado) devido ao incremento lá de baixo
						unset($url_parametros[$url_parametro_cont+1]); // Subtrai (depois de usado) devido ao incremento lá de baixo
						$url_parametros = array_values($url_parametros); // Reorganiza os índices do array

						break; // vai para a próxima regra especial
					}
					$url_parametro_cont++;
				}
			}
		}

		//-----

		// Percorre cada uma das regras (inteiras) estabelecidas anteriormente
		// Ex.: $regras_parametros: [0]=>'fatorcms=@diretorio',[1]=>'usuario=@controller',[2]=>'=@metodo'
		for ($regra_parametro_cont=0; $regra_parametro_cont<count($regras_parametros); $regra_parametro_cont++)
		{
			// Pega a primeira regra e em seguida o $nome dela
			$regra_primeira = $regras_parametros[$regra_parametro_cont][0];
			// Ex.: "=@controller", $nome fica "" e $atribuicao fica com "@controller"
			list($nome, $atribuicao) = explode('=', $regra_primeira);

			//-----

			$executar_regra = TRUE;
			// ... verifica se todos os parâmetros da regra estão na URL. Se sim, a regra será executada por inteiro
			for ($i=0; $i<count($regras_parametros[$regra_parametro_cont]); $i++)
			{
				$regra = $regras_parametros[$regra_parametro_cont][$i];
				// Ex.: "=@controller", $nome fica "" e $atribuicao fica com "@controller"
				list($nome, $atribuicao) = explode('=', $regra);

				//if ( ! empty($nome) AND isset($url_parametros[$i]) AND $url_parametros[$i] != $nome)
				if ( ! empty($nome))
				{
					if ( ! isset($url_parametros[$i]) OR (isset($url_parametros[$i]) AND $url_parametros[$i] != $nome))
					{
						$executar_regra = FALSE;
						break;
					}
				}
			}

			// ... e percorre toda a regra
			if ($executar_regra)
			{
				for ($i=0; $i<count($regras_parametros[$regra_parametro_cont]); $i++)
				{
					$regra = $regras_parametros[$regra_parametro_cont][$i];
					list($nome, $atribuicao) = explode('=', $regra);

					$this->transformar_parametro((isset($url_parametros[$i]) ? $url_parametros[$i] : NULL), $nome, $atribuicao);
				}

				// Se ainda existem parâmetros na URL e na regra não, adiciona o resto dos parâmetros como parâmetros mesmo
				for ($j=$i; $j<count($url_parametros); $j++)
				{
					$this->carregar_parametro(NULL, $url_parametros[$j]);
				}

				break; // Sai do FOR que percorre as regras
			}
		}
	}


	/**
	 * De acordo com a atribuição, define o que fazer com o parâmetro
	 * @param $parametro
	 * @param $nome
	 * @param $atribuicao
	 */
	private function transformar_parametro($parametro, $nome, $atribuicao)
	{
		if ($atribuicao == '@diretorio' AND is_null($this->diretorio))
		{
			$this->diretorio = empty($nome) ? $parametro : $nome;
		}
		elseif ($atribuicao == '@controller' AND is_null($this->controller))
		{
			// Se não houver parâmetro na URL e nem um nome na regra, coloca o PADRAO
			if (empty($parametro) AND empty($nome))
			{
				$this->controller = CONTROLLER_PADRAO;
			}
			else
			{
				$this->controller = empty($nome) ? $parametro : $nome;
			}
		}
		elseif ($atribuicao == '@metodo' AND is_null($this->metodo))
		{
			// Se não houver parâmetro na URL e nem um nome na regra, coloca o PADRAO
			if (empty($parametro) AND empty($nome))
			{
				$this->metodo = METODO_PADRAO;
			}
			else
			{
				$this->metodo = empty($nome) ? $parametro : $nome;
			}
		}
		elseif (strpos($atribuicao, '$') !== FALSE)
		{
			$this->carregar_parametro(str_replace('$', '', $atribuicao), empty($nome) ? $parametro : $nome);
		}
	}



	/**
	 * Utilizando os dados da função redirecionar, esta aqui instancia a classe da requisição, chama o método correspondente
	 * e se houver, passa os parâmetros necessários
	 */
	public function redirecionar()
	{
		// Monta o caminho para o arquivo do controller que queremos dar require
		// Aqui o nome que vem da URL com hífen, ex.: "area-restrita", é igual ao nome do arquivo
		$arquivo = (empty($this->diretorio)?'':$this->diretorio.'/').'controllers/'.strtolower($this->controller).'.php';

		// É o mesmo teste que a função autoload faz, mas aqui permite que o erro seja tratado
		if (file_exists($arquivo))
		{
			// Se o controller existe, require nele
			require_once $arquivo;


			// Adicionamos possíveis diretórios antes do nome do Controller
			// Aqui retiramos o hífen do nome do controller para juntar as palavras e criar o nome correto da classe
			$classe = (empty($this->diretorio) ? '' : $this->diretorio.'_').'Controller_'.(str_replace('-', '', $this->controller));

			if (class_exists($classe))
			{
				// Instancia o objeto
				$controller = new $classe;

				// Trocamos os hífens do nome do método que vem da URL por underscores, que é o correto dentro da classe
				$metodo = str_replace('-', '_', $this->metodo);

				if (method_exists($controller, $metodo))
				{
					// Se o método existe dentro da classe do controller, executa ele
					$controller->$metodo($this->parametros);
				}
				else
				{
					header('HTTP/1.1 404 Not Found');
					$erro = new Controller_Erro(404);
					$erro->index();
					exit;
				}
			}
			else
			{
				header('HTTP/1.1 404 Not Found');
				$erro = new Controller_Erro(404);
				$erro->index();
				exit;
			}
		}
		else
		{
			header('HTTP/1.1 404 Not Found');
			$erro = new Controller_Erro(404);
			$erro->index();
			exit;
		}
	}

	
	/**
	 * Função que transforma o que for passado para ela em um array de parâmetros
	 * @param  $url_completa
	 * @param  $url_parametros
	 * @return void
	 */
	protected function get_parametros_adicionar($url_completa, &$url_parametros)
	{
		if (strpos($url_completa, '?') !== FALSE)
		{
			// Posição do ponto de interrogação
			$posicao = strpos($url_completa, '?');
			// Pega a string a partir do ponto de interrogação (+1 para deixar de ser 'inclusive')
			$url_completa_aux = substr($url_completa, $posicao+1);
			// Divide a string através da divisão entre parâmetros
			$parametros = explode('&', $url_completa_aux);
			if (count($parametros) > 0)
			{
				// Para cada parâmetro, adiciona no objeto
				foreach($parametros as $parametro)
				{
					// O nome do parâmetro é vai até o 'igual' e o valor é o que vier depois até o fim
					$this->carregar_parametro(substr($parametro, 0, strpos($parametro, '=')), substr($parametro, strpos($parametro, '=')+1));
				}
			}
			// Limpa a string que será usada nas clean urls (retiramos o '?' lá em cima)
			$url_parametros = str_replace('?'.$url_completa_aux, '', $url_parametros);
		}
	}

} // end class

//-----------------------------------------------------------------------

// Inclui o acesso ao banco
require_once 'sistema/bd.php'; // Por que tem que ter esse sistema/???? É relativo ao include ou ao diretório original do arquivo? sempre achei que fosse do diretório original!

// Cria a instância do objeto
$start = new Iniciar;
// Inicialização da classe e início do funcionomento do sistema
$start->iniciar();
// Faz o redirecionomento apropriadao para o controller e metodo especificados
$start->redirecionar();
