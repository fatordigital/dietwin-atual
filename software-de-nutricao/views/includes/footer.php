<footer>
	<div class="conteudo hidden-sm hidden-xs">
		<div class="container">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="laranja">
					<div class="row">
						<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-left">
							<h2>Rótulo de Alimentos</h2>
						</div>
						<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-left">
							<p>Conheça o primeiro software para cálculo nutricional do Brasil. Desenvolvimento por nutricionistas, ele calcula a informação nutricional, conforme as Resoluções 359 e 360 da ANVISA. </p>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
							<div class="outros">
								<a href="http://www.rotulodealimentos.com.br" title="Rótulo de Alimentos" target="_blank" style="color: #FFF;">Ideal para profissionais que realizam consultoria nutricional.</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="azul">
					<h2>Suporte especializado</h2>
					<p>Está com alguma dificuldade em trabalhar com um software da família Dietwin? Fique tranquilo que iremos ajudá-lo. Utilize um dos canais abaixo para fazer contato.  </p>
					<div class="telefone-footer">
						<span>(51)</span> 3337.7908
					</div>
					<a href="mailto:suporte@dietwin.com.br" title="Envie um e-mail para suporte@dietwin.com.br" class="link-suporte">suporte@dietwin.com.br</a>
				</div>
			</div>
		</div>
	</div>
	<section class="logo">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				<img src="<?php echo SITE_URL ?>/views/imagens/dietwin-logo.png" alt="dietWin - Software de Nutrição">
			</div>
		</div>
	</section>
</footer>
<script src="<?php echo SITE_URL ?>/views/js/plugins.js"></script>
<script src="<?php echo SITE_URL ?>/views/js/main.js"></script>
<script>
  var stage = new swiffy.Stage(document.getElementById('swiffycontainer'),
                               swiffyobject);

  stage.start();
</script>

    <script type="text/javascript" src="//assets.zendesk.com/external/zenbox/v2.6/zenbox.js"></script>
    <style type="text/css" media="screen, projection">
      @import url(//assets.zendesk.com/external/zenbox/v2.6/zenbox.css);
    </style>
	<?php /*
    <script type="text/javascript">
    $(document).ready(function() {
      if (typeof(Zenbox) !== "undefined") {
        Zenbox.init({
          dropboxID:   "20193565",
          url:         "https://dietwin.zendesk.com",
          tabTooltip:  "Ajuda",
          tabImageURL: "https://p4.zdassets.com/external/zenbox/images/tab_pt_help.png",
          tabColor:    "#6B9461",
          tabPosition: "Left"
        });
      }
    });
    </script>
	*/ ?>

	<!-- Start of dietwin Zendesk Widget script --> <script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}(" https://assets.zendesk.com/embeddable_framework/main.js","dietwin.zendesk.com"); /*]]>*/</script> <!-- End of dietwin Zendesk Widget script -->
