<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Institucional extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        $view = new View('institucional.php');
		$this->view_variaveis_obrigatorias($view);

		$view->adicionar('body_class', 'institucional');
		$view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Institucional - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */


} // end class