<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_Videos extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai e testa se o usuário está logado
	 */
	public function __construct()
	{
		parent::__construct();
		$this->verificar_usuario_logado();
		$this->verificar_usuario_permissao('multimidia');
	}


	/**
	 * Método padrão que não deve ser chamado diretamente. Se for, vai para a listagem
	 * @return void
	 */
	public function index()
	{
        header('Location: '.SITE_URL.'/fatorcms/videos/listar');
		exit;
	}


	/**
	 * Método inicial que exibe a lista de registros do banco de dados
	 * @param $parametros object
	 * @return void
	 */
	public function listar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'videos-lista.php'));
		$view->adicionar('body_class', 'videos lista');
		$view->adicionar('lateral_menu', array('item'=>'multimidia','link'=>'listar videos'));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$this->view_adicionar_obrigatorias($view);

		//-----

		// Lógica caso tenha sido feita uma busca na listagem
		$clausula_where = ' TRUE ';

		$buscar = NULL;
		if (isset($parametros->buscar) AND strlen(trim($parametros->buscar)) > 0)
		{
			$buscar = trim($parametros->buscar);
			$buscar_aux = Funcoes::mysqli_escape(str_replace(' ', '%', $buscar));
			$clausula_where .= ' AND (video.titulo LIKE "%'.$buscar_aux.'%" OR video.descricao LIKE "%'.$buscar_aux.'%")';
		}

		//-----

		// Ordenação dos resultados, podendo vir da página
		$ordenar_por = (isset($parametros->ordenar_por)) ? strtolower($parametros->ordenar_por) : 'data'; // Valor padrão
		$ordem = isset($parametros->ordem) ? strtolower($parametros->ordem) : 'desc'; // Valor padrão

		switch ($ordenar_por)
		{
			case 'data': $ordenar_por_sql = 'video.data'; break;
			case 'titulo': $ordenar_por_sql = 'video.titulo'; break;
			case 'categoria': $ordenar_por_sql = 'produto.nome'; break;
            default: $ordenar_por_sql = $ordenar_por; break;
		}

		//-----

		$video = new FatorCMS_Model_Video;
		// Define a cláusula do select
		$video->clausula = '
			SELECT video.*, produto.nome AS produto_nome
			FROM {tabela_nome} AS video
				LEFT JOIN {tabela_prefixo}produtos AS produto ON video.produto_id = produto.id
				WHERE '.$clausula_where.' ORDER BY '.$ordenar_por_sql.' '.$ordem
		;

		// Através da paginação vai verificar se existe a necessidade de dividir o resultado em várias páginas e se é necessário adicionar um LIMIT
		$paginacao = new Paginacao($video, $parametros, 20, 10);

		// Executa a cláusula lá de cima
		$videos = $video->select(NULL, TRUE); // NULL para não alterar a clásula já definida e TRUE para forçar retorno em array

		//-----

		$view->adicionar('buscar', $buscar);
		$view->adicionar('ordenar_por', $ordenar_por);
		$view->adicionar('ordem', $ordem);
		$view->adicionar('videos', $videos);
		$view->adicionar('paginacao', $paginacao);

		$view->exibir();
	}


	/**
	 * Dependendo do parâmetro, aprensenta a tela de edição ou de cadastro
	 * @param $parametros object
	 * @return void
	 */
	public function editar($parametros)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'videos-form.php'));

        $view->adicionar('body_class', 'videos form');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
		$this->view_adicionar_obrigatorias($view);

        // Decide o que fazer
        if (isset($parametros->_0) AND is_numeric($parametros->_0))
        {
            // É atualização
            $video = new FatorCMS_Model_Video;
	        $video = $video->select('SELECT * FROM {tabela_nome} AS video WHERE id = '.Funcoes::mysqli_escape($parametros->_0));
            $view->adicionar('video', $video);

            $view->adicionar('lateral_menu', array('item'=>'multimidia','link'=>''));
        }
        else
        {
            // É cadastro
			$view->adicionar('video', NULL);
			$view->adicionar('lateral_menu', array('item'=>'multimidia','link'=>'cadastrar video'));
        }

		// Lista de Produtos para o <select>
        $produto = new FatorCMS_Model_Produto;
        $view->adicionar('produtos', $produto->select('SELECT * FROM {tabela_nome} ORDER BY nome', TRUE));

        $view->exibir();
    }


	/**
	 * Recebe os parâmetros do formulário e adiciona o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function cadastrar($parametros)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'videos-form.php'));
		$this->view_adicionar_obrigatorias($view);

		//-----

		if ($parametros AND isset($parametros->titulo))
		{
			$video = new FatorCMS_Model_Video;
			$video->carregar($parametros);

			// Campos especiais
			$video->titulo_seo = Funcoes::texto_para_seo($video->titulo);

			//-----

			// Verifica se o titulo_seo está disponível, se não inicia o contador até achar um
			$cont = 2;
			do
			{
				$video_existe = new FatorCMS_Model_Video;
				$video_existe = $video_existe->select('SELECT id FROM {tabela_nome} WHERE titulo_seo = "'.$video->titulo_seo.'"');
				if ($video_existe AND ! is_null($video_existe->id))
				{
					$video->titulo_seo = Funcoes::texto_para_seo($video->titulo).'-'.$cont++;
				}
			} while ($video_existe AND ! is_null($video_existe->id));

			//-----

			// Log
			$video->log_id = $this->cms_usuario_logado->id;
			$video->log_data = date('Y-m-d H:i:s');

			//-----

            $video->colunas_mysqli_escape();

			if ($video->verificar_null(array('id')))
			{
				// Validações ---

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($video->insert())
					{
						new Notificacao('Vídeo <strong>'.$video->titulo.'</strong> cadastrado com sucesso.', 'success', TRUE);
						header('Location: '.SITE_URL.'/fatorcms/videos/listar');
						exit;
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao salvar os dados do vídeo.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
			}
            
			// Para retornar ao formulário com valores inseridos
			$view->adicionar('video', $video);
		}
		else
		{
			$view->adicionar('video', NULL);
		}

		//-----

		// Lista de Produtos para o <select>
        $produto = new FatorCMS_Model_Produto;
        $view->adicionar('produtos', $produto->select('SELECT * FROM {tabela_nome} ORDER BY nome', TRUE));

		$view->adicionar('body_class', 'videos formulario');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
        $view->adicionar('lateral_menu', array('item'=>'multimidia','link'=>'cadastrar video'));

        $view->exibir();
	}


	/**
	 * Recebe os parâmetros do formulário e atualiza o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function atualizar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'videos-form.php'));
		$this->view_adicionar_obrigatorias($view);

		//-----

		if (isset($parametros->id) AND is_numeric($parametros->id))
		{
			$video = new FatorCMS_Model_Video;
			$video->carregar($parametros);

			// Campos especiais
			$video->titulo_seo = Funcoes::texto_para_seo($video->titulo);

			//-----

			// Verificamos se o titulo_seo de antes é o igual ao novo. Se for diferente verificamos a possibilidade
			$video_atual = new FatorCMS_Model_Video($video->id);
			if ($video->titulo_seo != $video_atual->titulo_seo)
			{
				// Verifica se o titulo_seo está disponível (desde que não seja o do próprio registro), se não inicia o contador até achar um
				$cont = 2;
				do
				{
					$video_existe = new FatorCMS_Model_Video;
					$video_existe = $video_existe->select('SELECT id FROM {tabela_nome} WHERE titulo_seo = "'.$video->titulo_seo.'" AND id != '.$video->id);
					if ($video_existe AND ! is_null($video_existe->id))
					{
						$video->titulo_seo = Funcoes::texto_para_seo($video->titulo).'-'.$cont++;
					}
				} while ($video_existe AND ! is_null($video_existe->id));
			}

			//-----

			// Log
			$video->log_id = $this->cms_usuario_logado->id;
			$video->log_data = date('Y-m-d H:i:s');

			//-----

			$video->colunas_mysqli_escape();

			if ($video->verificar_null())
			{
				// Validações ---

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($video->update())
					{
						new Notificacao('Vídeo <strong>'.$video->titulo.'</strong> atualizado com sucesso.', 'success', TRUE);
						header('Location: '.SITE_URL.'/fatorcms/videos/listar');
						exit;
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao atualizar os dados do vídeo.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos os campos marcados são obrigatórios.');
			}

			// Para retornar ao formulário com valores inseridos
			$view->adicionar('video', $video);
		}
		else
		{
			$notificacao = new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention');
		}

		//-----

		// Lista de Produtos para o <select>
        $produto = new FatorCMS_Model_Produto;
        $view->adicionar('produtos', $produto->select('SELECT * FROM {tabela_nome} ORDER BY nome', TRUE));

		$view->adicionar('body_class', 'videos formulario');
		$view->adicionar('lateral_menu', array('item'=>'multimidia','link'=>''));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$view->exibir();
	}


	/**
	 * Recebe o ID do registro e excluir ele do banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function excluir($parametros)
	{
		if (isset($parametros->_0) AND is_numeric($parametros->_0))
		{
			$video = new FatorCMS_Model_Video($parametros->_0);

			if ($video AND ! is_null($video->id))
			{
				if ($video->delete())
				{
					new Notificacao('O vídeo <strong>'.$video->titulo.'</strong> foi excluído com sucesso.', 'success', TRUE);
				}
				else
				{
					new Notificacao('Ocorreu um erro ao excluir o vídeo <strong>'.$video->titulo.'</strong>.', 'error', TRUE);
				}
            }
			else
			{
				new Notificacao('Nenhuma informação encontrada para este vídeo.', 'error', TRUE);
			}
		}
		else
		{
			new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention', 'error', TRUE);
		}

		header('Location: '.SITE_URL.'/fatorcms/videos/listar');
		exit;
	}



	/****************************** MÉTODOS EXTRAS ******************************/


} // end class