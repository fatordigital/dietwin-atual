<?php include 'includes/cabecalho.php'; ?>

<body class="comparativo">

<?php include 'includes/topo.php'; ?>

        <div class="titulo-principal">
            <p><span>Qual a melhor</span><br />Solução?</p>
        </div>
        <div class="titulo-resumo">
            <p>Cada software da família dietWin é cuidadosamente construído tendo como foco um perfil de usuário. Descubra qual é a melhor solução para você!</p>
        </div>
</div>
</div>

<div class="conteudo">
    <div class="container">
        <div class="box">

            <?php include 'includes/toolbar.php' ?>
            <br/>
            <h3>COMPARATIVO DAS FUNÇÕES DE CADA VERSÃO</h3>

            <p>Ao compararmos softwares de nutrição uma atenção especial deve estar voltada para a funcionalidade de cada versão. O objetivo principal nem sempre é o mais relevante e sim o número de ferramentas e suas funções são os determinantes das diferenças entre eles. Analisar a integridade relacional das informações obtidas no banco de dados é de importância fundamental, pois um cadastro “morto” sem uma ação para análise das implicações e interações nutricionais de nada adianta. No dietWin, todas as informações tem uma razão de ser com o fim específico de geral resultados oara AUXILIAR O NUTRICIONISTA NA TOMADA DE DECISÃO frente ao diagnóstico nutricional, obviamente sem ser conclusivo a interpretação dos resultados sempre será do profissional que assina a avaliação.Ao compararmos softwares de nutrição uma atenção especial deve estar voltada para a funcionalidade de cada versão. O objetivo principal nem sempre é o mais relevante e sim o número de ferramentas e suas funções são os determinantes das diferenças entre eles. Analisar a integridade relacional das informações obtidas no banco de dados é de importância fundamental, pois um cadastro “morto” sem uma ação para análise das implicações e interações nutricionais de nada adianta. No dietWin, todas as informações tem uma razão de ser com o fim específico de geral resultados oara AUXILIAR O NUTRICIONISTA NA TOMADA DE DECISÃO frente ao diagnóstico nutricional, obviamente sem ser conclusivo a interpretação dos resultados sempre será do profissional que assina a avaliação.</p>

            <table cellpadding="0" cellspacing="0" border="0" class="qual-versao">
                <thead>
                    <th class="coluna_texto"></th>
                    <th class="coluna_profissional titulo-profissional" style="color:#060">Profissional Plus</th>
                    <th class="coluna_personal titulo-personal">Personal</th>
                    <th class="coluna_rotulo titulo-rotulo">Rótulo de Alimentos</th>
                </thead>
                <tbody>
                 <th colspan="4" class="coluna-titulo">Avaliação Clínica</th>
                    <tr>
                        <td>Av. Clínica para análise das implicações nutricionais</td>
                        <td class="sim"></td>
                        <td class="nao"></td>
                        <td class="nao"></td>
                    </tr>
                        <tr>
                            <td>Doenças/Condutas, referidos pelo paciente</td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>Av. Clínica para análise das implicações nutricionais</td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>Exames Laboratoriais </td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>Sintomas e Sinais Clínicos, referidos pelo paciente</td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>Fármacos, referidos pelo paciente</td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>Receituário</td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>Prontuário</td>
                            <td class="sim"></td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>Inquéritos</td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>História clínica pregressa</td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>Anamnese simplificada</td>
                            <td class="sim"></td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>Registro da história alimentar</td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>Alergias e aversões alimentares, sinalizar</td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                            <td class="nao"></td>
                        </tr>
                        <tr>
                            <td>Histórico de consultas</td>
                            <td class="sim"></td>
                            <td class="sim"></td>
                            <td class="nao"></td>
                        </tr>
                 <th colspan="4" class="coluna-titulo">Avaliação Nutricional</th>
                     <tr>
                         <td>Av. Nutricional: Adulto, Idoso, Adolescentes, Criança, Gestante, </td>
                         <td class="sim"></td>
                         <td class="sim"></td>
                         <td class="nao"></td>
                     </tr>
                     <tr>
                         <td>Histórico</td>
                         <td class="sim"></td>
                         <td class="sim"></td>
                         <td class="nao"></td>
                     </tr>
                     <tr>
                         <td>Medidas antropométricas: 10 dobras cutâneas, 12 perímetros, 3 diâmetros, altura do joelho</td>
                         <td class="sim"></td>
                         <td class="sim"></td>
                         <td class="nao"></td>
                     </tr>
                 <tr>
                     <td>Índice fotográfico das medidas antopométricas</td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                     <td class="nao"></td>
                 </tr>
                 <tr>
                     <td>Protocolo para Av. Nutricional do Atleta</td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                     <td class="nao"></td>
                 </tr>
                 <tr>
                     <td><b>Peso teórico: </b>6 protocolos: Berardinelli, West, Grant, Lorens, Cormillot, Médias dos protocolos. Índice de Wolf. Tabela comparativa de medidas, Classificação do peso teórico.</td>
                     <td class="sim"></td>
                     <td>Berardinelli</td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td>Relação % peso atual X peso teórico</td>
                     <td class="sim"></td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td>Gráfico de evolução da curva de peso</td>
                     <td class="sim"></td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td><b>IMC:</b> 6 protocolos da classificação do IMC (OMS, OMS/Garrow/81-Keys  T  T./72-Krosla&Lowe/67, Shills, Royal College of Physicians, National Institute of Diabetes. </td>
                     <td class="sim"></td>
                     <td>OMS/2004</td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td>Histórico</td>
                     <td class="sim"></td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td>CB/CMB/PCT (Jellife), AMB (<b>Ann Arlor/1990</b>)  </td>
                     <td class="sim"></td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td><b>Composição corporal</b> – 6 protocolos: Faulkner, Pollock & Jackson, Guedes, Durnin & Womersley, Katch & Mcardle, Petroski, media dos protocolos para % gordura, peso gordo, peso magro, peso ósseo, peso residual e densidade corporal.</td>
                     <td class="sim"></td>
                     <td>Pollock & Jackson</td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td><b>Estimativas calóricas VET:</b> Protocolos TMB/VET/ FAO/OMS 85, FAM/OMS/85, GEAF/Kacth, HB/LONG 97, HB/Waitzberg & Rodrigues 95, Hb/queimados. VET para redução de peso, VET treinamento/pré-competição/ competição.</td>
                     <td class="sim"></td>
                     <td>Parcial</td>
                     <td class="nao"></td>
                 </tr>

                 <tr>
                     <td><b>Gestante:</b> a avaliação da gestante tem uma tela específica quando este grupo é selecionado apresentando a curva gestacional de Atalah,1997 e protocolo de acordo com o Ministério da Saúde, 1986. Relatórios.</td>
                     <td class="sim"></td>
                     <td>Parcial</td>
                     <td class="nao"></td>
                 </tr>
                 <tr>
                     <td><b>Criança:</b> a avaliação de crianças tem uma tela específica quando este grupo é selecionado apresentando as curvas de crescimento conforme o NCHS 1983 (WHO 2006 e 2007). Indicadores nutricionais: Gomes, Macias, Waterloo, Escore-Z, IMC, Leileko, % adequação Soc. Bras. Pediatria, Mc Laren, Marcondes, Jellife, Garn SM para Altura estimada. Relatórios.</td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                     <td class="nao"></td>
                 </tr>
                 <tr>
                     <td><b>Criança:</b> indicadores nutricionais das curvas de crescimento do WHO 2006-2007</td>
                     <td class="nao"></td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>
                 <tr>
                     <td><b>Gasto energético na atividade física:</b> protocolo do Katch, 3 tabelas de atividade física (Katch, Williams, Krause). Relatórios.</td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                     <td class="nao"></td>
                 </tr>
                 <tr>
                     <td><b>Av. nutricional de grupo</b></td>
                     <td class="nao"></td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>
                 <tr>
                     <td>Diagnóstico Nutricional</td>
                     <td class="sim"></td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>
                 <tr>
                     <td>Histórico de consultas</td>
                     <td class="sim"></td>
                     <td class="sim"></td>
                     <td class="nao"></td>
                 </tr>
                </tbody>
            <th colspan="4" class="coluna-titulo">Prescrição Dietética</th>
                <tr>
                    <td>Prescrição dietética, 28 cardápios por consulta </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Recordatório </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Gravar a dieta como modelo</td>
                    <td class="nao"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Estatística de “nutrientes de dietas” selecionados para grupos de pacientes</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Pesquisa e Busca facilitada de alimentos e receitas </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Alimentos indicados: doenças; sintomas/sinais; fármaco </td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Alimentos e receitas favoritos por classificação </td>
                    <td class="nao"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Análise dietética: 135 nutrientes</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Análise dietética: interações nutricionais; QAVP/E; % adequação DRI’s</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Análise dietética de Carboidratos; Carga glicêmica das por refeições; CHO complexos e simples; Fibra solúvel e insolúvel</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Análise dietética de Carboidratos; Índice glicêmico de alimentos e receitas;</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Análise dietética: % Ferro absorvível; Água/líquidos da dieta; Biodisponibilidade de nutrientes</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Análise dietética: alimentos que compõe a análise dos nutrientes</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Análise do Demonstrativo dos nutrientes envolvidos nas interações/implicações nutricionais da dieta e recordatório </td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Distribuição da dieta na pirâmide alimentar brasileira </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Lista de alimentos equivalentes dietéticos “calculado” </td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Lista de substituição dos alimentos</td>
                    <td class="nao"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Recomendação dietoterápica </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Cuidados nutricionais relacionados a avaliação clínica</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>QAVP – distribuição macronutrientes dieta</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Copiar e colar alimentos, refeições ou dietas</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Gráfico da distribuição de macronutrientes dieta/refeições</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Gráfico da distribuição dos alimentos da dieta na pirâmide alimentar brasileira</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Gráfico Comparativo de macronutrientes previstos e encontrados na dieta</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
            <th colspan="4" class="coluna-titulo">Cadastros: Banco de Dados</th>

                <tr>
                    <td>3 Tabelas de alimentos e receitas</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Alimentos</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Receitas </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Modelos de dieta, refeição </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Classificações de alimentos e receitas</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Fabricantes </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Bibliografias</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Sabores</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Tipos de refeição</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Pacientes</td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Pacientes:grupo; tipo; religião; raça; profissão</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Protocolo de Doenças/Condutas dietoterápicas</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Protocolo de Exames laboratoriais </td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Protocolo de Sintomas/sinais clínicos</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Protocolo de Interações fármaco/nutrientes </td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Interações, Cuidados nutricionais</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Inquéritos: modelos de questionários </td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Recomendações diárias</td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Anamnese </td>
                    <td class="nao"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Alimentos e receitas favoritos por classificação </td>
                    <td class="nao"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Lista de substituição dos alimentos, padrão </td>
                    <td class="nao"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Modelos de dieta, padrão </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
            <th colspan="4" class="coluna-titulo">Ferramentas</th>

                <tr>
                    <td>Estatísticas do Perfil do paciente atendido </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Etiquetas de pacientes/clientes </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Agenda de consultas </td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Listagem de cadastros  </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Grupos de acesso  </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Configurações para uso do sistema </td>
                    <td class="nao"></td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Configurações para os relatórios  </td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Config. Usuários: Recomendação Dietoterápica; Receituários; Distrib. Macronutrientes e  % refeição </td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Grupo de acesso ao sistema </td>
                    <td class="sim"></td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                </tr>
                <tr>
                    <td>Assistente para a rotulagem </td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Formato de etiqueta, para a rotulagem </td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                    <td class="sim"></td>
                </tr>
                <tr>
                    <td>Modelos de rótulo, para a rotulagem </td>
                    <td class="nao"></td>
                    <td class="nao"></td>
                    <td class="sim"></td>
                </tr>

            </table>
            <br/>
            <br/>
            <br/>
            <h3>Comentários da autora sobre as versões dietWin:</h3>

            <table cellpadding="0" cellspacing="0" border="0" class="qual-versao-autora">
                <thead>
                <th class="coluna_profissional titulo-profissional" style="color:#060">Profissional Plus</th>
                <th class="coluna_personal titulo-personal">Personal</th>
                <th class="coluna_rotulo titulo-rotulo">Rótulo de Alimentos</th>
                </thead>
                <tbody>
                <th colspan="3" class="coluna-titulo">É um software para:</th>
                <tr>
                    <td>- atendimento clínico nutricional;<br/>
                        - aquele profissional que tem tempo;<br/>
                        - quem gosta de mexer e alimentar o sistema ajustando os cadastros ao seu modo de trabalhar, personalizando-o;<br/>
                        - quem precisa formar um banco de dados para pesquisa ou publicar artigos;<br/>
                        - quem gosta de ter múltiplas opções de atendimento no mercado de trabalho;<br/>
                        - quem gosta de manter protocolos diferenciados de atendimento em diferentes áreas de atuação da nutrição;<br/>
                        - quem quer montar o “seu” livro eletrônico, onde todas as informações estarão no lugar certo quando ele precisar<br/>
                    </td>
                    <td>- atendimento clínico nutricional;<br/>
                        -aquele profissional que precisa de agilidade e não disponibiliza de muito tempo para alimentar o sistema;<br/>
                        - ser prático, mas que preencha as necessidades básicas do atendimento nutricional.<br/>
                        - quem está começando a utilizar um software pela primeira vez;<br/>
                        - o profissional que está começando atuar na área clínica;<br/>
                    </td>
                    <td>- quem necessita executar o cálculo da Informação nutricional;<br/>
                        - quem executa consultoria nutricional & gastronômica;<br/>
                        - quem quer manter um banco de dados de fichas técnicas de um receituário.<br/>
                    </td>
                </tr>

            <th colspan="3" class="coluna-titulo">Ideal para:</th>
                <tr>
                    <td>Destina-se a todas as áreas da nutrição</td>
                    <td>Consultórios; personal diet; consultorias; hospitais; ensino acadêmico; UANs</td>
                    <td>empresas fabricante de produtos alimentícios; consultorias nutricionais; UANs</td>
                </tr>

                </tbody>
            </table>

            <br/><br/><br/>
        </div>
        <div class="clear"></div>

        <div class="clear"></div>
        <div class="inferior"></div>
    </div>
</div>

<?php include 'includes/rodape.php'; ?>