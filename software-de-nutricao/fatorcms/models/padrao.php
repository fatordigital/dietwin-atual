<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe

- $colunas_informacoes_cache está no final deste arquivo


*/


/**
 * Delaração para auto-complete em IDEs
 * @property object conexao
 * @property string clausula
 */

class FatorCMS_Model_Padrao
{
	protected $conexao;
	protected $clausula;
	protected $tabela_prefixo;
	protected $tabela_nome;
	protected $update_colunas_para_null = array();
	protected $colunas = array();
	protected $colunas_informacoes = array();


	/**
	 * Método mágico SET para as propriedades do Model
	 * @param $nome
	 * @param $valor
	 */
	public function __set($nome, $valor)
    {
	    if (array_key_exists($nome, $this->colunas))
	    {
		    // NÃO MEXER NESSE REGEX - http://php.net/manual/pt_BR/function.preg-match.php (exemplo #4)
		    // Serve para pegarmos o tipo e o tamanho do campo, de formatos assim: varchar(50), int(11), date etc.
		    // disponibilizando assim: $retorno['tipo'] e $retorno['tamanho']
		    preg_match('/(?P<tipo>\w+)(\((?P<tamanho>\d+)\))?/', $this->colunas_informacoes[$nome]->tipo, $retorno);

		    switch ($retorno['tipo'])
		    {
			    case 'char' :
			    case 'varchar' :
				    $this->colunas[$nome] = ( ! empty($valor) AND is_string($valor)) ? mb_substr(trim($valor), 0, $retorno['tamanho']) : NULL;
				    break;

			    case 'tinyint' :
			    case 'smallint' :
				    $this->colunas[$nome] = ! is_null($valor) ? intval($valor) : NULL;
				    break;

			    case 'int' :
			    case 'bigint' :
			        // Se houver zerofill, tratamos o valor como string
			        if (stristr($this->colunas_informacoes[$nome]->tipo, 'zerofill'))
			        {
				        $this->colunas[$nome] = ! is_null($valor) ? mb_substr(trim($valor), 0, $retorno['tamanho']) : NULL;
			        }
			        else
			        {
                        $this->colunas[$nome] = ! is_null($valor) ? floatval($valor) : NULL;
			        }
                    break;

                case 'decimal' :
	                $this->colunas[$nome] = ! is_null($valor) ? ( ! is_numeric($valor)) ? (str_replace(',', '.', str_replace(array('R$',' ','.'), '', $valor))) : ($valor) : NULL;
                    break;

			    case 'enum' :
				    $this->colunas[$nome] = ! is_null($valor) ? trim($valor) : NULL;
				    break;

			    case 'text' :
                    $this->colunas[$nome] = ! is_null($valor) ? mb_substr(trim($valor), 0, 65000) : NULL;
                    break;

			    case 'mediumtext' :
	                $this->colunas[$nome] = ! is_null($valor) ? mb_substr(trim($valor), 0, 16770000) : NULL;
	                break;

			    case 'datetime' :
                    $this->colunas[$nome] = ! is_null($valor) ? date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $valor))) : NULL;
                    break;

			    case 'date' :
				    $this->colunas[$nome] = ! is_null($valor) ? date('Y-m-d', strtotime(str_replace('/', '-', $valor))) : NULL;
				    break;
		    }
        }
	    else
	    {
		    $this->$nome = $valor;
	    }
    }


	/**
	 * Método mágico GET para as propriedades do Model
	 * @param $nome
	 * @return mixed
	 */
    public function __get($nome)
    {
	    if (array_key_exists($nome, $this->colunas))
	    {
            return $this->colunas[$nome];
        }
	    else
	    {
		    return $this->$nome;
	    }
    }

	/**
	 * Método mágico ISSET para as propriedades do Model
	 * @param $nome
	 * @return bool
	 */
	public function  __isset($nome)
	{
		return (array_key_exists($nome, $this->colunas) AND ! is_null($this->$nome));
	}


	//----- Método normais


	/**
	 * Faz a conexão com o banco, busca as informações sobre a tabela do Model (ou carrega do "cache") e se houver o
	 * paramêtro ID, carrega o objeto
	 * @param null $id
	 */
	public function __construct($id = NULL)
	{
		global $bd, $bd_config, $colunas_informacoes_cache;

		// Seta no Model a conexão criada lá na classe BD
		$this->conexao = $bd->conexao;
		// Pega o prefixo das tabelas uque foi definido no config.php
		$this->tabela_prefixo = $bd_config[SITE_LOCAL]['tabela_prefixo'];
		// Juntamos o prefixo com o nome da tabela do Model para criar o nome correto da tabela
		$this->tabela_nome = $this->tabela_prefixo.$this->tabela_nome;

		//-----

		// Se as informações sobre as colunas da tabela não estiverem no nosso "cache", busca no BD
		if ( ! isset($colunas_informacoes_cache[$this->tabela_nome]))
		{
			// Carregamos as informações da tabela do Model
			$query = $this->conexao->query('SHOW COLUMNS FROM '.$this->tabela_nome);

			while ($linha = $query->fetch_object())
			{
				// Iniciamos o valor da coluna para NULL
				$this->colunas[$linha->Field] = NULL;

				$informacoes = new stdClass;
				$informacoes->tipo = $linha->Type; // Tipo da coluna (varchar, int, date...)
				$informacoes->permite_null = ($linha->Null == 'YES'); // Permite valor NULL? (possíveis 'NO' e 'YES')
				$informacoes->chave = $linha->Key; // É um tipo de chave? Primary key..
				$informacoes->valor_padrao = $linha->Default; // Valor padrão da coluna, se houver
				$informacoes->extra = $linha->Extra; // auto_increment opr enquanto
				$this->colunas_informacoes[$linha->Field] = $informacoes;

				// Cópia para o cache
				$colunas_informacoes_cache[$this->tabela_nome][$linha->Field] = $informacoes;
			}
		}
		else
		{
			// Se as informações já existirem, reutilizamos
			foreach ($colunas_informacoes_cache[$this->tabela_nome] as $nome=>$informacoes)
			{
				// Iniciamos o valor da coluna para NULL
				$this->colunas[$nome] = NULL;
				// Copiamos o "cache" para o lugar correto
				$this->colunas_informacoes[$nome] = $informacoes;
			}
		}

		//-----

		// Se houver parâmetro, carrega o objeto do banco
		if ( ! is_null($id) AND is_numeric($id))
		{
			$this->id = $id;
			$this->colunas_mysqli_escape();
			$this->carregar($this->select('SELECT * FROM {tabela_nome} WHERE id = "'.$this->id.'"'));
		}
	}
	
	
	/**
	 * Recebe um conjunto de dados e o nome da classe e carrega os valores nas variáveis do objeto
	 * @param $dados
	 * @return void
	 */
	public function carregar($dados)
	{
		// Se for um objeto ...
		if (is_object($dados))
		{
			// ... transforma em array para pegar o nome de cada variável dentro
			if ( ! isset($dados->colunas))
			{
				$dados = get_object_vars($dados);
			}
			else
			{
				// Se $dados for um Model nosso, percorremos as colunas dele para transformar tudo em array
				$dados_aux = array();
				foreach ($dados->colunas as $nome=>$valor)
				{
					$dados_aux[$nome] = $dados->$nome;
				}
				$dados = $dados_aux;
			}
		}

		foreach ($dados as $nome=>$valor)
		{
			// Testa a existência do nome dentro da lista de colunas da tabela
			if (array_key_exists($nome, $this->colunas))
			{
				if (is_string($valor) AND strlen($valor) == 0)
				{
					$valor = NULL;
					// Se for possível, atualizar para NULL, adicionando no array para atualizar para NULL
					if ($this->colunas_informacoes[$nome]->permite_null)
					{
						$this->adicionar_update_colunas_para_null($nome);
					}
				}
				// Chama o método passando o valor como parâmetro
				$this->$nome = $valor;
			}
		}
	}


	/**
	 * Realiza uma consulta no banco de dados, devolvendo um objeto ou um array deles
	 * @param null $clausula
	 * @param bool $is_array
	 * @return array|bool
	 */
	public function select($clausula = NULL, $is_array = FALSE)
	{
		// Caso seja passado uma cláusula diretamente para a função, executa ela
		if ( ! is_null($clausula))
		{
			$this->clausula = $clausula;
		}

		// Troca uma possível variável pelo nome da tabela do Model
		$this->clausula = str_replace('{tabela_nome}', $this->tabela_nome, $this->clausula);

		// Troca uma possível variável pelo prefixo das tabelas no banco de dados. Utilizado muito em JOINs
		$this->clausula = str_replace('{tabela_prefixo}', $this->tabela_prefixo, $this->clausula);

		// Executa o SELECT no banco de dados
		$query = $this->conexao->query($this->clausula);

		if ($query AND $query->num_rows > 0)
		{
			if ($query->num_rows == 1 AND ! $is_array)
			{
				return $query->fetch_object(get_class($this));
			}
			else
			{
				$objetos = array();
				while ($linha = $query->fetch_object(get_class($this)))
				{
					$objetos[] = $linha;
				}
				return (count($objetos) > 0) ? $objetos : FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	

	/**
	 * Realizar uma inserção no banco, salvando o ID (auto increment) automaticamente no objeto
	 * @return bool
	 */
	public function insert()
	{
		$colunas = array();
		$valores = array();

		// Percorre as colunas do Model e pega o valor, se não for NULL
		foreach ($this->colunas as $nome=>$valor)
		{
			if ( ! is_null($this->$nome) AND strlen($this->$nome) > 0)
			{
				$colunas[] = $nome;
				// Todos os valores vão entre aspas, e aparentemente funciona (com exceção do NULL)
				$valores[] = "'".$this->$nome."'";
			}
		}
        
		$sql = 'INSERT INTO '.$this->tabela_nome.' ('.implode(', ',$colunas).') VALUES ('.implode(',',$valores).')';

		$query = $this->conexao->query($sql);

		if ($query)
		{
			if (array_key_exists('id', $this->colunas))
			{
				$this->id = $this->conexao->insert_id;
			}
		}

		return $query;
	}


	/**
	 * Realiza o update no banco, retornando boolean para o sucesso ou não da operação
	 * @param array $update_colunas_para_null
	 * @return bool
	 */
	public function update($update_colunas_para_null = array())
	{
		// Se recebeu colunas por parâmetro, atualiza o array local
		if (count($update_colunas_para_null) > 0)
		{
			foreach ($update_colunas_para_null as $coluna)
			{
				$this->adicionar_update_colunas_para_null($coluna);
			}
		}
		
		// Uma só variável que acomoda a coluna e valor
		$colunas_valores = array();

		// Percorre as colunas do Model e pega o valor
		foreach ($this->colunas as $nome=>$valor)
		{
			if ( ! is_null($this->$nome))
			{
				// Não queremos atualizar a primary key da tabela
				if ($nome != 'id')
				{
					$colunas_valores[] = $nome."='".$this->$nome."'";
				}
			}
			elseif (in_array($nome, $this->update_colunas_para_null))
			{
				// Caso a variável esteja no array de atualizar para NULL, adiciona essa instrução
				$colunas_valores[] = $nome.'=NULL';
			}
		}

		$sql = 'UPDATE '.$this->tabela_nome.' SET '.implode(',',$colunas_valores).' WHERE id = '.$this->id;

		return $this->conexao->query($sql);
	}


	/**
	 * Executa o delete no banco, conforme o ID salvo no próprio objeto
	 * @return bool
	 */
	public function delete()
	{
		// Monta a query
		$sql = 'DELETE FROM '.$this->tabela_nome.' WHERE id = '.$this->id;
		// Executa ela e retorna TRUE ou FALSE
		return $this->query($sql);
	}


	/**
	 * Método para execução direta no banco de dados de uma cláusula, passada por parâmetro ou já existente
	 * @param $clausula
	 * @return mixed
	 */
	public function query($clausula = NULL)
	{
		return $this->conexao->query(is_null($clausula) ? $this->clausula : $clausula);
	}


	/**
	 * Método para execução direta no banco de dados de uma cláusula, passada por parâmetro ou já existente, que tenha uma ou mais querys
	 * @param null $clausula
	 * @return bool
	 */
	public function multi_query($clausula = NULL)
	{
		return $this->conexao->multi_query(is_null($clausula) ? $this->clausula : $clausula);
	}



	/*********************************** MÉTODOS EXTRAS ***********************************/



	/**
	 * Executa a função para tratamento dos dados antes de fazer as consultas no banco
	 */
	public function colunas_mysqli_escape()
	{
		foreach ($this->colunas as $nome=>$valor)
		{
			$valor_novo = Funcoes::mysqli_escape($this->$nome, $this->conexao);
			// O TinyMCE está adicionando "\r\n" no final de cada parágrafo, e aqui retiramos eles
			$valor_novo = ! is_null($valor) ? str_replace('\r\n', '', $valor_novo) : $valor;
			$this->$nome = $valor_novo;
		}
	}


	/**
	 * Verificamos se alguma coluna está com valor NULL quando o banco de dados não permite
	 * @param array $excecoes Passe a lista de colunas que não entram na verificação
	 * @return bool
	 */
	public function verificar_null($excecoes = array())
	{
		foreach ($this->colunas as $nome=>$valor)
		{
			if (is_null($this->$nome) AND ! $this->colunas_informacoes[$nome]->permite_null AND ! in_array($nome, $excecoes))
			{
				return FALSE;
			}
		}
		return TRUE;
	}


	/**
	 * Método que recebe o nome das colunas que podem ser atualizadas para NULL no momento do UPDATE
	 * @param $nome
	 */
	public function adicionar_update_colunas_para_null($nome)
	{
		if ($this->colunas_informacoes[$nome]->permite_null)
		{
			$this->update_colunas_para_null[] = trim($nome);
		}
	}


	public function buscar_colunas()
	{
		return $this->colunas;
	}


} // end class


// Para evitar consultas desnecessárias no BD para buscar os tipos das colunas
$colunas_informacoes_cache = array();