<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Downloads extends Controller_Padrao
{
  /**
   * Chama o construtor da classe pai
   */
  public function __construct()
  {
    parent::__construct();
  }


  public function index($parametros)
  {
    header("Location: /downloads/teste-gratis");
  }


  public function baixar_produto(){
    //header("Location: http://www.dietwin.com.br/download/Instalar-dietWin-V2969.exe");
    //header("Location: http://www.dietwin.com.br/download/InstalarLight-dietWin-Consultorio-V2979.exe");
    //header("Location: http://www.dietwin.com.br/download/20151130-InstalarLight-dietWin-V3004.exe");
    //header("Location: http://www.dietwin.com.br/download/20160404-InstalarLight-dietWin-Consult%C3%B3rio-V3048.exe");
    //header("Location: http://www.dietwin.com.br/download/InstalarLight-dietWin-V3084.exe");
    header("Location: http://www.dietwin.com.br/download/InstalarLight-dietWin-V3090.exe");
  }

  public function atualizar_produto(){
    header("Location: http://www.dietwin.com.br/download/AtualizarLight-dietWin-V3090.exe");
  }


  public function teste_gratis(){

    $view = new View('teste-gratis.php');
    $this->view_variaveis_obrigatorias($view);

    $view->adicionar('body_class', 'index');
    $view->adicionar('notificacao',new Notificacao());
    $view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

    $view->exibir();
  }

  public function teste(){
      $dir = getcwd();
      require_once $dir.'/biblioteca/Mailchimp.php';

      $apikey = "87b52468df61b41c24729a0759156aca-us8";
      $merge_vars = array('MM1'=>$mm1);
      $listid = '7835733ec9';
      $MailChimp = new Mailchimp($apikey);
      $result = $MailChimp->lists->subscribe($listid, array('email'=>"andre.carello@fatordigital.com.br"),$merge_vars,false,true,false,false);
      print_r($result);
  }

    public function atualizar(){
        $view = new View('atualizar.php');
        $this->view_variaveis_obrigatorias($view);

        $view->adicionar('body_class', 'index');
        $view->adicionar('notificacao',new Notificacao());
        $view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        $view->exibir();
    }

    public function baixar(){
        $view = new View('baixar.php');
        $this->view_variaveis_obrigatorias($view);

        $view->adicionar('body_class', 'index');
        $view->adicionar('notificacao',new Notificacao());
        $view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        $view->exibir();
    }

}
