<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/
class Controller_Padrao
{

	/**
	 * Construtor
	 *
	 * Inicializa as classes que serão utilizadas nas classes estendidas
	 */  
	public function __construct()
	{
		if ( ! isset($_SESSION))
		{
			session_start();
		}
	}


	/**
	 * Esta função é chamada pelas classes de herança para definir algumas variáveis antes de prosseguir
	 * com o resto do próprio método
	 * @param null $parametros
	 * @return void
	 */
	protected function iniciar($parametros = NULL)
	{
		
	}


	/**
	 * Jogamos para a view algumas variáveis que são utilizadas em todas as páginas
	 * @param $view
	 * @return void
	 */
	protected function view_variaveis_obrigatorias(&$view)
	{
		
		//-----

		// Variáveis de SEO para o caso de esquecermos nos controllers
		$view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');
		$view->adicionar('pagina_description', '');
		$view->adicionar('pagina_keywords', '');

		//-----

		// Informações para o colorbox de indicar página para amigo

		$view->adicionar('toolbar_indicar_amigo_url', 'http://'.$_SERVER['HTTP_HOST'].'/'.$_SERVER['REQUEST_URI']);

        if (isset($_SESSION['cliente_id']))
        {
            $cliente = new Model_Cliente($_SESSION['cliente_id']);
            $view->adicionar('cliente_login',$cliente);
        }


		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Cache-Control: no-cache");
		header("Pragma: no-cache");
		header("Content-Type: text/html; charset=UTF-8");

		
	}

	
} // end class