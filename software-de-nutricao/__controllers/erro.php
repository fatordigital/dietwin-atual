<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Erro extends Controller_Padrao
{
	public $numero;


	public function __construct($numero = NULL)
	{
		parent::__construct();

		$this->numero = $numero;
	}


    public function index($parametros = NULL)
	{
        $this->iniciar();

		$this->numero = isset($parametros->numero) ? (int) $parametros->numero : $this->numero;

		$view = new View('erro.php');

		$this->view_variaveis_obrigatorias($view);

		//-----

		$view->adicionar('body_class', 'http-erro '.$this->numero);

		switch ($this->numero)
		{
			case 404 :
				$view->adicionar('pagina_title', 'Erro 404 Conteúdo não encontrado - dietWin - Softwares de Nutrição');
				$view->adicionar('breadcrumb', 'Erro 404 - Conteúdo não encontrado');
				$view->adicionar('titulo', 'Erro 404');
				$view->adicionar('mensagem', 'O conteúdo que você está procurando não foi encontrado.');
				break;
			default :
				$view->adicionar('pagina_title', 'Erro - dietWin - Softwares de nutrição - Encontre o melhor software para você!');
				$view->adicionar('breadcrumb', 'Erro');
				$view->adicionar('titulo', 'Erro');
				$view->adicionar('mensagem', 'Ocorreu um erro.');
				break;
		}
		
		$view->adicionar('pagina_description', '');
		$view->adicionar('pagina_keywords', '');

        $view->exibir();
    }
	
} // end class