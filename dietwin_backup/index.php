<?php
error_reporting(E_ALL);
//ini_set('display_errors', 2);

// Include das condigurações de banco, email e diretórios
require_once 'config.php';

// error_reporting(E_ALL);
// ini_set('display_errors', 1);
ini_set('memory_limit', '-1');

// Início da lógica do sistema
// Monta a URL do sistema
$diretorios = explode('/', $_SERVER['PHP_SELF']);

// Retira o 'index.php'
if ($diretorios[count($diretorios)-1] == 'index.php')
{
	unset($diretorios[count($diretorios)-1]);
}

// ATENÇÃO: Não colocar o '/' final nos endereços abaixo
// Utilizado para imagens, css, javascript e outros includes
define('SITE_BASE', 'http://'.$_SERVER['HTTP_HOST'].implode('/', $diretorios));
// Utilizado para requisições pelo sistema, como formulários e links
define('SITE_URL', 'http://'.$_SERVER['HTTP_HOST'].implode('/', $diretorios).( IS_IIS ? '/index.php' : ''));


// Salva onde o sistema está sendo executado para buscar as configurações
if (strpos($_SERVER['HTTP_HOST'], 'dev') !== FALSE OR strpos($_SERVER['HTTP_HOST'], 'localhost') !== FALSE)
{
	$site_local = 'server';
}
else
{
	$site_local = 'cliente';
}
define ('SITE_LOCAL', $site_local);

// Carrega a função de Autoload para que seja possível instanciar uma classe sem fazer o include na mão
require 'sistema/autoloader.php';
// Classe que cuida das requisições e redirecionada para os lugares certos
require 'sistema/iniciar.php';
