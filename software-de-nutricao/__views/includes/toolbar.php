
<div class="toolbar">

    <?php
    if (isset($body_class)  AND $body_class != 'area-cliente'){
        ?>

        <div class="ferramentas">

            <ul>
                <li>
                    <a href="#" id="imprimir" title="Versão para impressão dessa página"><img src="<?php echo SITE_BASE ?>/views/imagens/toolbar-imprimir.gif" alt="dietwin" /></a>
                </li>
                <li>
                    <a href="#" id="colorbox-enviar-amigo" title="Enviar esta página por e-mail"><img src="<?php echo SITE_BASE ?>/views/imagens/toolbar-email.gif" alt="enviar amigo"/></a>
                    <!-- Conteúdo do colorbox do botão "Enviar para uma amigo por e-mail" -->
                    <div style="display:none">
                        <div class="colorbox-enviar-amigo">
                            <div class="topo"></div>
                            <div class="box">
                                <div class="conteudo-colorbox">
                                    <div class="titulo-colorbox">Indicar conteúdo para um amigo</div>
                                    <p class="borda-titulo">Se você deseja enviar essa página para um amigo, preencha o formulário abaixo.</p>
                                    <p><span>Página:</span> <a href="<?php echo $toolbar_indicar_amigo_url ?>" target="_blank"><?php echo Funcoes::cortar_texto(str_replace($toolbar_indicar_amigo_url,'', $pagina_title), 55) ?></a></p>
                                    <p><span>Endereço:</span> <a href="<?php echo $toolbar_indicar_amigo_url ?>" target="_blank"><?php echo Funcoes::cortar_texto(str_replace('http://','',$toolbar_indicar_amigo_url), 55) ?></a></p>
                                    <form method="post" action="<?php echo SITE_URL ?>/toolbar/enviar_amigo" id="form-enviar-amigo">

                                        <input type="hidden" name="url" id="url_amigo" value="<?php echo $toolbar_indicar_amigo_url ?>" />
                                        <input type="hidden" name="titulo" id="titulo_amigo" value="<?php echo $pagina_title ?>" />

                                        <label class="ajustar"><b>Nome do seu amigo:</b></label>
                                        <input type="text" name="nome_amigo" id="nome_amigo" class="obrigatorio" maxlength="100" />

                                        <label class="ajustar"><b>E-mail do seu amigo:</b></label>
                                        <input type="text" name="email_amigo" id="email_amigo" class="obrigatorio" maxlength="255" />

                                        <div class="clear"></div>

                                        <label><b>Seu nome:</b></label>
                                        <input type="text" name="seu_nome" id="seu_nome" class="obrigatorio" maxlength="100" />

                                        <label><b>Seu e-mail:</b></label>
                                        <input type="text" name="seu_email" id="seu_email" class="obrigatorio" maxlength="255" />

                                        <div class="clear"></div>

                                        <label class="ajustar"><b>Envie uma mensagem:</b></label>
                                        <textarea name="amigo_mensagem" id="amigo_mensagem" style="overflow:auto; resize:none"></textarea>

                                        <button type="submit">Enviar</button>

                                        <div class="clear"></div>
                                    </form>
                                    <div id="enviar_amigo_notification"></div>
                                </div>
                            </div>
                            <div class="inferior"></div>
                        </div>
                    </div>
                    <!-- Fim do colorbox do botão "Enviar para uma amigo por e-mail" -->
                </li>
            </ul>
        </div>
        <div class="ferramentas">
            <a href="#" class="semBorda" title="Aumentar fonte" id="aumenta_fonte"><img src="<?php echo SITE_BASE ?>/views/imagens/toolbar-aumentar.gif" alt="" /></a>
            <a href="#" title="Diminuir fonte" id="reduz_fonte"><img src="<?php echo SITE_BASE ?>/views/imagens/toolbar-diminuir.gif" alt="" /></a>
        </div>
        <div class="ferramentas">
            <span>Compartilhe</span>
            <!-- AddThis Button BEGIN -->
            <div class="addthis_toolbox addthis_default_style ">
                <a class="addthis_button_facebook" title="Compartilhe no Facebook"></a>
                <a class="addthis_button_twitter" title="Compartilhe no Twitter"></a>
                <a class="addthis_button_orkut" title="Compartilhe no Orkut"></a>
                <a class="addthis_button_google_plusone" title="Compartilhe no Google Buzz"></a>
                <a class="addthis_button_compact semBorda"></a>
                <!-- <a class="addthis_counter addthis_bubble_style"></a> -->
            </div>
            <!-- AddThis Button END -->
        </div>

      <?php
    }

    ?>


    <div class="clear"></div>      
</div>