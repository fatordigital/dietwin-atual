<?php include 'includes/cabecalho.php'; ?>

<body class="comprar comprar-<?php echo $classe ?>">

<?php include 'includes/topo.php'; ?>
	
    <div class="titulo-principal">
        <p><span>comprar o</span><br />dietWin</p>        
    </div>
    <div class="titulo-resumo">
        <p>	A praticidade de comprar online, com toda a comodidade e segurança que você precisa. Aproveite nossas promoções e adquira hoje mesmo a sua versão do dietWin.</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container">
    	
        <?php include 'includes/toolbar.php' ?>
        
        <div class="produto" id="tamanho-fonte">
        	<?php
            if ($classe == 'profissional')
            { ?>
                <img src="<?php echo SITE_BASE ?>/views/imagens/comprar-profissional-plus.jpg" alt="dietWin Profissional " />
                <div class="info">
                    <h1>dietWin <span>Profissional </span></h1>
                    <p>O objetivo principal do dietWin Profissional Plus é <b>auxiliar o nutricionista na tomada de decisão</b>, monitorando informações através do relacionamento de resultados obtidos para análise das avaliações executadas e gerar subsídios para facilitar o diagnóstico nutricional.<br />
                    <div class="aviso-profissional-plus"><b>Aviso importante:</b> Esta versão encontra-se em desenvolvimento. Atualizações frequentes serão disponibilizadas até a sua conclusão, por esta razão sua avaliação e sugestões são muito bem vindas para melhoria do sistema.</div>
                </div>
            <?php }
            else if ($classe == 'personal')
            { ?>
                <img src="<?php echo SITE_BASE ?>/views/imagens/comprar-personal.jpg" alt="dietWin Personal " />
                <div class="info">
                    <h1>dietWin <span>Personal </span></h1>
                    <p>É uma versão que atende todas as necessidades básicas da avaliação nutricional e prescrição dietética para todos os ciclos da vida. Um novo lay-out de telas é apresentado para uma nova proposta de atendimento clínico diferenciado onde a evolução da consulta fica disponível durante todo o atendimento. Permite a abertura de múltiplas fichas de pacientes e pesquisas facilitadas. ...</p>
                </div>
            <?php }
            else
            { ?>
                <img src="<?php echo SITE_BASE ?>/views/imagens/comprar-rotulo-de-alimentos.jpg" alt="dietWin Rótulo de Alimentos " />
                <div class="info">
                    <h1>dietWin <span>Rótulo de Alimentos </span></h1>
                    <p>Este programa tem a finalidade principal de calcular as Informações Nutricionais que devem estar presentes nos rótulos dos alimentos embalados de acordo as regras estabelecidas e as exigência Brasileiras determinadas pela ANVISA Resolução - RDC nº 360, de 23 de dezembro de 2003 - Aprova Regulamento Técnico sobre Rotulagem ... </p>
                </div>
            <?php } ?>

            <div class="opcoes">

                <!-- inicio do widget do Webchat Locaweb -->
                <div id="webchat_widget" style="float:right;"><!-- Coloque esse div onde você quer que o widget apareça. --></div>
                <script type="text/javascript">
                    (function() {
                        var wc = document.createElement('script'); wc.type = "text/javascript";
                        wc.src = 'https://dietwin.webchatlw.com.br/widget.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(wc, s);
                    })();
                </script>
                <div style="clear: both;"></div>


                <a href="#" class="mais-info link-mais-info" title="Solicite maiores informações sobre a ferramenta">Solicite maiores informações sobre a ferramenta</a>

                <!-- Conteúdo do colorbox do botão "Mais Informações" -->
                <div style="display:none">
                    <div class="colorbox-mais-info">
                        <div class="topo"></div>
                        <div class="box">
                            <div class="titulo-colorbox">Mais Informações</div>
                            <div class="conteudo-colorbox">

                                <p>Preencha o formulário abaixo para solicitar mais informações sobre este software.</p>
                                <form method="post" action="<?php echo SITE_URL ?>/profissional/mais_info" id="form-mais-info" >

                                    <label><b>Nome:</b></label>
                                    <input type="text" class="obrigatorio" name="info_nome" id="info_nome" maxlength="100" />

                                    <label><b>E-mail:</b></label>
                                    <input type="text" class="obrigatorio" name="info_email" id="info_email" maxlength="255" />

                                    <label><b>Telefone:</b></label>
                                    <input type="text" class="obrigatorio" name="info_telefone" id="info_telefone" maxlength="14" />

                                    <label><b>Cidade:</b></label>
                                    <input type="text" class="obrigatorio" name="info_cidade" id="info_cidade" maxlength="100" />

                                    <label><b>Mensagem:</b></label>
                                    <textarea type="text" class="obrigatorio" name="info_mensagem" id="info_mensagem" style="resize: vertical; overflow:auto"></textarea>

                                    <button type="submit">Enviar</button>
                                    <div class="clear"></div>
                                </form>

                                <div id="form_notification"></div>

                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="inferior"></div>
                    </div>
                </div>
                <!-- Fim do colorbox do botão "Mais Informações" -->
            </div>
            <div class="clear"></div>
        </div>       
        
        <div class="instalar">
        	<h2>Importante:</h2>           

			<p>Ao comprar o dietwin você estará adquirindo um software que permite a instalação em um único computador. Essa licença ficará registrada em seu CPF. Se você deseja instalar em mais computadores (em sua casa e no consultório, por exemplo), você precisa adquirir licenças adicionais.</p>
            
            <div class="mais-licensas">
                <ul>
                    <span class="aviso">Importante</span>
                    <li>Se deseja instalar em mais de 1  computador, <a href="<?php echo SITE_BASE?>/orcamento" title="Solicite uma proposta personalizada">clique aqui</a> e solicite uma proposta personalizada.</li>
                    <li>Para cada computador será gerado um número de série diferente. Não pode ser dividido com outra pessoa, somente para o mesmo CPF</li>
                </ul>
            </div>
            
            <?php /* <div class="grafico">
                <p class="textLicense">
                    <span id="contador">1</span>
                    <span id="computers">computador</span>
                </p>
                <div id="valueLicense" class="valueLicense">(incluso)</div>
                <div style="text-align:center"><img src="<?php echo SITE_BASE ?>/views/imagens/slider-divisorias.gif" /></div>
                <div class="sliderAviso">
                    <span>Importante:</span>
                    <p>Se deseja instalar em mais de 10 computadores, <a href="/comprar/orcamento.php">clique aqui</a> e solicite uma proposta personalizada.</p>
                    <p>Para cada computador será gerado um número de série diferente. Não pode ser dividido com outra pessoa, somente para o mesmo CPF.</p>
                </div>
            </div> */?>
            
            <div class="valor">
            	<div class="valores-numero">
                    <p class="total" id="valor_total"><span class="moeda">R$ </span><span id="valorTotal"><?php echo number_format($licenca,0) ?></span></p>
                    <p class="quantidade"><b>Para instalação em <span>1</span> computador</b></p>
                </div>
                <div class="cupon">
                	<p>Eu tenho um cupom de desconto</p>
                    <form method="post" action="<?php echo SITE_URL.'/software/'.$classe.'/comprar' ?>" id="atualizar_cupon" >
                        <input type="text" name="cupon" id="cupon" maxlength="255" value="" />
                        <button type="submit">Buscar</button>
                    </form>
                    <div class="mensagem"></div>
           			<div class="clear"></div>
                </div>
            </div>            
            
            <div class="clear"></div>
		</div>
        
        <img src="<?php echo SITE_BASE ?>/views/imagens/pagseguro.jpg" class="pagseguro" alt="Compra segura com Pagseguro" />
        <form action="<?php echo SITE_URL.'/software/'.$classe.'/dados-compra' ?>" method="post">
            <input type="hidden" id="input_licencas" name="licencas" value="0" />
            <input type="image" src="<?php echo SITE_BASE ?>/views/imagens/prosseguir-compra.jpg" class="prosseguir" value="Prosseguir com a compra." />
        </form>

        
        <div class="formas-pagamento">
            <img src="<?php echo SITE_BASE ?>/views/imagens/formas-pagamento.jpg" alt="Todas as formas de pagamento" />
        </div>
            
        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>  
</div>

<div id="div-chat" >
    <a id="chat-abrir" href="#" onClick="wc_popup(); return false;"><img src="<?php echo SITE_BASE; ?>/views/imagens/chat-passivo.png" alt="chat" /></a>
    <a id="chat-fechar" href="#"><img src="<?php echo SITE_BASE; ?>/views/imagens/chat-fechar.png" alt="chat" /></a>
</div>

<?php include 'includes/rodape.php'; ?>