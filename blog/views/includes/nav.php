<div class="nav-altura">
    <section class="nav">
        <a id="comeco-conteudo"></a>
        <div class="container">
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                <a href="<?php echo LINK_TO ?>/" title="Dietwin - Software de Nutrição">
                    <img src="<?php echo SITE_URL ?>/views/imagens/dietwin-logo-nav.png" alt="dietWin - Software de Nutrição" class="img-responsive">
                </a>
            </div>
            <div class="col-lg-6 col-md-6 hidden-sm hidden-xs text-right">
                <nav>
                    <?php
                    if($_SERVER['REQUEST_URI']!="/"){ $nav_link=LINK_TO; }else{ $nav_link=""; }
                    ?>
                    <ul>
                        <li><a href="<?php echo $nav_link; ?>software-de-nutricao/#caracteristicas" title="Caracteristicas">Caracteristicas</a></li>
                        <li><a href="<?php echo $nav_link; ?>software-de-nutricao/planos" title="Comprar">Comprar</a></li>
                        <li><a href="<?php echo $nav_link; ?>software-de-nutricao/#teste-gratis" title="Teste Grátis">Teste Grátis</a></li>
                        <li><a href="<?php echo $nav_link; ?>software-de-nutricao/" title="O Dietwin">O Dietwin</a></li>
                        <?php if(!empty($novidades)){ ?>
                        <li><a href="<?php echo $nav_link; ?>blog" title="Blog">Blog</a></li>
                        <?php } ?>
                        <li><a href="<?php echo $nav_link; ?>software-de-nutricao/#contato" title="Contato">Contato</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-4 col-md-4 hidden-sm hidden-xs text-right">
                <div class="telefone hidden-md">
                    <span>(51)</span> 3337.7908
                </div>
                <a class="exibir-telefone outros-botoes hidden-md" title="Clique aqui para ver nosso telefone">
                    <img src="<?php echo SITE_URL ?>/views/imagens/telefone-ico.png" alt="Telefone">
                </a>
                <a href="#" onClick="wc_popup()" class="outros-botoes" title="Clique aqui para falar conosco através do nosso chat">
                    <img src="<?php echo SITE_URL ?>/views/imagens/chat-ico.png" alt="Chat">
                </a>
                <a href="https://www.dietwin.com.br/software-de-nutricao/planos" class="comprar-agora botao-comprar">
                    <img src="<?php echo SITE_URL ?>/views/imagens/carrinho-compras.png" alt="Compre agora"> Comprar agora
                </a>
                <br />
                <?php if(isset($cliente_login)){ ?>
                <div class="usuario">
                    Olá <span><?php echo $cliente_login->nome; ?></span>, você está logado. <a href="<?php echo SITE_URL ?>/logout" style="color: #FFA54C">Sair</a>
                </div>
                <?php } ?>
            </div>
            <div class="hidden-lg hidden-md col-sm-4 col-xs-4 pull-right text-right">
                <img src="<?php echo SITE_URL ?>/views/imagens/menu-responsivo-ico.png" alt="Menu" class="icone-menu">
            </div>
        </div>
        <div class="col-sm-12 col-xs-12">
            <div class="menu-mobile">
                <nav>
                    <?php
                    if($_SERVER['REQUEST_URI']!="/"){ $nav_link=LINK_TO; }else{ $nav_link=""; }
                    ?>
                    <ul>
                        <li><a href="<?php echo $nav_link; ?>#caracteristicas" title="Caracteristicas">Caracteristicas</a></li>
                        <li><a href="<?php echo LINK_TO; ?>software-de-nutricao/planos" title="Comprar">Comprar</a></li>
                        <li><a href="<?php echo $nav_link; ?>#teste-gratis" title="Teste Grátis">Teste Grátis</a></li>
                        <li><a href="https://www.dietwin.com.br/#dietwin" title="O Dietwin">O Dietwin</a></li>
                        <li><a href="https://www.dietwin.com.br/#contato" title="Contato">Contato</a></li>
                    </ul>
                </nav>
            </div>
        </div>      
    </section>
</div>