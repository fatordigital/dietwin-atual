<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php' ?>
		
	<div id="main-content"> <!-- Main Content Section with everything -->

		<!-- Page Head -->
		<h2>Lista de Transações do PagSeguro</h2>

		<p id="page-intro">
			<?php if (is_null($transacao_status_id)) { ?>
				Abaixo estão listadas todas as transações do PagSeguro.
			<?php } elseif ($transacao_status_id == 1) { ?>
				Abaixo estão listadas todas as transações do PagSeguro que estão aguardando pagamento.
			<?php } elseif ($transacao_status_id == 2) { ?>
				Abaixo estão listadas todas as transações do PagSeguro que estão em análise.
			<?php } elseif ($transacao_status_id == 3) { ?>
				Abaixo estão listadas todas as transações do PagSeguro que estão pagas.
			<?php } elseif ($transacao_status_id == 4) { ?>
				Abaixo estão listadas todas as transações do PagSeguro que estão disponíveis.
			<?php } elseif ($transacao_status_id == 5) { ?>
				Abaixo estão listadas todas as transações do PagSeguro que estão em disputa.
			<?php } elseif ($transacao_status_id == 6) { ?>
				Abaixo estão listadas todas as transações do PagSeguro que foram devolvidas.
			<?php } elseif ($transacao_status_id == 7) { ?>
				Abaixo estão listadas todas as transações do PagSeguro que estão canceladas.
			<?php } ?>
		</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<form action="<?php echo $busca_url ?>" method="get" id="pagseguro_buscar_form">
			<fieldset>
				<p>
					<label for="buscar">Digite uma ou mais palavras-chave para buscar por uma transação realizada:</label>
					<input class="text-input medium-input" type="text" id="buscar" name="buscar" maxlength="100" value="<?php echo isset($buscar) ? $buscar : '' ?>" />
					<input class="button" type="submit" value="Buscar" />
				</p>
			</fieldset>
			<div class="clear"></div><!-- End .clear -->
		</form>

		<?php if (isset($buscar) AND ! is_null($buscar)) { ?>
			<p class="buscar-resultado">
				Foram encontrados <strong><?php echo $paginacao->linhas_total ?></strong> resultado(s) para a busca por "<strong><?php echo $buscar ?></strong>".
				<a title="Limpar a busca" href="<?php echo $busca_url ?>"><img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier-cross.png" alt="Limpar busca" /> Clique aqui para limpar a busca</a>
			</p>
		<?php } ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Transações
					<?php if ($transacao_status_id == 1) { ?>
						Aguardando Pagamento
					<?php } elseif ($transacao_status_id == 2) { ?>
						Em Análise
					<?php } elseif ($transacao_status_id == 3) { ?>
						Pagas
					<?php } elseif ($transacao_status_id == 4) { ?>
						Disponíveis
					<?php } elseif ($transacao_status_id == 5) { ?>
						Em Disputa
					<?php } elseif ($transacao_status_id == 6) { ?>
						Devolvidas
					<?php } elseif ($transacao_status_id == 7) { ?>
						Canceladas
					<?php } ?>
				</h3>


				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<table>

					<thead>
						<tr>
							<?php echo Funcoes::montar_th_ordenacao_listagem('pagseguro', $transacao_status, $paginacao, $buscar, 'Data', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('pagseguro', $transacao_status, $paginacao, $buscar, 'ID', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('pagseguro', $transacao_status, $paginacao, $buscar, 'Situação', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('pagseguro', $transacao_status, $paginacao, $buscar, 'Compra', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('pagseguro', $transacao_status, $paginacao, $buscar, 'Cliente', $ordenar_por, $ordem) ?>
							<th>Ações</th>
						</tr>
					</thead>

					<tfoot>
						<tr>
							<td colspan="6">

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_quantidades() ?>

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_links() ?>

								<div class="clear"></div>
							</td>
						</tr>
					</tfoot>

					<tbody>
						<?php
						if ($transacoes AND count($transacoes) > 0)
						{
							foreach ($transacoes as $transacao)
							{
							?>
								<tr>
									<td class="<?php echo $ordenar_por=='data' ? 'current' : '' ?>"><?php echo date('d/m/Y \à\s H:i:s', strtotime($transacao->data)) ?></td>
									<td class="<?php echo $ordenar_por=='id' ? 'current' : '' ?>">
										<a href="<?php echo SITE_URL ?>/fatorcms/pagseguro/visualizar/<?php echo $transacao->id ?>" title="Visualizar mais detalhes da transação">
											<?php echo $transacao->identificador ?>
										</a>
									</td>
									<td class="<?php echo $ordenar_por=='situacao' ? 'current' : '' ?>"><?php echo $transacao->transacao_status ?></td>
									<td class="<?php echo $ordenar_por=='compra' ? 'current' : '' ?>">
										<a href="<?php echo SITE_URL ?>/fatorcms/compras/visualizar/<?php echo $transacao->compra_id ?>" title="Visualiza a compra do cliente &quot;<?php echo $transacao->cliente_nome ?>&quot;">
											<?php echo $transacao->compra_codigo ?>
										</a>
									</td>
									<td class="<?php echo $ordenar_por=='cliente' ? 'current' : '' ?>">
                                        <a href="<?php echo SITE_URL ?>/fatorcms/clientes/editar/<?php echo $transacao->cliente_id ?>" title="Editar os dados do cliente &quot;<?php echo $transacao->cliente_nome ?>&quot;">
                                            <?php echo $transacao->cliente_nome ?>
                                        </a>
                                    </td>
									<td>
										<!-- Icons -->
										<a href="<?php echo SITE_URL ?>/fatorcms/pagseguro/visualizar/<?php echo $transacao->id ?>" title="Visualizar mais detalhes da transação">
                                            <img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier.png" alt="Visualizar" />
                                        </a>
									</td>
								</tr>
							<?php
							}
						}
						else
						{
							echo '<tr><td colspan="6">'.((isset($buscar) AND ! is_null($buscar)) ? 'Nenhuma transação encontrada para esta busca.' : 'Nenhuma transação cadastrada até o momento.' ).'</td></tr>';
						}
						?>
					</tbody>

				</table>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>