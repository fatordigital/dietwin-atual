<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Cliente extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        header('Location: '.SITE_URL.'/cliente/cadastro');
	}

    public function teste(){
        $cep = '90000000';
        $controller_frete = new Controller_Frete();
        

        if($controller_frete->cepValido($cep)){
            echo 'sim';
        }else{
            echo 'nao';
        }
    }

    public function cadastro($parametros)
    {
        $view = new View('cliente_cadastro.php');
        $this->view_variaveis_obrigatorias($view);

        $estados = new Model_Estado();
        $estados = $estados->select("SELECT * FROM {tabela_nome} ORDER BY {tabela_nome}.nome");

        $cidades = new Model_Cidade();
        $cidades = $cidades->select("SELECT * FROM {tabela_nome} WHERE {tabela_nome}.estado_id = 23 ORDER BY {tabela_nome}.nome");

        if(isset($parametros->nome)){
            $view->adicionar('campos', $parametros);
        }

        $view->adicionar('estados', $estados);
        $view->adicionar('cidades', $cidades);

        $view->adicionar('body_class', 'index');
        $view->adicionar('notificacao',new Notificacao());
        $view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        $view->exibir();
    }

    public function enviar_cadastro($parametros){
        if($parametros->nome!="" AND $parametros->email!="" AND $parametros->senha!="" AND $parametros->confirmar_senha!="" AND $parametros->profissao!="" AND $parametros->tipo_pessoa!="" AND $parametros->rg!="" AND $parametros->cpf!="" AND $parametros->endereco!="" AND $parametros->numero!="" AND $parametros->bairro!="" AND $parametros->estado!="" AND $parametros->cidade!="" AND $parametros->telefone!="" AND $parametros->celular!=""){
            if(sha1($parametros->senha)==sha1($parametros->confirmar_senha)){
                $email = new Model_Cliente();
                $email = $email->select("SELECT COUNT(*) as total, {tabela_nome}.* FROM {tabela_nome} WHERE email='".$parametros->email."'");
                if($email->total == 0 || ($email->total == 1 && $email->tipo == 0)){

                    $controller_frete = new Controller_Frete();

                    if($controller_frete->cepValido($parametros->cep)){                        

                        $cliente = new Model_Cliente();

                        $cliente->nome = ucwords(strtolower($parametros->nome));
                        $cliente->email = strtolower($parametros->email);
                        $cliente->senha = sha1($parametros->senha);
                        if($parametros->tipo_pessoa == 'Pessoa Física') {
                            $cliente->cpf = str_replace(array('.','-'), '', $parametros->cpf);
                            $cliente->rg = str_replace(array('.','-'), '', $parametros->rg);
                        } else {
                            $cliente->cnpj = str_replace(array('.','-','/'), '', $parametros->cpf);
                            $cliente->nome_empresa = $parametros->rg;
                        }
                        $cliente->telefone_principal = str_replace(array('.','-','(',')'), '', $parametros->telefone);
                        $cliente->telefone_extra = str_replace(array('.','-','(',')'), '', $parametros->celular);
                        $cliente->profissao = $parametros->profissao;
                        $cliente->cadastro_data = date('d/m/Y H:i:s');
                        $cliente->ultimo_login_data = date('d/m/Y H:i:s');
                        $cliente->cadastro_completo = '1';
                        $cliente->tipo = '1';

                        $cliente->insert();

                        $cliente_id = $cliente->id;

                        $endereco_principal = new Model_ClienteEndereco();
                        $endereco_principal->entrega = 0;
                        $endereco_principal->cliente_id = $cliente_id;
                        $endereco_principal->endereco = $parametros->endereco;
                        $endereco_principal->numero = $parametros->numero;
                        $endereco_principal->complemento = $parametros->complemento;
                        $endereco_principal->bairro = $parametros->bairro;
                        $endereco_principal->cidade_id = $parametros->cidade;
                        $endereco_principal->estado_id = $parametros->estado;
                        $endereco_principal->cep = (isset($parametros->cep) AND !empty($parametros->cep)) ? str_replace(array('-','.','_'),'',$parametros->cep) : NULL;

                        $endereco_principal->insert();

                        //rdstation
                        /*RdStation::addLeadConversionToRdstationCrm('d-form-cadastro', array(
                                                                                    'email'     => $cliente->email, 
                                                                                    'nome'      => $cliente->nome, 
                                                                                    'telefone'  => $parametros->telefone, 
                                                                                    'celular'   => $parametros->celular,
                                                                                    'profissao' => $parametros->profissao,
                                                                                    'cnpj'       => $parametros->cnpj,
                                                                                    'rg'        => $parametros->rg,
                                                                                    )
                                                                    );*/

                        // Efetua o login após o cadastro
                        $atualiza = new Model_Cliente();
                        $atualiza->id = $cliente->id;
                        $atualiza->ultimo_login_data = date('d/m/Y H:i:s');
                        $atualiza->update();
                        $_SESSION['cliente_id'] = $cliente->id;

                        $_SESSION['cadastro_sucesso'] = "Cadastro efetuado com sucesso.";
                        header("Location: ".SITE_URL."/comprar/dietwin#!");

                    }else{
                        $_SESSION['cadastro_error'] = "CEP válido ou inexistente, por favor corriga o campo CEP.";
                    }

                }else{
                    $_SESSION['cadastro_error'] = 'Este e-mail já está sendo utilizado. <a href="'.SITE_URL.'/esqueci-minha-senha" style="color:#fff; font-weight:bold">Clique aqui</a> para recuperar a sua senha.';
                }

            }else{
                $_SESSION['cadastro_error'] = "Senha e Confirmar senha não conferem.";
            }
        }else{
            $_SESSION['cadastro_error'] = "Preencha todos os campos corretamente.";
        }
        if(isset($_SESSION['cadastro_error'])){
            $this->cadastro($parametros);
        }else{
            header("Location: ".SITE_URL."/comprar/dietwin#!");
        }
    }

    public function editar_cadastro($parametros){

        if(isset($parametros->id)){

            $campos = $parametros;

        }else{

            if(isset($parametros->_0)){

                $cliente = new Model_Cliente();
                $cliente = $cliente->select('SELECT * FROM {tabela_nome} WHERE id='.$parametros->_0);

                //var_dump($cliente);

                //die;

                if($cliente){
                    
                    $campos = (object) array();

                    $campos->id = $cliente->id;
                    $campos->nome = $cliente->nome;
                    $campos->email = $cliente->email;
                    $campos->cpf = $cliente->cpf;
                    $campos->rg = $cliente->rg;
                    $campos->cnpj = $cliente->cnpj;
                    $campos->inscricao_estadual = $cliente->inscricao_estadual;
                    $campos->responsavel_nome = $cliente->responsavel_nome;
                    $campos->telefone_principal = $cliente->telefone_principal;
                    $campos->telefone_extra = $cliente->telefone_extra;
                    $campos->profissao = $cliente->profissao;               

                    if(empty($cliente->cpf)){
                        $campos->tipo_pessoa = 'Pessoa Jurídica';
                    }else{
                        $campos->tipo_pessoa = 'Pessoa Física';
                    }

                    $endereco_principal = new Model_ClienteEndereco();
                    $endereco_principal = $endereco_principal->select('SELECT * FROM {tabela_nome} WHERE cliente_id='.$parametros->_0);

                    if($endereco_principal){

                        $campos->endereco = $endereco_principal->endereco;
                        $campos->numero = $endereco_principal->numero;
                        $campos->complemento = $endereco_principal->complemento;
                        $campos->bairro = $endereco_principal->bairro;
                        $campos->cidade_id = $endereco_principal->cidade_id;
                        $campos->estado_id = $endereco_principal->estado_id;
                        $campos->cep = $endereco_principal->cep;

                    }else{
                        header("Location: ".SITE_URL);
                    }                

                    

                }else{
                    header("Location: ".SITE_URL);
                }

            }else{
                header("Location: ".SITE_URL);
            }

        }

        $estados = new Model_Estado();
        $estados = $estados->select("SELECT * FROM {tabela_nome} ORDER BY {tabela_nome}.nome");

        $cidades = new Model_Cidade();
        $cidades = $cidades->select("SELECT * FROM {tabela_nome} WHERE {tabela_nome}.estado_id = 23 ORDER BY {tabela_nome}.nome");

        $view = new View('cliente_editar_cadastro.php');
        $this->view_variaveis_obrigatorias($view);

        $view->adicionar('campos', $campos);

        $view->adicionar('estados', $estados);
        $view->adicionar('cidades', $cidades);

        $view->adicionar('body_class', 'index');
        $view->adicionar('notificacao',new Notificacao());
        $view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        $view->exibir();
    }

    public function atualizar_cadastro($parametros){
        if($parametros->nome!="" AND $parametros->profissao!="" AND $parametros->tipo_pessoa!="" AND $parametros->rg!="" AND $parametros->cpf!="" AND $parametros->endereco!="" AND $parametros->numero!="" AND $parametros->bairro!="" AND $parametros->estado_id!="" AND $parametros->cidade_id!="" AND $parametros->telefone_principal!="" AND $parametros->telefone_extra!=""){

            $controller_frete = new Controller_Frete();

            if($controller_frete->cepValido($parametros->cep)){     

                $id = $parametros->id;                   

                $cliente = new Model_Cliente();

                $cliente->id = $id;
                $cliente->email = $parametros->email;
                $cliente->nome = ucwords(strtolower($parametros->nome));                
                if($parametros->tipo_pessoa == 'Pessoa Física') {
                    $cliente->cpf = str_replace(array('.','-'), '', $parametros->cpf);
                    $cliente->rg = str_replace(array('.','-'), '', $parametros->rg);
                } else {
                    $cliente->cnpj = str_replace(array('.','-','/'), '', $parametros->cpf);
                    $cliente->nome_empresa = $parametros->rg;
                }
                $cliente->telefone_principal = str_replace(array('.','-','(',')'), '', $parametros->telefone_principal);
                $cliente->telefone_extra = str_replace(array('.','-','(',')'), '', $parametros->telefone_extra);
                $cliente->profissao = $parametros->profissao;                
                
                $cliente->update();

                $end_cliente = new Model_ClienteEndereco();
                $end_cliente = $end_cliente->select("SELECT * FROM {tabela_nome} WHERE cliente_id='".$id."'");

                if($end_cliente){

                    $endereco_principal = new Model_ClienteEndereco();                
                    $endereco_principal->id = $end_cliente->id;
                    $endereco_principal->cliente_id = $id;
                    $endereco_principal->endereco = $parametros->endereco;
                    $endereco_principal->numero = $parametros->numero;
                    $endereco_principal->complemento = $parametros->complemento;
                    $endereco_principal->bairro = $parametros->bairro;
                    $endereco_principal->cidade_id = $parametros->cidade_id;
                    $endereco_principal->estado_id = $parametros->estado_id;
                    $endereco_principal->cep = str_replace(array('-','.','_'),'',$parametros->cep);

                }

                $endereco_principal->update();

                //$_SESSION['cadastro_sucesso'] = "Cadastro atualizado com sucesso.";
                //header("Location: ".SITE_URL."/cadastro/editar-cadastro/".$id);

            }else{
                $_SESSION['cadastro_error'] = "CEP válido ou inexistente, por favor corriga o campo CEP.";
            }                    
            
        }else{
            $_SESSION['cadastro_error'] = "Preencha todos os campos corretamente.";
        }
        if(isset($_SESSION['cadastro_error'])){
            $this->editar_cadastro($parametros);
        }else{
            header("Location: ".SITE_URL."/comprar/dietwin#!");
        }
    }

	
	public function ajax_get_cidades($parametros) {
		$id = $parametros->_0;
		
		$cidades = new Model_Cidade();
        $cidades = $cidades->select("SELECT * FROM {tabela_nome} WHERE {tabela_nome}.estado_id = {$id} ORDER BY {tabela_nome}.nome");
        //var_dump($cidades);die;
		$select = '<option value="">Selecione</option>';
        $total = count($cidades);
        if ($total > 1)
        {
    		foreach($cidades as $cidade) {
    			$select .= '<option value="' . $cidade->id . '">' . $cidade->nome . '</option>';
    		}
        } else {
            $select .= '<option value="' . $cidades->id . '">' . $cidades->nome . '</option>';
        }
		echo $select;
	}



} // end class