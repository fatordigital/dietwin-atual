<?php
//---------- cópia do index.php

include_once '../../../../config.php';

define('SITE_BASE', 'http://'.$_SERVER['HTTP_HOST']);
// Utilizado para requisições pelo sistema, como formulários e links
define('SITE_URL', 'http://'.$_SERVER['HTTP_HOST']);

// Salva onde o sistema está sendo executado para buscar as configurações
if (strpos($_SERVER['HTTP_HOST'], 'fdserver') !== FALSE OR strpos($_SERVER['HTTP_HOST'], 'localhost') !== FALSE)
{
	$site_local = 'fdserver';
}
else
{
	$site_local = 'cliente';
}
define ('SITE_LOCAL', $site_local);

//---------- includes

include_once '../../../../sistema/log.php';
include_once '../../../../sistema/notificacao.php';
include_once '../../../../sistema/sistema.php';
include_once '../../../../sistema/bd.php';
include_once '../../../../fatorcms/models/padrao.php';
include_once '../../../../fatorcms/models/arquivo.php';
include_once '../../../../biblioteca/funcoes.php';
include_once '../../../../biblioteca/WideImage/WideImage.php';

//---------- lógica do upload

$arquivo_id = trim($_POST['arquivo_id']);

$log = new Log('Chegou no arquivo do uploadfy, arquivo: '.$arquivo_id);

$i = 0;

foreach ($_FILES as $arquivos)
{
    $arquivo = $arquivos['tmp_name'][$i];
    $erro = $arquivos['error'][$i];
    $nome = $arquivos['name'][$i];

    $log = new Log('tmp_name: '.$arquivo);
    $log = new Log('error: '.$erro);
    $log = new Log('name: '.$nome);


    if (move_uploaded_file($arquivo, '../../../../arquivos/arquivos/'.$nome)) {
        $banco_arquivo = new FatorCMS_Model_Arquivo($arquivo_id);
        $banco_arquivo->link = NULL;
        $banco_arquivo->arquivo = $nome;

        $log = new Log($banco_arquivo->id);
        if ( ! $banco_arquivo->update(array('link')))
        {
            new Log('Erro: ' . mysqli_error());
        }
    }
    else
    {
        $log = new Log('Erro ao fazer upload do arquivo. Arquivo_id= '.$arquivo_id);
    }

    $i++;
}

echo 1;