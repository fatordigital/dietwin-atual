<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_CMSUsuarios extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai e testa se o usuário está logado
	 */
	public function __construct()
	{
		parent::__construct();
		$this->verificar_usuario_logado();
		$this->verificar_usuario_permissao('usuarios-fatorcms');
	}
	
	
	/**
	 * Método padrão que não deve ser chamado diretamente. Se for, redireciona para a listagem
	 * @return void
	 */
	public function index()
	{
		header('Location: '.SITE_URL.'/fatorcms/cms-usuarios/listar');
		exit;
	}


	/**
	 * Método que faz a renderização da lista de registros do BD
	 * @param $parametros object
	 * @return void
	 */
	public function listar($parametros)
	{
		// Foi feita ou "contém" uma busca
		$clausula_where = ' TRUE ';
		$buscar = NULL;
		if (isset($parametros->buscar) AND strlen(trim($parametros->buscar)) > 0)
		{
			$buscar = trim($parametros->buscar);
			$buscar_aux = Funcoes::mysqli_escape(str_replace(' ', '%', $buscar));
			$clausula_where .= ' AND (nome LIKE "%'.$buscar_aux.'%" OR usuario LIKE "%'.$buscar_aux.'%")';
		}

		//-----
		// Ordenação dos resultados, podendo vir da página
		$ordenar_por = (isset($parametros->ordenar_por)) ? strtolower($parametros->ordenar_por) : 'nome'; // Valor padrão
		$ordem = isset($parametros->ordem) ? strtolower($parametros->ordem) : 'asc'; // Valor padrão

		//-----

		$cms_usuario = new FatorCMS_Model_CMSUsuario;
		// Define a cláusula do select
		$cms_usuario->clausula = '
			SELECT *
			FROM {tabela_nome}
			WHERE '.$clausula_where.'
			ORDER BY '.$ordenar_por.' '.$ordem;

		// Através da paginação vai verificar se existe a necessidade de dividir o resultado em várias páginas e se é necessário adicionar um LIMIT
		$paginacao = new Paginacao($cms_usuario, $parametros, 10, 10);

		// Executa a cláusula lá de cima
		$usuarios = $cms_usuario->select(NULL, TRUE); // NULL para não alterar a clásula já definida e TRUE para forçar retorno em array

		//-----

        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'cms-usuarios-lista.php'));
		$view->adicionar('body_class', 'usuarios-fatorcms lista');
		$view->adicionar('lateral_menu', array('item'=>'usuarios-fatorcms','link'=>'listar todos'));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$this->view_adicionar_obrigatorias($view);

		$view->adicionar('buscar', $buscar);
		$view->adicionar('ordenar_por', $ordenar_por);
		$view->adicionar('ordem', $ordem);
		$view->adicionar('cms_usuarios', $usuarios);
		$view->adicionar('paginacao', $paginacao);

		$view->exibir();
	}


	/**
	 * Dependendo do parâmetro, aprensenta a tela de edição ou de cadastro
	 * @param $parametros object
	 * @return void
	 */
	public function editar($parametros)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'cms-usuarios-form.php'));
        $view->adicionar('body_class', 'usuarios-fatorcms formulario');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$this->view_adicionar_obrigatorias($view);

		//-----

        if (isset($parametros->_0) AND is_numeric($parametros->_0))
        {
            // É atualização, mas só para administrador ou o próprio usuário
	        if ( ! $this->cms_usuario_logado->eh_administrador AND intval($this->cms_usuario_logado->id) != intval($parametros->_0))
	        {
		        new Notificacao('Somente um <strong>Administrador</strong> ou o <strong>próprio usuário</strong> podem editar seus dados.', 'error', TRUE);
                header('Location: '.SITE_URL.'/fatorcms/cms-usuarios/listar');
                exit;
	        }

	        $cms_usuario = new FatorCMS_Model_CMSUsuario($parametros->_0);
            $view->adicionar('cms_usuario', $cms_usuario);

	        // Informações extras
	        $view->adicionar('cms_usuario_permissoes', $this->carregar_usuario_permissoes($cms_usuario->id));

            $view->adicionar('lateral_menu', array('item'=>'usuarios-fatorcms','link'=>''));
        }
        else
        {
            // É cadastro, mas só para administradores
	        if ( ! $this->cms_usuario_logado->eh_administrador)
            {
                new Notificacao('Somente um <strong>Administrador</strong> pode cadastrar usuários.', 'error', TRUE);
                header('Location: '.SITE_URL.'/fatorcms/cms-usuarios/listar');
                exit;
            }

			$view->adicionar('cms_usuario', NULL);
	        $view->adicionar('cms_usuario_permissoes', NULL);

			$view->adicionar('lateral_menu', array('item'=>'usuarios-fatorcms','link'=>'cadastrar'));
        }

		// Informações extras
        $view->adicionar('cms_modulos', $this->carregar_modulos());

        $view->exibir();
    }


	/**
	 * Recebe os parâmetros do formulário e adiciona o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function cadastrar($parametros)
	{
		// Só para administradores
        if ( ! $this->cms_usuario_logado->eh_administrador)
        {
            new Notificacao('Somente um <strong>Administrador</strong> pode cadastrar usuários.', 'error', TRUE);
            header('Location: '.SITE_URL.'/fatorcms/cms-usuarios/listar');
            exit;
        }

		//-----

		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'cms-usuarios-form.php'));

		$this->view_adicionar_obrigatorias($view);

		if ($parametros AND isset($parametros->nome))
		{
			$cms_usuario = new FatorCMS_Model_CMSUsuario;
			$cms_usuario->carregar($parametros);

			// Campos especiais
			$cms_usuario->senha = sha1($cms_usuario->senha);
			$cms_usuario->eh_administrador = ($this->cms_usuario_logado->eh_administrador ? $cms_usuario->eh_administrador : FALSE); // Jogadinha para que ninguém se torne administrador

			$cms_usuario->colunas_mysqli_escape();

			//-----

			if ($cms_usuario->verificar_null(array('id')))
			{
				// Validações ---

				$cms_usuario_existe = new FatorCMS_Model_CMSUsuario;
				$cms_usuario_existe = $cms_usuario_existe->select('
					SELECT id, usuario
					FROM {tabela_nome}
					WHERE usuario = "'.$cms_usuario->usuario.'"
				');

				if ( $cms_usuario_existe AND ! is_null($cms_usuario_existe->id))
				{
					$notificacao = new Notificacao('O usuário <strong>'.$cms_usuario->usuario.'</strong> já consta no sistema. Por favor digite outro usuário.');
				}

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($cms_usuario->insert())
					{
						// Já que somente Administradores entram neste método, aqui não verificamos se ele é admin ou não.
						if ($this->relacionar_permissoes($cms_usuario->id, $parametros->cms_modulos_ids))
						{
							new Notificacao('Usuário <strong>'.$cms_usuario->nome.'</strong> cadastrado com sucesso.', 'success', TRUE);
							header('Location: '.SITE_URL.'/fatorcms/cms-usuarios/listar');
							exit;
						}
						else
						{
							$notificacao = new Notificacao('Ocorreu um erro ao definir as permissões do usuário.','error');
						}
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao salvar os dados do usuário.','error');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos os campos marcados são obrigatórios.','error');
			}

			// Para retornar ao formulário com valores inseridos
			$view->adicionar('cms_usuario', $cms_usuario);
			$view->adicionar('cms_usuario_permissoes', isset($parametros->cms_modulos_ids) ? $this->transformar_post_modulos_ids($parametros->cms_modulos_ids) : NULL);
		}
		else
		{
			$view->adicionar('cms_usuario', NULL);
		}

		$view->adicionar('cms_modulos', $this->carregar_modulos());

		$view->adicionar('body_class', 'usuarios-fatorcms formulario');
		$view->adicionar('lateral_menu', array('item'=>'usuarios-fatorcms','link'=>'cadastrar'));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
		$view->exibir();
	}
	

	/**
	 * Recebe os parâmetros do formulário e atualiza o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function atualizar($parametros)
	{
		// Só para administradores ou o próprio usuário
        if ( ! $this->cms_usuario_logado->eh_administrador AND intval($this->cms_usuario_logado->id) != intval($parametros->id))
        {
	        new Notificacao('Somente um <strong>Administrador</strong> ou o </strong>próprio usuário</strong> podem editar seus dados.', 'error', TRUE);
            header('Location: '.SITE_URL.'/fatorcms/cms-usuarios/listar');
            exit;
        }

		//-----

		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'cms-usuarios-form.php'));

		$this->view_adicionar_obrigatorias($view);

		if (isset($parametros->id) AND is_numeric($parametros->id))
		{
			$cms_usuario = new FatorCMS_Model_CMSUsuario;
			$cms_usuario->carregar($parametros);

			// Campos especiais
			$cms_usuario->senha = ( ! empty($cms_usuario->senha) ? sha1($cms_usuario->senha) : NULL);
			$cms_usuario->eh_administrador = ($this->cms_usuario_logado->eh_administrador ? $cms_usuario->eh_administrador : FALSE); // Jogadinha para que ninguém se torne administrador

			$cms_usuario->colunas_mysqli_escape();

			//-----
            
			if ($cms_usuario->verificar_null(array('senha')))
			{
				// Validações ---

				$cms_usuario_existe = new FatorCMS_Model_CMSUsuario;
				$cms_usuario_existe = $cms_usuario_existe->select(
					'SELECT id, usuario FROM {tabela_nome} '.
					'WHERE usuario = "'.$cms_usuario->usuario.'" AND id != '.$cms_usuario->id
				);

				if ( $cms_usuario_existe AND ! is_null($cms_usuario_existe->id))
				{
					$notificacao = new Notificacao('O usuário <strong>'.$cms_usuario->usuario.'</strong> já está cadastrado no sistema. Por favor digite outro usuário.');
				}
				
				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($cms_usuario->update())
					{
						// Fazemos os relacionamentos de permissão (só administradores, para que o usuário não exclua suas permissões por vir em branco)
						if ($this->cms_usuario_logado->eh_administrador)
						{
							if ( ! $this->relacionar_permissoes($cms_usuario->id, $parametros->cms_modulos_ids))
							{
								$notificacao = new Notificacao('Ocorreu um erro ao definir as permissões do usuário.','error');
							}
						}

						if ( ! isset($notificacao))
						{
							new Notificacao('Usuário <strong>'.$cms_usuario->nome.'</strong> atualizado com sucesso.', 'success', TRUE);
							header('Location: '.SITE_URL.'/fatorcms/cms-usuarios/listar');
							exit;
						}
						// Deixamos passar
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao atualizar os dados do usuário.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos os campos marcados são obrigatórios.');
			}

			// Para retornar ao formulário com valores inseridos
			$view->adicionar('cms_usuario', $cms_usuario);
			$view->adicionar('cms_usuario_permissoes', isset($parametros->cms_modulos_ids) ? $this->transformar_post_modulos_ids($parametros->cms_modulos_ids) : NULL);
		}
		else
		{
			$notificacao = new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention');
		}

        $view->adicionar('cms_modulos', $this->carregar_modulos());

		$view->adicionar('body_class', 'usuarios-fatorcms formulario');
		$view->adicionar('lateral_menu', array('item'=>'usuarios-fatorcms','link'=>''));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$view->exibir();
	}

	
	/**
	 * Recebe o ID do registro e excluir ele do banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function excluir($parametros)
	{
		// Só para administradores
        if ( ! $this->cms_usuario_logado->eh_administrador)
        {
            new Notificacao('Somente um <strong>Administrador</strong> pode excluir usuários.', 'error', TRUE);
            header('Location: '.SITE_URL.'/fatorcms/cms-usuarios/listar');
            exit;
        }

		//-----

		if (isset($parametros->_0) AND is_numeric($parametros->_0))
		{
			$cms_usuario = new FatorCMS_Model_CMSUsuario($parametros->_0);

			if ($cms_usuario AND ! is_null($cms_usuario->id))
			{
				if ($cms_usuario->delete())
				{
					// Excluímos os possíveis registros de permissão
					$cms_permissao = new FatorCMS_Model_CMSPermissao;
					$cms_permissao->cms_usuario_id = $cms_usuario->id;
					$cms_permissao->delete();

					new Notificacao('Usuário <strong>'.$cms_usuario->nome.'</strong> excluído com sucesso.', 'success', TRUE);
				}
				else
				{
					new Notificacao('Ocorreu um erro ao excluir o usuário <strong>'.$cms_usuario->nome.'</strong>.', 'error', TRUE);
				}
			}
			else
			{
				new Notificacao('Usuário não encontrado.', 'error', TRUE);
			}
		}
		else
		{
			new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention', 'error', TRUE);
		}

		header('Location: '.SITE_URL.'/fatorcms/cms-usuarios/listar');
		exit;
	}


	/****************************** MÉTODOS EXTRAS ******************************/


	/**
	 * Retorna todos os módulos cadastrados no banco, geralmente para completar <select>s
	 * @return array|bool
	 */
	protected function carregar_modulos()
	{
		$cms_modulo = new FatorCMS_Model_CMSModulo;
		return $cms_modulo->select('SELECT * FROM {tabela_nome} ORDER BY nome', TRUE);
	}


	/**
	 * Retorna todas as permissões para módulos de determinado usuário
	 * @param $cms_usuario_id
	 * @return array|bool
	 */
	protected function carregar_usuario_permissoes($cms_usuario_id)
	{
		if ( ! is_null($cms_usuario_id) AND is_numeric($cms_usuario_id))
		{
			$cms_permissao = new FatorCMS_Model_CMSPermissao;
			return $cms_permissao->select('SELECT * FROM {tabela_nome} WHERE cms_usuario_id = '.$cms_usuario_id, TRUE);
		}
		else
		{
			return FALSE;
		}
	}


	/**
	 * Quando ocorre um problema no cadastro ou atualização, e recarregamos a página para reapresentar o formulário, temos que
	 * pegar os módulos escolhidos e reapresentá-los
	 * @param $cms_modulos_ids
	 * @return array|bool
	 */
	protected function transformar_post_modulos_ids($cms_modulos_ids)
	{
		if ($cms_modulos_ids AND count($cms_modulos_ids) > 0)
		{
			$cms_usuario_permissoes = array();

			foreach ($cms_modulos_ids as $modulo_id)
			{
				$cms_permissao = new FatorCMS_Model_CMSPermissao;
				$cms_permissao->cms_modulo_id = $modulo_id;
				$cms_usuario_permissoes[] = $cms_permissao;
			}

			return $cms_usuario_permissoes;
		}

		return FALSE;
	}


	/**
	 * Cuida do relacionamento entre permissões e usuário. Realiza a exclusão dos relacionamentos prévios e recria eles
	 * @param $cms_usuario_id
	 * @param $cms_modulos_ids
	 * @return bool Retorna TRUE acontecendo pelo menos uma inserção
	 */
	protected function relacionar_permissoes($cms_usuario_id, $cms_modulos_ids)
	{
		$retorno = FALSE;

		// Apaga todos os registros relacionados ao usuário
		$cms_permissao = new FatorCMS_Model_CMSPermissao;
		$cms_permissao->cms_usuario_id = $cms_usuario_id;

		if ($cms_permissao->delete())
		{
			if (count($cms_modulos_ids) > 0)
			{
				// Percorre cada ingrediente para fazer o relacionamento de novo, se for o caso
				foreach ($cms_modulos_ids as $cms_modulo_id)
				{
					$cms_permissao->cms_modulo_id = $cms_modulo_id;

					if ($cms_permissao->insert())
					{
						// Tendo uma inserção pelo menos, já está ok
						$retorno = TRUE;
					}
					else
					{
						$log = new Log('Erro ao relacionar permissão com usuário. $cms_usuario_id='.$cms_usuario_id.'|$cms_modulo_id='.$cms_modulo_id);
					}

					// Limpamos o ID para a próxima inserção
					$cms_permissao->id = NULL;
				}
			}
			else
			{
				// Se não veio nenhum, pode ser apenas exclusão de todos, uma limpeza
				$retorno = TRUE;
			}
		}
		else
		{
			$log = new Log('Erro ao excluir as permissões de um usuário. $cms_usuario_id:'.$cms_usuario_id);
		}

		return $retorno;
	}

} // end class