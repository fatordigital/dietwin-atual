<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php'; ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

        <!-- Page Head -->
        <h2>Informações de uma compra</h2>
        <p id="page-intro">
            Abaixo estão listados todos os dados relacionados à compra de código <strong><?php echo $compra->codigo ?></strong>.
            <br /><em style="font-size:12px">Dica: clique nos títulos cinzas das caixas para esconder ou mostrar seu conteúdo.</em>
        </p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<div class="content-box column-left"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Compra</h3>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

                <p>
                    <strong>Data</strong><br />
                    <?php echo date('d/m/Y \à\s H:i:s', strtotime($compra->data)) ?>
                </p>

                <p>
                    <strong>Código</strong><br />
                    <?php echo $compra->codigo ?>
                </p>

                <p>
                    <strong>Forma de entrega</strong><br />
                    <?php echo $compra->entrega_forma ?>
                </p>

                <p>
                    <strong>Valor de entrega</strong><br />
                    R$ <?php echo number_format($compra->entrega_valor, 2, ',', '.') ?>
                </p>

                <p>
                    <strong>Método de pagamento</strong><br />
                    <?php echo $compra->pagamento_metodo.($compra->pagamento_metodo == 'Boleto' ? ' - Parcelas: '.$compra->boleto_parcelas.' - Data de Vencimento: Dia '.$compra->boleto_vencimento : '') ?>
                </p>

                <p>
                    <strong>Situação</strong><br />
                    <?php echo $compra->situacao ?>
                    <br /><small>Esta informação não é gerada automaticamente como a situação da transação no PagSeguro.</small>
                </p>


			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->



        <div class="content-box column-right"><!-- Start Content Box -->

            <div class="content-box-header">

                <h3>Produto</h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->

            <div class="content-box-content">

                <p>
                    <strong>Produto</strong><br />
                    <a href="<?php echo SITE_URL.'/fatorcms/produtos/editar/'.$compra_produto->id ?>"><?php echo $compra_produto->produto_nome ?></a>
                </p>

                <p>
                    <strong>Versão</strong><br />
                    <?php echo $compra_produto->versao ?>
                </p>

                <p>
                    <strong>Valor</strong><br />
                    R$ <?php echo number_format($compra_produto->valor, 2, ',', '.') ?>
                </p>

                <p>
                    <strong>Número de licenças</strong><br />
                    <?php echo $compra_produto->licenca_quantidade ?>
                </p>

                <p>
                    <strong>Valor de cada licença</strong><br />
                    R$ <?php echo number_format($compra_produto->licenca_valor, 2, ',', '.') ?>
                </p>

                <?php if ($compra_cupom) { ?>

                    <p>
                        <strong>Cupom de desconto</strong><br />
                        <a href="<?php echo SITE_URL.'/fatorcms/cupons/editar/'.$compra_cupom->cupom_id ?>"><?php echo $compra_cupom->codigo ?></a>
                        <?php echo ' - R$ '.number_format($compra_cupom->valor, 2, ',', '.') ?>
                    </p>

                <?php } ?>

                <p>
                    <strong>Valor total da compra (produto + licenças + entrega<?php if ($compra_cupom) { echo ' + cupom de desconto'; } ?>)</strong><br />
                    R$ <?php echo number_format($compra_produto->valor + ($compra_produto->licenca_quantidade * $compra_produto->licenca_valor) + $compra->entrega_valor - ($compra_cupom ? $compra_cupom->valor : 0), 2, ',', '.') ?>
                </p>

	            <?php if ( ! is_null($compra->onde_foi_adquirido)) { ?>

		            <p>
			            <strong>Onde foi adquirido?</strong><br />
			            <?php echo $compra->onde_foi_adquirido ?>
		            </p>

	            <?php } ?>

	            <?php if ( ! is_null($compra->informacao_adicional)) { ?>

		            <p>
			            <strong>Informações adicionais</strong><br />
			            <?php echo $compra->informacao_adicional ?>
		            </p>

	            <?php } ?>

            </div> <!-- End .content-box-content -->

        </div> <!-- End .content-box -->



        <div class="content-box column-left clear-left"><!-- Start Content Box -->

            <div class="content-box-header">

                <h3>Cliente</h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->

            <div class="content-box-content">

                <p>
                    <strong>Nome</strong><br />
                    <a href="<?php echo SITE_URL.'/fatorcms/clientes/editar/'.$compra->cliente_id ?>"><?php echo $compra_cliente->nome ?></a>
                </p>

                <?php if ( ! is_null($compra_cliente->cnpj)) { ?>

                    <p>
                        <strong>CNPJ</strong><br />
                        <?php echo Funcoes::formatar_cnpj($compra_cliente->cnpj) ?>
                    </p>

	                <p>
			            <strong>Inscrição estadual</strong><br />
			            <?php echo $compra_cliente->inscricao_estadual ?>
		            </p>

                    <p>
                        <strong>Responsável</strong><br />
                        <?php echo $compra_cliente->responsavel_nome ?>
                    </p>

                <?php }  ?>

                <p>
                    <strong>CPF</strong><br />
                    <?php echo Funcoes::formatar_cpf($compra_cliente->cpf) ?>
                </p>

                <p>
                    <strong>RG</strong><br />
                    <?php echo $compra_cliente->rg ?>
                </p>

                <p>
                    <strong>E-mail</strong><br />
                    <a href="mailto:<?php echo $compra_cliente->email ?>"><?php echo $compra_cliente->email ?></a>
                </p>

	            <?php if ( ! is_null($compra_cliente->email_secundario)) { ?>

		            <p>
			            <strong>E-mail secundário</strong><br />
			            <a href="mailto:<?php echo $compra_cliente->email_secundario ?>"><?php echo $compra_cliente->email_secundario ?></a>
		            </p>

	            <?php } ?>

                <p>
                    <strong>Telefone principal</strong><br />
                    <?php echo Funcoes::formatar_telefone($compra_cliente->telefone_principal) ?>
                </p>

                <?php if ( ! is_null($compra_cliente->telefone_extra)) { ?>

                    <p>
                        <strong>Telefone extra</strong><br />
                        <?php echo Funcoes::formatar_telefone($compra_cliente->telefone_extra) ?>
                    </p>

                <?php } ?>

				<?php if ( ! is_null($compra_cliente->profissao)) { ?>

		            <p>
			            <strong>Profissão</strong><br />
			            <?php echo $compra_cliente->profissao ?>
		            </p>

	            <?php } ?>

                <p>
                    <strong>Data de cadastro</strong><br />
                    <?php echo date('d/m/Y \à\s H:i:s', strtotime($compra_cliente->cadastro_data)) ?>
                </p>

                <p>
                    <strong>Data do último login no site</strong><br />
                    <?php echo ( ! is_null($compra_cliente->ultimo_login_data) ? date('d/m/Y \à\s H:i:s', strtotime($compra_cliente->ultimo_login_data)) : '<em>não disponível</em>') ?>
                </p>

                <p>
                    <strong>Pode fazer login no site?</strong><br />
                    <?php echo ($compra_cliente->ativo ? 'Sim' : 'Não') ?>
                </p>


            </div> <!-- End .content-box-content -->

        </div> <!-- End .content-box -->



        <div class="content-box column-right clear-right"><!-- Start Content Box -->

            <div class="content-box-header">

                <h3>Endereço de entrega</h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->

            <div class="content-box-content">

                <p>
                    <strong>Tipo</strong><br />
                    <?php echo $compra_endereco->tipo?>
                </p>

                <p>
                    <strong>Endereço</strong><br />
                    <a href="<?php echo SITE_URL.'/fatorcms/clientes/editar/'.$compra->cliente_id ?>/aba/2">
                        <?php echo $compra_endereco->endereco.', '.$compra_endereco->numero.( ! is_null($compra_endereco->complemento) ? ' / '.$compra_endereco->complemento : '') ?>
                    </a>
                </p>

                <p>
                    <strong>Bairro</strong><br />
                    <?php echo $compra_endereco->bairro ?>
                </p>

                <p>
                    <strong>Cidade</strong><br />
                    <?php echo $compra_endereco->cidade_nome ?>
                </p>

                <p>
                    <strong>Estado</strong><br />
                    <?php echo $compra_endereco->estado_nome ?>
                </p>

                <p>
                    <strong>CEP</strong><br />
                    <?php echo Funcoes::formatar_cep($compra_endereco->cep) ?>
                </p>

                <p>
                    <strong>Observação</strong><br />
                    <?php echo ( ! is_null($compra_endereco->observacao) ? $compra_endereco->observacao : '<em>não informou</em>') ?>
                </p>

            </div> <!-- End .content-box-content -->

        </div> <!-- End .content-box -->



        <?php if ($compra_pagseguro) { ?>

            <div class="content-box column-left clear-left"><!-- Start Content Box -->

                <div class="content-box-header">

                    <h3>PagSeguro</h3>

                    <div class="clear"></div>

                </div> <!-- End .content-box-header -->

                <div class="content-box-content">

                    <p>
                        <strong>Data da última atualização</strong><br />
                        <?php echo date('d/m/Y \à\s H:i:s', strtotime($compra_pagseguro->data)) ?>
                    </p>

                    <p>
                        <strong>Identificador</strong><br />
                        <?php echo $compra_pagseguro->identificador ?>
                    </p>

                    <p>
                        <strong>Situação</strong><br />
                        <?php echo $compra_pagseguro->transacao_status ?>
                    </p>

                    <p>
                        <strong>Tipo de pagamento</strong><br />
                        <?php echo $compra_pagseguro->pagamento_tipo ?>
                    </p>

                    <p>
                        <strong>Meio de pagamento</strong><br />
                        <?php echo $compra_pagseguro->pagamento_meio ?>
                    </p>

                </div> <!-- End .content-box-content -->

            </div> <!-- End .content-box -->

        <?php } ?>


        <div class="clear"></div>

<?php require_once 'fatorcms/views/includes/rodape.php' ?>