<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property int novidade_id
 * @property int tag_id
 */

class FatorCMS_Model_NovidadeTag extends FatorCMS_Model_Padrao
{
	protected $tabela_nome = 'novidades_tags';


	/****************************** NOVOS MÉTODOS ******************************/


	/**
	 * Executa o delete no banco, conforme o os parâmetros salvos no objeto
	 * @return bool
	 */
	public function delete()
	{
		// "Limpa" os valores
		$this->colunas_mysqli_escape();

		if ( ! is_null($this->id))
		{
			// Exclusão de um registro específico
			$sql = 'DELETE FROM '.$this->tabela_nome.' WHERE id = '.$this->id;
			return $this->query($sql);
		}
		elseif ( ! is_null($this->novidade_id))
		{
			// Exclusão de todos os registro de determinada novidade
			$sql = 'DELETE FROM '.$this->tabela_nome.' WHERE novidade_id = '.$this->novidade_id;
			return $this->query($sql);
		}
		elseif ( ! is_null($this->tag_id))
		{
			// Exclusão de todos os registro de determinada tag
			$sql = 'DELETE FROM '.$this->tabela_nome.' WHERE tag_id = '.$this->tag_id;
			return $this->query($sql);
		}

		// Se chegou aqui, é porque não excluiu nada
		return FALSE;
	}

} // end class