<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php'; ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

        <!-- Page Head -->
		<?php if (is_null($cliente) OR is_null($cliente->id)) { ?>
			<h2>Cadastro de um Cliente</h2>
			<p id="page-intro">Utilize o formulário abaixo para incluir um cliente no site.</p>
		<?php } else { ?>
			<h2>Edição de um Cliente</h2>
			<p id="page-intro">No formulário abaixo você pode alterar os dados de um cliente.</p>
		<?php } ?>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<?php if (is_null($cliente) OR is_null($cliente->id)) { ?>
					<h3>Dados de um cliente</h3>
				<?php } else { ?>
					<h3>Informações do cliente <?php echo $cliente ? $cliente->nome : '' ?> (Pessoa <?php echo (is_null($cliente->cnpj)) ? '<span style="color: red">Física</span>' : '<span style="color: blue">Jurídica</span>'; ?>)</h3>
				<?php } ?>
				
				<ul class="content-box-tabs">
					<li><a href="#tab1" <?php echo ( ! isset($aba) OR $aba == 1 ) ? 'class="default-tab"' : '' ?>>Dados</a></li>
					<?php if ( ! is_null($cliente) AND ! empty($cliente->id)) { ?>
						<li><a href="#tab2" <?php echo (isset($aba) AND $aba == 2 ) ? 'class="default-tab"' : '' ?>>Endereços</a></li>
						<?php if ($cms_usuario_logado->eh_administrador OR in_array('compras', $cms_usuario_logado_modulos)) { ?>
							<li><a href="#tab3" <?php echo (isset($aba) AND $aba == 3 ) ? 'class="default-tab"' : '' ?>>Compras</a></li>
						<?php } ?>
						<?php if ($cms_usuario_logado->eh_administrador OR in_array('interacoes', $cms_usuario_logado_modulos)) { ?>
							<li><a href="#tab4" <?php echo (isset($aba) AND $aba == 4 ) ? 'class="default-tab"' : '' ?>>Interações</a></li>
						<?php } ?>
                        <?php if (($cms_usuario_logado->eh_administrador OR in_array('compras', $cms_usuario_logado_modulos)) AND $plano_assistencia) { ?>
                            <li><a href="#tab5" <?php echo (isset($aba) AND $aba == 5 ) ? 'class="default-tab"' : '' ?>>Plano de assistência ativo</a></li>
                        <?php } ?>
                        <?php if (($cms_usuario_logado->eh_administrador OR in_array('recorrencia', $cms_usuario_logado_modulos))) { ?>
                            <li><a href="#tab6" <?php echo (isset($aba) AND $aba == 6 ) ? 'class="default-tab"' : '' ?>>Recorrência</a></li>
                        <?php } ?>
					<?php } ?>
				</ul>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<div class="tab-content <?php echo ( ! isset($aba) OR $aba == 1 ) ? 'default-tab' : '' ?>" id="tab1">
			
					<form action="<?php echo SITE_URL ?>/fatorcms/clientes/<?php echo (is_null($cliente) OR is_null($cliente->id)) ? 'cadastrar' : 'atualizar' ?>" method="post" id="cliente_form">

						<fieldset class="column-left">

							<?php if ( ! is_null($cliente)) { ?>
								<input type="hidden" name="id" value="<?php echo $cliente->id ?>" />
							<?php } ?>

							<p>
								<label for="nome">Nome</label>
								<input class="text-input large-input required" type="text" id="nome" name="nome" maxlength="100" value="<?php echo $cliente ? $cliente->nome : '' ?>" />
							</p>

							<p>
								<label for="email">E-mail</label>
								<input class="text-input large-input required" type="text" id="email" name="email" maxlength="255" value="<?php echo $cliente ? $cliente->email : '' ?>" />
							</p>

							<p>
								<label for="email_secundario">E-mail secundário</label>
								<input class="text-input large-input" type="text" id="email_secundario" name="email_secundario" maxlength="255" value="<?php echo $cliente ? $cliente->email_secundario : '' ?>" />
								<br /><small>Este endereço só está disponível dentro do FatorCMS.</small>
							</p>

							<p>
								<label for="senha">Senha</label>
								<input class="text-input medium-input <?php echo (is_null($cliente) OR is_null($cliente->id)) ? 'required' : '' ?>" type="password" id="senha" name="senha" maxlength="100" />

                                <?php if (is_null($cliente) OR is_null($cliente->id)) { ?>
                                <p>Gerar senha aleatória: <input type="checkbox" id="aleatoria" name="senha_aleatoria" value="aleatoria"></p>
                                <?php } ?>
                            </p>

							<p>
								<label for="rg">RG</label>
								<input class="text-input medium-input required" type="text" id="rg" name="rg" maxlength="20" value="<?php echo $cliente ? $cliente->rg : '' ?>" />
								<br /><small>Utilize este campo também para o <strong>Responsável</strong> da empresa.</small>
							</p>

							<p>
								<label for="cpf">CPF</label>
								<input class="text-input medium-input required" type="text" id="cpf" name="cpf" maxlength="14" value="<?php echo $cliente ? $cliente->cpf : '' ?>" />
								<br /><small>Utilize este campo também para o <strong>Responsável</strong> da empresa.</small>
							</p>

                            <p>
                                <label for="nome_empresa">Razão Social</label>
                                <input class="text-input medium-input" type="text" id="nome_empresa" name="nome_empresa" maxlength="18" value="<?php echo $cliente ? $cliente->nome_empresa : '' ?>" />
                                <br /><small>Deixe este campo em branco caso o cadastro seja de uma pessoa física.</small>
                            </p>

							<p>
								<label for="inscricao_estadual">Inscrição Estadual</label>
								<input class="text-input medium-input" type="text" id="inscricao_estadual" name="inscricao_estadual" maxlength="20" value="<?php echo $cliente ? $cliente->inscricao_estadual : '' ?>" />
								<br /><small>Deixe este campo em branco caso o cadastro seja de uma pessoa física.</small>
							</p>

							<p>
								<label for="cnpj">CNPJ</label>
								<input class="text-input medium-input" type="text" id="cnpj" name="cnpj" value="<?php echo $cliente ? $cliente->cnpj : '' ?>" />
								<br /><small>Deixe este campo em branco caso o cadastro seja de uma pessoa física.</small>
							</p>


							<p>
								<label for="responsavel_nome">Responsável na empresa</label>
								<input class="text-input large-input" type="text" id="responsavel_nome" name="responsavel_nome" maxlength="100" value="<?php echo $cliente ? $cliente->responsavel_nome : '' ?>" />
								<br /><small>Deixe este campo em branco caso o cadastro seja de uma pessoa física.</small>
							</p>

							<p>
								<label for="telefone_principal">Telefone principal</label>
								<input class="text-input medium-input required" type="text" id="telefone_principal" name="telefone_principal" maxlength="14" value="<?php echo $cliente ? $cliente->telefone_principal : '' ?>" />
							</p>

							<p>
								<label for="telefone_extra">Telefone extra</label>
								<input class="text-input medium-input" type="text" id="telefone_extra" name="telefone_extra" maxlength="14" value="<?php echo $cliente ? $cliente->telefone_extra : '' ?>" />
							</p>

							<p>
								<label for="profissao">Profissão</label>
								<input class="text-input large-input required" type="text" id="profissao" name="profissao" maxlength="100" value="<?php echo $cliente ? $cliente->profissao  : '' ?>" />
								<br /><small>A profissão do cliente ou do responsável na empresa.</small>
							</p>

						</fieldset>

						<fieldset class="column-right">

							<p>
								<label for="nome">Data de cadastro</label>
								<em><?php echo ($cliente AND ! is_null($cliente->cadastro_data)) ? $cliente->cadastro_data : 'não disponível' ?></em>
							</p>

							<p>
								<label for="nome">Data do último login</label>
								<em><?php echo ($cliente AND ! is_null($cliente->ultimo_login_data)) ? $cliente->ultimo_login_data : 'não disponível' ?></em>
							</p>

							<p>
								<label for="ativo_1">Pode fazer login no site?</label>
								<input type="radio" name="ativo" id="ativo_1" value="1" <?php echo ($cliente AND $cliente->ativo) ? 'checked="checked"' : '' ?> /> <span class="sim">Sim</span>
								<input type="radio" name="ativo" id="ativo_0" value="0" <?php echo ( ! $cliente OR ! $cliente->ativo) ? 'checked="checked"' : '' ?> /> <span class="nao">Não</span>
							</p>
                            <?php /*
                            <p>
                                <label for="arquivo_1">Pode fazer download dos arquivos padrão no site?</label>
                                <input type="radio" name="arquivos" id="arquivo_1" value="1" /> <span class="sim">Sim</span>
                                <input type="radio" name="arquivos" id="arquivo_2" value="0" checked="checked" /> <span class="nao">Não</span>
                            </p>
                            */ ?>
                            <?php if (!is_null($cliente) AND !is_null($cliente->id)) { ?>
                            <div class="content-box"><!-- Start Content Box -->

                                <div class="content-box-header">

                                    <h3>Tipo de Cadastro</h3>
                                    <div class="clear"></div>

                                </div> <!-- End .content-box-header -->

                                <div class="content-box-content">
                                    <p>
                                        <label for="tipo_1">Marque o tipo de cadastro</label>
                                        <input type="radio" name="tipo" id="tipo_1" value="1" <?php echo ($cliente AND $cliente->tipo) ? 'checked="checked"' : '' ?> /> <span class="sim">Cliente</span>
                                        <input type="radio" name="tipo" id="tipo_0" value="0" <?php echo ( ! $cliente OR ! $cliente->tipo) ? 'checked="checked"' : '' ?> /> <span class="nao">Prospect</span>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>

						</fieldset>

						<fieldset class="clear">

							<p>
								<input class="button" type="submit" value="<?php echo (is_null($cliente) OR is_null($cliente->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
							</p>

						</fieldset>

					</form>
					
				</div>


				<?php if ( ! is_null($cliente) AND ! empty($cliente->id)) { // ------------------------------------------------------------------------------------- ?>

					<div class="tab-content tab-enderecos <?php echo ( isset($aba) AND $aba == 2 ) ? 'default-tab' : '' ?>" id="tab2">

						<table>

							<thead>
								<tr>
									<th>Endereço</th>
									<th>Localidade</th>
									<th>Observação</th>
									<th>Para entrega?</th>
									<th>Ações</th>
								</tr>
							</thead>

							<tfoot>
								<tr>
									<td colspan="5">
										<div class="clear"></div>
									</td>
								</tr>
							</tfoot>

							<tbody>
								<?php
		                        if($cliente_enderecos AND count($cliente_enderecos) > 0)
								{
									foreach ($cliente_enderecos as $endereco)
									{
									?>
										<tr>
											<td><?php echo $endereco->endereco.', '.$endereco->numero.(!is_null($endereco->complemento)?' / '.$endereco->complemento:'') ?></td>
											<td><?php echo $endereco->bairro.' - '.$endereco->cidade_nome.' / '.$endereco->estado_sigla.'<br />'.Funcoes::formatar_cep($endereco->cep) ?></td>
											<td><?php echo ( ! empty($endereco->observacao) ? $endereco->observacao : '<em>Não informada</em>') ?></td>
											<td><span class="<?php echo (($endereco->entrega OR count($cliente_enderecos) == 1) ? 'sim': 'nao') ?>"><?php echo (($endereco->entrega OR count($cliente_enderecos) == 1) ? 'Sim': 'Não') ?></span></td>
											<td>
												<a href="<?php echo SITE_URL ?>/fatorcms/clientes/endereco-editar/<?php echo $endereco->id ?>" title="Editar o endereço &quot;<?php echo $endereco->endereco ?>&quot;" class="endereco_editar" id="endereco_<?php echo $endereco->id ?>">
													<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" />
												</a>
												<a href="<?php echo SITE_URL ?>/fatorcms/clientes/endereco-excluir/<?php echo $endereco->id ?>" title="Excluir o endereço &quot;<?php echo $endereco->endereco ?>&quot;" class="item-confirmar">
													<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/cross.png" alt="Excluir" />
												</a>
											</td>
										</tr>
									<?php
									}
								}
								else
								{
									echo '<tr><td colspan="5">Nenhum endereço cadastrado até o momento.</td></tr>';
								}
								?>
							</tbody>

						</table>

						<!-- Formulário para cadastro/edição de um endereço -->

						<div class="notificacao_js"></div> <!-- DIV para inserções de notificações via jQuery -->

						<h4>Formulário de Endereços</h4>
						
						<p>Utilize o formulário abaixo para cadastrar um novo endereço para o cliente. Ou clique na edição de um dos itens acima.</p>

						<form action="<?php echo SITE_URL ?>/fatorcms/clientes/<?php echo ( ! isset($cliente_endereco) OR is_null($cliente_endereco->id)) ? 'endereco-cadastrar' : 'endereco-atualizar' ?>" method="post" id="cliente_endereco_form">

							<fieldset>

								<input type="hidden" name="cliente_id" value="<?php echo $cliente->id ?>" />

								<?php if (isset($cliente_endereco) AND  ! is_null($cliente_endereco)) { ?>
									<input type="hidden" name="id" value="<?php echo $cliente_endereco->id ?>" />
								<?php } ?>

								<?php /* <p>
									<label for="tipo">Tipo</label>
									<select class="text-input small-input required" id="tipo" name="tipo">
										<option value="Residencial <?php echo (isset($cliente_endereco) AND $cliente_endereco->tipo == 'Residencial') ? 'selected="selected"' : '' ?>">Residencial</option>
										<option value="Comercial" <?php echo (isset($cliente_endereco) AND $cliente_endereco->tipo == 'Comercial') ? 'selected="selected"' : '' ?>>Comercial</option>
										<option value="Outro" <?php echo (isset($cliente_endereco) AND $cliente_endereco->tipo == 'Outro') ? 'selected="selected"' : '' ?>>Outro</option>
									</select>
								</p> */ ?>

								<p>
									<label for="endereco">Endereço</label>
									<input class="text-input large-input required" type="text" id="endereco" name="endereco" maxlength="200" value="<?php echo isset($cliente_endereco) ? $cliente_endereco->endereco : '' ?>" />
								</p>

								<p>
									<label for="numero">Número</label>
									<input class="text-input small-input required" type="text" id="numero" name="numero" maxlength="7" value="<?php echo isset($cliente_endereco) ? $cliente_endereco->numero : '' ?>" />
								</p>

								<p>
									<label for="complemento">Complemento</label>
									<input class="text-input medium-input" type="text" id="complemento" name="complemento" maxlength="100" value="<?php echo isset($cliente_endereco) ? $cliente_endereco->complemento : '' ?>" />
								</p>

								<p>
									<label for="bairro">Bairro</label>
									<input class="text-input medium-input required" type="text" id="bairro" name="bairro" maxlength="100" value="<?php echo isset($cliente_endereco) ? $cliente_endereco->bairro : '' ?>" />
								</p>

								<p>
									<label for="cnpj">Estado</label>
									<select class="text-input medium-input required" id="estado_id" name="estado_id">
										<option value="">Selecione um estado</option>
										<?php
										if ($estados AND count($estados) > 0)
										{
											foreach ($estados as $estado)
											{
												echo '<option value="'.$estado->id.'">'.$estado->nome.'</option>';
											}
										}
										?>
									</select>
								</p>

								<p>
									<label for="cpf">Cidade</label>
									<select class="text-input medium-input required" id="cidade_id" name="cidade_id">
										<option value="">Selecione um estado acima</option>
									</select>
								</p>

								<p>
									<label for="cep">CEP</label>
									<input class="text-input small-input required" type="text" id="cep" name="cep" maxlength="9" value="<?php echo isset($cliente_endereco) ? Funcoes::formatar_cep($cliente_endereco->cep) : '' ?>" />
								</p>

								<p>
									<label for="observacao">Observação</label>
									<textarea class="text-input large-input" id="observacao" name="observacao" rows="10" cols="79"><?php echo isset($cliente_endereco) ? $cliente_endereco->observacao : '' ?></textarea>
								</p>

								<p>
									<input class="button" type="submit" value="<?php echo ( ! isset($cliente_endereco) OR is_null($cliente_endereco->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
								</p>

							</fieldset>

						</form>

					</div> <!-- tab2 ------------------------------------------------------------ -->

					<?php if ($cms_usuario_logado->eh_administrador OR in_array('compras', $cms_usuario_logado_modulos)) { ?>

						<div class="tab-content tab-compras <?php echo (isset($aba) AND $aba == 3 ) ? 'default-tab' : '' ?>" id="tab3">

							<table>

								<thead>
									<tr>
										<th>Data</th>
										<th>Código</th>
										<th>Produto</th>
										<th>Valor total</th>
										<th>Forma de Entrega</th>
										<th>Mét. de Pagamento</th>
										<th>Situação</th>
			                            <th>Ações</th>
									</tr>
								</thead>

								<tfoot>
									<tr>
										<td colspan="8">
											<div class="clear"></div>
										</td>
									</tr>
								</tfoot>

								<tbody>
									<?php
			                        if($cliente_compras AND count($cliente_compras) > 0)
									{
										foreach ($cliente_compras as $compra)
										{
										?>
											<tr>
												<td><?php echo date('d/m/Y \à\s H:i:s', strtotime($compra->data))?></td>
												<td>
													<a href="<?php echo SITE_URL ?>/fatorcms/compras/visualizar/<?php echo $compra->id ?>" title="Visualizar a compra &quot;<?php echo $compra->codigo ?>&quot;">
														<?php echo $compra->codigo ?>
													</a>
												</td>
                                                <td title="Entre parênteses está o número de licenças adquiridas junto com o produto">
                                                    <a href="<?php echo SITE_URL ?>/fatorcms/produtos/editar/<?php echo $compra->produto_id ?>" title="Visualizar a compra &quot;<?php echo $compra->codigo ?>&quot;">
                                                        <?php echo $compra->produto_nome ?>
                                                    </a>
                                                    <?php echo ' ('.$compra->licenca_quantidade.')' ?>
                                                </td>
												<td nowrap="nowrap">R$ <?php echo number_format($compra->valor_total + $compra->entrega_valor, 2, ',', '.') ?></td>
												<td><?php echo $compra->entrega_forma ?></td>
												<td><?php echo $compra->pagamento_metodo ?></td>
												<td><?php echo $compra->situacao.' / '.( ! empty($compra->transacao_status) ? $compra->transacao_status : ' -- ') ?></td>
												<td>
													<a href="<?php echo SITE_URL ?>/fatorcms/compras/visualizar/<?php echo $compra->id ?>" title="Visualizar a compra &quot;<?php echo $compra->codigo ?>&quot;">
														<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier.png" alt="Visualizar" />
													</a>
													<a href="<?php echo SITE_URL ?>/fatorcms/clientes/compra-editar/<?php echo $compra->id ?>" class="compra_editar" title="Editar a compra &quot;<?php echo $compra->codigo ?>&quot;">
														<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" />
													</a>
												</td>
											</tr>
										<?php
										}
									}
									else
									{
										echo '<tr><td colspan="8">Nenhuma compra feita até o momento.</td></tr>';
									}
									?>
								</tbody>

							</table>

							<!-- Formulário para cadastro/edição de uma compra -->

							<div class="notificacao_js"></div> <!-- DIV para inserções de notificações via jQuery -->

							<h4>Formulário de Compras</h4>

							<p>Utilize o formulário abaixo para cadastrar uma nova compra para o cliente. Ou clique na edição de um dos itens acima.</p>

							<form action="<?php echo SITE_URL ?>/fatorcms/clientes/<?php echo ( ! isset($cliente_compra) OR is_null($cliente_compra->id)) ? 'compra-cadastrar' : 'compra-atualizar' ?>" method="post" id="cliente_compra_form">

								<fieldset class="column-left">

									<input type="hidden" name="cliente_id" value="<?php echo $cliente->id ?>" />

									<?php if (isset($cliente_compra) AND  ! is_null($cliente_compra)) { ?>
										<input type="hidden" name="id" value="<?php echo $cliente_compra->id ?>" />
									<?php } ?>

									<p>
										<label for="data">Data</label>
										<input class="text-input small-input required" type="text" id="data" name="data" maxlength="10" value="<?php echo isset($cliente_compra) ? $cliente_compra->data : '' ?>" />
									</p>

									<p>
										<label for="produto_id">Produto</label>
										<span id="produto_atual" class="esconder valor-atual"></span>
										<small id="produto_atual_ajuda" class="esconder"><br />Se for necessário alterar o produto da compra, utilize o campo abaixo para selecioná-lo.</small>
										<select class="text-input large-input required" id="produto_id" name="produto_id">
											<?php
											if($produtos AND count($produtos) > 0)
											{
												foreach ($produtos as $produto)
												{
													echo '<option value="'.$produto->id.'">'.$produto->nome.' (versão: '.$produto->versao.')'.'</option>';
												}
											}
											?>
										</select>
									</p>

									<p>
										<label for="produto_versao">Versão</label>
										<input class="text-input small-input" type="text" id="produto_versao" name="produto_versao" maxlength="10" value="<?php echo isset($cliente_compra) ? $cliente_compra->valor_entrega : '' ?>" />
										<br /><small>Deixe este campo em branco para que seja utilizada a versão atual do software selecionado acima.</small>
									</p>

									<p>
										<label for="produto_licencas">Número de licenças</label>
										<input class="text-input small-input" type="text" id="produto_licencas" name="produto_licencas" maxlength="2" value="<?php echo isset($cliente_compra) ? $cliente_compra->valor_entrega : '' ?>" />
									</p>

									<p>
										<label for="pagamento_metodo">Método de pagamento</label>
										<select class="text-input medium-input required" id="pagamento_metodo" name="pagamento_metodo">
											<option value="Boleto">Boleto</option>
											<option value="PagSeguro">PagSeguro</option>
											<option value="Cheque">Cheque</option>
											<option value="Depósito / Transferência">Depósito / Transferência</option>
										</select>
									</p>

									<p>
										<label for="boleto_parcelas">Parcelas do boleto</label>
										<input class="text-input small-input" type="text" id="boleto_parcelas" name="boleto_parcelas" maxlength="2" value="<?php echo isset($cliente_compra) ? $cliente_compra->boleto_parcelas : '' ?>" />
										<br /><small>Preencher somente se o pagamento da compra for feito através do <strong>Boleto Bancário</strong>.</small>
									</p>

                                    <p>
                                        <label for="boleto_vencimento">Data de vencimento das parcelas</label>
                                        <input class="text-input small-input" type="text" id="boleto_vencimento" name="boleto_vencimento" maxlength="2" value="<?php echo isset($cliente_compra) ? $cliente_compra->boleto_vencimento : '' ?>" />
                                        <br /><small>Preencher somente se o pagamento da compra for feito através do <strong>Boleto Bancário</strong>.</small>
                                    </p>

									<p>
										<label for="situacao">Situação</label>
										<select class="text-input medium-input required" id="situacao" name="situacao">
											<option value="">Selecionar uma opção</option>
											<option value="Aguardando pagamento">Aguardando pagamento</option>
											<option value="Aprovada">Aprovada</option>
											<option value="Cancelada">Cancelada</option>
											<option value="Pedido enviado">Pedido enviado</option>
										</select>
									</p>

								</fieldset>

								<fieldset class="column-right">

									<p>
										<label for="cliente_endereco_id">Endereço para entrega</label>
										<span id="endereco_atual" class="esconder valor-atual"></span>
										<small id="endereco_atual_ajuda" class="esconder"><br />Se for necessário alterar o endereço da compra, utilize o campo abaixo para selecioná-lo.</small>
										<select class="text-input large-input required" id="cliente_endereco_id" name="cliente_endereco_id">
											<?php
											if($cliente_enderecos AND count($cliente_enderecos) > 0)
											{
												foreach ($cliente_enderecos as $endereco)
												{
													echo '<option value="'.$endereco->id.'">'.
															$endereco->endereco.', '.$endereco->numero.(!is_null($endereco->complemento)?' / '.$endereco->complemento:'').' - '.
															$endereco->bairro.' - '.$endereco->cidade_nome.' / '.$endereco->estado_sigla.
															'</option>';
												}
											}
                                            else
                                            {
                                                echo '<option value="">Nenhum endereço cadastrado até o momento</option>';
                                            }
											?>
										</select>
									</p>

									<p>
										<label for="entrega_forma">Forma de entrega</label>
										<select class="text-input small-input required" id="entrega_forma" name="entrega_forma">
											<option value="PAC">PAC</option>
											<option value="Sedex">Sedex</option>
										</select>
									</p>

									<p>
										<label for="entrega_valor">Valor da entrega</label>
										<input class="text-input small-input required" type="text" id="entrega_valor" name="entrega_valor" maxlength="11" value="<?php echo isset($cliente_compra) ? $cliente_compra->valor_entrega : '' ?>" />
									</p>

									<p>
										<label for="cliente_endereco_id">Cupom utilizado na compra</label>
										<span id="cupom_atual" class="esconder valor-atual"></span>
										<small id="cupom_atual_ajuda" class="esconder"><br />Se for necessário alterar o cupom utilizado na compra, utilize o campo abaixo para selecioná-lo.</small>
										<select class="text-input large-input" id="cliente_cupom_id" name="cliente_cupom_id">
											<option value="-1">Não utilizar cupom de desconto</option>
											<?php
											if($cupons AND count($cupons) > 0)
											{
												foreach ($cupons as $cupom)
												{
													echo '<option value="'.$cupom->id.'">'.$cupom->codigo.' - '.$cupom->produto_nome.' - R$ '.number_format($cupom->valor, 2, ',', '.').'</option>';
												}
											}
											?>
										</select>
									</p>

									<p>
										<label for="onde_foi_adquirido">Onde foi adquirido?</label>
										<input class="text-input large-input" type="text" id="onde_foi_adquirido" name="onde_foi_adquirido" maxlength="200" value="<?php echo isset($cliente_compra) ? $cliente_compra->onde_foi_adquirido : '' ?>" />
									</p>

									<p>
										<label for="informacao_adicional">Informações adicionais</label>
										<textarea class="text-input large-input" rows="10" cols="40" id="informacao_adicional" name="informacao_adicional"><?php echo isset($cliente_compra) ? $cliente_compra->informacao_adicional : '' ?></textarea>
									</p>

								</fieldset>

								<fieldset class="clear">

									<p>
										<input class="button" type="submit" value="<?php echo ( ! isset($cliente_compra) OR is_null($cliente_compra->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
									</p>

								</fieldset>

							</form>

						</div> <!-- tab3 ------------------------------------------------------------ -->

					<?php } // cms_usuario_logado_modulos ?>

					<?php if ($cms_usuario_logado->eh_administrador OR in_array('interacoes', $cms_usuario_logado_modulos)) { ?>

						<div class="tab-content tab-interacoes <?php echo (isset($aba) AND $aba == 4 ) ? 'default-tab' : '' ?>" id="tab4">

                            <p>Utilize o formulário abaixo para cadastrar uma interação para este cliente, ou clique no ícone respectivo para editar uma interação anterior.</p>

                            <div class="notificacao_js"></div> <!-- DIV para inserções de notificações via jQuery -->

                            <form action="<?php echo SITE_URL ?>/fatorcms/clientes/<?php echo ( ! isset($cliente_interacao) OR is_null($cliente_interacao->id)) ? 'interacao-cadastrar' : 'interacao-atualizar' ?>" method="post" id="cliente_interacao_form">

                                <fieldset class="column-left" style="width: 25%">

                                    <input type="hidden" name="cliente_id" value="<?php echo $cliente->id ?>" />

	                                <p>
		                                <label for="tipo">Tipo</label>
		                                <select class="text-input large-input required" id="interacao_tipo" name="tipo">
			                                <option value="Envio de material">Envio de material</option>
			                                <option value="Envio de boleto">Envio de boleto</option>
			                                <option value="Chave de registro">Chave de registro</option>
			                                <option value="Suporte técnico">Suporte técnico</option>
			                                <option value="Reclamação">Reclamação</option>
			                                <option value="Outros">Outros</option>
		                                </select>
	                                </p>

	                            </fieldset>

	                            <fieldset class="column-right" style="width: 70%">

		                            <p>
			                            <label for="enviar_cliente_0">Ao salvar esta interação, enviar seu conteúdo para o cliente?</label>
			                            <input type="radio" name="enviar_cliente" id="enviar_cliente_1" value="1" /> <span class="sim">Sim</span>
			                            <input type="radio" name="enviar_cliente" id="enviar_cliente_0" value="0" checked="checked" /> <span class="nao">Não</span>
		                            </p>

	                            </fieldset>

	                            <fieldset class="clear">

                                    <p>
                                        <label for="titulo">Título</label>
                                        <input class="text-input large-input" type="text" id="titulo" name="titulo" maxlength="200" value="<?php echo ( isset($cliente_interacao) ? $cliente_interacao->titulo : '' ) ?>" />
                                        <br /><small>Este campo é opcional.</small>
                                    </p>

                                    <p class="required-input">
                                        <label for="conteudo">Conteúdo</label>
                                        <textarea class="text-input required interacao_conteudo" cols="80" rows="10" id="conteudo" name="conteudo"><?php echo ( isset($cliente_interacao) ? $cliente_interacao->conteudo : '') ?></textarea>
                                    </p>

                                    <p>
                                        <input class="button" type="submit" value="<?php echo ( ! isset($cliente_compra) OR is_null($cliente_compra->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
                                    </p>

                                </fieldset>

                            </form>

                            <h4 style="border-top:1px solid #ccc; margin-top:20px; padding-top:20px">Interações</h4>

							<table class="interacoes">

								<tbody>
									<?php
			                        if($cliente_interacoes AND count($cliente_interacoes) > 0)
									{
										foreach ($cliente_interacoes as $interacao)
										{
										?>
											<tr>
												<td>
                                                    <table>
                                                        <tr class="interacoes-cabecalho">
                                                            <td>
                                                                <strong id="interacao_<?php echo $interacao->id ?>">Código:</strong>
                                                                <?php echo $interacao->id ?>
                                                            </td>

	                                                        <td>
		                                                        <strong>Tipo:</strong>
		                                                        <?php echo $interacao->tipo ?>
	                                                        </td>

                                                            <td>
                                                                <strong>Data:</strong>
                                                                <?php echo date('d/m/Y \à\s H:i:s', strtotime($interacao->data)) ?>
                                                            </td>

                                                            <td>
                                                                <strong>Autor:</strong>
                                                                <?php echo $interacao->cms_usuario_nome ?>
                                                            </td>

                                                            <td>
                                                                <?php if ($cms_usuario_logado->eh_administrador OR $interacao->cms_usuario_id == $cms_usuario_logado->id) { ?>
                                                                    <a href="<?php echo SITE_URL ?>/fatorcms/clientes/interacao-editar/<?php echo $interacao->id ?>" class="interacao_editar" title="Editar a interação &quot;<?php echo $interacao->id ?>&quot;">
                                                                        <img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" />
                                                                    </a>
                                                                <?php } ?>
                                                                <?php if ($cms_usuario_logado->eh_administrador) { ?>
                                                                    <a href="<?php echo SITE_URL ?>/fatorcms/clientes/interacao-excluir/<?php echo $interacao->id ?>" class="item-confirmar" title="Excluir a interação &quot;<?php echo $interacao->id ?>&quot;">
                                                                        <img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/cross.png" alt="Excluir" />
                                                                    </a>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5">
                                                                <?php if ( ! empty($interacao->titulo)) { ?>
                                                                    <h5><?php echo $interacao->titulo ?></h5>
                                                                <?php } ?>
                                                                <?php echo $interacao->conteudo ?>
                                                            </td>
                                                        </tr>
                                                    </table>
												</td>
											</tr>
										<?php
										}
									}
									else
									{
										echo '<tr><td colspan="1">Nenhuma interação feita até o momento.</td></tr>';
									}
									?>
								</tbody>

							</table>

						</div> <!-- tab4 ------------------------------------------------------------ -->

					<?php } // cms_usuario_logado_modulos interacoes ?>

                    <?php if (($cms_usuario_logado->eh_administrador OR in_array('compras', $cms_usuario_logado_modulos)) AND $plano_assistencia) { ?>

                        <div class="tab-content tab-plano-assistencia <?php echo (isset($aba) AND $aba == 5 ) ? 'default-tab' : '' ?>" id="tab5">

                            <!-- Formulário para edição de um plano de assistencia -->

                            <div class="notificacao_js"></div> <!-- DIV para inserções de notificações via jQuery -->

                            <h4>Edição do Plano de Assistência</h4>

                            <p>Utilize o formulário abaixo para editar os dados do plano de assistências do cliente <?php echo $cliente->nome; ?>.</p>

                            <form action="<?php echo SITE_URL ?>/fatorcms/clientes/atualizar-plano-assistencia" method="post" id="cliente_plano_assistencia_form">

                                <fieldset class="column-left">

                                    <input type="hidden" name="cliente_id" value="<?php echo $cliente->id ?>" />
                                    <input type="hidden" name="id" value="<?php echo $plano_assistencia->id ?>" />

                                    <p>
                                        <label for="data">Data da compra</label>
                                        <input class="text-input small-input required" type="text" id="data_compra" name="data" maxlength="10" value="<?php echo isset($plano_assistencia) ? date('d/m/Y',strtotime($plano_assistencia->data)) : '' ?>" />
                                    </p>

                                    <p>
                                        <label for="data">Data de início do plano</label>
                                        <input class="text-input small-input required" type="text" id="plano_inicio" name="inicio" maxlength="10" value="<?php echo isset($plano_assistencia) ? date('d/m/Y',strtotime($plano_assistencia->inicio)) : '' ?>" />
                                    </p>

                                    <p>
                                        <label for="data">Data de fim do plano</label>
                                        <input class="text-input small-input required" type="text" id="plano_fim" name="fim" maxlength="10" value="<?php echo isset($plano_assistencia) ? date('d/m/Y',strtotime($plano_assistencia->fim)) : '' ?>" />
                                    </p>

                                    <p>
                                        <label for="produto_licencas">Número de licenças</label>
                                        <input class="text-input small-input" type="text" id="licenca_quantidade" name="licenca_quantidade" maxlength="2" value="<?php echo isset($plano_assistencia) ? $plano_assistencia->licenca_quantidade : '' ?>" />
                                    </p>

                                </fieldset>

                                <fieldset class="column-right">

                                    <p>
                                        <label for="entrega_valor">Valor total da compra</label>
                                        <input disabled="disabled" class="text-input small-input" type="text" id="valor_total" name="valor_total" maxlength="11" value="<?php echo isset($plano_assistencia) ? $plano_assistencia->total : '' ?>" />
                                    </p>


                                    <p>
                                        <label for="informacao_adicional">Informações adicionais</label>
                                        <textarea class="text-input large-input" rows="10" cols="40" id="informacao_adicional_plano" name="informacao_adicional"><?php echo isset($plano_assistencia) ? $plano_assistencia->informacao_adicional : '' ?></textarea>
                                    </p>

                                </fieldset>

                                <fieldset class="clear">

                                    <p>
                                        <input class="button" type="submit" value="Atualizar" />
                                    </p>

                                </fieldset>

                            </form>

                        </div> <!-- tab3 ------------------------------------------------------------ -->

                    <?php } // cms_usuario_logado_modulos ?>
					
					<?php if (($cms_usuario_logado->eh_administrador OR in_array('recorrencia', $cms_usuario_logado_modulos))) { ?>

					    <div class="tab-content tab-recorrencia <?php echo (isset($aba) AND $aba == 6 ) ? 'default-tab' : '' ?>" id="tab6">

					        <!-- Formulário para edição de recorrencia -->

					        <div class="notificacao_js"></div> <!-- DIV para inserções de notificações via jQuery -->

					        <h4>Edição de Recorrência</h4>

					        <p>Utilize o formulário abaixo para editar os dados de recorrência do cliente <?php echo $cliente->nome; ?>.</p>

					        <form action="<?php echo SITE_URL ?>/fatorcms/clientes/atualizar-recorrencia" method="post" id="cliente_recorrencia_form">

					            <fieldset class="column-left">

					                <input type="hidden" name="cliente_id" value="<?php echo $cliente->id ?>" />
					                <input type="hidden" name="id" value="<?php echo $recorrencia==true ? $recorrencia->id : '' ?>" />

					                <p>
					                    <label for="data">Versão do Produto</label>
					                    <input class="text-input small-input" type="text" id="versao_produto" name="versao_produto" maxlength="150" value="<?php echo $recorrencia==true ? $recorrencia->versao_produto : '' ?>" />
					                </p>							

					                <p>
					                    <label for="data">Data de Expiração</label>
					                    <input class="text-input small-input" type="text" id="plano_inicio" name="data_expiracao" maxlength="10" value="<?php echo $recorrencia==true ? date('d/m/Y',strtotime($recorrencia->data_expiracao)) : '' ?>" />
					                </p>

					                <p>
					                    <label for="data">Licença</label>
					                    <input class="text-input small-input" type="text" id="licenca" name="licenca" maxlength="150" value="<?php echo $recorrencia==true ? $recorrencia->licenca : '' ?>" />
					                </p>


					            </fieldset>

					            <fieldset class="clear">

					                <p>
					                    <input class="button" type="submit" value="Atualizar" />
					                </p>

					            </fieldset>

					        </form>

					    </div> <!-- tab3 ------------------------------------------------------------ -->

					<?php } // cms_usuario_logado_modulos ?>

				<?php } // is_null cliente ?>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>