<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property int arquivo_id
 * @property int cliente_id
 */

class FatorCMS_Model_ArquivoCliente extends FatorCMS_Model_Padrao
{
	protected $tabela_nome = 'arquivos_clientes';


	/****************************** NOVOS MÉTODOS ******************************/


	/**
	 * Executa o delete no banco, conforme o os parâmetros salvos no objeto
	 * @return bool
	 */
	public function delete()
	{
		// "Limpa" os valores
		$this->colunas_mysqli_escape();

		if ( ! is_null($this->id))
		{
			// Exclusão de um registro específico
			$sql = 'DELETE FROM '.$this->tabela_nome.' WHERE id = '.$this->id;
			return $this->query($sql);
		}
		elseif ( ! is_null($this->arquivo_id))
		{
			// Exclusão de todos os registro de determinada novidade
			$sql = 'DELETE FROM '.$this->tabela_nome.' WHERE arquivo_id = '.$this->arquivo_id;
			return $this->query($sql);
		}
		elseif ( ! is_null($this->cliente_id))
		{
			// Exclusão de todos os registro de determinada tag
			$sql = 'DELETE FROM '.$this->tabela_nome.' WHERE cliente_id = '.$this->cliente_id;
			return $this->query($sql);
		}

		// Se chegou aqui, é porque não excluiu nada
		return FALSE;
	}

} // end class