<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_Interacoes extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai e testa se o usuário está logado
	 */
	public function __construct()
	{
		parent::__construct();
		$this->verificar_usuario_logado();
		$this->verificar_usuario_permissao('interacoes');
	}


	/**
	 * Método padrão que não deve ser chamado diretamente. Se for, vai para a listagem
	 * @return void
	 */
	public function index()
	{
        header('Location: '.SITE_URL.'/fatorcms/interacoes/listar');
		exit;
	}


	/**
	 * Método inicial que exibe a lista de registros do banco de dados
	 * @param $parametros object
	 * @return void
	 */
	public function listar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'interacoes-lista.php'));
		$view->adicionar('body_class', 'interacoes-lista lista');
		$view->adicionar('lateral_menu', array('item'=>'interacoes','link'=>'listar todas'));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$this->view_adicionar_obrigatorias($view);

		//-----

		// Lógica caso tenha sido feita uma busca na listagem
		$clausula_where = ' TRUE ';

		$buscar = NULL;
		if (isset($parametros->buscar) AND strlen(trim($parametros->buscar)) > 0)
		{
			$buscar = trim($parametros->buscar);
			$buscar_aux = Funcoes::mysqli_escape(str_replace(' ', '%', $buscar));
			$clausula_where .= ' AND (cliente.nome LIKE "%'.$buscar_aux.'%" OR cliente.email LIKE "%'.$buscar_aux.'%" OR
										cliente.cpf LIKE "%'.$buscar_aux.'%" OR cliente.cnpj LIKE "%'.$buscar_aux.'%" OR
										interacao.titulo LIKE "%'.$buscar_aux.'%" OR interacao.conteudo LIKE "%'.$buscar_aux.'%" OR
										interacao.tipo LIKE "%'.$buscar_aux.'%" OR
										cms_usuario.nome LIKE "%'.$buscar_aux.'%" OR interacao.id LIKE "%'.$buscar_aux.'%")';
		}

		//-----

		// Ordenação dos resultados, podendo vir da página
		$ordenar_por = (isset($parametros->ordenar_por)) ? strtolower($parametros->ordenar_por) : 'data'; // Valor padrão
		$ordem = isset($parametros->ordem) ? strtolower($parametros->ordem) : 'desc'; // Valor padrão

		switch ($ordenar_por)
		{
            case 'data': $ordenar_por_sql = 'interacao.data'; break;
            case 'cod': $ordenar_por_sql = 'interacao.id'; break;
            case 'cliente': $ordenar_por_sql = 'cliente.nome'; break;
            case 'tipo': $ordenar_por_sql = 'interacao.tipo'; break;
            case 'titulo': $ordenar_por_sql = 'interacao.titulo'; break;
			case 'conteudo': $ordenar_por_sql = 'interacao.conteudo'; break;
			case 'autor': $ordenar_por_sql = 'cms_usuario.nome'; break;
            default: $ordenar_por_sql = $ordenar_por; break;
		}


		//-----

		$interacao = new FatorCMS_Model_Interacao;
		// Define a cláusula do select
		$interacao->clausula = '
			SELECT interacao.id, interacao.data, interacao.tipo, interacao.titulo, interacao.conteudo,
			    cliente.id AS cliente_id, cliente.nome AS cliente_nome,
			    cms_usuario.id AS cms_usuario_id, cms_usuario.nome AS cms_usuario_nome
			FROM {tabela_nome} AS interacao
			    LEFT JOIN {tabela_prefixo}clientes AS cliente ON interacao.cliente_id = cliente.id
			    LEFT JOIN {tabela_prefixo}cms_usuarios AS cms_usuario ON interacao.cms_usuario_id = cms_usuario.id
			WHERE '.$clausula_where.'
			GROUP BY interacao.id
			ORDER BY '.$ordenar_por_sql.' '.$ordem
		;

		// Através da paginação vai verificar se existe a necessidade de dividir o resultado em várias páginas e se é necessário adicionar um LIMIT
		$paginacao = new Paginacao($interacao, $parametros, 30, 15);

		// Executa a cláusula lá de cima
        $interacoes = $interacao->select(NULL, TRUE); // NULL para não alterar a clásula já definida e TRUE para forçar retorno em array

		//-----

		$view->adicionar('buscar', $buscar);
		$view->adicionar('ordenar_por', $ordenar_por);
		$view->adicionar('ordem', $ordem);
		$view->adicionar('interacoes', $interacoes);
		$view->adicionar('paginacao', $paginacao);

		$view->exibir();
	}


} // end class