<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_Galerias extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai e testa se o usuário está logado
	 */
	public function __construct()
	{
		parent::__construct();
		$this->verificar_usuario_logado();
		$this->verificar_usuario_permissao('multimidia');
	}


	/**
	 * Método padrão que não deve ser chamado diretamente. Se for, vai para a listagem
	 * @return void
	 */
	public function index()
	{
		header('Location: '.SITE_URL.'/fatorcms/galerias/listar');
		exit;
	}


	/**
	 * Método inicial que exibe a lista de registros do banco de dados
	 * @param $parametros object
	 * @return void
	 */
	public function listar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'galerias-lista.php'));
		$view->adicionar('body_class', 'galerias lista');
		$view->adicionar('lateral_menu', array('item'=>'multimidia','link'=>'listar galerias'));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$this->view_adicionar_obrigatorias($view);

		//-----

		// Lógica caso tenha sido feita uma busca na listagem
		$clausula_where = ' TRUE ';

		$buscar = NULL;
		if (isset($parametros->buscar) AND strlen(trim($parametros->buscar)) > 0)
		{

			$buscar = trim($parametros->buscar);
			$buscar_aux = Funcoes::mysqli_escape(str_replace(' ', '%', $buscar));
			$clausula_where .= 'AND (galeria.titulo LIKE "%'.$buscar_aux.'%" OR galeria.descricao LIKE "%'.$buscar_aux.'%")';
		}

		//-----

		// Ordenação dos resultados, podendo vir da página
		$ordenar_por = (isset($parametros->ordenar_por)) ? strtolower($parametros->ordenar_por) : 'data'; // Valor padrão
		$ordem = isset($parametros->ordem) ? strtolower($parametros->ordem) : 'desc'; // Valor padrão

		switch ($ordenar_por)
        {
            case 'data': $ordenar_por_sql = 'galeria.data'; break;
            case 'titulo': $ordenar_por_sql = 'galeria.titulo'; break;
            case 'fotos': $ordenar_por_sql = 'numero_fotos'; break;
            case 'produto': $ordenar_por_sql = 'produto.nome'; break;
            default: $ordenar_por_sql = $ordenar_por; break;
        }

		//-----

		$galeria = new FatorCMS_Model_Galeria;
		$galeria->clausula = '
			SELECT galeria.*, produto.nome AS produto_nome, COUNT(foto.id) AS numero_fotos
			FROM {tabela_nome} AS galeria
				LEFT JOIN {tabela_prefixo}produtos AS produto ON galeria.produto_id = produto.id
				LEFT JOIN {tabela_prefixo}galerias_fotos AS foto ON galeria.id = foto.galeria_id
				WHERE '.$clausula_where.'
				GROUP BY galeria.id
				ORDER BY '.$ordenar_por_sql.' '.$ordem
		;

		// Através da paginação vai verificar se existe a necessidade de dividir o resultado em várias páginas e se é necessário adicionar um LIMIT
		$paginacao = new Paginacao($galeria, $parametros, 10, 10);

		// Executa a cláusula lá de cima
		$galerias = $galeria->select(NULL, TRUE); // NULL para não alterar a clásula já definida e TRUE para forçar retorno em array

		//-----

		$view->adicionar('buscar', $buscar);
		$view->adicionar('ordenar_por', $ordenar_por);
		$view->adicionar('ordem', $ordem);
		$view->adicionar('galerias', $galerias);
		$view->adicionar('paginacao', $paginacao);

		$view->exibir();
	}


    /**
	 * Dependendo do parâmetro, aprensenta a tela de edição ou de cadastro
	 * @param $parametros object
	 * @return void
	 */
	public function editar($parametros)
	{
        $view = new View(array('diretorios' => 'fatorcms', 'arquivo' => 'galerias-form.php'));

        $view->adicionar('body_class', 'galerias form');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
		$this->view_adicionar_obrigatorias($view);

        // Decide o que fazer
        if (isset($parametros->_0) AND is_numeric($parametros->_0))
        {
            // É atualização
            $galeria = new FatorCMS_Model_Galeria;
	        $galeria = $galeria->select('SELECT * FROM {tabela_nome} WHERE id = ' . Funcoes::mysqli_escape($parametros->_0));

            $fotos = new FatorCMS_Model_GaleriaFoto;
            $fotos = $fotos->select('SELECT * FROM {tabela_nome} WHERE galeria_id = ' . $parametros->_0, TRUE);

            $view->adicionar('galeria', $galeria);
            $view->adicionar('fotos', $fotos);
            $view->adicionar('lateral_menu', array('item' => 'multimidia', 'link' => ''));

        }
        else
        {
            // É cadastro
			$view->adicionar('galeria', NULL);
			$view->adicionar('lateral_menu', array('item' => 'multimidia', 'link' => 'cadastrar galeria'));

        }

		// Lista de Produtos para o <select>, com exceção dos já utilizados
		// Caso seja atualização, permitimos exibir o produto da galeria atual
        $produto = new FatorCMS_Model_Produto;
        $view->adicionar('produtos', $produto->select('
            SELECT * FROM {tabela_nome}
            WHERE id NOT IN (
                SELECT produto_id FROM {tabela_prefixo}galerias
                WHERE TRUE '.(isset($galeria->id) ? ' AND produto_id != '.$galeria->produto_id : '').'
            )
            ORDER BY nome
		', TRUE));

		//-----

		// Abre a aba que veio na URL, ou a padrão
		$view->adicionar('aba', isset($parametros->aba) ? $parametros->aba : 1);

        $view->exibir();
    }


	/**
	 * Recebe os parâmetros do formulário e adiciona o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function cadastrar($parametros)
	{
        $view = new View(array('diretorios' => 'fatorcms', 'arquivo' => 'galerias-form.php'));
		$this->view_adicionar_obrigatorias($view);

		//-----

		if ($parametros AND isset($parametros->titulo))
		{
			$galeria = new FatorCMS_Model_Galeria;
			$galeria->carregar($parametros);
			
			// Campos especiais
			$galeria->titulo_seo = Funcoes::texto_para_seo($parametros->titulo);

			//-----

			// Verifica se o titulo_seo está disponível, se não inicia o contador até achar um
			if ( ! empty($galeria->titulo))
			{
				$cont = 2;
				do
				{
					$galeria_existe = new FatorCMS_Model_Galeria;
	                $galeria_existe = $galeria_existe->select('SELECT id FROM {tabela_nome} WHERE titulo_seo = "' . $galeria->titulo_seo . '"');
					if ($galeria_existe AND ! is_null($galeria_existe->id))
					{
	                    $galeria->titulo_seo = Funcoes::texto_para_seo($galeria->titulo) . '-' . $cont++;
					}
				} while ($galeria_existe AND ! is_null($galeria_existe->id));
			}
			else
			{
				$notificacao = new Notificacao('Todos os campos marcados são obrigatórios.');
			}

			//-----

			// Log
			$galeria->log_id = $this->cms_usuario_logado->id;
			$galeria->log_data = date('Y-m-d H:i:s');

			//-----

            $galeria->colunas_mysqli_escape();

			if ( ! isset($notificacao) AND $galeria->verificar_null(array('id')))
			{
				// Validações ---

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($galeria->insert())
					{
                        new Notificacao('Galeria <strong>' . $galeria->titulo . '</strong> cadastrada com sucesso.', 'success', TRUE);
                        header('Location: ' . SITE_URL . '/fatorcms/galerias/listar');
						exit;
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao salvar os dados da galeria.');
					}
				}
				// Deixa passar porque já veio uma notificação da validação
			}
			else
			{
				$notificacao = (isset($notificacao)) ? $notificacao : new Notificacao('Todos os campos marcados são obrigatórios.');
			}

			// Para retornar ao formulário com valores inseridos
			$view->adicionar('galeria', $galeria);
		}
		else
		{
			$view->adicionar('galeria', NULL);
		}

		//-----

		// Lista de Produtos para o <select>, com exceção dos já utilizados
        $produto = new FatorCMS_Model_Produto;
        $view->adicionar('produtos', $produto->select('
            SELECT * FROM {tabela_nome}
            WHERE id NOT IN (SELECT produto_id FROM {tabela_prefixo}galerias)
            ORDER BY nome
		', TRUE));

		$view->adicionar('body_class', 'galerias formulario');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
        $view->adicionar('lateral_menu', array('item' => 'multimidia', 'link' => 'cadastrar galeria'));

        $view->exibir();
	}

	
	/**
	 * Recebe os parâmetros do formulário e atualiza o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function atualizar($parametros)
	{
        $view = new View(array('diretorios' => 'fatorcms', 'arquivo' => 'galerias-form.php'));
		$this->view_adicionar_obrigatorias($view);

		//-----

		if (isset($parametros->id) AND is_numeric($parametros->id))
		{
			$galeria = new FatorCMS_Model_Galeria;
			$galeria->carregar($parametros);

			// Campos especiais
			$galeria->titulo_seo = Funcoes::texto_para_seo($galeria->titulo);

            //-----

            // Verificamos se o titulo_seo de antes é o igual ao novo. Se for diferente verificamos a possibilidade
			if ( ! empty($galeria->titulo))
			{
	            $galeria_atual = new FatorCMS_Model_Galeria($galeria->id);
	            if ($galeria->titulo_seo != $galeria_atual->titulo_seo) {
	                // Verifica se o titulo_seo está disponível (desde que não seja o do próprio registro), se não inicia o contador até achar um
	                $cont = 2;
	                do
	                {
	                    $galeria_existe = new FatorCMS_Model_Galeria;
	                    $galeria_existe = $galeria_existe->select('SELECT id FROM {tabela_nome} WHERE titulo_seo = "' . $galeria->titulo_seo . '" AND id != ' . $galeria->id);
	                    if ($galeria_existe AND !is_null($galeria_existe->id)) {
		                    $galeria->titulo_seo = Funcoes::texto_para_seo($galeria->titulo) . '-' . $cont++;
	                    }
	                } while ($galeria_existe AND !is_null($galeria_existe->id));
	            }
			}
			else
			{
				$notificacao = new Notificacao('Todos os campos marcados são obrigatórios.');
			}

			//-----

			// Log
			$galeria->log_id = $this->cms_usuario_logado->id;
			$galeria->log_data = date('Y-m-d H:i:s');

			//-----

			$galeria->colunas_mysqli_escape();

			if ( ! isset($notificacao) AND $galeria->verificar_null())
			{
				// Validações ---

				// Fim das validações ---

				if ( ! isset($notificacao))
				{

					if ($galeria->update())
					{
						new Notificacao('Galeria <strong>'.$galeria->titulo.'</strong> atualizada com sucesso.', 'success', TRUE);
						header('Location: '.SITE_URL.'/fatorcms/galerias/listar');
						exit;
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao atualizar os dados da galeria.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = (isset($notificacao)) ? $notificacao : new Notificacao('Todos os campos marcados são obrigatórios.');
			}

			// Para retornar ao formulário com valores inseridos
			$view->adicionar('galeria', $galeria);
		}
		else
		{
			$notificacao = new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention');
		}

		//-----

		// Lista de Produtos para o <select>, com exceção dos já utilizados
        $produto = new FatorCMS_Model_Produto;
        $view->adicionar('produtos', $produto->select('
            SELECT * FROM {tabela_nome}
            WHERE id NOT IN (
                SELECT produto_id FROM {tabela_prefixo}galerias
                WHERE produto_id != '.$galeria->produto_id.'
            )
            ORDER BY nome
		', TRUE));

		$view->adicionar('body_class', 'galerias formulario');
        $view->adicionar('lateral_menu', array('item' => 'agenda', 'link' => ''));
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$view->exibir();
	}


	/**
	 * Recebe o ID do registro e excluir ele do banco de dados
	 * @param  $parametros
	 * @return void
	 */
    public function excluir($parametros)
	{
		if (isset($parametros->_0) AND is_numeric($parametros->_0))
		{
			$galeria = new FatorCMS_Model_Galeria($parametros->_0);

			if ($galeria AND ! is_null($galeria->id))
			{
				// Apagamos o registro do banco de dados
				if ($galeria->delete())
				{
					// Percorremos todas as fotos da galeria e excluímos tanto os registros quanto os arquivos
					$foto = new FatorCMS_Model_GaleriaFoto;
					$fotos = $foto->select('SELECT id, galeria_id, arquivo FROM {tabela_nome} WHERE galeria_id = '.$galeria->id, TRUE);

					if ($fotos AND count($fotos) > 0)
					{
						foreach ($fotos as $foto)
						{
							@unlink('arquivos/galerias/'.$foto->galeria_id.'/'.$foto->arquivo);
							@unlink('arquivos/galerias/'.$foto->galeria_id.'/thumb1/'.$foto->arquivo);
							@unlink('arquivos/galerias/'.$foto->galeria_id.'/thumb2/'.$foto->arquivo);

							if ( ! $foto->delete())
							{
								new Log('Erro ao excluir a foto de uma galeria. $galeria_id='.$galeria->id.'|$foto_id='.$foto->id);
							}
						}

						// Excluimos os diretórios dos arquivos
						@rmdir('arquivos/galerias/'.$foto->galeria_id.'/thumb2/');
						@rmdir('arquivos/galerias/'.$foto->galeria_id.'/thumb1/');
						@rmdir('arquivos/galerias/'.$foto->galeria_id);
					}

					new Notificacao('Galeria <strong>'.$galeria->titulo.'</strong> excluída com sucesso.', 'success', TRUE);
				}
				else
				{
					new Notificacao('Ocorreu um erro ao excluir a galeria <strong>'.$galeria->titulo.'</strong>.', 'error', TRUE);
				}
			}
			else
			{
				new Notificacao('Galeria não encontrada.', 'error', TRUE);
			}
		}
		else
		{
			new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention', 'error', TRUE);
		}

		header('Location: '.SITE_URL.'/fatorcms/galerias/listar');
		exit;
	}


	/****************************** MÉTODOS EXTRAS ******************************/


	/**
	 * Método chamado via AJAX na lista de fotos de uma galeria que apaga uma foto do BD e seus arquivos
	 * @param $parametros
	 */
    public function apagar_foto($parametros)
    {
        $foto = new FatorCMS_Model_GaleriaFoto($parametros->_0);

	    @unlink('arquivos/galerias/'.$foto->galeria_id.'/'.$foto->arquivo);
        @unlink('arquivos/galerias/'.$foto->galeria_id.'/thumb1/'.$foto->arquivo);
        @unlink('arquivos/galerias/'.$foto->galeria_id.'/thumb2/'.$foto->arquivo);

        if ($foto->delete())
        {
            echo json_encode(array('resultado' => 1));
        }
        else
        {
            echo json_encode(array('resultado' => 0));
        }
    }


	/**
	 * Método chamado via AJAX na lista de fotos de uma galeria que atualiza o título da foto
	 * @param $parametros
	 */
    public function atualizar_foto($parametros)
    {
        $foto = new FatorCMS_Model_GaleriaFoto;
	    $foto->id = Funcoes::mysqli_escape($parametros->_0);
        $foto->titulo = Funcoes::mysqli_escape(trim($parametros->titulo));

        if ($foto->update())
        {
            echo json_encode(array('resultado' => 1));
        }
        else
        {
            echo json_encode(array('resultado' => 0));
        }
    }


} // end class
