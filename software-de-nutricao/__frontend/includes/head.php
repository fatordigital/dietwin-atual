<?php $baseurl = '//'.$_SERVER["HTTP_HOST"]; ?>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>dietWin - Software de Nutrição</title>
    
    <!-- Tags para a indexação em mecânismos de busca -->
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="Fator Digital - http://www.fatordigital.com.br" />
    <meta name="robots" content="all" />
    <meta name="revisit-after" content="7 days" />
    
    <!-- Viewport padrão pois o site é responsivo -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Tags OG para compartilhamento em redes sociais -->
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:url" content="http://www.meusite.com.br/ola-mundo" />
    <meta property="og:title" content="Título da página ou artigo" />
    <meta property="og:site_name" content="Nome do meu site" />
    <meta property="og:description" content="Minha boa descrição para intrigar os usuários." />
    <meta property="og:image" content="www.meusite.com.br/imagem.jpg" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:type" content="website" />
    

    <link rel="stylesheet" href="<?php echo $baseurl ?>/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo $baseurl ?>/css/flexslider.css" />
    <link rel="stylesheet" href="<?php echo $baseurl ?>/css/style.css" />

    <!--[if lt IE 9]>
        <script src="//raw.github.com/mylovecompany/ie9-js/master/ie9.min.js">IE7_PNG_SUFFIX=".png";</script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>