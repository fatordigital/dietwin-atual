<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="content-language" content="pt-br" />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Tabela Nutricional | Dietwin - Faça rotulagem nutricional em minutos </title>

		<meta property="og:url" content="http://www.dietwin.com.br/" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="http://www.dietwin.com.br/views/imagens/og-dietwin.jpg" />
        <meta property="og:image:type" content="image/jpg" />
        <meta property="og:image:width" content="640"/>
        <meta property="og:image:height" content="400" />
        <meta property="og:site_name" content="Dietwin">
        <meta property="og:title" content="DietWin - Softwares de nutrição">
        <meta property="og:description" content="O dietWin é um conjunto de Softwares de Nutrição, desenvolvidos por nutricionistas que imprime confiabilidade aos dados apresentados.">
		
		<meta name="geo.region" content="BR-RS" />
		<meta name="geo.placename" content="Porto Alegre" />
		<meta name="geo.position" content="-14.235004;-51.92528" />
		<meta name="ICBM" content="-14.235004, -51.92528" />    

		<!-- BEGIN: CARREGAMENTO DOS FAVICONS -->
		<link rel="apple-touch-icon" sizes="57x57" href="assets/images/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="assets/images/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="assets/images/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="assets/images/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="assets/images/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="assets/images/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="assets/images/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="assets/images/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="assets/images/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="assets/images/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="assets/images/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<!-- END: CARREGAMENTO DOS FAVICONS -->


		<!-- BEGIN: CARREGAMENTO DAS FOLHAS DE ESTILO/FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed:200,400,700" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Fira+Sans+Extra+Condensed:200,400,600" rel="stylesheet">
		<link rel="stylesheet" href="assets/css/normalize.css">
		<link rel="stylesheet" href="assets/css/lib/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/lib/owl.carousel.min.css">
		<link rel="stylesheet" href="assets/css/lib/owl.theme.default.min.css">
		<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/main.css">
		<!-- END: CARREGAMENTO DAS FOLHAS DE ESTILO/FONTS -->

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-3191692-1', 'auto');
            ga('send', 'pageview');

        </script>

	</head>
    <body>
