<?php include 'includes/cabecalho.php'; ?>

<body class="softwares personal">

<?php include 'includes/topo.php'; ?>
	
    <div class="titulo-principal">
        <p><span>dietWin</span><br />Personal</p>
    </div>
    <div class="titulo-resumo">
        <p>É um software que atende todas as necessidades básicas do atendimento clínico e nutricional. É um programa simples, compacto e ágil, desenvolvido com o que há de mais moderno na tecnologia de softwares.</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container">
    	
        <?php include 'includes/toolbar.php' ?>
        
        <h1>dietWin <span>Personal </span></h1>
        
        <div class="box" id="tamanho-fonte">
        	<h2>O Software de nutrição mais interativa, moderna e ágil.</h2>
        
            <p>É uma versão que atende todas as necessidades básicas da avaliação nutricional e prescrição dietética para todos os ciclos da vida. Um novo lay-out de telas é apresentado para uma nova proposta de atendimento clínico diferenciado onde a evolução da consulta fica disponível durante todo o atendimento. Permite a abertura de múltiplas fichas de pacientes e pesquisas facilitadas. É o primeiro da série das novas versões do projeto dietWin apresentando uma performance de sistema mais compacto, moderno, ágil e flexível, desenvolvido com o que há de mais moderno na tecnologia de softwares, permitindo inúmeras configurações do sistema para personalizar seu uso. Pela sua agilidade e objetividade é uma versão ideal também para: Hospitais, Merenda Escolar, Personal diet ou consultorias nutricionais.</p>

            <ul class="detalhes">
                <li>Oferece agilidade e rapidez no atendimento clínico</li>
                <li>Ideal para atende as necessidades das áreas da nutrição: consultórios, hospitais, personal diet, merenda escolar, alunos em formação acadêmica, consultorias nutricionais e unidades de alimentação e nutrição.</li>
                <li>Uma ferramenta prática para a formulação de cardápios, receitas.</li>
                <li>Ideal para pesquisadores que necessitam formar um banco de dados para obter resultados de análise dietética de macro e micronutrientes.</li>
                <li>Integração automática entre módulos e sistemas, facilitando a rotina dos usuários.</li>
                <li>Utilização dos protocolos padronizados de domínio público na nutrição.</li>
                <li>Utilização das mais avançadas tecnologias buscando o máximo de agilidade e eficiência.</li>
                <li>Soluções flexíveis e comprovadas, utilizado em diversas pesquisas e amplamente aceito pelas Comunidades Acadêmicas e Científicas.</li>
                <li>Alia conhecimento e experiência no desenvolvimento das melhores ferramentas desde 1993.</li>
			</ul>

			<h3>Interface amigável e intuitiva</h3>
            <?php
            if ($fotos and count($fotos)>0)
            {
            ?>
            <ul class="thumbs">
                <?php
                foreach ($fotos as $foto)
                {
                ?>
            	<li><a href="<?php echo SITE_BASE ?>/arquivos/galerias/<?php echo $foto->galeria_id ?>/<?php echo $foto->arquivo ?>" rel="profissional" title="<?php echo isset($foto->titulo)?$foto->titulo:'dietWin Personal' ?>"><img src="<?php echo SITE_BASE ?>/arquivos/galerias/<?php echo $foto->galeria_id ?>/thumb1/<?php echo $foto->arquivo ?>" alt="<?php echo isset($foto->titulo)?$foto->titulo:'dietWin Personal' ?>" /></a></li>
                <?php
                }
                ?>

            </ul>

            <div class="clear"></div>
            <a href="<?php echo SITE_URL ?>/software/personal/galeria-foto" class="link-thumbs" title="Veja mais imagens e vídeos do dietWin Personal ">Veja mais imagens do dietWin Personal </a>
            <?php
            }
            ?>

            
            <h3>Recursos e soluções para quem é exigente.</h3>
            <h4>Por quê escolher o dietWin Personal?</h4>
            <h5>CONSULTA NUTRICIONAL</h5>
            <p>Para uma primeira consulta com uma ficha para o paciente é aberta, toda a evolução da consulta fica acessível durante o atendimento clínico. Todos os procedimentos da avaliação são executados dentro dela,  permitindo que várias fichas de paciente possam ser abertas  simultaneamente para atendimento familiar ou consulta. A navegação pelos módulos desta ficha segue o padrão Windows também sem ordem seqüencial para o atendimento. Várias telas podem ser abertas para acessar os cadastros necessários. As ferramentas necessárias para a avaliação estão abaixo listadas e elas dão acesso aos cadastros do dietWin denominado banco de dados. Ainda, os resultados para análise e tomada de decisão com vistas ao diagnóstico nutricional. Esta ficha contempla o acesso:</p>

            <ul class="detalhes">
                <li>Cadastro paciente</li>
                <li>Avaliação Nutricional</li>
                <li>Prontuário</li>
                <li>Pesq.Alim/receitas</li>
                <li>Alimentos favoritos</li>
                <li>Anamnese</li>
                <li>Prescrição dietética</li>
                <li>Evolução consultas</li>
                <li>Modelos dietas</li>
                <li>Lista substituição</li>
            </ul>

            <h5>CADASTRO DO PACIENTE</h5>
            <p>é uma ficha simplificada com os dados básicos do paciente. Permite cadastrar seu e-mail desta forma todo relatório aberto pode ser encaminhado automaticamente ao paciente durante o atendimento. Neste módulo ficam arquivados as ferramentas:</p>


			<ul class="detalhes">
           	    <li><b>Prontuário: </b>todas as consultas podem ser evoluídas e ficando o histórico arquivado para evolução do registro.</li>
           	    <li><b>Recomendações dietoterápicas: </b>vários modelos cadastrados em campo texto</li>
           	    <li><b>Lista de substituição para o paciente</b></li>
           	    <li><b>Capa dos relatórios</b></li>
            </ul>

            <h5>ANAMNESE</h5>
            <p>é uma ferramenta com campo texto, que traz um questionário padrão que pode ser alterado e gravado como um novo modelo de anamnese. Vários modelos podem ser cadastrados.</p>

            <h5>AVALIAÇÃO NUTRICIONAL: </h5>
            <p>Para todos os ciclos da vida. Toda a avaliação nutricional é sinalizada automaticamente dispensando configurações ou seleções de fórmulas ou equações. Todos os parâmetros e indicadores do estado nutricional são calculados para análise e diagnóstico nutricional. Todas as referências das equações utilizadas são descritas nas telas. Para avaliar, apenas os dados mínimos são obrigatórios como peso e altura, não há restrições de medidas para a avaliação nutricional básica. Compreende:</p>

            <ul class="detalhes">
                <li>ADULTOS</li>
                <li>ADOLESCENTES</li>
                <li>GESTANTE</li>
                <li>IDOSOS</li>
                <li>CRIANÇAS</li>
                <li>ATLETA</li>
            </ul>

            <p><b>Antropometria: </b> com as seguintes medidas: peso, peso estimado, peso desejável, peso habitual, altura, altura estimada, 3 diâmetros, 10 dobras cutâneas, 12 perímetros. Relatórios.</p>

            <p><b>Indicadores nutricionais: </b></p>

            <ul class="detalhes">
                <li>Peso: PT Berardinelli, Classific. Biotipo, % PA/PT Blackburn, </li>
                <li>Gráfico de evolução peso</li>
                <li>IMC/OMS/2004, CB/CMB/PCT- Jellife, Circ. Cintura-Risco DC</li>
                <li>% Gordura: Pollock & Jackson 1978</li>
                <li>Estimativas calóricas: TMB-FAO/OMS 85, GEB-LONG/97, VET-FAO/OMS 85,</li>
                <li><b>Gestante: </b>DUM, PPG, DPP, IMC, GPR, Idade gestacional, Gráfico IMC Atalah/97,</li>
                <li><b>Criança: </b>curvas de crescimento WHO 2006/2007 para Peso/idade, Peso/altura, Altura/ idade, IMC/I, TMB,
                    VET (kcalXP RDA89), GEB,Schofield/1985, Recomendação PTN/g/dia
                </li>
                <li><b>Idoso: </b>IMC Lipschitz, 1994, e os indicadores compatíveis com adulto</li>
            </ul>

            <h5>PRESCRIÇÃO DIETÉTICA: </h5>

            <p>Esta ferramenta permite uma agilidade na prescrição dietética em <b>"segundos"</b> uma vez que é possível utilizar modelos de dietas pré-cadastradas que podem ser ajustadas e individualizadas ao paciente sem com isso alterar o modelo padrão.</p>

            <p><b>Cadastrar prescrições dietéticas e recordatórios: </b>não há restrição quanto ao número de dietas ou cardápios prescritos para cada consulta. Basta identificar o nome ou descrição do cardápio prescrito. Um modelo de dieta inserida ao paciente e modificada pode ser salva como um novo modelo. </p>

            <p><b>Inserir alimentos e receitas </b>na prescrição através das <b>ferramentas</b> disponibilizadas:</p>
            <ul class="detalhes">
                <li>Pesquisa de alimentos (mais 3.700 cadastros) e receitas (mais de 1.250 preparações) </li>
                <li>Pesquisa avançada: com filtros facilitadores da pesquisa por nutrientes, por grupo de alimentos,
                    categorias de alimentos e Fabricantes
                </li>
                <li>Pesquisar Alimentos favoritos</li>
                <li>Inserir <b>modelos de dietas</b></li>
                <li>Lista de substituição para o paciente</li>
                <li>Copiar e colar refeições ou dietas de um dia para o outro, é outro facilitador</li>
                <li>Horário das refeições</li>
            </ul>

            <p><b>Análise dietética: permite analisar a dieta prescrita</b></p>
            <ul class="detalhes">
                <li>Nutrientes por classificação: dieta/ refeição/ alimento</li>
                <li>% adequação das DRI’s configurada </li>
                <li>Interações nutricionais básica: Rel g/caloria, Rel kcal/N, Peso, PTN-AN/Veg, NPU, NDpcal, Na, NaCl, Sal de adição</li>
                <li>Contagem de carboidratos</li>
                <li>Gráficos: macronutrientes</li>
                <li>Pirâmide alimentar brasileira</li>
            </ul>

            <h5>OUTROS MÓDULOS</h5>
            <p>contempla em seu banco de dados</p>

            <ul class="detalhes">
                <li>Mais 3700 - Fichas técnica de alimentos </li>
                <li>Mais 1250 - Fichas técnica de receitas</li>
                <li>Cadastro de novas fichas de alimentos e receitas</li>
                <li>Pesquisa avançada: de nutrientes, por grupo de alimentos, categorias de alimentos e Fabricantes</li>
                <li>Cadastro de Modelos de dieta</li>
                <li>Cadastro de Alimentos favoritos</li>
                <li>Cadastro de Lista de substituição padrão</li>
                <li>Anamnese: cadastro de modelos</li>
                <li>Recomendações dietoterápicas: cadastro de modelos</li>
                <li>Histórico de evolução consultas por datas</li>
                <li>Cadastro de medidas caseiras</li>
                <li>Assistente para a avaliação simplificada</li>
                <li>Replicar cadastro de alimentos e receitas</li>
                <li>Configurações do sistema para: Ficha técnica alimento e receita, Pesquisa de alimentos e receitas,E-mail para exportação de relatórios, cadastro do nutricionista
                </li>
                <li>Relatórios e capa de relatórios personalizada</li>
                <li>Exportação de relatórios para: PDF, XLS, RTM, BMP</li>
                <li>Enviar relatórios para e-mail do paciente</li>
            </ul>





            <h3>Muitos recursos, pouco investimento</h3>
            <p>Com um investimento R$ 0,97 ao dia, este é o valor da sua amortização em um ano.  </p>
            <p>A aquisição dá direito: 1 licença do direito de uso; Suporte técnico por 90 dias sem custo: via telefone, chat ou e-mail; Treinamento online; Atualização sem custo;</p>
            <p>Após este período você pode optar pelo: <b>PLANO DE ASSISTÊNCIA DIETWIN</b>, contempla uma série de vantagens por tempo ilimitado e não obrigatório. A adesão ao plano oferece: Suporte técnico via telefone, chat ou e-mail; Suporte online; Treinamento online; Atualização sem custo; Upgrade gratuito; Desconto de 50% na 2ª. Licença adquirida; Desconto de 60% na troca ou aquisição de outra versão dietWin. <b>Investimento: <s>De R$ 250,00</s> por R$ 98,00/ANUAL</b></p>


            <div class="box-valor">
            	<p class="valor"><span>R$</span>395</p>
                <p class="condicoes">Podendo ser pago em até 5x no boleto ou cartão de crédito!</p>
                <a href="<?php echo SITE_URL ?>/software/personal/comprar" title="Compre seu dietWin Personal  na Loja Online"><img src="<?php echo SITE_BASE ?>/views/imagens/compre-loja-online-personal.gif" alt="Compre na Loja Online" /></a>
            </div>
            <img src="<?php echo SITE_BASE ?>/views/imagens/dietwin-personal-thumb.jpg" alt="dietWin Personal " />
            <div class="clear"></div>
            
            <h3>O que é preciso para rodar o Personal ?</h3>

            <ul class="detalhes">
           		<li><b>Configuração Mínima </b>(Instalação: local): Processador com 750MHz; 256 Mb de RAM; 500 Mb de espaço livre em disco; Windows 2000 Pro ou superior; Monitor resolução 1024X768, Impressora. ou em rede</li>
           		<li><b>Configuração Mínima </b>(Instalação em Servidor): Processador com 1GHz; 512 Mb de RAM; 500 Mb de espaço livre em disco; Windows 2000 Server ou superior; Windows XP Pro ou superior</li>
			</ul>
            
            <h3>Um canal aberto com você</h3>
            <p>Disponibilizamos treinamentos online para apresentação do sistema. Um manual de ajuda em cada tela aberta do sistema bem como um botão com vídeo de operação ajudam no entendimento do sistema.
                O projeto dietWin avança sempre de forma intermitente, participe apresentando suas sugestões para novas implementações ou modificações dos protocolos.
            </p>
            
            <div class="box-treinamento">
            	<p>Confira os últimos vídeos disponíveis sobre o dietwin Personal</p>
                <?php
                if ($videos AND count($videos)>0)
                {
                ?>
                <ul class="galeria">
                    <?php
                    foreach ($videos as $video)
                    {
                    ?>
                    <?php
                    $regex = '/http\:\/\/www\.youtube\.com\/watch\?v=(\w{11})/';
                    preg_match($regex, $video->link,$matches);
                    ?>
                	<li>
                        <a href="<?php echo SITE_URL ?>/suporte/personal/video/<?php echo $video->titulo_seo ?>" class="thumb" title="<?php echo $video->titulo ?>">
                            <img src="<?php echo SITE_BASE ?>/biblioteca/timthumb.php?src=http://i3.ytimg.com/vi/<?php echo substr($video->link, strrpos($video->link,'v=')+2,11) ?>/0.jpg&w=48&h=48" alt="<?php echo $video->titulo ?>" />
                            <div class="play"></div>
                        </a>
                    </li>
                    <?php
                    }
                    ?>

                </ul>
                <?php
                }
                else
                {
                    echo '<p>Não há vídeos cadastrados para esta categoria.</p>';
                }
                ?>
                <div class="clear"></div>
                <a href="<?php echo SITE_URL ?>/software/personal/videos" title="Veja a galeria completa de vídeos do dietWin Personal ">&#187; Veja a galeria completa</a>
            </div>

            <div class="box-treinamento">
                <p>Últimas dúvidas respondidas sobre a ferramenta:</p>
                <?php
                if ($faqs AND count($faqs)>0)
                {
                    ?>
                    <ul class="duvidas">
                        <?php
                        foreach ($faqs as $faq)
                        {
                            ?>
                            <li><a href="<?php echo SITE_URL.'/suporte/personal/faq/'.$faq->pergunta_seo ?>" title="<?php echo $faq->pergunta ?>"><?php echo $faq->pergunta ?></a></li>
                            <?php
                        }
                        ?>

                    </ul>
                    <?php
                }
                else
                {
                    echo '<p>Não há dúvidas cadastradas para esta categoria.</p>';
                }
                ?>
                <div class="clear"></div>
                <a href="<?php echo SITE_URL ?>/suporte/personal/faq" title="Veja todas as dúvidas sobre o dietWin Personal ">&#187; Veja todas as dúvidas</a>
            </div>
        </div>
        
        <div class="opcoes">
            <!-- inicio do widget do Webchat Locaweb -->
            <div id="webchat_widget"><!-- Coloque esse div onde você quer que o widget apareça. --></div>
            <script type="text/javascript">
                (function() {
                    var wc = document.createElement('script'); wc.type = "text/javascript";
                    wc.src = 'https://dietwin.webchatlw.com.br/widget.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(wc, s);
                })();
            </script>
            <!-- fim do widget do Webchat Locaweb -->
            <a href="#" class="link-mais-info" title="Solicite maiores informações sobre a ferramenta"><img src="<?php echo SITE_BASE ?>/views/imagens/botao-mais-info.png" alt="Solicite maiores informações sobre a ferramenta" /></a>

            <!-- Conteúdo do colorbox do botão "Mais Informações" -->
            <div style="display:none">
                <div class="colorbox-mais-info">
                    <div class="topo"></div>
                    <div class="box">
                        <div class="titulo-colorbox">Mais Informações</div>
                        <div class="conteudo-colorbox">

                            <p>Preencha o formulário abaixo para solicitar mais informações sobre este software.</p>
                            <form method="post" action="<?php echo SITE_URL ?>/personal/mais_info" id="form-mais-info" >

                                <label><b>Nome:</b></label>
                                <input type="text" class="obrigatorio" name="info_nome" id="info_nome" maxlength="100" />

                                <label><b>E-mail:</b></label>
                                <input type="text" class="obrigatorio" name="info_email" id="info_email" maxlength="255" />

                                <label><b>Telefone:</b></label>
                                <input type="text" class="obrigatorio" name="info_telefone" id="info_telefone" maxlength="14" />

                                <label><b>Cidade:</b></label>
                                <input type="text" class="obrigatorio" name="info_cidade" id="info_cidade" maxlength="100" />

                                <label><b>Mensagem:</b></label>
                                <textarea type="text" class="obrigatorio" name="info_mensagem" id="info_mensagem" style="resize: vertical; overflow:auto"></textarea>

                                <button type="submit">Enviar</button>
                                <div class="clear"></div>
                            </form>

                            <div id="form_notification"></div>

                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="inferior"></div>
                </div>
            </div>
            <!-- Fim do colorbox do botão "Mais Informações" -->

            <a href="<?php echo SITE_URL ?>/software/personal/videos" title="Acesse o centro de multimídia e treinamento dessa ferramenta"><img src="<?php echo SITE_BASE ?>/views/imagens/botao-centro-multimidia.png" alt="Acesse o centro de multimídia e treinamento dessa ferramenta" /></a>
            <div class="separador"></div>
            <a href="<?php echo SITE_URL ?>/software/personal/comprar" class="comprar-personal" title="Compre Online seu dietWin Personal "><img src="<?php echo SITE_BASE ?>/views/imagens/botao-compre-online-personal.png" alt="Compre Online seu dietWin Personal " /></a>

            <div class="separador"></div>

            <h3>Conheça as outras ferramentas</h3>

            <ul>
                <li class="bubbleInfo" id="pop10">
                    <a href="<?php echo SITE_BASE ?>/profissional" class="trigger" title="Conheça o dietWin Professional">
                        <img src="<?php echo SITE_BASE ?>/views/imagens/opcao-profissional.jpg" alt="Conheça o dietWin Profissional" />
                        <table id="tpop10" class="popup">
                            <tbody>
                            <tr>
                                <td class="topleft corner"></td> <td class="top"></td>	<td class="topright corner"></td>
                            </tr>
                            <tr>
                                <td class="left"></td> <td><table class="popup-contents">
                                <tbody>
                                <tr>
                                    <td><b>Profissional Plus </b></td>
                                </tr>
                                </tbody></table>
                            </td>
                                <td class="right"></td>
                            </tr>
                            <tr>
                                <td class="bottomleft corner"></td> <td class="bottom"><img width="13" height="29" alt="popup tail" src="<?php echo SITE_BASE ?>/views/imagens/menu-bubble-tail2.gif"/></td> <td class="bottomright corner"></td>
                            </tr>
                            </tbody>
                        </table>
                    </a>
                </li>
                <li class="bubbleInfo" id="pop12">
                    <a href="<?php echo SITE_BASE ?>/rotulo-de-alimentos" class="trigger" title="Conheça o dietWin Rótulo de Alimentos">
                        <img src="<?php echo SITE_BASE ?>/views/imagens/opcao-rotulo-de-alimentos.jpg" alt="Conheça o dietWin Rótulo de Alimentos" />
                        <table id="tpop11" class="popup">
                            <tbody>
                            <tr>
                                <td class="topleft corner"></td> <td class="top"></td>	<td class="topright corner"></td>
                            </tr>
                            <tr>
                                <td class="left"></td> <td><table class="popup-contents">
                                <tbody>
                                <tr>
                                    <td><b>Rótulo de Alimentos</b></td>
                                </tr>
                                </tbody></table>
                            </td>
                                <td class="right"></td>
                            </tr>
                            <tr>
                                <td class="bottomleft corner"></td> <td class="bottom"><img width="13" height="29" alt="popup tail" src="<?php echo SITE_BASE ?>/views/imagens/menu-bubble-tail2.gif"/></td> <td class="bottomright corner"></td>
                            </tr>
                            </tbody>
                        </table>
                    </a>
                </li>
            </ul>
        </div>
        
        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>  
</div>

<div id="div-chat" >
    <a id="chat-abrir" href="#" onClick="wc_popup(); return false;"><img src="<?php echo SITE_BASE; ?>/views/imagens/chat-passivo.png" alt="chat" /></a>
    <a id="chat-fechar" href="#"><img src="<?php echo SITE_BASE; ?>/views/imagens/chat-fechar.png" alt="chat" /></a>
</div>

<?php include 'includes/rodape.php'; ?>