<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.'); ?>

<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php' ?>
		
	<div id="main-content"> <!-- Main Content Section with everything -->

		<!-- Page Head -->
		<h2>Bem-vindo(a) <?php echo $cms_usuario_logado->nome ?>.</h2>
		<p id="page-intro">Escolha uma das opções no menu ao lado.</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

<?php require_once 'fatorcms/views/includes/rodape.php' ?>