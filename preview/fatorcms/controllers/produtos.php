<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_Produtos extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai e testa se o usuário está logado
	 */
	public function __construct()
	{
		parent::__construct();
		$this->verificar_usuario_logado();
		$this->verificar_usuario_permissao('produtos');
	}


	/**
	 * Método padrão que não deve ser chamado diretamente. Se for, vai para a listagem
	 * @return void
	 */
	public function index()
	{
        header('Location: '.SITE_URL.'/fatorcms/produtos/listar');
		exit;
	}


	/**
	 * Método inicial que exibe a lista de registros do banco de dados
	 * @param $parametros object
	 * @return void
	 */
	public function listar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'produtos-lista.php'));
		$view->adicionar('body_class', 'produtos lista');
		$view->adicionar('lateral_menu', array('item'=>'produtos','link'=>'listar todos'));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$this->view_adicionar_obrigatorias($view);

		//-----

		// Lógica caso tenha sido feita uma busca na listagem
		$clausula_where = ' TRUE ';

		$buscar = NULL;
		if (isset($parametros->buscar) AND strlen(trim($parametros->buscar)) > 0)
		{
			$buscar = trim($parametros->buscar);
			$buscar_aux = Funcoes::mysqli_escape(str_replace(' ', '%', $buscar));
			$clausula_where .= ' AND (nome LIKE "%'.$buscar_aux.'%")';
		}

		//-----

		// Ordenação dos resultados, podendo vir da página
		$ordenar_por = (isset($parametros->ordenar_por)) ? strtolower($parametros->ordenar_por) : 'nome'; // Valor padrão
		$ordem = isset($parametros->ordem) ? strtolower($parametros->ordem) : 'desc'; // Valor padrão

		switch ($ordenar_por)
		{
			case 'nome': $ordenar_por_sql = 'nome'; break;
			case 'valor': $ordenar_por_sql = 'valor'; break;
			case 'versao': $ordenar_por_sql = 'versao'; break;
			case 'valor-da-licenca': $ordenar_por_sql = 'licenca_valor'; break;
			default : $ordenar_por_sql = $ordenar_por; break;
		}

		//-----

		$produto = new FatorCMS_Model_Produto;
		// Define a cláusula do select
		$produto->clausula = 'SELECT * FROM {tabela_nome} WHERE '.$clausula_where.' ORDER BY '.$ordenar_por_sql.' '.$ordem
		;

		// Através da paginação vai verificar se existe a necessidade de dividir o resultado em várias páginas e se é necessário adicionar um LIMIT
		$paginacao = new Paginacao($produto, $parametros, 20, 10);

		// Executa a cláusula lá de cima
		$produtos = $produto->select(NULL, TRUE); // NULL para não alterar a clásula já definida e TRUE para forçar retorno em array

		//-----

		$view->adicionar('buscar', $buscar);
		$view->adicionar('ordenar_por', $ordenar_por);
		$view->adicionar('ordem', $ordem);
		$view->adicionar('produtos', $produtos);
		$view->adicionar('paginacao', $paginacao);

		$view->exibir();
	}


	/**
	 * Dependendo do parâmetro, aprensenta a tela de edição ou de cadastro
	 * @param $parametros object
	 * @return void
	 */
	public function editar($parametros)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'produtos-form.php'));

        $view->adicionar('body_class', 'produtos form');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
		$this->view_adicionar_obrigatorias($view);

        //-----

        // Decide o que fazer
        if (isset($parametros->_0) AND is_numeric($parametros->_0))
        {
            // É atualização
            $produto = new FatorCMS_Model_Produto;
	        $produto = $produto->select('SELECT * FROM {tabela_nome} AS produto WHERE id = '.Funcoes::mysqli_escape($parametros->_0));
            $view->adicionar('produto', $produto);

            $view->adicionar('lateral_menu', array('item'=>'produtos','link'=>''));
        }
        else
        {
            // É cadastro
			//$view->adicionar('produto', NULL);
			//$view->adicionar('lateral_menu', array('item'=>'produtos','link'=>'cadastrar'));
	        header('Location: '.SITE_URL.'/fatorcms/produtos/listar');
	        exit;
        }

		// Lista de Produtos para o <select>
        $produto = new FatorCMS_Model_Produto;
        $view->adicionar('produtos', $produto->select('SELECT * FROM {tabela_nome} ORDER BY nome', TRUE));

        $view->exibir();
    }


	/**
	 * Recebe os parâmetros do formulário e adiciona o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	/*public function cadastrar($parametros)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'produtos-form.php'));
		$this->view_adicionar_obrigatorias($view);

		//-----

		if ($parametros AND isset($parametros->nome))
		{
			$produto = new FatorCMS_Model_Produto;
			$produto->carregar($parametros);

			// Campos especiais
			$produto->nome_seo = Funcoes::texto_para_seo($produto->nome);

			//-----

			// Log
			$produto->log_id = $this->cms_usuario_logado->id;
			$produto->log_data = date('Y-m-d H:i:s');

			//-----

            $produto->colunas_mysqli_escape();

			if ($produto->verificar_null(array('id')))
			{
				// Validações ---

                // Verifica se o nome_seo está disponível, se não inicia o contador até achar um
                $cont = 2;
                do
                {
                    $produto_existe = new FatorCMS_Model_Produto;
                    $produto_existe = $produto_existe->select('SELECT id FROM {tabela_nome} WHERE nome_seo = "'.$produto->nome_seo.'"');
                    if ($produto_existe AND ! is_null($produto_existe->id))
                    {
                        $produto->nome_seo = Funcoes::texto_para_seo($produto->nome).'-'.$cont++;
                    }
                } while ($produto_existe AND ! is_null($produto_existe->id));

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($produto->insert())
					{
						new Notificacao('Produto <strong>'.$produto->nome.'</strong> cadastrado com sucesso.', 'success', TRUE);
						header('Location: '.SITE_URL.'/fatorcms/produtos/listar');
						exit;
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao salvar os dados do produto.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
			}
            
			// Para retornar ao formulário com valores inseridos
			$view->adicionar('produto', $produto);
		}
		else
		{
			$view->adicionar('produto', NULL);
		}

		//-----

		$view->adicionar('body_class', 'produtos formulario');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
        $view->adicionar('lateral_menu', array('item'=>'produtos','link'=>'cadastrar produto'));

        $view->exibir();
	}*/


	/**
	 * Recebe os parâmetros do formulário e atualiza o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function atualizar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'produtos-form.php'));
		$this->view_adicionar_obrigatorias($view);

		//-----

		if (isset($parametros->id) AND is_numeric($parametros->id))
		{
			$produto = new FatorCMS_Model_Produto;
			$produto->carregar($parametros);

			// Campos especiais
			$produto->nome_seo = Funcoes::texto_para_seo($produto->nome);

			//-----

			// Verificamos se o nome_seo de antes é o igual ao novo. Se for diferente verificamos a possibilidade
			$produto_atual = new FatorCMS_Model_Produto($produto->id);
			if ($produto->nome_seo != $produto_atual->nome_seo)
			{
				// Verifica se o nome_seo está disponível (desde que não seja o do próprio registro), se não inicia o contador até achar um
				$cont = 2;
				do
				{
					$produto_existe = new FatorCMS_Model_Produto;
					$produto_existe = $produto_existe->select('SELECT id FROM {tabela_nome} WHERE nome_seo = "'.$produto->nome_seo.'" AND id != '.$produto->id);
					if ($produto_existe AND ! is_null($produto_existe->id))
					{
						$produto->nome_seo = Funcoes::texto_para_seo($produto->nome).'-'.$cont++;
					}
				} while ($produto_existe AND ! is_null($produto_existe->id));
			}

			//-----

			// Log
			$produto->log_id = $this->cms_usuario_logado->id;
			$produto->log_data = date('Y-m-d H:i:s');

			//-----

			$produto->colunas_mysqli_escape();

			if ($produto->verificar_null())
			{
				// Validações ---

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($produto->update())
					{
						new Notificacao('Produto <strong>'.$produto->nome.'</strong> atualizado com sucesso.', 'success', TRUE);
						header('Location: '.SITE_URL.'/fatorcms/produtos/listar');
						exit;
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao atualizar os dados do produto.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos os campos marcados são obrigatórios.');
			}

			// Para retornar ao formulário com valores inseridos
			$view->adicionar('produto', $produto);
		}
		else
		{
			$notificacao = new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention');
		}

		//-----

		// Lista de Produtos para o <select>
        $produto = new FatorCMS_Model_Produto;
        $view->adicionar('produtos', $produto->select('SELECT * FROM {tabela_nome} ORDER BY nome', TRUE));

		$view->adicionar('body_class', 'produtos formulario');
		$view->adicionar('lateral_menu', array('item'=>'produtos','link'=>''));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$view->exibir();
	}


	/**
	 * Recebe o ID do registro e excluir ele do banco de dados
	 * @param  $parametros
	 * @return void
	 */
	/*public function excluir($parametros)
	{
		if (isset($parametros->_0) AND is_numeric($parametros->_0))
		{
			$produto = new FatorCMS_Model_Produto($parametros->_0);

			if ($produto AND ! is_null($produto->id))
			{
				if ($produto->delete())
				{
					new Notificacao('O produto <strong>'.$produto->nome.'</strong> foi excluído com sucesso.', 'success', TRUE);
				}
				else
				{
					new Notificacao('Ocorreu um erro ao excluir o produto <strong>'.$produto->nome.'</strong>.', 'error', TRUE);
				}
            }
			else
			{
				new Notificacao('Nenhuma informação encontrada para este produto.', 'error', TRUE);
			}
		}
		else
		{
			new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention', 'error', TRUE);
		}

		header('Location: '.SITE_URL.'/fatorcms/produtos/listar');
		exit;
	}*/



	/****************************** MÉTODOS EXTRAS ******************************/


} // end class