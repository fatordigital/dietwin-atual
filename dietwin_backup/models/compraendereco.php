<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property int compra_id
 * @property string tipo
 * @property string endereco
 * @property int numero
 * @property string complemento
 * @property string bairro
 * @property int cidade_id
 * @property int estado_id
 * @property int cep
 * @property string observacao
 */

class Model_CompraEndereco extends Model_Padrao
{
	protected $tabela_nome = 'compras_enderecos';


    /****************************** NOVOS MÉTODOS ******************************/


    /**
     * Executa o delete no banco, conforme o os parâmetros salvos no objeto
     * @return bool
     */
    public function delete()
    {
        // "Limpa" os valores
        $this->colunas_mysqli_escape();

        if ( ! is_null($this->id))
        {
            // Exclusão de um registro específico
            $sql = 'DELETE FROM '.$this->tabela_nome.' WHERE id = '.$this->id;
            return $this->query($sql);
        }
        elseif ( ! is_null($this->compra_id))
        {
            // Exclusão de todos os registro de determinada compra
            $sql = 'DELETE FROM '.$this->tabela_nome.' WHERE compra_id = '.$this->compra_id;
            return $this->query($sql);
        }

        // Se chegou aqui, é porque não excluiu nada
        return FALSE;
    }

} // end class