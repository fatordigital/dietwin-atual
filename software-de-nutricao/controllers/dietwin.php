<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Dietwin extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
		//$_SESSION['produto_temp'] = 'dietwin';


    	
    	if(isset($_SESSION['produto_temp']) AND $_SESSION['produto_temp']=='dietwin-plus'){
    		$view = new View('dietwinplus.php');
    	}else{
			$_SESSION['produto_temp'] = 'dietwin';
    		$view = new View('dietwin.php');
    	}

    	//var_dump($view); die;
    	
		$this->view_variaveis_obrigatorias($view);

    	$produto = new Model_Produto();
    	$dietwin_preco = $produto->select("SELECT * FROM {tabela_nome} WHERE nome_seo='".$_SESSION['produto_temp']."'");
    	$dietwin_preco = $dietwin_preco->valor;

    	$view->adicionar('dietwin_valor',$dietwin_preco);

		$view->adicionar('body_class', 'index');
		$view->adicionar('notificacao',new Notificacao());
    	$view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	public function produto($parametros){

		//print_r($parametros); die;

		if(!isset($parametros->_0)){
			$_SESSION['produto_temp'] == 'dietwin';
			header('Location: '.SITE_URL.'/dietwin');
		}else{
			$_SESSION['produto_temp'] = $parametros->_0;
		}
		header('Location: '.SITE_URL.'/dietwin');		
	}


} // end class