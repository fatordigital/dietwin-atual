<?php include 'includes/cabecalho.php'; ?>

<body class="index">

<?php include 'includes/topo.php'; ?>

    <div class="titulo-principal">
        <span>Para quem busca a melhor solução em tecnologia para a</span>
        <div class="clear"></div>
        <p>NUTRIÇÃO</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container">        
        <div class="familia-dietwin">
        	<h2><span>Família</span><br /> dietWin</h2>
            <ul>
                <li class="professional">
                    <h3>Profissional </h3>
                    <p>É a versão mais completa da família de softwares dietWin. Executa muito mais do que os procedimentos padrões da avaliação clínica, avaliação nutricional e prescrição dietética para todos os ciclos da vida, pois contempla a consulta nos protocolos Doenças, Fármacos, Sintomas e sinais clínicos, Exames laboratoriais. ... </p>
                    <a href="<?php echo SITE_URL ?>/profissional" class="cufon" title="Conheça o Profissional "><span>CONHEÇA O</span> PROFISSIONAL </a>
                </li>
                <li class="personal">
                    <h3>Personal </h3>
                    <p>É uma versão que atende todas as necessidades básicas da avaliação nutricional e prescrição dietética para todos os ciclos da vida. Um novo lay-out de telas é apresentado para uma nova proposta de atendimento clínico diferenciado onde a evolução da consulta fica disponível durante todo o atendimento. Permite a abertura de múltiplas fichas de pacientes e pesquisas facilitadas. ...</p>
                    <a href="<?php echo SITE_URL ?>/personal" class="cufon" title="Conheça o Profissional "><span>CONHEÇA O</span> PERSONAL </a>
                </li>
                <li class="rotulo-alimentos">
                    <h3>Rótulo de Alimentos</h3>
                    <p>Este programa tem a finalidade principal de calcular as Informações Nutricionais que devem estar presentes nos rótulos dos alimentos embalados de acordo as regras estabelecidas e as exigência Brasileiras determinadas pela ANVISA Resolução - RDC nº 360, de 23 de dezembro de 2003 - Aprova Regulamento Técnico sobre Rotulagem ... </p>
                    <a href="<?php echo SITE_URL ?>/rotulo-de-alimentos" class="cufon" title="Conheça o Rótulo de Alimentos"><span>CONHEÇA O</span> RÓTULO DE ALIMENTOS</a>
                </li>
                <li class="descubra-versao">
                    <h3>Qual versão eu escolho?</h3>
                    <a href="<?php echo SITE_URL ?>/qual-versao-escolho" class="cufon" title="Descubra qual a melhor ferramenta para atender às suas necessidades">DESCUBRA QUAL A MELHOR FERRAMENTA PARA ATENDER ÀS SUAS NECESSIDADES.</a>
                    <p>A família dietWin é composta por 3 softwares inovadores no mercado de nutrição, cada um como funcionalidades e recursos especifícos.</p>
                </li>
            </ul>
            <div class="clear"></div>
        </div>
        
        <div class="clear"></div>

        <div class="inferior"></div>
    </div>    
</div>

<?php include 'includes/rodape.php'; ?>