<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="content-language" content="pt-br" />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tabela Nutricional | DietWin</title>

        <!-- BEIGN: META NAME -->
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <!-- END: META NAME -->
        <!-- BEGIN: META TAGS DE GEOLOCALIZACAO -->
        <meta name="geo.position" content="">
        <meta name="geo.placename" content="">
        <meta name="geo.region" content="">
        <!-- END: META TAGS DE GEOLOCALIZACAO -->

        <!-- BEGIN: CARREGAMENTO DOS FAVICONS -->
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo SITE_URL ?>/assets_tabela/images/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo SITE_URL ?>/assets_tabela/images/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_URL ?>/assets_tabela/images/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo SITE_URL ?>/assets_tabela/images/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_URL ?>/assets_tabela/images/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo SITE_URL ?>/assets_tabela/images/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo SITE_URL ?>/assets_tabela/images/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo SITE_URL ?>/assets_tabela/images/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo SITE_URL ?>/assets_tabela/images/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo SITE_URL ?>/assets_tabela/images/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo SITE_URL ?>/assets_tabela/images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo SITE_URL ?>/assets_tabela/images/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo SITE_URL ?>/assets_tabela/images/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo SITE_URL ?>/assets_tabela/images/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo SITE_URL ?>/assets_tabela/images/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- END: CARREGAMENTO DOS FAVICONS -->
        <!-- BEGIN: CARREGAMENTO DAS FOLHAS DE ESTILO/FONTS -->
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans+Condensed:200,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans+Extra+Condensed:200,400,600" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo SITE_URL ?>/assets_tabela/css/normalize.css">
        <link rel="stylesheet" href="<?php echo SITE_URL ?>/assets_tabela/css/lib/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo SITE_URL ?>/assets_tabela/css/lib/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo SITE_URL ?>/assets_tabela/css/lib/owl.theme.default.min.css">
        <link rel="stylesheet" href="<?php echo SITE_URL ?>/assets_tabela/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo SITE_URL ?>/assets_tabela/css/main.css">
        <!-- END: CARREGAMENTO DAS FOLHAS DE ESTILO/FONTS -->

        <!-- BEGIN: ANALYTICS -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-3191692-4', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- END: ANALYTICS -->

    </head>
    <body>

<a name="#A1" class="aaa">anchor</a>
    <section class="header">
<header>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#"><img src="<?php echo SITE_URL ?>/assets_tabela//images/tabela-nutricional.png" alt="Tabela Nutricional"></a>
                    </div>
                    <div class="nav navbar-nav navbar-right">
                        <a href="http://app.dietwin.com.br" class="btn btn-primary navbar-btn outline">
                            <span class="hidden-xs hidden-sm">Acesse sua conta</span><span class="hidden-md hidden-lg"><i class="fa fa-sign-in" aria-hidden="true"></i></span></a>
                        <a href="javascript:void(0)" class="btn btn-primary navbar-btn" v-on:click="opened = true">FAÇA UM TESTE GRÁTIS</a>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </nav>
    <div class="header-content">
        <div class="header-content-front text-center">
            <h1>
                Dietwin - Tabela Nutricional.<br />
                <small>Faça rótulo de alimentos em minutos.</small>
            </h1>
            <a href="http://app.dietwin.com.br/usuarios/novo" target="_blank" class="btn btn-danger">FAÇA UM TESTE <strong>GRATUITO</strong></a>
        </div>
    </div>
    <!-- <div class="center-canvas hide">
        <canvas id="canvas" width="100%" height="auto" style="display: block; background-color:rgba(255, 255, 255, 0.00)"></canvas>
    </div> -->
    <div class="center-canvas">
        <figure class="center">
            <img class="hidden-xs hidden-sm" src="<?php echo SITE_URL ?>/assets_tabela//images/slider/computador.png" alt="">
            <img class="hidden-md hidden-lg" src="<?php echo SITE_URL ?>/assets_tabela//images/slider/smartphone.png" alt="">
        </figure>
        <div class="canvas-left">
            <figure class="item">
                <img src="<?php echo SITE_URL ?>/assets_tabela//images/slider/fruta1.png" alt="">
            </figure>
            <figure class="item">
                <img src="<?php echo SITE_URL ?>/assets_tabela//images/slider/fruta2.png" alt="">
            </figure>
            <figure class="item">
                <img src="<?php echo SITE_URL ?>/assets_tabela//images/slider/fruta3.png" alt="">
            </figure>
            <figure class="item">
                <img src="<?php echo SITE_URL ?>/assets_tabela//images/slider/fruta4.png" alt="">
            </figure>
            <figure class="item">
                <img src="<?php echo SITE_URL ?>/assets_tabela//images/slider/fruta5.png" alt="">
            </figure>
        </div>
        <div class="canvas-right">
            <figure class="item">
                <img src="<?php echo SITE_URL ?>/assets_tabela//images/slider/produto1.png" alt="">
            </figure>
            <figure class="item">
                <img src="<?php echo SITE_URL ?>/assets_tabela//images/slider/produto2.png" alt="">
            </figure>
            <figure class="item">
                <img src="<?php echo SITE_URL ?>/assets_tabela//images/slider/produto3.png" alt="">
            </figure>
            <figure class="item">
                <img src="<?php echo SITE_URL ?>/assets_tabela//images/slider/produto4.png" alt="">
            </figure>
            <figure class="item">
                <img src="<?php echo SITE_URL ?>/assets_tabela//images/slider/produto5.png" alt="">
            </figure>
        </div>
    </div>
</header>

<main>
    <section class="basic-info">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-10 col-md-offset-1 center-flex">
                    <div class="owl-carousel owl-carousel-1">
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-12 col-sm-5 col-sm-offset-0 col-md-3 col-md-offset-1 no-padding">
                                    <figure>
                                        <img src="<?php echo SITE_URL ?>/assets_tabela/images/rotulagem-de-alimentos.png" alt="Rotulagem Nutricional">
                                    </figure>
                                </div>
                                <div class="col-xs-12 col-sm-7 col-sm-offset-0 col-md-7 col-md-offset-1 no-padding-left">
                                    <div class="owl-item-content">
                                        <h3>
                                            Rotulagem Nutricional<br />
                                            <small>Simples, rápido e confiável.</small>
                                        </h3>
                                        <p class="white">
                                            Sistema desenvolvido por pessoas experientes em montagem de tabelas nutricionais. Em apenas alguns minutos e poucos cliques.
                                        </p>
                                        <p class="white">
                                            Conte com o aliado perfeito para realizar o cálculo nutricional de maneira rápida e confiável. O software faz o cálculo a partir da ficha técnica, atendendo rigorosamente aos padrões das resoluções 359 e 360 da ANVISA.
                                        </p>
                                        <p>
                                            <a href="http://app.dietwin.com.br/usuarios/novo" target="_blank" class="btn btn-danger">FAÇA UM TESTE GRATUITO</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <figure>
                                        <img src="<?php echo SITE_URL ?>/assets_tabela/images/rotulagem-de-alimentos.png" alt="Rotulagem Nutricional">
                                    </figure>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <div class="owl-item-content">
                                        <h3>
                                            Rotulagem Nutricional<br />
                                            <small>A quem se destina?</small>
                                        </h3>
                                        <p class="white">
                                            O Dietwin foi desenvolvido pensando em auxiliar o seu trabalho em todos os momentos, seja para uso de nutricionistas consultores, profissionais especializados, Indústrias alimentícias ou empresas de todos os portes, voltadas a produção e/ou distribuição de gêneros alimentícios, restaurantes, padarias, hospitais, pesquisadores e Unidades de Alimentação e Nutrição (UAN's).
                                        </p>
                                        <p>
                                            <a href="http://app.dietwin.com.br/usuarios/novo" target="_blank" class="btn btn-danger">FAÇA UM TESTE GRATUITO</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <figure>
                                        <img src="<?php echo SITE_URL ?>/assets_tabela/images/rotulagem-de-alimentos.png" alt="Rotulagem Nutricional">
                                    </figure>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <div class="owl-item-content">
                                        <h3>
                                            Rotulagem Nutricional<br />
                                            <small>Como é calculada a Informação Nutricional?</small>
                                        </h3>
                                        <p class="white">
                                            Com o acesso a um banco de dados com mais de 15 mil ingredientes a ferramenta de pesquisa nas tabelas TACO, USDA, IBGE e DIETWIN, permite ainda você construir e adicionar a sua tabela com ingredientes e receitas.
                                        </p>
                                        <p class="white">
                                            Calcula automaticamente o valor nutricional das suas receitas através de uma ficha técnica com cálculos de análise dietética completa e avançada.
                                        </p>
                                        <p>
                                            <a href="http://app.dietwin.com.br/usuarios/novo" target="_blank" class="btn btn-danger">FAÇA UM TESTE GRATUITO</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <figure>
                                        <img src="<?php echo SITE_URL ?>/assets_tabela/images/rotulagem-de-alimentos.png" alt="Rotulagem Nutricional">
                                    </figure>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <div class="owl-item-content">
                                        <h3>
                                            Faça você mesmo a Tabela Nutricional
                                        </h3>
                                        <p class="white">
                                            A tabela nutricional é obrigatória em quase todos os alimentos comercializados prontos, seja caseiro ou industrializado, e tem como objetivo informar o consumidor a quantidade de nutrientes e calorias que aquele alimento ou bebida possui.
                                        </p>
                                        <p class="white">
                                            Conhecida também como Informação Nutricional, esta tabela possui as quantidades de carboidratos, proteínas, gorduras totais, além de vitaminas e minerais, por porção de acordo com o VRC (Valor de Referência para Consumo). Você só precisa da ficha técnica completa, com ingredientes e quantidades para o sistema calcular todas informações nutricionais.
                                        </p>
                                        <p>
                                            <a href="http://app.dietwin.com.br/usuarios/novo" target="_blank" class="btn btn-danger">FAÇA UM TESTE GRATUITO</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <a name="#A3" class="aaa">anchor</a>
    <section class="software-info">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1">
                    <h2>
                        Primeiro software de nutrição. Desde 1994.<br />
                        <small>Muitos recursos para facilitar o seu trabalho.</small>
                    </h2>
                    <div class="item same-height">
                        <figure>
                            <img src="<?php echo SITE_URL ?>/assets_tabela/images/icones/valores-diarios.png" alt="% Valores Diários (IDR) baseados para uma dieta de 2.000 calorias.">
                        </figure>
                        <p>
                            % Valores Diários (IDR) baseados para uma dieta de 2.000 calorias.
                        </p>
                    </div>
                    <div class="item same-height">
                        <figure>
                            <img src="<?php echo SITE_URL ?>/assets_tabela/images/icones/ficha-tecnica.png" alt="Ficha Técnica de Alimentos e receitas">
                        </figure>
                        <p>Ficha Técnica de alimentos e receitas.</p>
                    </div>
                    <div class="item same-height">
                        <figure>
                            <img src="<?php echo SITE_URL ?>/assets_tabela/images/icones/listagem-de-alimentos.png" alt="Listagens de alimentos e produtos">
                        </figure>
                        <p>
                            Listagens de alimentos e produtos.
                        </p>
                    </div>
                    <div class="item same-height">
                        <figure>
                            <img src="<?php echo SITE_URL ?>/assets_tabela/images/icones/analise-de-informacao.png" alt="Análise de Informação Nutricional de mais de 168 nutrientes.">
                        </figure>
                        <p>
                            Análise de Informação Nutricional de mais de 168 nutrientes.
                        </p>
                    </div>
                    <div class="item same-height">
                        <figure>
                            <img src="<?php echo SITE_URL ?>/assets_tabela/images/icones/modelos-de-rotulos.png" alt="3 modelos de rótulo disponíveis, de acordo com a ANVISA.">
                        </figure>
                        <p>
                            3 modelos de rótulo disponíveis, de acordo com a ANVISA.
                        </p>
                    </div>
                    <div class="item same-height">
                        <figure>
                            <img src="<?php echo SITE_URL ?>/assets_tabela/images/icones/configuracao.png" alt="Configuração: nutrientes, relatório, rótulo.">
                        </figure>
                        <p>
                            Configuração: nutrientes, relatório, rótulo.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h3>
                        Tenha acesso gratuito até 3 receitas com 10 ingredientes.<br />
                        Faça sua rotulagem nutricional com todo suporte técnico e nutricional necessário.
                    </h3>
                    <a href="http://app.dietwin.com.br/usuarios/novo" target="_blank" class="btn btn-default">FAÇA UM TESTE GRÁTIS SISTEMA DE ROTULAGEM NUTRICIONAL ONLINE</a>
                </div>
            </div>
        </div>
    </section>
    <a name="#A4" class="aaa">anchor</a>
    <section class="cross-info">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-5 col-md-offset-1 no-padding">
                    <figure>
                        <img src="<?php echo SITE_URL ?>/assets_tabela/images/pc.png" alt="" class="img-responsive center-block">
                    </figure>
                </div>
                <div class="col-xs-12 col-md-5">
                    <h3>
                        Faça tudo online.<br />
                        <small>Você só precisa de um navegador.</small>
                    </h3>
                    <p>
                        A Dietwin é uma empresa especializada em Software de Nutrição, que esta há 20 anos inovando, agora apostando em cloudcomputer (plataforma on-line). Simplificar as tarefas e melhorar a tomada de decisão é com a Dietwin.
                    </p>
                    <p>
                        Contamos com um ótimo atendimento e uma equipe de nutricionistas para melhor entender suas necessidades e resolver seus problemas.
                    </p>
                    <ul class="list-inline">
                        <li>
                            <figure>
                                <img src="<?php echo SITE_URL ?>/assets_tabela/images/google-chrome.png" alt="ícone do navegador Google Chrome">
                            </figure>
                        </li>
                        <li>
                            <figure>
                                <img src="<?php echo SITE_URL ?>/assets_tabela/images/mozilla-firefox.png" alt="ícone do navegador Mozilla Firefox">
                            </figure>
                        </li>
                        <li>
                            <figure>
                                <img src="<?php echo SITE_URL ?>/assets_tabela/images/internet-explorer.png" alt="ícone do navegador Internet Explorer">
                            </figure>
                        </li>
                        <li>
                            <figure>
                                <img src="<?php echo SITE_URL ?>/assets_tabela/images/opera.png" alt="ícone do navegador Opera">
                            </figure>
                        </li>
                        <li>
                            <figure>
                                <img src="<?php echo SITE_URL ?>/assets_tabela/images/safari.png" alt="ícone do navegador Safari">
                            </figure>
                        </li>
                    </ul>
                    <a href="http://app.dietwin.com.br/usuarios/novo" class="btn btn-danger">FAÇA UM TESTE GRATUITO</a>
                </div>
            </div>
        </div>
    </section>
    <a name="#A5" class="aaa">anchor</a>
    <section class="diet-win-online">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6 col-md-offset-1">
                    <h3>
                        Quer saber como montar tabelas nutricionais de maneira rápida sem ter que fazer cálculos?<br />
                        <span class="text-danger">Surpeenda-se com o Dietwin Online</span>
                    </h3>
                    <p>
                        Preencha o formulário e agende uma apresentação do software Dietwin Tabela Nutricional.
                    </p>
                </div>
                <div class="col-md-4">
                    <div id="success"></div>
                    <form action="#" method="post">
                        <div class="form-group">
                            <input type="text" id="nome" name="nome" class="form-control" placeholder="Nome" required="">
                        </div>
                        <div class="form-group">
                            <input type="text" id="email" name="email" class="form-control" placeholder="Email" required="">
                        </div>
                        <div class="form-group">
                            <textarea name="mensagem" id="mensagem" cols="30" rows="10" class="form-control" placeholder="Mensagem" required=""></textarea>
                        </div>
                        <button type="submit" name="button" id="enviar" class="btn btn-default pull-right">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <a name="#A6" class="aaa">anchor</a>
    <section class="blog">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-10 col-md-offset-1">
                    <h3>
                        Acompanhe nosso blog
                    </h3>

                    <div class="col-xs-12 col-sm-6">
                        <div class="post">
                            <a href="#" target="_blank">
                                <figure>
                                    <img src="<?php echo SITE_URL ?>/assets_tabela/images/blog-image.jpg" alt="Rotulagem dos alimentos alergênicos">
                                </figure>
                            </a>
                            <div class="post-content">
                                <h4><a href="javascript:void(0)" target="_blank">Rotulagem dos alimentos alergênicos</a></h4>
                                <p>
                                    <a href="javascript:void(0)" target="_blank">
                                        Apesar de serem muito comuns, as alergias alimentares não são tratadas com prioridade, o que fez com que a ANVISA criasse uma resolução (RDC n.26/2015) em que regulamenta a rotulagem dos alimentos alergênicos.
                                    </a>
                                </p>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="comments pull-left">
                                            <h6>0 comentário (s)</h6>
                                        </div><!-- end of .comments.pull-left -->
                                    </div><!-- end of .col-xs-6 -->
                                    <div class="col-xs-6">
                                        <a href="javascript:void(0)" class="btn btn-primary pull-right">
                                            Leia mais
                                        </a>
                                    </div><!-- end of .col-xs-6 -->
                                </div><!-- end of row. -->
                                <div class="post-date">
                                    <span>12<br /><small>Fev</small></span>
                                </div><!-- end of .post-date -->
                            </div><!-- end of .post-content -->
                        </div><!-- end of .post -->
                    </div><!-- end of .cols -->

                    <div class="col-xs-12 col-sm-6">
                        <div class="post">
                            <a href="#" target="_blank">
                                <figure>
                                    <img src="<?php echo SITE_URL ?>/assets_tabela/images/blog-image.jpg" alt="Rotulagem dos alimentos alergênicos">
                                </figure>
                            </a>
                            <div class="post-content">
                                <h4><a href="javascript:void(0)" target="_blank">Rotulagem dos alimentos alergênicos</a></h4>
                                <p>
                                    <a href="javascript:void(0)" target="_blank">
                                        Apesar de serem muito comuns, as alergias alimentares não são tratadas com prioridade, o que fez com que a ANVISA criasse uma resolução (RDC n.26/2015) em que regulamenta a rotulagem dos alimentos alergênicos.
                                    </a>
                                </p>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="comments pull-left">
                                            <h6>0 comentário (s)</h6>
                                        </div><!-- end of .comments.pull-left -->
                                    </div><!-- end of .col-xs-6 -->
                                    <div class="col-xs-6">
                                        <a href="javascript:void(0)" class="btn btn-primary pull-right">
                                            Leia mais
                                        </a>
                                    </div><!-- end of .col-xs-6 -->
                                </div><!-- end of row. -->
                                <div class="post-date">
                                    <span>12<br /><small>Fev</small></span>
                                </div><!-- end of .post-date -->
                            </div><!-- end of .post-content -->
                        </div><!-- end of .post -->
                    </div><!-- end of .cols -->

                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <a href="http://www.rotulodealimentos.com.br/blog" target="_blank" class="btn btn-danger">VER TODAS AS NOVIDADES</a>
                        </div>
                    </div>
                </div><!-- end of .cols -->
            </div><!-- end of .row -->
        </div><!-- end of .container -->
    </section>

    <a name="#A7" class="aaa">anchor</a>
    <section class="table-prices">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="text-center">
                        Conheça nossos planos.<br />
                        <small>Dúvidas? Fale conosco através de um de nossos canais de atendimento.</small>
                    </h2>
                </div><!-- end of .col-xs-12 -->
            </div><!-- end of .row -->
            <div class="row">
                <div class="col-xs-12 col-sm-4 price text-center">
                    <h4>Avulso</h4>
                    <ul class="list-unstyled">
                        <li>Tempo Indeterminado</li>
                        <li>suporte on-line</li>
                        <li><strong><small>R$</small> <big>39,90</big></strong></li>
                    </ul>
                    <a href="https://app.dietwin.com.br/comprar/rotulagem?promocaorotulagem&quantidade=1" target="_blank" class="btn btn-default btn-invert">COMPRAR</a>
                </div>
                <div class="col-xs-12 col-sm-4 price best text-center">
                    <div class="new">
                        O MAIS CONTRATADO
                    </div>
                    <h4>Pacote de 10 Rótulos</h4>
                    <ul class="list-unstyled">
                        <li>30 dias de acesso</li>
                        <li>Unidades adicionais: <strong><small>R$</small> <big>9,98</big></strong></li>
                        <li><strong><small>R$</small> <big>139,70</big></strong></li>
                    </ul>
                    <a href="https://app.dietwin.com.br/login?promocaoacesso&pacote=standard&assinatura=mensal&rotulos=10&codigoPromocional=" target="_blank" class="btn btn-default">COMPRAR</a>
                </div>
                <div class="col-xs-12 col-sm-4 price text-center">
                    <h4>Pacote de 100 Rótulos</h4>
                    <ul class="list-unstyled">
                        <li>1 ano de acesso.</li>
                        <li>Unidades adicionais: <strong><small>R$</small> <big>4,98</big></strong></li>
                        <li><strong><small>R$</small> <big>857,10</big></strong></li>
                    </ul>
                    <a href="https://app.dietwin.com.br/login?promocaoacesso&pacote=standard&assinatura=anual&rotulos=100&codigoPromocional=" target="_blank" class="btn btn-default btn-invert">COMPRAR</a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-10 col-md-offset-1 suporte text-center">
                    <figure>
                        <img src="<?php echo SITE_URL ?>/assets_tabela/images/icones/suporte-icon.png" alt="">
                    </figure>
                    <h3>Suporte especializado</h3>
                    <p>
                        Está com alguma dificuldade em trabalhar com um software da família Dietwin?<br />
                        Fique tranquilo que iremos ajudá-lo. Utilize um dos canais abaixo para fazer contato.
                    </p>
                    <p>
                        <big><img src="<?php echo SITE_URL ?>/assets_tabela/images/icones/whatsapp.png" alt=""> 99930.2289</big><br />
                        <big>(51) 3337.7908</big><br />
                        <a href="suporte@dietwin.com.br">suporte@dietwin.com.br</a>
                    </p>
                </div>
            </div>
        </div><!-- end of .container -->
    </section>
    <a name="#A8" class="aaa">anchor</a>
    <section class="footer">
        <a href="#" target="_blank">
            <figure>
                <img src="<?php echo SITE_URL ?>/assets_tabela/images/diet-win-logo.png" alt="Diet Win">
            </figure>
        </a>
    </section>

<!-- BEGIN: MODAL -->
<div class="fd-modal-overlay" v-on:click="opened = false"></div>
<div class="fd-modal">
    <a href="javascript:void(0)" class="fd-modal-close" v-on:click="opened = false">
        <img src="<?php echo SITE_URL ?>/assets_tabela/images/icones/close.svg" alt="">
    </a>
    <div class="fd-modal-header">

    </div>
    <div class="fd-modal-content">
        <h2 class="title text-uppercase">Faça um teste grátis</h2>
        <form class="" action="" method="post">
            <div class="group">
                <input type="text" name="email_modal" id="email_modal" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" maxlength="50" required>
                <label>E-mail</label>
            </div><!-- end of .group -->
            <div class="group">
                <button type="submit" class="btn btn-default pull-right">Enviar</button>
            </div>
        </form>
    </div>
    <div class="fd-modal-footer">
    </div>
</div>
<!-- END: MODAL -->


        <!-- BEGIN: CARREGAMENTO DE JAVASCRIPTS -->
        <script type="text/javascript" src="<?php echo SITE_URL;?>/assets_tabela/js/lib/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="<?php echo SITE_URL;?>/assets_tabela/js/lib/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.28/vue.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo SITE_URL;?>/assets_tabela/js/lib/mask.min.js"></script>
        <script type="text/javascript" src="<?php echo SITE_URL;?>/assets_tabela/js/lib/jqBootstrapValidation.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/swiffy/v7.4/runtime.js"></script>
        <script type="text/javascript" src="<?php echo SITE_URL;?>/assets_tabela/js/lib/owl.carousel.min.js"></script>
        <script type="text/javascript" src="<?php echo SITE_URL;?>/assets_tabela/js/lib/swiffy.js"></script>
        <script type="text/javascript" src="https://code.createjs.com/createjs-2015.11.26.min.js"></script>
        <script type="text/javascript" src="<?php echo SITE_URL;?>/assets_tabela/js/lib/header.animate.js"></script>
        <script type="text/javascript" src="<?php echo SITE_URL;?>/assets_tabela/js/lib/jquery.match-height.js"></script>
        <script type="text/javascript" src="<?php echo SITE_URL;?>/assets_tabela/js/main.js"></script>
        <!-- END: CARREGAMENTO DE JAVASCRPTS -->
    </body>
</html>

