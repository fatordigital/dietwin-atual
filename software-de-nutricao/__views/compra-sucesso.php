<?php include 'includes/cabecalho.php'; ?>

<body class="comprar  comprar-<?php echo $classe ?> dados pagamento">

<?php include 'includes/topo.php'; ?>

     <div class="titulo-principal">
        <p><span>comprar o</span><br />dietWin</p>        
    </div>
    <div class="titulo-resumo">
        <p>	A praticidade de comprar online, com toda a comodidade e segurança que você precisa. Aproveite nossas promoções e adquira hoje mesmo a sua versão do dietWin.</p>
    </div>
    </div>
</div>

<div class="conteudo">
<div class="container">

<div class="retorno">

    <?php
    $total = $compra_produto->valor+($compra_produto->licenca_valor*$compra_produto->licenca_quantidade)+$compra->entrega_valor-((isset($compra_cupom) AND $compra_cupom)?$compra_cupom->valor:0);
    ?>

    <h1>Você realizou uma compra na <span>dietWin</span>. Abaixo estão algumas informações sobre ela:</h1>
    
    <h2>Código da compra: <span class="codigo"><?php echo $compra->codigo; ?></span></h2>
    
    <h2>Forma de pagamento: <span class="codigo"><?php echo $compra->pagamento_metodo; ?></span></h2>

    <?php if (isset($pagseguro))
    { ?>
        <h2>Código da transação do PagSeguro: <span class="codigo">Dia <?php echo $pagseguro; ?></span></h2>
    <?php }
    else
    { ?>
    <h2>Nº de parcelas: <span class="codigo"><?php echo $compra->boleto_parcelas; ?></span></h2>
    <h2>Data de vencimento dos boletos: <span class="codigo">Dia <?php echo str_pad($compra->boleto_vencimento,2,'0',STR_PAD_LEFT); ?></span></h2>
    <?php } ?>

    <?php if ($compra_produto->produto_id != 4) { ?><h2>Forma de entrega: <span class="codigo"><?php echo $compra->entrega_forma; ?></span></h2><?php } ?>

    <?php if ($compra_produto->produto_id != 4) { ?><h2>Valor do frete: <span class="codigo">R$ <?php echo number_format($compra->entrega_valor, 2, ',', '.'); ?></span></h2><?php } ?>
    
    <h2>Produto: <span class="codigo"><?php echo $produto->nome; ?></span></h2>
    
    <h2>Valor do produto: <span class="codigo">R$ <?php echo number_format($produto->valor, 2, ',', '.'); ?></span></h2>
    
    <h2>Nº de licenças adicionais: <span class="codigo"><?php echo $compra_produto->licenca_quantidade; ?></span></h2>

    <?php echo (($compra_produto->licenca_quantidade>0)?'<h2>Valor da licença adicional: <span class="codigo">'.number_format($produto->licenca_valor, 2, ',', '.').'</span></h2>':''); ?>

    <h2>Cupom: <span class="codigo"><?php echo ((isset($compra_cupom) AND $compra_cupom AND $compra_cupom->id > 0)?'R$ '.number_format($compra_cupom->valor,2,',','.'):'Não'); ?></span></h2>

    <?php if ($compra_produto->produto_id != 4) { ?><h2>Endereço de entrega: <span class="codigo"><?php echo $compra_endereco->endereco.', '.$compra_endereco->numero.(isset($compra_endereco->complemento)?'/'.$compra_endereco->complemento:'').' - '.$compra_endereco->bairro.' - '.$compra_endereco->cep.' - '.$cidade->nome.' - '.$estado->sigla; ?></span></h2><?php } ?>
    
    <h2>Total da Compra: <span class="codigo">R$ <?php echo number_format($total, 2, ',', '.'); ?></span></h2>

    <?php if(!isset($pagseguro)) { ?>

    <h2 style="margin-top: 30px !important;"><span class="codigo" style="background: #E2DCD8 !important; padding: 25px !important;">Enviaremos o boleto bancário através de seu e-mail cadastrado</span></h2>
    <?php } ?>

    <?php if($compra_produto->produto_id == 4) { ?>

    <h2 style="margin-top: 30px !important;"><span class="codigo" style="background: #E2DCD8 !important; padding: 25px !important;">O período de 1 ano do plano de assistência passará a contar a partir da confirmação do pagamento</span></h2>
    <?php } ?>

	<!-- tracking code do adwords -->
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/989425335/?value=<?php echo $valor_total_adwords ?>&amp;label=7P-wCNHE3wUQt93l1wM&amp;guid=ON&amp;script=0"/>
    
    <script type="text/javascript">
	// tracking code de evento do Analytics
    _gaq.push(['_trackEvent', 'compra', 'retorno', 'finalizado', <?php echo $valor_total_adwords ?>]);
    </script>
	<!-- desconsiderar antigo adwords -->
    <?php //<img height="1" width="1" style="border-style:none !important;" alt="" src="http://www.googleadservices.com/pagead/conversion/1059574202/?value=0&amp;label=AoglCP6irAIQuqOf-QM&amp;guid=ON&amp;script=0" /> ?>
</div>

<div class="clear"></div>
</div>
<div class="inferior"></div>
</div>

<?php include 'includes/rodape.php'; ?>