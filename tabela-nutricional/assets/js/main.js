new Vue({
    el: "body",
    data: {
        ua: /*@cc_on!@*/false || !!document.documentMode,
        opened: false,
        overlay: $('.fd-modal-overlay'),
        fd_modal: $('.fd-modal')
    },
    watch: {
        'opened': function (val, old) {
            if (val == true) {
                this.overlay.fadeIn(300).addClass('show');
                this.fd_modal.fadeIn(300).addClass('open');
            } else {
                this.overlay.fadeOut(300).removeClass('show');
                this.fd_modal.fadeOut(300).removeClass('open');
            }
        },
    }
});


$(window).resize( function() {
    var btn = $('.software-info .btn.btn-default');

    if($(window).width() < 768){
        $(btn).html('FAÇA UM TESTE GRÁTIS SISTEMA<br />DE ROTULAGEM NUTRICIONAL ONLINE');
    }else{
        $(btn).html('FAÇA UM TESTE GRÁTIS SISTEMA DE ROTULAGEM NUTRICIONAL ONLINE');
    }

});

$(document).ready(function(){

    var nav = $('.navbar.navbar-default');
    //var e = new swiffy.Stage(document.getElementById("swiffycontainer"),swiffyobject,{});

    var options = {
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    }


    //e.setBackground(null);
    //e.start();

    nav.affix({
        offset: {
            top: 20,
        }
    });

    $('.same-height').matchHeight(options);

    $('input, textarea').blur(function() {
        if ($(this).val()){
            $(this).addClass('used');
        }
        else{
            $(this).removeClass('used');
        }
    });





    $('input').blur(function() {
        if ($(this).val()){
            $(this).addClass('used');
        }
        else{
            $(this).removeClass('used');
        }
    });


    $('input[name="telefone"]')
        .mask("(99) 9999-9999?9")
        .focusout(function (event) {
            var target, phone, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            phone = target.value.replace(/\D/g, '');
            element = $(target);
            element.unmask();
            if(phone.length > 10) {
                element.mask("(99) 99999-999?9");
            } else {
                element.mask("(99) 9999-9999?9");
            }
        });


        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });

        $('.owl-carousel-1').owlCarousel({
            center: true,
            items: 1,
            loop: true,
            margin: 10,
            nav: true,
            dots: false,
            navText: ['<img src="assets/images/carousel-arrow.png" alt="<<">','<img src="assets/images/carousel-arrow.png" alt=">>">'],

            responsive: {
                0: {
                    items:1
                }
            },
        });



    $("input,textarea").jqBootstrapValidation({
        preventSubmit:!0,
        submitError: function(){},
        submitSuccess: function(e,a){
            a.preventDefault();
            var s = $("input#nome").val(),
                t = $("input#email").val(),
                i = $("textarea#mensagem").val(),
                r = s ;

            $("#enviar").html('<i class="fa fa-spinner fa-2x fa-spin" aria-hidden="true"></i>').attr("disabled","disabled").css({"letter-spacing":"0"});

            $.ajax({
                type:"POST",
                url:"send.php",
                dataType:"json",
                encode:!0,
                data:{
                    nome:s,
                    email:t,
                    mensagem:i
                },
                cache:!1,

                success:function(){
                    $("#success").html("<div class='aviso-envio'>"),
                    $("#success").append("<p class='thin success'>Obrigado <b>Sr(a) "+r+"</b><br>Sua mensagem foi enviada com sucesso.<br>Entraremos em contato o mais breve possível.</p>"),
                    $("#success").append("</div>"),
                    $("#form").trigger("reset"),
                    $("#nome").hide(),
                    $("#email").hide(),
                    $("#mensagem").hide(),
                    $("#enviar").hide();

                    //ga("send","pageview","/goal/contato");
                    ga("send", "pageview", "/goal/duvidas/agendamento_apresentacao");
                },

                error:function(error){
                    console.log(error);
                    $(".form-control").addClass("error"),
                    $("#enviar-mensagem").html("Enviar").removeAttr("disabled","disabled"),
                    $("#success").html("<div class='alert alert-danger'>"),
                    $("#success > .alert-danger").html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>"),
                    $("#success > .alert-danger").append("Desculpe <strong>"+r+"</strong>, n&atilde;o foi poss&iacute;­vel enviar sua mensagem."),
                    $("#success > .alert-danger").append("</div>"),
                    $("#form").trigger("reset")
                }
            })
        },

        filter:function(){
            return $(this).is(":visible")
        }
    });
});

//
// $('section').first().addClass('active');
//
//
// $(document).on('mousewheel DOMMouseScroll', function (e) {
//     e.preventDefault();
//     var active = $('section.active');
//     var delta = e.originalEvent.detail < 0 || e.originalEvent.wheelDelta > 0 ? 1 : -1;
//
//     if (delta < 0) {
//         next = active.next();
//         if (next.length) {
//             var timer = setTimeout(function () {
//                 $('body, html').animate({
//                     scrollTop: next.offset().top
//                 }, 300);
//
//                     next.addClass('active')
//                         .siblings().removeClass('active');
//
//
//
//                 clearTimeout(timer);
//             }, 600);
//         }
//     } else {
//         prev = active.prev();
//         if (prev.length) {
//             var timer = setTimeout(function () {
//                 $('body, html').animate({
//                     scrollTop: prev.offset().top
//                 }, 300);
//                 prev.addClass('active')
//                     .siblings().removeClass('active');
//
//                 clearTimeout(timer);
//             }, 600);
//         }
//     }
// });


$(document).ready( function () {
    var delay = false;

    if($(window).width() > 992){
        $(document).on('mousewheel DOMMouseScroll', function(event) {
            event.preventDefault();
            if(delay) return;

            delay = true;
            setTimeout(function(){delay = false},200)

            var wd = event.originalEvent.wheelDelta || -event.originalEvent.detail;

            var a = document.getElementsByClassName('aaa');
            // var a = document.querySelectorAll('a');
            if(wd < 0) {
                for(var i = 0 ; i < a.length ; i++) {
                    var t = a[i].getClientRects()[0];
                    if(t && t.top >= 40) break;
                }
            }
            else {
                for(var i = a.length-1 ; i >= 0 ; i--) {
                    var t = a[i].getClientRects()[0];
                    if(t && t.top < -20) break;
                }
            }

            if(i >= 0 && i < a.length) {
                $('html,body').animate({
                    scrollTop: a[i].offsetTop
                });
            }
        });
    }
});




//
// var canvas, stage, exportRoot;
// function init() {
// 	canvas = document.getElementById("canvas");
// 	images = images||{};
// 	ss = ss||{};
// 	var loader = new createjs.LoadQueue(false);
// 	loader.addEventListener("fileload", handleFileLoad);
// 	loader.addEventListener("complete", handleComplete);
// 	loader.loadManifest(lib.properties.manifest);
// }
// function handleFileLoad(evt) {
// 	if (evt.item.type == "image") { images[evt.item.id] = evt.result; }
// }
// function handleComplete(evt) {
// 	//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
// 	var queue = evt.target;
// 	var ssMetadata = lib.ssMetadata;
// 	for(i=0; i<ssMetadata.length; i++) {
// 		ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
// 	}
// 	exportRoot = new lib.Versão3_Animação();
// 	stage = new createjs.Stage(canvas);
// 	stage.addChild(exportRoot);
// 	//Registers the "tick" event listener.
// 	createjs.Ticker.setFPS(lib.properties.fps);
// 	createjs.Ticker.addEventListener("tick", stage);
// 	//Code to support hidpi screens and responsive scaling.
// 	(function(isResp, respDim, isScale, scaleType) {
// 		var lastW, lastH, lastS=1;
// 		window.addEventListener('resize', resizeCanvas);
// 		resizeCanvas();
// 		function resizeCanvas() {
// 			var w = lib.properties.width, h = lib.properties.height;
// 			var iw = window.innerWidth, ih=window.innerHeight;
// 			var pRatio = window.devicePixelRatio, xRatio=iw/w, yRatio=ih/h, sRatio=1;
// 			if(isResp) {
// 				if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
// 					sRatio = lastS;
// 				}
// 				else if(!isScale) {
// 					if(iw<w || ih<h)
// 						sRatio = Math.min(xRatio, yRatio);
// 				}
// 				else if(scaleType==1) {
// 					sRatio = Math.min(xRatio, yRatio);
// 				}
// 				else if(scaleType==2) {
// 					sRatio = Math.max(xRatio, yRatio);
// 				}
// 			}
// 			canvas.width = w*pRatio*sRatio;
// 			canvas.height = h*pRatio*sRatio;
// 			canvas.style.width = w*sRatio+'px';
// 			canvas.style.height = h*sRatio+'px';
// 			stage.scaleX = pRatio*sRatio;
// 			stage.scaleY = pRatio*sRatio;
// 			lastW = iw; lastH = ih; lastS = sRatio;
// 		}
// 	})(false,'both',false,1);
// }
