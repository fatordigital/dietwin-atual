$(document).ready(function(){

	$(window).scroll(function(){
        var st = $(this).scrollTop();
        $('header h1').css({'margin-top': 50+(st/2)+"px", 'margin-bottom': 90-(st/3)+"px", 'opacity': 1-(st/600)});
        $('.botoes-chamada').css({'margin-top': +(st/7)+"px", 'opacity': 1-(st/600)});
    });

    var navpos = $('section.nav').offset();
    console.log(navpos.top);
    $(window).bind('scroll', function() {
      if ($(window).scrollTop() > navpos.top) {
        $('section.nav').addClass('fixed');
       }
       else {
         $('section.nav').removeClass('fixed');
       }
    });

    $('.icone-menu').click(function(){
        $('.menu-mobile').slideToggle(300);
    });

    $('.menu-mobile a').click(function(){
        $('.menu-mobile').slideToggle(300);
    });

    $('a.exibir-telefone').click(function(){
        var efeito = 'slide';
        var opcao = { direction: 'right' };
        var duracao = 300;
        $('.telefone').toggle(efeito, opcao, duracao);
    });

    $('table td .conteudo-feature span.titulo-feature').click(function(){
        $(this).siblings('div.detalhe-feature').slideToggle();
    });

    $('#controla-tabs').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 120,
        itemMargin: 5,
        asNavFor: '#tabs'
    });

    $('#tabs').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#controla-tabs"
    });

    $('.flex-direction-nav a').empty();

    /*$('#tabs').flexslider({
        animation: "slide",
        controlNav: "thumbnails",
        slideshow: false
    });

    $('ol li img').each(function(){
        var t = $(this);
        var src1 = t.attr('src');
        var newSrc = src1.substring(0, src1.lastIndexOf('.'));;

        t.hover(function(){
            $(this).attr('src', newSrc+ '-hover.' + /[^.]+$/.exec(src1));
        }, function(){
            $(this).attr('src', newSrc + '.' + /[^.]+$/.exec(src1));
        });
    });*/

    // Select Cidades






	$("input#telefone").mask('(00) 0000-0000');
    $('input#cpf_cnpj').mask('00000000000000')

    $('form#orcamento').validate({
         invalidHandler: function(e, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $(".erro-form").html('Ops, você esqueceu de preencher um dos campos. Por favor, complete as informações e clique novamente em “Fazer o download”.').fadeIn(500);
            } else {
                $(".erro-form").fadeOut(500);
            }
        },
        submitHandler: function(form) {
            //var campos = $('form#orcamento').serialize();
            var campos = $(form).serialize();
            $.ajax({
                type: 'POST',
                url: SITE_URL+'/email/orcamento',
                data: campos,
                success: function(response){
                    $(form).clearForm();
                    $('.erro-form').fadeOut(500);
                    $('.ok-form').fadeIn(500).html('Orçamento enviado com sucesso').delay(8000).fadeOut(500);
                }
            });
            return false;
        }
    });

	$("form#duvidas").validate({
        rules:{
            nome:{
                required: true
            },
            email:{
                required: true, email: true
            },
            telefone: {
                required: true
            },
            cidade: {
            	required: true
            }
        },
        invalidHandler: function(e, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $(".erro-form").html('Ops, você esqueceu de preencher um dos campos. Por favor, complete as informações e clique novamente em “Enviar Orçamento”.').fadeIn(500);
            } else {
                $(".erro-form").fadeOut(500);
            }
        },
        submitHandler: function(form) {
			$('form#duvidas input[type=submit]').val('Enviando...').attr('disabled','disabled');
            var campos = $('form#duvidas').serialize();
            $.ajax(
            {
                type:'POST',
                url:SITE_URL+'/email/ajax_email_download_trial_link',
                data:campos,
                success:function(response)
                {
                    $('form#duvidas').slideUp(500);
                    $('form#duvidas').html('<h2><strong>Sucesso!</strong></h2><h2>O link para download foi enviado para seu e-mail.</h2>').stop(500);
                    $('form#duvidas').fadeIn();

                    // Goal - GTM
                    $(document).ready(function(){
                        dataLayer.push({'event': 'VirtualPageview','sendPageview':'/goal/teste-gratis'});
                    });
                }
            }
            );
            return false;
        }
    });

    $("form#cadastro").validate({
        // Desabilitamos a anchoragem no form quando clicar em submit
        focusInvalid: false,
        rules:{
            cep:{required:true, minlength: 9}
        },

        /*rules:{
            nome:{required: true},
            email:{required: true, email: true},
            senha:{required: true},
            senha_confimar:{required: true, equalTo:"#senha"},
            profissao:{required: true},
            tipo_pessoa:{required: true},
            rg:{required: true},
            cpf:{required: true},
            endereco:{required: true},
            bairro:{required: true},
            numero:{required: true},
            cep:{required: true},
            estado:{required: true},
            cidade:{required: true},
            telefone:{required: true},
            celular:{required: true}
        },
        messages:{
            nome:{required:"- Digite seu nome."},
            email:{required:"- Digite seu e-mail", email:"- Digite um e-mail correto"},
            senha:{required:"- Digite sua senha"},
            senha_confimar:{equalTo:"- As senhas não conferem."},
            profissao:{required:"- Digite sua profissão"},
            tipo_pessoa:{required:"- Escolha o tipo de pessoa, física ou jurídica"},
            rg:{required:"- Digite seu RG"},
            cpf:{required:"- Digite seu CPF"},
            endereco:{required:"- Digite seu Endereço"},
            bairro:{required:"- Digite seu Bairro"},
            numero:{required:"- Digite o número de seu endereço"},
            cep:{required:"- Digite seu CEP"},
            estado:{required:"- Escolha seu Estado"},
            cidade:{required:"- Escolha sua Cidade"},
            telefone:{required:"- Digite seu telefone"},
            celular:{required:"- Digite seu celular"}
        },*/
        errorPlacement: function(error, element) {
            error.appendTo('.erro-form');
            $('.erro-form').fadeIn(500);
        }
        /*
        invalidHandler: function(e, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $(".erro-form").html().fadeIn(500);
            } else {
                $(".erro-form").fadeOut(500);
            }
        }
        */
    });

    $('form input[name="cep"]').mask('00000-000');
    $('form input[name="celular"]').mask('(00)0000-0000');
    $('form input[name="telefone"]').mask('(00)0000-0000');
    $('form input[name="cpf"]').mask('000.000.000-00');
    //$('form input[name="rg"]').mask('000.000.00-00');
    $('form input[name="rg"]').mask('999999999999999');

    $('form [name="tipo_pessoa"]').on('change', function(e){
        e.preventDefault()
        var id = $(this).attr('id');
        console.log(id);
        if(id == 'pessoa-fisica'){
            $('form label[for="rg"]:not(.error)').text('RG:');
            $('form label[for="rg"].error').text('-Digite seu RG');
            $('form input[name="rg"]').val('').mask('999999999999999').removeAttrs('^data-').attr('data-rule-required','true').attr('data-msg-required','- Digite seu RG');
            $('form label[for="cpf"]:not(.error)').text('CPF:');
            $('form label[for="cpf"].error').text('-Digite o seu CPF');
            $('form input[name="cpf"]').val('').mask('000.000.000-00').removeAttrs('^data-').attr('data-rule-required','true').attr('data-msg-required','- Digite seu CPF');
        }else{
            $('form label[for="rg"]:not(.error)').text('Razão Social:');
            $('form label[for="rg"].error').text('-Digite sua Razão Social');
            $('form input[name="rg"]').val('').unmask().removeAttrs('^data-').attr('data-rule-required','true').attr('data-msg-required','- Digite sua Razão Social');
            $('form label[for="cpf"]:not(.error)').text('CNPJ:');
            $('form label[for="cpf"].error').text('-Digite seu CNPJ');
            $('form input[name="cpf"]').val('').mask('00.000.000/0000-00').removeAttrs('^data-').attr('data-rule-required','true').attr('data-msg-required','- Digite seu CNPJ');
        }
    });

	$("a.leia-mais").click(function(){
		$("section.quem-faz").slideToggle();
	});


    // 1025440395

    //Smooth Scroll
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
    });

    $('.boleto').on('click', function(e){
        e.preventDefault();
        $(this).animate({'opacity':'1'}, 500);
        $('.opcoes').fadeIn(500);
        $('.forma.boleto').animate({'margin':'0 !important'}, 500)
        $('.pagseguro').animate({'opacity':'0.5'}, 500);
        //$('.processo-compra-submit').css({'opacity':'1'}).removeAttr('disabled');
        $('#metodo_pagamento').val('boleto');
        if($('input[name="check-frete"]').is(':checked')){
            console.log('liberado boleto');
            $('.processo-compra-submit').css({'opacity':'1'}).removeAttr('disabled');
        }
    });

    $('.pagseguro').on('click', function(e){
        e.preventDefault();
        $(this).animate({'opacity':'1'}, 500);
        $('.boleto').animate({'opacity':'0.5'}, 500);
        $('.forma.boleto').animate({'margin':'20px 0 0 !important'}, 500)
        $('.opcoes').fadeOut(500);
        //$('.processo-compra-submit').css({'opacity':'1'}).removeAttr('disabled');
        $('#metodo_pagamento').val('pagseguro');
        if($('input[name="check-frete"]').is(':checked')){
            console.log('liberado pagseguro');
            $('.processo-compra-submit').css({'opacity':'1'}).removeAttr('disabled');
        }
    });

    $('.finalizar-compra').on('click', function(e){

            if($('input[name="check-frete"]').is(':checked')){
                if($('input[name="metodo_pagamento"]').val() != ''){

                }else{
                    alert('Selecione o método de pagamento.');
                    return false;
                }
            }else{
                alert('Selecione o frete.');
                return false;
            }


    });


    $('input[name="check-frete"]').on('change', function(e){
        e.preventDefault();
        var valor_produto = $('.valor-produto').data('valor-produto'),
            servico = $(this).val(),
            valor_frete = $(this).data('valor'),
            valor_total = 0,
            valor_cupom = $('.valor-cupom').data('valor-cupom');

        valor_produto = parseFloat(valor_produto);
        valor_frete = parseFloat(valor_frete);
        valor_cupom = parseFloat(valor_cupom);
        //console.log(valor_cupom);

        valor_total = valor_produto += valor_frete;
        if(!isNaN(valor_cupom)){
            valor_total = valor_total-valor_cupom;
        }
        valor_total = valor_total.toFixed(2);

        $('input[name="frete"]').val(valor_frete);
        $('input[name="servico"]').val(servico);


        if($('input[name="metodo_pagamento"]').val() != ''){
            $('.processo-compra-submit').css({'opacity':'1'}).removeAttr('disabled');
        }

        $('.mostra-frete').fadeIn();
        $('.mostra-frete.valor').text('R$ '+ConvertToReal(valor_frete.toFixed(2)));

        $('.reais').text(ConvertToReal(valor_total));
    });

    /*setInterval(function(){
        $.get(SITE_URL+'/abandonado/ajax', {}, function(){
            console.log('consultando...');
        });
    }, 3000);*/
});

function ConvertToReal(number) {
    var number = number.toString(),
    dollars = number.split('.')[0],
    cents = (number.split('.')[1] || '') +'00';
    dollars = dollars.split('').reverse().join('')
        .replace(/(\d{3}(?!$))/g, '$1,')
        .split('').reverse().join('');
    return dollars + ',' + cents.slice(0, 2);
}

function setCookie(cname, cvalue, exhours) {
    var d = new Date();
    d.setTime(d.getTime() + (exhours*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

function closeFloating(){
    $(document).ready(function(){
        $('.floating_bg').fadeOut(400);
        $('.floating').fadeOut(400);
    });
}

function showFloating(){
    $(document).ready(function(){
        $('.floating_bg').fadeIn(150);
        $('.floating').fadeIn(150);
    });
}

// Floating Banner
$(document).ready(function(){
    var altura = $('.floating').height();
    var largura = $('.floating').width();
    $('.floating').css('margin','-'+altura/2+'px 0 0 -'+largura/2+'px');

    if(!getCookie('floating_natal')){
        showFloating();
        setCookie('floating_natal', 'true', 1);
    }
});
