<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Toolbar extends Controller_Padrao
{

	/**
	 * Envia email para amigo com link de conteúdo visitado
	 * @param $parametros
	 * @return void
	 */
	public function enviar_amigo($parametros)
	{
		$nome_amigo = isset($parametros->nome_amigo) ? trim($parametros->nome_amigo) : NULL;
		$email_amigo = isset($parametros->email_amigo) ? trim($parametros->email_amigo) : NULL;
		$seu_nome = isset($parametros->seu_nome) ? trim($parametros->seu_nome) : NULL;
		$seu_email = isset($parametros->seu_email) ? trim($parametros->seu_email) : NULL;
		$amigo_mensagem = isset($parametros->amigo_mensagem) ? trim($parametros->amigo_mensagem) : NULL;
		$url = isset($parametros->url) ? trim($parametros->url) : NULL;
		$titulo = isset($parametros->titulo) ? trim($parametros->titulo) : NULL;

		// amigo_mensagem não é obrigatório

		if ( ! empty($nome_amigo) AND ! empty($email_amigo) AND ! empty($seu_nome) AND ! empty($seu_email))
		{
			$mail = new Controller_Email;

			if ($mail->enviar_amigo($parametros))
			{
				echo json_encode(array('tipo'=>'sucesso', 'mensagem'=>'O conteúdo foi enviado para o seu amigo com sucesso!'));
			}
			else
			{
				echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Ocorreu um erro ao enviar sua mensagem. Por favor, tente novamente mais tarde.'));
			}
		}
		else
		{
			echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Ocorreu um erro ao receber suas informações. Por favor, tente novamente mais tarde.'));
		}
    }


} // end class