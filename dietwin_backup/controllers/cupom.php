<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Cupom extends Controller_Padrao
{
  /**
   * Chama o construtor da classe pai
   */
  public function __construct()
  {
    parent::__construct();
  }


  public function index($parametros)
  {
    $codigo = $parametros->codigo;
    $parametros->produto_seo = $_SESSION['produto_temp'];

    $produto = new Model_Produto();
    $produto = $produto->select("SELECT * FROM {tabela_nome} WHERE nome_seo='".$parametros->produto_seo."'");

    if ($produto)
    {
      //echo $parametros->codigo; exit;
      $cupom = new Model_Cupom();
      $cupom = $cupom->select("SELECT * FROM {tabela_nome}
                          WHERE codigo='".$codigo."' AND produto_id=".$produto->id."
                          AND ativo=1 AND validade_inicio <= CURRENT_DATE AND validade_fim >= CURRENT_DATE");
      if ($cupom)
      {
        $_SESSION['cupom_id'] = $cupom->id;
        echo str_replace('.00', '',$cupom->valor);
        exit;
      }
    }
    echo 0;
  }

}