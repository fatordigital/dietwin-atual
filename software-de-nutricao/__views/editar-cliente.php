<?php include 'includes/cabecalho.php'; ?>

<body class="area-cliente softwares cadastro">

<?php include 'includes/topo.php'; ?>

<div class="titulo-principal">
    <p><span>Bem vindo à </span><br />Área do Cliente</p>
</div>
<div class="titulo-resumo">
    <p>Essa área é restrita e de uso exclusivo de usuários cadastrados ou clientes dietWin. Aqui você encontrará informações e arquivos importantes para a utilização do dietWin.</p>
</div>
</div>
</div>

<div class="conteudo">
    <div class="container">
        <?php include 'includes/toolbar.php' ?>
        <br />

        <div class="box">
            <h2>Editar meus dados de cadastro</h2>
            <p>Mantenha seus dados cadastrais sempre atualizados! Através dessas informações é que a equipe do dietWin pode entrar em contato com você ou enviar suas compras!</p>
            <div class="formulario">

            <p>Utilize o formulário abaixo para cadastrar-se no site da dietwin.</p>

            <?php if (isset($notificacao)) { $notificacao->site_exibir(); } ?>

            <form method="post" action="<?php echo SITE_URL ?>/area-do-cliente/atualizar-dados" id="form-cadastro" >
                <label><b>Nome:</b></label>
                <input type="text" class="obrigatorio" name="nome" id="nome" maxlength="48" value="<?php echo isset($cliente->nome)?$cliente->nome:'' ?>" />
                <div class="clear"></div>

                <div>
                    <label><b>E-mail:</b></label>
                    <input type="text" class="obrigatorio" name="email" id="email" maxlength="255" value="<?php echo isset($cliente->email)?$cliente->email:'' ?>" />
                    <div class="clear"></div>

                </div>

                <label><b>Senha:</b></label>
                <input type="password" class="<?php echo ( ! isset($cliente) OR is_null($cliente->id)) ? 'obrigatorio' : '' ?>" name="senha" id="senha" maxlength="100" value="" />
                <div class="clear"></div>

                <label><b>Profissão:</b></label>
                <input type="text" class="obrigatorio" name="profissao" id="profissao" maxlength="100" value="<?php echo isset($cliente->profissao)?$cliente->profissao:'' ?>" />
                <div class="clear"></div>

               <?php /* <p><b>Escolha uma das opções abaixo</b></p>
                <div class="usuario">
                    <input type="radio" id="pessoa_fisica" name="usuario" value="pessoa_fisica" <?php echo !isset($cliente->cnpj)?' checked="checked"':'' ?>/><span>Pessoa Física</span>
                    <input type="radio" id="pessoa_juridica" name="usuario" value="pessoa_juridica"<?php echo isset($cliente->cnpj)?' checked="checked"':'' ?> /><span>Pessoa Jurídica</span>
                    <div class="clear"></div>
                </div>      */?>
                <input type="hidden" id="pessoa_fisica" name="usuario" value="<?php echo !isset($cliente->cnpj)?'pessoa_fisica':'pessoa_juridica' ?>" />

                <div class="clear"></div>
                <?php if (!isset($cliente->cnpj)) { ?>
                <div class="pessoa-fisica"<?php echo !isset($cliente->cnpj)?'style="display:block"':'' ?>>
                    <label><b>RG:</b></label>
                    <input type="text" class="obrigatorio" name="rg" id="rg" maxlength="10" value="<?php echo isset($cliente->rg)?$cliente->rg:'' ?>" />
                    <div class="clear"></div>

                    <label><b>CPF:</b></label>
                    <input type="text" class="obrigatorio" name="cpf" id="cpf" maxlength="14" value="<?php echo isset($cliente->cpf)?$cliente->cpf:'' ?>" />
                    <div class="clear"></div>
                </div>
                <?php } ?>

                <?php if (isset($cliente->cnpj)) { ?>
                <div class="pessoa-juridica"<?php echo isset($cliente->cnpj)?'style="display:block"':'' ?>>
                    <label class="ajustar"><b>Nome da Empresa:</b></label>
                    <input type="text" class="obrigatorio" name="nome_empresa" id="nome_empresa" value="<?php echo isset($cliente->nome_empresa)?$cliente->nome_empresa:'' ?>" />
                    <div class="clear"></div>

                    <label><b>CNPJ:</b></label>
                    <input type="text" class="obrigatorio" name="cnpj" id="cnpj" maxlength="18" value="<?php echo isset($cliente->cnpj)?$cliente->cnpj:'' ?>" />
                    <div class="clear"></div>

                    <label class="ajustar"><b>Inscrição estadual:</b></label>
                    <input type="text" class="obrigatorio" name="inscricao_estadual" id="inscricao_estadual" maxlength="18" value="<?php echo isset($cliente->inscricao_estadual)?$cliente->inscricao_estadual:'' ?>" />
                    <div class="clear"></div>

                    <label class="ajustar"><b>Responsável na empresa:</b></label>
                    <input type="text" class="obrigatorio" name="responsavel_nome" id="nome_responsavel" maxlength="100" value="<?php echo isset($cliente->cnpj)?$cliente->responsavel_nome:'' ?>" />
                    <div class="clear"></div>

                    <label class="ajustar"><b>RG do responsável:</b></label>
                    <input type="text" class="obrigatorio" name="rg_responsavel" id="rg_responsavel" maxlength="10" value="<?php echo isset($cliente->cnpj)?$cliente->rg:'' ?>" />
                    <div class="clear"></div>

                    <label class="ajustar"><b>CPF do responsável:</b></label>
                    <input type="text" class="obrigatorio" name="cpf_responsavel" id="cpf_responsavel" maxlength="14" value="<?php echo isset($cliente->cnpj)?$cliente->cpf:'' ?>" />
                    <div class="clear"></div>
                </div>
                <?php } ?>

                <label><b>Endereço:</b></label>
                <input type="text" class="obrigatorio" name="endereco" id="endereco" maxlength="100" value="<?php echo isset($endereco_principal->endereco)?$endereco_principal->endereco:'' ?>" />
                <div class="clear"></div>

                <label><b>Número:</b></label>
                <input type="text" class="numero" name="numero" id="numero" maxlength="7" value="<?php echo isset($endereco_principal->numero)?$endereco_principal->numero:'' ?>" />

                <label class="complemento-label"><b>Complemento:</b></label>
                <input type="text" name="complemento" class="complemento" id="complemento" maxlength="100" value="<?php echo isset($endereco_principal->complemento)?$endereco_principal->complemento:'' ?>" />

                <label class="cep-label"><b>Cep:</b></label>
                <input type="text" class="obrigatorio cep" name="cep" id="cep" maxlength="9" value="<?php echo isset($endereco_principal->cep)?$endereco_principal->cep:'' ?>" />
                <div class="clear"></div>

                <label><b>Bairro:</b></label>
                <input type="text" class="obrigatorio bairro" name="bairro" id="bairro" maxlength="100" value="<?php echo isset($endereco_principal->bairro)?$endereco_principal->bairro:'' ?>" />

                <label class="estado-label"><b>Estado:</b></label>
                <select class="estado obrigatorio" name="estado" id="estado">
                    <?php
                    if ($estados AND count($estados)>0)
                    {
                        foreach ($estados as $estado)
                        { ?>
                            <option value="<?php echo $estado->id ?>" <?php echo (isset($endereco_principal->estado_id) AND $endereco_principal->estado_id == $estado->id)?' selected="selected"':'' ?>><?php echo $estado->nome ?></option>
                            <?php }
                    }
                    ?>
                </select>

                <label class="cidade-label"><b>Cidade:</b></label>
                <select class="obrigatorio cidade" name="cidade" id="cidade">
                    <?php
                    if (isset($cidades_principal) AND $cidades_principal AND count($cidades_principal) > 0)
                    {
                        foreach ($cidades_principal as $cidade)
                        { ?>
                            <option value="<?php echo $cidade->id ?>"<?php echo (isset($endereco_principal->cidade_id) AND $cidade->id == $endereco_principal->cidade_id)?' selected="selected"':'' ?>><?php echo $cidade->nome ?></option>
                            <?php }
                    }
                    ?>
                </select>

                <label><b>Observação:</b></label>
                <textarea type="text" class="obrigatorio observacao" name="observacao" id="observacao" style="resize:none"><?php echo isset($endereco_principal->observacao)?$endereco_principal->observacao:'' ?></textarea>

                <div class="clear"></div>

                <div class="endereco-entrega">
                    <p><b>Endereço para Entrega:</b> </p>

                    <div style="margin: 0 20px 0 85px">
                        <input id="checkbox-endereco-entrega" type="checkbox" value="1" name="endereco_para_entrega" <?php echo (!isset($endereco_entrega) OR !$endereco_entrega) ? 'checked="checked"' : ''  ?>
                               style="float: none; height: auto!important; width: auto!important; margin: 0; vertical-align: baseline" />
                        <b>Desejo receber no mesmo endereço informado acima</b>
                    </div>

                    <div class="clear"></div>

                    <label><b>Endereço:</b></label>
                    <input type="text" class="endereco" name="endereco_entrega" id="endereco_entrega" maxlength="100" value="<?php echo isset($endereco_entrega->endereco) ? $endereco_entrega->endereco : '' ?>" <?php echo (!isset($endereco_entrega) OR !$endereco_entrega) ? 'disabled="disabled"' : '' ?> />
                    <div class="clear"></div>

                    <label><b>Número:</b></label>
                    <input type="text" class="numero" name="numero_entrega" id="numero_entrega" maxlength="7" value="<?php echo isset($endereco_entrega->numero)?$endereco_entrega->numero:'' ?>" <?php echo (!isset($endereco_entrega) OR !$endereco_entrega) ? 'disabled="disabled"' : '' ?> />

                    <label class="complemento-label"><b>Complemento:</b></label>
                    <input type="text" name="complemento_entrega" class="complemento" id="complemento_entrega" maxlength="100" value="<?php echo isset($endereco_entrega->complemento)?$endereco_entrega->complemento:'' ?>" <?php echo (!isset($endereco_entrega) OR !$endereco_entrega) ? 'disabled="disabled"' : '' ?> />

                    <label class="cep-label"><b>Cep:</b></label>
                    <input type="text" class="cep" name="cep_entrega" id="cep_entrega" maxlength="9" value="<?php echo isset($endereco_entrega->cep)?$endereco_entrega->cep:'' ?>" <?php echo (!isset($endereco_entrega) OR !$endereco_entrega) ? 'disabled="disabled"' : '' ?> />
                    <div class="clear"></div>

                    <label><b>Bairro:</b></label>
                    <input type="text" class="bairro" name="bairro_entrega" id="bairro_entrega" maxlength="100" value="<?php echo isset($endereco_entrega->bairro)?$endereco_entrega->bairro:'' ?>" <?php echo (!isset($endereco_entrega) OR !$endereco_entrega) ? 'disabled="disabled"' : '' ?> />

                    <label class="estado-label"><b>Estado:</b></label>
                    <select class="estado" name="estado_entrega" id="estado_entrega" <?php echo (!isset($endereco_entrega) OR !$endereco_entrega) ? 'disabled="disabled"' : '' ?>>
                        <?php
                        if ($estados AND count($estados)>0)
                        {
                            foreach ($estados as $estado)
                            { ?>
                                <option value="<?php echo $estado->id ?>" <?php echo (isset($endereco_entrega->estado_id) AND $endereco_entrega->estado_id == $estado->id)?' selected="selected"':'' ?>><?php echo $estado->nome ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>

                    <label class="cidade-label"><b>Cidade:</b></label>
                    <select class="cidade" name="cidade_entrega" id="cidade_entrega" <?php echo (!isset($endereco_entrega) OR !$endereco_entrega) ? 'disabled="disabled"' : '' ?> >
                        <?php
                        if (isset($cidades_entrega) AND $cidades_entrega AND count($cidades_entrega) > 0)
                        {

                            foreach ($cidades_entrega as $cidade)
                            { ?>
                                <option value="<?php echo $cidade->id ?>"<?php echo (isset($endereco_entrega->cidade_id) AND $cidade->id == $endereco_entrega->cidade_id)?' selected="selected"':'' ?>><?php echo $cidade->nome ?></option>
                                <?php }
                        }
                        ?>
                    </select>

                    <label><b>Observação:</b></label>
                    <textarea type="text" class="observacao" name="observacao_entrega" id="observacao_entrega" style="resize:none" <?php echo (!isset($endereco_entrega) OR !$endereco_entrega) ? 'disabled="disabled"' : '' ?> ><?php echo isset($endereco_entrega->observacao)?$endereco_entrega->observacao:'' ?></textarea>

                    <div class="clear"></div>
                </div>

                <label class="ajustar"><b>Telefone principal:</b></label>
                <input type="text" class="obrigatorio" name="telefone_principal" id="telefone_principal" maxlength="15" value="<?php echo isset($cliente->telefone_principal)?$cliente->telefone_principal:'' ?>" />
                <div class="clear"></div>

                <label class="ajustar"><b>Telefone extra:</b></label>
                <input type="text" class="" name="telefone_extra" id="telefone_extra" maxlength="15" value="<?php echo isset($cliente->telefone_extra)?$cliente->telefone_extra:'' ?>" />
                <div class="clear"></div>

                <button type="submit" class="<?php echo (isset($cliente) AND ! is_null($cliente->id))?'alterar':'cadastrar' ?>">Enviar</button>

                <div class="clear"></div>

            </form>

            <div id="form_notification" class=""></div>
            <div class="clear"></div>
            </div>
        </div>


        <?php include 'includes/sidebar-area-cliente.php'; ?>



        <div class="clear"></div>
    </div>
    <div class="inferior"></div>
</div>



<?php include 'includes/rodape.php'; ?>