<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php' ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

		<!-- Page Head -->
		<h2>Lista de Galerias cadastradas</h2>
		<p id="page-intro">Abaixo estão listadas todas as galerias de foto que são exibidas na área de Multimídia do site.</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<form action="<?php echo SITE_URL ?>/fatorcms/galerias/listar" method="get" id="galerias_buscar_form">
			<fieldset>
				<p>
					<label for="buscar">Digite uma ou mais palavras-chave para buscar por uma galeria cadastrada:</label>
					<input class="text-input medium-input" type="text" id="buscar" name="buscar" maxlength="100" value="<?php echo isset($buscar) ? $buscar : '' ?>" />
					<input class="button" type="submit" value="Buscar" />
				</p>
			</fieldset>
			<div class="clear"></div><!-- End .clear -->
		</form>

		<?php if (isset($buscar) AND ! is_null($buscar)) { ?>
			<p class="buscar-resultado">
				Foram encontrados <strong><?php echo $paginacao->linhas_total ?></strong> resultado(s) para a busca por "<strong><?php echo $buscar ?></strong>".
				<a title="Limpar a busca" href="<?php echo SITE_URL?>/fatorcms/galerias/listar"><img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier-cross.png" alt="Limpar busca" /> Clique aqui para limpar a busca</a>
			</p>
		<?php } ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Galerias</h3>

				<input class="galeria button botao-cadastrar" type="button" value="Cadastrar uma nova galeria de fotos" />

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<table>

					<thead>
						<tr>
							<?php echo Funcoes::montar_th_ordenacao_listagem('galerias', 'listar', $paginacao, $buscar, 'Data', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('galerias', 'listar', $paginacao, $buscar, 'Título', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('galerias', 'listar', $paginacao, $buscar, 'Fotos', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('galerias', 'listar', $paginacao, $buscar, 'Produto', $ordenar_por, $ordem) ?>
                            <th>Ações</th>
						</tr>
					</thead>

					<tfoot>
						<tr>
							<td colspan="5">

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_quantidades() ?>

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_links() ?>

								<div class="clear"></div>
							</td>
						</tr>
					</tfoot>

					<tbody>
						<?php
                        if($galerias AND count($galerias) > 0)
						{
							foreach ($galerias as $galeria)
							{
							?>
								<tr>
									<td class="<?php echo $ordenar_por=='data' ? 'current' : '' ?>"><?php echo date('d/m/Y',strtotime($galeria->data)) ?></td>
									<td class="<?php echo $ordenar_por=='titulo' ? 'current' : '' ?>">
										<a href="<?php echo SITE_URL ?>/fatorcms/galerias/editar/<?php echo $galeria->id ?>" title="Editar a galeria &quot;<?php echo $galeria->titulo ?>&quot;">
											<?php echo $galeria->titulo ?>
										</a>
									</td>
									<td class="<?php echo $ordenar_por=='fotos' ? 'current' : '' ?>"><?php echo (is_null($galeria->numero_fotos) ? 0 : $galeria->numero_fotos) ?></td>
									<td class="<?php echo $ordenar_por=='produto' ? 'current' : '' ?>"><?php echo (is_null($galeria->produto_nome) ? '<em>Tópicos diversos</em>' : $galeria->produto_nome) ?></td>
									<td>
										<!-- Icons -->
										<a href="<?php echo SITE_URL ?>/fatorcms/galerias/editar/<?php echo $galeria->id ?>" title="Editar a galeria &quot;<?php echo $galeria->titulo ?>&quot;">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" />
										</a>
										<a href="<?php echo SITE_URL ?>/fatorcms/galerias/excluir/<?php echo $galeria->id ?>" title="Excluir a galeria &quot;<?php echo $galeria->titulo ?>&quot;" class="item-confirmar">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/cross.png" alt="Excluir" />
										</a>
									</td>
								</tr>
							<?php
							}
						}
						else
						{
							echo '<tr><td colspan="5">'.((isset($buscar) AND ! is_null($buscar)) ? 'Nenhuma galeria encontrada para esta busca.' : 'Nenhuma galeria cadastrada até o momento.' ).'</td></tr>';
						}
						?>
					</tbody>

				</table>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>