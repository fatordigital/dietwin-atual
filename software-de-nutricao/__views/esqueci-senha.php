<?php include 'includes/cabecalho.php'; ?>

<body class="cadastro esqueci-senha">

<?php include 'includes/topo.php'; ?>
	
    <div class="titulo-principal">
        <p><span>Recupere sua</span><br />Senha</p>
    </div>
    <div class="titulo-resumo">
        <p>Em caso de problemas com sua senha preencha o formulário abaixo e você receberá sua nova senha automaticamente por e-mail. Esta nova senha será enviada para o e-mail que foi cadastrado!</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container">
    	
        <div class="formulario">
            <p>Digite seu e-mail cadastrada na dietWin para receber sua nova senha.</p>

            <div id="form_notification"></div>
            <div class="clear"></div>

            <form method="post" action="<?php echo SITE_URL ?>/esqueci-senha/enviar" id="form-senha" >
                
                <label><b>E-mail:</b></label>
                <input type="text" class="obrigatorio" name="email" id="email" maxlength="255" value="" />
                <div class="clear"></div>

                <button type="submit">Enviar</button>

                <div class="clear"></div>

            </form>
            <div class="clear"></div>
        </div>
        
        <div class="clear"></div>        
    </div>
    <div class="clear"></div>
    <div class="inferior"></div>  
</div>

<?php include 'includes/rodape.php'; ?>