<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jonathan.alba
 * Date: 09/05/12
 * Time: 16:14
 * To change this template use File | Settings | File Templates.
 */
class Controller_Imagem extends Controller_Padrao
{
    /**
     * Chama o construtor da classe pai
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método inicial que faz a renderização básica da página
     * @param $parametros
     * @return void
     */
    public function index($parametros)
    {
        //header("Content-type: image/jpeg");
        include 'biblioteca/WideImage/WideImage.php';
        WideImage::load('http://i3.ytimg.com/vi/UuLKhkbyrrk/0.jpg')->resize(229, 133)->crop('center','center',229,133)->output('jpg',80);
    }

}
