<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Pagamento extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        $produto_nome = $_SESSION['produto_nome'];
        $_SESSION['licencas'] = 1;
        //Se tentar acessar essa pagina sem antes ter acessado a dados-compra
        if (!isset($_SESSION['licencas']) OR !isset($_SESSION['produto']) OR !isset($_SESSION['processo_compra']) OR !isset($_SESSION['cliente_id']))
        {
            echo 'erro';
        }

        $cliente = new Model_Cliente($_SESSION['cliente_id']);
        if ($cliente->cadastro_completo == 0)
        {
            $notificacao = new Notificacao('Você precisa completar os seus dados para poder finalizar a compra','erro',TRUE);
            header('Location: '.SITE_URL.'/area-do-cliente/editar-dados'); exit;
        }

        if (isset($_SESSION['cupom_id']))
        {
            $cupom = new Model_Cupom($_SESSION['cupom_id']);

            if (!is_null($cupom->cliente_id) AND $cliente->id != $cupom->cliente_id)
            {
                unset($_SESSION['cupom_id']);
            }
        }

        //Recuperar o endereço de entrega do cliente
        $endereco = new Model_ClienteEndereco;
        $endereco = $endereco->select('SELECT * FROM {tabela_nome} WHERE entrega=1 AND cliente_id='.$_SESSION['cliente_id']);
        if ( ! $endereco OR is_null($endereco->id))
        {
	        $endereco = new Model_ClienteEndereco;
            $endereco = $endereco->select('SELECT * FROM {tabela_nome} WHERE entrega=0 AND cliente_id='.$_SESSION['cliente_id']);
        }

        //Recupera a cidade e o estado do endereco de entrega
        $cidade = new Model_Cidade($endereco->cidade_id);
        $estado = new Model_Estado($endereco->estado_id);

        $view = new View('pagamento.php');
		$this->view_variaveis_obrigatorias($view);

        $view->adicionar('classe',$classe);

        $view->adicionar('frete',$frete);

        $view->adicionar('cupom',(isset($_SESSION['cupom']))?$_SESSION['cupom']:NULL);
        $view->adicionar('produto',$_SESSION['produto']);
        $view->adicionar('licencas',$_SESSION['licencas']);

        $view->adicionar('endereco',$endereco);
        $view->adicionar('cidade',$cidade);
        $view->adicionar('estado',$estado);

		$view->adicionar('body_class', 'pagamento');
		$view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Escolha a forma de pagamento - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */


} // end class