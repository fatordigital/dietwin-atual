<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Dietwin</title>

		<!-- Estilos -->
		<link rel="stylesheet" type="text/css" href="assets/css/lib/reset.css">
		<link rel="stylesheet" type="text/css" href="assets/css/lib/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/lib/owl.carousel.css">
		<link rel="stylesheet" type="text/css" href="assets/css/style.min.css">

		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
	</head>
	<body>