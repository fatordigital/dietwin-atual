<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_EsqueciMinhaSenha extends Controller_Padrao
{

	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();

		require_once 'biblioteca/SwiftMailer/swift_required.php';
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
		// ini_set('memory_limit', '3096M');
		// $min = 0;
		// $max = 13000;
		// while($min < $max){
			// $model_cliente = new Model_Cliente;
			// $clientes = $model_cliente->select('SELECT * FROM {tabela_nome} LIMIT ' . $min .', ' . $min+100);
			// foreach ($clientes as $key => $cliente) {
				// Contamos quantos pedidos o cliente em questão tem
				// $compra_total = new Model_Compra;
				// $compra_total = $compra_total->select('SELECT COUNT(id) as compra_total FROM {tabela_nome} WHERE cliente_id = '.$cliente->id);
				// if($compra_total->compra_total > 0){
					// $cliente_atualiza = new Model_Cliente;
					// $cliente_atualiza->id = $cliente->id; 
					// $cliente_atualiza->compras_total = $compra_total->compra_total;
					// $cliente_atualiza->update();
				// }
			// }
			// $min += 100;
		// }
		
		if (!empty($parametros->acao) && $parametros->acao == 'Enviar')
		{
			if (!empty($parametros->email))
			{
				$email = new Model_Cliente();
                $email = $email->select("SELECT COUNT(*) as total, {tabela_nome}.* FROM {tabela_nome} WHERE email='".$parametros->email."'");

				if ($email->total > 0 && $email->tipo == 1) 
				{
					$senha = $this->generatePAssword();

					$cliente = new Model_Cliente();
					$cliente->id = $email->id;
					$cliente->senha = sha1($senha);
					$cliente->update();

					// Variáveis utilizadas no envio do email
			        $assunto = 'dietWin - Nova Senha';

			        // Variáveis para montar o html da mensagem
			        $titulo = 'Nova senha';

			        $nome = 'dietWin';

			        $conteudo = '
						<p><font face="Arial" size="-1"><b>E-mail:</b> '.trim($email->email).'</font></p>
						<p><font face="Arial" size="-1"><b>Nova senha:</b> '.$senha.'</font></p>
						<p><font face="Arial" size="-2"><br><br><i>Mensagem enviada em '.date('d/m/Y \à\s H:i:s').'.</i></font></p>
					';

			        // Incluimos o html, que concatena as variáveis acima
			        include 'views/includes/email-template.php';

			        // Criamos a mensagem com assunto, mensagem, destinatário, reply-to
			        $message = $this->criar_mensagem($assunto, $email_html, array('email'=>$email->email,'nome'=>$email->nome), NULL);
					
					// Criamos o modo de envio do email
			        $transport = $this->criar_transport();

			        // Enviamos o emails e retornamos boolean
			        $enviar = $this->enviar($transport, $message);

			        if ($enviar) {
			        	$_SESSION['erro_esqueci_senha'] = '<div class="alert alert-success" role="alert">Sua nova senha foi mandanda para o seu e-mail.</div>';
			        } else {
			        	$_SESSION['erro_esqueci_senha'] = '<div class="alert alert-danger" role="alert">Ouve um erro ao tentar enviar a nova senha.</div>';
			        }
				} else {
					$_SESSION['erro_esqueci_senha'] = '<div class="alert alert-danger" role="alert">O e-mail Informado não consta em nosso banco de dados.</div>';
				}
			} else {
				$_SESSION['erro_esqueci_senha'] = '<div class="alert alert-danger" role="alert">Informe o seu e-mail.</div>';
			}
		}
		$view = new View('esqueci-minha-senha.php');
        $this->view_variaveis_obrigatorias($view);

        $view->adicionar('body_class', 'index');
        $view->adicionar('notificacao',new Notificacao());
        $view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        $view->exibir();

	}

	public function generatePAssword($length = 8)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }

    /******************************* MÉTODOS PRIVADOS DA CLASSE *******************************/
	

	/**
	 * Instancia o objeto que fará o transporte da mensagem. Poder ser mail() ou SMTP
	 * @return object
	 */
	private function criar_transport()
	{
		global $email_config;

		if ($email_config[SITE_LOCAL]['smtp_enviar'])
		{
			// Se for SMTP, pega todas as configurações
			if ( ! is_null($email_config[SITE_LOCAL]['smtp_usuario']))
			{
				return Swift_SmtpTransport::newInstance($email_config[SITE_LOCAL]['smtp_servidor'], $email_config[SITE_LOCAL]['smtp_porta'])
					->setUsername($email_config[SITE_LOCAL]['smtp_usuario'])
					->setPassword($email_config[SITE_LOCAL]['smtp_senha']);
			}
			else
			{
				return Swift_SmtpTransport::newInstance($email_config[SITE_LOCAL]['smtp_servidor'], $email_config[SITE_LOCAL]['smtp_porta']);
			}
		}
		else
		{
			// Caso seja mail()
			// Se o site for hospedado na Locaweb, temos que passar um parâmetro obrigatório
			if (SITE_LOCAL == 'cliente')
			{
				// O %s vai ser automaticamente substituído internamente pelo SwiftMailer
                return Swift_MailTransport::newInstance('-r%s');
			}
			else
			{
                return Swift_MailTransport::newInstance();
			}
		}

	}


	/**
	 * Instancia o objeto com a mensagem a ser enviada, fazendo algum tratamento quando os parâmetros vierem NULL
	 * @param $assunto
	 * @param $mensagem
	 * @param null $destinatario
	 * @param null $reply_to
	 * @return object
	 */
	private function criar_mensagem($assunto, $mensagem, $destinatario = NULL, $reply_to = NULL)
	{
		global $email_config;

		// Se o remetente vier NULL, usamos os dados do config.php
		if (is_null($reply_to))
		{
			$reply_to = array('email' => $email_config[SITE_LOCAL]['reply_to'], 'nome' => $email_config[SITE_LOCAL]['reply_to_nome']);
		}

		// Se o destinatario vier NULL, usamos os dados do config.php
		if (is_null($destinatario))
		{
			$destinatario = array($email_config[SITE_LOCAL]['para'] => $email_config[SITE_LOCAL]['para_nome']);
		}
		elseif (isset($destinatario['email'])) // Entra aqui caso tenha sido passado um array com um só destinatário, mas no formato antigo
		{
			$destinatario = array($destinatario['email'] => $destinatario['nome']);
		}

		//-----

		$log = new Log(var_export($destinatario, TRUE));
		$log = new Log($assunto);
		$log = new Log($mensagem);

		return Swift_Message::newInstance()
			->setFrom(array($email_config[SITE_LOCAL]['de'] => $email_config[SITE_LOCAL]['de_nome']))
			->setReturnPath($email_config[SITE_LOCAL]['return_path'])
			//->setTo(array($destinatario['email'] => $destinatario['nome']))
			->setTo($destinatario)
			->setReplyTo(array($reply_to['email'] => $reply_to['nome']))
			->setSubject($assunto)
			->setBody($mensagem, 'text/html');
	}


	/**
	 * Cria o objeto Mailer com o Transport e a Message e faz o envio do email, retornando o sucesso ou não
	 * @param $transport
	 * @param $message
	 * @return int
	 */
    private function enviar($transport, $message)
    {
        // Inicializa o objeto que gerencia tudo
        $mailer = Swift_Mailer::newInstance($transport);

        return $mailer->send($message);
    }

}