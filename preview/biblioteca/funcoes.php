<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/**
 * Classe Funcoes
 *
 * Coleção de funções utilizadas pelo site. Preferencialmente elas devem ser declaradas
 * como STATIC, permitindo que sejam chamadas sem a necessidade de se instanciar um objeto.
 */
 
/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Funcoes
{
	/**
	 * Faz o tratamento de dados inseridos pelo usuário antes de acessar o banco de dados
	 * @static
	 * @param $valor
	 * @return string|void
	 */
	public static function mysqli_escape($valor)
	{
		global $bd;

		if ( ! is_null($valor))
		{
			$valor = function_exists('mysqli_real_escape_string') ? mysqli_real_escape_string($bd->conexao, $valor) : addcslashes($valor, '%_\\\n\r\"\'');
		}

		return $valor;
	}


	/**
	 * Retorna o nome do mês por extenso, com a opção de ser abreviado
	 * @static
	 * @param  $mes
	 * @param bool $abreviatura
	 * @return string
	 */
	public static function mes_nome($mes, $abreviatura = FALSE)
	{
		switch ( (int) $mes)
		{
			case 1: return $abreviatura ? 'Jan' : 'Janeiro';
			case 2: return $abreviatura ? 'Fev' : 'Fevereiro';
			case 3: return $abreviatura ? 'Mar' : 'Março';
			case 4: return $abreviatura ? 'Abr' : 'Abril';
			case 5: return $abreviatura ? 'Mai' : 'Maio';
			case 6: return $abreviatura ? 'Jun' : 'Junho';
			case 7: return $abreviatura ? 'Jul' : 'Julho';
			case 8: return $abreviatura ? 'Ago' : 'Agosto';
			case 9: return $abreviatura ? 'Set' : 'Setembro';
			case 10: return $abreviatura ? 'Out' : 'Outubro';
			case 11: return $abreviatura ? 'Nov' : 'Novembro';
			case 12: return $abreviatura ? 'Dez' : 'Dezembro';
		}
	}


	/**
	 * Retorna o nome do dia da semana por extenso, com a opção de ser abreviado
	 * @static
	 * @param  $dia_da_semana
	 * @param bool $abreviatura
	 * @return string
	 */
	public static function dia_da_semana_nome($dia_da_semana, $abreviatura = FALSE)
	{
		switch ( (int) $dia_da_semana)
		{
			case 0: return $abreviatura ? 'Dom' : 'Domingo';
			case 1: return $abreviatura ? 'Seg' : 'Segunda-feira';
			case 2: return $abreviatura ? 'Ter' : 'Terça-feira';
			case 3: return $abreviatura ? 'Qua' : 'Quarta-feira';
			case 4: return $abreviatura ? 'Qui' : 'Quinta-feira';
			case 5: return $abreviatura ? 'Sex' : 'Sexta-feira';
			case 6: return $abreviatura ? 'Sáb' : 'Sábado';
		}
	}


	/**
	 * Recebe um texto e transforma (http://cubiq.org/the-perfect-php-clean-url-generator
	 * @param $str
	 * @param array $replace
	 * @param string $delimiter
	 * @return mixed|string
	 */
	public static function texto_para_seo($str, $replace=array(), $delimiter='-')
	{
		if( ! empty($replace) )
		{
			$str = str_replace((array)$replace, ' ', $str);
		}

		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}


	/**
	 * Monta a tag TH que é utilizada nas tabelas com opção de ordenação da listagem
	 * @static
	 * @param $controller
	 * @param $metodo
	 * @param $paginacao
	 * @param $buscar
	 * @param $item
	 * @param $ordenar_por
	 * @param $ordem
	 * @return string
	 */
	public static function montar_th_ordenacao_listagem($controller, $metodo, $paginacao, $buscar, $item, $ordenar_por, $ordem)
	{
		// Trata o item antes para as comparações a seguir
		$item_seo = Funcoes::texto_para_seo($item);

		// Cria o TH e se for o caso, adiciona a classe de atual item sendo ordenado
		$html = '<th class="'.($ordenar_por == $item_seo ? 'current' : '').'">';
			// Cria o A e o HREF, e adiciona o início da URL com os elementos necessários
			$html .= '<a href="'.SITE_URL.'/fatorcms/'.$controller.( ! is_null($metodo) ? '/'.$metodo : '');
				// Parâmetros para a ordenação
				$html .= '/ordenar-por/'.$item_seo.'/ordem/'.(($ordenar_por == $item_seo AND $ordem == 'asc') ? 'desc' : 'asc');

				// Se está sendo feita uma busca, a ordenação tem que respeitá-la
				if (strlen($buscar) > 0)
				{
					$html .= '/buscar/'.$buscar;
				}

				// Se está percorrendo as páginas de resultados, a ordenação tem que respeitar isso também
				if ($paginacao->sendo_usada)
				{
					$html .= '/pagina/'.$paginacao->pagina_atual;
				}

				$html .= '" '; // fechou o href

				$html .= 'class="'.(($ordenar_por == $item_seo AND $ordem == 'desc') ? 'up' : 'down').'" ';
				$html .= 'title="Ordenar por '.$item.' em ordem '.($ordem == 'asc' ? 'Ascendente' : 'Decrescente').'">';
				$html .= $item;
			$html .= '</a>';
		$html .= '</th>';

		return $html;
	}


	/**
	 * Verifica se um conjunto de dígitos é um CPF válido
	 * @static
	 * @param $cpf
	 * @return bool
	 * @url http://www.alejandromoraga.com.br/validar-cpf-em-php
	 */
	public static function validar_cpf($cpf)
	{
		if ( ! preg_match('|^(\d{3})\.?(\d{3})\.?(\d{3})\-?(\d{2})$|', $cpf, $matches))
			return false;

		array_shift($matches);
		$str = implode('', $matches);

		for ($i=0; $i < 10; $i++)
			if ($str == str_repeat($i, 11))
				return false;

		for ($t=9; $t < 11; $t++) {
			for ($d=0, $c=0; $c < $t; $c++)
				$d += $str[$c] * ($t + 1 - $c);

			$d = ((10 * $d) % 11) % 10;

			if ($str[$c] != $d)
				return false;
		}

		return $str;
	}


	/**
	 * Verifica se um conjunto de dígitos é um CNPJ válido
	 * @static
	 * @param $cnpj
	 * @return bool
	 * @url http://www.alejandromoraga.com.br/validar-cnpj-em-php
	 */
	public static function validar_cnpj($cnpj)
	{
		if ( ! preg_match('|^(\d{2,3})\.?(\d{3})\.?(\d{3})\/?(\d{4})\-?(\d{2})$|', $cnpj, $matches))
			return false;

		array_shift($matches);

		$str = implode('', $matches);
		if (strlen($str) > 14)
			$str = substr($str, 1);

		$sum1 = 0;
		$sum2 = 0;
		$sum3 = 0;
		$calc1 = 5;
		$calc2 = 6;

		for ($i=0; $i <= 12; $i++) {
			$calc1 = $calc1 < 2 ? 9 : $calc1;
			$calc2 = $calc2 < 2 ? 9 : $calc2;

			if ($i <= 11)
				$sum1 += $str[$i] * $calc1;

			$sum2 += $str[$i] * $calc2;
			$sum3 += $str[$i];
			$calc1--;
			$calc2--;
		}

		$sum1 %= 11;
		$sum2 %= 11;

		return ($sum3 && $str[12] == ($sum1 < 2 ? 0 : 11 - $sum1) && $str[13] == ($sum2 < 2 ? 0 : 11 - $sum2)) ? $str : false;
	}


    /**
     * Reduz o tamanho do texto
     * @static
     * @param $texto
     * @param $limite
     * @param string $sufixo
     * @return string
     */
	public static function cortar_texto($texto, $limite, $sufixo = '...')
	{
		$texto = strip_tags($texto);
		if(strlen($texto) > $limite)
		{
			$espaco = mb_substr($texto,$limite,1);
			while($espaco != " ")
			{
				$espaco = substr($texto,$limite,1);
				if ( ($espaco == " ") or ($espaco == ".") or ($limite >= strlen($texto)) )
					break; // para o while se for um espaço
				$limite++;
			}
			return mb_substr($texto,0,$limite).$sufixo;
		}
		else
			return mb_substr($texto,0,$limite);
	}


	/**
	 * Recebe um número (provavelmente do banco de dados) contendo os dígitos do CPF e retorna uma string formatada com a máscara padrão
	 * @static
	 * @param $cpf
	 * @return string
	 */
	public static function formatar_cpf($cpf)
	{
		if (is_null($cpf))
		{
			return NULL;
		}

		/*if (strlen($cpf) < 11)
		{
			$cpf = str_pad($cpf, 11, 0, STR_PAD_LEFT);
		}*/
		return substr($cpf,0,3).'.'.substr($cpf,3,3).'.'.substr($cpf,6,3).'-'.substr($cpf,9);
	}


	/**
	 * Recebe um número (provavelmente do banco de dados) contendo os dígitos do CNPJ e retorna uma string formatada com a máscara padrão
	 * @static
	 * @param $cnpj
	 * @return string
	 */
	public static function formatar_cnpj($cnpj)
	{
		if (is_null($cnpj))
		{
			return NULL;
		}

		/*if (strlen($cnpj) < 14)
		{
			$cnpj = str_pad($cnpj, 14, 0, STR_PAD_LEFT);
		}*/
		return substr($cnpj,0,2).'.'.substr($cnpj,2,3).'.'.substr($cnpj,5,3).'/'.substr($cnpj,8,4).'-'.substr($cnpj,12);
	}


	/**
	 * Recebe um número (provavelmente do banco de dados) contendo os dígitos do CEP e retorna uma string formatada com a máscara padrão
	 * @static
	 * @param $cep
	 * @return string
	 */
	public static function formatar_cep($cep)
	{
		if (is_null($cep))
		{
			return NULL;
		}

		/*if (strlen($cep) < 8)
		{
			$cep = str_pad($cep, 8, 0, STR_PAD_LEFT);
		}*/
		return substr($cep,0,5).'-'.substr($cep,5);
	}


    /**
     * Recebe um número (provavelmente do banco de dados) contendo os dígitos do telefone e retorna uma string formatada com a máscara padrão
     * @static
     * @param $telefone
     * @return string
     */
    public static function formatar_telefone($telefone)
    {
        if (is_null($telefone))
        {
            return NULL;
        }

        return '('.substr($telefone,0,2).') '.substr($telefone,2,4).'-'.substr($telefone,6);
    }


	/**
	 * Não mexer nessa função
	 * @static
	 * @return string
	 */
	public static function gerar_senha()
	{
		$constante_e = '271828182845904523536028747135266249775724709369995';
		return substr(base64_encode(sha1(('http://'.$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!='80'?':'.$_SERVER["SERVER_PORT"]:'').mktime(12,34,56)).md5($constante_e))).$constante_e, 10, 10);
	}

	public static function blog_data_mes_completo($data){
		$data = explode(" ",$data);
		$data = $data[0];
		$data = explode("-",$data);

          switch($data[1]){
            case "01" : $data[1]="janeiro"; break;
            case "02" : $data[1]="fevereiro"; break;
            case "03" : $data[1]="março"; break;
            case "04" : $data[1]="abril"; break;
            case "05" : $data[1]="maio"; break;
            case "06" : $data[1]="junho"; break;
            case "07" : $data[1]="julho"; break;
            case "08" : $data[1]="agosto"; break;
            case "09" : $data[1]="setembro"; break;
            case "10" : $data[1]="outubro"; break;
            case "11" : $data[1]="novembro"; break;
            case "12" : $data[1]="dezembro"; break;
            default : $data[1]="janeiro";
          }

        return  $data[2]." de ".$data[1]." de ".$data[0];
	}

	public static function blog_data_mes_abreviado($data){
		$data = explode(" ",$data);
		$data = $data[0];
		$data = explode("-",$data);

          switch($data[1]){
            case "01" : $data[1]="Jan"; break;
            case "02" : $data[1]="Fev"; break;
            case "03" : $data[1]="Mar"; break;
            case "04" : $data[1]="Abr"; break;
            case "05" : $data[1]="Mai"; break;
            case "06" : $data[1]="Jun"; break;
            case "07" : $data[1]="Jul"; break;
            case "08" : $data[1]="Ago"; break;
            case "09" : $data[1]="Set"; break;
            case "10" : $data[1]="Out"; break;
            case "11" : $data[1]="Nov"; break;
            case "12" : $data[1]="Dez"; break;
            default : $data[1]="janeiro";
          }

          return $data[1];
	}

	public static function blog_data_abreviado($data){
		$data = explode(" ",$data);
		$data = $data[0];
		$data = explode("-",$data);

          switch($data[1]){
            case "01" : $data[1]="Jan"; break;
            case "02" : $data[1]="Fev"; break;
            case "03" : $data[1]="Mar"; break;
            case "04" : $data[1]="Abr"; break;
            case "05" : $data[1]="Mai"; break;
            case "06" : $data[1]="Jun"; break;
            case "07" : $data[1]="Jul"; break;
            case "08" : $data[1]="Ago"; break;
            case "09" : $data[1]="Set"; break;
            case "10" : $data[1]="Out"; break;
            case "11" : $data[1]="Nov"; break;
            case "12" : $data[1]="Dez"; break;
            default : $data[1]="janeiro";
          }

          return  $data[2]." de ".$data[1]." de ".$data[0];
	}

	public static function blog_data_dia($data){
		$data = explode(" ",$data);
		$data = $data[0];
		$data = explode("-",$data);

        return $data[2];
	}

} // end class

