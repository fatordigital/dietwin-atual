<?php include 'includes/cabecalho.php'; ?>

<body class="comprar  comprar-<?php echo $classe ?> dados pagamento">

<?php include 'includes/topo.php'; ?>
	
    <div class="titulo-principal">
        <p><span>comprar o</span><br />dietWin</p>        
    </div>
    <div class="titulo-resumo">
        <p>	A praticidade de comprar online, com toda a comodidade e segurança que você precisa. Aproveite nossas promoções e adquira hoje mesmo a sua versão do dietWin.</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container">
    	<?php

        $produto_valor = $produto->valor;
        $licencas_valor = str_replace(',','',$licencas)*str_replace(',','',$produto->licenca_valor);
        $cupom_valor = (isset($cupom)? number_format($cupom->valor,2):'0,00');

        $licencas_valor = (float)str_replace(',','',$licencas_valor);
        $produto_valor = (float)str_replace(',','',$produto_valor);
        $cupom_valor = (float)str_replace(',','',$cupom_valor);
        $total = $licencas_valor+$produto_valor-$cupom_valor;
        ?>
        <h1>Como você deseja realizar o pagamento (<span>R$ <?php echo number_format($total,2,',','.'); ?></span>)?</h1>
        
        <div class="opcao boleto">
        	<a id="link_boleto" title="Boleto Bancário parcelado diretamente com o dietWin"><img src="<?php echo SITE_BASE ?>/views/imagens/boleto-bancario.png" alt="Boleto Bancário parcelado diretamente com o dietWin" /></a>
        </div>
        
        <div class="opcao">
        	<a id="link_pagseguro" title="Pagamento à vista ou parcelado por cartão de crédito via PagSeguro"><img src="<?php echo SITE_BASE ?>/views/imagens/pagamento-pagseguro.png" alt="Pagamento à vista ou parcelado por cartão de crédito via PagSeguro" /></a>
        </div>

        <div class="entrega">
            <h2>Endereço para entrega:</h2>

            <label><b>Endereço:</b></label>
            <input disabled="disabled" type="text" class="endereco" name="endereco_entrega" id="endereco_entrega" maxlength="100" value="<?php echo $endereco->endereco ?>" />
            <div class="clear"></div>

            <label><b>Número:</b></label>
            <input disabled="disabled" type="text" class="numero" name="numero_entrega" id="numero_entrega" maxlength="100" value="<?php echo $endereco->numero ?>" />

            <label class="complemento-label"><b>Complemento:</b></label>
            <input disabled="disabled" type="text" name="complemento_entrega" class="complemento" id="complemento_entrega" maxlength="100" value="<?php echo $endereco->complemento ?>" />

            <label class="cep-label"><b>Cep:</b></label>
            <input disabled="disabled" type="text" class="cep" name="cep_entrega" id="cep_entrega" maxlength="100" value="<?php echo $endereco->cep ?>" />
            <div class="clear"></div>

            <label><b>Bairro:</b></label>
            <input disabled="disabled" type="text" class="bairro" name="bairro_entrega" id="bairro_entrega" maxlength="100" value="<?php echo $endereco->bairro ?>" />

            <label class="cidade-label"><b>Cidade:</b></label>
            <input disabled="disabled" type="text" class="cidade" name="cidade_entrega" id="cidade_entrega" maxlength="100" value="<?php echo $cidade->nome ?>" />

            <label class="estado-label"><b>Estado:</b></label>
            <input disabled="disabled" type="text" class="estado" name="estado_entrega" id="estado_entrega" maxlength="100" value="<?php echo $estado->nome ?>" />

            <a href="<?php echo SITE_URL.'/area-do-cliente/editar-dados' ?>" class="alterar-entrega" title="Alterar endereço de entrega"><img src="<?php echo SITE_BASE ?>/views/imagens/alterar-endereco.png" alt="Alterar endereço de entrega" /></a>

            <div class="clear"></div>
        </div>

        <div class="entrega">
            <div class="separador"></div>

            <h2>Forma de entrega</h2>

            <form method="post" action="<?php echo SITE_URL ?>/fale-conosco/enviar" id="forma-entrega">
                <input type="hidden" id="input-valor-compra" value="<?php echo $total ?>" />
                <input type="hidden" id="input-valor-frete" value="<?php echo $frete ?>" />
                <input type="radio" id="download" checked="checked" name="forma_envio" value="download" /><span class="label-opcao">Download (gratuito)</span>
                <input type="radio" id="pac" name="forma_envio" value="pac" /><span class="label-opcao">PAC (R$12,00)</span>
                <?php //ORIGINAL<input type="radio" id="pac" checked="checked" name="forma_envio" value="pac" /><span class="label-opcao">PAC (envio gratuito)</span> ?>
                <div class="ajuste_sedex">
                    <input type="radio" id="sedex" name="forma_envio" value="sedex" /><span class="label-opcao">Sedex (envio cobrado)</span>
                </div>
                <div class="mensagem-entrega"><p>Valor total do frete: <span id="valor-total-frete">Gratuito</span></p></div>

                <div class="clear"></div>
            </form>

            <div class="valor-total-compra"><p>Valor total da compra: <span id="span-total-compra">R$ <?php echo number_format($total,2,',','.') ?></span></p></div>

            <div class="clear"></div>
        </div>

        <div class="compra-boleto">
            <div class="separador"></div>

            <h2>Preencha as informações abaixo para que possamos gerar os boletos bancários:</h2>
            <form method="post" action="<?php echo SITE_URL.'/software/'.$classe.'/boleto-compra' ?>" id="form-pagamento" >
                <input id="forma-entrega-boleto" type="hidden" name="entrega_forma" value="download" />
                <label><b>Número de parcelas:</b></label>
                <select name="parcelas" id="parcelas">
                <?php
                    if ($classe == 'profissional') {
                        $max = 8;
                    }
                    else if ($classe == 'personal') {
                        $max = 5;
                    }
                    else {
                        $max = 1;
                    }
                    for ($i = 0 ; $i < $max ; $i++)
                    {
                        echo '<option value="'.($i+1).'">'.($i+1).'</option>';
                    }
                ?>

                </select>
                
                <label><b>Data de vencimento dos boletos:</b></label>
                <div class="dias">
                	<input type="radio" name="data" value="05" checked="checked" /><span>Dia 05</span>
                    <input type="radio" name="data" value="15" /><span>Dia 15</span>
                    <input type="radio" name="data" value="25" /><span>Dia 25</span>
                </div>
                
                <div class="clear"></div>
                
                <div class="separador"></div>
    
                <p class="info-pedido"><b>Confira os dados acima e, caso estejam corretamente preenchidos, clique no botão "Enviar pedido de compra" para confirmar os dados e efetivar seu pedido</b></p>
                
                <button type="submit">Enviar</button>
                
                <div id="form_notification"></div>
                
                <div class="clear"></div>
            </form>
            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="compra-pagseguro">
            <div class="separador"></div>

            <p class="info-pagseguro">Ao clicar em <span>"Finalizar a compra"</span> você será direcionado ao site do PagSeguro, onde será realizado todo o processo de pagamento, garantindo total segurança. Após a conclusão você será redirecionado para o site do dietwin novamente.</p>

            <form method="post" action="<?php echo SITE_URL.'/software/'.$classe.'/pagseguro-compra' ?>" id="form-pagseguro" >
                <input id="forma-entrega-pagseguro" type="hidden" name="entrega_forma" value="download" />
                <button type="submit">Enviar</button>

            <?/* <a href="<?php echo SITE_URL.'/software/'.$classe.'/finalizar-compra' ?>" title="Finalizar a compra"><img src="<?php echo SITE_BASE ?>/views/imagens/finalizar-compra.png" alt="Finalizar a compra" /></a> */?>
            
            <div class="clear"></div>
        </div>
        
        <div class="clear"></div>
            
        <div class="formas-pagamento">
            <img src="<?php echo SITE_BASE ?>/views/imagens/formas-pagamento.jpg" alt="Todas as formas de pagamento" />
        </div>
                
        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>  
</div>

<?php include 'includes/rodape.php'; ?>