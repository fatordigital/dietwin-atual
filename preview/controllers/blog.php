<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

    /* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


    */

    class Controller_Blog extends Controller_Padrao
    {
        /**
         * Chama o construtor da classe pai
         */
        public function __construct()
        {
            parent::__construct();
        }


        public function index($parametros)
        {
            $view = new View('blog-capa.php');
            $this->view_variaveis_obrigatorias($view);

            if (isset($_GET['dev'])) {
                echo date("Y-m-d H:i:s", strtotime("+5 hour"));
                die;
            }

            $categorias = new Model_Novidade;
            $categorias = $categorias->select("SELECT fd_novidades_categorias.*, COUNT(fd_novidades.id) AS total_posts FROM fd_novidades_categorias INNER JOIN fd_novidades ON fd_novidades.categoria_id = fd_novidades_categorias.id WHERE fd_novidades.alias = 'dietwin' GROUP BY fd_novidades_categorias.id
");
            $novidades = new Model_Novidade;
            $novidades = $novidades->select("SELECT * FROM fd_novidades WHERE fd_novidades.alias = 'dietwin' AND fd_novidades.eh_visivel=1 AND (fd_novidades.data BETWEEN '2000-01-01 00:00:00' AND '" . date("Y-m-d H:i:s", strtotime("+5 hour")) . "') ORDER BY fd_novidades.data DESC LIMIT 0,9");

            $total_posts = new Model_Novidade;
            $total_posts = $total_posts->select("SELECT COUNT(*) as total FROM fd_novidades INNER JOIN fd_novidades_categorias ON fd_novidades.categoria_id = fd_novidades_categorias.id WHERE fd_novidades.alias = 'dietwin' AND fd_novidades.eh_visivel=1 AND (fd_novidades.data BETWEEN '2000-01-01 00:00:00' AND '" . date("Y-m-d H:i:s", strtotime("+5 hour")) . "') ORDER BY fd_novidades.data DESC");

            if ($total_posts->total > 0) {
                $pages = ceil($total_posts->total / 6);
            } else {
                $pages = 1;
            }

            $view->adicionar('paginacao', $pages);

            $view->adicionar('categorias', $categorias);
            $view->adicionar('novidades', $novidades);

            //print('<pre>'); print_r($categorias); die;


            $view->adicionar('body_class', 'index');
            $view->adicionar('notificacao', new Notificacao());
            $view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

            $view->exibir();
        }

        public function categoria($parametros)
        {
            if (!isset($parametros->_0)) {
                header("Location: " . SITE_URL . "/blog");
            }

            $categoria_titulo_seo = $parametros->_0;

            //print('<pre>'); print_r($parametros); echo $parametros->p; die;

            if (isset($parametros->p) AND $parametros->p > 1) {
                $inicio = ($parametros->p - 1) * 6;
                $fim = ($parametros->p - 1) * 6 + 6;
            } else {
                $inicio = 0;
                $fim = 6;
            }

            $view = new View('blog-listagem.php');
            $this->view_variaveis_obrigatorias($view);

            $categorias = new Model_Novidade;
            $categorias = $categorias->select("SELECT fd_novidades_categorias.*, COUNT(fd_novidades.id) AS total_posts FROM fd_novidades_categorias INNER JOIN fd_novidades ON fd_novidades.categoria_id = fd_novidades_categorias.id WHERE fd_novidades.alias = 'dietwin' GROUP BY fd_novidades_categorias.id
");
            if ($categoria_titulo_seo != 'todos') {
                $where_categoria = "AND fd_novidades_categorias.tag='" . $categoria_titulo_seo . "'";
            } else {
                $where_categoria = "";
            }

            $novidades = new Model_Novidade;
            $novidades = $novidades->select("SELECT fd_novidades.*, fd_novidades_categorias.tag FROM fd_novidades INNER JOIN fd_novidades_categorias ON fd_novidades.categoria_id = fd_novidades_categorias.id WHERE fd_novidades.alias = 'dietwin' AND fd_novidades.eh_visivel=1 " . $where_categoria . " AND (fd_novidades.data BETWEEN '2000-01-01 00:00:00' AND '" . date("Y-m-d H:i:s", strtotime("+5 hour")) . "') ORDER BY fd_novidades.data DESC LIMIT " . $inicio . "," . $fim . "");

            $total_posts = new Model_Novidade;
            $total_posts = $total_posts->select("SELECT COUNT(*) as total FROM fd_novidades INNER JOIN fd_novidades_categorias ON fd_novidades.categoria_id = fd_novidades_categorias.id WHERE fd_novidades.alias = 'dietwin' AND fd_novidades.eh_visivel=1 " . $where_categoria . " AND (fd_novidades.data BETWEEN '2000-01-01 00:00:00' AND '" . date("Y-m-d H:i:s", strtotime("+5 hour")) . "') ORDER BY fd_novidades.data DESC");

            if ($total_posts->total > 0) {
                $pages = ceil($total_posts->total / 6);
            } else {
                $pages = 1;
            }

            $view->adicionar('paginacao', $pages);
            $view->adicionar('categoria_ativa', $parametros->_0);
            $view->adicionar('categorias', $categorias);
            $view->adicionar('novidades', $novidades);

            $view->adicionar('body_class', 'index');
            $view->adicionar('notificacao', new Notificacao());
            $view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

            $view->exibir();
        }

        public function post($parametros)
        {
            if (!isset($parametros->_0)) {
                header("Location: " . SITE_URL . "/blog");
            }

            $view = new View('blog-post.php');
            $this->view_variaveis_obrigatorias($view);

            $categorias = new Model_Novidade;
            $categorias = $categorias->select("SELECT fd_novidades_categorias.*, COUNT(fd_novidades.id) AS total_posts FROM fd_novidades_categorias INNER JOIN fd_novidades ON fd_novidades.categoria_id = fd_novidades_categorias.id WHERE fd_novidades.alias = 'dietwin' GROUP BY fd_novidades_categorias.id
");

            $novidade = new Model_Novidade;
            $novidade = $novidade->select("SELECT fd_novidades.*, fd_novidades_categorias.tag AS categoria_nome FROM fd_novidades INNER JOIN fd_novidades_categorias ON fd_novidades.categoria_id = fd_novidades_categorias.id WHERE fd_novidades.alias = 'dietwin' AND fd_novidades.titulo_seo = '" . $parametros->_0 . "' LIMIT 0,1
");

            if (empty($novidade)) {
                header("Location: " . SITE_URL . "/blog");
            }

            $novidades_recentes = new Model_Novidade;
            $novidades_recentes = $novidades_recentes->select("SELECT * FROM fd_novidades WHERE fd_novidades.alias = 'dietwin' AND fd_novidades.eh_visivel=1 AND (fd_novidades.data BETWEEN '2000-01-01 00:00:00' AND '" . date("Y-m-d H:i:s", strtotime("+5 hour")) . "') ORDER BY fd_novidades.data DESC LIMIT 0,5");
            $view->adicionar('novidades_recentes', $novidades_recentes);


            $view->adicionar('novidade', $novidade);
            $view->adicionar('categorias', $categorias);
            $view->adicionar('body_class', 'index');
            $view->adicionar('notificacao', new Notificacao());
            $view->adicionar('pagina_title', 'dietWin - Softwares de nutrição - Encontre o melhor software para você!');

            $view->exibir();
        }

        public function cadastro_email()
        {

            $_SESSION['cadastro_blog_sucesso'] = true;


            RdStation::addLeadConversionToRdstationCrm('d-form-cadastro-blog', array(
                    'email' => $_POST['email']
                )
            );

            header("Location: " . SITE_URL . $_POST['url'] . "#!");

        }


    }