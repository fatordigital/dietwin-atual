<?php include 'includes/cabecalho.php'; ?>

<div class="clear"></div>

<div class="conteudo">
	<div class="topo-container">
        <div class="breadcrumb">
            <a href="<?php SITE_URL ?>" title="Ir para página inicial da Escola Factum">Página Inicial</a>
            <span>Fale Conosco</span>
        </div>
        
        <h1 class="titulo-principal">Fale Conosco</h1>
    </div>
    <div class="clear"></div>
    <div class="container interna"> 
        <div class="conteudo-contato">            
            <p>Para falar com a Factum preencha o formulário abaixo ou entre em contato diretamente pelo telefone <span>(51) 3212.7600</span>.</p>
            <div class="visualizar-mapa">           	
		        <p><a href="#">Confira a localização da Factum no Google Maps</a></p>
            </div>
            <div class="clear"></div>
            <form method="post" action="<?php echo SITE_URL ?>/fale-conosco/enviar" id="form-fale-conosco" >

                <label><b>Nome:</b></label>
                <input type="text" class="obrigatorio" name="contato_nome" id="contato_nome" maxlength="100" value="" />
                
                <label><b>E-mail:</b></label>
                <input type="text" class="obrigatorio" name="contato_email" id="contato_email" maxlength="255" value="" />
                
                <label><b>Assunto:</b></label>
                <input type="text" class="obrigatorio" name="contato_assunto" id="contato_assunto" maxlength="100" value="" />
                
                <label><b>Telefone:</b></label>
                <input type="text" class="obrigatorio" name="contato_fone" id="contato_fone" maxlength="14" value="" />
                
                <label><b>Mensagem:</b></label>
                <textarea type="text" class="obrigatorio" name="contato_mensagem" id="contato_mensagem" style="overflow:auto"></textarea>

                <button type="submit">Enviar</button>
                
                <div id="form_notification"></div>
            </form>
        </div>
        
        <?php include 'includes/sidebar-contato.php'; ?>
               
    </div>
</div>


<!-- Conteúdo do colorbox para visualizar o mapa da factum -->
<div style="display:none">
    <div class="mapa-factum">
        <iframe width="560" height="460" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com.br/maps?q=escola+factum&amp;hl=pt-BR&amp;sll=-29.030108,-51.229441&amp;sspn=0.015382,0.027874&amp;vpsrc=0&amp;hq=escola+factum&amp;t=m&amp;ie=UTF8&amp;hnear=&amp;ll=-30.031889,-51.229537&amp;spn=0.023927,0.045447&amp;z=15&amp;iwloc=A&amp;cid=15129240466161820206&amp;output=embed&amp;iwloc=near"></iframe>
    </div>
</div>


<?php include 'includes/rodape.php'; ?>