<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property int produto_id
 * @property string titulo
 * @property string titulo_seo
 * @property string resumo
 * @property string descricao
 * @property string link
 * @property string data
 * @property int log_id
 * @property string log_data

 */

class FatorCMS_Model_Video extends FatorCMS_Model_Padrao
{
	protected $tabela_nome = 'videos';

} // end class