<?php include 'includes/cabecalho.php'; ?>

<body class="comprar comprar-plano dados">

<?php include 'includes/topo.php'; ?>

<div class="titulo-principal">
    <p><span>comprar </span><br />Plano de Assistência</p>
</div>
<div class="titulo-resumo">
    <p>	Descontos, upgrades gratuitos, suporte, treinamento e outras vantagens diferenciadas.</p>
</div>
</div>
</div>

<div class="conteudo">
	<div class="container">
    	
        <h1>Dados de sua compra</h1>
        
        <table border="0" cellpadding="0" cellspacing="2">
          <tr>
            <td class="cor" width="670"><b>1 <?php echo $_SESSION['produto']->nome ?> - 1 ano </b></td>
              <?php
              $produto_valor = $_SESSION['produto']->valor;
              //$produto_valor = str_replace('.',',',$produto_valor);
              ?>
            <td class="cor" width="170" align="right"><b>R$ <?php echo number_format($produto_valor, 2, ',', '.') ?></b></td>
          </tr>
          <tr>
            <td class="cor" width="670"><b><?php echo $_SESSION['licencas']; ?> computadores adicionais suportados</b></td>
              <?php
              $licencas_valor = str_replace(',','',$_SESSION['licencas'])*str_replace(',','',$_SESSION['produto']->licenca_valor);
              //$licencas_valor = str_replace('.',',',$licencas_valor);
              ?>
            <td class="cor" width="170" align="right"><b>R$ <?php echo number_format($licencas_valor, 2, ',', '.') ?></b></td>
          </tr>
          <tr>
            <td class="desconto" width="670" align="right"><b>Cupom de desconto utilizado</b></td>
              <?php
              $cupom_valor = (isset($_SESSION['cupom'])? number_format($_SESSION['cupom']->valor,2):'0,00');
              //$cupom_valor = str_replace('.',',',$cupom_valor);
              ?>
            <td class="desconto" width="170" align="right"><b>R$ <?php echo ($cupom_valor == '0,00') ? $cupom_valor : '- '.number_format($cupom_valor, 2, ',', '.') ?></b></td>
          </tr>
          <tr>
            <td width="670" align="right" valign="middle"><h2>Total de sua compra</h2></td>
              <?php
              $licencas_valor = (float)str_replace(',','',$licencas_valor);
              $produto_valor = (float)str_replace(',','',$produto_valor);
              $cupom_valor = (float)str_replace(',','',$cupom_valor);
              $total_compra = $licencas_valor+$produto_valor-$cupom_valor;
              //$total_compra = str_replace('.',',',$total_compra);
              $_SESSION['processo_compra'] = TRUE;
              ?>
            <td width="170" align="right"><p class="total-compra"><span>R$</span><?php echo number_format($total_compra, 2, ',', '.') ?></p></td>
          </tr>
          <tr>
            <td width="670" align="right" valign="middle"></td>
            <td width="170" class="valor" align="right"><p><b>Para proteção de <span><?php echo number_format((float)$_SESSION['licencas']+1,0); ?></span><br />computadores</b></p></td>
          </tr>
        </table>

        <img src="<?php echo SITE_BASE ?>/views/imagens/pagseguro.jpg" class="pagseguro" alt="Compra segura com Pagseguro" />
        <form action="<?php echo SITE_URL.'/area-do-cliente/plano-de-assistencia/pagamento' ?>" method="post">
            <input type="image" src="<?php echo SITE_BASE ?>/views/imagens/prosseguir-compra.jpg" class="prosseguir" value="Prosseguir com a compra." />
        </form>
                
        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>  
</div>

<?php include 'includes/rodape.php'; ?>