<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_FAQ extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai e testa se o usuário está logado
	 */
	public function __construct()
	{
		parent::__construct();
		$this->verificar_usuario_logado();
		$this->verificar_usuario_permissao('faq');
	}


	/**
	 * Método padrão que não deve ser chamado diretamente. Se for, vai para a listagem
	 * @return void
	 */
	public function index()
	{
        header('Location: '.SITE_URL.'/fatorcms/faq/listar');
		exit;
	}


	/**
	 * Método inicial que exibe a lista de registros do banco de dados
	 * @param $parametros object
	 * @return void
	 */
	public function listar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'faq-lista.php'));
		$view->adicionar('body_class', 'faq lista');
		$view->adicionar('lateral_menu', array('item'=>'faq','link'=>'listar perguntas'));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$this->view_adicionar_obrigatorias($view);

		//-----

		// Lógica caso tenha sido feita uma busca na listagem
		$clausula_where = ' TRUE ';

		$buscar = NULL;
		if (isset($parametros->buscar) AND strlen(trim($parametros->buscar)) > 0)
		{
			$buscar = trim($parametros->buscar);
			$buscar_aux = Funcoes::mysqli_escape(str_replace(' ', '%', $buscar));
			$clausula_where .= ' AND (faq.pergunta LIKE "%'.$buscar_aux.'%" OR faq.resposta LIKE "%'.$buscar_aux.'%"
			                            OR produto.nome LIKE "%'.$buscar_aux.'%")';
		}

		//-----

		// Ordenação dos resultados, podendo vir da página
		$ordenar_por = (isset($parametros->ordenar_por)) ? strtolower($parametros->ordenar_por) : 'pergunta'; // Valor padrão
		$ordem = isset($parametros->ordem) ? strtolower($parametros->ordem) : 'asc'; // Valor padrão

		switch ($ordenar_por)
		{
			case 'categoria': $ordenar_por_sql = 'faq.produto_id'; break;
			case 'pergunta': $ordenar_por_sql = 'faq.pergunta'; break;
			case 'categoria': $ordenar_por_sql = 'produto.nome'; break;
            default: $ordenar_por_sql = $ordenar_por; break;
		}

		//-----

		$faq = new FatorCMS_Model_FAQ;
		$faq->clausula = '
			SELECT faq.*, produto.nome AS produto_nome
			FROM {tabela_nome} AS faq
				LEFT JOIN {tabela_prefixo}produtos AS produto ON faq.produto_id = produto.id
			WHERE '.$clausula_where.'
			ORDER BY '.$ordenar_por_sql.' '.$ordem
		;

		// Através da paginação vai verificar se existe a necessidade de dividir o resultado em várias páginas e se é necessário adicionar um LIMIT
		$paginacao = new Paginacao($faq, $parametros, 20, 10);

		// Executa a cláusula lá de cima
		$faqs = $faq->select(NULL, TRUE); // NULL para não alterar a clásula já definida e TRUE para forçar retorno em array

		//-----

        $faq_aux = new FatorCMS_Model_Faq();
        $faqs_aux = $faq_aux->select('SELECT * FROM {tabela_nome} ORDER BY id',TRUE);

        $faqs_produtos = array();
        foreach ($faqs_aux as $faq_aux)
        {
            //print_r($faq_aux); exit;
            $produto = new FatorCMS_Model_Produto();
            $produtos = $produto->select("
                SELECT
                    fd_faqs_produtos.produto_id AS 'id',
                    {tabela_nome}.nome AS 'nome',
                    {tabela_nome}.nome_seo AS 'nome_seo',
                    fd_faqs_produtos.faq_id AS 'faq_id'
                FROM fd_produtos
                RIGHT JOIN fd_faqs_produtos ON {tabela_nome}.id = fd_faqs_produtos.produto_id
                WHERE fd_faqs_produtos.faq_id = ".$faq_aux->id."
            ",TRUE);

            foreach ($produtos as $produto)
            {
                //print_r($produto); exit;
                $faqs_produtos[$produto->faq_id][] = $produto;
            }

        }
        //print_r($faqs_produtos); exit;
        //-----

		$view->adicionar('buscar', $buscar);
		$view->adicionar('ordenar_por', $ordenar_por);
		$view->adicionar('ordem', $ordem);
		$view->adicionar('faqs', $faqs);
        $view->adicionar('produtos', $faqs_produtos);
		$view->adicionar('paginacao', $paginacao);

		$view->exibir();
	}


	/**
	 * Dependendo do parâmetro, aprensenta a tela de edição ou de cadastro
	 * @param $parametros object
	 * @return void
	 */
	public function editar($parametros)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'faq-form.php'));
        $view->adicionar('body_class', 'faq form');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$this->view_adicionar_obrigatorias($view);

        //-----

        // Decide o que fazer
        if (isset($parametros->_0) AND is_numeric($parametros->_0))
        {
            // É atualização
            $faq = new FatorCMS_Model_FAQ($parametros->_0);
            $view->adicionar('faq', $faq);

            // Informações extras
            $view->adicionar('faq_produtos', $this->carregar_pergunta_produtos($faq->id));

            $view->adicionar('lateral_menu', array('item'=>'faq','link'=>''));
        }
        else
        {
            // É cadastro
			$view->adicionar('faq', NULL);
            $view->adicionar('faq_produtos', NULL);

			$view->adicionar('lateral_menu', array('item'=>'faq','link'=>'cadastrar pergunta'));
        }

        //-----

        // Lista de Produtos para o <select>
        $view->adicionar('produtos', $this->carregar_produtos());

        $view->exibir();
    }


	/**
	 * Recebe os parâmetros do formulário e adiciona o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function cadastrar($parametros)
	{
        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'faq-form.php'));
		$this->view_adicionar_obrigatorias($view);

		//-----

		if ($parametros AND isset($parametros->pergunta))
		{
			$faq = new FatorCMS_Model_FAQ;
			$faq->carregar($parametros);

			// Campos especiais
			$faq->pergunta_seo = Funcoes::texto_para_seo($faq->pergunta);

			//-----

			// Log
			$faq->log_id = $this->cms_usuario_logado->id;
			$faq->log_data = date('Y-m-d H:i:s');

			//-----

            $faq->colunas_mysqli_escape();

			if ($faq->verificar_null(array('id')))
			{
				// Validações ---

                // Verifica se o pergunta_seo está disponível, se não inicia o contador até achar um
                $cont = 2;
                do
                {
                    $faq_existe = new FatorCMS_Model_FAQ;
                    $faq_existe = $faq_existe->select('SELECT id FROM {tabela_nome} WHERE pergunta_seo = "'.$faq->pergunta_seo.'" AND produto_id = "'.$faq->produto_id.'"');
                    if ($faq_existe AND ! is_null($faq_existe->id))
                    {
                        $faq->pergunta_seo = Funcoes::texto_para_seo($faq->pergunta).'-'.$cont++;
                    }
                } while ($faq_existe AND ! is_null($faq_existe->id));

                if (!isset($parametros->produtos_ids) OR count($parametros->produtos_ids)==0)
                {
                    $notificacao = new Notificacao('Você deve escolher pelo menos uma categoria de produto.');
                }

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($faq->insert())
					{
                        if ($this->relacionar_produtos($faq->id, $parametros->produtos_ids))
                        {
                            new Notificacao('Pergunta <strong>'.$faq->pergunta.'</strong> cadastrada com sucesso.', 'success', TRUE);
                            header('Location: '.SITE_URL.'/fatorcms/faq/listar');
                            exit;
                        }
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao salvar os dados da pergunta.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos campos marcados são obrigatórios.');
			}

            // Para retornar ao formulário com valores inseridos
            $view->adicionar('faq', $faq);
            $view->adicionar('faq_produtos', isset($parametros->produtos_ids) ? $this->transformar_post_produtos_ids($parametros->produtos_ids) : NULL);
		}
		else
		{
			$view->adicionar('faq', NULL);
            $view->adicionar('faq_produtos',NULL);
		}

		//-----

        // Lista de Produtos para o <select>
        $view->adicionar('produtos', $this->carregar_produtos());

		$view->adicionar('body_class', 'faq formulario');
        $view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);
        $view->adicionar('lateral_menu', array('item'=>'faq','link'=>'cadastrar pergunta'));

        $view->exibir();
	}


	/**
	 * Recebe os parâmetros do formulário e atualiza o registro no banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function atualizar($parametros)
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'faq-form.php'));
		$this->view_adicionar_obrigatorias($view);

		//-----

		if (isset($parametros->id) AND is_numeric($parametros->id))
		{
			$faq = new FatorCMS_Model_FAQ;
			$faq->carregar($parametros);

			// Campos especiais
			$faq->pergunta_seo = Funcoes::texto_para_seo($faq->pergunta);

			//-----

			// Log
			$faq->log_id = $this->cms_usuario_logado->id;
			$faq->log_data = date('Y-m-d H:i:s');

			//-----

			$faq->colunas_mysqli_escape();

			if ($faq->verificar_null())
			{
				// Validações ---

                // Verificamos se o pergunta_seo de antes é o igual ao novo. Se for diferente verificamos a possibilidade
                $faq_atual = new FatorCMS_Model_FAQ($faq->id);
                if ($faq->pergunta_seo != $faq_atual->pergunta_seo)
                {
                    // Verifica se o pergunta_seo está disponível (desde que não seja o do próprio registro), se não inicia o contador até achar um
                    $cont = 2;
                    do
                    {
                        $faq_existe = new FatorCMS_Model_FAQ;
                        $faq_existe = $faq_existe->select('SELECT id FROM {tabela_nome} WHERE pergunta_seo = "'.$faq->pergunta_seo.'" AND produto_id = "'.$faq->produto_id.'" AND id != '.$faq->id);
                        if ($faq_existe AND ! is_null($faq_existe->id))
                        {
                            $faq->pergunta_seo = Funcoes::texto_para_seo($faq->pergunta).'-'.$cont++;
                        }
                    } while ($faq_existe AND ! is_null($faq_existe->id));
                }

                if (!isset($parametros->produtos_ids) OR count($parametros->produtos_ids)==0)
                {
                    $notificacao = new Notificacao('Você deve escolher pelo menos uma categoria de produto.');
                }

				// Fim das validações ---

				if ( ! isset($notificacao))
				{
					if ($faq->update())
					{
                        if ( ! $this->relacionar_produtos($faq->id, $parametros->produtos_ids))
                        {
                            $notificacao = new Notificacao('Ocorreu um erro ao relacionar os produtos da pergunta.','error');
                        }
                        else
                        {
                            new Notificacao('Pergunta <strong>'.$faq->pergunta.'</strong> atualizada com sucesso.', 'success', TRUE);
                            header('Location: '.SITE_URL.'/fatorcms/faq/listar');
                            exit;
                        }
					}
					else
					{
						$notificacao = new Notificacao('Ocorreu um erro ao atualizar os dados da pergunta.');
					}
				}
				// Deixa passar porque já veio uma notificação
			}
			else
			{
				$notificacao = new Notificacao('Todos os campos marcados são obrigatórios.');
			}

            // Para retornar ao formulário com valores inseridos
            $view->adicionar('faq', $faq);
            $view->adicionar('faq_produtos', isset($parametros->produtos_ids) ? $this->transformar_post_produtos_ids($parametros->produtos_ids) : NULL);
        }
		else
		{
			$notificacao = new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention');
		}

		//-----

        // Lista de Produtos para o <select>
        $view->adicionar('produtos', $this->carregar_produtos());

		$view->adicionar('body_class', 'faq formulario');
		$view->adicionar('lateral_menu', array('item'=>'faq','link'=>''));
		$view->adicionar('notificacao', isset($notificacao) ? $notificacao : new Notificacao);

		$view->exibir();
	}


	/**
	 * Recebe o ID do registro e excluir ele do banco de dados
	 * @param  $parametros
	 * @return void
	 */
	public function excluir($parametros)
	{
		if (isset($parametros->_0) AND is_numeric($parametros->_0))
		{
			$faq = new FatorCMS_Model_FAQ($parametros->_0);

			if ($faq AND ! is_null($faq->id))
			{
				if ($faq->delete())
				{
                    // Excluímos os possíveis registros de permissão
                    $faq_produto = new FatorCMS_Model_FaqProduto();
                    $faq_produto->faq_id = $faq->id;
                    $faq_produto->delete();

					new Notificacao('A pergunta <strong>'.$faq->pergunta.'</strong> foi excluída com sucesso.', 'success', TRUE);
				}
				else
				{
					new Notificacao('Ocorreu um erro ao excluir a pergunta <strong>'.$faq->pergunta.'</strong>.', 'error', TRUE);
				}
            }
			else
			{
				new Notificacao('Nenhuma informação encontrada para esta pergunta.', 'error', TRUE);
			}
		}
		else
		{
			new Notificacao('Falha no recebimento dos dados. Tente novamente.', 'attention', 'error', TRUE);
		}

		header('Location: '.SITE_URL.'/fatorcms/faq/listar');
		exit;
	}



	/****************************** MÉTODOS EXTRAS ******************************/

    /**
     * Retorna todos os produtos cadastrados no banco, inclusive o produto "Tópico diversos", geralmente para completar <select>s
     * @return array|bool
     */
    protected function carregar_produtos()
    {
        $cms_modulo = new FatorCMS_Model_Produto;
        return $cms_modulo->select("
            SELECT 0 AS 'id','Tópicos diversos' AS 'nome','topicos-diversos' AS 'nome_seo'
            UNION ALL
            SELECT fd_produtos.id AS 'id', fd_produtos.nome AS 'nome', fd_produtos.nome_seo AS 'nome_seo'
            FROM fd_produtos
        ", TRUE);
    }


    /**
     * Retorna todos os produtos associados a determinada pergunta
     * @param $faq_id
     * @return array|bool
     */
    protected function carregar_pergunta_produtos($faq_id)
    {
        if ( ! is_null($faq_id) AND is_numeric($faq_id))
        {
            $produto = new FatorCMS_Model_FaqProduto();
            return $produto->select('SELECT * FROM {tabela_nome} WHERE faq_id = '.$faq_id, TRUE);
        }
        else
        {
            return FALSE;
        }
    }


    /**
     * Quando ocorre um problema no cadastro ou atualização, e recarregamos a página para reapresentar o formulário, temos que
     * pegar os produtos escolhidos e reapresentá-los
     * @param $produtos_ids
     * @return array|bool
     */
    protected function transformar_post_produtos_ids($produtos_ids)
    {
        if ($produtos_ids AND count($produtos_ids) > 0)
        {
            $faq_produtos = array();

            foreach ($produtos_ids as $produto_id)
            {
                $faq_produto = new FatorCMS_Model_FaqProduto();
                $faq_produto->produto_id = $produto_id;
                $faq_produtos[] = $faq_produto;
            }

            return $faq_produtos;
        }

        return FALSE;
    }


    /**
     * Cuida do relacionamento entre produtos e faq. Realiza a exclusão dos relacionamentos prévios e recria eles
     * @param $faq_id
     * @param $produtos_ids
     * @return bool Retorna TRUE acontecendo pelo menos uma inserção
     */
    protected function relacionar_produtos($faq_id, $produtos_ids)
    {
        $retorno = FALSE;

        // Apaga todos os registros relacionados à pergunta
        $faq_produto = new FatorCMS_Model_FaqProduto();
        $faq_produto->faq_id = $faq_id;

        if ($faq_produto->delete())
        {
            if (count($produtos_ids) > 0)
            {
                // Percorre cada ingrediente para fazer o relacionamento de novo, se for o caso
                foreach ($produtos_ids as $produto_id)
                {
                    $faq_produto->produto_id = $produto_id;

                    if ($faq_produto->insert())
                    {
                        // Tendo uma inserção pelo menos, já está ok
                        $retorno = TRUE;
                    }
                    else
                    {
                        $log = new Log('Erro ao relacionar produtos com pergunta. $faq_id='.$faq_id.'|$produto_id='.$produto_id);
                    }

                    // Limpamos o ID para a próxima inserção
                    $faq_produto->id = NULL;
                }
            }
            else
            {
                // Se não veio nenhum, pode ser apenas exclusão de todos, uma limpeza
                $retorno = TRUE;
            }
        }
        else
        {
            $log = new Log('Erro ao excluir os produtos da pergunta. $faq_id:'.$faq_id);
        }

        return $retorno;
    }

} // end class