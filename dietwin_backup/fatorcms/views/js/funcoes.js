$(document).ready(function()
{
	// Adiciona um prompt antes de cada "confirmação" no FatorCMS
	$('.item-confirmar').click(function()
	{
		return confirm($(this).attr('title')+'?');
	});

	// Todos os botões "voltar" fazem a mesma coisa
	$('.voltar').click(function()
	{
		history.go(-1);
	});
	
    //-----

	$.validator.setDefaults({
		errorElement: 'span',
		errorClass: 'input-notification-error',
		highlight: function(element, errorClass, validClass)
        {
            if($(element).is('textarea'))
            {
                // Coloca o erro na TABLE do TinyMCE
                $('.mceLayout').addClass(errorClass).removeClass(validClass);
            }
            else
            {
                // Os outros campos permanecem recebem a classe de erro diretamente
                $(element).addClass(errorClass).removeClass(validClass);
            }
        },
        unhighlight: function(element, errorClass, validClass)
        {
	        // O contrário da lógica acima
	        if($(element).is('textarea'))
            {
	            $('.mceLayout').removeClass(errorClass).addClass(validClass);
            }
            else
            {
                $(element).removeClass(errorClass).addClass(validClass);
            }
        },
        errorPlacement: function(error, element)
        {
	        if($(element).is('textarea'))
            {
                error.insertAfter(element.next());
            }
            else if($(element).is('input[type=radio]'))
            {
                error.insertAfter($(element).siblings(':last'));
            }
	        else
            {
                error.insertAfter(element);
            }
        }
	});

    //-----

	// Validações dos formulários de cadastro

    $("#cms_usuario_form").validate(
    {
	    rules:
	    {
            senha: {
                required: function(element) {return $(element).hasClass('required')},
                minlength: 6
            },
            eh_administrador: {required:true}
        },
        messages:
        {
            nome: 'Obrigatório',
            usuario: 'Obrigatório',
            senha: {
                required: 'Obrigatória',
                minlength: 'A senha deve ter no mínimo 6 caracteres'
            },
            eh_administrador: 'Obrigatório'
        }
    });


	//-----

	$('#novidade_form #resumo').bind('keyup change blur', function() {campo_contador($(this), $('#resumo_contador'), 600);});

    $("#novidade_form").validate(
    {
        rules:
        {
            data:{required:true, dateBR:true},
            eh_visivel:{required:true}
        },
        messages:
        {
            titulo:'Obrigatório',
            resumo:'Obrigatório',
            conteudo:'Obrigatório',
            data:{required:'Obrigatória',dateBR:'Escolha uma data válida.'},
	        eh_visivel:'Obrigatório'
        }
    });

	//-----

	$('#video_form #resumo').bind('keyup change blur', function() {campo_contador($(this), $('#resumo_contador'), 600);});
	$('#video_form #titulo').bind('keyup change blur', function() {campo_contador($(this), $('#titulo_contador'), 100);});

	$("#video_form").validate(
    {
        rules:
        {
            data:{required:true,dateBR:true}
        },
        messages:
        {
            produto_id:'Obrigatório',
            titulo:'Obrigatório',
            link:'Obrigatório',
            descricao:'Obrigatória',
            data:{required:'Obrigatória',dateBR:'Escolha uma data válida.'}
        }
    });

	//-----

	if ($('#galeria_form').length)
	{
		$("#galeria_form").validate(
	    {
	        rules:
	        {
	            data:{required:true, dateBR:true}
	        },
	        messages:
	        {
	            produto_id:'Obrigatório',
	            titulo:'Obrigatório',
	            descricao:'Obrigatória',
	            data:{required:'Obrigatória',dateBR:'Escolha uma data válida.'}
	        }
	    });

		$('a.apagar_foto').click(function(e)
		{
			e.preventDefault();

			if (confirm('Tem certeza que deseja excluir esta foto?'))
			{
				var $this = $(this);
				var $parent = $this.parent().parent();

				var id = $(this).parent().siblings('input[name=id]').val();

				$.post(
					SITE_URL+'/fatorcms/galerias/apagar-foto/'+id,
					function(data)
					{
						obj = eval('(' + data + ')');
						if (obj.resultado == 1)
						{
							$parent.hide('slow');
						}
					}
				);
			}
		});

		$('a.atualizar_foto').click(function(e)
		{
			e.preventDefault();

			var $this = $(this);
			var $parent = $this.parent().parent();

			var id = $(this).parent().siblings('input[name=id]').val();
			var input = $(this).parent().siblings('input[name=titulo]');
			var titulo = input.val();

			$.ajax({
				type: 'POST',
				url: SITE_URL+'/fatorcms/galerias/atualizar-foto/'+id,
				data: {'titulo' : titulo},
				dataType: 'json',
				beforeSend: function()
				{
					$this.fadeOut(500, function() {$this.text('Atualizando...').fadeIn(500).fadeOut(2000);});
				},
				success: function(data)
				{
					if (data.resultado == 1)
					{
						$this.fadeIn(2000, function()
						{
							$(input).animate({backgroundColor : '#D5FFCE'}, 2000);
							$(input).delay(2000).css('background', "url('/fatorcms/views/imagens/icones/tick_circle.png') no-repeat center right");
							$this.text('Atualizado!');
							$this.fadeOut(2000, function() {$this.text('Atualizar').fadeIn(250);});
							$(input).animate({backgroundColor : 'white'}, 2000);
							var t = setTimeout( function() { $(input).css('background', 'none'); $(input).css('background', 'none'); }, 10000);
						});
					}
				},
				error: function(data)
				{
					console.log(data);
				}
			});
		});

	} // if galeria_form lenght


    if ($('#arquivo_form').length)
    {
        $("#arquivo_form").validate({
            rule: {
                titulo: 'required',
                descricao: 'required'
            },
            messages: {
                titulo:'Obrigatório',
                descricao:'Obrigatória'
            },
            submitHandler: function(form) {

                var titulo = $('#titulo').val();
                var descricao = $('#descricao').val();
                var link = $('#link').val();
                var action = $(form).attr('action');
                var clientes = $('#as-values-clientes').val();

                var ok = true;
                if (action.indexOf('cadastrar') != -1)
                {
                    if (link == '' && $(".uploadifyQueueItem").length == 0)
                    {
                        adicionar_notificacao($('.notificacao_js'), 'error', 'Você precisa ou informar o nome do arquivo já enviado via ftp ou enviar um arquivo.');
                        $('html, body').animate({scrollTop:0}, 1);
                        ok = false;
                    }
                }

                if (ok)
                {
                    if (clientes == '' || clientes == ',')
                    {
                        adicionar_notificacao($('.notificacao_js'), 'error', 'Um arquivo deve estar associado a pelo menos 1 cliente.');
                        $('html, body').animate({scrollTop:0}, 1);
                        ok = false;
                    }
                }

                if (ok)
                {
                    $.post(
                        action,
                        $(form).serialize(),
                        function (retorno)
                        {
                            if (retorno.tipo != 'success')
                            {
                                adicionar_notificacao($('.notificacao_js'), retorno.tipo, retorno.mensagem);
                                $('html, body').animate({scrollTop:0}, 'slow');
                            }
                            else
                            {
                                var id = retorno.id;

                                if ($(".uploadifyQueueItem").length > 0)
                                {
                                    console.log('entrou aqui');
                                    $('#uploader_fotos').uploadifySettings( 'scriptData' , { 'arquivo_id' : id } ) ;
                                    $('#uploader_fotos').uploadifyUpload();
                                }
                                else
                                {
                                    window.location.href = SITE_URL+'/fatorcms/arquivos/listar';
                                }
                            }
                        },
                        'json'
                    );
                }

            }
        });

        // autoSuggest para tags
        if ($('#cliente_id').length)
        {
            $('#cliente_id').autoSuggest(
                SITE_URL+'/fatorcms/arquivos/listar-clientes',
                {
                    startText:'',
                    emptyText:'Nenhum cliente encontrado',
                    selectedItemProp:'nome',
                    selectedValuesProp:'id',
                    searchObjProps:'nome',
                    asHtmlID:'clientes',
                    neverSubmit:true,
                    resultsHighlight: false,
                    preFill:clientes.itens
                }
            );
        }

        $('.as-input').blur(function(e){
            $(this).val('');
        });

        $('.opcoes').change(function(e){
            $('.div-opcoes').toggle();
            if ($(this).val() == 'link')
            {
                $('#uploader_fotos').uploadifyClearQueue();
            }
            else
            {
                $('#link').val('');
            }
        });

    } // if arquivo_form lenght

	//-----

    //<editor-fold desc="Formulário CLiente">
    $('#rg').mask('9?9999999999', {placeholder: ''});
	$('#cpf').mask('999.999.999-99', {placeholder: ' '});
	$('#cnpj').mask('99.999.999/9999-99', {placeholder: ' '});
	$('#telefone_principal, #telefone_extra').mask('(99) 9999-9999', {placeholder: ' '});

    mostra = false;
    $("#aleatoria").click(function(){
        $("#senha").toggleClass("required");
        if(mostra == false){
            $("#senha").hide();
            mostra = true;
        }else if(mostra == true){
            $("#senha").show();
            mostra = false;
        }
    });

    $("#cliente_form").validate(
    {
        rules:
        {
            email: {required:true, email:true},
            email_secundario: {required:false, email:true},
            ativo:{required:true}
        },
        messages:
        {
            nome:'Obrigatório',
            email:{required:'Obrigatório', email:'Endereço inválido'},
            email_secundario:{required:'Obrigatório', email:'Endereço inválido'},
            senha:'Obrigatória',
            rg:'Obrigatória',
            cpf:'Obrigatório',
            telefone_principal:'Obrigatório',
            profissao:'Obrigatória'
        }
    });

    $("#prospect_form").validate(
        {
            rules:
            {
                email: {required:true, email:true},
                email_secundario: {required:false, email:true}
            },
            messages:
            {
                nome:'Obrigatório',
                email:{required:'Obrigatório', email:'Endereço inválido'},
                email_secundario:{email:'Endereço inválido'}
            }
        });

    //</editor-fold>

    //<editor-fold desc="Formulário Endereço Cliente">

    $('#numero').mask('9?99999', {placeholder: ''});

    $('#cliente_endereco_form').validate(
    {
        messages:
        {
            tipo:'Obrigatório',
            endereco:'Obrigatório',
            numero:'Obrigatório',
            bairro:'Obrigatório',
            estado_id:'Obrigatório',
            cidade_id:'Obrigatória',
            cep:'Obrigatório'
        },
	    submitHandler: function(form)
	    {
            var action = $(form).attr('action');
		    $.post(
				action,
			    $(form).serialize(),
			    function (retorno)
			    {
				    if (retorno.tipo == 'success')
				    {
                        if (action.indexOf('cliente') != -1)
                        {
                            window.location.href = SITE_URL+'/fatorcms/clientes/editar/'+$(form).find('input[name=cliente_id]').val()+'/aba/2';
                        }
                        else
                        {
                            window.location.href = SITE_URL+'/fatorcms/prospects/editar/'+$(form).find('input[name=cliente_id]').val()+'/aba/2';
                        }
				    }
				    else
				    {
					    adicionar_notificacao($('.tab-enderecos .notificacao_js'), retorno.tipo, retorno.mensagem);
				    }
			    },
				'json'
		    );
	    }
    });

	$('.endereco_editar').click(function(e)
	{
		e.preventDefault();

		$('.tab-enderecos .notificacao_js').html('<div class="notification information png_bg"><div>Carregando...</div></div>').css({display:'block','opacity':1});
		var url = $(this).attr('href');
		// Só para dar um tempinho de aparecer a informação acima
		setTimeout(function()
		{
			$.get(
				url,
				{},
				function (retorno)
				{
					$('.tab-enderecos .notificacao_js').fadeTo(400, 0, function() { $(this).slideUp(400); }).html('');

					if (retorno.tipo == 'success')
					{
                        // Se existir, removemos o hidden id
                        if ($('#cliente_endereco_form input[name=id]').length) { $('#cliente_endereco_form input[name=id]').remove(); }
                        // Se existir, removemos o botão de Cancelar
                        if ($('#cliente_endereco_form fieldset:last p input[type=button]').length) { $('#cliente_endereco_form fieldset:last p input[type=button]').remove(); }

						$('#cliente_id').val(retorno.cliente_endereco.cliente_id);
						$('#tipo').val(retorno.cliente_endereco.tipo).focus();
						$('#endereco').val(retorno.cliente_endereco.endereco);
						$('#numero').val(retorno.cliente_endereco.numero);
						$('#complemento').val(retorno.cliente_endereco.complemento);
						$('#bairro').val(retorno.cliente_endereco.bairro);
						$('#estado_id').val(retorno.cliente_endereco.estado_id);
						// Atualizamos o valor da variável
						cidade_id = retorno.cliente_endereco.cidade_id;
						$('#estado_id').change();
						$('#cep').val(retorno.cliente_endereco.cep.toString().substr(0, 5)+'-'+retorno.cliente_endereco.cep.toString().substr(5));
						$('#observacao').val(retorno.cliente_endereco.observacao);

                        if (url.indexOf('cliente') != -1)
                        {
                            $('#cliente_endereco_form').attr('action', SITE_URL+'/fatorcms/clientes/endereco-atualizar');
                        }
                        else
                        {
                            $('#cliente_endereco_form').attr('action', SITE_URL+'/fatorcms/prospects/endereco-atualizar');
                        }

						$('#cliente_endereco_form fieldset').append($('<input>').attr('name', 'id').attr('type', 'hidden').attr('value', retorno.cliente_endereco.id));
						$('#cliente_endereco_form input.button').val('Atualizar');

                        $('#cliente_endereco_form fieldset p:last').append($('<input>').attr('type', 'button').addClass('button').attr('onclick', 'window.location.reload()').attr('value', 'Cancelar'));
					}
					else
					{
						adicionar_notificacao($('.tab-enderecos .notificacao_js'), retorno.tipo, retorno.mensagem);
					}
				},
				'json'
			);
		}, 500);
	});

	// Vai ser atualizada no carregamento do endereço, e então usada na função abaixo para selecionar a cidade
	var cidade_id = null;

	$('#estado_id').bind('change', {cidade_id: cidade_id}, function(event)
	{
		var estado_id = $(this).val();
		if (estado_id.length > 0 && ! isNaN(estado_id))
		{
			$('#cidade_id').html('').append($('<option></option>').attr('value', '').text('Carregando...'));
			$.post(
				SITE_URL+'/fatorcms/clientes/buscar-cidades-estado',
				{estado_id : estado_id},
				function (data)
				{
					$.each(data, function(chave, valor)
					{
						$('#cidade_id').append($('<option></option>').attr('value', chave).text(valor));
					});
					$('#cidade_id option').eq(0).remove();

					if (cidade_id)
					{
						$('#cidade_id').val(cidade_id);
					}
				},
				'json'
			)
		}
		$('#cidade_id').html('').append($('<option></option>').attr('value', '').text('Selecione um estado acima'));
	});
    //</editor-fold>

    //<editor-fold desc="Formulário Compra Cliente">
    $('#boleto_parcelas, #produto_licencas').mask('9?9', {placeholder: ''});
	$('#entrega_valor, #valor_total').priceFormat({
	    prefix: 'R$ ',
	    centsSeparator: ',',
	    thousandsSeparator: '.'
	});

	$('#cliente_compra_form').validate(
    {
        messages:
        {
            data:'Obrigatória',
            produto:'Obrigatório',
            pagamento_metodo:'Obrigatório',
            situacao:'Obrigatória',
            cliente_endereco_id:'Obrigatório',
            entrega_forma:'Obrigatória',
            entrega_valor:'Obrigatório'
        },
	    submitHandler: function(form)
	    {
		    $.post(
				$(form).attr('action'),
			    $(form).serialize(),
			    function (retorno)
			    {
				    if (retorno.tipo == 'success')
				    {
					    window.location.href = SITE_URL+'/fatorcms/clientes/editar/'+$(form).find('input[name=cliente_id]').val()+'/aba/3';
				    }
				    else
				    {
					    adicionar_notificacao($('.tab-compras .notificacao_js'), retorno.tipo, retorno.mensagem);
				    }
			    },
				'json'
		    );
	    }
    });

	$('.compra_editar').click(function(e)
	{
		e.preventDefault();

		$('.tab-compras .notificacao_js').html('<div class="notification information png_bg"><div>Carregando...</div></div>').css({display:'block','opacity':1});
		var url = $(this).attr('href');
		// Só para dar um tempinho de aparecer a informação acima
		setTimeout(function()
		{
			$.get(
				url,
				{},
				function (retorno)
				{
					$('.tab-compras .notificacao_js').fadeTo(400, 0, function() { $(this).slideUp(400); }).html('');

					if (retorno.tipo == 'success')
					{
						// Se existir, removemos o hidden id
						if ($('#cliente_compra_form input[name=id]').length) { $('#cliente_compra_form input[name=id]').remove(); }
                        // Se existir, removemos o botão de Cancelar
                        if ($('#cliente_compra_form fieldset:last p input[type=button]').length) { $('#cliente_compra_form fieldset:last p input[type=button]').remove(); }

						var data_array = retorno.cliente_compra.data.substr(0, 10).split('-');
						$('#data').val(data_array[2]+'/'+data_array[1]+'/'+data_array[0]);
						$('#produto_atual').html(
							retorno.cliente_compra_produto.produto_nome +
							' - versão: '+ retorno.cliente_compra_produto.versao
						).removeClass('esconder');
						$('#produto_atual_ajuda').removeClass('esconder');
						$('#produto_id').removeClass('required').prepend($('<option></option>').attr('value', '').text('Escolha um produto para substituição')).val('');

						$('#produto_licencas').val((retorno.cliente_compra_produto.licenca_quantidade > 0 ? retorno.cliente_compra_produto.licenca_quantidade : ''));
						$('#pagamento_metodo').val(retorno.cliente_compra.pagamento_metodo);
						$('#boleto_parcelas').val(retorno.cliente_compra.boleto_parcelas);
                        $('#boleto_vencimento').val(retorno.cliente_compra.boleto_vencimento);
						$('#situacao').val(retorno.cliente_compra.situacao);

						$('#endereco_atual').html(
							retorno.cliente_compra_endereco.endereco +
							', '+ retorno.cliente_compra_endereco.numero +
							(retorno.cliente_compra_endereco.complemento != undefined ? ' - '+retorno.cliente_compra_endereco.complemento : '') +
							' - ' + retorno.cliente_compra_endereco.bairro +
							' - ' + retorno.cliente_compra_endereco.cidade_nome +
							' / ' + retorno.cliente_compra_endereco.estado_sigla
						).removeClass('esconder');
						$('#endereco_atual_ajuda').removeClass('esconder');
						$('#cliente_endereco_id').removeClass('required').prepend($('<option></option>').attr('value', '').text('Escolha um endereço para substituição')).val('');

						$('#entrega_forma').val(retorno.cliente_compra.entrega_forma);
						$('#entrega_valor').val(formatar_numero(retorno.cliente_compra.entrega_valor, 2, '.', ',', 'R$ ', '', '', ''));

                        if (retorno.cliente_compra_cupom.id)
                        {
                            $('#cupom_atual').html(
                                retorno.cliente_compra_cupom.cupom_codigo +
                                    ' - '+ retorno.cliente_compra_cupom.produto_nome +
                                    ' - ' + formatar_numero(retorno.cliente_compra_cupom.valor, 2, '.', ',', 'R$ ', '', '', '')
                            ).removeClass('esconder');
                        }
                        else
                        {
                            $('#cupom_atual').html('Nenhum cupom foi utilizado nesta compra').removeClass('esconder');
                        }
                        $('#cupom_atual_ajuda').removeClass('esconder');
                        $('#cliente_cupom_id').removeClass('required').prepend($('<option></option>').attr('value', '').text('Escolha um cupom para substituição')).val('');

                        $('#onde_foi_adquirido').val(retorno.cliente_compra.onde_foi_adquirido);
                        $('#informacao_adicional').val(retorno.cliente_compra.informacao_adicional);

						$('#cliente_compra_form').attr('action', SITE_URL+'/fatorcms/clientes/compra-atualizar');
						$('#cliente_compra_form fieldset:first').append($('<input>').attr('name', 'id').attr('type', 'hidden').attr('value', retorno.cliente_compra.id));
						$('#cliente_compra_form input.button').val('Atualizar');

                        $('#cliente_compra_form fieldset:last p').append($('<input>').attr('type', 'button').addClass('button').attr('onclick', 'window.location.reload()').attr('value', 'Cancelar'));
					}
					else
					{
						adicionar_notificacao($('.tab-compras .notificacao_js'), retorno.tipo, retorno.mensagem);
					}
				},
				'json'
			);
		}, 500);
	});

    //</editor-fold>

    $('#cliente_plano_assistencia_form').validate(
        {
            messages:
            {
                data:'Obrigatória',
                fim:'Obrigatória',
                inicio:'Obrigatória'
            },
            submitHandler: function(form)
            {
                $.post(
                    $(form).attr('action'),
                    $(form).serialize(),
                    function (retorno)
                    {
                        if (retorno.tipo == 'success')
                        {
                            window.location.href = SITE_URL+'/fatorcms/clientes/editar/'+$(form).find('input[name=cliente_id]').val()+'/aba/5';
                        }
                        else
                        {
                            adicionar_notificacao($('.tab-compras .notificacao_js'), retorno.tipo, retorno.mensagem);
                        }
                    },
                    'json'
                );
            }
        });

    $('#cliente_recorrencia_form').validate(
        {
            submitHandler: function(form)
            {
                $.post(
                    $(form).attr('action'),
                    $(form).serialize(),
                    function (retorno)
                    {
                        if (retorno.tipo == 'success')
                        {
                            window.location.href = SITE_URL+'/fatorcms/clientes/editar/'+$(form).find('input[name=cliente_id]').val()+'/aba/6';
                        }
                        else
                        {
                            adicionar_notificacao($('.tab-recorrencia .notificacao_js'), retorno.tipo, retorno.mensagem);
                        }
                    },
                    'json'
                );
            }
        });

    //<editor-fold desc="Formulário Interação Cliente">

    $('table.interacoes table tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table rows

    $('#cliente_interacao_form').validate(
        {
            messages:
            {
                tipo:'Obrigatório',
                titulo:'Obrigatório',
                conteudo:'Obrigatório'
            },
            submitHandler: function(form)
            {
                var action = $(form).attr('action');
                $.post(
                    $(form).attr('action'),
                    $(form).serialize(),
                    function (retorno)
                    {
                        if (retorno.tipo == 'success')
                        {
                            if (action.indexOf('cliente') != -1)
                            {
                                window.location.href = SITE_URL+'/fatorcms/clientes/editar/'+$(form).find('input[name=cliente_id]').val()+'/aba/4';
                            }
                            else
                            {
                                window.location.href = SITE_URL+'/fatorcms/prospects/editar/'+$(form).find('input[name=cliente_id]').val()+'/aba/4';
                            }
                        }
                        else
                        {
                            adicionar_notificacao($('.tab-interacoes .notificacao_js'), retorno.tipo, retorno.mensagem);
                        }
                    },
                    'json'
                );
            }
        });

    $('.interacao_editar').click(function(e)
    {
        e.preventDefault();

        $('.tab-interacoes .notificacao_js').html('<div class="notification information png_bg"><div>Carregando...</div></div>').css({display:'block','opacity':1});
        var url = $(this).attr('href');
        $('html,body').animate({scrollTop:0},1000);
        // Só para dar um tempinho de aparecer a informação acima
        setTimeout(function()
        {
            $.get(
                url,
                {},
                function (retorno)
                {
                    if (retorno.tipo != 'error')
                    {
                        $('.tab-interacoes .notificacao_js').fadeTo(400, 0, function() { $(this).slideUp(400); }).html('');
                    }

                    if (retorno.tipo == 'success')
                    {
                        // Se existir, removemos o hidden id
                        if ($('#cliente_interacao_form input[name=id]').length) { $('#cliente_interacao_form input[name=id]').remove(); }
                        // Se existir, removemos o botão de Cancelar
                        if ($('#cliente_interacao_form fieldset:last p input[type=button]').length) { $('#cliente_interacao_form fieldset:last p input[type=button]').remove(); }

                        $('#interacao_tipo').val(retorno.cliente_interacao.tipo);
                        $('#titulo').val(retorno.cliente_interacao.titulo);
                        $('#conteudo').val(retorno.cliente_interacao.conteudo);
                        tinyMCE.editors.conteudo.load();

                        if (url.indexOf('cliente') != -1)
                        {
                            $('#cliente_interacao_form').attr('action', SITE_URL+'/fatorcms/clientes/interacao-atualizar');
                        }
                        else
                        {
                            $('#cliente_interacao_form').attr('action', SITE_URL+'/fatorcms/prospects/interacao-atualizar');
                        }

                        $('#cliente_interacao_form fieldset:first').append($('<input>').attr('name', 'id').attr('type', 'hidden').attr('value', retorno.cliente_interacao.id));
                        $('#cliente_interacao_form input.button').val('Atualizar');

                        $('#cliente_interacao_form fieldset p:last').append($('<input>').attr('type', 'button').addClass('button').attr('onclick', 'window.location.reload()').attr('value', 'Cancelar'));
                    }
                    else
                    {
                        adicionar_notificacao($('.tab-interacoes .notificacao_js'), retorno.tipo, retorno.mensagem);
                    }
                },
                'json'
            );
        }, 500);
    });
    //</editor-fold>

	//-----

    $('#valor, #licenca_valor').priceFormat({
        prefix: 'R$ ',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });


    $("#produto_form").validate(
    {
        messages:
        {
            nome:'Obrigatório',
            valor:'Obrigatório',
            versao:'Obrigatória',
            licenca_valor:'Obrigatório'
        }
    });

	//-----

    $('#faq_form #pergunta').bind('keyup change blur', function() {campo_contador($(this), $('#pergunta_contador'), 200);});

    $("#faq_form").validate(
    {
        messages:
        {
            'produtos_ids[]':'Obrigatória',
            pergunta:'Obrigatória',
            resposta:'Obrigatória'
        }
    });

	//-----

    $('#valor').priceFormat({
        prefix: 'R$ ',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $("#cupom_form").validate(
    {
        messages:
        {
            codigo:'Obrigatório',
            valor:'Obrigatório',
            ativo:'Obrigatório'
        }
    });

	//-----

	$("#data, #data_inicial, #data_final, #validade_inicio, #validade_fim, #plano_fim, #plano_inicio, #data_compra").mask('99/99/9999', {placeholder: ' '}).datepicker({
		dateFormat: 'dd/mm/yy',
		showButtonPanel: true,
		changeMonth: true,
		changeYear: true,
		showOn: 'both',
		buttonImage: SITE_BASE+'/fatorcms/views/imagens/icones/calendar.png',
		buttonImageOnly: true,
        onClose: function(texto, input) {$('#'+input.id).valid();}
	});

    // autoSuggest para tags
	if ($('#tags').length)
	{
		$('#tags').autoSuggest(
			SITE_URL+'/fatorcms/tags/ajax-buscar',
			{
				startText:'',
				emptyText:'Nenhuma tag encontrada',
				selectedItemProp:'nome',
				selectedValuesProp:'nome',
				searchObjProps:'nome',
				asHtmlID:'tags',
				neverSubmit:true,
				preFill:tags.itens
			}
		);
	}

	//-----

	// Funcionamento dos botões de cadastro nas listagens

    $('.novidade.botao-cadastrar').click(function() { document.location.href = SITE_URL+'/fatorcms/novidades/cadastrar'; });
    $('.novidade-categoria.botao-cadastrar').click(function() { document.location.href = SITE_URL+'/fatorcms/novidades/categorias-cadastrar'; });
	$('.video.botao-cadastrar').click(function() { document.location.href = SITE_URL+'/fatorcms/videos/cadastrar'; });
	$('.galer.botao-cadastrar').click(function() { document.location.href = SITE_URL+'/fatorcms/galerias/cadastrar'; });
    $('.cliente.botao-cadastrar').click(function() { document.location.href = SITE_URL+'/fatorcms/clientes/cadastrar'; });
    $('.prospect.botao-cadastrar').click(function() { document.location.href = SITE_URL+'/fatorcms/prospects/cadastrar'; });
	$('.produto.botao-cadastrar').click(function() { document.location.href = SITE_URL+'/fatorcms/produtos/cadastrar'; });
	$('.faq.botao-cadastrar').click(function() { document.location.href = SITE_URL+'/fatorcms/faq/cadastrar'; });
	$('.cupom.botao-cadastrar').click(function() { document.location.href = SITE_URL+'/fatorcms/cupons/cadastrar'; });
    $('.arquivo.botao-cadastrar').click(function() { document.location.href = SITE_URL+'/fatorcms/arquivos/cadastrar'; });

    //-----

    $('#cep').mask('99999-999', {placeholder: ' '});
    $('#telefone').mask('(99) 9999-9999', {placeholder: ' '});

    /* $( "#clientes" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: SITE_URL+'/fatorcms/cupons/listar-clientes',
                dataType: "json",
                data: {
                    maximo: 15,
                    iniciais: request.term
                },
                success: function( data ) {
                    $('#cliente_id').val()
                    response( $.map( data.clientes, function( cliente ) {
                        return {
                            label: cliente.nome,
                            value: cliente.id
                        }
                    }));
                }
            });
        },

        select: function(event, ui) {
            $('#cliente_id').val(ui.item.value);
            $(this).val(ui.item.label);
            return false;
        },
        minLength: 2,
        open: function() {
            $('#cliente_id').val('');
            $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
            $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });

    $('#clientes').keyup(function(e){
        $('#cliente_id').val('');
    });

    $('#clientes').blur(function(e){
        if ($('#cliente_id').val() == '')
        {
            $(this).val('');
        }
    }); */

    // autoSuggest para tags
    if ($('#cliente_id').length)
    {
        $('#cliente_id').autoSuggest(
            SITE_URL+'/fatorcms/cupons/listar-clientes',
            {
                startText:'',
                emptyText:'Nenhum cliente encontrado',
                selectedItemProp:'nome',
                selectedValuesProp:'id',
                searchObjProps:'nome',
                asHtmlID:'clientes',
                selectionLimit: 1,
                neverSubmit:true,
                resultsHighlight: false,
                preFill:clientes.itens
            }
        );
    }

    $('.as-input').blur(function(e){
        $(this).val('');
    });
});

//-----



//-----

function campo_contador(campo, contador, limite)
{
	var total = parseInt($(campo).val().length);
	if (total > limite)
	{
		$(campo).val($(campo).val().substr(0,limite));
		total = limite;
	}
	$(contador).html(limite-total);
}

//-----

jQuery.validator.addMethod('dateBR', function(value, element)
{
	//contando chars
	if(value.length!=10) return false;
	// verificando data
    var data 	= value;
    var dia 	= data.substr(0,2);
    var barra1	= data.substr(2,1);
    var mes 	= data.substr(3,2);
    var barra2	= data.substr(5,1);
    var ano 	= data.substr(6,4);
	if(data.length!=10||barra1!="/"||barra2!="/"||isNaN(dia)||isNaN(mes)||isNaN(ano)||dia>31||mes>12)return false;
	if((mes==4||mes==6||mes==9||mes==11)&&dia==31)return false;
	if(mes==2 && (dia>29||(dia==29&&ano%4!=0)))return false;
	if(ano < 1900)return false;
	return true;
}, 'Informe uma data válida.');  // Mensagem padrão

//-----

function adicionar_notificacao(elemento, tipo, mensagem)
{
	$(elemento).html(
        '<div class="notification '+tipo+' png_bg">' +
            '<a href="#" class="close"><img src="'+SITE_BASE+'/fatorcms/views/imagens/icones/cross_grey_small.png" title="Fechar esta notificação" alt="fechar" /></a>' +
        '<div>'+mensagem+'</div></div>'
    );
}

//-----

// number formatting function
// copyright Stephen Chapman 24th March 2006, 10th February 2007
// permission to use this function is granted provided
// that this copyright notice is retained intact
//http://ntt.cc/2008/04/25/6-very-basic-but-very-useful-javascript-number-format-functions-for-web-developers.html
function formatar_numero(num, dec, thou, pnt, curr1, curr2, n1, n2)
{
    var x = Math.round(num * Math.pow(10,dec));
    if (x >= 0) n1=n2='';

    var y = (''+Math.abs(x)).split('');
    var z = y.length - dec;

    if (z<0) z--;

    for(var i = z; i < 0; i++)
        y.unshift('0');

    y.splice(z, 0, pnt);
    if(y[0] == pnt) y.unshift('0');

    while (z > 3)
    {
        z-=3;
        y.splice(z,0,thou);
    }

    var r = curr1+n1+y.join('')+n2+curr2;
    return r;
}
