<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property string pergunta
 * @property string pergunta_seo
 * @property string resposta
 * @property int log_id
 * @property string log_data
 */

class FatorCMS_Model_FAQ extends FatorCMS_Model_Padrao
{
	protected $tabela_nome = 'faqs';

} // end class