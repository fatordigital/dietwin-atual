<?php require_once 'includes/head.php' ?>
<body>
<?php require_once 'includes/nav.php' ?>

<section class="conteudo cinza">
  <div class="container">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <h1 class="interna">Carrinho de compras</h1>
      <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ut mi eget purus semper vestibulum sit amet nec justo. Vivamus non elit non est faucibus hendrerit. Fusce at nulla ut sapien tempus fringilla</p>-->
    </div>
    <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
      <img src="<?php echo SITE_URL ?>/views/imagens/caixas-dietwin-cadastre-se.jpg" alt="dietWin Plus e dietWin Tradicional">
    </div>
    <section class="formulario">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php if(isset($_SESSION['erro_compra'])){ ?>
        <div style="background-color:#C43131; padding:15px; color:#fff; margin:0 0 20px 0;">
            <?php echo $_SESSION['erro_compra']; ?>
        </div>
        <?php } unset($_SESSION['erro_compra']); ?>

        <?php if($frete_cep_valido=='yes'){ ?>

        <?php if(isset($_SESSION['cliente_id'])){ ?>
        <div class="titulo-formulario">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>Como deseja receber sua cópia do Dietwin?</h2>
          </div>
        </div>
        <div class="endereco-entrega">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <?php foreach($frete as $f): ?>
              <?php
              if($f[0]['valor'] != 0){
                $valor = ' (<span class="text-warning">+ R$ ' . number_format($f[0]['valor'], 2, ',', '.') . '</span>)';
              } else{
                $valor = ' (<span class="text-success">Sem Custo</span>)';
              }
              $prazo = '';
              switch ($f['servico']) {
                case 'Sedex':
                  $prazo .= '3 a 5 dias úteis';
                  break;
                case 'PAC';
                  $prazo .= '3 a 8 dias úteis';
                  break;
                case 'Download':
                  $prazo .= 'Até 1 dia útil';
                  break;
                default:
                  break;
              }
              if($f['erro']==10){
                $prazo = ($f[0]['prazo_entrega']+2).' a '.($f[0]['prazo_entrega']+4).' dias úteis'; 
              }
              ?>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <label><input type="radio" name="check-frete" value="<?php echo $f['servico'] ?>" data-valor="<?php echo $f[0]['valor'] ?>"> Por <?php echo $f['servico'] . $valor ?><br><small class="alinha-small"><?php echo $prazo ?></small></label><br />
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="titulo-formulario">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>Dados de sua compra</h2>
          </div>
        </div>
        <div class="endereco-entrega">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="lista-produtos">
                <ol>
                  <li><?php echo $produto_nome; ?></li>
                  <?php if(isset($cupom_valor)){ ?>
                  <li>Cupom de Desconto</li>
                  <?php } ?>
                  <li class="mostra-frete">Frete</li>
                </ol>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
              <div class="listagem-valores">
                <ul>
                  <li class="valor-produto" data-valor-produto="<?php echo $produto_valor ?>">R$ <?php echo number_format($produto_valor, 2, ',', '.'); ?></li>
                  <?php if(isset($cupom_valor)){ ?>
                  <li class="valor-cupom" data-valor-cupom="<?php echo $cupom_valor; ?>">R$ - <?php echo str_replace('.00','',$cupom_valor); ?>,00</li>
                  <?php } ?>
                  <li class="mostra-frete valor"></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="formulario-cadastro total-venda">
          <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 text-right">
              Total de sua compra:
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-right">
              <div class="preco">
                <?php
                $desconto = 0;
                if(isset($cupom_valor)){
                  $desconto = $cupom_valor;
                }
                ?>
                <span class="moeda">R$</span><span class="reais"><?php echo number_format($produto_valor-$desconto, 2, ',', '.'); ?></span><span class="centavos"></span>
              </div>
            </div>
          </div>
        </div>
        <?php if(!isset($_SESSION['cliente_id'])){ ?>
        <div class="endereco-entrega login-cadastro" id="!">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <p class="detalhe-finalizar-compra">Para realizar a sua compra na loja virtual dietWIn, é importante que você seja um usuário cadastrado no site. O processo de cadastro é simples e rápido e garante a segurança na sua compra e na entrega de seus pedidos!</p>

              <?php if(isset($_SESSION['login_necessario_compra'])){ ?>
              <div style="background-color: #638959; padding:10px; color:#fff; text-align:center; margin:10px 0 20px 0; font-size:13px;">
                Você precisa estar logado para continuar a compra. Se você ainda não possui sua conta, cadastre-se.
              </div>
              <?php unset($_SESSION['login_necessario_compra']); } ?>

            </div>
            <div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-12 col-xs-12">
              <h3>Já sou cadastrado</h3>
              <form action="<?php echo SITE_URL ?>/login" id="login">
                <label for="email">E-mail:</label>
                <input type="text" id="email" name="email">
                <label for="senha">Senha:</label>
                <input type="password" id="senha" name="senha">
                <input type="hidden" name="produto_seo" value="<?php echo $produto_nome_seo; ?>"/>
                <a href="<?php echo SITE_URL ?>/esqueci-minha-senha" class="pull-left" style="margin-top:8px">Esqueci minha senha</a>
                <input type="submit" value="Entrar" class="processo-compra-submit">

                <?php if(isset($_SESSION['login_negado']) OR isset($_SESSION['login_erro_campos'])){ ?>
                <div style="width:240px; background-color: #B23535; padding:10px; float:left; color:#fff; font-size:12px;">
                  <?php if(isset($_SESSION['login_negado'])){ echo "Usuário ou senha inválidos."; unset($_SESSION['login_negado']);} ?>
                  <?php if(isset($_SESSION['login_erro_campos'])){ echo "Preencha os campos corretamente."; unset($_SESSION['login_erro_campos']);} ?>
                </div>
                <?php } ?>

              </form>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 text-center">
              <h3>Quero me cadastrar</h3>
              <a href="<?php echo SITE_URL ?>/cliente/cadastro" title="Quero me cadastrar">
                <img src="<?php echo SITE_URL ?>/views/imagens/quero-me-cadastrar.png" alt="Quero me cadastrar" class="quero-me-cadastrar">
              </a>
            </div>
          </div>
        </div>
        <?php }else{ $_SESSION['processo_compra'] = TRUE; ?>
        <div class="titulo-formulario">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>Como você deseja realizar o pagamento?</h2>
          </div>
        </div>
        <form action="<?php echo SITE_URL ?>/finalizar-compra" method="post">
        <input type="hidden" name="frete" />
        <input type="hidden" name="servico" />
        <div class="endereco-entrega forma-pagamento">
          <div class="row">
            <div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 col-sm-12 col-xs-12">
              <div class="forma boleto">
                <img src="<?php echo SITE_URL ?>/views/imagens/boleto-bancario-ico.gif" alt="Boleto Bancário parcelado diretamente com o dietWin">
                Boleto Bancário diretamente com o dietWin
              </div>
              <div class="opcoes">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-xs-12 col-md-12" style="display: none;">
                    <label for="quantidade-parcelas">Quantidade de Parcelas</label>
                    <select name="quantidade_parcelas" id="quantidade-parcelas">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                    </select>
                  </div>
                  <div class="col-lg-6 col-md-6 col-xs-12 col-md-12">
                    <label for="data-vencimento">Data de vencimento</label>
                    <select name="data_vencimento" id="data-vencimento">
                      <option value="05">Dia 05</option>
                      <option value="15">Dia 15</option>
                      <option value="25">Dia 25</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
              <div class="forma pagseguro">
                <img src="<?php echo SITE_URL ?>/views/imagens/pag-seguro-ico.gif" alt="Pagamento à vista ou parcelado por cartão de crédito via PagSeguro"> Pagamento à vista ou parcelado por cartão de crédito via PagSeguro
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="formulario-cadastro">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs text-center">
              <img src="<?php echo SITE_URL ?>/views/imagens/compra-segura-pagseguro.png" alt="Compra 100% segura - PagSeguro">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs text-center">
              <img src="<?php echo SITE_URL ?>/views/imagens/formas-de-pagamento.png" alt="Compra 100% segura - PagSeguro" class="cartoes-compra">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-4 hidden-xs pull-right">
                <input type="submit" value="Finalizar compra" id="" class="finalizar-compra processo-compra-submit" <?php if(isset($_SESSION['cliente_id'])){ echo ''; } ?>>
                <input type="hidden" name="metodo_pagamento" id="metodo_pagamento" value=""/>
                <input type="hidden" name="finalizar_compra" value="true" />
                <input type="hidden" name="produto_seo" value="<?php echo $produto_nome_seo; ?>"/>
            </div>
          </div>
        </div>
        </form>

        <?php }else{ //End - $frete_cep_valido ?>
        
        <div class="titulo-formulario">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>Complete o seu cadastro!</h2>
          </div>
        </div>
        <div class="endereco-entrega">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <p>
                Ocorreu um problema ao continuarmos sua compra, seu CEP parece ser inválido ou inexistente. <br>
                Por favor, atualize seu cadastro inserindo um CEP válido <strong><a href="<?php echo SITE_URL ?>/cliente/editar-cadastro/<?php echo $cliente_id; ?>">Clicando Aqui</a></strong>
              </p>
            </div>
          </div>
        </div>
        <?php } ?>


      </div>
    </section>
  </div>
</section>



<?php require_once 'includes/footer.php' ?>
</body>
</html>
