<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property string titulo
 * @property string titulo_seo
 * @property string resumo
 * @property string conteudo
 * @property datetime data
 * @property string imagem
 * @property int eh_visivel
 * @property int log_id
 * @property datetime log_data
 */

class FatorCMS_Model_Novidade extends FatorCMS_Model_Padrao
{
	protected $tabela_nome = 'novidades';

} // end class