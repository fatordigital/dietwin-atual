<?php include 'includes/cabecalho.php'; ?>

<body class="area-cliente softwares">

<?php include 'includes/topo.php'; ?>

<div class="titulo-principal">
    <p><span>Bem vindo à </span><br />Área do Cliente</p>
</div>
<div class="titulo-resumo">
    <p>Essa área é restrita e de uso exclusivo de usuários cadastrados ou clientes dietWin. Aqui você encontrará informações e arquivos importantes para a utilização do dietWin.</p>
</div>
</div>
</div>

<div class="conteudo">
    <div class="container">

        <?php include 'includes/toolbar.php' ?>
        <br />
        <div class="box">
            <h2>Plano de Assistência dietWin</h2>
            <br/>
        <p><img src="<?php SITE_BASE ?>/views/imagens/plano-assistencia.png" alt="Conheça o plano de assistencia" align="left" style="margin-left: -40px;"><br/>
            O Plano de Assistência dietWin é um serviço diferenciado prestado pelo dietWin para clientes que necessitam de suporte exclusivo, treinamento e acesso privilegiado à atualizações e upgrades de sistemas.<br/>
        </p>
            <br/>
            <p>O plano, com validade de um ano, disponibiliza os seguintes diferenciais:</p>
            <div class="clear"></div>
            <ul class="lista-plano">
                <li>• Desconto de 60% na troca ou aquisição de outra versão do dietWin.</li>
                <li>• Desconto de 75% na 2ª licença adquirida.</li>
                <li>• Atualizações sem custo</li>
                <li>• Upgrade gratuito (Profissional 2011)</li>
                <li>• dietWin Personal (chave temporária)</li>
                <li>• Suporte técnico via telefone</li>
                <li>• Suporte online diferenciado</li>
                <li>• Treinamento online</li>
                <li>• Possibilidade de tirar dúvidas com a autora do dietWin.</li>
            </ul>
            <br/>
            <h3>Toda a segurança e facilidade ao seu alcance</h3>
            <p>O plano de assistência do dietWin tem um valor muito em conta: R$ 99 para cada software e apenas R$ 49 para cada computador adicional protegido.</p>

            <div class="box-valor">
                <p class="valor"><span>R$</span>99</p>
                <p class="condicoes"><i>Mais R$ 49 por licença/ computador adicional.</i></p>
                <a href="<?php echo SITE_URL ?>/area-do-cliente/plano-de-assistencia/comprar" title="Compre seu Plano de Assistência na Loja Online"><img src="<?php echo SITE_BASE ?>/views/imagens/compre-loja-online.gif" alt="Compre seu Plano de Assistência na Loja Online" /></a>
            </div>
            <img style="margin-left: 50px;" src="<?php echo SITE_BASE ?>/views/imagens/plano-img.png" alt="Plano de Assistência " />
            <div class="clear"></div>
        </div>

        <?php include 'includes/sidebar-area-cliente.php'; ?>


        <div class="clear"></div>
    </div>
    <div class="inferior"></div>
</div>



<?php include 'includes/rodape.php'; ?>