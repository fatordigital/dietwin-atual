<?php include 'includes/cabecalho.php'; ?>

<body class="area-cliente softwares">

<?php include 'includes/topo.php'; ?>

<div class="titulo-principal">
    <p><span>Bem vindo à </span><br />Área do Cliente</p>
</div>
<div class="titulo-resumo">
    <p>Essa área é restrita e de uso exclusivo de usuários cadastrados ou clientes dietWin. Aqui você encontrará informações e arquivos importantes para a utilização do dietWin.</p>
</div>
</div>
</div>

<div class="conteudo">
<div class="container">

<?php include 'includes/toolbar.php' ?>
<br />
<div class="box">
    <h2>Escolha sua opção no menu ao lado</h2>

</div>

<?php include 'includes/sidebar-area-cliente.php'; ?>


<div class="clear"></div>
</div>
<div class="inferior"></div>
</div>



<?php include 'includes/rodape.php'; ?>