<!DOCTYPE html>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <?php require_once 'includes/head.php' ?>
    <body>
		<?php require_once 'includes/nav.php' ?>
        
        <section class="conteudo cinza">
            <div class="container">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <h1 class="interna">Cadastre-se</h1>
                    <p>A equipe do dietWin está sempre à disposição de seus clientes. Preencha o formulário abaixo para efetuar seu cadastro e ter acesso a todos os softwares da dietWin!</p>
                </div>
                <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
                    <img src="<?php echo $baseurl ?>/imagens/caixas-dietwin-cadastre-se.jpg" alt="dietWin Plus e dietWin Tradicional">
                </div>
                <section class="formulario">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                        <div class="titulo-formulario">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h2>Formulário de cadastro</h2>
                            </div>
                        </div>
                        <form action="#" id="cadastro">
                            <div class="formulario-cadastro">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="nome">Nome:</label>
                                        </div>
                                        <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                                            <input type="text" id="nome" name="nome">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="email">E-mail:</label>
                                        </div>
                                        <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                                            <input type="text" id="email" name="email">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="senha">Senha:</label>
                                        </div>
                                        <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                                            <input type="password" id="senha" name="senha">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="profissao">Profissão:</label>
                                        </div>
                                        <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                                            <input type="text" id="profissao" name="profissao">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 pull-right">
                                            <label>Escola uma das opções abaixo:</label>
                                            <div class="tipo-pessoa">
                                                <input type="radio" name="tipo-pessoa" id="pessoa-fisica" value="Pessoa Física">
                                                <label for="pessoa-fisica">Pessoa Física</label>
                                                <input type="radio" name="tipo-pessoa" id="pessoa-juridica" value="Pessoa Jurídica">
                                                <label for="pessoa-juridica">Pessoa Jurídica</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="rg">RG:</label>
                                        </div>
                                        <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                                            <input type="text" id="rg" name="rg">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="cpf">CPF:</label>
                                        </div>
                                        <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                                            <input type="text" id="cpf" name="cpf">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="endereco">Endereço:</label>
                                        </div>
                                        <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                                            <input type="text" id="endereco" name="endereco">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="bairro">Bairro:</label>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                            <input type="text" id="bairro" name="bairro">
                                        </div>
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="numero">Número:</label>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                            <input type="text" id="numero" name="numero">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="cep">CEP:</label>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                            <input type="text" id="cep" name="cep">
                                        </div>
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="complemento">Complemento:</label>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                            <input type="text" id="complemento" name="complemento">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="estado">Estado:</label>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                            <select name="estado" id="estado">
                                                <option value="null"></option>
                                                <option value="Rio Grande do Sul">Rio Grande do Sul</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="cidade">Cidade:</label>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                            <select name="cidade" id="cidade">
                                                <option value="null"></option>
                                                <option value="Porto Alegre">Porto Alegre</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="telefone">Telefone:</label>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                            <input type="text" id="telefone" name="telefone">
                                        </div>
                                        <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                            <label for="celular">Celular:</label>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                            <input type="text" id="celular" name="celular">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="endereco-entrega">
                                <div class="row">
                                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 pull-right">
                                        <h3>Endereço para entrega:</h3>
                                        <input type="checkbox" id="usar-endereco"> <label for="usar-endereco">Desejo receber no mesmo endereço informado acima</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                        <label for="endereco-entrega">Endereço:</label>
                                    </div>
                                    <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                                        <input type="text" id="endereco-entrega" name="endereco-entrega">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                        <label for="bairro-entrega">Bairro:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                        <input type="text" id="bairro-entrega" name="bairro-entrega">
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                        <label for="numero-entrega">Número:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                        <input type="text" id="numero-entrega" name="numero-entrega">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                        <label for="cep-entrega">CEP:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                        <input type="text" id="cep-entrega" name="cep-entrega">
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                        <label for="complemento-entrega">Complemento:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                        <input type="text" id="complemento-entrega" name="complemento-entrega">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                        <label for="estado-entrega">Estado:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                        <select name="estado-entrega" id="estado-entrega">
                                            <option value="null"></option>
                                            <option value="Rio Grande do Sul">Rio Grande do Sul</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                        <label for="cidade-entrega">Cidade:</label>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                                        <select name="cidade-entrega" id="cidade-entrega">
                                            <option value="null"></option>
                                            <option value="Porto Alegre">Porto Alegre</option>
                                        </select>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs">
                                        <label for="observacoes">Observações:</label>
                                    </div>
                                    <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                                        <textarea name="observacoes" id="observacoes"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="formulario-cadastro">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="erro-form"></div>
                                    </div>
                                    <div class="col-lg-3 col-sm-4 col-sm-4 col-xs-12 pull-right">
                                        <input type="submit" value="Cadastrar" class="processo-compra-submit">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </section>

        <?php require_once 'includes/footer.php' ?>
    </body>
</html>
