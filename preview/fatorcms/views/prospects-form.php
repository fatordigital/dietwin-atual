<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php'; ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

        <!-- Page Head -->
		<?php if (is_null($cliente) OR is_null($cliente->id)) { ?>
			<h2>Cadastro de um Prospect</h2>
			<p id="page-intro">Utilize o formulário abaixo para incluir um prospect no site.</p>
		<?php } else { ?>
			<h2>Edição de um Prospect</h2>
			<p id="page-intro">No formulário abaixo você pode alterar os dados de um prospect.</p>
		<?php } ?>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<?php if (is_null($cliente) OR is_null($cliente->id)) { ?>
					<h3>Dados de um prospect</h3>
				<?php } else { ?>
					<h3>Informações do prospect <?php echo $cliente ? $cliente->nome : 'Não informado' ?> (Pessoa <?php echo (is_null($cliente->cnpj)) ? '<span style="color: red">Física</span>' : '<span style="color: blue">Jurídica</span>'; ?>)</h3>
				<?php } ?>
				
				<ul class="content-box-tabs">
					<li><a href="#tab1" <?php echo ( ! isset($aba) OR $aba == 1 ) ? 'class="default-tab"' : '' ?>>Dados</a></li>
					<?php if ( ! is_null($cliente) AND ! empty($cliente->id)) { ?>
						<li><a href="#tab2" <?php echo (isset($aba) AND $aba == 2 ) ? 'class="default-tab"' : '' ?>>Endereços</a></li>
						<?php if ($cms_usuario_logado->eh_administrador OR in_array('interacoes', $cms_usuario_logado_modulos)) { ?>
							<li><a href="#tab4" <?php echo (isset($aba) AND $aba == 4 ) ? 'class="default-tab"' : '' ?>>Interações</a></li>
						<?php } ?>
					<?php } ?>
				</ul>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<div class="tab-content <?php echo ( ! isset($aba) OR $aba == 1 ) ? 'default-tab' : '' ?>" id="tab1">
			
					<form action="<?php echo SITE_URL ?>/fatorcms/prospects/<?php echo (is_null($cliente) OR is_null($cliente->id)) ? 'cadastrar' : 'atualizar' ?>" method="post" id="prospect_form">

						<fieldset class="column-left">

							<?php if ( ! is_null($cliente)) { ?>
								<input type="hidden" name="id" value="<?php echo $cliente->id ?>" />
							<?php } ?>

							<p>
								<label for="nome">Nome</label>
								<input class="text-input large-input required" type="text" id="nome" name="nome" maxlength="100" value="<?php echo $cliente ? $cliente->nome : '' ?>" />
							</p>

							<p>
								<label for="email">E-mail</label>
								<input class="text-input large-input required" type="text" id="email" name="email" maxlength="255" value="<?php echo $cliente ? $cliente->email : '' ?>" />
							</p>

							<p>
								<label for="email_secundario">E-mail secundário</label>
								<input class="text-input large-input" type="text" id="email_secundario" name="email_secundario" maxlength="255" value="<?php echo $cliente ? $cliente->email_secundario : '' ?>" />
								<br /><small>Este endereço só está disponível dentro do FatorCMS.</small>
							</p>

							<p>
								<label for="senha">Senha</label>
								<input class="text-input medium-input <?php echo (is_null($cliente) OR is_null($cliente->id)) ? '' : '' ?>" type="password" id="senha" name="senha" maxlength="100" />
							</p>

							<p>
								<label for="rg">RG</label>
								<input class="text-input medium-input" type="text" id="rg" name="rg" maxlength="20" value="<?php echo $cliente ? $cliente->rg : '' ?>" />
								<br /><small>Utilize este campo também para o <strong>Responsável</strong> da empresa.</small>
							</p>

							<p>
								<label for="cpf">CPF</label>
								<input class="text-input medium-input" type="text" id="cpf" name="cpf" maxlength="14" value="<?php echo $cliente ? $cliente->cpf : '' ?>" />
								<br /><small>Utilize este campo também para o <strong>Responsável</strong> da empresa.</small>
							</p>

                            <p>
                                <label for="cnpj">Nome da Empresa</label>
                                <input class="text-input medium-input" type="text" id="nome_empresa" name="nome_empresa" maxlength="18" value="<?php echo $cliente ? $cliente->nome_empresa : '' ?>" />
                                <br /><small>Deixe este campo em branco caso o cadastro seja de uma pessoa física.</small>
                            </p>

							<p>
								<label for="cnpj">CNPJ</label>
								<input class="text-input medium-input" type="text" id="cnpj" name="cnpj" value="<?php echo $cliente ? $cliente->cnpj : '' ?>" />
								<br /><small>Deixe este campo em branco caso o cadastro seja de uma pessoa física.</small>
							</p>

							<p>
								<label for="inscricao_estadual">Inscrição Estadual</label>
								<input class="text-input medium-input" type="text" id="inscricao_estadual" name="inscricao_estadual" maxlength="20" value="<?php echo $cliente ? $cliente->inscricao_estadual : '' ?>" />
								<br /><small>Deixe este campo em branco caso o cadastro seja de uma pessoa física.</small>
							</p>

							<p>
								<label for="responsavel_nome">Responsável na empresa</label>
								<input class="text-input large-input" type="text" id="responsavel_nome" name="responsavel_nome" maxlength="100" value="<?php echo $cliente ? $cliente->responsavel_nome : '' ?>" />
								<br /><small>Deixe este campo em branco caso o cadastro seja de uma pessoa física.</small>
							</p>

							<p>
								<label for="telefone_principal">Telefone principal</label>
								<input class="text-input medium-input" type="text" id="telefone_principal" name="telefone_principal" maxlength="14" value="<?php echo $cliente ? $cliente->telefone_principal : '' ?>" />
							</p>

							<p>
								<label for="telefone_extra">Telefone extra</label>
								<input class="text-input medium-input" type="text" id="telefone_extra" name="telefone_extra" maxlength="14" value="<?php echo $cliente ? $cliente->telefone_extra : '' ?>" />
							</p>

							<p>
								<label for="profissao">Profissão</label>
								<input class="text-input large-input" type="text" id="profissao" name="profissao" maxlength="100" value="<?php echo $cliente ? $cliente->profissao  : '' ?>" />
								<br /><small>A profissão do prospect ou do responsável na empresa.</small>
							</p>

						</fieldset>

						<fieldset class="column-right">

							<p>
								<label for="nome">Data de cadastro</label>
								<em><?php echo ($cliente AND ! is_null($cliente->cadastro_data)) ? $cliente->cadastro_data : 'não disponível' ?></em>
							</p>

							<p>
								<label for="nome">Data do último login</label>
								<em><?php echo ($cliente AND ! is_null($cliente->ultimo_login_data)) ? $cliente->ultimo_login_data : 'não disponível' ?></em>
							</p>

							<p>
								<label for="ativo_1">Pode fazer login no site?</label>
								<input type="radio" name="ativo" id="ativo_1" value="1" <?php echo ($cliente AND $cliente->ativo) ? 'checked="checked"' : '' ?> /> <span class="sim">Sim</span>
								<input type="radio" name="ativo" id="ativo_0" value="0" <?php echo ( ! $cliente OR ! $cliente->ativo) ? 'checked="checked"' : '' ?> /> <span class="nao">Não</span>
							</p>

                            <?php if (!is_null($cliente) AND !is_null($cliente->id)) { ?>
                            <div class="content-box"><!-- Start Content Box -->

                                <div class="content-box-header">

                                    <h3>Tipo de Cadastro</h3>
                                    <div class="clear"></div>

                                </div> <!-- End .content-box-header -->

                                <div class="content-box-content">
                                    <p>
                                        <label for="tipo_1">Marque o tipo de cadastro</label>
                                        <input type="radio" name="tipo" id="tipo_1" value="1" <?php echo ($cliente AND $cliente->tipo) ? 'checked="checked"' : '' ?> /> <span class="sim">Cliente</span>
                                        <input type="radio" name="tipo" id="tipo_0" value="0" <?php echo ( ! $cliente OR ! $cliente->tipo) ? 'checked="checked"' : '' ?> /> <span class="nao">Prospect</span>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>

						</fieldset>

						<fieldset class="clear">

							<p>
								<input class="button" type="submit" value="<?php echo (is_null($cliente) OR is_null($cliente->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
							</p>

						</fieldset>

					</form>
					
				</div>


				<?php if ( ! is_null($cliente) AND ! empty($cliente->id)) { // ------------------------------------------------------------------------------------- ?>

					<div class="tab-content tab-enderecos <?php echo ( isset($aba) AND $aba == 2 ) ? 'default-tab' : '' ?>" id="tab2">

						<table>

							<thead>
								<tr>
									<th>Endereço</th>
									<th>Localidade</th>
									<th>Observação</th>
									<th>Para entrega?</th>
									<th>Ações</th>
								</tr>
							</thead>

							<tfoot>
								<tr>
									<td colspan="5">
										<div class="clear"></div>
									</td>
								</tr>
							</tfoot>

							<tbody>
								<?php
		                        if($cliente_enderecos AND count($cliente_enderecos) > 0)
								{
									foreach ($cliente_enderecos as $endereco)
									{
									?>
										<tr>
											<td><?php echo $endereco->endereco.', '.$endereco->numero.(!is_null($endereco->complemento)?' / '.$endereco->complemento:'') ?></td>
											<td><?php echo $endereco->bairro.' - '.$endereco->cidade_nome.' / '.$endereco->estado_sigla.'<br />'.Funcoes::formatar_cep($endereco->cep) ?></td>
											<td><?php echo ( ! empty($endereco->observacao) ? $endereco->observacao : '<em>Não informada</em>') ?></td>
											<td><span class="<?php echo (($endereco->entrega OR count($cliente_enderecos) == 1) ? 'sim': 'nao') ?>"><?php echo (($endereco->entrega OR count($cliente_enderecos) == 1) ? 'Sim': 'Não') ?></span></td>
											<td>
												<a href="<?php echo SITE_URL ?>/fatorcms/prospects/endereco-editar/<?php echo $endereco->id ?>" title="Editar o endereço &quot;<?php echo $endereco->endereco ?>&quot;" class="endereco_editar" id="endereco_<?php echo $endereco->id ?>">
													<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" />
												</a>
												<a href="<?php echo SITE_URL ?>/fatorcms/prospects/endereco-excluir/<?php echo $endereco->id ?>" title="Excluir o endereço &quot;<?php echo $endereco->endereco ?>&quot;" class="item-confirmar">
													<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/cross.png" alt="Excluir" />
												</a>
											</td>
										</tr>
									<?php
									}
								}
								else
								{
									echo '<tr><td colspan="5">Nenhum endereço cadastrado até o momento.</td></tr>';
								}
								?>
							</tbody>

						</table>

						<!-- Formulário para cadastro/edição de um endereço -->

						<div class="notificacao_js"></div> <!-- DIV para inserções de notificações via jQuery -->

						<h4>Formulário de Endereços</h4>
						
						<p>Utilize o formulário abaixo para cadastrar um novo endereço para o cliente. Ou clique na edição de um dos itens acima.</p>

						<form action="<?php echo SITE_URL ?>/fatorcms/prospects/<?php echo ( ! isset($cliente_endereco) OR is_null($cliente_endereco->id)) ? 'endereco-cadastrar' : 'endereco-atualizar' ?>" method="post" id="cliente_endereco_form">

							<fieldset>

								<input type="hidden" name="cliente_id" value="<?php echo $cliente->id ?>" />

								<?php if (isset($cliente_endereco) AND  ! is_null($cliente_endereco)) { ?>
									<input type="hidden" name="id" value="<?php echo $cliente_endereco->id ?>" />
								<?php } ?>

								<?php /* <p>
									<label for="tipo">Tipo</label>
									<select class="text-input small-input required" id="tipo" name="tipo">
										<option value="Residencial <?php echo (isset($cliente_endereco) AND $cliente_endereco->tipo == 'Residencial') ? 'selected="selected"' : '' ?>">Residencial</option>
										<option value="Comercial" <?php echo (isset($cliente_endereco) AND $cliente_endereco->tipo == 'Comercial') ? 'selected="selected"' : '' ?>>Comercial</option>
										<option value="Outro" <?php echo (isset($cliente_endereco) AND $cliente_endereco->tipo == 'Outro') ? 'selected="selected"' : '' ?>>Outro</option>
									</select>
								</p> */ ?>

								<p>
									<label for="endereco">Endereço</label>
									<input class="text-input large-input required" type="text" id="endereco" name="endereco" maxlength="200" value="<?php echo isset($cliente_endereco) ? $cliente_endereco->endereco : '' ?>" />
								</p>

								<p>
									<label for="numero">Número</label>
									<input class="text-input small-input required" type="text" id="numero" name="numero" maxlength="7" value="<?php echo isset($cliente_endereco) ? $cliente_endereco->numero : '' ?>" />
								</p>

								<p>
									<label for="complemento">Complemento</label>
									<input class="text-input medium-input" type="text" id="complemento" name="complemento" maxlength="100" value="<?php echo isset($cliente_endereco) ? $cliente_endereco->complemento : '' ?>" />
								</p>

								<p>
									<label for="bairro">Bairro</label>
									<input class="text-input medium-input required" type="text" id="bairro" name="bairro" maxlength="100" value="<?php echo isset($cliente_endereco) ? $cliente_endereco->bairro : '' ?>" />
								</p>

								<p>
									<label for="cnpj">Estado</label>
									<select class="text-input medium-input required" id="estado_id" name="estado_id">
										<option value="">Selecione um estado</option>
										<?php
										if ($estados AND count($estados) > 0)
										{
											foreach ($estados as $estado)
											{
												echo '<option value="'.$estado->id.'">'.$estado->nome.'</option>';
											}
										}
										?>
									</select>
								</p>

								<p>
									<label for="cpf">Cidade</label>
									<select class="text-input medium-input required" id="cidade_id" name="cidade_id">
										<option value="">Selecione um estado acima</option>
									</select>
								</p>

								<p>
									<label for="cep">CEP</label>
									<input class="text-input small-input required" type="text" id="cep" name="cep" maxlength="9" value="<?php echo isset($cliente_endereco) ? Funcoes::formatar_cep($cliente_endereco->cep) : '' ?>" />
								</p>

								<p>
									<label for="observacao">Observação</label>
									<textarea class="text-input large-input" id="observacao" name="observacao" rows="10" cols="79"><?php echo isset($cliente_endereco) ? $cliente_endereco->observacao : '' ?></textarea>
								</p>

								<p>
									<input class="button" type="submit" value="<?php echo ( ! isset($cliente_endereco) OR is_null($cliente_endereco->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
								</p>

							</fieldset>

						</form>

					</div> <!-- tab2 ------------------------------------------------------------ -->

					<?php if ($cms_usuario_logado->eh_administrador OR in_array('interacoes', $cms_usuario_logado_modulos)) { ?>

						<div class="tab-content tab-interacoes <?php echo (isset($aba) AND $aba == 4 ) ? 'default-tab' : '' ?>" id="tab4">

                            <p>Utilize o formulário abaixo para cadastrar uma interação para este cliente, ou clique no ícone respectivo para editar uma interação anterior.</p>

                            <div class="notificacao_js"></div> <!-- DIV para inserções de notificações via jQuery -->

                            <form action="<?php echo SITE_URL ?>/fatorcms/prospects/<?php echo ( ! isset($cliente_interacao) OR is_null($cliente_interacao->id)) ? 'interacao-cadastrar' : 'interacao-atualizar' ?>" method="post" id="cliente_interacao_form">

                                <fieldset class="column-left" style="width: 25%">

                                    <input type="hidden" name="cliente_id" value="<?php echo $cliente->id ?>" />

	                                <p>
		                                <label for="interacao_tipo">Tipo</label>
		                                <select class="text-input large-input required" id="interacao_tipo" name="tipo">
			                                <option value="Envio de material">Envio de material</option>
			                                <option value="Envio de boleto">Envio de boleto</option>
			                                <option value="Chave de registro">Chave de registro</option>
			                                <option value="Suporte técnico">Suporte técnico</option>
			                                <option value="Reclamação">Reclamação</option>
			                                <option value="Outros">Outros</option>
		                                </select>
	                                </p>

	                            </fieldset>

	                            <fieldset class="column-right" style="width: 70%">

		                            <p>
			                            <label for="enviar_cliente_0">Ao salvar esta interação, enviar seu conteúdo para o cliente?</label>
			                            <input type="radio" name="enviar_cliente" id="enviar_cliente_1" value="1" /> <span class="sim">Sim</span>
			                            <input type="radio" name="enviar_cliente" id="enviar_cliente_0" value="0" checked="checked" /> <span class="nao">Não</span>
		                            </p>

	                            </fieldset>

	                            <fieldset class="clear">

                                    <p>
                                        <label for="titulo">Título</label>
                                        <input class="text-input large-input" type="text" id="titulo" name="titulo" maxlength="200" value="<?php echo ( isset($cliente_interacao) ? $cliente_interacao->titulo : '' ) ?>" />
                                        <br /><small>Este campo é opcional.</small>
                                    </p>

                                    <p class="required-input">
                                        <label for="conteudo">Conteúdo</label>
                                        <textarea class="text-input required interacao_conteudo" cols="80" rows="10" id="conteudo" name="conteudo"><?php echo ( isset($cliente_interacao) ? $cliente_interacao->conteudo : '') ?></textarea>
                                    </p>

                                    <p>
                                        <input class="button" type="submit" value="<?php echo ( ! isset($cliente_compra) OR is_null($cliente_compra->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
                                    </p>

                                </fieldset>

                            </form>

                            <h4 style="border-top:1px solid #ccc; margin-top:20px; padding-top:20px">Interações</h4>

							<table class="interacoes">

								<tbody>
									<?php
			                        if($cliente_interacoes AND count($cliente_interacoes) > 0)
									{
										foreach ($cliente_interacoes as $interacao)
										{
										?>
											<tr>
												<td>
                                                    <table>
                                                        <tr class="interacoes-cabecalho">
                                                            <td>
                                                                <strong id="interacao_<?php echo $interacao->id ?>">Código:</strong>
                                                                <?php echo $interacao->id ?>
                                                            </td>

	                                                        <td>
		                                                        <strong>Tipo:</strong>
		                                                        <?php echo $interacao->tipo ?>
	                                                        </td>

                                                            <td>
                                                                <strong>Data:</strong>
                                                                <?php echo date('d/m/Y \à\s H:i:s', strtotime($interacao->data)) ?>
                                                            </td>

                                                            <td>
                                                                <strong>Autor:</strong>
                                                                <?php echo $interacao->cms_usuario_nome ?>
                                                            </td>

                                                            <td>
                                                                <?php if ($cms_usuario_logado->eh_administrador OR $interacao->cms_usuario_id == $cms_usuario_logado->id) { ?>
                                                                    <a href="<?php echo SITE_URL ?>/fatorcms/prospects/interacao-editar/<?php echo $interacao->id ?>" class="interacao_editar" title="Editar a interação &quot;<?php echo $interacao->id ?>&quot;">
                                                                        <img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" />
                                                                    </a>
                                                                <?php } ?>
                                                                <?php if ($cms_usuario_logado->eh_administrador) { ?>
                                                                    <a href="<?php echo SITE_URL ?>/fatorcms/prospects/interacao-excluir/<?php echo $interacao->id ?>" class="item-confirmar" title="Excluir a interação &quot;<?php echo $interacao->id ?>&quot;">
                                                                        <img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/cross.png" alt="Excluir" />
                                                                    </a>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5">
                                                                <?php if ( ! empty($interacao->titulo)) { ?>
                                                                    <h5><?php echo $interacao->titulo ?></h5>
                                                                <?php } ?>
                                                                <?php echo $interacao->conteudo ?>
                                                            </td>
                                                        </tr>
                                                    </table>
												</td>
											</tr>
										<?php
										}
									}
									else
									{
										echo '<tr><td colspan="1">Nenhuma interação feita até o momento.</td></tr>';
									}
									?>
								</tbody>

							</table>

						</div> <!-- tab4 ------------------------------------------------------------ -->

					<?php } // cms_usuario_logado_modulos interacoes ?>

				
				<?php } // is_null cliente ?>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>