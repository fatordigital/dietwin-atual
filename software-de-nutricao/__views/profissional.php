<?php include 'includes/cabecalho.php'; ?>

<body class="softwares profissional">

<?php include 'includes/topo.php'; ?>
	
    <div class="titulo-principal">
        <p><span>dietWin</span><br />Profissional <img src="<?php echo SITE_BASE ?>/views/imagens/plus.jpg" alt="profissional dietwin" style="margin-bottom:14px;" /></p>        
    </div>
    <div class="titulo-resumo">
        <p>O software mais abrangente da família dietWin é também a ferramenta mais completa do mercado. Desenvolvida para profissionais exigentes que necessitam de todas as informações ao seu alcance e de ferramentas que facilitem o seu dia-a-dia.</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container">
    	
        <?php include 'includes/toolbar.php' ?>
        
        <h1>dietWin <span style="color:#024D20 !important">Profissional </span><span style="color:#309128 !important">Plus</span></h1>
        
        <div class="box" id="tamanho-fonte">
        	<h2>Conheça o novo dietWin, o software mais completo e inovador de nutrição.</h2>
        
            <p>O objetivo principal do dietWin Profissional Plus é <strong>auxiliar o nutricionista na tomada de decisão</strong>, monitorando informações através do relacionamento de resultados obtidos para análise das avaliações executadas. Além disso, gerar subsídios para facilitar o diagnóstico nutricional, obviamente sem ser conclusivo pois não isenta a interpretação dos resultados que sempre será do profissional que assina a avaliação.

O diferencial desta versão esta em disponibilizar inúmeras ferramentas e funções especificas, permitindo avaliar as prováveis interações e implicações nutricionais para um aprofundamento da avaliação e análise dietética dos resultados obtidos.</p>

			<h3>Relacionamento das Informações em cadeia</h3>
            
            <div class="profissional_box_cadeia">
            	Estratégias Nutricionais
            </div>
            <img src="<?php echo SITE_BASE ?>/views/imagens/seta_cadeia.jpg" alt="dietwin" style="margin-left:30px;" />
            <br />
            Definem os passos para a conduta nutricional.
			<br />
            
            <div class="profissional_box_cadeia">
            	Nutriente: <span style="color:#333;">Cuidados Nutricionais</span>
            </div>
            <img src="<?php echo SITE_BASE ?>/views/imagens/seta_cadeia.jpg" alt="dietwin" style="margin-left:30px;" />
            <br />
            Análise de resultados e monitoramento dos cuidados nutricionais.
			<br />
            
            <div class="profissional_box_cadeia">
            	Av.Clínica: <span style="color:#333;">Protocolos de Doenças, Sintomas/sinais Clínicos e Fármacos</span>
            </div>
            <img src="<?php echo SITE_BASE ?>/views/imagens/seta_cadeia.jpg" alt="dietwin" style="margin-left:30px;" />
            <br />
            Nos protocolos são definidas as estratégias nutricionais  básicas.
			<br />
            
            <div class="profissional_box_cadeia">
            	Prescrição Dietética: <span style="color:#333;">Estratégias Nutricionais </span>
            </div>
            <img src="<?php echo SITE_BASE ?>/views/imagens/seta_cadeia.jpg" alt="dietwin" style="margin-left:30px;" />
            <br />
            Análise de resultados e monitoramento dos cuidados nutricionais.
			<br />
            
            <div class="profissional_box_cadeia">
            	Análise Dietética: <span style="color:#333;">Diagnóstico Nutricional  </span>
            </div>
           
            <br /><br />
            <div class="profissional_box_cadeia">
            	Tomada de Decisão
            </div>
            <br /><br /><br /><br />
            
            <h3>Atendimento nutricional ágil e completo em todas as etapas e ciclos de vida</h3>
            
            <p>O dietWin permite um atendimento completo e ágil, através da navegação em guias, com interface similar ao Office, tornando intuitivo e fácil de usar,  não restringindo a ordem da consulta, indo de uma avaliação nutricional ao plano alimentar.</p>
            
            <img src="<?php echo SITE_BASE ?>/views/imagens/profissional_etapas.jpg" alt="profissional" style="margin:30px 0 50px 0;" />
            

            <h3>Interface amigável e intuitiva</h3>
                    <?php
                    if ($fotos and count($fotos)>0)
                    {
                    ?>
                        <ul class="thumbs">
                            <?php
                            foreach ($fotos as $foto)
                            {
                            ?>
                                <li><a href="<?php echo SITE_BASE ?>/arquivos/galerias/<?php echo $foto->galeria_id ?>/<?php echo $foto->arquivo ?>" rel="profissional" title="<?php echo isset($foto->titulo)?$foto->titulo:'dietWin Profissional' ?>"><img src="<?php echo SITE_BASE ?>/arquivos/galerias/<?php echo $foto->galeria_id ?>/thumb1/<?php echo $foto->arquivo ?>" alt="<?php echo isset($foto->titulo)?$foto->titulo:'dietWin Profissional' ?>" /></a></li>
                            <?php
                            }
                            ?>
        
                        </ul>
        
                        <div class="clear"></div>
                    <a href="<?php echo SITE_URL ?>/software/profissional/galeria-foto" class="link-thumbs" title="Veja mais imagens e vídeos do dietWin Profissional ">Veja mais imagens do dietWin Profissional </a>
                    <?php
                    }
                    ?>

            
            <h3>Recursos pra quem é inteligente</h3>
            <p><strong>Tudo o que você precisa durante o atendimento fica acessível na mesma tela sem sair dela.</strong>  Ao abrir a ficha do paciente todo histórico de consultas estão disponíveis para navegação na <strong>evolução das consultas anteriores</strong>, permitindo uma melhor visualização dos resultados para a análise e diagnóstico nutricional. Múltiplas telas podem ser abertas para navegação simultânea. <strong>Não há uma ordem sequencial para o atendimento, cada aba da avaliação é independente, o usuário escolhe por onde deseja começar.</strong></p>

            <img src="<?php echo SITE_BASE ?>/views/imagens/profissional_abas.jpg" style="margin:30px 0 50px 0;" alt="profissional abas" />
            
            <h3>Avaliação Clínica</h3>
            
            <p>Este módulo apresenta um grande diferencial das ferramentas dietWin, pois com esta avaliação permite estabelecer uma rede de informações em cadeia onde são avaliadas tendo uma ação sinalizadora para o monitoramento dos nutrientes críticos avaliados na prescrição da dieta.<br /><br />  
  		  	
• Ficha do paciente.<br /><br />
• Doenças/Condutas: São mais de 140 doenças cadastradas, com protocolos e condutas, alimentos indicados e contra indicados.<br /><br />
• Sintomas e Sinais Clínicos – Mais de 680 sinais e sintomas com informações para você melhor identificar as interações em seus pacientes.<br /><br />
• Fármacos – São mais de 500 medicamentos cadastrados, com nomes genéricos, substância ativa, grupo farmacológico, interações nutricionais, cuidados nutricionais, efeitos colaterais, além de exames alterados.<br /><br />
• Exames Laboratoriais – São 100 exames que podem ser incluídos na ficha, divididos por faixa etária e valor de referência. Evolução dos exames.<br /><br />
• Anamnese: permite cadastrar inúmeros modelos de questionários para serem utilizados de acordo com cada tipo de consulta<br /><br />
• Prontuário: permite o registro da evolução de cada consulta e seus comentários.<br /><br />
• Recomendações nutricionais: podem ser criados para diferentes situações clinicas avaliadas.</p>

            
			<h3>Avaliação Nutricional</h3>
            
            <p>Para todos os ciclos da vida. Toda a avaliação nutricional é sinalizada automaticamente dispensando configurações ou seleções de fórmulas ou equações. Todos os parâmetros e indicadores do estado nutricional são calculados para análise e diagnóstico nutricional. Todas as referências das equações utilizadas são descritas nas telas. Para avaliar, apenas os dados mínimos são obrigatórios como peso e altura, não há restrições de medidas para a avaliação nutricional básica. <br /><br />
 
<strong>Antropometria:</strong> apenas peso atual e altura são as medidas obrigatórias para a emissão de alguns indicadores nutricionais. Outras medidas: peso estimado, peso desejável, peso habitual, altura estimada, 3 medidas de diâmetros, 10 medidas para dobras cutâneas, 12 perímetros. Cada campo da antropometria sinaliza a localização da medida aferida em um boneco.<br /><br />

<strong>Indicadores nutricionais:</strong> são sinalizados de acordo com os dados do paciente e as medidas informadas.<br /><br />

<strong>ADULTO: Protocolo da Avaliação</strong><br /><br />

<strong>• Peso teórico:</strong> Berardinelli, West/1980, MET LIFE/1983, Lorenz, Cormillot, Médias dos protocolos, classificação:<br /><br />
- Relação Blackburn: % PA/PT,PA/PD,PA/PH, Classificação do biótipo, Indice de Wolf.<br />
- Gráfico da curva de evolução de peso, Comparativo de antropometria, Gráfico de medidas antropométricas.<br />
- Peso / Chumlea et al. (1988),  Altura estimada Chumlea, Roche, Steinbaugh (1985).<br /><br />

<strong>•  Classificação do estado nutricional:</strong> IMC: OMS/2004, OMS/Garrow/81, Keys et al./72, Krosla&Lowe/67, Shills/94, 
Royal College of Physicians/1983, Waterlow/199.<br /><br />
      - Jellife: CB, CMB, PCT, <br />
      - Heymsfield/1999: AB, AMB, AGB, classificação  Ann Arlor/1990<br />
      - Circunferência da cintura,SBC<br /><br />

<strong>• Composição corporal, protocolos de % gordura: </strong>Pollock & Jackson, 1978, Pollock & Ward, 1980, Faulkner/1968, Guedes/1985, Katch & Mcardle/1973, Petroski/1995, Peso gordo/Siri, Peso magro/Siri, Peso ósseo/Matiegka,1921, Peso residual/Matiegka,1921, Peso muscular/Matiegka,1921, Densidade corporal, Durnin & Womersley/1984.<br /><br />

<strong>•	Estimativas calóricas: TMB/FAO/OMS/85, GEB/Long/97:</strong> <br />
   - VET: FAO/OMS/85, FAM/OMS/85, HB/Waitzberg & Rodrigues 95, HB/queimados, GEAF/Kacth  <br />
   - VET para redução de peso, VEMTA, Escola Argentina, Reins,NT, % Redução, CFN, Nutroclínica <br /><br />

<strong>IDOSO: Protocolo da Avaliação</strong><br />
•	IMC Lipschitz,1994<br />
•	Indicadores nutricionais compatíveis com o do adulto<br /><br />

<strong>GESTANTE: Protocolo da Avaliação</strong> <br />
•	Normas do protocolo do Ministério da Saúde, 1986 e SISVAN 2005 <br />
•	DUM (data da última menstruação), <br />
•	Peso pré-gravídico <br />
•	DPP (data prevista do parto), <br />
•	Ganho de peso recomendado do período, MS/IOM/1992<br />
•	Ganho de peso médio semanal, <br />
•	Relação peso e altura segundo o percentil. <br />
•	Curva de ganho de peso gestacional coma classificação do IMC de Atalah et AL.1997. <br />
•	VET/FAO/OMS/85 <br />
•	Adicional energético gestacional FAO/OMS/85 e NATIONAL RESEARCH COUNCIL<br /><br />

<strong>CRIANÇA E ADOLESCENTE: Protocolo da Avaliação</strong> <br />
•	Gráficos das curvas de crescimento da OMS 2006 e 2007; <br />
•	Percentil: P/I, A/I, P/A, IMC/I; <br />
•	Escore-z; <br />
•	Diagnóstico nutricional;<br />
•	TMB/FAO/OMS/85; <br />
•	VET (kcal/kg/P, RDA 89);<br />
•	GEB, Schofield 85;<br />
•	Recomendação de PTN/g/kg / RDA 89;</p>


				<h3>Avaliação Nutricional</h3>
                
                <p>Esta avaliação nutricional tem a sua disposição inúmeras possibilidades para análise que vão desde uma:<br /><br />
- Prescrição simples, direta, ágil que pode diminuir significativamente seu tempo de consulta  imprimindo rapidez no seu atendimento até uma ...<br /><br />
- Prescrição refinadas mais especializadas, com funções que ao serem exploradas evoluem os recursos da análise dietética com rapidez e precisão.<br /><br />

A escolha desse formato é uma decisão que depende do momento, do que se está avaliando e das prioridades que uma consulta exige. Por outro lado, também é uma questão de conhecimento do sistema do quanto ele foi testado, revisado, avaliado e alimentado de acordo com o entendimento do profissional diante de uma prescrição.  <br /><br />

<strong>Ferramentas facilitadoras para a prescrição:</strong><br />
•	Pesquisa de alimentos e receitas (5.150 cadastros, 3.849 alimentos e 1.301 receitas);<br />
•	Pesquisa avançada com filtros de nutrientes, grupos e categorias de alimentos;<br />
•	Modelos de cardápios;<br />
•	Alimentos favoritos;<br />
•	Lista de substituição de alimentos<br />
Estratégias nutricionais;<br />
•	Características dietéticas;<br />
•	Nutrientes para monitoramento;<br />
•	QAVP (Quadro de análise dos valores previstos);<br />
•	AMDR;<br />
•	Distribuição energética entre as refeições;<br />
•	Conflitos e Características alimentares;<br /><br />

<strong> Análise dietética: </strong> <br />
•	Análise de 143 nutrientes;<br />
•	Alimentos que compõe a quantidade do nutriente;<br />
•	Adequação das recomendações diária das DRIS: EAR, RDA, Al, UL;<br />
•	Interações nutricionais: Rel g/caloria, Rel kcal/N, Peso g/dieta, PTN-AN/VEG, NPU, NDpcal, Na, NAcl, Sal de adição;<br />
•	Pirâmide alimentar, análise detalhada de cada grupo da pirâmide;<br />
•	QAVE (Quadro de análise dos valores encontrados):<br />
    - Avaliação clínica (monitoramento dos nutrientes implicados e sua adequação);<br />
    - AMDR;<br />
    - Análise e monitoramento do açúcar de adição;<br />
    - g/kg/PA;<br />
   -Distribuição energética das refeições;<br /><br />

<strong>Análise dietética avançada:</strong><br />
•	Análise da carga glicêmica por refeição (IG);<br />
•	Quadro de análise de valores encontrados e previstos (para macronutrientes);<br />
•	Análise de Biodisponibilidade de nutrientes: Relação molar dos nutrientes calculados;<br />
•	Análise do % Ferro absorvível;<br /><br />

<strong>Relatórios: </strong>Inúmeras opções e todas podendo ser exportados permitindo que as Prescrições dietéticas possam ser enviadas por e-mail com a exportação do relatório em diferente extensão de arquivo.
</p>

				
              <h3>Ferramentas Complementares</h3>
              
              <p>
                •	Cadastro de protocolo: Doenças, Sintomas e Sinais, Fármacos, Exames laboratoriais e Conduta dietoterápica;<br />
                •	Cadastro da Ficha técnica de alimentos;<br />
                •	Cadastro da Ficha técnica de receitas;<br />
                •	Cadastro da modelos de cardápios;<br />
                •	Tabelas: TACO, DIETWIN e tabela do usuário;<br />
                •	Mais 1300 receitas cadastradas;<br />
                •	Mais 3500 alimentos;<br />
                •	Replicar cadastro de alimentos e receitas;<br />
                •	Cadastro de grupos de alimentos favoritos;<br />
                •	Cadastro de tipo de refeição;<br />
                •	Cadastro de medidas caseiras;<br />
                •	Horário das refeições;<br />
                •	Contagem de carboidrato;s<br />
                •	Copiar e colar alimentos, refeições ou dietas;<br />
                •	Configurações: ficha técnica alimento e receita, Pesquisa de alimentos e receitas, E-mail para exportação de relatórios, cadastro do nutricionista;<br />
                •	Relatórios, exportação Excel, PDF, BMP, Word;<br />
                •	Capa dos relatórios.<br />
              </p>
              
              <h4>Banco de dados do DietWin</h4>
              
              <p>
              Possui 3 tabelas de alimentos para selecionar:<br /><br />
            <strong>• Tabela Taco 4ª.edição</strong><br /><br />
            <strong>• Tabela dietWin:</strong> que é resultado de uma compilação de dados das principais tabelas: TACO, IBGE,  USDA,  
            CENEXA, Alemã, Repertório Geral dos Alimentos, Fichas técnicas de receitas, para o cadastro micronutrientes 
            rastreamos a bibliografia disponível.<br /><br />
            
            - Alimentos Preparados: todas as Fichas técnicas de receitas são resultado do banco de dados de nossa empresa
             BRUBINS Comércio de Alimentos e Supergelados e nossa experiência na área de alimentação em mais de 30 anos.<br /><br />
             
            - Alimentos industrializados: o fabricante está referenciado na ficha do cadastro.<br /><br />
            
            - Fórmulas industrializadas: para nutrição enteral tem o laboratório referenciado na ficha do cadastro.<br /><br />
            
            <strong>• Tabela do usuário:</strong> é aquela tabela gerada a partir de todas as alterações ou cadastros efetuadas pelo usuário.<br /><br />
            
            <strong>Referências das fórmulas e equações:</strong> estão sinalizadas em cada tela da avaliação com as referências utilizadas.<br /><br />
            
            <strong>Configurações:</strong> permite personalizar funções para navegação.<br /><br />
            
            <strong>Relatórios:</strong> inúmeras opções de relatórios em todos os módulos do sistema.<br /><br />
              </p>
            







            <h3>Muitos recursos, pouco investimento</h3>
            <!--<p>Um investimento de R$ 1,89 ao dia, este é o valor da sua amortização em um ano.</p>
            <p>A aquisição dá direito: 1 licença do direito de uso; Suporte técnico por 90 dias sem custo: via telefone, chat ou e-mail; Treinamento online; Atualização sem custo;</p>
            <p>Após este período você pode optar pelo: <b>PLANO DE ASSISTÊNCIA DIETWIN</b>, contempla uma série de vantagens por tempo ilimitado e não obrigatório. A adesão ao plano oferece: Suporte técnico via telefone, chat ou e-mail; Suporte online; Treinamento online; Atualização sem custo; Upgrade gratuito; Desconto de 50% na 2ª. Licença adquirida; Desconto de 60% na troca ou aquisição de outra versão dietWin.<b> Investimento: <s>De R$ 250,00</s> por R$ 98,00/ANUAL</b></p>-->
            <div class="box-valor">
            	<p class="valor" style="color:#0A5521 !important"><span>R$</span>690</p>
                <p class="condicoes">Podendo ser pago em até 8x no boleto ou cartão de crédito!</p>
                <a href="<?php echo SITE_URL ?>/software/profissional/comprar" title="Compre seu dietWin Profissional Plus  na Loja Online"><img src="<?php echo SITE_BASE ?>/views/imagens/compre-loja-online.gif" alt="Compre seu dietWin Profissional  na Loja Online" /></a>
            </div>
            <img src="<?php echo SITE_BASE ?>/views/imagens/dietwin-profissional-plus.jpg" alt="dietWin Profissional Plus " />
            <div class="clear"></div>
            
            <h5 style="color:#060;">Incluído 1 ano de plano de assistência</h5>
            
            <br /><br /><br />
            
            
            <h4>Plano de assistência DietWin Anual</h4>
            
            <p>
            Contempla uma série de vantagens por tempo ilimitado e não obrigatório. A adesão ao plano oferece: Suporte técnico via telefone, chat ou e-mail; Suporte online; Treinamento online; Atualização sem custo; Upgrade gratuito;<br /><br />

<strong><span style="font-size:18px;">Investimento: De R$ 250,00 por R$ 125,00/ANUAL</span></strong>
            </p>
            <br /><br /><br />
            
            
            
            <h3>Requisitos</h3>
            
            <strong>Windows 8, Windows 7 ou XP. 32 e 64 bits</strong><br />
			Monitor Resolução minima 1024x768<br /><br />

			<strong>Mac OS + Parallels</strong><br />(Via máquina virtual você utiliza plenamente os recursos do dietwin. Desta forma o Mac OS X e o Windows funcionarão lado a lado. Basta
adquirir o Parallels Desktop para Mac ou o VMWare Fusion.)<br /><br /><br />

            
            
            
            
            
            
            
            
            
            
            <!--<h3>O que é preciso para rodar o Profissional Plus ?</h3>


            <ul class="detalhes">
           		<li><b>Configuração Mínima (Instalação: local):</b> Processador com 750MHz; 256 Mb de RAM; 500 Mb de espaço livre em disco; Windows 2000 Pro ou superior; Monitor resolução 1024X768, Impressora. ou em rede</li>
                <li><b>Configuração Mínima (Instalação em Servidor):</b> Processador com 1GHz; 512 Mb de RAM; 500 Mb de espaço livre em disco; Windows 2000 Server ou superior; Windows XP Pro ou superior</li>
			</ul>-->
            
            
            
            
            
            
            <h3>Um canal aberto com você</h3>
            <p>Disponibilizamos treinamentos online para apresentação do sistema. Um manual de ajuda e arquivos passo-a-passo são instalados no mesmo diretório da instalação do dietWin Profissional.
                O projeto dietWin avança sempre de forma intermitente, participe apresentando suas sugestões para novas implementações ou modificações dos protocolos.
            </p>
            
            <div class="box-treinamento">
            	<p>Confira os últimos vídeos disponíveis sobre o dietwin Profissional</p>
                <?php
                if ($videos AND count($videos)>0)
                {
                    ?>
                    <ul class="galeria">
                        <?php
                        foreach ($videos as $video)
                        {
                            ?>
                            <?php
                            $regex = '/http\:\/\/www\.youtube\.com\/watch\?v=(\w{11})/';
                            preg_match($regex, $video->link,$matches);
                            ?>
                            <li>
                                <a href="<?php echo SITE_URL ?>/suporte/profissional/video/<?php echo $video->titulo_seo ?>" class="thumb" title="<?php echo $video->titulo ?>">
                                    <img src="<?php echo SITE_BASE ?>/biblioteca/timthumb.php?src=http://i3.ytimg.com/vi/<?php echo substr($video->link, strrpos($video->link,'v=')+2,11) ?>/0.jpg&w=48&h=48" alt="<?php echo $video->titulo ?>" />
                                    <div class="play"></div>
                                </a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                    <?php
                }
                else
                {
                    echo '<p>Não há vídeos cadastrados para esta categoria.</p>';
                }
                ?>
                <div class="clear"></div>
                <a href="<?php echo SITE_URL ?>/software/profissional/videos" title="Veja a galeria completa de vídeos do dietWin Profissional ">&#187; Veja a galeria completa</a>
            </div>
            
            <div class="box-treinamento">
            	<p>Últimas dúvidas respondidas sobre a ferramenta:</p>
                <?php
                if ($faqs AND count($faqs)>0)
                {
                ?>
                <ul class="duvidas">
                    <?php
                    foreach ($faqs as $faq)
                    {
                    ?>
                	<li><a href="<?php echo SITE_URL.'/suporte/profissional/faq/'.$faq->pergunta_seo ?>" title="<?php echo $faq->pergunta ?>"><?php echo $faq->pergunta ?>?</a></li>
                    <?php
                    }
                    ?>
                   </ul>
                <?php
                }
                else
                {
                    echo '<p>Não há dúvidas cadastradas para esta categoria.</p>';
                }
                ?>
                <div class="clear"></div>
                <a href="<?php echo SITE_URL ?>/suporte/profissional/faq" title="Veja todas as dúvidas sobre o dietWin Profissional ">&#187; Veja todas as dúvidas</a>
            </div>
        </div>
        
        <div class="opcoes">
            <!-- inicio do widget do Webchat Locaweb -->
            <div id="webchat_widget"><!-- Coloque esse div onde você quer que o widget apareça. --></div>
            <script type="text/javascript">
                (function() {
                    var wc = document.createElement('script'); wc.type = "text/javascript";
                    wc.src = 'https://dietwin.webchatlw.com.br/widget.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(wc, s);
                })();
            </script>
            <!-- fim do widget do Webchat Locaweb -->

          <?php /*  <a href="#" title="Converse agora mesmo com nosso atendimento online"><img src="<?php echo SITE_BASE ?>/views/imagens/botao-chat.png" alt="Converse agora mesmo com nosso atendimento online" /></a> */?>
            <a href="#" class="link-mais-info" title="Solicite maiores informações sobre a ferramenta"><img src="<?php echo SITE_BASE ?>/views/imagens/botao-mais-info.png" alt="Solicite maiores informações sobre a ferramenta" /></a>

            <!-- Conteúdo do colorbox do botão "Mais Informações" -->
            <div style="display:none">
                <div class="colorbox-mais-info">
                    <div class="topo"></div>
                    <div class="box">
                        <div class="titulo-colorbox">Mais Informações</div>
                        <div class="conteudo-colorbox">

                            <p>Preencha o formulário abaixo para solicitar mais informações sobre este software.</p>
                            <form method="post" action="<?php echo SITE_URL ?>/profissional/mais_info" id="form-mais-info" >

                                <label><b>Nome:</b></label>
                                <input type="text" class="obrigatorio" name="info_nome" id="info_nome" maxlength="100" />

                                <label><b>E-mail:</b></label>
                                <input type="text" class="obrigatorio" name="info_email" id="info_email" maxlength="255" />

                                <label><b>Telefone:</b></label>
                                <input type="text" class="obrigatorio" name="info_telefone" id="info_telefone" maxlength="14" />

                                <label><b>Cidade:</b></label>
                                <input type="text" class="obrigatorio" name="info_cidade" id="info_cidade" maxlength="100" />

                                <label><b>Mensagem:</b></label>
                                <textarea type="text" class="obrigatorio" name="info_mensagem" id="info_mensagem" style="resize: vertical; overflow:auto"></textarea>

                                <button type="submit">Enviar</button>
                                <div class="clear"></div>
                            </form>

                            <div id="form_notification"></div>

                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="inferior"></div>
                </div>
            </div>
            <!-- Fim do colorbox do botão "Mais Informações" -->

            <a href="<?php echo SITE_URL ?>/software/profissional/videos" title="Acesse o centro de multimídia e treinamento dessa ferramenta"><img src="<?php echo SITE_BASE ?>/views/imagens/botao-centro-multimidia.png" alt="Acesse o centro de multimídia e treinamento dessa ferramenta" /></a>
            <div class="separador"></div>
            <a href="<?php echo SITE_URL ?>/software/profissional/comprar" class="comprar-profissional" title="Compre Online seu dietWin Profissional "><img src="<?php echo SITE_BASE ?>/views/imagens/botao-compre-online-profissional.png" alt="Compre Online seu dietWin Profissional " /></a>
            <br /><br />
            <!--<span style="font-size:14px; margin:0 0 20px 35px; color:#060;">30 dias grátis! <strong>Experimente!</strong></span>
            <br /><br />-->

            <div class="separador"></div>

            <h3>Conheça as outras ferramentas</h3>

            <ul>
                <li class="bubbleInfo" id="pop11">
                    <a href="<?php echo SITE_BASE ?>/personal" class="trigger" title="Conheça o dietWin Personal">
                        <img src="<?php echo SITE_BASE ?>/views/imagens/opcao-personal.jpg" alt="Conheça o dietWin Personal" />
                        <table id="tpop11" class="popup">
                            <tbody>
                            <tr>
                                <td class="topleft corner"></td> <td class="top"></td>	<td class="topright corner"></td>
                            </tr>
                            <tr>
                                <td class="left"></td> <td><table class="popup-contents">
                                <tbody>
                                <tr>
                                    <td><b>Personal </b></td>
                                </tr>
                                </tbody></table>
                            </td>
                                <td class="right"></td>
                            </tr>
                            <tr>
                                <td class="bottomleft corner"></td> <td class="bottom"><img width="13" height="29" alt="popup tail" src="<?php echo SITE_BASE ?>/views/imagens/menu-bubble-tail2.gif"/></td> <td class="bottomright corner"></td>
                            </tr>
                            </tbody>
                        </table>
                    </a>
                </li>
                <li class="bubbleInfo" id="pop12">
                    <a href="<?php echo SITE_BASE ?>/rotulo-de-alimentos" class="trigger" title="Conheça o dietWin Rótulo de Alimentos">
                        <img src="<?php echo SITE_BASE ?>/views/imagens/opcao-rotulo-de-alimentos.jpg" alt="Conheça o dietWin Rótulo de Alimentos" />
                        <table id="tpop12" class="popup">
                            <tbody>
                            <tr>
                                <td class="topleft corner"></td> <td class="top"></td>	<td class="topright corner"></td>
                            </tr>
                            <tr>
                                <td class="left"></td> <td><table class="popup-contents">
                                <tbody>
                                <tr>
                                    <td><b>Rótulo de Alimentos</b></td>
                                </tr>
                                </tbody></table>
                            </td>
                                <td class="right"></td>
                            </tr>
                            <tr>
                                <td class="bottomleft corner"></td> <td class="bottom"><img width="13" height="29" alt="popup tail" src="<?php echo SITE_BASE ?>/views/imagens/menu-bubble-tail2.gif"/></td> <td class="bottomright corner"></td>
                            </tr>
                            </tbody>
                        </table>
                    </a>
                </li>
            </ul>
        </div>
        
        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>
</div>

<div id="div-chat" >
    <a id="chat-abrir" href="#" onClick="wc_popup()"><img src="<?php echo SITE_BASE; ?>/views/imagens/chat-passivo.png" alt="chat" /></a>
    <a id="chat-fechar" href="#"><img src="<?php echo SITE_BASE; ?>/views/imagens/chat-fechar.png" alt="chat" /></a>
</div>

<?php include 'includes/rodape.php'; ?>