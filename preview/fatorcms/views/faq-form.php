<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php'; ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

        <!-- Page Head -->
		<?php if (is_null($faq) OR is_null($faq->id)) { ?>
			<h2>Cadastro de uma Pergunta da FAQ</h2>
			<p id="page-intro">Utilize o formulário abaixo para cadastrar uma pergunta no site.</p>
		<?php } else { ?>
			<h2>Edição de uma Pergunta da FAQ</h2>
			<p id="page-intro">No formulário abaixo você pode alterar os dados de uma pergunta.</p>
		<?php } ?>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<?php if (is_null($faq) OR is_null($faq->id)) { ?>
					<h3>Dados de uma pergunta</h3>
				<?php } else { ?>
					<h3>Informações da pergunta <?php echo $faq ? $faq->pergunta : '' ?></h3>
				<?php } ?>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<form action="<?php echo SITE_URL ?>/fatorcms/faq/<?php echo (is_null($faq) OR is_null($faq->id)) ? 'cadastrar' : 'atualizar' ?>" method="post" id="faq_form">

					<fieldset>

						<?php if ( ! is_null($faq)) { ?>
							<input type="hidden" name="id" value="<?php echo $faq->id ?>" />
						<?php } ?>

                        <p>
                            <label for="produtos_ids">Categoria</label>
                            <select name="produtos_ids[]" id="produtos_ids" class="small-input required" multiple="multiple" size="4">
                                <?php
                                if ($produtos AND count($produtos) > 0)
                                {
                                    foreach ($produtos as $produto)
                                    {
                                        $selected = FALSE;
                                        if (!is_null($faq_produtos) AND count($faq_produtos) > 0)
                                        {
                                            foreach ($faq_produtos as $faq_produto)
                                            {
                                                if (intval($produto->id) == intval($faq_produto->produto_id))
                                                {
                                                    $selected = TRUE;
                                                    break;
                                                }
                                            }
                                        }
                                        echo '<option value="'.$produto->id.'" '.($selected ? 'selected="selected"' : '').'>'.$produto->nome.'</option>';
                                    }
                                }
                                ?>
                            </select><br />
                            <small>Trabalhe com as teclas <strong>Ctrl</strong> e <strong>Shift</strong> para a múltipla seleção de itens.</small>
                        </p>

                        <p>
                            <label for="pergunta">Pergunta</label>
                            <input class="text-input large-input required" type="text" id="pergunta" name="pergunta" maxlength="200" value="<?php echo $faq ? $faq->pergunta : '' ?>" />
                            <br/><small>Número de caracteres restantes: <span id="pergunta_contador">200</span></small>
                        </p>

						<p class="required-input">
							<label for="resposta">Resposta</label>
							<textarea class="text-input required large-input" id="resposta" name="resposta" cols="80" rows="10"><?php echo $faq ? $faq->resposta : '' ?></textarea>
						</p>

						<p>
							<input class="button" type="submit" value="<?php echo (is_null($faq) OR is_null($faq->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
						</p>

					</fieldset>

					<div class="clear"></div><!-- End .clear -->

				</form>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>