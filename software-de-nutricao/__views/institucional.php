<?php include 'includes/cabecalho.php'; ?>

<body class="institucional">

<?php include 'includes/topo.php'; ?>
	
    <div class="titulo-principal">
        <p><span>Quem está por trás do</span><br />dietWin</p>        
    </div>
    <div class="titulo-resumo">
        <p>Por trás da família de softwares de nutrição mais vendidos do Brasil está uma equipe experiente de nutricionistas e desenvolvedores. Conheça um pouco sobre a história e as pessoas envolvidas.</p>
    </div>
    </div>
</div>

<div class="conteudo">
	<div class="container" id="tamanho-fonte">
    	
        <?php include 'includes/toolbar.php' ?>
        
        <h1><span>Institucional do</span> dietWin Softwares de Nutrição</h1>
        
        <div class="box">
        	<h2>Quando a experiência em nutrição encontra o conhecimento e tecnologia de ponta</h2>

            <h3>DIETWIN. AUXILIANDO O NUTRICIONISTA NA TOMADA DE DECISÃO.</h3>

                <p>O dietWin é uma empresa especializada em softwares de nutrição, desenvolvido por nutricionistas e tem como objetivo de otimizar, padronizar e qualificar os processos e rotinas de trabalho, facilitando a interpretação e imprimindo confiabilidade aos dados apresentados. Além disso, permite ao profissional ampliar sua perspectiva no mercado de trabalho, dinamizando e otimizando seu tempo e padronizando suas rotinas evitando o re-trabalho.</p>
                <p>Com a evolução da Ciência da Nutrição são necessárias ferramentas de suporte para execução das atividades do nutricionista. Com os avanços tecnológicos, nossos softwares vêm se adaptando a essas mudanças. Como líder nacional de softwares de nutrição, estamos sempre à frente e por isso estamos sempre remodelamos nossa família de softwares visando atender melhor as necessidades dos nutricionistas, economizando no investimento necessário e tornando você mais satisfeito com nossos produtos. </p>

            

            <div class="clear"></div>
            <p>O dietWin é uma empresa focada em desenvolver ferramentas que auxiliem o nutriciovnista na tomada de decisão, tornando o trabalho mais ágil e confiável, garantindo maior fluxo e controle de informações e, conseqüentemente, maiores resultados. É para isso que trabalhamos, desde 1993, desenvolvendo softwares de forma independente.</p>
            <p>
                O dietWin foi criado em 1993, é uma empresa sediada em Porto Alegre/RS, mantém uma parceria na área da informática com outra grande empresa para análise do sistema e desenvolvimento dos softwares dietWin desde 2001, a Dataweb possui uma equipe de analistas de sistemas e uma estrutura completa que une inteligência e tecnologia para garantir agilidade, qualidade e eficiência.
            </p>
            <h3>Forma de trabalho focada em atingir os melhores resultados</h3>
            <p>Acreditamos que para a evolução da ciência da nutrição são necessárias ferramentas de suporte para execução das atividades diárias do nutricionista. É para isso que trabalhamos, ao longo destes anos, desenvolvendo softwares de forma independente. </p>
            <p>Para garantir a sua melhor utilização, o dietWin atua principalmente através: </p>
            <ul class="detalhes">
                <li>Do desenvolvimento continuado dos softwares </li>
                <li>Na manutenção constante do sistema</li>
                <li>Pesquisando as últimas novidades da nutrição </li>
                <li>Trabalho constante na atualização do banco de dados. </li>
                <li>Realizando treinamentos on-line </li>
                <li>Do atendimento do suporte aos usuários</li>
            </ul>

            <p>O Projeto dietWin é intermitente, estamos continuadamente na busca da melhor ferramenta e executando melhorias no sistema. Seu passo a passo é acompanhado por nutricionistas autores e mentores do sistema. </p>
            <p>Mantemos um canal em afinação entre nossa equipe de nutricionistas, estagiários de nutrição, técnicos de informática e nossos usuários, que depuram as sugestões e informações colhidas com vistas a novas implementações.</p>
            <p>O dietWin é uma solução moderna que permite obter melhores resultados com baixo custo.   </p>

            <h3>Logomarca em diversos formatos</h3>
            <br/>
            <h3>
            <b><a href="<?php echo SITE_BASE ?>/arquivos/marca-dietwin.cdr" title="Logomarca no formato Corel Draw 12">Corel Draw 12 - Download da Logomarca em Formato Corel</a></b> <br/>
            <b><a href="<?php echo SITE_BASE ?>/arquivos/marca-dietwin.png" title="Logomarca no formato PNG Transparente">PNG Transparente - Download da Logomarca em Formato PNG</a></b><br/>
            <b><a href="<?php echo SITE_BASE ?>/arquivos/marca-dietwin.jpg" title="Logomarca no formato JPEG">JPEG - Download da Logomarca em Formato JPG</a></b></h3>

        </div>

        <div class="autores">
            <h3>Autora</h3>


            <p><b><a href="<?php echo SITE_BASE ?>/arquivos/curriculo-carmen.doc" title="Autora, Carmen Suzana Bassoa Reinstein">Carmen Suzana Bassoa Reinstein,</a></b> Nutricionista e Socióloga, Especialista em Nutrição Clínica e Pesquisa, Diretora da Brubins Alimentos SUpergelados Ltda, autora do dietWin Softwares de Nutrição.</p>
            <br/>
            <h3>Co-Autor</h3>
            <p><b><a href="http://www.brubins.com.br" title="Co-Autor, Bruno Bassoa Reinstein">Bruno Bassoa Reinstein,</a></b> Nutricionista e Especialista em Marketing pela ESPM-POA. Atua no desenvolvimento de novos produtos.</p>
            <br/>
            <h3>Análise de Sistemas e Desenvolvimento</h3>
            <p><b><a href="http://www.dataweb.com.br" title="Análise de Sistemas e Desenvolvimento, Rafael Zingano">Rafael Zingano,</a></b> sócio da Dataweb, empresa parceira no desenvolvimento do dietWin desde 2004. Analista de sistemas.</p>
        </div>








        <div class="clear"></div>        
    </div>
    <div class="inferior"></div>  
</div>

<?php include 'includes/rodape.php'; ?>