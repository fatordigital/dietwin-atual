<?php
/**
 * Configurações básicas do sistema
 *
 * Todas as requisições passam por esta página. Aqui são definidas algumas configurações iniciais
 * e em seguida a classe Iniciar é iniciada e a partir dela a mágica acontece.
 */

date_default_timezone_set('America/Sao_Paulo');


// Informações de conexão com o Banco de Dados
$bd_config = array(

// Configuração para acesso ao nosso banco local
'server' => array(
	'url'            => 'server',
	'usuario'        => 'root',
	'senha'          => 'root',
	'banco'          => 'dietwin2014',
	'tabela_prefixo' => 'fd_'
	),

//-----
// Configuração para acesso ao banco da prévia ou banco final do cliente
'cliente' => array(
	'url'            => '127.0.0.1',
	'usuario'        => 'root',
	'senha'          => '6zzr55fbygYi',
	'banco'          => 'dietwin',
	'tabela_prefixo' => 'fd_'
	)
);


/*
'cliente' => array(
	'url'            => 'mysql01.dietwin2.hospedagemdesites.ws',
	'usuario'        => 'dietwin2',
	'senha'          => 'pumaaguia2002',
	'banco'          => 'dietwin2',
	'tabela_prefixo' => 'fd_'
	)
);
*/

//-----

// Configurações para o envio dos emails
$email_config = array(

'server' => array(
	'para'          => 'felipe.schmitz@fatordigital.com.br',
	'para_nome'     => 'dietWin - Desenvolvimento',
	'de'            => 'felipe.schmitz@fatordigital.com.br',
	'de_nome'       => 'dietWin - Desenvolvimento',
	'reply_to'      => 'felipe.schmitz@fatordigital.com.br',
	'reply_to_nome' => 'dietWin - Desenvolvimento',
	'return_path'   => 'felipe.schmitz@fatordigital.com.br',
	'smtp_enviar'   => TRUE,
	'smtp_servidor' => 'smtp.mandrillapp.com',
	'smtp_porta'    => '587',
	'smtp_cripto'   => 'tls',
	'smtp_usuario'  => 'roberto@fatordigital.com.br',
	'smtp_senha'    => 'LjDve4b7NGoqDehammgH5g'
	),

'cliente' => array(
	'para'          => 'sac@dietwin.com.br',
	'para_nome'     => 'dietWin',
	'de'            => 'sac@dietwin.com.br',
	'de_nome'       => 'dietWin',
	'reply_to'      => 'sac@dietwin.com.br',
	'reply_to_nome' => 'dietWin',
	'return_path'   => 'nao-responder@dietwin.com.br',
	'smtp_enviar'   => TRUE,
	'smtp_servidor' => 'smtp.gmail.com',
	'smtp_porta'    => 587,
	'smtp_cripto'   => 'tls',
	'smtp_usuario'  => 'nao-responder@dietwin.com.br',
	'smtp_senha'    => 'ins24BRU'
	)
);


// Nome do controller que deve ser usado quando nenhum for passado
define('CONTROLLER_PADRAO', 'Blog');

// Nome do método que será executada no controller caso uma não seja definida
define('METODO_PADRAO', 'index');

// O sistema está rodando no Apache ou no IIS 6?
define('IS_IIS', preg_match('/IIS/', $_SERVER['SERVER_SOFTWARE']));

// Configura a codificação para as funções multi_byte
mb_internal_encoding('UTF-8');
// Para a função de SEO
setlocale(LC_ALL, 'pt_BR.UTF8');

//----------

// Configurações do PagSeguro
$pagseguro_config = array(

    'server' => array(
        'email' => 'fatordigital@fatordigital.com.br',
        'token' => 'B032F04641B04BA884B5D42C4C8ADC07'
    ),

    'cliente' => array(
        'email' => 'bruno@brubins.com.br',
        'token' => 'E9D020D9AD53474F881285A031E2F4B5'
    )
);

//----------

$regras_especiais = array('buscar/=$buscar', 'pagina/=$pagina', 'ordenar-por/=$ordenar_por', 'ordem/=$ordem', 'aba/=$aba');

$regras = array(
	// Funcionamento Padrão do FatorCMS! Nunca deve ser apagado e deve ser sempre a última da parte do FatorCMS!
	'fatorcms=@diretorio/=@controller/=@metodo', // Define que o primeiro parâmetro, quando for "fatorcms", especifica um diretório

	//----- ACIMA regras do fatorcms, ABAIXO regras do site

    //Página específica de pergunta ou video
    'suporte=$local/=$categoria/video=@controller/=$titulo_seo',
    'suporte=$local/=$categoria/faq=@controller/=$titulo_seo',
    'suporte=$local/=$categoria/videos=@controller',
    'suporte=@controller',

    //Controllers que envolvem suporte(Videos,faq)
    'suporte=$local/=$categoria/=@controller',

    'area-do-cliente=@controller/downloads=@metodo/=$arquivo',

    'area-do-cliente=$local/plano-de-assistencia=@controller/=@metodo',

    'software=$local/=$categoria/comprar=@controller/cupom=@metodo',

    //Galeria de Fotos dos Softwares
    'software=$local/=$categoria/=@controller',

    //Descricao dos produtos
    'software=$local/=@controller',

    //FAQ
    'faq=@controller/duvida=@metodo',
    'faq=@controller',

    'comprar=@controller/identificacao=@metodo',
    'comprar=@controller',


    //Videos
    'videos=@controller',

    //Novidades
    'novidade=@controller/=$titulo_seo',

	// PagSeguro
	//'pagseguro-compra=@controller/=$pg_notificacao_tipo/=$pg_notificacao_codigo',

	//---

	// Erros
	'erro=@controller/=$numero',

    'blog=@controller/=@metodo',

	// Funcionamento Padrão! Nunca deve ser apagado e deve ser sempre a última!
    '=@controller/=@metodo'// Define que o que vier no primeiro parâmetro é Controller, e no segundo é o método da classe do Controller
);