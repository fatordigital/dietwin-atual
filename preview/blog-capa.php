<?php require_once 'includes/head.php' ?>
<body>
<?php require_once 'includes/nav.php' ?>

<section class="blog">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2>Acompanhe nosso blog</h2>
		</div>

		<?php if(isset($novidades) AND is_array($novidades) AND isset($novidades[0])){ ?>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">			
			<div class="post<?php if($novidades[0]->categoria_id==999){echo ' ebook';} ?>">
				<?php if($novidades[0]->categoria_id==999){echo '<div class="flag_ebook"></div>';} ?>
				<a href="<?php echo SITE_URL."/blog/post/".$novidades[0]->titulo_seo; ?>">
					<?php if($novidades[0]->imagem!=""){ ?>
		          	<img src="<?php echo SITE_URL ?>/arquivos/novidades/thumb1/<?php echo $novidades[0]->imagem; ?>" width="100%" class="img-responsive" alt="">
		          	<?php }else{ ?>
		            <!-- <img src="<?php echo SITE_URL ?>/views/imagens/blog/post-1-thumb1.jpg" class="img-responsive" alt=""> -->
		          	<?php } ?>
				</a>
				<div class="content grande">
		          <h3><a href="<?php echo SITE_URL."/blog/post/".$novidades[0]->titulo_seo; ?>"><?php echo $novidades[0]->titulo; ?></a></h3>
		          <p><?php echo $novidades[0]->resumo; ?></p>
		        </div>
				<hr>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center footer">
					<span class="data"><?php echo Funcoes::blog_data_mes_completo($novidades[0]->data); ?></span>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center num-comentarios footer normal">
					<span class="icon-comentario"></span>
					<div class="clearfix"></div>
					<span class="total-comentarios"><span class="disqus-comment-count" data-disqus-url="<?php echo SITE_URL."/blog/post/".$novidades[0]->titulo_seo; ?>">0</span> comentário (s)</span>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center footer">
					<a href="<?php echo SITE_URL."/blog/post/".$novidades[0]->titulo_seo; ?>" class="saiba-mais">Leia mais</a>
				</div>
			</div>
		</div>
		<?php }else if(!empty($novidades)){ ?>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">			
			<div class="post<?php if($novidades->categoria_id==999){echo ' ebook';} ?>">
				<?php if($novidades->categoria_id==999){echo '<div class="flag_ebook"></div>';} ?>
				<a href="<?php echo SITE_URL."/blog/post/".$novidades->titulo_seo; ?>">
					<?php if($novidades->imagem!=""){ ?>
		          	<img src="<?php echo SITE_URL ?>/arquivos/novidades/thumb1/<?php echo $novidades->imagem; ?>" width="100%" class="img-responsive" alt="">
		          	<?php }else{ ?>
		            <!-- <img src="<?php echo SITE_URL ?>/views/imagens/blog/post-1-thumb1.jpg" class="img-responsive" alt=""> -->
		          	<?php } ?>
				</a>
				<div class="content grande">
		          <h3><a href="<?php echo SITE_URL."/blog/post/".$novidades->titulo_seo; ?>"><?php echo $novidades->titulo; ?></a></h3>
		          <p><?php echo $novidades->resumo; ?></p>
		        </div>
				<hr>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center footer">
					<span class="data"><?php echo Funcoes::blog_data_mes_completo($novidades->data); ?></span>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center num-comentarios footer normal">
					<span class="icon-comentario"></span>
					<div class="clearfix"></div>
					<span class="total-comentarios"><span class="disqus-comment-count" data-disqus-url="<?php echo SITE_URL."/blog/post/".$novidades->titulo_seo; ?>">0</span> comentário (s)</span></span>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center footer">
					<a href="<?php echo SITE_URL."/blog/post/".$novidades->titulo_seo; ?>" class="saiba-mais">Leia mais</a>
				</div>
			</div>
		</div>
		<?php }else{ ?>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			Nenhum post cadastrado no momento.
		</div>
		<?php } ?>

		<?php if(isset($novidades) AND is_array($novidades) AND isset($novidades[1])){ ?>
		<div class="col-lg-6 col md-6 col-sm-6 col-xs-12">			
			<div class="post<?php if($novidades[1]->categoria_id==999){echo ' ebook';} ?>">
				<?php if($novidades[1]->categoria_id==999){echo '<div class="flag_ebook"></div>';} ?>
				<a href="<?php echo SITE_URL."/blog/post/".$novidades[1]->titulo_seo; ?>">
					<?php if($novidades[1]->imagem!=""){ ?>
		          	<img src="<?php echo SITE_URL ?>/arquivos/novidades/thumb1/<?php echo $novidades[1]->imagem; ?>" width="100%" class="img-responsive" alt="">
		          	<?php }else{ ?>
		            <!-- <img src="<?php echo SITE_URL ?>/views/imagens/blog/post-1-thumb1.jpg" class="img-responsive" alt=""> -->
		          	<?php } ?>
				</a>
				<div class="content grande">
		          <h3><a href="<?php echo SITE_URL."/blog/post/".$novidades[1]->titulo_seo; ?>"><?php echo $novidades[1]->titulo; ?></a></h3>
		          <p><?php echo $novidades[1]->resumo; ?></p>
		        </div>
				<hr>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center footer">
					<span class="data"><?php echo Funcoes::blog_data_mes_completo($novidades[1]->data); ?></span>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center num-comentarios footer normal">
					<span class="icon-comentario"></span>
					<div class="clearfix"></div>
					<span class="total-comentarios"><span class="disqus-comment-count" data-disqus-url="<?php echo SITE_URL."/blog/post/".$novidades[1]->titulo_seo; ?>">0</span> comentário (s)</span>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center footer">
					<a href="<?php echo SITE_URL."/blog/post/".$novidades[1]->titulo_seo; ?>" class="saiba-mais">Leia mais</a>
				</div>
			</div>
		</div>
		<?php } ?>

		<?php if(isset($novidades) AND is_array($novidades) AND isset($novidades[2])){ ?>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">			
			<div class="post<?php if($novidades[2]->categoria_id==999){echo ' ebook';} ?>">
				<?php if($novidades[2]->categoria_id==999){echo '<div class="flag_ebook"></div>';} ?>
				<a href="<?php echo SITE_URL."/blog/post/".$novidades[2]->titulo_seo; ?>">
					<?php if($novidades[2]->imagem!=""){ ?>
		          	<img src="<?php echo SITE_URL ?>/arquivos/novidades/thumb1/<?php echo $novidades[2]->imagem; ?>" width="100%" class="img-responsive" alt="">
		          	<?php }else{ ?>
		            <!-- <img src="<?php echo SITE_URL ?>/views/imagens/blog/post-1-thumb1.jpg" class="img-responsive" alt=""> -->
		          	<?php } ?>
				</a>
				<div class="content grande">
		          <h3><a href="<?php echo SITE_URL."/blog/post/".$novidades[2]->titulo_seo; ?>"><?php echo $novidades[2]->titulo; ?></a></h3>
		          <p><?php echo $novidades[2]->resumo; ?></p>
		        </div>
				<hr>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center footer">
					<span class="data"><?php echo Funcoes::blog_data_mes_completo($novidades[2]->data); ?></span>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center num-comentarios footer normal">
					<span class="icon-comentario"></span>
					<div class="clearfix"></div>
					<span class="total-comentarios"><span class="disqus-comment-count" data-disqus-url="<?php echo SITE_URL."/blog/post/".$novidades[2]->titulo_seo; ?>">0</span> comentário (s)</span>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center footer">
					<a href="<?php echo SITE_URL."/blog/post/".$novidades[2]->titulo_seo; ?>" class="saiba-mais">Leia mais</a>
				</div>
			</div>
		</div>
		<?php } ?>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="sidebar-home" id="!">
				<div class="sidebar">
					<h4>Blog Dietwin</h4>
					<hr>
					<p>Bem-vindo ao blog para nutricionistas. É aqui que você fica fica sabendo das melhorias do programa e sobre o mercado de nutrição. Acompanhe as últimas novidades e dicas.</p>
					<p><a href="<?php echo SITE_URL; ?>">Não conhece o Dietwin?</a></p>

					<?php if(isset($_SESSION['cadastro_blog_sucesso'])){ ?>
					<div class="alert alert-success blog-cadastro-sucesso" role="alert">						
						E-mail cadastrado com sucesso!
					</div>
					<?php unset($_SESSION['cadastro_blog_sucesso']); } ?>
					<form action="<?php echo SITE_URL."/blog/cadastro-email" ?>" method="post" role="form" class="form-inline">
						<div class="form-group">
							<label class="sr-only" for="email">E-mail</label>
							<input type="email" name="email" class="form-control" id="email" placeholder="seu@email" required="required">
							<input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
						</div>
						<button type="submit">Cadastrar</button>
					</form>
					

					<div class="categorias">
						<h4>Categorias</h4>						
						<ul>
						<?php
						if(isset($categorias) AND is_array($categorias)){ 						
						foreach($categorias as $categoria): ?>	
							<li><a href="<?php echo SITE_URL."/blog/categoria/".$categoria->tag; ?>"><?php echo $categoria->nome; ?> <span class="num"><?php echo $categoria->total_posts; ?></span></a></li>
						<?php 
						endforeach;						 						
						 }else{ 
						 	if(!empty($categorias)){
						 		?>
						 			<li><a href="<?php echo SITE_URL."/blog/categoria/".$categorias->tag; ?>"><?php echo $categorias->nome; ?> <span class="num"><?php echo $categorias->total_posts; ?></span></a></li>
						 		<?php
						 	}else{
						 		echo "<li>Nenhuma categoria cadastrada.</li>";
						 	}
						 ?>							
						<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix separador"></div>

		<?php if(isset($novidades) AND is_array($novidades) AND isset($novidades[3])){ ?>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="post<?php if($novidades[3]->categoria_id==999){echo ' ebook';} ?>">
				<a href="<?php echo SITE_URL."/blog/post/".$novidades[3]->titulo_seo; ?>">
					<?php if($novidades[3]->imagem!=""){ ?>
		          	<img src="<?php echo SITE_URL ?>/arquivos/novidades/thumb2/<?php echo $novidades[3]->imagem; ?>" width="100%" class="img-responsive" alt="">
		          	<?php }else{ ?>
		            <!-- <img src="<?php echo SITE_URL ?>/views/imagens/blog/post-1-thumb1.jpg" class="img-responsive" alt=""> -->
		          	<?php } ?>
				</a>
				<div class="content">
		          <h3><a href="<?php echo SITE_URL."/blog/post/".$novidades[3]->titulo_seo; ?>"><?php echo $novidades[3]->titulo; ?></a></h3>
		          <p><?php echo $novidades[3]->resumo; ?></p>
		        </div>
				<hr>
				<div class="col-lg-6-col-md-6 col-sm-6 col-xs-6 text-center footer separador">
					<span class="data"><?php echo Funcoes::blog_data_mes_completo($novidades[3]->data); ?></span>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center footer">
					<a href="<?php echo SITE_URL."/blog/post/".$novidades[3]->titulo_seo; ?>" class="saiba-mais">Leia mais</a>
				</div>
			</div>
		</div>
		<?php } ?>

		<?php if(isset($novidades) AND is_array($novidades) AND isset($novidades[4])){ ?>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="post<?php if($novidades[4]->categoria_id==999){echo ' ebook';} ?>">
				<a href="<?php echo SITE_URL."/blog/post/".$novidades[4]->titulo_seo; ?>">
					<?php if($novidades[4]->imagem!=""){ ?>
		          	<img src="<?php echo SITE_URL ?>/arquivos/novidades/thumb2/<?php echo $novidades[4]->imagem; ?>" width="100%" class="img-responsive" alt="">
		          	<?php }else{ ?>
		            <!-- <img src="<?php echo SITE_URL ?>/views/imagens/blog/post-1-thumb1.jpg" class="img-responsive" alt=""> -->
		          	<?php } ?>
				</a>
				<div class="content">
		          <h3><a href="<?php echo SITE_URL."/blog/post/".$novidades[4]->titulo_seo; ?>"><?php echo $novidades[4]->titulo; ?></a></h3>
		          <p><?php echo $novidades[4]->resumo; ?></p>
		        </div>
				<hr>
				<div class="col-lg-6-col-md-6 col-sm-6 col-xs-6 text-center footer separador">
					<span class="data"><?php echo Funcoes::blog_data_mes_completo($novidades[4]->data); ?></span>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center footer">
					<a href="<?php echo SITE_URL."/blog/post/".$novidades[4]->titulo_seo; ?>" class="saiba-mais">Leia mais</a>
				</div>
			</div>
		</div>
		<?php } ?>

		<?php if(isset($novidades) AND is_array($novidades) AND isset($novidades[5])){ ?>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="post<?php if($novidades[5]->categoria_id==999){echo ' ebook';} ?>">
				<a href="<?php echo SITE_URL."/blog/post/".$novidades[5]->titulo_seo; ?>">
					<?php if($novidades[5]->imagem!=""){ ?>
		          	<img src="<?php echo SITE_URL ?>/arquivos/novidades/thumb2/<?php echo $novidades[5]->imagem; ?>" width="100%" class="img-responsive" alt="">
		          	<?php }else{ ?>
		            <!-- <img src="<?php echo SITE_URL ?>/views/imagens/blog/post-1-thumb1.jpg" class="img-responsive" alt=""> -->
		          	<?php } ?>
				</a>
				<div class="content">
		          <h3><a href="<?php echo SITE_URL."/blog/post/".$novidades[5]->titulo_seo; ?>"><?php echo $novidades[5]->titulo; ?></a></h3>
		          <p><?php echo $novidades[5]->resumo; ?></p>
		        </div>
				<hr>
				<div class="col-lg-6-col-md-6 col-sm-6 col-xs-6 text-center footer separador">
					<span class="data"><?php echo Funcoes::blog_data_mes_completo($novidades[5]->data); ?></span>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center footer">
					<a href="<?php echo SITE_URL."/blog/post/".$novidades[5]->titulo_seo; ?>" class="saiba-mais">Leia mais</a>
				</div>
			</div>
		</div>
		<?php } ?>

		<?php if(isset($novidades) AND is_array($novidades) AND isset($novidades[6])){ ?>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="post<?php if($novidades[6]->categoria_id==999){echo ' ebook';} ?>">
				<a href="<?php echo SITE_URL."/blog/post/".$novidades[6]->titulo_seo; ?>">
					<?php if($novidades[6]->imagem!=""){ ?>
		          	<img src="<?php echo SITE_URL ?>/arquivos/novidades/thumb2/<?php echo $novidades[6]->imagem; ?>" width="100%" class="img-responsive" alt="">
		          	<?php }else{ ?>
		            <!-- <img src="<?php echo SITE_URL ?>/views/imagens/blog/post-1-thumb1.jpg" class="img-responsive" alt=""> -->
		          	<?php } ?>
				</a>
				<div class="content">
		          <h3><a href="<?php echo SITE_URL."/blog/post/".$novidades[6]->titulo_seo; ?>"><?php echo $novidades[6]->titulo; ?></a></h3>
		          <p><?php echo $novidades[6]->resumo; ?></p>
		        </div>
				<hr>
				<div class="col-lg-6-col-md-6 col-sm-6 col-xs-6 text-center footer separador">
					<span class="data"><?php echo Funcoes::blog_data_mes_completo($novidades[6]->data); ?></span>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center footer">
					<a href="<?php echo SITE_URL."/blog/post/".$novidades[6]->titulo_seo; ?>" class="saiba-mais">Leia mais</a>
				</div>
			</div>
		</div>
		<?php } ?>

		<?php if(isset($novidades) AND is_array($novidades) AND isset($novidades[7])){ ?>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="post<?php if($novidades[7]->categoria_id==999){echo ' ebook';} ?>">
				<a href="<?php echo SITE_URL."/blog/post/".$novidades[7]->titulo_seo; ?>">
					<?php if($novidades[7]->imagem!=""){ ?>
		          	<img src="<?php echo SITE_URL ?>/arquivos/novidades/thumb2/<?php echo $novidades[7]->imagem; ?>" width="100%" class="img-responsive" alt="">
		          	<?php }else{ ?>
		            <!-- <img src="<?php echo SITE_URL ?>/views/imagens/blog/post-1-thumb1.jpg" class="img-responsive" alt=""> -->
		          	<?php } ?>
				</a>
				<div class="content">
		          <h3><a href="<?php echo SITE_URL."/blog/post/".$novidades[7]->titulo_seo; ?>"><?php echo $novidades[7]->titulo; ?></a></h3>
		          <p><?php echo $novidades[7]->resumo; ?></p>
		        </div>
				<hr>
				<div class="col-lg-6-col-md-6 col-sm-6 col-xs-6 text-center footer separador">
					<span class="data"><?php echo Funcoes::blog_data_mes_completo($novidades[7]->data); ?></span>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center footer">
					<a href="<?php echo SITE_URL."/blog/post/".$novidades[7]->titulo_seo; ?>" class="saiba-mais">Leia mais</a>
				</div>
			</div>
		</div>
		<?php } ?>

		<?php if(isset($novidades) AND is_array($novidades) AND isset($novidades[8])){ ?>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<div class="post<?php if($novidades[8]->categoria_id==999){echo ' ebook';} ?>">
				<a href="<?php echo SITE_URL."/blog/post/".$novidades[8]->titulo_seo; ?>">
					<?php if($novidades[8]->imagem!=""){ ?>
		          	<img src="<?php echo SITE_URL ?>/arquivos/novidades/thumb2/<?php echo $novidades[8]->imagem; ?>" width="100%" class="img-responsive" alt="">
		          	<?php }else{ ?>
		            <!-- <img src="<?php echo SITE_URL ?>/views/imagens/blog/post-1-thumb1.jpg" class="img-responsive" alt=""> -->
		          	<?php } ?>
				</a>
				<div class="content">
		          <h3><a href="<?php echo SITE_URL."/blog/post/".$novidades[8]->titulo_seo; ?>"><?php echo $novidades[8]->titulo; ?></a></h3>
		          <p><?php echo $novidades[8]->resumo; ?></p>
		        </div>
				<hr>
				<div class="col-lg-6-col-md-6 col-sm-6 col-xs-6 text-center footer separador">
					<span class="data"><?php echo Funcoes::blog_data_mes_completo($novidades[8]->data); ?></span>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center footer">
					<a href="<?php echo SITE_URL."/blog/post/".$novidades[8]->titulo_seo; ?>" class="saiba-mais">Leia mais</a>
				</div>
			</div>
		</div>
		<?php } ?>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="paginacao">
				<ul>
					<?php

					$categoria_ativa = 'todos';

					 if(isset($_GET['p'])){ 
						if($_GET['p']==1){
					?>
						<li class="prev"><a href="<?php echo SITE_URL."/blog/categoria/".$categoria_ativa."?p=1"; ?>"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
					<?php
						}else{
					?>
						<li class="prev"><a href="<?php echo SITE_URL."/blog/categoria/".$categoria_ativa."?p=".($_GET['p']-1); ?>"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
					<?php
						}
						}else{
					?>		
						<li class="prev"><a href="<?php echo SITE_URL."/blog/categoria/".$categoria_ativa."?p=1"; ?>"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
					<?php
						}
					?>		

					<?php
					for($x=1; $x<=$paginacao; $x++){
						if(isset($_GET['p'])){
					?>
					<li <?php echo $x==$_GET['p'] ? 'class="active"' : ''; ?>><a href="<?php echo SITE_URL."/blog/categoria/".$categoria_ativa."?p=".$x; ?>"><?php echo $x; ?></a></li>
					<?php }else{ ?>
						<li <?php echo $x==1 ? 'class="active"' : ''; ?>><a href="<?php echo SITE_URL."/blog/categoria/".$categoria_ativa."?p=".$x; ?>"><?php echo $x; ?></a></li>
					<?php
					} } ?>

					<?php if(isset($_GET['p'])){ 
						if($_GET['p']==$paginacao){
					?>
						<li class="next"><a href="<?php echo SITE_URL."/blog/categoria/".$categoria_ativa."?p=".$paginacao; ?>"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
					<?php
						}else{
					?>
						<li class="next"><a href="<?php echo SITE_URL."/blog/categoria/".$categoria_ativa."?p=".($_GET['p']+1); ?>"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
					<?php
						}
						}else{
					?>		
						<li class="next"><a href="<?php echo SITE_URL."/blog/categoria/".$categoria_ativa."?p=".$paginacao; ?>"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
					<?php
						}
					?>	

				</ul>
			</div>
		</div>


	</section>

	<script type="text/javascript">
		/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
		var disqus_shortname = 'dietwinblog'; // required: replace example with your forum shortname

		/* * * DON'T EDIT BELOW THIS LINE * * */
		(function () {
		var s = document.createElement('script'); s.async = true;
		s.type = 'text/javascript';
		s.src = '//' + disqus_shortname + '.disqus.com/count.js';
		(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
		}());
	</script>


<?php require_once 'includes/footer.php' ?>
</body>
</html>