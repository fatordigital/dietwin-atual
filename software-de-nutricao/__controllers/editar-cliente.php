<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_EditarCliente extends Controller_Padrao
{
    /**
     * Chama o construtor da classe pai
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método inicial que faz a renderização básica da página
     * @param $parametros
     * @return void
     */
    public function index($parametros)
    {
        $view = new View('editar-cliente.php');
        $this->view_variaveis_obrigatorias($view);

        $estado = new Model_Estado();
        $estados = $estado->select('SELECT * FROM {tabela_nome} ORDER BY id',TRUE);
        $view->adicionar('estados', $estados);

        $cidade = new Model_Cidade();
        $cidades_principal = $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id=1 ORDER BY id',TRUE);
        $view->adicionar('cidades_principal',$cidades_principal);

        $cidade = new Model_Cidade();
        $cidades_entrega = $cidade->select('SELECT * FROM {tabela_nome} WHERE estado_id=1 ORDER BY id',TRUE);
        $view->adicionar('cidades_entrega',$cidades_entrega);

        $view->adicionar('notificacao', new Notificacao);
        $view->adicionar('body_class', 'area-cliente');
        $view->adicionar('pagina_title', 'Editar cliente - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        $view->exibir();
    }

    /* ***************************** MÉTODOS EXTRAS ***************************** */



} // end class