<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<?php require_once 'includes/head.php' ?>
<body>
<?php require_once 'includes/nav.php' ?>

<section class="conteudo cinza">
  <div class="container">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <h1 class="interna">Comprar o <span class="verde">Diet</span><span class="azul">win</span> Plus</h1>
      <p>A praticidade de comprar online, com toda a comodidade e segurança que você precisa. Aproveite nossas promoções e adquira hoje mesmo a sua versão do dietWin.</p>
    </div>
    <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
      <img src="<?php echo SITE_URL ?>/views/imagens/caixas-dietwin-compra.jpg" alt="dietWin Plus e dietWin Tradicional" class="img-responsive">
    </div>
  </div>
</section>
<section class="conteudo cinza">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="sobre-dietwin-plus">
        <div class="row">
          <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
            <img src="<?php echo SITE_URL ?>/views/imagens/demo-dietwin-plus.jpg" alt="dietwin Plus" class="img-responsive">
          </div>
          <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
            <h2><span class="verde">Diet</span><span class="azul">win</span> Plus</h2>
            <p>O objetivo principal do Dietwin Plus é auxiliar o nutricionista na tomada de decisão, monitorando informações através do relacionamento de resultados obtidos para análise das avaliações executadas. Além disso, gera subsídios para facilitar o diagnóstico nutricional.
O Dietwin Plus é indicado para um atendimento mais completo.</p>
            <p class="aviso" style="background-color:#D9710E;"><b>Aviso importante</b>: Esta versão encontra-se em desenvolvimento. Atualizações frequentes serão disponibilizadas até a sua conclusão, por esta razão sua avaliação e sugestões são muito bem vindas para melhoria do sistema.</p>

            <p><strong>Termo de Aceite Dietwin e Dietwin Plus</strong></p>

            <p>
              • As versões Dietwin e Dietwin Plus têm suas ferramentas baseadas nos procedimentos padrões adotadas na nutrição, sendo seu conhecimento necessário para sua utilização. Todas as referências de equações e protocolos utilizadas no Dietwin são de domínio público na nutrição e estão citadas dentro do sistema, seguindo o padrão de avaliação e análises nutricionais conhecidos. <br>
              • Disponibilizamos suporte técnico gratuito para as funcionalidades da ferramenta com pessoal especializado: via e-mail; <Br />
              • O Dietwin disponibiliza uma ajuda online com alguns vídeos e telas de operação, através do link: <a href="http://atendimento.dietwin.com.br">http://atendimento.dietwin.com.br</a> na aba Dietwin; <br>
              • Disponibilizamos uma apresentação do software online (*), realizado por um nutricionista do suporte mediante agendamento de um horário compatível entre as partes. O Dietwin não oferece treinamento, mas sim esclarecimento de dúvidas através de e-mails; <br>
              • Suporte via acesso remoto assistido (*): Quando necessário, o suporte realiza um acesso remoto, mediante agendamento de um horário compatível entre as partes, para solução de problemas que não conseguiram ser resolvidos através dos procedimentos liberados. Este acesso é realizado por um técnico de suporte especializado e mediante a presença assistida do usuário. <br>
              <strong>(*) Observação:</strong> O programa Team Viewer é necessário para apresentação do sistema online e para executar o procedimento de acesso remoto mencionados anteriormente.              
            </p>

            <p style="text-align:right; padding-right:20px;">              
              <input type="checkbox" name="termos" id="termos" />
              <label for="termos">Aceito os Termos</label>
            </p>

          </div>
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
            <div class="contatos-duvidas">
              <a href="#" title="Clique aqui e fale com nossos atendentes">
                <img src="<?php echo SITE_URL ?>/views/imagens/atendentes-off.png" alt="No momento nossos atendentes estão offline">
              </a>
              <a href="#" title="Solicite maiores informações sobre o dietWin">
                <img src="<?php echo SITE_URL ?>/views/imagens/solicitar-informacoes.png" alt="Solicite maiores informações sobre o dietWin">
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="observacao-importante">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>Importante:</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <ul>
              <li>Ao comprar o dietwin você estará adquirindo um software que permite a instalação em <strong>um único computador</strong>.</li>
              <li>Se deseja instalar em mais de 1 computador (em sua casa e no consultório, por exemplo), <a href="//www.dietwin.com.br/orcamento" title="Clique aqui">clique aqui</a> e solicite uma proposta personalizada.</li>
              <li>Para cada computador será gerado um número de série diferente.<br />Não pode ser dividido com outra pessoa, somente para o mesmo CPF.</li>
            </ul>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center">
            <div class="preco">
              <span class="titulo">A partir de</span>
              <span class="moeda">R$</span><span class="reais"><?php echo str_replace('.00','',(string)$dietwin_valor); ?>,</span><span class="centavos">00</span>
            </div>
          </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center">

                <form action="<?php echo SITE_URL ?>/cupom" method="post" id="cupom_form">
                    <div>
                        <strong>Você possui um cupom?</strong>
                        <input type="text" name="codigo" value="" placeholder="Digite seu cupom"/>
                        <input type="hidden" name="produto_seo" value="dietwin"/>
                    </div>
                    <div>
                        <a href="javascript:verificarCupom();">Verificar Cupom</a>
                    </div>
                    <div class="cupom_mensagem">

                    </div>
                </form>

            </div>
        </div>
      </div>
      <div class="prosseguir">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/compra-segura-pagseguro.png" alt="Compra 100% segura - PagSeguro">
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs text-center">
            <img src="<?php echo SITE_URL ?>/views/imagens/formas-de-pagamento.png" alt="Compra 100% segura - PagSeguro" class="cartoes-compra">
          </div>
          <div class="col-lg-3 col-md-3 col-sm-4 hidden-xs pull-right">
            <input onclick="javascript:verificaTermos()" type="submit" value="Prosseguir comprando" class="processo-compra-submit">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">

    function verificaTermos(){
      if($("#termos").is(':checked')) {
          location.href='<?php echo SITE_URL ?>/comprar/dietwin';        
      } else {
          alert("Por favor, leia e aceite os termos.");
      }    
    }

    var produto_preco = <?php echo $dietwin_valor; ?>;

    $(document).ready(function(){
        $('#cupom_form').bind("keypress", function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });
    });

    function verificarCupom(){
        $.ajax({
            type: "POST",
            data: $("#cupom_form").serialize(),
            url: "<?php echo SITE_URL ?>/cupom",
            dataType: 'text',
            success: function(data){
                if(data!=0){
                    $('.cupom_mensagem').text('Cupom válido!');
                    $('.preco .titulo').text('Desconto de R$ '+data+',00');
                    var total = produto_preco-data;
                    $('.preco .reais').text(total);
                    $('.cupom_mensagem').css({'background-color':'#59B200'}).fadeIn();
                }else{
                    $('.cupom_mensagem').text('Cupom inválido.');
                    $('.cupom_mensagem').css({'background-color':'#D90000'}).fadeIn();
                }
            }
        });
    }

</script>

<?php require_once 'includes/footer.php' ?>
</body>
</html>
