$(document).ready(function($) {	

	$('html, body').animate({
        scrollTop: 0
    }, 1200);

	if($(window).width() > 768){
		$('#fullpage').fullpage({
	        anchors: ['header', 'dietwin', 'dietwin-profissional', 'tabela-nutricional','tabela-nutricional-funcionalidades', 'contato'],
	        verticalCentered:false,
	        scrollBar: true
	    });

	    srcMonitor();
	    animateFruit();
		animateImgMonito();
		menuFixed();	
	}

	zSlider();
	zSlider2();
	scrollNavegacao();
	btnLinkHeader();

	/** Mascaras de campos **/
	$('#form1 input[name="telefone"]').mask("(99) 9999-9999?9").focusout(function (event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        element.unmask();
        if(phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    });

	var fomulario1 = $('#form1').validate({
		rules:{
			nome:{
				required: true
			},
			email:{
				required: true,
				email: true,
			},
			telefone:{
				required: true,
			},
			mensagem:{
				required: true,
			}
		},
		submitHandler: function(form) {
			$.ajax({
                 type: "POST",
                 url: "contato.php",
                 data: $(form).serialize(),
                 success: function () {
                 	//ga('send','pageview','/goal/whats/home');                    
                    fomulario1.resetForm();   
                    form.reset();
                    
                    $('#form1 p.msg-form').addClass('active');
                    setTimeout(function(){ $('#form1 p.msg-form').removeClass('active');}, 5000);
                 },
                 error: function (error) {
                     console.log(error);
                 }
            });
            return false;
		}	
	});

	var fomulario2 = $('#fomr2').validate({
		rules:{
			newsEmail:{
				required: true
			}
		},
		submitHandler: function(form) {
            var validator = this;
			$.ajax({
                 type: "POST",
                 url: "/newsletter.php",
                 data: $(form).serialize(),
                 success: function (r) {
                 	ga('send','pageview','/goal/newsletter');
                    fomulario2.resetForm();   
                    form.reset();
                    
                    $('#form2 p.msg-form').addClass('active');
                    setTimeout(function(){ $('#form1 p.msg-form').removeClass('active');}, 5000);
                 },
                 error: function(r) {
                     validator.showErrors({
                         newsEmail: r.responseJSON.msg
                     });
                 }
            });
            return false;
		}	
	});

	var linkMenu = $('.nav.navbar-nav.navbar-right li a.aaa');
	linkMenu.css({'display': 'none'});
	$(window).on('scroll', function () {
		if($('.navbar.navbar-default').hasClass('fixed')){
			$(linkMenu).fadeIn(300);
		}else{
			$(linkMenu).fadeOut(300);
		}
    });
});	


function zSlider(){
	$(".my-control").on('click', function(event) {
		event.preventDefault();
		if($(this).hasClass('left-control')){
			$('.item').removeClass('active');
			setTimeout(function(){$('#funcionalidades').addClass('active');},500);
		}
		else{
			$('.item').removeClass('active');
			setTimeout(function(){$('#calculo-dietas').addClass('active');},500);
		}
	});
}

function zSlider2(){
    $(".my-control").on('click', function(event) {
        event.preventDefault();
        if($(this).hasClass('left-control')){
            $('.item').removeClass('active');
            setTimeout(function(){$('#funcionalidades-2').addClass('active');},500);
        }
        else{
            $('.item').removeClass('active');
            setTimeout(function(){$('#calculo-dietas-2').addClass('active');},500);
        }
    });
}

function animateFruit(){
	var offsetElement = $('.tabela-nutricional').offset().top;
	var ulWhite  = $('ul.animation-white');
	var ulOrange = $('ul.animation-orange');
	var celular  = $('div.celular');

	$(window).scroll(function () { 
		if ($(this).scrollTop() >= $('#section4').offset().top) { 
			ulWhite.addClass("active"); 


			setTimeout(function(){ $('ul.animation-white li:nth-of-type(1)').css('opacity', 1);}, 300);
			setTimeout(function(){ $('ul.animation-white li:nth-of-type(2)').css('opacity', 1);}, 600);
			setTimeout(function(){ $('ul.animation-white li:nth-of-type(3)').css('opacity', 1);}, 900);
			setTimeout(function(){ $('ul.animation-white li:nth-of-type(4)').css('opacity', 1);}, 1200);
			setTimeout(function(){ $('ul.animation-white li:nth-of-type(5)').css('opacity', 1);}, 1500);

			setTimeout(function(){ $('ul.animation-orange li:nth-of-type(1)').css('opacity', 1);}, 2000);
			setTimeout(function(){ $('ul.animation-orange li:nth-of-type(2)').css('opacity', 1);}, 2300);
			setTimeout(function(){ $('ul.animation-orange li:nth-of-type(3)').css('opacity', 1);}, 2700);
			setTimeout(function(){ $('ul.animation-orange li:nth-of-type(4)').css('opacity', 1);}, 3000);
			setTimeout(function(){ $('ul.animation-orange li:nth-of-type(5)').css('opacity', 1);}, 3300);			
		}
		else { 
			ulWhite.removeClass("active"); 
			ulOrange.removeClass("active"); 
			$('ul.animation-white li').css('opacity', 0);
			$('ul.animation-orange li').css('opacity', 0);
		} 
	}); 
}

function animateImgMonito(){
	var elementoMonitor = $('.box-monitor');
	var section;

	section = [
		$('div.header').offset().top, 
		$('div.section:nth-of-type(1)').offset().top,
		$('div.section:nth-of-type(2)').offset().top,
		$('div.section:nth-of-type(3)').offset().top,
		$('div.section:nth-of-type(4)').offset().top,
		$('div.section:nth-of-type(5)').offset().top,
		$('div.footer').offset().top
	];		

	$(window).scroll(function () {

		var alturaSection = ($('div.section.active').height() / 2);
		var sectionAtiva  = $('div.section.active').offset().top;
		var move = (alturaSection + sectionAtiva) - 255;

		for (var i = 0; i < section.length; i++) {		
			if(section[i]  >= sectionAtiva) {
				indice = i;				
				break;
			}
		}

		if(indice > 2 && indice < 6){
			$('.box-monitor').css({
				top: move+'px',
			});	

		}
	});
}

function menuFixed(){

	$(window).scroll(function () {
		if($(this).scrollTop() > 500){	
			if($(this).scrollTop() < $('div.dietwin').offset().top){
				$('nav.navbar-default').addClass('myOpacity');
				setTimeout(function(){ $('nav.navbar-default').addClass('fixed');},100);
				setTimeout(function(){ $('nav.navbar-default').removeClass('myOpacity'); },1000);
			}					
		}else{
			$('nav.navbar-default').addClass('myOpacity');			
			$('nav.navbar-default').removeClass('fixed');
			setTimeout(function(){ $('nav.navbar-default').removeClass('myOpacity');},400);
		}		
	});
}

function srcMonitor(){
	$(window).scroll(function () {
		var srcImg = $('.box-monitor img').attr('src').split('/');
		var stringSrcImg = '';

		if($("#section3").hasClass('active')){			
			srcImg[srcImg.length - 1] = 'monitor-1.png';
			for(i = 0 ; i < srcImg.length; i++){
				if(i == 0){
					stringSrcImg = stringSrcImg + srcImg[i];
				}
				else{
					stringSrcImg = stringSrcImg + '/' + srcImg[i];
				}
			}
			setTimeout(function(){$('.box-monitor img').attr('src', stringSrcImg);},500);
		}
		else{
			srcImg[srcImg.length - 1] = 'monitor.png';
			for(i = 0 ; i < srcImg.length; i++){
				if(i == 0){
					stringSrcImg = stringSrcImg + srcImg[i];
				}
				else{
					stringSrcImg = stringSrcImg + '/' + srcImg[i];
				}
			}
			setTimeout(function(){$('.box-monitor img').attr('src', stringSrcImg);},500);
		}
	});
}

function scrollNavegacao(){
	$('#section0 .navbar-right a').not('.is_link').on('click', function(event) {
		event.preventDefault();
		var link = $(this).attr('href');
		
		$('html,body').animate({
			scrollTop: $(link).offset().top
		}, 1200);
	});
}

function btnLinkHeader(){
	$('#link-prof').on('click', function(event) {
		event.preventDefault();
		var link = $(this).attr('href');

		$('html,body').animate({
			scrollTop: $(link).offset().top
		}, 1200);
	});
}