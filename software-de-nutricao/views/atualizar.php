<?php require_once 'includes/head.php' ?>
<body>
<?php require_once 'includes/nav.php' ?>

<section class="conteudo cinza">
  <div class="container">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <h1 class="interna" style="font-size:48px;">Faça o download do Dietwin Atualizador 3090. <br>Siga os procedimentos recebido no email.</h1>
      <p><b>Aguarde o download iniciar, caso não inicie, <a href="<?php echo SITE_URL ?>/downloads/atualizar_produto">clique aqui</a> .</b></p>
    </div>
    <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
      <img src="<?php echo SITE_URL ?>/views/imagens/caixas-dietwin-compra.jpg" alt="dietWin Plus e dietWin Tradicional" class="img-responsive">
    </div>
  </div>
</section>

<script type="text/javascript">
	setTimeout(function(){
		window.location = "<?php echo SITE_URL ?>/downloads/atualizar_produto";
	}, 3000);
</script>
<?php require_once 'includes/footer.php' ?>
</body>
</html>
