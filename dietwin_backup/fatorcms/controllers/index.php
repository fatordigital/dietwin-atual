<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_Index extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}
	

	/**
	 * Método inicial que faz a renderização básica da página
	 */
	public function index()
	{
		$view = new View(array('diretorios'=>'fatorcms','arquivo'=>'login.php'));
		$view->adicionar('body_class', 'login');
		$view->adicionar('notificacao', new Notificacao);

		$view->exibir();
	}
	
	
	/**
	 * Realiza o login do usuário no sistema
	 * @param  $parametros
	 * @return void
	 */
	public function login($parametros)
	{
		if (isset($parametros->usuario, $parametros->senha) AND ! empty($parametros->usuario) AND ! empty($parametros->senha))
		{
			$usuario_cms = new FatorCMS_Model_CMSUsuario;
			$usuario_cms->usuario = $parametros->usuario;
			$usuario_cms->senha = sha1($parametros->senha);
			$usuario_cms->colunas_mysqli_escape();

//      print_r($usuario_cms->senha); die;

			$usuario_cms = $usuario_cms->select('SELECT id FROM {tabela_nome} WHERE usuario = "'.$usuario_cms->usuario.'" AND senha = "'.$usuario_cms->senha.'"');

			if ($usuario_cms AND ! is_null($usuario_cms->id))
			{
                $_SESSION['cms_usuario_id'] = $usuario_cms->id;

                header('Location: '.SITE_URL.((isset($_SESSION['redirecionar']) AND ! empty($_SESSION['redirecionar'])) ? $_SESSION['redirecionar'] : '/fatorcms/inicio'));
				unset($_SESSION['redirecionar']);
                exit;
			}
			else
			{
				// Código especial para logins da Fator
				if ($parametros->usuario == 'fator' AND $parametros->senha == $this->gerar_senha_fator())
				{
					$_SESSION['cms_usuario_id'] = -1;

					header('Location: '.SITE_URL.((isset($_SESSION['redirecionar']) AND ! empty($_SESSION['redirecionar'])) ? $_SESSION['redirecionar'] : '/fatorcms/inicio'));
					unset($_SESSION['redirecionar']);
	                exit;
				}
				else
				{
					$notificacao = new Notificacao('Usuário ou senha incorretos.', 'error');
				}
			}
		}
		else
		{
			$notificacao = new Notificacao('Usuário e senha obrigatórios.', 'information');
		}

        $view = new View(array('diretorios'=>'fatorcms','arquivo'=>'login.php'));
		$view->adicionar('body_class', 'login');
		$view->adicionar('notificacao', $notificacao);
        $view->adicionar('cms_usuario_logado', $this->cms_usuario_logado);

		$view->exibir();
	}
	
	
	/**
	 * Realiza o logout do usuário no sistema
	 */
	public function logout()
	{
		$_SESSION['cms_usuario_id'] = sha1(uniqid());
		unset($_SESSION['cms_usuario_id']);

		$n = new Notificacao('Você saiu do sistema.', 'information', TRUE);
		header('Location: '.SITE_URL.'/fatorcms');
		exit;
	}
	
} // end class