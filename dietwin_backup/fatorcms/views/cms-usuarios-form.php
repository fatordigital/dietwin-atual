<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.'); ?>

<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php'; ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

        <!-- Page Head -->
		<?php if (is_null($cms_usuario) OR is_null($cms_usuario->id)) { ?>
			<h2>Cadastro de um Usuário</h2>
			<p id="page-intro">Utilize o formulário abaixo para incluir um usuário no sistema.</p>
		<?php } else { ?>
			<h2>Edição de um Usuário</h2>
			<p id="page-intro">Utilize o formulário abaixo para alterar os dados de um usuário.</p>
		<?php } ?>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<?php if (is_null($cms_usuario) OR is_null($cms_usuario->id)) { ?>
					<h3>Dados de um usuário</h3>
				<?php } else { ?>
					<h3>Informações do usuário <?php echo $cms_usuario ? $cms_usuario->nome : '' ?></h3>
				<?php } ?>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<form action="<?php echo SITE_URL ?>/fatorcms/cms-usuarios/<?php echo (is_null($cms_usuario) OR is_null($cms_usuario->id)) ? 'cadastrar' : 'atualizar' ?>" method="post" id="cms_usuario_form">

					<fieldset class="column-left">

						<?php if ( ! is_null($cms_usuario)) { ?>
							<input type="hidden" name="id" value="<?php echo $cms_usuario->id ?>" />
						<?php } ?>
						<p>
							<label for="nome">Nome</label>
							<input class="text-input medium-input required" type="text" id="nome" name="nome" maxlength="50" value="<?php echo $cms_usuario ? $cms_usuario->nome : '' ?>" />
						</p>

						<p>
							<label for="usuario">Usuário</label>
							<input class="text-input medium-input required" type="text" id="usuario" name="usuario" maxlength="50" value="<?php echo $cms_usuario ? $cms_usuario->usuario : '' ?>" />
						</p>

						<p>
							<label for="senha">Senha</label>
							<input class="text-input medium-input <?php echo (is_null($cms_usuario) OR is_null($cms_usuario->id)) ? 'required' : '' ?> " type="password" id="senha" name="senha" maxlength="100" />
						</p>

					</fieldset>

					<fieldset class="column-right">

						<?php if ($cms_usuario_logado->eh_administrador) { ?>

							<p>
								<label for="cms_modulos_ids">Permissão de acesso para os seguintes módulos</label>
								<select name="cms_modulos_ids[]" id="cms_modulos_ids" class="large-input" multiple="multiple" size="7">
									<?php
									if ($cms_modulos AND count($cms_modulos) > 0)
									{
										foreach ($cms_modulos as $cms_modulo)
										{
											$selected = FALSE;
											foreach ($cms_usuario_permissoes as $cms_usuario_permissao)
											{
												if (intval($cms_modulo->id) == intval($cms_usuario_permissao->cms_modulo_id))
												{
													$selected = TRUE;
													break;
												}
											}
											echo '<option value="'.$cms_modulo->id.'" '.($selected ? 'selected="selected"' : '').'>'.$cms_modulo->nome.'</option>';
										}
									}
									?>
								</select><br />
								<small>Trabalhe com as teclas <strong>Ctrl</strong> e <strong>Shift</strong> para a múltipla seleção de itens.</small>
							</p>


							<p>
								<label for="eh_administrador_1">É administrador?</label>
								<input type="radio" name="eh_administrador" id="eh_administrador_1" value="1" <?php echo ($cms_usuario AND $cms_usuario->eh_administrador) ? 'checked="checked"' : '' ?> /> <span class="sim">Sim</span>
								<input type="radio" name="eh_administrador" id="eh_administrador_0" value="0" <?php echo ( ! $cms_usuario OR ! $cms_usuario->eh_administrador) ? 'checked="checked"' : '' ?> /> <span class="nao">Não</span>
							</p>

						<?php } ?>

					</fieldset>

					<fieldset class="clear">

						<p>
							<input class="button" type="submit" value="<?php echo (is_null($cms_usuario) OR is_null($cms_usuario->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
						</p>

					</fieldset>

					<div class="clear"></div><!-- End .clear -->

				</form>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->
		
<?php require_once 'fatorcms/views/includes/rodape.php' ?>