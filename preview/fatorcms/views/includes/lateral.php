<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.'); ?>

	<div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

		<h1 id="sidebar-title"><a href="<?php echo SITE_URL ?>/fatorcms/inicio">FatorCMS</a></h1>

		<!-- Logo (221px wide) -->
		<a href="<?php echo SITE_URL ?>/fatorcms/inicio"><img id="logo" src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/logo.png" alt="FatorCMS logo" /></a>

		<!-- Sidebar Profile links -->
		<div id="profile-links">
			Olá, <?php echo $cms_usuario_logado->nome ?><br />
			<br />
			<a href="<?php echo SITE_URL ?>" title="Voltar para a capa do dietWin">Voltar para o site</a> | <a href="<?php echo SITE_URL ?>/fatorcms/index/logout" title="Realizar logout">Logout</a>
		</div>

		<ul id="main-nav">  <!-- Accordion Menu -->

			<li>
				<a class="nav-top-item no-submenu <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'inicio') ? 'current' : '' ?>" href="<?php echo SITE_URL ?>/fatorcms/inicio">Início</a>
			</li>

			<?php if ($cms_usuario_logado->eh_administrador OR in_array('clientes', $cms_usuario_logado_modulos)) { ?>
		    <li>
				<a href="#" class="nav-top-item <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'clientes') ? 'current' : '' ?>">
					Clientes
				</a>
				<ul>
					<li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'clientes' AND $lateral_menu['link'] == 'listar todos') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/clientes/listar">Listar todos</a></li>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'clientes' AND $lateral_menu['link'] == 'cadastrar') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/clientes/cadastrar">Cadastrar</a></li>
				</ul>
			</li>
			<?php } ?>

            <?php if ($cms_usuario_logado->eh_administrador OR in_array('prospects', $cms_usuario_logado_modulos)) { ?>
                <?php /*<li>
                    <a href="#" class="nav-top-item <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'prospects') ? 'current' : '' ?>">
                        Prospects
                    </a>
                    <ul>
                        <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'prospects' AND $lateral_menu['link'] == 'listar todos') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/prospects/listar">Listar todos</a></li>
                        <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'prospects' AND $lateral_menu['link'] == 'cadastrar') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/prospects/cadastrar">Cadastrar</a></li>
                    </ul>
                </li> */ ?>
                <?php } ?>


			<?php if ($cms_usuario_logado->eh_administrador OR in_array('interacoes', $cms_usuario_logado_modulos)) { ?>
		    <li>
				<a href="<?php echo SITE_URL ?>/fatorcms/interacoes/listar" class="nav-top-item no-submenu <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'interacoes') ? 'current' : '' ?>">
					Interações
				</a>
			</li>
			<?php } ?>
			


			<?php if ($cms_usuario_logado->eh_administrador OR in_array('compras', $cms_usuario_logado_modulos)) { ?>
            <li>
                <a href="<?php echo SITE_URL ?>/fatorcms/compras/listar" class="nav-top-item no-submenu <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'compras') ? 'current' : '' ?>">
					Compras
				</a>
			</li>
	        <?php } ?>


			<?php if ($cms_usuario_logado->eh_administrador OR in_array('pagseguro', $cms_usuario_logado_modulos)) { ?>
			<li>
                <a href="#" class="nav-top-item <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'pagseguro') ? 'current' : '' ?>">
					PagSeguro
				</a>
				<ul>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'pagseguro' AND $lateral_menu['link'] == 'aguardando') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/pagseguro/aguardando">Aguardando Pagamento</a></li>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'pagseguro' AND $lateral_menu['link'] == 'analise') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/pagseguro/analise">Em análise</a></li>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'pagseguro' AND $lateral_menu['link'] == 'pagas') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/pagseguro/pagas">Pagas</a></li>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'pagseguro' AND $lateral_menu['link'] == 'disponiveis') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/pagseguro/disponiveis">Disponíveis</a></li>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'pagseguro' AND $lateral_menu['link'] == 'disputa') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/pagseguro/disputa">Em disputa</a></li>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'pagseguro' AND $lateral_menu['link'] == 'devolvidas') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/pagseguro/devolvidas">Devolvidas</a></li>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'pagseguro' AND $lateral_menu['link'] == 'canceladas') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/pagseguro/canceladas">Canceladas</a></li>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'pagseguro' AND $lateral_menu['link'] == 'listar todas') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/pagseguro/listar">Listar todas</a></li>
                </ul>
			</li>
			<?php } ?>


            <?php if ($cms_usuario_logado->eh_administrador OR in_array('produtos', $cms_usuario_logado_modulos)) { ?>
            <li>
                <a href="#" class="nav-top-item <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'produtos') ? 'current' : '' ?>">
                    Produtos
                </a>
                <ul>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'produtos' AND $lateral_menu['link'] == 'listar todos') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/produtos/listar">Listar todos</a></li>
                    <?php /* <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'produtos' AND $lateral_menu['link'] == 'cadastrar') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/produtos/cadastrar">Cadastrar</a></li> */ ?>
                </ul>
            </li>
            <?php } ?>


            <?php if ($cms_usuario_logado->eh_administrador OR in_array('cupons', $cms_usuario_logado_modulos)) { ?>
            <li>
                <a href="#" class="nav-top-item <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'cupons') ? 'current' : '' ?>">
                    Cupons
                </a>
                <ul>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'cupons' AND $lateral_menu['link'] == 'listar todos') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/cupons/listar">Listar todos</a></li>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'cupons' AND $lateral_menu['link'] == 'cadastrar') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/cupons/cadastrar">Cadastrar</a></li>
                </ul>
            </li>
            <?php } ?>


            
			<?php if ($cms_usuario_logado->eh_administrador OR in_array('novidades', $cms_usuario_logado_modulos)) { ?>
            <li>
				<a href="#" class="nav-top-item <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'novidades') ? 'current' : '' ?>">
					Blog
				</a>
				<ul>
					<li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'novidades' AND $lateral_menu['link'] == 'listar todas') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/novidades/listar">Listar Posts</a></li>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'novidades' AND $lateral_menu['link'] == 'cadastrar') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/novidades/cadastrar">Cadastrar Post</a></li>

                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'novidades' AND $lateral_menu['link'] == 'listar todas categorias') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/novidades/categorias">Listar todas Categorias</a></li>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'novidades' AND $lateral_menu['link'] == 'cadastrar categorias') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/novidades/categorias-cadastrar">Cadastrar Categoria</a></li>
				</ul>
			</li>
	        <?php } ?>

	        <!--


			<?php if ($cms_usuario_logado->eh_administrador OR in_array('multimidia', $cms_usuario_logado_modulos)) { ?>
            <li>
				<a href="#" class="nav-top-item <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'multimidia') ? 'current' : '' ?>">
					Multimídia
				</a>
				<ul>
					<li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'multimidia' AND $lateral_menu['link'] == 'listar galerias') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/galerias/listar">Listar as galerias de fotos</a></li>
					<li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'multimidia' AND $lateral_menu['link'] == 'cadastrar galeria') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/galerias/cadastrar">Cadastrar uma galeria de fotos</a></li>
					<li class="separador"><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'multimidia' AND $lateral_menu['link'] == 'listar videos') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/videos/listar">Listar todos os vídeos</a></li>
					<li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'multimidia' AND $lateral_menu['link'] == 'cadastrar video') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/videos/cadastrar">Cadastrar um vídeo</a></li>
				</ul>
			</li>
	        <?php } ?>
	        -->

            <?php if ($cms_usuario_logado->eh_administrador OR in_array('area-cliente', $cms_usuario_logado_modulos)) { ?>
                <li>
                    <a href="#" class="nav-top-item <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'area-cliente') ? 'current' : '' ?>">
                        Área do Cliente
                    </a>
                    <ul>
                        <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'area-cliente' AND $lateral_menu['link'] == 'listar arquivos') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/arquivos/listar">Listar os arquivos</a></li>
                        <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'area-cliente' AND $lateral_menu['link'] == 'cadastrar arquivo') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/arquivos/cadastrar">Cadastrar um arquivo</a></li>
                    </ul>
                </li>
            <?php } ?>

            <!--
			<?php if ($cms_usuario_logado->eh_administrador OR in_array('faq', $cms_usuario_logado_modulos)) { ?>
			<li>
				<a href="#" class="nav-top-item <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'faq') ? 'current' : '' ?>">
					FAQ
				</a>
				<ul>
					<li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'faq' AND $lateral_menu['link'] == 'listar perguntas') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/faq/listar">Listar perguntas</a></li>
                    <li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'faq' AND $lateral_menu['link'] == 'cadastrar pergunta') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/faq/cadastrar">Cadastrar pergunta</a></li>
				</ul>
			</li>
			<?php } ?>
			-->


			<?php if ($cms_usuario_logado->eh_administrador OR in_array('usuarios-fatorcms', $cms_usuario_logado_modulos)) { ?>
            <li>
				<a href="#" class="nav-top-item <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'usuarios-fatorcms') ? 'current' : '' ?>">
					Usuários FatorCMS
				</a>
				<ul>
					<li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'usuarios-fatorcms' AND $lateral_menu['link'] == 'listar todos') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/cms-usuarios/listar">Listar todos</a></li>
					<?php if ($cms_usuario_logado->eh_administrador) { ?>
						<li><a <?php echo (isset($lateral_menu['item']) AND $lateral_menu['item'] == 'usuarios-fatorcms' AND $lateral_menu['link'] == 'cadastrar') ? 'class="current"' : '' ?> href="<?php echo SITE_URL ?>/fatorcms/cms-usuarios/cadastrar">Cadastrar</a></li>
					<?php } ?>
				</ul>
			</li>
	        <?php } ?>

		</ul> <!-- End #main-nav -->

	</div></div> <!-- End #sidebar -->