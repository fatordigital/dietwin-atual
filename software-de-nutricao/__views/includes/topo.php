
<div class="topo">
                <script type="text/javascript">
                (function() {
                    var wc = document.createElement('script'); wc.type = "text/javascript";
                    wc.src = 'https://dietwin.webchatlw.com.br/widget.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(wc, s);
                })();
            </script>
<!-- inicio do widget do Webchat Locaweb -->
    <div class="container">
    	<div class="logo">
        	<a href="<?php echo SITE_URL ?>/index" title="dietWin - Softwares de Nutrição"><img src="<?php echo SITE_BASE ?>/views/imagens/dietwin-logo.png" alt="dietWin - Softwares de Nutrição" /></a>
        </div>

<!-- Visualização sem estar logado -->
<?php if (!isset($_SESSION['cliente_id'])) { ?>
            <div class="topo-login">
                <h5>Área do Cliente</h5>

                <form action="<?php echo SITE_URL.$_SERVER['REQUEST_URI'] ?>" method="post" id="form-topo-login">
                    <div class="input-bg">
                        <input type="text" name="email" id="email-login" class="input-login" value="E-mail" />
                    </div>
                    <div class="input-bg-senha">
                        <input type="password" name="senha" id="senha-login" class="input-login" value="Senha" />
                        <input type="submit" name="login" id="form-topo-login-enviar" class="btn-login" value="">
                    </div>
                </form>
                <div class="clear"></div>
                <div id="form-topo-notification" class=""></div>
                <br/>
                <a href="<?php echo SITE_URL ?>/esqueci-senha" class="link-topo-login">• Esqueci a minha senha »</a><br/>
                <a href="<?php echo SITE_URL ?>/cadastro-cliente" class="link-topo-login">• Quero me cadastrar »</a>
            </div>
<?php } else { ?>

<!-- Visualização para cliente logado -->
        <div class="topo-login cliente-on">
           <p>Seja bem vindo, <span><?php echo $cliente_login->nome; ?></span></p>

            <a href="<?php echo SITE_URL ?>/area-do-cliente" class="link-clienteon">Área do Cliente</a>
            <a href="<?php echo SITE_URL.'/logout' ?>" class="link-clienteon">Logout</a>
        </div>
<?php } ?>


        <div class="facebook">
        	<a href="http://www.facebook.com/dietwin.softwares" target="_blank" title="Acompanhe a dietWin no Facebook"><img src="<?php echo SITE_BASE ?>/views/imagens/facebook.png" alt="Acompanhe a dietWin no Facebook" /></a>
        </div>

        <div class="chat2">
            <?php if(!isset($_COOKIE['chat']) OR $_COOKIE["chat"] == 'false'){ ?>
                <img src="<?php echo SITE_BASE ?>/views/imagens/fechar-chat.png" alt="Fechar o chat" class="close-chat" onClick="wc_popup()">
            <?php } ?>
            <a href="javascript:wc_popup();" title="Converse conosco">
                <img src="<?php echo SITE_BASE ?>/views/imagens/chat.png" alt="Converse conosco" class="chat-bubble">
            </a>
        </div>


        <ul class="menu">
        	<li class="institucional"><a href="<?php echo SITE_URL ?>/institucional" title="Institucional da dietWin"><img src="<?php echo SITE_BASE ?>/views/imagens/menu-institucional.png" alt="Institucional da dietWin" /></a></li>
            <li class="softwares mais-opcoes">
                <a href="<?php echo SITE_URL ?>/softwares" class="menu-link" title="Softwares da dietWin">
                    <img src="<?php echo SITE_BASE ?>/views/imagens/menu-softwares.png" alt="Softwares da dietWin" />
                </a>

                <div class="box-menu">
                    <ul class="submenu_suporte">
                        <li><a class="link-conteudo profissional" href="<?php echo SITE_URL ?>/profissional" title="Profissional ">Profissional Plus</a></li>
                        <li><a class="link-conteudo personal" href="<?php echo SITE_URL ?>/personal" title="Personal ">Personal </a></li>
                        <li><a class="link-conteudo rotulo-de-alimentos" href="<?php echo SITE_URL ?>/rotulo-de-alimentos" title="Rótulo de Alimentos">Rótulo de Alimentos</a></li>
                        <li><a class="link-conteudo versao" href="<?php echo SITE_URL ?>/qual-versao-escolho" title="Qual versão eu escolho?">Qual versão eu escolho?</a></li>
                    </ul>
                </div>

            </li>
            <li class="suporte">
                <a href="<?php echo SITE_URL ?>/suporte" class="menu-link_suporte" title="Suporte da dietWin">
                    <img src="<?php echo SITE_BASE ?>/views/imagens/menu-suporte.png" alt="Suporte da dietWin" />
                </a>
                <div class="box-menu_suporte">
                    <ul class="submenu">
                        <li><a class="link-conteudo versao titulo_faq" href="<?php echo SITE_URL ?>/faq" title="Faq dietWin">Faq</a></li>
                    </ul>
                </div>
            </li>
            <li class="contato"><a href="<?php echo SITE_URL ?>/contato" title="Contato da dietWin"><img src="<?php echo SITE_BASE ?>/views/imagens/menu-contato.png" alt="Contato da dietWin" /></a></li>
        </ul>
    
<script type="text/javascript" src="//assets.zendesk.com/external/zenbox/v2.6/zenbox.js"></script>
<style type="text/css" media="screen, projection">
  @import url(//assets.zendesk.com/external/zenbox/v2.6/zenbox.css);
</style>
<script type="text/javascript">
  if (typeof(Zenbox) !== "undefined") {
    Zenbox.init({
      dropboxID:   "20283106",
      url:         "https://dietwin.zendesk.com",
      tabTooltip:  "Suporte",
      tabImageURL: "https://assets.zendesk.com/external/zenbox/images/tab_pt_support_right.png",
      tabColor:    "black",
      tabPosition: "Right"
    });
  }
</script>
        
