<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php' ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

		<!-- Page Head -->
		<h2>Lista de Clientes cadastrados</h2>
		<p id="page-intro">Abaixo estão listados todos os clientes que estão cadastrados no site.</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<form action="<?php echo SITE_URL ?>/fatorcms/clientes/listar" method="get" id="clientes_buscar_form">
			<fieldset>
				<p>
					<label for="buscar">Digite uma ou mais palavras-chave para buscar por um cliente cadastrado:</label>
					<input class="text-input medium-input" type="text" id="buscar" name="buscar" maxlength="100" value="<?php echo isset($buscar) ? $buscar : '' ?>" />
					<input class="button" type="submit" value="Buscar" />
				</p>
			</fieldset>
			<div class="clear"></div><!-- End .clear -->
		</form>

		<?php if (isset($buscar) AND ! is_null($buscar)) { ?>
			<p class="buscar-resultado">
				Foram encontrados <strong><?php echo $paginacao->linhas_total ?></strong> resultado(s) para a busca por "<strong><?php echo $buscar ?></strong>".
				<a title="Limpar a busca" href="<?php echo SITE_URL?>/fatorcms/clientes/listar"><img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier-cross.png" alt="Limpar busca" /> Clique aqui para limpar a busca</a>
			</p>
		<?php } ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Clientes</h3>

				<input class="cliente button botao-cadastrar" type="button" value="Cadastrar um novo cliente" />

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<table>

					<thead>
						<tr>
							<?php echo Funcoes::montar_th_ordenacao_listagem('clientes', 'listar', $paginacao, $buscar, 'Nome', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('clientes', 'listar', $paginacao, $buscar, 'Compras', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('clientes', 'listar', $paginacao, $buscar, 'Email', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('clientes', 'listar', $paginacao, $buscar, 'CPF/CNPJ', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('clientes', 'listar', $paginacao, $buscar, 'Ativo?', $ordenar_por, $ordem) ?>
                            <th>Ações</th>
						</tr>
					</thead>

					<tfoot>
						<tr>
							<td colspan="7">

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_quantidades() ?>

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_links() ?>

								<div class="clear"></div>
							</td>
						</tr>
					</tfoot>

					<tbody>
						<?php
                        if($clientes AND count($clientes) > 0)
						{
							foreach ($clientes as $cliente)
							{
							?>
								<tr>
									<td class="<?php echo $ordenar_por=='nome' ? 'current' : '' ?>">
										<a href="<?php echo SITE_URL ?>/fatorcms/clientes/editar/<?php echo $cliente->id ?>" title="Editar o cliente &quot;<?php echo $cliente->nome ?>&quot;">
											<?php echo is_null($cliente->nome_empresa) ? $cliente->nome : $cliente->nome_empresa; ?>
										</a>
                                        <?php echo !is_null($cliente->plano_assistencia) ? '<a style="cursor: default;" onclick="return false;" href="#" title="Plano de assistência com vencimento em '.date("d/m/Y",strtotime($cliente->plano_assistencia)).'"><img style="float: right;" src="'.SITE_URL.'/fatorcms/views/imagens/icones/boia-plano.png" /></a>':'' ?>
									</td>
									<td class="<?php echo $ordenar_por=='compras' ? 'current' : '' ?>"><?php echo $cliente->compras_total ?></td>
									<td class="<?php echo $ordenar_por=='email' ? 'current' : '' ?>"><?php echo $cliente->email ?></td>
									<td class="<?php echo $ordenar_por=='cpf-cnpj' ? 'current' : '' ?>"><?php echo (empty($cliente->cnpj) ? Funcoes::formatar_cpf($cliente->cpf) : Funcoes::formatar_cnpj($cliente->cnpj)) ?></td>
									<td class="<?php echo $ordenar_por=='ativo' ? 'current' : '' ?>">
										<span class="<?php echo $cliente->ativo ? 'sim': 'nao' ?>"><?php echo $cliente->ativo ? 'Sim': 'Não' ?></span>
									</td>
									<td>
										<a href="<?php echo SITE_URL ?>/fatorcms/clientes/editar/<?php echo $cliente->id ?>" title="Editar o cliente &quot;<?php echo $cliente->nome ?>&quot;">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" />
										</a>
                                        <?php if ($cliente->ativo) { ?>
                                            <a href="<?php echo SITE_URL ?>/fatorcms/clientes/alterar-ativo/<?php echo $cliente->id ?>" title="Desabilitar o cliente &quot;<?php echo $cliente->nome ?>&quot;" class="item-confirmar">
                                                <img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/cross.png" alt="Desabilitar" />
                                            </a>
                                        <?php } else { ?>
                                            <a href="<?php echo SITE_URL ?>/fatorcms/clientes/alterar-ativo/<?php echo $cliente->id ?>" title="Habilitar o cliente &quot;<?php echo $cliente->nome ?>&quot;" class="item-confirmar">
                                                <img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/tick.png" alt="Habilitar" />
                                            </a>
                                        <?php } ?>
									</td>
								</tr>
							<?php
							}
						}
						else
						{
							echo '<tr><td colspan="7">'.((isset($buscar) AND ! is_null($buscar)) ? 'Nenhum cliente encontrado para esta busca.' : 'Nenhum cliente cadastrado até o momento.' ).'</td></tr>';
						}
						?>
					</tbody>

				</table>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>