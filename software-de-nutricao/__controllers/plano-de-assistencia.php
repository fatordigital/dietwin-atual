<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_PlanoDeAssistencia extends Controller_Padrao
{
    /**
     * Chama o construtor da classe pai
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método inicial que faz a renderização básica da página
     * @param $parametros
     * @return void
     */
    public function index($parametros)
    {
        if (!isset($_SESSION['cliente_id']))
        {
            header('HTTP/1.1 404 Not Found');
            $erro_404 = new Controller_Erro;
            $erro_404->index(404);
            exit;
        }

        $view = new View('plano-de-assistencia.php');
        $this->view_variaveis_obrigatorias($view);

        $view->adicionar('notificacao', new Notificacao);
        $view->adicionar('body_class', 'area-cliente');
        $view->adicionar('pagina_title', 'Conheça o Plano de Assistência - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        $view->exibir();
    }

    public function comprar($parametros)
    {
        if (!isset($_SESSION['cliente_id']))
        {
            header('HTTP/1.1 404 Not Found');
            $erro_404 = new Controller_Erro;
            $erro_404->index(404);
            exit;
        }

        $produto = new Model_Produto(4);

        $view = new View('comprar-plano-de-assistencia.php');
        $this->view_variaveis_obrigatorias($view);

        $view->adicionar('classe','plano-de-assistencia');
        $view->adicionar('licenca',$produto->valor);
        $view->adicionar('produto',$produto);

        $view->adicionar('body_class', 'comprar');
        $view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Comprar - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        $view->exibir();
    }


    public function dados_compra($parametros)
    {
        if (!isset($_SESSION['cliente_id']))
        {
            header('HTTP/1.1 404 Not Found');
            $erro_404 = new Controller_Erro;
            $erro_404->index(404);
            exit;
        }

        $licencas = (isset($parametros->licencas) AND strlen($parametros->licencas)>0)?$parametros->licencas : NULL;
        if ((!isset($licencas) OR !is_numeric($licencas) OR !(int)$licencas < 0))
        {
            if (!isset($_SESSION['licencas']))
            {
                header('HTTP/1.1 404 Not Found');
                $erro_404 = new Controller_Erro;
                $erro_404->index(404);
                exit;
            }
            else
            {
                $licencas = $_SESSION['licencas'];
            }
        }

        $produto = new Model_Produto(4);

        $cupom = NULL;
        if (isset($_SESSION['cupom_id']))
        {
            $cupom = new Model_Cupom();
            $cupom = $cupom->select("SELECT * FROM {tabela_nome}
                        WHERE id='".$_SESSION['cupom_id']."' AND produto_id=".$produto->id."
                        AND ativo=1 AND validade_inicio <= CURRENT_DATE AND validade_fim >= CURRENT_DATE");
            if (!$cupom)
            {
                $cupom = NULL;
                unset($_SESSION['cupom_id']);
            }
        }

        $cliente = new Model_Cliente($_SESSION['cliente_id']);

        $view = new View('dados-compra-plano-de-assistencia.php');
        $this->view_variaveis_obrigatorias($view);

        $view->adicionar('cliente',$cliente);

        $view->adicionar('classe','plano-de-assistencia');
        $_SESSION['cupom'] = $cupom;
        $_SESSION['produto'] = $produto;
        $_SESSION['licencas'] = isset($licencas)?$licencas:$_SESSION['licencas'];

        $view->adicionar('body_class', 'dados-compra');
        $view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Dados de sua compra - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        $view->exibir();
    }

    public function pagamento($parametros)
    {
        //Se tentar acessar essa pagina sem antes ter acessado a dados-compra
        if (!isset($_SESSION['licencas']) OR !isset($_SESSION['produto']) OR !isset($_SESSION['processo_compra']) OR !isset($_SESSION['cliente_id']))
        {
            header('HTTP/1.1 404 Not Found');
            $erro_404 = new Controller_Erro;
            $erro_404->index(404);
            exit;
        }

        $cliente = new Model_Cliente($_SESSION['cliente_id']);
        if ($cliente->cadastro_completo == 0)
        {
            $notificacao = new Notificacao('Você precisa completar os seus dados para poder finalizar a compra','erro',TRUE);
            header('Location: '.SITE_URL.'/area-do-cliente/editar-dados'); exit;
        }

        if (isset($_SESSION['cupom_id']))
        {
            $cupom = new Model_Cupom($_SESSION['cupom_id']);

            if (!is_null($cupom->cliente_id) AND $cliente->id != $cupom->cliente_id)
            {
                unset($_SESSION['cupom_id']);
            }
        }

        //Para definir as cores da pagina de acordo com o produto da url
        $produto_body = new Model_Produto(4);


        $view = new View('pagamento-plano-de-assistencia.php');
        $this->view_variaveis_obrigatorias($view);

        $view->adicionar('classe','plano-de-assistencia');

        $view->adicionar('cupom',(isset($_SESSION['cupom']))?$_SESSION['cupom']:NULL);
        $view->adicionar('cupom',(isset($_SESSION['cupom']))?$_SESSION['cupom']:NULL);
        $view->adicionar('produto',$_SESSION['produto']);
        $view->adicionar('licencas',$_SESSION['licencas']);

        $view->adicionar('body_class', 'pagamento');
        $view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Escolha a forma de pagamento - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

        $view->exibir();
    }

    /* ***************************** MÉTODOS EXTRAS ***************************** */



} // end class