<!DOCTYPE html>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <?php require_once 'includes/head.php' ?>
    <body>
		<?php require_once 'includes/nav.php' ?>
        
        <section class="conteudo cinza">
            <div class="container">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <h1 class="interna">Comprar o <span class="verde">diet</span><span class="azul">win</span> Plus</h1>
                    <p>A praticidade de comprar online, com toda a comodidade e segurança que você precisa. Aproveite nossas promoções e adquira hoje mesmo a sua versão do dietWin.</p>
                </div>
                <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
                    <img src="<?php echo $baseurl ?>/imagens/caixas-dietwin-compra.jpg" alt="dietWin Plus e dietWin Tradicional" class="img-responsive">
                </div>
            </div>
        </section>
        <section class="conteudo cinza">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="sobre-dietwin-plus">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                                <img src="<?php echo $baseurl ?>/imagens/demo-dietwin-plus.jpg" alt="dietwin Plus" class="img-responsive">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                                <h2><span class="verde">diet</span><span class="azul">win</span> Plus</h2>
                                <p>O objetivo principal do dietWin Profissional Plus é auxiliar o nutricionista na tomada de decisão, monitorando informações através do relacionamento de resultados obtidos para análise das avaliações executadas e gerar subsídios para facilitar o diagnóstico nutricional.</p>
                                <p class="aviso"><b>Aviso importante</b>: Esta versão encontra-se em desenvolvimento. Atualizações frequentes serão disponibilizadas até a sua conclusão, por esta razão sua avaliação e sugestões são muito bem vindas para melhoria do sistema.</p>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 text-center">
                                <div class="contatos-duvidas">
                                    <a href="#" title="Clique aqui e fale com nossos atendentes">
                                        <img src="<?php echo $baseurl ?>/imagens/atendentes-off.png" alt="No momento nossos atendentes estão offline">
                                    </a>
                                    <a href="#" title="Solicite maiores informações sobre o dietWin">
                                        <img src="<?php echo $baseurl ?>/imagens/solicitar-informacoes.png" alt="Solicite maiores informações sobre o dietWin">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="observacao-importante">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h2>Importante:</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <ul>
                                    <li>Ao comprar o dietwin você estará adquirindo um software que permite a instalação em <strong>um único computador</strong>.</li>
                                    <li>Se deseja instalar em mais de 1 computador (em sua casa e no consultório, por exemplo), <a href="//www.dietwin.com.br/orcamento" title="Clique aqui">clique aqui</a> e solicite uma proposta personalizada.</li>
                                    <li>Para cada computador será gerado um número de série diferente.<br />Não pode ser dividido com outra pessoa, somente para o mesmo CPF.</li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center">
                                <div class="preco">
                                    <span class="titulo">A partir de</span>
                                    <span class="moeda">R$</span><span class="reais">420,</span><span class="centavos">00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="prosseguir">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs text-center">
                                <img src="<?php echo $baseurl ?>/imagens/compra-segura-pagseguro.png" alt="Compra 100% segura - PagSeguro">
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs text-center">
                                <img src="<?php echo $baseurl ?>/imagens/formas-de-pagamento.png" alt="Compra 100% segura - PagSeguro" class="cartoes-compra">
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 hidden-xs pull-right">
                                <input type="submit" value="Prosseguir comprando" class="processo-compra-submit">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php require_once 'includes/footer.php' ?>
    </body>
</html>
