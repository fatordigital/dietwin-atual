<div class="nav-altura">
    <section class="nav">
        <a id="comeco-conteudo"></a>
        <div class="container">
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                <a href="<?php echo $baseurl ?>/" title="dietWin - Software de Nutrição">
                    <img src="<?php echo $baseurl ?>/imagens/dietwin-logo-nav.png" alt="dietWin - Software de Nutrição" class="img-responsive">
                </a>
            </div>
            <div class="col-lg-5 col-md-5 hidden-sm hidden-xs text-right">
                <nav>
                    <ul>
                        <li><a href="#caracteristicas" title="Caracteristicas">Caracteristicas</a></li>
                        <li><a href="#comprar" title="Comprar">Comprar</a></li>
                        <li><a href="#teste-gratis" title="Teste Grátis">Teste Grátis</a></li>
                        <li><a href="#o-dietwin" title="O dietWin">O dietWin</a></li>
                        <li><a href="#contato" title="Contato">Contato</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-5 col-md-5 hidden-sm hidden-xs text-right">
                <div class="telefone">
                    <span>(51)</span> 3337.7908
                </div>
                <a class="exibir-telefone outros-botoes" title="Clique aqui para ver nosso telefone">
                    <img src="<?php echo $baseurl ?>/imagens/telefone-ico.png" alt="Telefone">
                </a>
                <a href="#" class="outros-botoes" title="Clique aqui para falar conosco através do nosso chat">
                    <img src="<?php echo $baseurl ?>/imagens/chat-ico.png" alt="Chat">
                </a>
                <a href="#" class="comprar-agora botao-comprar">
                    <img src="<?php echo $baseurl ?>/imagens/carrinho-compras.png" alt="Compre agora"> Comprar agora
                </a>
            </div>
        </div>      
    </section>
</div>