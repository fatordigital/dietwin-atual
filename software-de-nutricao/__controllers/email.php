<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe

- Classe exclusiva para montagem e envio de emails, para reunir tudo em um só lugar e poupar os outros Controllers

*/

class Controller_Email extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();

        require_once 'biblioteca/SwiftMailer/swift_required.php';
	}
	
	
	/**
	 * Método inicial que faz a renderização básica da página
	 */
	public function index()
	{
		header('HTTP/1.1 403 Forbidden');
		exit;
	}


	/**
	 * Monta e envia a mensagem com o link para o amigo
	 * @param $parametros
	 * @return int
	 */
	public function enviar_amigo($parametros)
    {
		// Variáveis utilizadas no envio do email
		$assunto = 'dietWin - Indicação de link do seu amigo '.$parametros->seu_nome;

		// Variáveis para montar o html da mensagem
		$titulo = 'Indicação de conteúdo';
		$conteudo = '
			<p><font face="Arial" size="-1">'.trim($parametros->nome_amigo).', seu amigo <b>'.trim($parametros->seu_nome).'</b> lhe indicou uma página no nosso site que é do seu interesse. Acesse o conteúdo através do link abaixo:</font><br><br></p>
			<p><font face="Arial" size="-1"><b>Título:</b> '.trim($parametros->titulo).'</font></p>
			<p><font face="Arial" size="-1"><b>Link:</b> <a href="'.trim($parametros->url).'">'.trim($parametros->url).'</a></font></p>
		';

	    if ( ! empty($parametros->amigo_mensagem))
	    {
		    $conteudo .= '<p><font face="Arial" size="-1"><br><b>Mensagem:</b><br>'.trim($parametros->amigo_mensagem).'</font></p>';
		}

		// Incluimos o html, que concatena as variáveis acima
		include 'views/includes/email-template.php';

		// Criamos a mensagem com assunto, mensagem, destinatário, reply-to
		$message = $this->criar_mensagem($assunto, $email_html, array('email'=>$parametros->email_amigo,'nome'=>$parametros->nome_amigo), array('email'=>$parametros->seu_email,'nome'=>$parametros->seu_nome));
		
		// Criamos o modo de envio do email
		$transport = $this->criar_transport();

		// Enviamos o emails e retornamos boolean
		return $this->enviar($transport, $message);
    }


    /**
     * Monta e envia a mensagem de mais informações
     * @param $parametros
     * @return int
     */
    public function mais_info($parametros, $produto)
    {
        // Variáveis utilizadas no envio do email
        $assunto = 'dietWin - Mais Informações - ('.date('d/m/y H:i:s').')';

        // Variáveis para montar o html da mensagem
        $titulo = 'Mais Informações - '.$produto->nome;

        $conteudo = '
		    <p><font face="Arial" size="-1"><b>Nome:</b> '.trim($parametros->info_nome).'</font></p>
			<p><font face="Arial" size="-1"><b>E-mail:</b> '.trim($parametros->info_email).'</font></p>
			<p><font face="Arial" size="-1"><b>Telefone:</b> '.trim($parametros->info_telefone).'</font></p>
			<p><font face="Arial" size="-1"><b>Cidade:</b> '.trim($parametros->info_cidade).'</font></p>
            <p><font face="Arial" size="-1"><b>Produto:</b> '.trim($produto->nome).'</font></p>
			<p><font face="Arial" size="-1"><b>Mensagem:</b><br>'.trim($parametros->info_mensagem).'</font></p>
			<p><font face="Arial" size="-2"><br><br><i>Mensagem enviada em '.date('d/m/Y \à\s H:i:s').'.</i></font></p>
		';

        // Incluimos o html, que concatena as variáveis acima
        include 'views/includes/email-template.php';

        // Criamos a mensagem com assunto, mensagem, destinatário, reply-to
        $message = $this->criar_mensagem($assunto, $email_html, NULL, array('email'=>$parametros->info_email,'nome'=>$parametros->info_nome));
		
		// Criamos o modo de envio do email
        $transport = $this->criar_transport();

        // Enviamos o emails e retornamos boolean
        return $this->enviar($transport, $message);
    }


    /**
     * Monta e envia a mensagem de mais informações sobre tópico
     * @param $parametros
     * @return int
     */
    public function faq($parametros)
    {
        // Variáveis utilizadas no envio do email
        $assunto = 'dietWin - Mais Informações (FAQ) - ('.date('d/m/y H:i:s').')';

        // Variáveis para montar o html da mensagem
        $titulo = 'Mais Informações (FAQ)';

        $conteudo = '
		    <p><font face="Arial" size="-1"><b>Categoria:</b> '.trim($parametros->categoria).'</font></p>
			<p><font face="Arial" size="-1"><b>Pergunta:</b> '.trim($parametros->pergunta).'</font></p><br />
		    <p><font face="Arial" size="-1"><b>Nome:</b> '.trim($parametros->info_nome).'</font></p>
			<p><font face="Arial" size="-1"><b>E-mail:</b> '.trim($parametros->info_email).'</font></p>
			<p><font face="Arial" size="-1"><b>Telefone:</b> '.trim($parametros->info_telefone).'</font></p>
			<p><font face="Arial" size="-1"><b>Cidade:</b> '.trim($parametros->info_cidade).'</font></p>
			<p><font face="Arial" size="-1"><b>Mensagem:</b><br>'.trim($parametros->info_mensagem).'</font></p>
			<p><font face="Arial" size="-2"><br><br><i>Mensagem enviada em '.date('d/m/Y \à\s H:i:s').'.</i></font></p>
		';

        // Incluimos o html, que concatena as variáveis acima
        include 'views/includes/email-template.php';

        // Criamos a mensagem com assunto, mensagem, destinatário, reply-to
        $message = $this->criar_mensagem($assunto, $email_html, NULL, array('email'=>$parametros->info_email,'nome'=>$parametros->info_nome));
		
		// Criamos o modo de envio do email
        $transport = $this->criar_transport();

        // Enviamos o emails e retornamos boolean
        return $this->enviar($transport, $message);
    }


	/**
	 * Monta e envia a mensagem de esqueci senha
	 * @param $email
	 * @param $senha
	 * @return int
	 */
	public function senha($email, $senha)
    {
        // Variáveis utilizadas no envio do email
        $assunto = 'dietWin - Nova Senha';

        // Variáveis para montar o html da mensagem
        $titulo = 'Nova senha';

        $nome = 'dietWin';

        $conteudo = '
			<p><font face="Arial" size="-1"><b>E-mail:</b> '.trim($email).'</font></p>
			<p><font face="Arial" size="-1"><b>Nova senha:</b> '.$senha.'</font></p>
			<p><font face="Arial" size="-2"><br><br><i>Mensagem enviada em '.date('d/m/Y \à\s H:i:s').'.</i></font></p>
		';

        // Incluimos o html, que concatena as variáveis acima
        include 'views/includes/email-template.php';

        // Criamos a mensagem com assunto, mensagem, destinatário, reply-to
        $message = $this->criar_mensagem($assunto, $email_html, array('email'=>$email,'nome'=>$nome), NULL);
		
		// Criamos o modo de envio do email
        $transport = $this->criar_transport();

        // Enviamos o emails e retornamos boolean
        return $this->enviar($transport, $message);
    }

    /**
     * Monta e envia a mensagem quand um usuário é cadastrado com senha aleatória
     * @param $parametros
     * @return int
     */
    public function cadastra_usuario($parametros){
        // Variáveis utilizadas no envio do email
        $assunto = 'dietWin - Cadastro - ('.date('d/m/y H:i:s').')';

        // Variáveis para montar o html da mensagem
        $titulo = 'Cadastro';

        $conteudo = '
		    <p><font face="Arial" size="-1"><b>Nome:</b> '.trim($parametros->nome).'</font></p>
			<p><font face="Arial" size="-1"><b>E-mail:</b> '.trim($parametros->email).'</font></p>
			<p><font face="Arial" size="-1"><b>Senha:</b> '.trim($parametros->senha).'</font></p>
			<p><font face="Arial" size="-1"><b>Telefone:</b> '.trim($parametros->telefone_principal).'</font></p>
			<p><font face="Arial" size="-2"><br><br><i>Mensagem enviada em '.date('d/m/Y \à\s H:i:s').'.</i></font></p>
		';

        // Incluimos o html, que concatena as variáveis acima
        include 'views/includes/email-template.php';

        // Criamos a mensagem com assunto, mensagem, destinatário, reply-to
        //$message = $this->criar_mensagem($assunto, $email_html, NULL, NULL);
        $message = $this->criar_mensagem($assunto, $email_html, array('email'=>$parametros->email,'nome'=>$parametros->nome), NULL);


        // Criamos o modo de envio do email
        $transport = $this->criar_transport();

        // Enviamos o emails e retornamos boolean
        return $this->enviar($transport, $message);
    }


    /**
	 * Monta e envia a mensagem de contato
	 * @param $parametros
	 * @return int
	 */
	public function contato($parametros)
    {
        // Variáveis utilizadas no envio do email
        $assunto = 'dietWin - Contato - ('.date('d/m/y H:i:s').')';

        // Variáveis para montar o html da mensagem
        $titulo = 'Contato';

        $conteudo = '
		    <p><font face="Arial" size="-1"><b>Nome:</b> '.trim($parametros->nome).'</font></p>
			<p><font face="Arial" size="-1"><b>E-mail:</b> '.trim($parametros->email).'</font></p>
			<p><font face="Arial" size="-1"><b>Telefone:</b> '.trim($parametros->telefone).'</font></p>
			<p><font face="Arial" size="-1"><b>Cidade:</b> '.trim($parametros->cidade).'</font></p>
			<p><font face="Arial" size="-1"><b>Usuário:</b> '.trim($parametros->usuario).'</font></p>
			<p><font face="Arial" size="-1"><b>Assunto:</b> '.trim($parametros->assunto).'</font></p>
			<p><font face="Arial" size="-1"><b>Mensagem:</b><br>'.trim($parametros->mensagem).'</font></p>
			<p><font face="Arial" size="-2"><br><br><i>Mensagem enviada em '.date('d/m/Y \à\s H:i:s').'.</i></font></p>
		';

		// Incluimos o html, que concatena as variáveis acima
		include 'views/includes/email-template.php';

		// Criamos a mensagem com assunto, mensagem, destinatário, reply-to
		$message = $this->criar_mensagem($assunto, $email_html, NULL, array('email'=>$parametros->email,'nome'=>$parametros->nome));
		
		// Criamos o modo de envio do email
		$transport = $this->criar_transport();

		// Enviamos o emails e retornamos boolean
		return $this->enviar($transport, $message);
    }

    public function orcamento($parametros){
        $boll = false;
        $parametros->rotulo_de_alimento = "nenhum produto selecionado";
        if (isset($parametros->rotulo_de_alimento)){
            $boll = true;
            $parametros->produto = "Rótulo de Alimentos";
        }
        if(isset($parametros->personal)){
            if($boll){
                $parametros->produto =  $parametros->produto.",Personal";
            }
            else{
                $parametros->produto = "Personal";
                $boll = true;
            }
        }
        if(isset($parametros->profissional)){
            if($boll){
                $parametros->produto = $parametros->produto.",Profissional";
            }
            else{
                $parametros->produto = "Profissional";
                $boll = true;
            }
        }
        if(isset($parametros->analise_nutricional)){
            if($boll){
                $parametros->produto = $parametros->produto.",Análise Nutricional";
            }
            else{
                $parametros->produto = "Análise Nutricional";
                $boll = true;
            }
        }
        // Variáveis utilizadas no envio do email
        $assunto = 'dietWin - Contato - ('.date('d/m/y H:i:s').')';

        // Variáveis para montar o html da mensagem
        $titulo = 'Orçamento';

        $conteudo = '
		    <p><font face="Arial" size="-1"><b>Nome:</b> '.trim($parametros->nome).'</font></p>
		    <p><font face="Arial" size="-1"><b>CNPJ/CPF:</b> '.trim($parametros->cnpj_cpf).'</font></p>
			<p><font face="Arial" size="-1"><b>E-mail:</b> '.trim($parametros->email).'</font></p>
			<p><font face="Arial" size="-1"><b>Telefone:</b> '.trim($parametros->telefone).'</font></p>
			<p><font face="Arial" size="-1"><b>Celular:</b> '.trim($parametros->celular).'</font></p>
			<p><font face="Arial" size="-1"><b>Cidade:</b> '.trim($parametros->cidade).'</font></p>
			<p><font face="Arial" size="-1"><b>Endereço:</b> '.trim($parametros->endereco).'</font></p>
			<p><font face="Arial" size="-1"><b>Complemento:</b> '.trim($parametros->complemento).'</font></p>
			<p><font face="Arial" size="-1"><b>Número:</b> '.trim($parametros->numero).'</font></p>
			<p><font face="Arial" size="-1"><b>CEP:</b> '.trim($parametros->cep).'</font></p>
			<p><font face="Arial" size="-1"><b>Estado:</b> '.trim($parametros->estado).'</font></p>
			<p><font face="Arial" size="-1"><b>Produto(s):</b> '.trim($parametros->produto).'</font></p>
			<p><font face="Arial" size="-1"><b>Forma de Pagamento:</b><br>'.trim($parametros->forma_pagamento).'</font></p>
			<p><font face="Arial" size="-1"><b>Informação Adicional:</b> '.trim($parametros->info_adicional).'</font></p>

			<p><font face="Arial" size="-2"><br><br><i>Mensagem enviada em '.date('d/m/Y \à\s H:i:s').'.</i></font></p>
		';

        // Incluimos o html, que concatena as variáveis acima
        include 'views/includes/email-template.php';

        // Criamos a mensagem com assunto, mensagem, destinatário, reply-to
        $message = $this->criar_mensagem($assunto, $email_html, NULL, array('email'=>$parametros->email,'nome'=>$parametros->nome));

        // Criamos o modo de envio do email
        $transport = $this->criar_transport();

        // Enviamos o emails e retornamos boolean
        return $this->enviar($transport, $message);
    }


	/**
	 * Envia as informações de um compra para a dietWin
	 * @param $cliente
	 * @param $compra
	 * @return int
	 */
    public function pagseguro_dietwin($cliente, $compra)
    {
        //-----

        $compra_produto = new Model_CompraProduto();
        $compra_produto = $compra_produto->select('SELECT * FROM {tabela_nome} WHERE compra_id ='.$compra->id);

        $produto = new Model_Produto($compra_produto->produto_id);

        $compra_endereco = new Model_CompraEndereco();
        $compra_endereco = $compra_endereco->select('SELECT * FROM {tabela_nome} WHERE compra_id='.$compra->id);

        $compra_cupom = new Model_CompraCupom();
        $compra_cupom = $compra_cupom->select('SELECT * FROM {tabela_nome} WHERE compra_id='.$compra->id);

        $total = $compra_produto->valor+($compra_produto->licenca_valor*$compra_produto->licenca_quantidade)+$compra->entrega_valor-($compra_cupom?$compra_cupom->valor:0);

        // Telefones do cliente
        $cliente_telefones_string =  'Principal: ('.substr($cliente->telefone_principal,0,2).') '.substr($cliente->telefone_principal,2,4).'-'.substr($cliente->telefone_principal,6);
        $cliente_telefones_string .= ! is_null($cliente->telefone_extra) ? ' / Extra: ('.substr($cliente->telefone_extra,0,2).') '.substr($cliente->telefone_extra,2,4).'-'.substr($cliente->telefone_extra,6) : ' / Extra: -- ';

        //-----

        // Variáveis utilizadas no envio do email
        $assunto = 'dietWin - Compra realizada por '.$cliente->nome.' ('.$compra->codigo.')';

        // Variáveis para montar o html da mensagem
        $titulo = 'Nova compra';
        $conteudo = '
			<p><font face="Arial" size="-1">O cliente <b>'.$cliente->nome.'</b> efetou uma compra na dietWin. Esta compra <b>utiliza</b> o '.$compra->pagamento_metodo.' como forma de pagamento.</font><br><br></p>
			'.(($compra_produto->produto_id == 4) ? '<p><font face="Arial" size="-1"><b>Entrega:</b> '.$compra->entrega_forma.'</font></p>':'').'
			<p><font face="Arial" size="-1"><b>Forma de pagamento:</b> '.$compra->pagamento_metodo.'</font></p>
			<p><font face="Arial" size="-1"><b>Produto(s):</b> '.$produto->nome.'</font></p>
			<p><font face="Arial" size="-1"><b>Valor Total:</b> R$ '.number_format($total, 2, ',', '.').'</font></p>
			<p><font face="Arial" size="-1"><b>Telefone(s) do cliente:</b> '.$cliente_telefones_string.'</font></p>
			<p><font face="Arial" size="-1">Para mais informações sobre esta compra, faça login no FatorCMS e acesse o endereço <a href="'.SITE_URL.'/fatorcms/compras/visualizar/'.$compra->id.'">'.SITE_URL.'/fatorcms/compras/visualizar/'.$compra->id.'</a>.</font></p>
		';

        // Incluimos o html, que concatena as variáveis acima
        include 'views/includes/email-template.php';

        // Criamos a mensagem com assunto, mensagem, destinatário, reply-to
        $message = $this->criar_mensagem($assunto, $email_html, NULL, array('email'=>$cliente->email,'nome'=>$cliente->nome));
		
		// Criamos o modo de envio do email
        $transport = $this->criar_transport();

        // Enviamos o emails e retornamos boolean
        return $this->enviar($transport, $message);
    }


	/**
	 * No retorno do PagSeguro, com o status "Paga", enviamos o email para o cliente com informações da compra
	 * @param $cliente
	 * @param $compra
	 * @param $pagseguro
	 * @return int
	 */
    public function pagseguro_retorno_cliente($cliente, $compra, $pagseguro)
    {
        //-----

        $compra_produto = new Model_CompraProduto();
        $compra_produto = $compra_produto->select('SELECT * FROM {tabela_nome} WHERE compra_id ='.$compra->id);

        $produto = new Model_Produto($compra_produto->produto_id);

        $compra_endereco = new Model_CompraEndereco();
        $compra_endereco = $compra_endereco->select('SELECT * FROM {tabela_nome} WHERE compra_id='.$compra->id);

        $cidade = new Model_Cidade($compra_endereco->cidade_id);
        $estado = new Model_Estado($compra_endereco->estado_id);

        $compra_cupom = new Model_CompraCupom();
        $compra_cupom = $compra_cupom->select('SELECT * FROM {tabela_nome} WHERE compra_id='.$compra->id);

        $total = $compra_produto->valor+($compra_produto->licenca_valor*$compra_produto->licenca_quantidade)+$compra->entrega_valor-($compra_cupom?$compra_cupom->valor:0);

        //-----

        // Variáveis utilizadas no envio do email
        $assunto = 'dietWin - Compra Efetuada';

        // Variáveis para montar o html da mensagem
        $titulo = 'Nova compra';

        if ($compra_produto->produto_id != 4)
        {
            $conteudo = '
                <p><font face="Arial" size="-1">Você realizou uma compra na dietWin. Abaixo estão algumas informações sobre ela:</font><br><br></p>
                <p><font face="Arial" size="-1"><b>Código da compra:</b> '.$compra->codigo.'</font></p>
                <p><font face="Arial" size="-1"><b>Código da transação do PagSeguro:</b> '.$pagseguro->identificador.'</font></p>
                <p><font face="Arial" size="-1"><b>Forma de entrega:</b> '.$compra->entrega_forma.'</font></p>
                <p><font face="Arial" size="-1"><b>Valor do frete:</b> R$ '.number_format($compra->entrega_valor, 2, ',', '.').'</font></p>
                <p><font face="Arial" size="-1"><b>Forma de pagamento:</b> '.$compra->pagamento_metodo.'</font></p>
                <p><font face="Arial" size="-1"><b>Produto:</b> '.$produto->nome.'</font></p>
                <p><font face="Arial" size="-1"><b>Valor do produto:</b> R$ '.number_format($produto->valor, 2, ',', '.').'</font></p>
                <p><font face="Arial" size="-1"><b>Nº de licenças adicionais:</b> '.$compra_produto->licenca_quantidade.'</font></p>
                '.(($compra_produto->licenca_quantidade>0)?'<p><font face="Arial" size="-1"><b>Valor da licença adicional:</b> R$ '.number_format($produto->licenca_valor, 2, ',', '.').'</font></p>':'').'
                <p><font face="Arial" size="-1"><b>Cupom:</b> '.((isset($compra_cupom) AND $compra_cupom AND $compra_cupom->id > 0)?'R$ '.number_format($compra_cupom->valor,2,',','.'):'Não').'</font></p>
                <p><font face="Arial" size="-1"><b>Endereço de entrega:</b> '.$compra_endereco->endereco.', '.$compra_endereco->numero.(isset($compra_endereco->complemento)?'/'.$compra_endereco->complemento:'').' - '.$compra_endereco->bairro.' - '.$compra_endereco->cep.' - '.$cidade->nome.' - '.$estado->sigla.'</font></p>
                <p><font face="Arial" size="-1"><b>Total da Compra:</b> R$ '.number_format($total, 2, ',', '.').'</font></p>
                <p><font face="Arial" size="-2"><br><br><i>Mensagem enviada em '.date('d/m/Y \à\s H:i:s').'.</i></font></p>
		    ';
        }
        else
        {
            $conteudo = '
                <p><font face="Arial" size="-1">Você realizou uma compra na dietWin. Abaixo estão algumas informações sobre ela:</font><br><br></p>
                <p><font face="Arial" size="-1"><b>Código da compra:</b> '.$compra->codigo.'</font></p>
                <p><font face="Arial" size="-1"><b>Código da transação do PagSeguro:</b> '.$pagseguro->identificador.'</font></p>
                <p><font face="Arial" size="-1"><b>Forma de pagamento:</b> '.$compra->pagamento_metodo.'</font></p>
                <p><font face="Arial" size="-1"><b>Produto:</b> '.$produto->nome.'</font></p>
                <p><font face="Arial" size="-1"><b>Valor do produto:</b> R$ '.number_format($produto->valor, 2, ',', '.').'</font></p>
                <p><font face="Arial" size="-1"><b>Nº de licenças adicionais:</b> '.$compra_produto->licenca_quantidade.'</font></p>
                '.(($compra_produto->licenca_quantidade>0)?'<p><font face="Arial" size="-1"><b>Valor da licença adicional:</b> R$ '.number_format($produto->licenca_valor, 2, ',', '.').'</font></p>':'').'
                <p><font face="Arial" size="-1"><b>Cupom:</b> '.((isset($compra_cupom) AND $compra_cupom AND $compra_cupom->id > 0)?'R$ '.number_format($compra_cupom->valor,2,',','.'):'Não').'</font></p>
                <p><font face="Arial" size="-1"><b>Total da Compra:</b> R$ '.number_format($total, 2, ',', '.').'</font></p>
                <p><font face="Arial" size="-2"><br><br><i>Mensagem enviada em '.date('d/m/Y \à\s H:i:s').'.</i></font></p>
		    ';
        }



        // Incluimos o html, que concatena as variáveis acima
        include 'views/includes/email-template.php';

        // Criamos a mensagem com assunto, mensagem, destinatário, reply-to
        $message = $this->criar_mensagem($assunto, $email_html, array('email'=>$cliente->email,'nome'=>$cliente->nome), NULL);
		
		// Criamos o modo de envio do email
        $transport = $this->criar_transport();

        // Enviamos o emails e retornamos boolean
        return $this->enviar($transport, $message);
    }

    /**
     * Quando for boleto, envia pro cliente o email
     * @param $cliente
     * @param $compra
     * @param $pagseguro
     * @return int
     */
    public function boleto_retorno_cliente($cliente, $compra)
    {
        //-----

        $compra_produto = new Model_CompraProduto();
        $compra_produto = $compra_produto->select('SELECT * FROM {tabela_nome} WHERE compra_id ='.$compra->id);

        $produto = new Model_Produto($compra_produto->produto_id);

        $compra_endereco = new Model_CompraEndereco();
        $compra_endereco = $compra_endereco->select('SELECT * FROM {tabela_nome} WHERE compra_id='.$compra->id);

        $cidade = new Model_Cidade($compra_endereco->cidade_id);
        $estado = new Model_Estado($compra_endereco->estado_id);

        $compra_cupom = new Model_CompraCupom();
        $compra_cupom = $compra_cupom->select('SELECT * FROM {tabela_nome} WHERE compra_id='.$compra->id);

        $total = $compra_produto->valor+($compra_produto->licenca_valor*$compra_produto->licenca_quantidade)+$compra->entrega_valor-($compra_cupom?$compra_cupom->valor:0);

        //-----

        // Variáveis utilizadas no envio do email
        $assunto = 'dietWin - Compra Efetuada';

        // Variáveis para montar o html da mensagem
        $titulo = 'Nova compra';

        if ($compra_produto->produto_id != 4)
        {
            $conteudo = '
                <p><font face="Arial" size="-1">Você realizou uma compra na dietWin. Abaixo estão algumas informações sobre ela:</font><br><br></p>
                <p><font face="Arial" size="-1"><b>Código da compra:</b> '.$compra->codigo.'</font></p>
                <p><font face="Arial" size="-1"><b>Forma de pagamento:</b> '.$compra->pagamento_metodo.'</font></p>
                <p><font face="Arial" size="-1"><b>Nº de parcelas:</b> '.$compra->boleto_parcelas.'</font></p>
                <p><font face="Arial" size="-1"><b>Data de vencimento dos boletos:</b> Dia '.str_pad($compra->boleto_vencimento,2,'0',STR_PAD_LEFT).'</font></p>
                <p><font face="Arial" size="-1"><b>Forma de entrega:</b> '.$compra->entrega_forma.'</font></p>
                <p><font face="Arial" size="-1"><b>Valor do frete:</b> R$ '.number_format($compra->entrega_valor, 2, ',', '.').'</font></p>
                <p><font face="Arial" size="-1"><b>Produto:</b> '.$produto->nome.'</font></p>
                <p><font face="Arial" size="-1"><b>Valor do produto:</b> R$ '.number_format($produto->valor, 2, ',', '.').'</font></p>
                <p><font face="Arial" size="-1"><b>Nº de licenças adicionais:</b> '.$compra_produto->licenca_quantidade.'</font></p>
                '.(($compra_produto->licenca_quantidade>0)?'<p><font face="Arial" size="-1"><b>Valor da licença adicional:</b> R$ '.number_format($produto->licenca_valor, 2, ',', '.').'</font></p>':'').'
                <p><font face="Arial" size="-1"><b>Cupom:</b> '.((isset($compra_cupom) AND $compra_cupom AND $compra_cupom->id > 0)?'R$ '.number_format($compra_cupom->valor,2,',','.'):'Não').'</font></p>
                <p><font face="Arial" size="-1"><b>Endereço de entrega:</b> '.$compra_endereco->endereco.', '.$compra_endereco->numero.(isset($compra_endereco->complemento)?'/'.$compra_endereco->complemento:'').' - '.$compra_endereco->bairro.' - '.$compra_endereco->cep.' - '.$cidade->nome.' - '.$estado->sigla.'</font></p>
                <p><font face="Arial" size="-1"><b>Total da Compra:</b> R$ '.number_format($total, 2, ',', '.').'</font></p>
                <p><font face="Arial" size="-1"><b>Importante:</b> Se sua compra foi realizada em horário comercial, dentro de algumas horas você receberá os boletos para pagamento, de acordo com as opções e datas de vencimento escolhidas.</font></p>
                <p><font face="Arial" size="-2"><br><br><i>Mensagem enviada em '.date('d/m/Y \à\s H:i:s').'.</i></font></p>
            ';
        }
        else
        {
            $conteudo = '
                <p><font face="Arial" size="-1">Você realizou uma compra na dietWin. Abaixo estão algumas informações sobre ela:</font><br><br></p>
                <p><font face="Arial" size="-1"><b>Código da compra:</b> '.$compra->codigo.'</font></p>
                <p><font face="Arial" size="-1"><b>Forma de pagamento:</b> '.$compra->pagamento_metodo.'</font></p>
                <p><font face="Arial" size="-1"><b>Nº de parcelas:</b> '.$compra->boleto_parcelas.'</font></p>
                <p><font face="Arial" size="-1"><b>Data de vencimento dos boletos:</b> Dia '.str_pad($compra->boleto_vencimento,2,'0',STR_PAD_LEFT).'</font></p>
                <p><font face="Arial" size="-1"><b>Produto:</b> '.$produto->nome.'</font></p>
                <p><font face="Arial" size="-1"><b>Valor do produto:</b> R$ '.number_format($produto->valor, 2, ',', '.').'</font></p>
                <p><font face="Arial" size="-1"><b>Nº de licenças adicionais:</b> '.$compra_produto->licenca_quantidade.'</font></p>
                '.(($compra_produto->licenca_quantidade>0)?'<p><font face="Arial" size="-1"><b>Valor da licença adicional:</b> R$ '.number_format($produto->licenca_valor, 2, ',', '.').'</font></p>':'').'
                <p><font face="Arial" size="-1"><b>Cupom:</b> '.((isset($compra_cupom) AND $compra_cupom AND $compra_cupom->id > 0)?'R$ '.number_format($compra_cupom->valor,2,',','.'):'Não').'</font></p>
                <p><font face="Arial" size="-1"><b>Total da Compra:</b> R$ '.number_format($total, 2, ',', '.').'</font></p>
                <p><font face="Arial" size="-2"><br><br><i>Mensagem enviada em '.date('d/m/Y \à\s H:i:s').'.</i></font></p>
            ';
        }

        // Incluimos o html, que concatena as variáveis acima
        include 'views/includes/email-template.php';

        // Criamos a mensagem com assunto, mensagem, destinatário, reply-to
        $message = $this->criar_mensagem($assunto, $email_html, array('email'=>$cliente->email,'nome'=>$cliente->nome), NULL);

		// Criamos o modo de envio do email
        $transport = $this->criar_transport();
		
        // Enviamos o emails e retornamos boolean
        return $this->enviar($transport, $message);
    }


	/**
	 * Monta e envia para o cliente uma mensagem com informações sobre a interação
	 * @param $interacao
	 * @param $cliente
	 * @param $eh_atualizacao
	 * @return int
	 */
	public function interacao_cliente($interacao, $cliente, $eh_atualizacao = FALSE)
	{
		// Variáveis utilizadas no envio do email
		if ( ! $eh_atualizacao)
		{
			$assunto = 'dietWin - Nova interação (Cód: '.$interacao->id.')';
			$titulo = 'Nova interação';
		}
		else
		{
			$assunto = 'dietWin - Atualização de interação (Cód: '.$interacao->id.')';
			$titulo = 'Atualização de interação';
		}

		$conteudo = '
		    <p><font face="Arial" size="-1">Foi '.( ! $eh_atualizacao ? 'realizado o cadastro de uma nova interação' : 'feita uma atualização em uma interação').' no seu cadastro. Abaixo as informações dela:</font><br><br></p>
		    <p><font face="Arial" size="-1"><b>Código:</b> '.$interacao->id.'</font></p>
		    <p><font face="Arial" size="-1"><b>Tipo:</b> '.$interacao->tipo.'</font></p>
		    <p><font face="Arial" size="-1"><b>Título:</b> '.$interacao->titulo.'</font></p>
		    <p><font face="Arial" size="-1"><b>Data:</b> '.date('d/m/Y \à\s H:i:s', strtotime($interacao->data)).'</font></p>
		    <p><font face="Arial" size="-1"><b>Conteúdo:</b><br> '.$interacao->conteudo.'</font></p>
			<p><font face="Arial" size="-2"><br><br><i>Mensagem enviada em '.date('d/m/Y \à\s H:i:s').'.</i></font></p>
		';

		// Incluimos o html, que concatena as variáveis acima
		include 'views/includes/email-template.php';

		// Criamos a mensagem com assunto, mensagem, destinatário, reply-to
		$message = $this->criar_mensagem($assunto, $email_html, array('email'=>$cliente->email,'nome'=>(is_null($cliente->responsavel_nome) ? $cliente->nome : $cliente->responsavel_nome )));
		
		// Criamos o modo de envio do email
		$transport = $this->criar_transport();

		// Enviamos o emails e retornamos boolean
		return $this->enviar($transport, $message);
	}


	/******************************* MÉTODOS PRIVADOS DA CLASSE *******************************/
	

	/**
	 * Instancia o objeto que fará o transporte da mensagem. Poder ser mail() ou SMTP
	 * @return object
	 */
	private function criar_transport()
	{
		global $email_config;

		if ($email_config[SITE_LOCAL]['smtp_enviar'])
		{
			// Se for SMTP, pega todas as configurações
			if ( ! is_null($email_config[SITE_LOCAL]['smtp_usuario']))
			{
				return Swift_SmtpTransport::newInstance($email_config[SITE_LOCAL]['smtp_servidor'], $email_config[SITE_LOCAL]['smtp_porta'], $email_config[SITE_LOCAL]['smtp_cripto'])
					->setUsername($email_config[SITE_LOCAL]['smtp_usuario'])
					->setPassword($email_config[SITE_LOCAL]['smtp_senha']);
			}
			else
			{
				return Swift_SmtpTransport::newInstance($email_config[SITE_LOCAL]['smtp_servidor'], $email_config[SITE_LOCAL]['smtp_porta']);
			}
		}
		else
		{
			// Caso seja mail()
			// Se o site for hospedado na Locaweb, temos que passar um parâmetro obrigatório
			if (SITE_LOCAL == 'cliente')
			{
				// O %s vai ser automaticamente substituído internamente pelo SwiftMailer
                return Swift_MailTransport::newInstance('-r%s');
			}
			else
			{
                return Swift_MailTransport::newInstance();
			}
		}

	}


	/**
	 * Instancia o objeto com a mensagem a ser enviada, fazendo algum tratamento quando os parâmetros vierem NULL
	 * @param $assunto
	 * @param $mensagem
	 * @param null $destinatario
	 * @param null $reply_to
	 * @return object
	 */
	private function criar_mensagem($assunto, $mensagem, $destinatario = NULL, $reply_to = NULL)
	{
		global $email_config;

		// Se o remetente vier NULL, usamos os dados do config.php
		if (is_null($reply_to))
		{
			$reply_to = array('email' => $email_config[SITE_LOCAL]['reply_to'], 'nome' => $email_config[SITE_LOCAL]['reply_to_nome']);
		}

		// Se o destinatario vier NULL, usamos os dados do config.php
		if (is_null($destinatario))
		{
			$destinatario = array($email_config[SITE_LOCAL]['para'] => $email_config[SITE_LOCAL]['para_nome']);
		}
		elseif (isset($destinatario['email'])) // Entra aqui caso tenha sido passado um array com um só destinatário, mas no formato antigo
		{
			$destinatario = array($destinatario['email'] => $destinatario['nome']);
		}

		//-----

		$log = new Log(var_export($destinatario, TRUE));
		$log = new Log($assunto);
		$log = new Log($mensagem);

		return Swift_Message::newInstance()
			->setFrom(array($email_config[SITE_LOCAL]['de'] => $email_config[SITE_LOCAL]['de_nome']))
			->setReturnPath($email_config[SITE_LOCAL]['return_path'])
			//->setTo(array($destinatario['email'] => $destinatario['nome']))
			->setTo($destinatario)
			->setReplyTo(array($reply_to['email'] => $reply_to['nome']))
			->setSubject($assunto)
			->setBody($mensagem, 'text/html');
	}


	/**
	 * Cria o objeto Mailer com o Transport e a Message e faz o envio do email, retornando o sucesso ou não
	 * @param $transport
	 * @param $message
	 * @return int
	 */
    private function enviar($transport, $message)
    {
        // Inicializa o objeto que gerencia tudo
        $mailer = Swift_Mailer::newInstance($transport);

        return $mailer->send($message);
    }


} // end class