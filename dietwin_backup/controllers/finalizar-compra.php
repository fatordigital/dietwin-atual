<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');
/**
$cliente = new Model_Cliente($_SESSION['cliente_id']);
if ($cliente->cadastro_completo == 0)
{
$notificacao = new Notificacao('Você precisa completar os seus dados para poder finalizar a compra','erro',TRUE);
header('Location: '.SITE_URL.'/area-do-cliente/editar-dados'); exit;
}
 */

class Controller_FinalizarCompra extends Controller_Padrao
{
  /**
   * Chama o construtor da classe pai
   */
  public function __construct()
  {
    parent::__construct();
  }

  public function index($parametros)
  {
   //print_r($parametros); die;
    if($parametros->finalizar_compra=='true'){
      $produto_nome_seo = $parametros->produto_seo;

        $produto = new Model_Produto();
        $produto = $produto->select("SELECT * FROM {tabela_nome} WHERE nome_seo='".$parametros->produto_seo."'");

        $_SESSION['licencas'] = 0;
        $_SESSION['produto'] = $produto;
        $_SESSION['frete'] = $parametros->frete;
        $_SESSION['servico_frete'] = $parametros->servico;

      if(isset($_SESSION['cliente_id'])){
        if($parametros->metodo_pagamento=='boleto'){
            $_SESSION['pagamento_parcelas'] = $parametros->quantidade_parcelas;
            $_SESSION['pagamento_vencimento'] = $parametros->data_vencimento;
            $_SESSION['produto_nome'] = $parametros->produto_seo;
            header('Location: '.SITE_URL.'/boleto-compra');
        }
        if($parametros->metodo_pagamento=='pagseguro'){
            $_SESSION['pagamento_parcelas'] = $parametros->quantidade_parcelas;
            $_SESSION['pagamento_vencimento'] = $parametros->data_vencimento;
            $_SESSION['produto_nome'] = $parametros->produto_seo;
            header('Location: '.SITE_URL.'/pagseguro-compra');
        }
      }else{
        // não esta logado
        $_SESSION['login_necessario_compra'] = true;
        header('Location: '.SITE_URL.'/comprar/'.$produto_nome_seo.'#!');
      }
    }else{
      header('Location: '.SITE_URL.'/planos');
    }
  }

}