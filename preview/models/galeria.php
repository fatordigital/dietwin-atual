<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property int produto_id
 * @property string titulo
 * @property string titulo_seo
 * @property string descricao
 * @property string data
 * @property int log_id
 * @property string log_data
 */

class Model_Galeria extends Model_Padrao
{
	protected $tabela_nome = 'galerias';

} // end class