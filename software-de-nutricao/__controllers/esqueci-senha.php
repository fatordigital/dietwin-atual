<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe

*/

class Controller_EsqueciSenha extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        $view = new View('esqueci-senha.php');
		$this->view_variaveis_obrigatorias($view);

		$view->adicionar('body_class', 'esqueci-senha');
		$view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Esqueci minha senha - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}


    /**
     * Passa as informações para a montagem e envio do email a partir do formulário "Esqueci minha senha" do site
     * @param $parametros
     * @return void
     */
    public function enviar($parametros)
    {
        $email = isset($parametros->email) ? trim($parametros->email) : NULL;

        if ( ! empty($email))
        {
            $cliente = new Model_Cliente();
            $cliente = $cliente->select("SELECT id, email FROM {tabela_nome} WHERE email='".Funcoes::mysqli_escape($parametros->email)."'");

            if (isset($cliente) AND $cliente)
            {
	            $email = $cliente->email;
	            $senha = rand(100000,999999);

	            //-----

	            $cliente_update = new Model_Cliente;
                $cliente_update->id = $cliente->id;
                $cliente_update->senha = sha1($senha);

                $cliente_update->update();

                $mail = new Controller_Email;

                if ($mail->senha($email,$senha))
                {
                    echo json_encode(array('tipo'=>'sucesso', 'mensagem'=>'Sua nova senha foi enviada para seu e-mail!'));
                }
                else
                {
                    echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Ocorreu um erro ao enviar sua mensagem.'));
                }
            }
            else
            {
                echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Email não cadastrado na dietWin.'));
            }
        }
        else
        {
            echo json_encode(array('tipo'=>'erro', 'mensagem'=>'Digite abaixo seu e-mail cadastrado na dietWin.'));
        }
    }


	/* ***************************** MÉTODOS EXTRAS ***************************** */


} // end class