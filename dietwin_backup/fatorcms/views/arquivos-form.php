<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php'; ?>

<script type="text/javascript" src="<?php echo SITE_BASE ?>/fatorcms/views/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo SITE_BASE ?>/fatorcms/views/js/uploadify/swfobject.js"></script>
<link rel="stylesheet" href="<?php echo SITE_BASE ?>/fatorcms/views/js/uploadify/uploadify.css" type="text/css" media="screen" />
<script type="text/javascript">
$(document).ready(function()
{
	$('#uploader_fotos').uploadify(
	{
		'uploader'       : '<?php echo SITE_BASE; ?>/fatorcms/views/js/uploadify/uploadify.swf?preventswfcaching=<?php echo time(); ?>',
		'expressInstall' : '<?php echo SITE_BASE; ?>/fatorcms/views/js/uploadify/expressInstall.swf',
		'script'         : '<?php echo SITE_BASE ?>/fatorcms/views/js/uploadify/uploadify-arquivo.php',
		'cancelImg'      : '<?php echo SITE_BASE; ?>/fatorcms/views/js/uploadify/cancel.png',
		'buttonImg'      : '<?php echo SITE_BASE; ?>/fatorcms/views/js/uploadify/selecione-arquivo.png',
		'fileDataName'   : 'galeria_[]',
		//'scriptData'     : { 'galeria_id': '<?php echo ( ! is_null($arquivo) ? $arquivo->id : '') ?>' }
		'multi'          : true,
		'auto'           : false,
		'width'	         : 163,
        'height'         : 27,
		'fileExt'        : '*',
        'queueSizeLimit' : 1,
		'fileDesc'       : 'Arquivos (*)',
		'queueID'        : 'fila_upload_imagens',
		'sizeLimit'      : 1024 * 1024 * 20,
		'removeCompleted': true,
        'onUploadStart'  : function(file) {

        },
		'onAllComplete'  : function(event, data) {
            window.location.href = '<?php echo SITE_URL ?>/fatorcms/arquivos/listar';
		},
		'onError'        : function(event, data, errorObj) {
			console.log(errorObj.status);
		}
	});

});
</script>

	<div id="main-content"> <!-- Main Content Section with everything -->

        <!-- Page Head -->
		<?php if (is_null($arquivo) OR is_null($arquivo->id)) { ?>
			<h2>Cadastro de um Arquivo</h2>
			<p id="page-intro">Utilize o formulário abaixo para incluir um arquivo na "Área de Cliente".</p>
		<?php } else { ?>
			<h2>Edição de uma Arquivo</h2>
			<p id="page-intro">No formulário abaixo você pode alterar os dados de um arquivo da "Área de Cliente".</p>
		<?php } ?>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

        <div class="notificacao_js"></div> <!-- DIV para inserções de notificações via jQuery -->

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<?php if (is_null($arquivo) OR is_null($arquivo->id)) { ?>
					<h3>Dados de um arquivo</h3>
				<?php } else { ?>
					<h3>Informações do arquivo <?php echo $arquivo ? $arquivo->titulo : '' ?></h3>
				<?php } ?>

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<div class="tab-content <?php echo ( ! isset($aba) OR $aba == 1 ) ? 'default-tab' : '' ?>" id="tab1">
			
					<form action="<?php echo SITE_URL ?>/fatorcms/arquivos/<?php echo (is_null($arquivo) OR is_null($arquivo->id)) ? 'cadastrar' : 'atualizar' ?>" method="post" id="arquivo_form" enctype="multipart/form-data">

						<fieldset>

							<?php if ( ! is_null($arquivo)) { ?>
								<input type="hidden" name="id" value="<?php echo $arquivo->id ?>" />
							<?php } ?>

							<p>
								<label for="titulo">Título</label>
								<input class="text-input large-input required" type="text" id="titulo" name="titulo" maxlength="100" value="<?php echo $arquivo ? $arquivo->titulo : '' ?>" />
							</p>

							<p>
								<label for="descricao">Descrição</label>
								<textarea class="text-input required" cols="79" rows="10" id="descricao" name="descricao" ><?php echo $arquivo ? $arquivo->descricao : '' ?></textarea>
							</p>

                            <p>
                                <label>Disponível - <input type="checkbox" id="disponivel" name="habilitado" value="1" <?php if(isset($arquivo)){ echo ($arquivo->habilitado != "0") ? "checked='check'" : ''; } ?>>
                                </label>
                                Com esta opção habilitada este arquivo estará disponível para clientes que efetuarem cadastro futuramente.
                            </p>

                            <p>
                                <label for="cliente_id">Clientes</label>
                                <input class="" type="text" id="cliente_id" name="cliente_id" />
                            </p>
                            <br />
                            <div class="content-box"><!-- Start Content Box -->

                                <div class="content-box-header">

                                    <h3>Tipo de Envio</h3>
                                    <div class="clear"></div>

                                </div> <!-- End .content-box-header -->

                                <div class="content-box-content">
                                    <p>
                                        <span style="font-weight: bold; margin-right: 30px;">Nome do arquivo (Já foi enviado via ftp) <input type="radio" name="opcoes" class="opcoes" value="link" <?php echo (is_null($arquivo) OR ($arquivo AND !is_null($arquivo->link))) ? ' checked="checked" ' : '' ?>/></span>
                                        <span style="font-weight: bold;">Selecione o arquivo para envio: <input type="radio" name="opcoes" class="opcoes" value="arquivo" <?php echo ($arquivo AND !is_null($arquivo->arquivo)) ? ' checked="checked" ' : '' ?>/></span>
                                    </p>

                                    <div class="div-opcoes" <?php echo ($arquivo AND is_null($arquivo->link)) ? ' style="display: none;" ' : '' ?>>
                                        <input class="text-input large-input" type="text" id="link" name="link" maxlength="100" value="<?php echo $arquivo ? $arquivo->link : '' ?>" />
                                    </div>

                                    <div class="div-opcoes" <?php echo (is_null($arquivo) OR is_null($arquivo->arquivo)) ? ' style="display: none;" ' : '' ?>>
                                        <p>
                                            <input id="uploader_fotos" type="file" name="fotos" />
                                            <?php /*<a href="" onclick="$('#uploader_fotos').uploadifyUpload(); return false;"><img src="<?php echo SITE_BASE; ?>/fatorcms/views/js/uploadify/fazer-upload.png" width="190" height="27" style="border:0" /></a> */ ?>
                                            <a href="" onclick="$('#uploader_fotos').uploadifyClearQueue(); return false;"><img src="<?php echo SITE_BASE; ?>/fatorcms/views/js/uploadify/limpar-lista.png" width="184" height="27" style="border:0" /></a>
                                        </p>

                                        <p>
                                        <div id="fila_upload_imagens"></div>
                                        <?php if ($arquivo and !is_null($arquivo->arquivo)) { ?>
                                        <span>Arquivo atual: <a href="<?php echo SITE_BASE.'/fatorcms/arquivos/download/'.$arquivo->arquivo; ?>"><strong><?php echo $arquivo->arquivo . ' ('.number_format((filesize('arquivos/arquivos/'.$arquivo->arquivo)/1024)/1024,2,'.','').' Mb)' ?></strong></a></span>
                                        <?php } ?>
                                        </p>
                                    </div>
                                </div>
                            </div>



                            <p>
                                <input id="form-enviar" class="button" type="submit" value="<?php echo (is_null($arquivo) OR is_null($arquivo->id)) ? 'Cadastrar' : 'Atualizar' ?>" />
                            </p>

						</fieldset>

						<div class="clear"></div><!-- End .clear -->

					</form>
					
				</div>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>