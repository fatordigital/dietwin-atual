<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php' ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

		<!-- Page Head -->
		<h2>Lista de FAQ cadastradas</h2>
		<p id="page-intro">Abaixo estão listadas todas as perguntas e respostas que são exibidas na área FAQ do site.</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<form action="<?php echo SITE_URL ?>/fatorcms/faq/listar" method="get" id="faqs_buscar_form">
			<fieldset>
				<p>
					<label for="buscar">Digite uma ou mais palavras-chave para buscar por uma pergunta cadastrada:</label>
					<input class="text-input medium-input" type="text" id="buscar" name="buscar" maxlength="100" value="<?php echo isset($buscar) ? $buscar : '' ?>" />
					<input class="button" type="submit" value="Buscar" />
				</p>
			</fieldset>
			<div class="clear"></div><!-- End .clear -->
		</form>

		<?php if (isset($buscar) AND ! is_null($buscar)) { ?>
			<p class="buscar-resultado">
				Foram encontrados <strong><?php echo $paginacao->linhas_total ?></strong> resultado(s) para a busca por "<strong><?php echo $buscar ?></strong>".
				<a title="Limpar a busca" href="<?php echo SITE_URL?>/fatorcms/faq/listar"><img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier-cross.png" alt="Limpar busca" /> Clique aqui para limpar a busca</a>
			</p>
		<?php } ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>FAQ</h3>

				<input class="faq button botao-cadastrar" type="button" value="Cadastrar uma nova pergunta" />

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<table>

					<thead>
						<tr>
							<?php echo Funcoes::montar_th_ordenacao_listagem('faq', 'listar', $paginacao, $buscar, 'Pergunta', $ordenar_por, $ordem) ?>
                            <?php echo Funcoes::montar_th_ordenacao_listagem('faq', 'listar', $paginacao, $buscar, 'Resposta', $ordenar_por, $ordem) ?>
                            <?php echo Funcoes::montar_th_ordenacao_listagem('faq', 'listar', $paginacao, $buscar, 'Categoria', $ordenar_por, $ordem) ?>
                            <th>Ações</th>
                        </tr>
					</thead>

					<tfoot>
						<tr>
							<td colspan="4">

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_quantidades() ?>

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_links() ?>

								<div class="clear"></div>
							</td>
						</tr>
					</tfoot>

					<tbody>
						<?php
                        if($faqs AND count($faqs) > 0)
						{
							foreach ($faqs as $faq)
							{
							?>
								<tr>
                                    <td width="20%" class="<?php echo $ordenar_por=='pergunta' ? 'current' : '' ?>">
                                        <a href="<?php echo SITE_URL ?>/fatorcms/faq/editar/<?php echo $faq->id ?>" title="Editar a pergunta &quot;<?php echo $faq->pergunta ?>&quot;">
                                            <?php echo $faq->pergunta ?>
                                        </a>
                                    </td>
                                    <td width="55%" class="<?php echo $ordenar_por=='resposta' ? 'current' : '' ?>"><?php echo Funcoes::cortar_texto($faq->resposta, 100) ?></td>
                                    <td width="20%" class="<?php echo $ordenar_por=='categoria' ? 'current' : '' ?>">
                                        <?php
                                            $categorias = '';
                                            foreach ($produtos[$faq->id] as $produto)
                                            {
                                                $categorias .= (is_null($produto->nome) ? (' <em>Tópicos diversos</em>,') : (' '.$produto->nome).',');
                                            }
                                            echo substr($categorias,0,-1);
                                        ?>
                                    </td>
									<td width="5%">
										<a href="<?php echo SITE_URL ?>/fatorcms/faq/editar/<?php echo $faq->id ?>" title="Editar a pergunta &quot;<?php echo $faq->pergunta ?>&quot;">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" />
										</a>
										<a href="<?php echo SITE_URL ?>/fatorcms/faq/excluir/<?php echo $faq->id ?>" title="Excluir a pergunta &quot;<?php echo $faq->pergunta  ?>&quot;" class="item-confirmar">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/cross.png" alt="Excluir" />
										</a>
									</td>
								</tr>
							<?php
							}
						}
						else
						{
							echo '<tr><td colspan="4">'.((isset($buscar) AND ! is_null($buscar)) ? 'Nenhuma pergunta encontrada para esta busca.' : 'Nenhuma pergunta cadastrada até o momento.' ).'</td></tr>';
						}
						?>
					</tbody>

				</table>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>