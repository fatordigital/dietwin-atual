<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class FatorCMS_Controller_Tags extends FatorCMS_Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai e testa se o usuário está logado
	 */
	public function __construct()
	{
		parent::__construct();

		// Verifica se o usuário está logado
		$this->verificar_usuario_logado();
	}
	
	
	/**
	 * Não faz nada
	 */
	public function index()
	{
		// Não faz nada
	}


	/**
	 * Método utilizado no formulário pelo autoSuggest
	 * @param $parametros
	 * @return void
	 */
	public function ajax_buscar($parametros)
	{
		$texto = Funcoes::mysqli_escape($parametros->q);
		$data = array();

		$tag = new FatorCMS_Model_Tag;
		$tags = $tag->select('SELECT id, nome FROM {tabela_nome} WHERE LOWER(nome) LIKE "%'.mb_strtolower($texto).'%" ORDER BY nome', TRUE);

		if ($tags AND count($tags) > 0)
		{
			foreach($tags as $tag)
			{
				$json = array();
				$json['id'] = $tag->id;
				$json['nome'] = $tag->nome;
				$data[] = $json;
			}
		}
		
		header("Content-type: application/json");
		echo json_encode($data);
	}


	/****************************** MÉTODOS EXTRAS ******************************/
	

} // end class