<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe



*/

/**
 * Delaração para auto-complete em IDEs
 * @property int id
 * @property int cliente_id
 * @property string data
 * @property int codigo
 * @property string entrega_forma
 * @property float entrega_valor
 * @property string pagamento_metodo
 * @property int boleto_parcelas
 * @property int boleto_vencimento
 * @property string situacao
 * @property string onde_foi_adquirido
 * @property string informacao_adicional
 * @property string inicio
 * @property string fim
 */

class Model_Compra extends Model_Padrao
{
	protected $tabela_nome = 'compras';

} // end class