<div class="rodape">
	<div class="container">
    	<div class="dietwin">
        	<h2>O DIETWIN</h2>
            <ul>
            	<li><b><a href="<?php echo SITE_URL ?>/institucional" title="Institucional da DietWin">Institucional</a></b></li>
                <li><b><a href="<?php echo SITE_URL ?>/videos" title="Acesse a área multimídia da dietWin">Multimídia</a></b></li>
                <li><b><a href="<?php echo SITE_URL ?>/faq" title="Suporte da DietWin">FAQ</a></b></li>
                <li><b><a href="<?php echo SITE_URL ?>/novidades" title="Acompanhe as novidades da dietWin">Novidades</a></b></li>
                <li><b><a href="<?php echo SITE_URL ?>/qual-versao-escolho" title="">Qual versão eu escolho?</a></b></li>
                <li><b><a href="<?php echo SITE_URL ?>/suporte" title="Entre em contato com nosso suporte DietWin">Suporte</a></b></li>
                <li><b><a href="<?php echo SITE_URL ?>/contato" title="Entre em contato com a DietWin">Contato</a></b></li>

        	</ul>
        </div>
        <div class="softwares">
        	<div class="box">
            	<h2><span class="professional">Profissional Plus</span> </h2>
                <ul>
                    <li><b><a href="<?php echo SITE_URL ?>/profissional" title="Apresentação do dietWin Profissional ">Apresentação</a></b></li>
                    <li><b><a href="<?php echo SITE_URL ?>/suporte/profissional/videos" title="Multimídia & Treinamento do dietWin Profissional ">Multimídia & Treinamento</a></b></li>
                    <li><b><a href="<?php echo SITE_URL ?>/software/profissional/comprar" title="Comprar o dietWin Profissional ">Comprar</a></b></li>
                    <li><b><a href="<?php echo SITE_URL ?>/suporte/profissional/faq" title="Comprar o dietWin Rótulo de Alimentos">FAQ</a></b></li>
                </ul>
            </div>
            <div class="box">
            	<h2><span class="rotulo-alimentos">Rótulo de Alimentos</span></h2>
                <ul>
                    <li><b><a href="<?php echo SITE_URL ?>/rotulo-de-alimentos" title="Apresentação do dietWin Rótulo de Alimentos">Apresentação</a></b></li>
                    <li><b><a href="<?php echo SITE_URL ?>/suporte/rotulo-de-alimentos/videos" title="Multimídia & Treinamento do dietWin Rótulo de Alimentos">Multimídia & Treinamento</a></b></li>
                    <li><b><a href="<?php echo SITE_URL ?>/software/rotulo-de-alimentos/comprar" title="Comprar o dietWin Rótulo de Alimentos">Comprar</a></b></li>
                    <li><b><a href="<?php echo SITE_URL ?>/suporte/rotulo-de-alimentos/faq" title="Comprar o dietWin Rótulo de Alimentos">FAQ</a></b></li>
                </ul>
            </div>
            <div class="box">
            	<h2><span class="personal">Personal</span> </h2>
                <ul>
                    <li><b><a href="<?php echo SITE_URL ?>/personal" title="Apresentação do dietWin Personal">Apresentação</a></b></li>
                    <li><b><a href="<?php echo SITE_URL ?>/suporte/personal/videos" title="Multimídia & Treinamento do dietWin Personal">Multimídia & Treinamento</a></b></li>
                    <li><b><a href="<?php echo SITE_URL ?>/software/personal/comprar" title="Comprar o dietWin Personal">Comprar</a></b></li>
                    <li><b><a href="<?php echo SITE_URL ?>/suporte/personal/faq" title="Comprar o dietWin Rótulo de Alimentos">FAQ</a></b></li>
                </ul>
            </div>          
        </div>
        <div class="contato">
        	<img src="<?php echo SITE_BASE ?>/views/imagens/rodape-logo.gif" alt="dietWin - Softwares de Nutrição" />
            <p class="alinhado"><b>CENTRAL DE ATENDIMENTO</b></p>
            <p class="telefone">(51) 3337-7908</p>
            <div style="clear: both;"></div>
                <p class="end">Rua Xavier Ferreira, 101 - Bairro Auxiliadora<br/>
                Porto Alegre/RS - CEP: 90540-160<br/>
                FAX: (51) 3342-4495</p>
        </div>
        <div class="logo-fator">
        	<a href="http://www.fatordigital.com.br" title="Produzido por Fator Digital"><img src="<?php echo SITE_BASE ?>/views/imagens/logo-fator.gif" alt="Produzido por Fator Digital" /></a>
        </div>
        <div class="clear"></div>
    </div>
</div>

<!--<div class="form-orcamento">
    <div class="bg-orcamento"></div>
    <form action="" id="orcamento">
        <span class="preencha">Preencha os campos abaixo para ter um orçamento personalizado por parte de nossa equipe.</span>
        <label for="nome/empresa">Nome/Empresa:</label> <input type="text" id="nome/empresa"><br>
        <label for="cnpj/cpf">CNPJ/CPF:</label> <input type="text" id="cnpj/cpf"><br>
        <label for="email-contato">E-mail:</label> <input type="text" id="email-contato"><br>
        <label for="telefone">Telfone:</label> <input type="text" id="telefone"><br>
        <label for="celular">Celular:</label> <input type="text" id="celular"><br>
        <label for="endereco">Endereço:</label> <input type="text" id="endereco"><br>
        <label for="numero">Número:</label> <input type="text" id="numero" class="divide"><br>
        <label for="comp">Complemento:</label> <input type="text" id="complemento" class="divide semmargin"><br>
        <label for="bairro">Bairro:</label> <input type="text" id="bairro"><br>
        <label for="cidade">Cidade:</label> <input type="text" id="cidade"><br>
        <label for="estado">Estado:</label> <select name="" id="estado">
            <option value="RS">Rio Grande do Sul</option>
            <option value="SC">Santa Catarina</option>
            <option value="SP">São Paulo</option>
        </select><br>
        <label for="cep">CEP:</label> <input type="text" id="cep"><br>
        <label for="">Qual software você deseja?</label> <div class="checkboxes"><br>
            <input type="checkbox" value="rotulo-alimentos">Rotulo de Alimentos<br>
            <input type="checkbox" value="personal">Personal<br>
            <input type="checkbox" value="profissional">Profissional<br>
            <input type="checkbox" value="analise-nutricional">Análise Nutricional
        </div>
        <label for="forma-pagamento">Alguma forma de pagamento especial?</label> <textarea name="" id="forma-pagamento" cols="30" rows="10"></textarea><br>
        <label for="info-adicionais">Informações Adicionais:</label> <textarea name="" id="info-adicionais" cols="30" rows="10"></textarea>
        <input type="submit" value="Enviar">
    </form>
</div>-->

</body>
</html>
