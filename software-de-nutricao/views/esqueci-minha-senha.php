<?php require_once 'includes/head.php' ?>
<body>
<?php require_once 'includes/nav.php' ?>

<section class="conteudo cinza">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h1 class="interna">Esqueci Minha Senha</h1>
    </div>
    <section class="formulario">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="titulo-formulario">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>Informe o seu e-mail no campo abaixo</h2>
          </div>
        </div>
        <form action="<?php echo SITE_URL ?>/esqueci-minha-senha" method="post" class="form-inline" role="form">
        <div class="formulario-cadastro">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<?php if (isset($_SESSION['erro_esqueci_senha'])): ?>
            	<?php echo $_SESSION['erro_esqueci_senha'] ?>
            	<?php endif; unset($_SESSION['erro_esqueci_senha']); ?>
            	<div class="form-group">
            		<label for="email">E-mail:</label>
            		<input type="email" id="email" name="email">
            	</div>
            	<input type="submit" name="acao" value="Enviar" class="btn btn-success">
            </div>
          </div>
        </div>
        </form>
      </div>
    </section>
  </div>
</section>

<?php require_once 'includes/footer.php' ?>
</body>
</html>
