<!DOCTYPE html>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <?php require_once 'includes/head.php' ?>
    <body>
		<?php require_once 'includes/nav.php' ?>
        
        <section class="conteudo cinza">
            <div class="container">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <h1 class="interna">Qual a melhor solução <span class="verde">para você?</span></h1>
                    <p><b>Cada software da família dietWin é cuidadosamente construído tendo como foco um perfil de usuário. Descubra qual é a melhor solução para você!</b></p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quam dolor, sagittis vitae elit vitae, interdum tristique sem. Cras quis elit bibendum, commodo diam eget, consequat nisl. Sed rhoncus nulla non rutrum venenatis. </p>
                </div>
                <div class="col-lg-6 col-md-6 hidden-sm hidden-xs">
                    <img src="<?php echo $baseurl ?>/imagens/caixas-dietwin-compra.jpg" alt="dietWin Plus e dietWin Tradicional" class="img-responsive">
                </div>
            </div>
        </section>
        <section class="conteudo cinza">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="features" style="width: 60%">Features</th>
                                <th class="diet-win text-center" style="width: 20%">diet<b>Win</b></th>
                                <th class="diet-win-plus text-center" style="width: 20%">diet<b>Win</b> <span>Plus</span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="cinza-claro">
                                <td class="explicacao-feature">
                                    <img src="<?php echo $baseurl ?>/imagens/prescricao-dieta-ico.png" alt="Prescricao-dieta">
                                    <div class="conteudo-feature">
                                        <span class="titulo-feature">Prescrição da Dieta</span>
                                        <div class="detalhe-feature">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quam dolor, sagittis vitae elit vitae, interdum tristique sem. Cras quis elit bibendum, commodo diam eget.
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/positivo-ico.png" alt="Positivo">
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/positivo-ico.png" alt="Positivo">
                                </td>
                            </tr>
                            <tr>
                                <td class="explicacao-feature">
                                    <img src="<?php echo $baseurl ?>/imagens/avaliacao-clinica-ico.png" alt="Avaliação Clínica">
                                    <div class="conteudo-feature">
                                        <span class="titulo-feature">Avaliação Clínica</span>
                                        <div class="detalhe-feature">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quam dolor, sagittis vitae elit vitae, interdum tristique sem. Cras quis elit bibendum, commodo diam eget.
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/positivo-ico.png" alt="Positivo">
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/positivo-ico.png" alt="Positivo">
                                </td>
                            </tr>
                            <tr class="cinza-claro">
                                <td class="explicacao-feature">
                                    <img src="<?php echo $baseurl ?>/imagens/programa-exercicios-ico.png" alt="Prescricao-dieta">
                                    <div class="conteudo-feature">
                                        <span class="titulo-feature">Programa de Exercícios</span>
                                        <div class="detalhe-feature">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quam dolor, sagittis vitae elit vitae, interdum tristique sem. Cras quis elit bibendum, commodo diam eget.
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/negativo-ico.png" alt="Negativo">
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/positivo-ico.png" alt="Positivo">
                                </td>
                            </tr>
                            <tr>
                                <td class="explicacao-feature">
                                    <img src="<?php echo $baseurl ?>/imagens/prescricao-dietetica-ico.png" alt="Prescrição Dietética">
                                    <div class="conteudo-feature">
                                        <span class="titulo-feature">Prescrição Dietética</span>
                                        <div class="detalhe-feature">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quam dolor, sagittis vitae elit vitae, interdum tristique sem. Cras quis elit bibendum, commodo diam eget.
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/negativo-ico.png" alt="Negativo">
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/positivo-ico.png" alt="Positivo">
                                </td>
                            </tr>
                            <tr class="cinza-claro">
                                <td class="explicacao-feature">
                                    <img src="<?php echo $baseurl ?>/imagens/analise-dietetica-ico.png" alt="Análise Dietética">
                                    <div class="conteudo-feature">
                                        <span class="titulo-feature">Análise Dietética</span>
                                        <div class="detalhe-feature">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quam dolor, sagittis vitae elit vitae, interdum tristique sem. Cras quis elit bibendum, commodo diam eget.
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/negativo-ico.png" alt="Negativo">
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/positivo-ico.png" alt="Positivo">
                                </td>
                            </tr>
                            <tr>
                                <td class="explicacao-feature">
                                    <img src="<?php echo $baseurl ?>/imagens/prontuario-detalhado-ico.png" alt="Prontuário Detalhado">
                                    <div class="conteudo-feature">
                                        <span class="titulo-feature">Prontuário Detalhado</span>
                                        <div class="detalhe-feature">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quam dolor, sagittis vitae elit vitae, interdum tristique sem. Cras quis elit bibendum, commodo diam eget.
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/positivo-ico.png" alt="Negativo">
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/positivo-ico.png" alt="Positivo">
                                </td>
                            </tr>
                            <tr class="cinza-claro">
                                <td class="explicacao-feature">
                                    <img src="<?php echo $baseurl ?>/imagens/estrategia-monitorada-ico.png" alt="Estratégias Nutricionais Monitoradas">
                                    <div class="conteudo-feature">
                                        <span class="titulo-feature">Estratégias Nutricionais Monitoradas</span>
                                        <div class="detalhe-feature">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quam dolor, sagittis vitae elit vitae, interdum tristique sem. Cras quis elit bibendum, commodo diam eget.
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/positivo-ico.png" alt="positivo">
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/positivo-ico.png" alt="Positivo">
                                </td>
                            </tr>
                            <tr>
                                <td class="explicacao-feature">
                                    <img src="<?php echo $baseurl ?>/imagens/avaliacao-clinica-2-ico.png" alt="Avaliação Clínica">
                                    <div class="conteudo-feature">
                                        <span class="titulo-feature">Avaliação Clínica</span>
                                        <div class="detalhe-feature">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quam dolor, sagittis vitae elit vitae, interdum tristique sem. Cras quis elit bibendum, commodo diam eget.
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/positivo-ico.png" alt="positivo">
                                </td>
                                <td class="text-center">
                                    <img src="<?php echo $baseurl ?>/imagens/positivo-ico.png" alt="Positivo">
                                </td>
                            </tr>
                            <tr class="cinza-claro">
                                <td class="explicacao-feature">
                                    
                                </td>
                                <td class="text-center">
                                    <div class="preco">
                                        <span class="moeda">R$</span><span class="reais">420,</span><span class="centavos">00</span>
                                    </div>
                                    <div class="parcelamento">
                                        Em até <b>5x</b> no boleto ou<br />cartão de crédito.
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="preco">
                                        <span class="moeda">R$</span><span class="reais">690,</span><span class="centavos">00</span>
                                    </div>
                                    <div class="parcelamento">
                                        Em até <b>5x</b> no boleto ou<br />cartão de crédito.
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="explicacao-feature">
                                    
                                </td>
                                <td class="text-center">
                                    <a href="#" class="botao-comprar" title="Comprar agora">
                                        <img src="<?php echo $baseurl ?>/imagens/botao-comprar.png" alt="Comprar agora"> Comprar agora
                                    </a>
                                </td>
                                <td class="text-center">
                                    <a href="#" class="botao-testar" title="Testar grátis!">
                                        <img src="<?php echo $baseurl ?>/imagens/botao-teste.png" alt="Testar grátis!"> Comprar agora
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>

        <?php require_once 'includes/footer.php' ?>
    </body>
</html>