<?php require_once 'fatorcms/views/includes/cabecalho.php' ?>

<?php require_once 'fatorcms/views/includes/lateral.php' ?>

	<div id="main-content"> <!-- Main Content Section with everything -->

		<!-- Page Head -->
		<h2>Lista de Novidades cadastradas</h2>
		<p id="page-intro">Abaixo estão listadas todas as novidades que são exibidas no site.</p>

		<?php if (isset($notificacao)) $notificacao->fatorcms_exibir() ?>

		<form action="<?php echo SITE_URL ?>/fatorcms/novidades/listar" method="get" id="novidades_buscar_form">
			<fieldset>
				<p>
					<label for="buscar">Digite uma ou mais palavras-chave para buscar por uma novidade cadastrada:</label>
					<input class="text-input medium-input" type="text" id="buscar" name="buscar" maxlength="100" value="<?php echo isset($buscar) ? $buscar : '' ?>" />
					<input class="button" type="submit" value="Buscar" />
				</p>
			</fieldset>
			<div class="clear"></div><!-- End .clear -->
		</form>

		<?php if (isset($buscar) AND ! is_null($buscar)) { ?>
			<p class="buscar-resultado">
				Foram encontrados <strong><?php echo $paginacao->get_linhas_total ?></strong> resultado(s) para a busca por "<strong><?php echo $buscar ?></strong>".
				<a title="Limpar a busca" href="<?php echo SITE_URL?>/fatorcms/novidades/listar"><img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/magnifier-cross.png" alt="Limpar busca" /> Clique aqui para limpar a busca</a>
			</p>
		<?php } ?>

		<div class="content-box"><!-- Start Content Box -->

			<div class="content-box-header">

				<h3>Novidades</h3>

				<input class="novidade button botao-cadastrar" type="button" value="Cadastrar uma novidade" />

				<div class="clear"></div>

			</div> <!-- End .content-box-header -->

			<div class="content-box-content">

				<table>

					<thead>
						<tr>
							<?php echo Funcoes::montar_th_ordenacao_listagem('novidades', 'listar', $paginacao, $buscar, 'Data', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('novidades', 'listar', $paginacao, $buscar, 'Título', $ordenar_por, $ordem) ?>
							<?php echo Funcoes::montar_th_ordenacao_listagem('novidades', 'listar', $paginacao, $buscar, 'Visível no site?', $ordenar_por, $ordem) ?>
                            <th>URl</th>
                            <th>Site</th>
							<th>Ações</th>
						</tr>
					</thead>

					<tfoot>
						<tr>
							<td colspan="4">

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_quantidades() ?>

								<?php if (isset($paginacao)) $paginacao->fatorcms_exibir_links() ?>

								<div class="clear"></div>
							</td>
						</tr>
					</tfoot>

					<tbody>
						<?php
						if ($novidades AND count($novidades) > 0)
						{
							foreach ($novidades as $novidade)
							{
							?>
								<tr>
									<td class="<?php echo $ordenar_por=='data' ? 'current' : '' ?>"><?php echo date('d/m/Y', strtotime($novidade->data)) ?></td>
									<td class="<?php echo $ordenar_por=='titulo' ? 'current' : '' ?>">
										<a href="<?php echo SITE_URL ?>/fatorcms/novidades/editar/<?php echo $novidade->id ?>" title="Editar a novidade &quot;<?php echo $novidade->titulo ?>&quot;"><?php echo $novidade->titulo ?></a>
									</td>
									<td class="<?php echo $ordenar_por=='visivel-no-site' ? 'current' : '' ?>">
										<span class="<?php echo $novidade->eh_visivel ? 'sim': 'nao' ?>"><?php echo $novidade->eh_visivel ? 'Sim': 'Não' ?></span>
									</td>
                                    <td>
                                        <?php $url = $novidade->alias == 'dietwin' ? 'http://www.dietwin.com.br/blog/post/' : 'http://www.rotulodealimentos.com.br/blog/post/'; ?>
                                        <?php $url .= $novidade->titulo_seo; ?>
                                        <a href="<?php echo $url ?>" target="_blank">
                                            <?php echo $url ?>
                                        </a>
                                    </td>
                                    <td>
                                        <?php echo $novidade->alias == 'dietwin' ? 'DietWin' : 'Rótulo de Alimentos'; ?>
                                    </td>
									<td>
										<!-- Icons -->
										<a href="<?php echo SITE_URL ?>/fatorcms/novidades/editar/<?php echo $novidade->id ?>" title="Editar a novidade &quot;<?php echo $novidade->titulo ?>&quot;">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/pencil.png" alt="Editar" />
										</a>
										<a href="<?php echo SITE_URL ?>/fatorcms/novidades/excluir/<?php echo $novidade->id ?>" title="Excluir a novidade &quot;<?php echo $novidade->titulo ?>&quot;" class="item-confirmar">
											<img src="<?php echo SITE_BASE ?>/fatorcms/views/imagens/icones/cross.png" alt="Excluir" />
										</a>
									</td>
								</tr>
							<?php
							}
						}
						else
						{
							echo '<tr><td colspan="4">'.((isset($buscar) AND ! is_null($buscar)) ? 'Nenhuma novidade encontrada para esta busca.' : 'Nenhuma novidade cadastrada até o momento.' ).'</td></tr>';
						}
						?>
					</tbody>

				</table>

			</div> <!-- End .content-box-content -->

		</div> <!-- End .content-box -->

<?php require_once 'fatorcms/views/includes/rodape.php' ?>