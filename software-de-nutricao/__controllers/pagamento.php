<?php defined('SITE_URL') or die('O acesso direto n&atilde;o &eacute; permitido.');

/* Espaço para comentários, TODOs e explicações das modificações em novas versões desta classe


*/

class Controller_Pagamento extends Controller_Padrao
{
	/**
	 * Chama o construtor da classe pai
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Método inicial que faz a renderização básica da página
	 * @param $parametros
	 * @return void
	 */
	public function index($parametros)
	{
        //Se tentar acessar essa pagina sem antes ter acessado a dados-compra
        if (!isset($_SESSION['licencas']) OR !isset($_SESSION['produto']) OR !isset($_SESSION['processo_compra']) OR !isset($_SESSION['cliente_id']))
        {
            header('HTTP/1.1 404 Not Found');
            $erro_404 = new Controller_Erro;
            $erro_404->index(404);
            exit;
        }

        //Se meter uma url q nao tenha a categoria
        if (!isset($parametros->categoria) OR !strlen($parametros->categoria) >0)
        {
            header('HTTP/1.1 404 Not Found');
            $erro_404 = new Controller_Erro;
            $erro_404->index(404);
            exit;
        }
        //caso vá profissional-plus redirecionamos pro profissional "velho"
        if (isset($parametros->categoria) AND $parametros->categoria == 'profissional-plus')
        {
            header('Location: '.SITE_URL.'/software/profissional/pagamento');
            exit;
        }

        $cliente = new Model_Cliente($_SESSION['cliente_id']);
        if ($cliente->cadastro_completo == 0)
        {
            $notificacao = new Notificacao('Você precisa completar os seus dados para poder finalizar a compra','erro',TRUE);
            header('Location: '.SITE_URL.'/area-do-cliente/editar-dados'); exit;
        }

        if (isset($_SESSION['cupom_id']))
        {
            $cupom = new Model_Cupom($_SESSION['cupom_id']);

            if (!is_null($cupom->cliente_id) AND $cliente->id != $cupom->cliente_id)
            {
                unset($_SESSION['cupom_id']);
            }
        }

        //Para definir as cores da pagina de acordo com o produto da url
        $produto_body = new Model_Produto();
        $busca = $parametros->categoria;
        if($busca == 'profissional'){
            $busca = 'profissional-plus';
        }
        $produto_body = $produto_body->select("SELECT * FROM {tabela_nome} WHERE nome_seo='dietwin-".Funcoes::mysqli_escape($busca)."'");

        if (isset($produto_body) AND $produto_body)
        {
            $classe = $parametros->categoria;
        }
        else
        {
            $classe = substr($_SESSION['produto']->nome_seo,8);
        }

        //Recuperar o endereço de entrega do cliente
        $endereco = new Model_ClienteEndereco;
        $endereco = $endereco->select('SELECT * FROM {tabela_nome} WHERE entrega=1 AND cliente_id='.$_SESSION['cliente_id']);
        if ( ! $endereco OR is_null($endereco->id))
        {
	        $endereco = new Model_ClienteEndereco;
            $endereco = $endereco->select('SELECT * FROM {tabela_nome} WHERE entrega=0 AND cliente_id='.$_SESSION['cliente_id']);
        }

        //Recupera a cidade e o estado do endereco de entrega
        $cidade = new Model_Cidade($endereco->cidade_id);
        $estado = new Model_Estado($endereco->estado_id);

        //Recupera o frete
        $controller_frete = new Controller_Frete();
        $frete = $controller_frete->calcular($endereco->cep);

        if ($frete == 0)
        {
            $notificacao = new Notificacao('CEP inválido, corrija os seus dados para poder finalizar a compra','erro',TRUE);
            header('Location: '.SITE_URL.'/area-do-cliente/editar-dados'); exit;
        }
        else if ( $endereco->cep == '00000000')
        {
            $notificacao = new Notificacao('CEP inválido, corrija os seus dados para poder finalizar a compra','erro',TRUE);
            header('Location: '.SITE_URL.'/area-do-cliente/editar-dados'); exit;
        }

        $view = new View('pagamento.php');
		$this->view_variaveis_obrigatorias($view);

        $view->adicionar('classe',$classe);

        $view->adicionar('frete',$frete);

        $view->adicionar('cupom',(isset($_SESSION['cupom']))?$_SESSION['cupom']:NULL);
        $view->adicionar('produto',$_SESSION['produto']);
        $view->adicionar('licencas',$_SESSION['licencas']);

        $view->adicionar('endereco',$endereco);
        $view->adicionar('cidade',$cidade);
        $view->adicionar('estado',$estado);

		$view->adicionar('body_class', 'pagamento');
		$view->adicionar('notificacao', new Notificacao);

        $view->adicionar('pagina_title', 'Escolha a forma de pagamento - dietWin - Softwares de nutrição - Encontre o melhor software para você!');

		$view->exibir();
	}

	/* ***************************** MÉTODOS EXTRAS ***************************** */


} // end class