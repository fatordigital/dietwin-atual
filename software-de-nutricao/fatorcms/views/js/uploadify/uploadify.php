<?php
//---------- cópia do index.php

include_once '../../../../config.php';

define('SITE_BASE', 'http://'.$_SERVER['HTTP_HOST']);
// Utilizado para requisições pelo sistema, como formulários e links
define('SITE_URL', 'http://'.$_SERVER['HTTP_HOST']);

// Salva onde o sistema está sendo executado para buscar as configurações
if (strpos($_SERVER['HTTP_HOST'], 'fdserver') !== FALSE OR strpos($_SERVER['HTTP_HOST'], 'localhost') !== FALSE)
{
	$site_local = 'fdserver';
}
else
{
	$site_local = 'cliente';
}
define ('SITE_LOCAL', $site_local);

//---------- includes

include_once '../../../../sistema/log.php';
include_once '../../../../sistema/notificacao.php';
include_once '../../../../sistema/sistema.php';
include_once '../../../../sistema/bd.php';
include_once '../../../../fatorcms/models/padrao.php';
include_once '../../../../fatorcms/models/galeriafoto.php';
include_once '../../../../biblioteca/funcoes.php';
include_once '../../../../biblioteca/WideImage/WideImage.php';

//---------- lógica do upload

$galeria_id = trim($_POST['galeria_id']);

$log = new Log('Chegou no arquivo do uploadfy');

// Criamos os diretórios se necessário
if ( ! is_dir('../../../../arquivos/galerias/'.$galeria_id ) OR ! is_writable('../../../../arquivos/galerias/'.$galeria_id ) )
{
    @mkdir('../../../../arquivos/galerias/'.$galeria_id, 0777, TRUE);
    @mkdir('../../../../arquivos/galerias/'.$galeria_id.'/thumb1', 0777, TRUE);
    @mkdir('../../../../arquivos/galerias/'.$galeria_id.'/thumb2', 0777, TRUE);

    @chmod('../../../../arquivos/galerias/'.$galeria_id, 0777);
    @chmod('../../../../arquivos/galerias/'.$galeria_id.'/thumb1', 0777);
    @chmod('../../../../arquivos/galerias/'.$galeria_id.'/thumb2', 0777);
}

$i = 0;

foreach ($_FILES as $arquivos)
{
    $imagem = $arquivos['tmp_name'][$i];
    $erro = $arquivos['error'][$i];
    $nome = $arquivos['name'][$i];

    // Pegamos a string que vem depois do último ponto
    $imagem_extensao = strtolower(substr($nome, strrpos($nome,'.')+1));

    if ($erro === UPLOAD_ERR_OK AND in_array($imagem_extensao, array('jpg','jpeg')))
    {
        $imagem = WideImage::loadFromFile($imagem);
        $imagem_novo_nome = md5($nome.time()).'.'.$imagem_extensao;

        $imagem_nova = $imagem->resize(800, NULL, 'inside', 'down');
        $imagem_nova->saveToFile('../../../../arquivos/galerias/'.$galeria_id.'/'.$imagem_novo_nome, 80);

        $imagem_thumb_1 = $imagem->resize(136, NULL, 'outside', 'any')->crop('center', 'center', 136, 96);
        $imagem_thumb_1->saveToFile('../../../../arquivos/galerias/'.$galeria_id.'/thumb1/'.$imagem_novo_nome, 80);

        $imagem_thumb_2 = $imagem->resize(200, NULL, 'outside', 'any');
        $imagem_thumb_2->saveToFile('../../../../arquivos/galerias/'.$galeria_id.'/thumb2/'.$imagem_novo_nome, 85);

        $banco_imagem = new FatorCMS_Model_GaleriaFoto;
        $banco_imagem->galeria_id = Funcoes::mysqli_escape($_POST['galeria_id']);
        $banco_imagem->arquivo = $imagem_novo_nome;

        if ( ! $banco_imagem->insert())
        {
            new Log('Erro: ' . mysqli_error());
        }
    }

    $i++;
}

echo 1;