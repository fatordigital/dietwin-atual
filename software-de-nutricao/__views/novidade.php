<?php include 'includes/cabecalho.php'; ?>

<body class="novidades novidade">

<?php include 'includes/topo.php'; ?>

<div class="titulo-principal">
    <p><span>Acompanhe as</span><br />novidades</p>
</div>
<div class="titulo-resumo">
    <p>A equipe do dietWin disponibiliza sempre as últimas novidades de seus softwares para que você possa estar atualizado sobre toda a tecnologia oferecida!</p>
</div>
</div>
</div>

<div class="conteudo">
    <div class="container">

        <?php include 'includes/toolbar.php' ?>

        <div class="texto" id="tamanho-fonte">

            <h1><?php echo mb_strtolower($novidade->titulo) ?></h1>

            <p class="data"><?php echo substr($novidade->data,8,2).'/'.substr($novidade->data,5,2).'/'.substr($novidade->data,0,4); ?></p>
            <?php
            if (isset($novidade->imagem))
            {
            ?>
            <div class="imagem">
                <a href="<?php echo SITE_BASE ?>/arquivos/novidades/<?php echo $novidade->imagem ?>" rel="novidade" title="<?php echo $novidade->titulo ?>"><img src="<?php echo SITE_BASE ?>/arquivos/novidades/thumb1/<?php echo $novidade->imagem ?>" alt="<?php echo $novidade->titulo ?>" /></a>
            </div>
            <?php
            }
            ?>
            <?php echo $novidade->conteudo ?>
            <div class="clear"></div>
        </div>

        <div class="sidebar" id="tamanho-fonte">
            <a href="<?php echo SITE_URL ?>/novidades" title="Voltar para listagem de novidades">Voltar para listagem de novidades &#187;</a>
            <div class="filtro">
                <h2>Outras novidades</h2>
                <?php
                if ($outras_novidades AND count($outras_novidades) > 0)
                {
                ?>
                <ul>
                    <?php
                    $total = count($outras_novidades);
                    $i = 0;
                    foreach ($outras_novidades as $novidade)
                    {
                    ?>
                    <li<?php echo (++$i == $total) ? ' class="sem-borda"' : '' ?>>
                        <p class="data"><?php echo substr($novidade->data,8,2).'/'.substr($novidade->data,5,2).'/'.substr($novidade->data,0,4); ?></p>
                        <h3><?php echo mb_strtolower($novidade->titulo) ?></h3>
                        <p><?php echo Funcoes::cortar_texto($novidade->resumo,100); ?></p>
                        <a href="<?php echo SITE_URL ?>/novidade/<?php echo $novidade->titulo_seo ?>" title="Ler a novidade completa">Ler a novidade completa &#187;</a>
                    </li>
                    <?php
                    }
                    ?>

                </ul>
                <?php 
                }
                else
                {
                    echo '<p>Não há outras novidades cadastradas.</p>';
                }
                ?>
            </div>

            <div class="filtro">
                <h2>Busque por palavra-chave</h2>
                <form method="post" action="<?php echo SITE_URL.'/novidades/' ?>" id="buscar" >
                    <input type="text" name="buscar" id="busca-campo" maxlength="255" value="" />
                    <button type="submit">Buscar</button>
                </form>
                <div class="clear"></div>
            </div>

        </div>

        <div class="clear"></div>
    </div>
    <div class="inferior"></div>
</div>

<?php include 'includes/rodape.php'; ?>