<?php require_once 'includes/head.php' ?>
<body>
<?php require_once 'includes/nav.php' ?>

<section class="conteudo cinza">
  <div class="container">
  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	<h1 class="interna">Orçamento personalizado</h1>
    	<p>Preencha os campos abaixo para receber um orçamento personalizado referente ao produto que deseja!</p>
	</div>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<form action="#" method="post" class="form-horizontal" id="orcamento">
			<div class="form-group">
				<label for="nome" class="col-lg-2 col-md-2 col-sm-4 col-xs-12 text-right text-left-xs control-label">Nome:</label>
				<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
					<input type="text" id="nome" name="nome" class="form-control" data-rule-required="true" data-msg-required="Informe o seu nome">
				</div>
			</div>
			<div class="form-group">
				<label for="cpf_cnpj" class="col-lg-2 col-md-2 col-sm-4 col-xs-12 text-right text-left-xs control-label">CPF/CNPJ:</label>
				<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
					<input type="text" id="cpf_cnpj" name="cpf_cnpj" class="form-control" data-rule-required="true" data-msg-required="Informe o seu CPF/CNPJ">
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="col-lg-2 col-md-2 col-sm-4 col-xs-12 text-right text-left-xs control-label">E-mail:</label>
				<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
					<input type="email" id="email" name="email" class="form-control" data-rule-required="true" data-msg-required="Informe o seu e-mail" data-rule-email="true" data-msg-email="Informe um e-mail válido">
				</div>
			</div>
			<div class="form-group">
				<label for="telefone" class="col-lg-2 col-md-2 col-sm-4 col-xs-12 text-right text-left-xs control-label">Telefone:</label>
				<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
					<input type="text" id="telefone" name="telefone" class="form-control" data-rule-required="true" data-msg-required="Informe o seu telefone">
				</div>
			</div>
			<div class="form-group">
				<label for="celular" class="col-lg-2 col-md-2 col-sm-4 col-xs-12 text-right text-left-xs control-label">Celular:</label>
				<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
					<input type="text" id="celular" name="celular" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label for="endereco" class="col-lg-2 col-md-2 col-sm-4 col-xs-12 text-right text-left-xs control-label">Endereço:</label>
				<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
					<input type="text" id="endereco" name="endereco" class="form-control" data-rule-required="true" data-msg-required="Informe o seu endereço">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="row">
					<div class="form-group">
						<label for="numero" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs control-label">Número:</label>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<input type="text" id="numero" name="numero" class="form-control" data-rule-required="true" data-msg-required="Informe o número do endereço">
						</div>
					</div>
					<div class="form-group">
						<label for="cidade" class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs control-label">Cidade:</label>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<input type="text" id="cidade" name="cidade" class="form-control" data-rule-required="true" data-msg-required="Informe a cidade">
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="row">
					<div class="form-group">
						<label for="complemento" class="col-lg-3 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs control-label">Complemento:</label>
						<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
							<input type="text" id="complemento" name="complemento" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="estado" class="col-lg-3 col-md-4 col-sm-4 col-xs-12 text-right text-left-xs control-label">Estado:</label>
						<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
							<select name="estado" id="estado" data-rule-required="true" data-msg-required="Informe o estado" class="form-control">
								<option value="">Selecione</option>
								<option value="Acre">Acre</option>
			                    <option value="Alagoas">Alagoas</option>
			                    <option value="Amapá">Amapá</option>
			                    <option value="Amazonas">Amazonas</option>
			                    <option value="Bahia">Bahia</option>
			                    <option value="Ceará">Ceará</option>
			                    <option value="Distrito Federal">Distrito Federal</option>
			                    <option value="Espírito Santo">Espírito Santo</option>
			                    <option value="Goiás">Goiás</option>
			                    <option value="Maranhão">Maranhão</option>
			                    <option value="Mato Grosso">Mato Grosso</option>
			                    <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
			                    <option value="Minas Gerais">Minas Gerais</option>
			                    <option value="Pará">Pará</option>
			                    <option value="Paraíba">Paraíba</option>
			                    <option value="Paraná">Paraná</option>
			                    <option value="Pernambuco">Pernambuco</option>
			                    <option value="Piauí">Piauí</option>
			                    <option value="Rio de Janeiro">Rio de Janeiro</option>
			                    <option value="Rio Grande do Norte">Rio Grande do Norte</option>
			                    <option value="Rio Grande do Sul">Rio Grande do Sul</option>
			                    <option value="Rondônia">Rondônia</option>
			                    <option value="Roraima">Roraima</option>
			                    <option value="Santa Catarina">Santa Catarina</option>
			                    <option value="São Paulo">São Paulo</option>
			                    <option value="Sergipe">Sergipe</option>
			                    <option value="Tocantins">Tocantins</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
				<label for="cep" class="col-lg-2 col-md-2 col-sm-4 col-xs-12 text-right text-left-xs control-label">CEP:</label>
				<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
					<input type="text" id="cep" name="cep" class="form-control" data-rule-required="true" data-msg-required="Informe o CEP">
				</div>
			</div>
			<div class="form-group">
				<strong class="col-lg-2 col-md-2 col-sm-4 col-xs-12 text-right text-left-xs">Qual software você deseja?</strong>
				<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
					<div class="checkbox">
						<label>
							<input type="checkbox" name="produto[]" id="produto" data-rule-required="true" value="Dietwin" />Dietwin
						</label>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="produto[]" id="produto" value="Dietwin Plus" />Dietwin Plus
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="forma_pagamento" class="col-lg-2 col-md-2 col-sm-4 col-xs-12 text-right text-left-xs control-label">Alguma forma de pagamento especial?</label>
				<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
					<textarea class="form-control" name="forma_pagamento" id="forma_pagamento" style="overflow:auto"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label for="info_adicional" class="col-lg-2 col-md-2 col-sm-4 col-xs-12 text-right text-left-xs control-label">Informações Adicionais:</label>
				<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
					<textarea class="form-control" name="info_adicional" id="info_adicional" style="overflow:auto"></textarea>
				</div>
			</div>
			<div class="erro-form col-lg-offset-2 col-md-offset-2" style="margin-bottom: 10px"></div>
			<div class="ok-form col-lg-offset-2 col-md-offset-2" style="margin-bottom: 10px; display: none; background: #97b58e; padding: 10px; color: #fff; font-weight: bold;"></div>
			<button type="submit" class="col-lg-offset-2 col-md-offset-2 btn btn-success">Enviar Orçamento</button>
		</form>
	</div>
  </div>
</section>

<?php require_once 'includes/footer.php' ?>
</body>
</html>