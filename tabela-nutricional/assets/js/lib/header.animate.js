(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
lib.webFontTxtFilters = {};

// library properties:
lib.properties = {
	width: window.innerWidth - 50,
	height: 889,
	fps: 40,
	color: "#FFFFFF",
	opacity: 0.00,
	webfonts: {},
	manifest: [
		{src:"assets/images/header-animate.png", id:"header-animate"}
	]
};



lib.ssMetadata = [
		{name:"header-animate", frames: [[1922,2124,1920,4],[1922,1712,681,410],[0,0,1920,854],[0,856,1920,854],[0,1712,1920,854],[1922,0,1920,854],[1922,856,1920,854],[3844,0,115,93],[3844,95,105,95],[3951,113,90,92],[3844,192,69,111],[3961,0,95,111]]}
];


lib.webfontAvailable = function(family) {
	lib.properties.webfonts[family] = true;
	var txtFilters = lib.webFontTxtFilters && lib.webFontTxtFilters[family] || [];
	for(var f = 0; f < txtFilters.length; ++f) {
		txtFilters[f].updateCache();
	}
};
// symbols:



(lib.Bitmap2 = function() {
	this.spriteSheet = ss["header-animate"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.MacBookSilver1Image = function() {
	this.spriteSheet = ss["header-animate"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Shape17Image = function() {
	this.spriteSheet = ss["header-animate"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.Shape17Image_1 = function() {
	this.spriteSheet = ss["header-animate"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.Shape17Image_2 = function() {
	this.spriteSheet = ss["header-animate"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.Shape17Image_3 = function() {
	this.spriteSheet = ss["header-animate"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.Shape17Image_4 = function() {
	this.spriteSheet = ss["header-animate"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.Shape1Image = function() {
	this.spriteSheet = ss["header-animate"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.Shape2Image = function() {
	this.spriteSheet = ss["header-animate"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.Shape3Image = function() {
	this.spriteSheet = ss["header-animate"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.Shape4Image = function() {
	this.spriteSheet = ss["header-animate"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.Shape5Image = function() {
	this.spriteSheet = ss["header-animate"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.maça = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.Shape5Image();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,95,111.1);


(lib.mack = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.MacBookSilver1Image();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,681.2,410.1);


(lib.laranja = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.Shape3Image();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,90,92);


(lib.Shape17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Ag6CMQgGAAAAgKIAAkDQAAgKAGAAIB1AAQAFAAABAKIAAEDQgBAKgFAAgAAgCHIAbAAQADAAAAgFIAAisIgeAAgAAACHIAeAAIAAixIgeAAgAg+CCQAAAFAEAAIA5AAIAAixIg9AAgAAgguIAeAAIAAgSIgeAAgAg+guIBcAAIAAgSIhcAAgAARhOIAfAAIAAgIIgfAAgAgwhOIA2AAIAAgIIg2AAgAg+iBIAAAgIB8AAIAAggQAAgFgDAAIh1AAQgEAAAAAFgAArB4IAAgHIAGAAIAAAHgAAIB4IAAgHIAMAAIAAAHgAg2B4IAAgHIAcAAIAAAHgAArBnIAAgHIAGAAIAAAHgAAIBnIAAgHIAMAAIAAAHgAg2BnIAAgHIAnAAIAAAHgAArBXIAAgIIAGAAIAAAIgAAIBXIAAgIIAMAAIAAAIgAg2BXIAAgIIAkAAIAAAIgAArBGIAAgHIAGAAIAAAHgAAIBGIAAgHIAMAAIAAAHgAg2BGIAAgHIAnAAIAAAHgAArA2IAAgIIAGAAIAAAIgAAIA2IAAgIIAMAAIAAAIgAg2A2IAAgIIAyAAIAAAIgAArAlIAAgHIAGAAIAAAHgAAIAlIAAgHIAMAAIAAAHgAg2AlIAAgHIApAAIAAAHgAArAUIAAgHIAGAAIAAAHgAAIAUIAAgHIAMAAIAAAHgAg2AUIAAgHIAyAAIAAAHgAAoAEIAAgGIAMAAIAAAGgAAIAEIAAgGIAMAAIAAAGgAg2AEIAAgGIAyAAIAAAGgAAogLIAAgHIAMAAIAAAHgAAIgLIAAgHIAMAAIAAAHgAg2gLIAAgHIAyAAIAAAHgAAogbIAAgIIAMAAIAAAIgAAIgbIAAgIIAMAAIAAAIgAg2gbIAAgIIAwAAIAAAIgAAkgzIAAgIIAVAAIAAAIgAg2gzIAAgIIBNAAIAAAIgAg2hqIAAgTIBvAAIAAATg");
	mask.setTransform(1868.9,648.1);

	// Camada 3
	this.instance = new lib.Shape17Image();

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1862.4,634.1,13,28.1);


(lib.Shape17_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 2 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AhyCbQgEAAgDgDQgCgDAAgFIAAkfQAAgEACgEQADgDAEAAIDkAAQAEAAADADQADAEAAAEIAAEfQAAAFgDADQgDADgEAAgAA+CWIA0AAQAGAAAAgGIAAi/Ig6AAgAAACWIA6AAIAAjFIg6AAgAh4CQQABAGAFAAIBvAAIAAjFIh1AAgAA+gzIA6AAIAAgUIg6AAgAh4gzICyAAIAAgUIiyAAgAAihWIA8AAIAAgJIg8AAgAhdhWIBqAAIAAgJIhqAAgAh4iPIAAAjIDwAAIAAgjQAAgGgGAAIjkAAQgFAAgBAGgABUCFIAAgIIALAAIAAAIgAAQCFIAAgIIAWAAIAAAIgAhqCFIAAgIIA3AAIAAAIgABUBzIAAgJIALAAIAAAJgAAQBzIAAgJIAWAAIAAAJgAhqBzIAAgJIBMAAIAAAJgABUBgIAAgIIALAAIAAAIgAAQBgIAAgIIAWAAIAAAIgAhqBgIAAgIIBGAAIAAAIgABUBOIAAgIIALAAIAAAIgAAQBOIAAgIIAWAAIAAAIgAhqBOIAAgIIBMAAIAAAIgABUA7IAAgIIALAAIAAAIgAAQA7IAAgIIAWAAIAAAIgAhqA7IAAgIIBgAAIAAAIgABUApIAAgIIALAAIAAAIgAAQApIAAgIIAWAAIAAAIgAhqApIAAgIIBPAAIAAAIgABUAXIAAgJIALAAIAAAJgAAQAXIAAgJIAWAAIAAAJgAhqAXIAAgJIBgAAIAAAJgABOAEIAAgGIAXAAIAAAGgAAQAEIAAgGIAWAAIAAAGgAhqAEIAAgGIBgAAIAAAGgABOgMIAAgIIAXAAIAAAIgAAQgMIAAgIIAWAAIAAAIgAhqgMIAAgIIBgAAIAAAIgABOgeIAAgJIAXAAIAAAJgAAQgeIAAgJIAWAAIAAAJgAhqgeIAAgJIBcAAIAAAJgABFg5IAAgIIApAAIAAAIgAhog5IAAgIICWAAIAAAIgAhqh2IAAgVIDYAAIAAAVg");
	mask_1.setTransform(1710.4,624.2);

	// Camada 3
	this.instance_1 = new lib.Shape17Image_1();

	this.instance_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1697.9,608.7,25,31);


(lib.Shape17_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 2 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("Ah/CuQgFAAgDgCQgEgEAAgEIAAlGQAAgFAEgDQADgEAFAAID/AAQAGAAACAEQADADABAFIAAFGQgBAEgDAEQgCACgGAAgABGCqIA6AAQAHgBgBgFIAAjYIhAAAgAAACqIBAAAIAAjeIhAAAgAiGCkQAAAFAHABIB7AAIAAjeIiCAAgABGg6IBAAAIAAgWIhAAAgAiGg6IDGAAIAAgWIjGAAgAAnhhIBCAAIAAgKIhCAAgAhohhIB3AAIAAgKIh3AAgAiGiiIAAAoIEMAAIAAgoQABgGgHAAIj/AAQgHAAAAAGgABeCXIAAgKIANAAIAAAKgAASCXIAAgKIAZAAIAAAKgAh2CXIAAgKIA9AAIAAAKgABeCBIAAgJIANAAIAAAJgAASCBIAAgJIAZAAIAAAJgAh2CBIAAgJIBUAAIAAAJgABeBtIAAgJIANAAIAAAJgAASBtIAAgJIAZAAIAAAJgAh2BtIAAgJIBOAAIAAAJgABeBYIAAgJIANAAIAAAJgAASBYIAAgJIAZAAIAAAJgAh2BYIAAgJIBUAAIAAAJgABeBDIAAgJIANAAIAAAJgAASBDIAAgJIAZAAIAAAJgAh2BDIAAgJIBrAAIAAAJgABeAvIAAgKIANAAIAAAKgAASAvIAAgKIAZAAIAAAKgAh2AvIAAgKIBYAAIAAAKgABeAaIAAgJIANAAIAAAJgAASAaIAAgJIAZAAIAAAJgAh2AaIAAgJIBrAAIAAAJgABYAFIAAgHIAZAAIAAAHgAASAFIAAgHIAZAAIAAAHgAh2AFIAAgHIBrAAIAAAHgABYgOIAAgJIAZAAIAAAJgAASgOIAAgJIAZAAIAAAJgAh2gOIAAgJIBrAAIAAAJgABYgiIAAgJIAZAAIAAAJgAASgiIAAgJIAZAAIAAAJgAh2giIAAgJIBmAAIAAAJgABOhBIAAgJIAtAAIAAAJgAh1hBIAAgJICoAAIAAAJgAh2iFIAAgYIDxAAIAAAYg");
	mask_2.setTransform(1599.3,627.6);

	// Camada 3
	this.instance_2 = new lib.Shape17Image_2();

	this.instance_2.mask = mask_2;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1585.3,610.1,28,35);


(lib.Shape17_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 2 (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("AipDhQgGAAgEgFQgFgEABgGIAAmiQgBgHAFgEQAEgEAGAAIFTAAQAGAAAEAEQAFAEAAAHIAAGiQAAAGgFAEQgEAFgGAAgABcDaIBOAAQADAAADgCQACgCAAgEIAAkWIhWAAgAAADaIBWAAIAAkeIhWAAgAixDSQAAAIAIAAICkAAIAAkeIisAAgABchLIBWAAIAAgdIhWAAgAixhLIEHAAIAAgdIkHAAgAA0h9IBXAAIAAgOIhXAAgAiKh9ICeAAIAAgOIieAAgAixjQIAAAyIFjAAIAAgyQAAgEgCgCQgDgCgDAAIlTAAQgIAAAAAIgAB8DBIAAgMIASAAIAAAMgAAYDBIAAgMIAhAAIAAAMgAicDBIAAgMIBQAAIAAAMgAB8CmIAAgMIASAAIAAAMgAAYCmIAAgMIAhAAIAAAMgAicCmIAAgMIBuAAIAAAMgAB8CMIAAgMIASAAIAAAMgAAYCMIAAgMIAhAAIAAAMgAicCMIAAgMIBmAAIAAAMgAB8ByIAAgNIASAAIAAANgAAYByIAAgNIAhAAIAAANgAicByIAAgNIBuAAIAAANgAB8BXIAAgNIASAAIAAANgAAYBXIAAgNIAhAAIAAANgAicBXIAAgNICNAAIAAANgAB8A8IAAgMIASAAIAAAMgAAYA8IAAgMIAhAAIAAAMgAicA8IAAgMIBzAAIAAAMgAB8AiIAAgNIASAAIAAANgAAYAiIAAgNIAhAAIAAANgAicAiIAAgNICNAAIAAANgAB0AHIAAgLIAiAAIAAALgAAYAHIAAgLIAhAAIAAALgAicAHIAAgLICNAAIAAALgAB0gSIAAgMIAiAAIAAAMgAAYgSIAAgMIAhAAIAAAMgAicgSIAAgMICNAAIAAAMgAB0gsIAAgNIAiAAIAAANgAAYgsIAAgNIAhAAIAAANgAicgsIAAgNICHAAIAAANgABnhUIAAgMIA7AAIAAAMgAibhUIAAgMIDfAAIAAAMgAicirIAAgfIE+AAIAAAfg");
	mask_3.setTransform(1452.8,619.6);

	// Camada 3
	this.instance_3 = new lib.Shape17Image_3();

	this.instance_3.mask = mask_3;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1434.3,597.1,37,45);


(lib.Shape17_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 2 (mask)
	var mask_4 = new cjs.Shape();
	mask_4._off = true;
	mask_4.graphics.p("AjTEdQgHAAgFgGQgFgFgBgIIAAoTQABgIAFgFQAFgGAHAAIGmAAQAIAAAGAGQAEAFAAAIIAAITQAAAIgEAFQgGAGgIAAgAByEUIBhAAQAFAAADgDQACgDAAgEIAAlhIhrAAgAAAEUIBqAAIAAlrIhqAAgAjcEKQAAAEACADQADADAEAAIDNAAIAAlrIjWAAgAByhfIBrAAIAAglIhrAAgAjchfIFGAAIAAglIlGAAgABBifIBsAAIAAgRIhsAAgAisifIDFAAIAAgRIjFAAgAjakQQgCADAAAEIAABBIG5AAIAAhBQAAgEgCgDQgDgDgFAAImmAAQgEAAgDADgACbD1IAAgPIAVAAIAAAPgAAeD1IAAgPIApAAIAAAPgAjDD1IAAgPIBlAAIAAAPgACbDTIAAgPIAVAAIAAAPgAAeDTIAAgPIApAAIAAAPgAjDDTIAAgPICKAAIAAAPgACbCxIAAgPIAVAAIAAAPgAAeCxIAAgPIApAAIAAAPgAjDCxIAAgPICAAAIAAAPgACbCPIAAgPIAVAAIAAAPgAAeCPIAAgPIApAAIAAAPgAjDCPIAAgPICKAAIAAAPgACbBuIAAgQIAVAAIAAAQgAAeBuIAAgQIApAAIAAAQgAjDBuIAAgQICwAAIAAAQgACbBMIAAgPIAVAAIAAAPgAAeBMIAAgPIApAAIAAAPgAjDBMIAAgPICRAAIAAAPgACbAqIAAgPIAVAAIAAAPgAAeAqIAAgPIApAAIAAAPgAjDAqIAAgPICwAAIAAAPgACRAIIAAgNIApAAIAAANgAAeAIIAAgNIApAAIAAANgAjDAIIAAgNICwAAIAAANgACRgXIAAgQIApAAIAAAQgAAegXIAAgQIApAAIAAAQgAjDgXIAAgQICwAAIAAAQgACRg5IAAgPIApAAIAAAPgAAeg5IAAgPIApAAIAAAPgAjDg5IAAgPICoAAIAAAPgACBhqIAAgPIBJAAIAAAPgAjBhqIAAgPIEVAAIAAAPgAjDjaIAAgnIGNAAIAAAng");
	mask_4.setTransform(1321.3,624.6);

	// Camada 3
	this.instance_4 = new lib.Shape17Image_4();

	this.instance_4.mask = mask_4;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1298.3,596.1,46,57.1);


(lib.Couve = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.Shape2Image();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,105,95);


(lib.cenoura = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.Shape4Image();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,69,111.1);


(lib.bananas = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.Shape1Image();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,115.1,93);


(lib.rot5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.Shape17();
	this.instance.setTransform(-841.1,-121.1,1,1,0,0,0,960.2,427.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2DAD7").s().p("AmKIlIAAtdIBhhmIAAiGIJTAAIAACGIBhBmIAANdgADIkoIAAMlICdAAIAAslIhPhSgAlkH9IIGAAIAAsZIoGAAgAlJlEIH2AAIA7g+In2AAgAkCmqIIFAAIAAhTIoFAAg");
	this.shape.setTransform(39.6,64.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1801.3,-548.2,1920.5,854.2);


(lib.rot4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.Shape17_1();
	this.instance.setTransform(-714.2,-109.4,1,1,0,0,0,960.2,427.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2DAD7").s().p("AjuJXQgMAAgLgHQgKgFgHgOQgDgGgDgCIgGgFIgNgLQgPgOAAgVIAAsAQAAghAYgYIAZgVIAPgLQAHgHAJgPIADgGQgVgRAAgaQAAgXARgQQAQgQAXAAIBzAAIAAgwQAAghAZgYQAYgYAiAAQAhAAAYAYQAZAYAAAhIAAAwIB5AAQAXAAARAQQAQAQAAAXQAAAagVARIADAGQAJAPAHAHIAPALQARAMAIAJQAYAYAAAhIAAMAQAAAWgOANQgGAGgIAFIgGAFIgGAIQgHAOgJAFQgMAHgMAAgAkVICQAAAEADADIAJAHIAJAHQAGAFAIANIADAFIHeAAIACAAIACgFQAIgMAGgGIASgOQADgCAAgFIAAiZIorAAgAkVFAIIrAAIAAkpIiMAAIgFgNQgQgngkgZQglgZgsAAQgrAAglAZQgkAZgQAnIgFANIiMAAgABihbQAuAcAWAwIBwAAIAAjaIjgAAIAAgpIDaAAQgCgFgEgDIgVgRIgRgOQgMgKgNgWIgJgOImDAAIgJAOQgNAWgMAKIgRAOQgOAKgHAHIgGAIIBBAAIAAApIhHAAIAADaIBwAAQAWgwAugcQAtgdA0AAQA1AAAtAdgAjSmoQgFAEAAAGQAAAGAFAEQAEAEAGAAIGRAAQAGAAAEgEQAFgEAAgGQAAgGgFgEQgEgFgGAAImRAAQgGAAgEAFgAgfoiQgNAMAAARIAAAwIBSAAIAAgwQAAgRgMgMQgMgMgQAAQgRAAgMAMg");
	this.shape.setTransform(36.9,71.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1674.4,-536.5,1920.5,854.2);


(lib.Rot2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.Shape17_3();
	this.instance.setTransform(-458.1,-100.8,1,1,0,0,0,960.2,427.1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2DAD7").s().p("AjSJ4QgzAAglgmQgkgmAAg1IAAqBQAAg4AfgqQASgaAvgkQAnghAPgSQAYgeAEgmQgPgHgJgOQgKgPAAgSQAAgSAKgPQgXgWAAggQAAgeAUgWQAUgVAdAAIENAAQAdAAAUAVQAUAWAAAeQAAAggXAWQAKAPAAASQAAASgJAPQgKAOgPAIQAEAlAYAeQAPASAnAhQAvAkASAaQAfAqAAA4IAAKBQAAA1gkAmQglAmgzAAgAiXlDQgSAYgsAiQgpAigPATQgYAgAAAqIAAKBQAAAjAZAaQAYAZAiAAIGlAAQAiAAAZgZQAYgaAAgjIAAqBQAAgqgYggQgPgTgpgiQgsgigSgYQgfgngGgyIjlAAQgGAygfAngAiQniQgFAFAAAGQAAAGAFAFQAEAFAGgBIENAAQAGABAEgFQAFgFAAgGQAAgGgFgFQgEgFgGABIkNAAQgGgBgEAFgAiapDQgIAJAAAMQAAALAIAJQAJAJALAAIENAAQALAAAJgJQAIgJAAgLQAAgMgIgJQgJgJgLAAIkNAAQgLAAgJAJg");
	this.shape.setTransform(33.6,74.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1418.3,-527.9,1920.5,854.2);


(lib.rot1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.Shape17_4();
	this.instance.setTransform(-323,-120.7,1,1,0,0,0,960.2,427.1);
	this.instance.alpha = 0.5;

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2DAD7").s().p("AkWIXQgsAAgegfQgggfAAgsIAAqCQAAgtAbgjQAQgVAngeQAhgZAMgPQAUgYADgeQgNgFgIgNQgJgNAAgPQAAgUAOgPQAPgPAVAAIGtAAQAUAAAPAPQAPAPAAAUQAAAPgIANQgJANgOAFQAEAeAUAYQAMAPAhAZQAnAeAQAVQAaAjABAtIAAKCQAAAsggAfQgfAfgrAAgAjjlqQgQATglAcQgiAbgNAQQgUAZAAAiIAAKCQAAAcAVAVQATAUAdAAIItAAQAcAAAVgUQAUgVAAgcIAAqCQAAgigUgZQgNgQgigbQglgcgPgTQgbgggFgoImKAAQgEAogaAggAjfntQgEAEAAAFQAAAGAEADQAEAEAFAAIGtAAQAFAAAEgEQAEgDAAgGQAAgFgEgEQgEgEgFAAImtAAQgFAAgEAEg");
	this.shape.setTransform(38.5,66.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1283.2,-547.8,1920.5,854.2);


(lib.Pulando = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.rot5();
	this.instance.setTransform(-1293.4,-189.2,1,1,0,0,0,-841.1,-131);
	this.instance.filters = [new cjs.ColorFilter(0.69, 0.69, 0.69, 1, 79.05, 79.05, 79.05, 0)];
	this.instance.cache(-1803,-550,1925,858);

	this.instance_1 = new lib.rot4();
	this.instance_1.setTransform(-1276.5,-189.2,1,1,0,0,0,-719.2,-121);

	this.instance_2 = new lib.Shape17_2();
	this.instance_2.setTransform(-1248.6,-191.8,1,1,0,0,0,960.2,427.1);
	this.instance_2.filters = [new cjs.ColorFilter(0.46, 0.46, 0.46, 1, 137.7, 137.7, 137.7, 0)];
	this.instance_2.cache(1583,608,32,39);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B2DAD7").s().p("AnRIYIA5iPIAAsRIg5iPIOjAAIg5CPIAAMRIA5CPgAGdHzIgnhmIABsgIAmhgIs5AAIAoBnIgCMgIgmBfIM5AAgAEvGQIAAgpIAoAAIAAApgADeGQIAAgpIAoAAIAAApgACNGQIAAgpIAoAAIAAApgAA8GQIAAgpIApAAIAAApgAgTGQIAAgpIAnAAIAAApgAhjGQIAAgpIAoAAIAAApgAi0GQIAAgpIAoAAIAAApgAkFGQIAAgpIAoAAIAAApgAlWGQIAAgpIApAAIAAApgAEvljIAAgpIAoAAIAAApgADeljIAAgpIAoAAIAAApgACNljIAAgpIAoAAIAAApgAA8ljIAAgpIApAAIAAApgAgTljIAAgpIAnAAIAAApgAhjljIAAgpIAoAAIAAApgAi0ljIAAgpIAoAAIAAApgAkFljIAAgpIAoAAIAAApgAlWljIAAgpIApAAIAAApg");
	this.shape.setTransform(-627.8,8.7);

	this.instance_3 = new lib.Rot2();
	this.instance_3.setTransform(-1237,-190.7,1,1,0,0,0,-458.2,-114);
	this.instance_3.filters = [new cjs.ColorFilter(0.87, 0.87, 0.87, 1, 31.46, 33.15, 33.15, 0)];
	this.instance_3.cache(-1420,-530,1925,858);

	this.instance_4 = new lib.rot1();
	this.instance_4.setTransform(-1209.8,-192.1,1,1,0,0,0,-323.1,-133.9);
	this.instance_4.filters = [new cjs.ColorFilter(0.83, 0.83, 0.83, 1, 43.35, 43.35, 43.35, 0)];
	this.instance_4.cache(-1285,-550,1925,858);

	this.instance_5 = new lib.Shape17_2();
	this.instance_5.setTransform(-686.7,-191.8,1,1,0,0,0,960.2,427.1);
	this.instance_5.filters = [new cjs.ColorFilter(0.46, 0.46, 0.46, 1, 137.7, 137.7, 137.7, 0)];
	this.instance_5.cache(1583,608,32,39);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B2DAD7").s().p("AnRIYIA5iPIAAsRIg5iPIOjAAIg5CPIAAMRIA5CPgAGdHzIgnhmIABsgIAmhgIs5AAIAoBnIgCMgIgmBfIM5AAgAEvGQIAAgpIAoAAIAAApgADeGQIAAgpIAoAAIAAApgACNGQIAAgpIAoAAIAAApgAA8GQIAAgpIApAAIAAApgAgTGQIAAgpIAnAAIAAApgAhjGQIAAgpIAoAAIAAApgAi0GQIAAgpIAoAAIAAApgAkFGQIAAgpIAoAAIAAApgAlWGQIAAgpIApAAIAAApgAEvljIAAgpIAoAAIAAApgADeljIAAgpIAoAAIAAApgACNljIAAgpIAoAAIAAApgAA8ljIAAgpIApAAIAAApgAgTljIAAgpIAnAAIAAApgAhjljIAAgpIAoAAIAAApgAi0ljIAAgpIAoAAIAAApgAkFljIAAgpIAoAAIAAApgAlWljIAAgpIApAAIAAApg");
	this.shape_1.setTransform(-65.9,8.7);

	this.instance_6 = new lib.Rot2();
	this.instance_6.setTransform(-675.1,-190.7,1,1,0,0,0,-458.2,-114);
	this.instance_6.filters = [new cjs.ColorFilter(0.87, 0.87, 0.87, 1, 31.46, 33.15, 33.15, 0)];
	this.instance_6.cache(-1420,-530,1925,858);

	this.instance_7 = new lib.rot1();
	this.instance_7.setTransform(-647.9,-192.1,1,1,0,0,0,-323.1,-133.9);
	this.instance_7.filters = [new cjs.ColorFilter(0.83, 0.83, 0.83, 1, 43.35, 43.35, 43.35, 0)];
	this.instance_7.cache(-1285,-550,1925,858);

	this.instance_8 = new lib.rot5();
	this.instance_8.setTransform(-387.5,-187.3,1,1,0,0,0,-841.1,-131);
	this.instance_8.filters = [new cjs.ColorFilter(0.69, 0.69, 0.69, 1, 79.05, 79.05, 79.05, 0)];
	this.instance_8.cache(-1803,-550,1925,858);

	this.instance_9 = new lib.rot4();
	this.instance_9.setTransform(-370.5,-187.3,1,1,0,0,0,-719.2,-121);

	this.instance_10 = new lib.Shape17_2();
	this.instance_10.setTransform(-342.6,-189.9,1,1,0,0,0,960.2,427.1);
	this.instance_10.filters = [new cjs.ColorFilter(0.46, 0.46, 0.46, 1, 137.7, 137.7, 137.7, 0)];
	this.instance_10.cache(1583,608,32,39);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B2DAD7").s().p("AnRIYIA5iPIAAsRIg5iPIOjAAIg5CPIAAMRIA5CPgAGdHzIgnhmIABsgIAmhgIs5AAIAoBnIgCMgIgmBfIM5AAgAEvGQIAAgpIAoAAIAAApgADeGQIAAgpIAoAAIAAApgACNGQIAAgpIAoAAIAAApgAA8GQIAAgpIApAAIAAApgAgTGQIAAgpIAnAAIAAApgAhjGQIAAgpIAoAAIAAApgAi0GQIAAgpIAoAAIAAApgAkFGQIAAgpIAoAAIAAApgAlWGQIAAgpIApAAIAAApgAEvljIAAgpIAoAAIAAApgADeljIAAgpIAoAAIAAApgACNljIAAgpIAoAAIAAApgAA8ljIAAgpIApAAIAAApgAgTljIAAgpIAnAAIAAApgAhjljIAAgpIAoAAIAAApgAi0ljIAAgpIAoAAIAAApgAkFljIAAgpIAoAAIAAApgAlWljIAAgpIApAAIAAApg");
	this.shape_2.setTransform(278.1,10.6);

	this.instance_11 = new lib.Rot2();
	this.instance_11.setTransform(-331.1,-188.8,1,1,0,0,0,-458.2,-114);
	this.instance_11.filters = [new cjs.ColorFilter(0.87, 0.87, 0.87, 1, 31.46, 33.15, 33.15, 0)];
	this.instance_11.cache(-1420,-530,1925,858);

	this.instance_12 = new lib.rot1();
	this.instance_12.setTransform(-303.8,-190.2,1,1,0,0,0,-323.1,-133.9);
	this.instance_12.filters = [new cjs.ColorFilter(0.83, 0.83, 0.83, 1, 43.35, 43.35, 43.35, 0)];
	this.instance_12.cache(-1285,-550,1925,858);

	this.instance_13 = new lib.Shape17_2();
	this.instance_13.setTransform(219.3,-189.9,1,1,0,0,0,960.2,427.1);
	this.instance_13.filters = [new cjs.ColorFilter(0.46, 0.46, 0.46, 1, 137.7, 137.7, 137.7, 0)];
	this.instance_13.cache(1583,608,32,39);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#B2DAD7").s().p("AnRIYIA5iPIAAsRIg5iPIOjAAIg5CPIAAMRIA5CPgAGdHzIgnhmIABsgIAmhgIs5AAIAoBnIgCMgIgmBfIM5AAgAEvGQIAAgpIAoAAIAAApgADeGQIAAgpIAoAAIAAApgACNGQIAAgpIAoAAIAAApgAA8GQIAAgpIApAAIAAApgAgTGQIAAgpIAnAAIAAApgAhjGQIAAgpIAoAAIAAApgAi0GQIAAgpIAoAAIAAApgAkFGQIAAgpIAoAAIAAApgAlWGQIAAgpIApAAIAAApgAEvljIAAgpIAoAAIAAApgADeljIAAgpIAoAAIAAApgACNljIAAgpIAoAAIAAApgAA8ljIAAgpIApAAIAAApgAgTljIAAgpIAnAAIAAApgAhjljIAAgpIAoAAIAAApgAi0ljIAAgpIAoAAIAAApgAkFljIAAgpIAoAAIAAApgAlWljIAAgpIApAAIAAApg");
	this.shape_3.setTransform(840,10.6);

	this.instance_14 = new lib.Rot2();
	this.instance_14.setTransform(230.8,-188.8,1,1,0,0,0,-458.2,-114);
	this.instance_14.filters = [new cjs.ColorFilter(0.87, 0.87, 0.87, 1, 31.46, 33.15, 33.15, 0)];
	this.instance_14.cache(-1420,-530,1925,858);

	this.instance_15 = new lib.rot1();
	this.instance_15.setTransform(258.1,-190.2,1,1,0,0,0,-323.1,-133.9);
	this.instance_15.filters = [new cjs.ColorFilter(0.83, 0.83, 0.83, 1, 43.35, 43.35, 43.35, 0)];
	this.instance_15.cache(-1285,-550,1925,858);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_15},{t:this.instance_14},{t:this.shape_3},{t:this.instance_13},{t:this.instance_12},{t:this.instance_11},{t:this.shape_2},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.shape_1},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.shape},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2253.7,-618.9,3472.1,870.5);


(lib.Produtos_ = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.Pulando();
	this.instance.setTransform(-517.7,-183.8,1,1,0,0,0,-517.7,-183.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2253.7,-618.9,3472.1,870.5);


(lib.Notebook = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.mack();
	this.instance.setTransform(-0.1,0.2,1.771,1.771,0,0,0,340.4,205);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-603,-362.9,1206.4,726.3);


(lib.Frutas = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.Couve();
	this.instance.setTransform(93.6,8.1,1,1,0,0,0,52.5,47.5);
	this.instance.filters = [new cjs.ColorFilter(0.5, 0.5, 0.5, 1, 127.5, 127.5, 127.5, 0)];
	this.instance.cache(-2,-2,109,99);

	this.instance_1 = new lib.bananas();
	this.instance_1.setTransform(-49.1,9.1,1,1,0,0,0,57.5,46.5);
	this.instance_1.filters = [new cjs.ColorFilter(0.44, 0.44, 0.44, 1, 142.8, 142.8, 142.8, 0)];
	this.instance_1.cache(-2,-2,119,97);

	this.instance_2 = new lib.maça();
	this.instance_2.setTransform(-312.9,0,1,1,0,0,0,47.5,55.5);
	this.instance_2.filters = [new cjs.ColorFilter(0.5, 0.5, 0.5, 1, 127.5, 127.5, 127.5, 0)];
	this.instance_2.cache(-2,-2,99,115);

	this.instance_3 = new lib.cenoura();
	this.instance_3.setTransform(-175.6,4,1,1,0,0,0,34.5,55.5);
	this.instance_3.filters = [new cjs.ColorFilter(0.5, 0.5, 0.5, 1, 127.5, 127.5, 127.5, 0)];
	this.instance_3.cache(-2,-2,73,115);

	this.instance_4 = new lib.laranja();
	this.instance_4.setTransform(-454.7,9.6,1,1,0,0,0,45,46);
	this.instance_4.filters = [new cjs.ColorFilter(0.5, 0.5, 0.5, 1, 127.5, 127.5, 127.5, 0)];
	this.instance_4.cache(-2,-2,94,96);

	this.instance_5 = new lib.Couve();
	this.instance_5.setTransform(769.4,8.1,1,1,0,0,0,52.5,47.5);
	this.instance_5.filters = [new cjs.ColorFilter(0.5, 0.5, 0.5, 1, 127.5, 127.5, 127.5, 0)];
	this.instance_5.cache(-2,-2,109,99);

	this.instance_6 = new lib.bananas();
	this.instance_6.setTransform(626.7,9.1,1,1,0,0,0,57.5,46.5);
	this.instance_6.filters = [new cjs.ColorFilter(0.44, 0.44, 0.44, 1, 142.8, 142.8, 142.8, 0)];
	this.instance_6.cache(-2,-2,119,97);

	this.instance_7 = new lib.maça();
	this.instance_7.setTransform(362.9,0,1,1,0,0,0,47.5,55.5);
	this.instance_7.filters = [new cjs.ColorFilter(0.5, 0.5, 0.5, 1, 127.5, 127.5, 127.5, 0)];
	this.instance_7.cache(-2,-2,99,115);

	this.instance_8 = new lib.cenoura();
	this.instance_8.setTransform(500.2,4,1,1,0,0,0,34.5,55.5);
	this.instance_8.filters = [new cjs.ColorFilter(0.5, 0.5, 0.5, 1, 127.5, 127.5, 127.5, 0)];
	this.instance_8.cache(-2,-2,73,115);

	this.instance_9 = new lib.laranja();
	this.instance_9.setTransform(221.1,9.6,1,1,0,0,0,45,46);
	this.instance_9.filters = [new cjs.ColorFilter(0.5, 0.5, 0.5, 1, 127.5, 127.5, 127.5, 0)];
	this.instance_9.cache(-2,-2,94,96);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-499.7,-55.5,1321.7,115.1);


(lib.Interpolação2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Camada 1
	this.instance = new lib.Frutas();
	this.instance.setTransform(1276.3,2.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(776.6,-52.8,1321.6,115.1);


// stage content:
(lib.Versão3_Animação = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Notebook
	this.instance = new lib.Notebook();
	this.instance.setTransform(905.8,389.7,0.498,0.498,0,0,0,0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(141));

	// Camada 5 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Eg5yAalMAAAg1JMBzlAAAMAAAA1Jg");
	mask.setTransform(1463.3,354.3);

	// Produtos
	this.instance_1 = new lib.Produtos_();
	this.instance_1.setTransform(385.8,257,1,1,0,0,0,-517.7,-183.8);

	this.instance_1.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({x:1289.8},140).wait(1));

	// Esteira
	this.instance_2 = new lib.Bitmap2();
	this.instance_2.setTransform(-16,515,0.952,1);

	this.instance_3 = new lib.Bitmap2();
	this.instance_3.setTransform(-16,506,0.952,1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3},{t:this.instance_2}]}).wait(141));

	// Camada 8 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("EhOmAXqMAAAgvTMCdNAAAMAAAAvTg");
	mask_1.setTransform(468.9,356.9);

	// Frutas
	this.instance_4 = new lib.Interpolação2("synched",0);
	this.instance_4.setTransform(-1426.4,447.8);

	this.instance_4.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({x:-751.4},140).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(865.8,484.2,1867.5,386.6);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;
